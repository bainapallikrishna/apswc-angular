import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import {Employee,employee} from 'src/app/Interfaces/employee'

@Component({
  selector: 'app-employee-registration',
  templateUrl: './employee-registration.component.html',
  styleUrls: ['./employee-registration.component.css']
})
export class EmployeeRegistrationComponent implements OnInit {

  public emp:Employee;
  employeename:string;
  employeeid:string;
  dob:string;
  gender:string;
  employeemobile:string;
  employeemailid:string;
  sectionname:string;
  designation:string;
  
  
  employeeaddress:string;
  doj:string;
  emp1:employee;
  Sections:any;
  Designations:any;
  Locations:any;
  constructor() { }
  location="";
  locationtype="null";
  
  ngOnInit(): void {
  
    this.Sections=["B AND L","M AND QC","F AND A","Engineering","P AND A","Conference"];
    this.Designations=["GM","AGM","DGM","EE","Sr.Asst","Jr.Asst","RM","WHM"];
    this.Locations=[{Id:'R0001',Name:'Vizianagaram'},{Id:'R0002',Name:'Kakinada'},
    {Id:'R0003',Name:'TP Gudem'},{Id:'R0004',Name:'Vijayawada'},{Id:'R0005',Name:'Kadapa'}]
  }

  public onRegistration = () => {

    this.emp={
      employeeName:this.employeename,
      employeeId:this.employeeid,
      employeeMobile:this.employeemobile,
      employeeDob:this.dob,
      employeeGender:this.gender,
      employeeAddress:this.employeeaddress,
      employeeMailId:this.employeemailid,
      employeedesignation:this.designation,
      employeelocationType:this.locationtype,
      employeelocation:this.location,
      employeesection:this.sectionname,
      employeedateofjoining:this.doj
    };

    console.log(this.emp);

  }

}
