import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-warehouse-registration',
  templateUrl: './warehouse-registration.component.html',
  styleUrls: ['./warehouse-registration.component.css']
})
export class WarehouseRegistrationComponent implements OnInit {
  districtList:any;
  Regions:any;
  warehouseTypelist:any;
  warehousesubTypelist:any;
  EmployeeList:any;
  district:string;
  region:string;
  Warehousename:string;
  Warehousecode:string;
  warehousetype:string;
  waresubtype:string;
  capacity:string;
  warehousemanagername:string;
  mobile:string;
  constructor() { }

  ngOnInit(): void {
    this.districtList=[

      {districtCode:502,districtName:'ANANTAPUR'},
      {districtCode:503,districtName:'CHITTOOR'},
      {districtCode:505,districtName:'EAST GODAVARI'},
      {districtCode:506,districtName:'GUNTUR'},
      {districtCode:510,districtName:'KRISHNA'},
      {districtCode:511,districtName:'KURNOOL'},
      {districtCode:517,districtName:'PRAKASAM'},
      {districtCode:515,districtName:'SPSR NELLORE'},
      {districtCode:519,districtName:'SRIKAKULAM'},
      {districtCode:520,districtName:'VISAKHAPATANAM'},
      {districtCode:521,districtName:'VIZIANAGARAM'},
      {districtCode:523,districtName:'WEST GODAVARI'},
      {districtCode:504,districtName:'	Y.S.R.KADAPA'}
    ];
    this.Regions=[{Id:'R0001',Name:'Vizianagaram'},{Id:'R0002',Name:'Kakinada'},
    {Id:'R0003',Name:'TP Gudem'},{Id:'R0004',Name:'Vijayawada'},{Id:'R0005',Name:'Kadapa'}];
    this.warehouseTypelist=["AMC","Hired","IG","OWN","PEG Godowns"];

    this.warehousesubTypelist=["AMC","AMC-Hired","AMC-OWN","AMC-Pvt","HG","Hired","IG","OWN","OWN-PEG"];

    this.EmployeeList=[{empcode:'emp01',empName:'Sri.V.S.V.N.Radha Krishna'},
    {empcode:'emp02',empName:'Sri.E.Ravi Kumar'},
    {empcode:'emp03',empName:'Sri P.Adinarayana'},
    {empcode:'emp04',empName:'Sri.K.Ravi Raj'},
    {empcode:'emp05',empName:'Sri V.S Appa Rao'}]


  }

  public onRegistration=()=> {
  
    

  }

}
