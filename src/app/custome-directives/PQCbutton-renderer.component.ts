import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
  selector: 'edit-button-renderer',
  template: `
    <span  class="btn btn-success btn-sm " (click)="PQCClick($event)" >Quality Checking </span>
    <span class="btn btn-primary btn-sm ml-1" (click)="DisiClick($event)">Disinfestation</span>
    <span class="btn btn-info btn-sm ml-1" (click)="DisiHistoryClick($event)">Disinfestation History</span>
    <span class="btn btn-warning btn-sm ml-1" (click)="SpillageClick($event)">Spillage</span>
      `
})

export class PQCbuttonRendererComponent implements ICellRendererAngularComp {

  params;
  label: string;

  agInit(params): void {
    this.params = params;
    this.label = this.params.label || null;
  }

  refresh(params?: any): boolean {
    return true;
  }

  PQCClick($event) {
    if (this.params.PQCClick instanceof Function) {
      // put anything into params u want pass into parents component
      const params = {
        event: $event,
        rowData: this.params.node.data
        // ...something
      }
      this.params.PQCClick(params);

    }
  }

  DisiHistoryClick($event) {
    if (this.params.DisiHistoryClick instanceof Function) {
      // put anything into params u want pass into parents component
      const params = {
        event: $event,
        rowData: this.params.node.data
        // ...something
      }
      this.params.DisiHistoryClick(params);

    }
  }

  DisiClick($event) {
    if (this.params.DisiClick instanceof Function) {
      // put anything into params u want pass into parents component
      const params = {
        event: $event,
        rowData: this.params.node.data
        // ...something
      }
      this.params.DisiClick(params);

    }
  }

  SpillageClick($event) {
    if (this.params.SpillageClick instanceof Function) {
      // put anything into params u want pass into parents component
      const params = {
        event: $event,
        rowData: this.params.node.data
        // ...something
      }
      this.params.SpillageClick(params);

    }
  }

}