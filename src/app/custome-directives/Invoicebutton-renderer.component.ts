import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'edit-button-renderer',
    template: `
    <span title='true' class="btn btn-success btn-sm " *ngIf="params.value=='0'" (click)="nextClick($event)" >
    <i class="fa fa-plus-circle"></i> Generate Invoice
    </span>
    
    <span title='true' class="btn btn-warning  btn-sm " *ngIf="params.value=='1'" (click)="nextClick($event)" >
    <i class="fa fa-download" aria-hidden="true"></i> Download Invoice
    </span>

      `
})

export class InvoicebuttonRendererComponent implements ICellRendererAngularComp {

    params;
    label: string;
  
    agInit(params): void {
      
      this.params = params;
      this.label = this.params.label || null;
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    nextClick($event) {
      if (this.params.nextClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.nextClick(params);
  
      }
    }
  }