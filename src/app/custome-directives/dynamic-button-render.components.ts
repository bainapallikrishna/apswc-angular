import { Component } from '@angular/core';
import { ICellRendererAngularComp } from "ag-grid-angular";

@Component({
    selector: 'edit-button-renderer',
    template: `
    <span title='true' class="btn btn-primary btn-sm " *ngIf="!params.value" (click)="ButtonClick($event)" >
    {{label}}
    </span>
    <span title='true' class="btn btn-success btn-sm " *ngIf="params.value"  (click)="DownloadReceipt($event)">
    {{label2}}
    </span>
    
      `
})

export class DynamicbuttonRendererComponent implements ICellRendererAngularComp {

    params;
    label: string;
    label2: string;
  
    agInit(params): void {
      this.params = params;
      this.label = this.params.label || null;
      this.label2 = this.params.label2 || null;
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    ButtonClick($event) {
      if (this.params.ButtonClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.ButtonClick(params);
  
      }
    }

    DownloadReceipt($event) {
      if (this.params.DownloadReceipt instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.DownloadReceipt(params);
  
      }
    }
  }