import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'edit-button-renderer',
    template: `
    <span title='true' class="btn btn-success btn-sm " *ngIf="params.value!='Com'" (click)="nextClick($event)" >
    Next
    </span>
      `
})

export class GBbuttonRendererComponent implements ICellRendererAngularComp {

    params;
    label: string;
  
    agInit(params): void {
      this.params = params;
      this.label = this.params.label || null;
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    nextClick($event) {
      if (this.params.nextClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.nextClick(params);
  
      }
    }
  }