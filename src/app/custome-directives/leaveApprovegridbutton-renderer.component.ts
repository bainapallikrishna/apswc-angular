import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'edit-button-renderer',
    template: `
    <button [disabled]="disableApprove" type="button" class="btn btn-success btn-sm" (click)="ApproveClick($event)">{{btnEdit}}</button>&nbsp;
    <button [disabled]="disableReject" class="btn btn-primary btn-sm" (click)="RejectClick($event);">{{btnReject}}</button>&nbsp;
    <button class="btn btn-primary btn-sm" (click)="ViewClick($event);">{{btnView}}</button>&nbsp;
    <button class="btn btn-primary btn-sm" (click)="historyClick($event);">{{btnHistory}}</button>&nbsp;
  
      `
})

export class leaveApprovegridbuttoncomponent implements ICellRendererAngularComp {

    params;
    label: string;
  
    btnEdit: string;
    btnReject:string;
    btnView: string;
    btnHistory: string;
    disableApprove:boolean = false;
    disableReject:boolean=false;
    agInit(params): void {
      this.params = params;
      this.label = this.params.label || null;
      this.btnReject = this.params.btnReject || null;
     
      this.btnEdit = this.params.btnEdit || null;
      this.btnView = this.params.btnView || null;
      this.btnHistory = this.params.btnHistory || null;

      this.disableApprove = this.params.disableApprove || null;
      this.disableReject = this.params.disableReject || null;
      
      //console.log(params.data.status);
      if(params.data.status=="Cancelled" || params.data.status=="Approved" || params.data.status=="Rejected"){this.disableApprove=true;}
      if(params.data.status=="Cancelled" || params.data.status=="Approved" || params.data.status=="Rejected"){this.disableReject=true;}
   
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    ApproveClick($event) {
      if (this.params.ApproveClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }

        this.params.ApproveClick(params);
  
      }
    }

    RejectClick($event) {
      if (this.params.RejectClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }

        this.params.RejectClick(params);
  
      }
    }

    ViewClick($event) {
      if (this.params.ViewClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }

        this.params.ViewClick(params);
  
      }
    }

    

    historyClick($event) {
      if (this.params.historyClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.historyClick(params);
  
      }
    }


  }