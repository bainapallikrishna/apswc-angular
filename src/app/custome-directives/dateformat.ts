import { Injectable } from "@angular/core";
import { NgbCalendar, NgbDateStruct } from "@ng-bootstrap/ng-bootstrap";

@Injectable({
    providedIn: 'root'
})
export class CustomDateParserFormatter {
    constructor(private calender: NgbCalendar) { }
    parse(value: string): NgbDateStruct {
        if (!value)
            return null
        let parts = value.split('-');
        return { year: +parts[0], month: +parts[1], day: +parts[2] } as NgbDateStruct

    }
    format(date: NgbDateStruct): string {
        return date ? date.year + "-" + ('0' + date.month).slice(-2) + "-" + ('0' + date.day).slice(-2) : null
    }

    getage(date: NgbDateStruct): number {
        if (!date)
            return 0;
        let currdate = this.calender.getToday();
        var currdatedays = (currdate.year * 365) + (currdate.month * 30) + currdate.day;
        var seldatedays = (date.year * 365) + (date.month * 30) + date.day;
        return Math.abs((currdatedays - seldatedays) / 365);
    }

    maxDate(): NgbDateStruct {
        
        let currdate = this.calender.getToday();
        return { year: currdate.year, month: currdate.month, day: currdate.day } as NgbDateStruct
        
    }

    GreaterDate(from: NgbDateStruct, to: NgbDateStruct): boolean {
        var fromdays = (from.year * 365) + (from.month * 30) + from.day;
        var todays = (to.year * 365) + (to.month * 30) + to.day;
        if (fromdays >= todays)
            return true;
        else
            return false;
    }

    HolidayGreaterDate(from: NgbDateStruct, to: NgbDateStruct): boolean {
        var fromdays = (from.year * 365) + (from.month * 30) + from.day;
        var todays = (to.year * 365) + (to.month * 30) + to.day;
        if (fromdays > todays)
        return true;
        else
            return false;
    }

    NoofDays(from: NgbDateStruct, to: NgbDateStruct): number {
        var fromdays = (from.year * 365) + (from.month * 30) + from.day;
        var todays = (to.year * 365) + (to.month * 30) + to.day;
           return (todays-fromdays);
    }

    DiffOfYears(from: NgbDateStruct, to: NgbDateStruct) : number{
        var fromdays = (from.year * 365) + (from.month * 30) + from.day;
        var todays = (to.year * 365) + (to.month * 30) + to.day;
        return Math.abs((todays - fromdays) / 365);
    }
}