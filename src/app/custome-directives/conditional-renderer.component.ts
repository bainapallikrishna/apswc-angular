import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'custom-button-cell',
    template: `   
    
     <span title='true' *ngIf="params.value!=null" >
     <i class="fa fa-check" aria-hidden="true" style="color: green;"></i>
     </span>
     <span title='true' *ngIf="params.value==null">
      <i class="fa fa-times" aria-hidden="true" style="color: red;"></i>
     </span>

    
    
      `
})

export class ConditionalRenderer implements ICellRendererAngularComp {

    params:any;
    label: string;
  
    agInit(params): void {
      this.params = params;
      this.label = this.params.label || null;
      
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  

   


  }