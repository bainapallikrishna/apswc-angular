import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'edit-button-renderer',
    template: `
    `
})

//    <span class="btn btn-danger btn-sm " (click)="CancelClick($event)" *ngIf="params.value=='0'">Cancel</span>
//<span *ngIf="params.value=='1'">NA</span>
//<span class="btn btn-info btn-sm " (click)="CancelClick($event)" *ngIf="params.value=='2'">View</span>


export class DSButtonRendererComponent implements ICellRendererAngularComp {

    params;
    label: string;
  
    agInit(params): void {
      this.params = params;
      this.label = this.params.label || null;
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    CancelClick($event) {
      if (this.params.CancelClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.CancelClick(params);
  
      }
    }
  }