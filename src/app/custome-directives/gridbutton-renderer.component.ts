import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'edit-button-renderer',
    template: `
    <button [disabled]="disableRefreshButtons" type="button" class="btn btn-success btn-sm" (click)="editClick($event)">{{btnEdit}}</button>&nbsp;
    <button class="btn btn-primary btn-sm" (click)="viewClick($event);">{{btnView}}</button>&nbsp;
    <button class="btn btn-primary btn-sm" (click)="historyClick($event);">{{btnHistory}}</button>
      `
})

export class gridbuttonrenderer implements ICellRendererAngularComp {

    params;
    label: string;
  
    btnEdit: string;
    btnView: string;
    btnHistory: string;
    disableRefreshButtons:boolean = false;
    agInit(params): void {
      this.params = params;
      this.label = this.params.label || null;
      this.btnEdit = this.params.btnEdit || null;
      this.btnView = this.params.btnView || null;
      this.btnHistory = this.params.btnHistory || null;

      this.disableRefreshButtons = this.params.disableRefreshButtons || null;
      
      //console.log(params.data.status);
      if(params.data.status=="Cancelled" || params.data.status=="Approved" || params.data.status=="Rejected"){this.disableRefreshButtons=true;}
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    editClick($event) {
      if (this.params.editClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }

        this.params.editClick(params);
  
      }
    }

    viewClick($event) {
      if (this.params.viewClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.viewClick(params);
  
      }
    }

    historyClick($event) {
      if (this.params.historyClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.historyClick(params);
  
      }
    }


  }