import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'edit-button-renderer',
    template: `
    <span title='true' class="btn btn-success btn-sm mx-1" *ngIf="params.value=='0'" (click)="editClick($event)">
    Approve/Reject
    </span>
    <button type="button" class="btn btn-success btn-sm mx-1" (click)="historyClick($event)">History</button>

    <button type="button" class="btn btn-success btn-sm" (click)="viewClick($event)">Upload GGH/AarogyaSri Certificate</button>`
  })

export class MedicalAPPREJBUTTONRendererComponent implements ICellRendererAngularComp {



    params;
    label: string;
  
    agInit(params): void {
    
   
      this.params = params;
      this.label = this.params.label || null;
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    editClick($event) {
      if (this.params.editClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
         
          // ...something
        }
        this.params.editClick(params);
  
      }
    }
    historyClick($event) {
      if (this.params.historyClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.historyClick(params);
  
      }
    }

    viewClick($event) {
      if (this.params.viewClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.viewClick(params);
  
      }
    }
 



  }