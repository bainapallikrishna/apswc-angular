import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'edit-button-renderer',
    template: `
    <span class="btn btn-success btn-sm mx-1" *ngIf="params.value=='0'" (click)="ApproveClick($event)">Approve/Reject</span>
    <span class="btn btn-info btn-sm mx-1" (click)="ViewClick($event)">History</span>`
    //<span class="btn btn-danger btn-sm" (click)="RejectClick($event)">Reject</span>
})

export class DSAPPButtonRendererComponent implements ICellRendererAngularComp {

    params;
    label: string;
  
    agInit(params): void {
      this.params = params;
      this.label = this.params.label || null;
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    ApproveClick($event) {
      if (this.params.ApproveClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.ApproveClick(params);
  
      }
    }

    RejectClick($event) {
      if (this.params.RejectClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.RejectClick(params);
  
      }
    }

    ViewClick($event) {
      if (this.params.ViewClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.ViewClick(params);
  
      }
    }
  }