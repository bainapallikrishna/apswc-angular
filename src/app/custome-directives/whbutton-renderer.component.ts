import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'edit-button-renderer',
    template: ` 
    <button class="btn btn-primary btn-sm" (click)="historyClick($event);">History</button>
      `
})

export class WHButtonRendererComponent implements ICellRendererAngularComp {

    params;
    label: string;
  
    agInit(params): void {
      this.params = params;
      this.label = this.params.label || 'Edit';
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    editClick($event) {
      if (this.params.editClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.editClick(params);
  
      }
    }

    viewClick($event) {
      if (this.params.viewClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.viewClick(params);
  
      }
    }

    historyClick($event) {
      if (this.params.historyClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.historyClick(params);
  
      }
    }


  }