import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'edit-button-renderer',
    template: `
    <button type="button" class="btn btn-success btn-sm mr-1"  *ngIf="params.value=='0'" (click)="editClick($event,1)">Approve/Reject</button>
    <button type="button" class="btn btn-success btn-sm mr-1" (click)="historyClick($event)">History</button>
    `
})

export class OFFICERAPPREJBUTTONRendererComponent implements ICellRendererAngularComp {

    params;
    label: string;
  
    agInit(params): void {
      this.params = params;
      this.label = this.params.label || null;
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    editClick($event,type) {
      if (this.params.editClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data,
          bttype:type
          // ...something
        }
        this.params.editClick(params);
  
      }
    }

    viewClick($event) {
      if (this.params.viewClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.viewClick(params);
  
      }
    }

    historyClick($event) {
      if (this.params.historyClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.historyClick(params);
  
      }
    }

}