import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'edit-button-renderer',
    template: `
    <button type="button" class="btn btn-success btn-sm" (click)="editClick($event)">Edit</button>
    <button type="button" class="btn btn-success btn-sm mx-1" (click)="transClick($event)">Employee Transfer</button>
    <button class="btn btn-primary btn-sm " (click)="viewClick($event);">View</button>
    <button class="btn btn-primary btn-sm ml-1" (click)="historyClick($event);">History</button>
      `
})

export class EMPButtonRendererComponent implements ICellRendererAngularComp {

    params;
    label: string;
  
    agInit(params): void {
      this.params = params;
      this.label = this.params.label || null;
    }
  
    refresh(params?: any): boolean {
      return true;
    }
  
    editClick($event) {
      if (this.params.editClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.editClick(params);
  
      }
    }

    
    transClick($event) {
      if (this.params.transClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.transClick(params);
  
      }
    }

    viewClick($event) {
      if (this.params.viewClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.viewClick(params);
  
      }
    }

    historyClick($event) {
      if (this.params.historyClick instanceof Function) {
        // put anything into params u want pass into parents component
        const params = {
          event: $event,
          rowData: this.params.node.data
          // ...something
        }
        this.params.historyClick(params);
  
      }
    }


  }