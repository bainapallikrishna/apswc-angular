import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyTransactionRegComponent } from './daily-transaction-reg.component';

describe('DailyTransactionRegComponent', () => {
  let component: DailyTransactionRegComponent;
  let fixture: ComponentFixture<DailyTransactionRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyTransactionRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyTransactionRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
