import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyValuationStockComponent } from './daily-valuation-stock.component';

describe('DailyValuationStockComponent', () => {
  let component: DailyValuationStockComponent;
  let fixture: ComponentFixture<DailyValuationStockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyValuationStockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyValuationStockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
