import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankloanregisterComponent } from './bankloanregister.component';

describe('BankloanregisterComponent', () => {
  let component: BankloanregisterComponent;
  let fixture: ComponentFixture<BankloanregisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankloanregisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankloanregisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
