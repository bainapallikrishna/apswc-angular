import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StackRegisterComponent } from './stack-register.component';

describe('StackRegisterComponent', () => {
  let component: StackRegisterComponent;
  let fixture: ComponentFixture<StackRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StackRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StackRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
