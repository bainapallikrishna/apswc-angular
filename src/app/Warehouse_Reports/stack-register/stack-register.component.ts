import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';

@Component({
  selector: 'app-stack-register',
  templateUrl: './stack-register.component.html',
  styleUrls: ['./stack-register.component.css']
})
export class StackRegisterComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  public components;
  public defaultColDef;
  public icons;
  StockDetails: any[];
  showloader: boolean = true;
  DepositorForm: FormGroup;
  isSubmit: boolean = false;
  istable: boolean = false;

  Godownlist: any[];
  Compartlist: any[];
  Stacklist: any[];
  cmdtylist: any[];

  WHTypelist:any[];
  whlist:any[];
  NgbDateStruct:any;
  isdiv: boolean = false;  

  @ViewChild('agGrid') agGrid: AgGridAngular;

  columnDefs = [
    { headerName: '#', width: 60, cellRenderer: 'rowIdRenderer',floatingFilter:false },
    { headerName: 'Compartment', width: 100, field: 'compartmenT_NAME' },
    { headerName: 'Stack', width: 80, field: 'stacK_NAME' },
    { headerName: 'Commodity', width: 100, field: 'commodity' },
    { headerName: 'Date Of Receipt/Issue', width: 150, field: 'transactioN_DATE', 
    cellRenderer: params => {
      // put the value in bold
      //return 'Value is **' + params.value  + '**';
      return params.value ? (new Date(params.value)).toLocaleDateString() : '';
    },
    sortable: true, filter: true },
    { headerName: 'ACK/RO No.', width: 80, field: 'acK_NO'},

    {
      headerName: 'Quantity Received', width: 300,
      children: [
        { headerName: 'No. of Bags', field: 'receiveD_BAGS', width: 120 },
        { headerName: 'Net Weight(MTs)', field: 'receiveD_WEIGHT', width: 120 },
        // {
        //   headerName: 'Net Weight', width: 240,
        //   children: [
        //     { headerName: 'MT', field: 'stacK_IN_WEIGHT', width: 80 },
        //     { headerName: 'Kgs', field: '', width: 80 },
        
        //   ]

        // },
      ]
    },

    { headerName: '% Moisture Content(Receipt)', width: 200, field: ''},

    {
      headerName: 'Quantity Delivered', width: 300,
      children: [
        { headerName: 'No. of Bags', field: 'delivereD_BAGS', width: 120 },
        { headerName: 'Net Weight(MTs)', field: 'delivereD_WEIGHT', width: 120 },
        // {
        //   headerName: 'Net Weight', width: 240,
        //   children: [
        //     { headerName: 'MT', field: 'stacK_OUT_WEIGHT', width: 80 },
        //     { headerName: 'Kgs', field: '', width: 80 },        
        //   ]

        // },
      ]
    },

    { headerName: '% Moisture Content(Issue)', width: 200, field: ''},

    {
      headerName: 'Closeing Balance', width: 300,
      children: [
        { headerName: 'No. of Bags', field: 'closinG_BALANCE_BAGS', width: 120 },
        { headerName: 'Net Weight(MTs)', field: 'closinG_BALANCE_WEIGHT', width: 120 },
        // {
        //   headerName: 'Net Weight', width: 240,
        //   children: [
        //     { headerName: 'MT', field: 'stacK_OUT_WEIGHT', width: 80 },
        //     { headerName: 'Kgs', field: '', width: 80 },        
        //   ]

        // },
      ]
    },
    { headerName: 'Date Of Inspection', width: 150, field: 'disinfectioN_DATE'},
    { headerName: 'Damage', width: 80, field: 'damage'},
    { headerName: 'Discolor', width: 80, field: 'discolor'},
    { headerName: 'Category', width: 80, field: 'category'},
    { headerName: 'M.C', width: 60, field: 'mc'},
    { headerName: 'Extent & Nature Of Infestation', width: 200, field: 'naturE_OF_INFESTATION'},
    { headerName: 'Classification', width: 120, field: 'clasification'},

    {
      headerName: 'Date Of Treatment Given',
      children: [
        { headerName: 'Spraying With Malathion', field: 'malation', width: 160 },
        { headerName: 'Spraying With Delta', field: 'delta', width: 150 },
        { headerName: 'Fumigation With ALP', field: 'alp', width: 150 },
        { headerName: 'Date Of Degassing', field: 'datE_OF_DEGASSING', width: 150 },
        
      ]
    },
    { headerName: 'Remarks', width: 100, field: 'remarks'}

   
  ];



  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private EncrDecr: EncrDecrServiceService,
    private datef: CustomDateParserFormatter,


  ) {


    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }


    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

    this.icons = {
      filter: ' '
    }
    
    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      wrapText: true,
      autoHeight: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };
  }

  ngOnInit(): void {

    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }

    this.DepositorForm = this.formBuilder.group({
      Godown: [null, Validators.required],
      Compart: [null],
      Stack: [null],
      cmdity: [null],
      WHtype:[null],
      Whname:[null],
      FromDate: [],
      ToDate: []

    });
    

    if(this.logUserrole=='101')
    {
      this.isdiv=true;
      this.istable=false;
      this.LoadWhDetails('WHTYPE');
      
    }
    else{
     // this.istable=true;
      this.isdiv=false;
      this.LoadGodowns('Godown')

    }
   
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //this.LoadDetails();
    params.api.setRowData(this.StockDetails);

  }

  LoadWhDetails(val) {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_08 = this.qualty.WHtype.value;
    this.service.postData(req, "GetWHDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if(val=="WHTYPE")
        this.WHTypelist = data.Details;
        else
        this.whlist = data.Details;
      }

    },
      error => console.log(error));
    

  }
  LoadWarehouses(val)
  {
    this.qualty.Whname.setValue(null);
    this.qualty.FromDate.setValue(this.datef.format(null));
    this.qualty.ToDate.setValue(this.datef.format(null));
    this.istable=false;
     this.LoadWhDetails(val);
  }

  LoadDetails() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_02 = this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    req.INPUT_03 = this.qualty.Godown.value;
    req.INPUT_04 = this.qualty.Compart.value;
    req.INPUT_05 = this.qualty.Stack.value;
    req.INPUT_06 = this.qualty.cmdity.value;

    req.INPUT_08 = this.qualty.WHtype.value;
    req.INPUT_09 = this.datef.format(this.qualty.FromDate.value);
    req.INPUT_10 = this.datef.format(this.qualty.ToDate.value);

    this.istable = true;
    this.service.postData(req, "GetStackRegisterDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        
        this.StockDetails = data.Details;
        this.gridApi.setRowData(this.StockDetails);


      }
      else {

        //Swal.fire('warning', data.StatusMessage, 'warning');
        this.gridApi.setRowData(data.Details);

      }




    },
      error => console.log(error));
    

  }


  LoadGodowns(val) {


    this.qualty.Compart.setValue(null);
    this.qualty.Stack.setValue(null);
    this.qualty.cmdity.setValue(null);
    this.Godownlist=[];
    this.Compartlist=[];
    this.Stacklist=[];
    this.cmdtylist=[];

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_02 = this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    this.service.postData(req, "GetGodown_Stack_cmprtdetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Godownlist = data.Details;
      }

    },
      error => console.log(error));
    

  }

  ChangeGodown(val) {

    this.qualty.Compart.setValue(null);
    this.qualty.Stack.setValue(null);
    this.qualty.cmdity.setValue(null);
    ;
    this.Compartlist=[];
    this.Stacklist=[];
    this.cmdtylist=[];
    this.istable=false;
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_02 = this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    req.INPUT_03 = this.qualty.Godown.value;
    this.service.postData(req, "GetGodown_Stack_cmprtdetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Compartlist = data.Details;
      }

    },
      error => console.log(error));
      

  }



  ChangeCompartment(val)
  {
    
    this.qualty.Stack.setValue(null);
    this.qualty.cmdity.setValue(null);    
    this.Stacklist=[];
    this.cmdtylist=[];
    this.istable=false;
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_02 = this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    req.INPUT_03 = this.qualty.Godown.value;
    req.INPUT_04 = this.qualty.Compart.value;
    this.service.postData(req, "GetGodown_Stack_cmprtdetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Stacklist = data.Details;

      }

    },
      error => console.log(error));
      

  }


  LoadCommodites(val)
  {

    
    this.qualty.cmdity.setValue(null);
    this.cmdtylist=[];
    this.istable=false;
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_02 = this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    req.INPUT_03 = this.qualty.Godown.value;
    req.INPUT_04 = this.qualty.Compart.value;
    req.INPUT_05 = this.qualty.Stack.value;
    this.service.postData(req, "GetGodown_Stack_cmprtdetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.cmdtylist = data.Details;

      }

    },
      error => console.log(error));
      

  }


  


 


  get qualty() { return this.DepositorForm.controls; }


  GetDetails() {

    this.isSubmit = true;
    this.StockDetails=[];
    if(this.logUserrole=="101")
    {
      if(!this.qualty.WHtype.value)
      {
        Swal.fire("warning","Select Warehouse Type","warning");
        return false;
      }
      
      if(!this.qualty.Whname.value)
      {
        Swal.fire("warning","Select Warehouse Name","warning");
        return false;
      }
      if(this.qualty.FromDate.value)  
      {
        if(!this.qualty.ToDate.value)
        {
          Swal.fire("warning","Please Select To Date","warning");
        return false;
        }
        if (this.datef.GreaterDate(this.qualty.FromDate.value, this.qualty.ToDate.value)) {
          Swal.fire('warning', "To Date Shoud be greater than From Date", 'warning');
          return false;
        }
      } 
     
      if (this.DepositorForm.invalid) {
        return false;
      }
      this.LoadDetails();

    }
    else
    {      

      if(this.qualty.FromDate.value)  
      {
        if(!this.qualty.ToDate.value)
        {
          Swal.fire("warning","Please Select To Date","warning");
        return false;
        }
        if (this.datef.GreaterDate(this.qualty.FromDate.value, this.qualty.ToDate.value)) {
          Swal.fire('warning', "To Date Shoud be greater than From Date", 'warning');
          return false;
        }
      } 
     
      if (this.DepositorForm.invalid) {
        return false;
      }

      this.LoadDetails();

    }
  }

}

