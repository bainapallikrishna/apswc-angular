import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LorryWeighbridgeReportComponent } from './lorry-weighbridge-report.component';

describe('LorryWeighbridgeReportComponent', () => {
  let component: LorryWeighbridgeReportComponent;
  let fixture: ComponentFixture<LorryWeighbridgeReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LorryWeighbridgeReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LorryWeighbridgeReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
