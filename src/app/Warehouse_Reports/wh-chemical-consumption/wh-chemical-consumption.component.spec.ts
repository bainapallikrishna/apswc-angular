import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WHChemicalConsumptionComponent } from './wh-chemical-consumption.component';

describe('WHChemicalConsumptionComponent', () => {
  let component: WHChemicalConsumptionComponent;
  let fixture: ComponentFixture<WHChemicalConsumptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WHChemicalConsumptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WHChemicalConsumptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
