import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';

@Component({
  selector: 'app-wh-chemical-consumption',
  templateUrl: './wh-chemical-consumption.component.html',
  styleUrls: ['./wh-chemical-consumption.component.css']
})
export class WHChemicalConsumptionComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  public components;
  public defaultColDef;
     public icons;
  StockDetails: any[];
  showloader: boolean = true;
  chemicallist: any[];
  DepositorForm: FormGroup;
  isSubmit: boolean = false;
  istable: boolean = false;


  WHTypelist:any[];
  whlist:any[];
  NgbDateStruct:any;
  isdiv: boolean = false; 
  pdfbtn:boolean=false;
  
  

  @ViewChild('agGrid') agGrid: AgGridAngular;

  columnDefs = [
    { headerName: '#', width: 60, cellRenderer: 'rowIdRenderer',floatingFilter:false },
    { headerName: 'Date', width: 150, field: 'date'},
    { headerName: 'Stack No', width: 150, field: 'stacK_ID'},
    { headerName: 'Godown No', width: 150, field: 'godowN_ID'},
    { headerName: 'Compartment', width: 150, field: 'compartmenT_ID'},
    { headerName: 'Number Of Bags', width: 180, field: 'nO_OF_BAGS'},
    { headerName: 'Quantity In MTs', width: 180, field: 'bagS_WEIGHT'},
   // { headerName: 'Area In SQ.KilloMeters', width: 200, field: ''},
    { headerName: 'Name Of Chemicals', width: 200, field: 'chemicaL_NAME'},    
    { headerName: 'Chemical Consumption', width: 250, field: 'chemicaL_QUANTITY'},

  ];



  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private EncrDecr: EncrDecrServiceService,
    private datef: CustomDateParserFormatter,


  ) {


    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }


    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
    this.icons = {
      filter: ' '
    }
    
    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      wrapText: true,
      autoHeight: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };
  }

  ngOnInit(): void {

    
    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }

    this.DepositorForm = this.formBuilder.group({
      chemicals: [null, Validators.required],
      WHtype:[null],
      Whname:[null],
      FromDate: [],
      ToDate: []
      

    });
    this.LoadChemicals()

    
    if(this.logUserrole=='101')
    {
      this.isdiv=true;
      this.istable=false;
      this.LoadWhDetails('WHTYPE');
      
    }
    else{
      
      this.isdiv=false;
      //this.LoadDetails();

    }
  }

  
  LoadWhDetails(val) {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_08 = this.qualty.WHtype.value;
    this.service.postData(req, "GetWHDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if(val=="WHTYPE")
        this.WHTypelist = data.Details;
        else
        this.whlist = data.Details;
      }

    },
      error => console.log(error));
    

  }
  LoadWarehouses(val)
  {
    this.qualty.Whname.setValue(null);
    this.qualty.FromDate.setValue(this.datef.format(null));
    this.qualty.ToDate.setValue(this.datef.format(null));
    this.istable=false;
     this.LoadWhDetails(val);
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;    
    params.api.setRowData(this.StockDetails);

  }

  LoadDetails() {

    this.showloader = true;
    const req = new InputRequest();
    
    req.INPUT_01= this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    req.INPUT_02 = this.qualty.chemicals.value;
    req.INPUT_08 = this.qualty.WHtype.value;
    req.INPUT_09 = this.datef.format(this.qualty.FromDate.value);
    req.INPUT_10 = this.datef.format(this.qualty.ToDate.value);

    this.service.postData(req, "GetChemicalConsumption").subscribe(data => {
      this.showloader = false;
      this.istable=true;
      if (data.StatusCode == "100") {
        this.StockDetails = data.Details;
        this.gridApi.setRowData(this.StockDetails);
        this.pdfbtn=true;
        


      }
      else {
        //Swal.fire('warning', data.StatusMessage, 'warning');
        this.gridApi.setRowData(data.Details);
        this.pdfbtn=false;

      }




    },
      error => console.log(error));
    

  }
  
  LoadChemicals() {

    this.showloader = true;
    const req = new InputRequest();   
    this.service.postData(req, "GetChemicals").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.chemicallist = data.Details;
      }

    },
      error => console.log(error));
   

  }

  

  ChangeChemical()
  {
    this.istable=false;
    this.StockDetails=[];
    this.qualty.FromDate.setValue(this.datef.format(null));
    this.qualty.ToDate.setValue(this.datef.format(null));
  
  }

  get qualty() { return this.DepositorForm.controls; }


  GetDetails() {

    this.isSubmit = true;
    this.StockDetails=[];
    
    if(this.logUserrole=="101")
    {
      if(!this.qualty.WHtype.value)
      {
        Swal.fire("warning","Select Warehouse Type","warning");
        return false;
      }
      
      if(!this.qualty.Whname.value)
      {
        Swal.fire("warning","Select Warehouse Name","warning");
        return false;
      }
      if(this.qualty.FromDate.value)  
      {
        if(!this.qualty.ToDate.value)
        {
          Swal.fire("warning","Please Select To Date","warning");
        return false;
        }
        if (this.datef.GreaterDate(this.qualty.FromDate.value, this.qualty.ToDate.value)) {
          Swal.fire('warning', "To Date Shoud be greater than From Date", 'warning');
          return false;
        }
      } 
     

      if (this.DepositorForm.invalid) {
        return false;
      }
      this.LoadDetails();

    }
    else
    {
      if(this.qualty.FromDate.value)  
      {
        if(!this.qualty.ToDate.value)
        {
          Swal.fire("warning","Please Select To Date","warning");
        return false;
        }
        if (this.datef.GreaterDate(this.qualty.FromDate.value, this.qualty.ToDate.value)) {
          Swal.fire('warning', "To Date Shoud be greater than From Date", 'warning');
          return false;
        }
      } 
     
      
      if (this.DepositorForm.invalid) {
        return false;
      }

      this.LoadDetails();

    }
  }

  Downloadpdf()
  {

    this.showloader = true;
    const req = new InputRequest();
    req.DIRECTION_ID="13";
    req.TYPEID="CHEMICALS_REPORT";
    req.INPUT_01= this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    req.INPUT_02 = this.qualty.chemicals.value;
    req.INPUT_08 = this.qualty.WHtype.value;
    req.INPUT_09 = this.datef.format(this.qualty.FromDate.value);
    req.INPUT_10 = this.datef.format(this.qualty.ToDate.value);


    this.service.postData(req, "APSWCMapsServiceConsume").subscribe(data => {
      this.showloader = false;
      if (data.Status == "Success") {
        var b = this.service.s_sd(data.CertIssuerData.Base64pdf, 'application/pdf');
        var l = document.createElement('a');
        l.href = window.URL.createObjectURL(b);
        l.download = "Chemical_Consumption"+".pdf";
        document.body.appendChild(l);
        l.click();
        document.body.removeChild(l);

        //console.log(data.Details);Base64pdf
        //this.fillPDFData(data.Details[0])
      }
      else
        Swal.fire('warning', data.Reason, 'warning');

    },
      error => console.log(error));
      

  }

}

