import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImprestRegisterComponent } from './imprest-register.component';

describe('ImprestRegisterComponent', () => {
  let component: ImprestRegisterComponent;
  let fixture: ComponentFixture<ImprestRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImprestRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImprestRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
