import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';

@Component({
  selector: 'app-depositor-ledger',
  templateUrl: './depositor-ledger.component.html',
  styleUrls: ['./depositor-ledger.component.css']
})
export class DepositorLedgerComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  public components;  
  public defaultColDef;
  public icons;
  StockDetails: any[];
  showloader: boolean = true;
  farmerlist: any[];
  DepositorForm: FormGroup;
  isSubmit: boolean = false;
  istable: boolean = false;


  WHTypelist:any[];
  whlist:any[];
  NgbDateStruct:any;
  isdiv: boolean = false;  
  pdfbtn:boolean=false;
 

  @ViewChild('agGrid') agGrid: AgGridAngular;

  columnDefs = [
    { headerName: '#', width: 60, cellRenderer: 'rowIdRenderer',floatingFilter:false },
    {
      headerName: 'Description Of Goods Receievd', width: 1450,
      children: [

        { headerName: 'Date Of Deposit', width: 180, field: 'deposiT_DATE'},
        { headerName: 'Warehouse Receipt No And Date', width: 300, field: 'receipT_NO_DATE'},
        { headerName: 'Description', width: 150, field: 'receivE_DESCRIPTION'},
        { headerName: 'Number Of Packages Or Bags Or Quantity', width: 350, field: 'receivE_BAGS'},
        { headerName: 'Weight Or/and measurement', width: 250, field: 'receivE_WEIGHT'},
        { headerName: 'Grade Or Quality Standard', width: 250, field: 'receivE_GRADE'},
      ]
    },

    {
      headerName: 'Description Of Goods Receievd Or Delivered', width: 1450,
      children: [
        { headerName: 'Description', width: 150, field: 'deliverY_DESCRIPTION'},
        { headerName: 'Number Of Packages Or Bags Or Quantity', width: 350, field: 'deliverY_BAGS'},
        { headerName: 'Weight Or/and measurement', width: 250, field: 'deliverY_WEIGHT'},
        { headerName: 'Grade Or Quality Standard', width: 250, field: 'deliverY_GRADE'},
        { headerName: 'Delivery Order No. And Date', width: 250, field: 'deliverY_NO_DATE'},
        { headerName: 'Insurance Charges And Out Of Pocket expenses on that A/C', width: 450, field: 'insurancE_CHARGES'},
      ]
    },

    {
      headerName: 'Expenses On', width: 500,
      children: [
        { headerName: 'Warehousing Charges', width: 250, field: 'warehousE_CHARGES' },
        { headerName: 'Account Of Miscellaneous Charges', width: 280, field: 'misC_CHARGES' },
      ]
    },

    {
      headerName: 'Particulars Of Payment made by Depositor', width: 700,
      children: [
        { headerName: 'Date of Payment', width: 200, field: 'paymenT_DATE' },
        { headerName: 'Amount', width: 150, field: 'amount'},
        { headerName: 'Description', width: 150, field: 'description'},
        { headerName: 'Number Of Packages Or Bags Or Quantity', width: 350, field: 'closinG_BALANCE_BAGS'},

      ]
    },

    {
      headerName: 'Balance Of Stock', width: 1600,
      children: [
        { headerName: 'Weight Or/ and measurement', width: 250, field: 'closinG_BALANCE_WEIGHT'},
        { headerName: 'Grade Or Quality Standard', width: 250, field: 'gradE_NAME'},      
        { headerName: 'Warehouse Manager', width: 180, field: 'warehousE_MANAGER'},
        { headerName: 'Market Price Of goods deposited on the date of deposit', width: 400, field: 'markeT_PRICE'},
        { headerName: 'Name Of Transformers to whom goods are transferred', width: 400, field: 'transformeR_NAME'},
        { headerName: 'Reference To the Document', width: 300, field: 'referencE_NO'},

      ]
    },
  ];



  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private EncrDecr: EncrDecrServiceService,
    private datef: CustomDateParserFormatter,


  ) {


    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }


    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

    this.icons = {
      filter: ' '
    }
    
    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      wrapText: true,
      autoHeight: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };
  }

  ngOnInit(): void {
    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }

    this.DepositorForm = this.formBuilder.group({
      Farmer: [null, Validators.required],
      WHtype:[null],
      Whname:[null],
      FromDate: [],
      ToDate: []
      

    });
   
    if(this.logUserrole=='101')
    {
      this.isdiv=true;
      this.istable=false;
      this.LoadWhDetails('WHTYPE');
      
    }
    else{
     
      this.isdiv=false;
      this.LoadFarmers('Farmers')

    }

    
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;    
    params.api.setRowData(this.StockDetails);

  }

  LoadWhDetails(val) {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_08 = this.qualty.WHtype.value;
    this.service.postData(req, "GetWHDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if(val=="WHTYPE")
        this.WHTypelist = data.Details;
        else
        this.whlist = data.Details;
      }

    },
      error => console.log(error));
    

  }
  LoadWarehouses(val)
  {
    this.qualty.Whname.setValue(null);
    this.qualty.FromDate.setValue(this.datef.format(null));
    this.qualty.ToDate.setValue(this.datef.format(null));
    this.istable=false;
     this.LoadWhDetails(val);
  }

  LoadDetails() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_02 = this.qualty.Farmer.value;
    req.INPUT_03 = this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    req.INPUT_08 = this.qualty.WHtype.value;
    req.INPUT_09 = this.datef.format(this.qualty.FromDate.value);
    req.INPUT_10 = this.datef.format(this.qualty.ToDate.value);

    this.service.postData(req, "GetDepositerLedger").subscribe(data => {
      this.showloader = false;
      this.istable=true;
      if (data.StatusCode == "100") {
        this.StockDetails = data.Details;
        this.gridApi.setRowData(this.StockDetails);
        this.pdfbtn=true;


      }
      else {
        //Swal.fire('warning', 'No Depositor Ledger Details Found', 'warning');
        this.gridApi.setRowData(data.Details);
        this.pdfbtn=false;

      }




    },
      error => console.log(error));
    

  }
  
  LoadFarmers(val) {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_02 = this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    this.service.postData(req, "GetFarmersAndCommodites").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.farmerlist = data.Details;
      }

    },
      error => console.log(error));
    

  }

  

  ChangeFarmer()
  {
    this.istable=false;
    this.StockDetails=[];
  }

  get qualty() { return this.DepositorForm.controls; }


  GetDetails() {

    this.isSubmit = true;
    this.StockDetails=[];
    
    if(this.logUserrole=="101")
    {
      if(!this.qualty.WHtype.value)
      {
        Swal.fire("warning","Select Warehouse Type","warning");
        return false;
      }
      
      if(!this.qualty.Whname.value)
      {
        Swal.fire("warning","Select Warehouse Name","warning");
        return false;
      }
      if(this.qualty.FromDate.value)  
      {
        if(!this.qualty.ToDate.value)
        {
          Swal.fire("warning","Please Select To Date","warning");
        return false;
        }
        if (this.datef.GreaterDate(this.qualty.FromDate.value, this.qualty.ToDate.value)) {
          Swal.fire('warning', "To Date Shoud be greater than From Date", 'warning');
          return false;
        }
      } 
     

      if (this.DepositorForm.invalid) {
        return false;
      }
      this.LoadDetails();

    }
    else
    {
      if(this.qualty.FromDate.value)  
      {
        if(!this.qualty.ToDate.value)
        {
          Swal.fire("warning","Please Select To Date","warning");
        return false;
        }
        if (this.datef.GreaterDate(this.qualty.FromDate.value, this.qualty.ToDate.value)) {
          Swal.fire('warning', "To Date Shoud be greater than From Date", 'warning');
          return false;
        }
      } 
     
      if (this.DepositorForm.invalid) {
        return false;
      }

      this.LoadDetails();

    }
  }


  Downloadpdf()
  {

    this.showloader = true;
    const req = new InputRequest();
    req.DIRECTION_ID="13";
    req.TYPEID="DEPOSITOR_LEDGER";
    req.INPUT_02 = this.qualty.Farmer.value;
    req.INPUT_03 = this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    req.INPUT_08 = this.qualty.WHtype.value;
    req.INPUT_09 = this.datef.format(this.qualty.FromDate.value);
    req.INPUT_10 = this.datef.format(this.qualty.ToDate.value);


    this.service.postData(req, "APSWCMapsServiceConsume").subscribe(data => {
      this.showloader = false;
      if (data.Status == "Success") {
        var b = this.service.s_sd(data.CertIssuerData.Base64pdf, 'application/pdf');
        var l = document.createElement('a');
        l.href = window.URL.createObjectURL(b);
        l.download = "Depositor_Ledger"+".pdf";
        document.body.appendChild(l);
        l.click();
        document.body.removeChild(l);

        //console.log(data.Details);Base64pdf
        //this.fillPDFData(data.Details[0])
      }
      else
        Swal.fire('warning', data.Reason, 'warning');

    },
      error => console.log(error));
      

  }


}

