import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepositorLedgerComponent } from './depositor-ledger.component';

describe('DepositorLedgerComponent', () => {
  let component: DepositorLedgerComponent;
  let fixture: ComponentFixture<DepositorLedgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositorLedgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositorLedgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
