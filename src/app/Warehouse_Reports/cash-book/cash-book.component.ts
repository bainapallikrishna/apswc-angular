import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';

@Component({
  selector: 'app-cash-book',
  templateUrl: './cash-book.component.html',
  styleUrls: ['./cash-book.component.css']
})
export class CashBookComponent implements OnInit {

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  public components;  
  public defaultColDef;
  public icons;
  StockDetails:any[];
  showloader: boolean = true;

  WHTypelist:any[];
  whlist:any[];
  NgbDateStruct:any;
  isdiv: boolean = false;  
  DepositorForm: FormGroup;
  isSubmit: boolean = false;
  istable: boolean = false;
  pdfbtn:boolean=false;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  columnDefs = [
    { headerName: '#', width: 60, cellRenderer: 'rowIdRenderer',floatingFilter:false },
    { headerName: 'Date', width: 150, field: 'wH_DATE' },
    { headerName: 'Rt No', width: 200, field: 'wH_RT_NO' },   
    { headerName: 'Particulars', width: 300, field: 'wH_PARTICULARS'},
    { headerName: 'L.F', width: 150, field: 'wH_LF' },   
    { headerName: 'Cash Account(Rs)', width: 200, field: 'casH_ACCOUNT' },      
    { headerName: 'Warehouse Charges and Other Receipt(Rs)', width: 300, field: 'wH_CHARGES' },
    { headerName: 'Total(Rs)', width: 200, field: 'wH_TOTAL' },
    { headerName: 'Date', width: 150, field: 'rE_date' },
    { headerName: 'Vr No', width: 200, field: 'rE_VR_NO' },   
    { headerName: 'Particulars', width: 300, field: 'rE_PARTICULARS'},
    { headerName: 'L.F', width: 150, field: 'rE_LF' },   
    { headerName: 'Cash Account(Rs)', width: 200, field: 'rE_CASH_ACCOUNT' },      
    { headerName: 'Remittances to Head Offices(Rs)', width: 300, field: 'remittances' },
    { headerName: 'Total(Rs)', width: 200, field: 'rE_TOTAL' },  
    
  ];



  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    
    private EncrDecr: EncrDecrServiceService,
    private datef: CustomDateParserFormatter,
    

  ) { 

    
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }


    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
    this.icons = {
      filter: ' '
    }
    
    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };
  }

  ngOnInit(): void {
    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }

    this.DepositorForm = this.formBuilder.group({      
      WHtype:[null],
      Whname:[null],
      FromDate: [],
      ToDate: []

    });

    
    if(this.logUserrole=='101')
    {
      this.isdiv=true;
      this.istable=false;
      this.LoadWhDetails('WHTYPE');
      
    }
    else{
      this.istable=true;
      this.isdiv=false;
      this.LoadDetails();

    }
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //this.LoadDetails();
    params.api.setRowData(this.StockDetails);
    
  }

  LoadWhDetails(val) {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_08 = this.qualty.WHtype.value;
    this.service.postData(req, "GetWHDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if(val=="WHTYPE")
        this.WHTypelist = data.Details;
        else
        this.whlist = data.Details;
      }

    },
      error => console.log(error));
    

  }
  LoadWarehouses(val)
  {
    this.qualty.Whname.setValue(null);
    this.qualty.FromDate.setValue(this.datef.format(null));
    this.qualty.ToDate.setValue(this.datef.format(null));
    this.istable=false;
     this.LoadWhDetails(val);
  }

  LoadDetails() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_02= this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;

    req.INPUT_08 = this.qualty.WHtype.value;
    req.INPUT_09 = this.datef.format(this.qualty.FromDate.value);
    req.INPUT_10 = this.datef.format(this.qualty.ToDate.value);

    this.service.postData(req, "GetCashBook").subscribe(data => {
      this.showloader = false;
      this.istable=true;
      if (data.StatusCode == "100") {
        this.StockDetails = data.Details;
        this.gridApi.setRowData(this.StockDetails);
        //console.log(this.StockDetails);
        this.pdfbtn=true;
      }
      else
      {
        Swal.fire('warning', data.StatusMessage, 'warning');
        this.gridApi.setRowData(data.Details);

      }

 


    },
      error => console.log(error));
     

  }


  get qualty() { return this.DepositorForm.controls; }


  GetDetails() {

    this.isSubmit = true;
    this.StockDetails=[];
    

    if(this.logUserrole=="101")
    {
      if(!this.qualty.WHtype.value)
      {
        Swal.fire("warning","Select Warehouse Type","warning");
        return false;
      }
      
      if(!this.qualty.Whname.value)
      {
        Swal.fire("warning","Select Warehouse Name","warning");
        return false;
      }
      if(this.qualty.FromDate.value)  
      {
        if(!this.qualty.ToDate.value)
        {
          Swal.fire("warning","Please Select To Date","warning");
        return false;
        }
        if (this.datef.GreaterDate(this.qualty.FromDate.value, this.qualty.ToDate.value)) {
          Swal.fire('warning', "To Date Shoud be greater than From Date", 'warning');
          return false;
        }
      } 
     

      if (this.DepositorForm.invalid) {
        return false;
      }
      this.LoadDetails();

    }
    else
    {

      if(this.qualty.FromDate.value)  
      {
        if(!this.qualty.ToDate.value)
        {
          Swal.fire("warning","Please Select To Date","warning");
        return false;
        }
        if (this.datef.GreaterDate(this.qualty.FromDate.value, this.qualty.ToDate.value)) {
          Swal.fire('warning', "To Date Shoud be greater than From Date", 'warning');
          return false;
        }
      } 
     

      if (this.DepositorForm.invalid) {
        return false;
      }

      this.LoadDetails();

    }



    
  }



  
  Downloadpdf()
  {

    this.showloader = true;
    const req = new InputRequest();
    req.DIRECTION_ID="13";
    req.TYPEID="CASH_BOOK";
    req.INPUT_02 = this.qualty.Farmer.value;
    req.INPUT_03 = this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    req.INPUT_08 = this.qualty.WHtype.value;
    req.INPUT_09 = this.datef.format(this.qualty.FromDate.value);
    req.INPUT_10 = this.datef.format(this.qualty.ToDate.value);


    this.service.postData(req, "APSWCMapsServiceConsume").subscribe(data => {
      this.showloader = false;
      if (data.Status == "Success") {
        var b = this.service.s_sd(data.CertIssuerData.Base64pdf, 'application/pdf');
        var l = document.createElement('a');
        l.href = window.URL.createObjectURL(b);
        l.download = "CashBook"+".pdf";
        document.body.appendChild(l);
        l.click();
        document.body.removeChild(l);

        //console.log(data.Details);Base64pdf
        //this.fillPDFData(data.Details[0])
      }
      else
        Swal.fire('warning', data.Reason, 'warning');

    },
      error => console.log(error));
      

  }

}
