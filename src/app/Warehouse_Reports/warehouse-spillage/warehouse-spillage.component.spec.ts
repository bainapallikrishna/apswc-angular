import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseSpillageComponent } from './warehouse-spillage.component';

describe('WarehouseSpillageComponent', () => {
  let component: WarehouseSpillageComponent;
  let fixture: ComponentFixture<WarehouseSpillageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseSpillageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseSpillageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
