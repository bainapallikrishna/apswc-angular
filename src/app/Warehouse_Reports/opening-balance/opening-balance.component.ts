import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';

@Component({
  selector: 'app-opening-balance',
  templateUrl: './opening-balance.component.html',
  styleUrls: ['./opening-balance.component.css']
})
export class OpeningBalanceComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  


  NgbDateStruct:any;
  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  public components;
  public defaultColDef;
  public icons;
  StockDetails: any[];
  showloader: boolean = true;
  DepositorForm: FormGroup;
  isSubmit: boolean = false;
  istable: boolean = false;
  isdiv: boolean = false;

  farmerlist: any[];
  cmdtylist: any[];
  mothlist: any[];
  yearlist: any[];
  WHTypelist:any[];
  whlist:any[];
  pdfbtn:boolean=false;


  @ViewChild('agGrid') agGrid: AgGridAngular;

  columnDefs = [
    { headerName: '#', width: 60, cellRenderer: 'rowIdRenderer',floatingFilter:false},
    { headerName: 'Commodity', width: 100, field: 'commodity'},
    { headerName: 'Month', width: 60, field: 'montH_ON'},
    { headerName: 'Year', width: 60, field: 'yeaR_ON'},
    { headerName: 'Date', width: 100, field: 'transactioN_DATE'},
    {
      headerName: 'Opening Balance', width: 300,
      children: [
        { headerName: 'Bags', field: 'openinG_BALANCE_BAGS', width: 100},
        { headerName: 'MTS Kgs', field: 'openinG_BALANCE_WEIGHT', width: 120}
      ]
    },
    {
      headerName: 'Receipts', width: 300,
      children: [
        { headerName: 'Bags', field: 'stacK_IN_BAGS', width: 100},
        { headerName: 'MTS Kgs', field: 'stacK_IN_WEIGHT', width: 120}
      ]
    },
    {
      headerName: 'Issues', width: 300,
      children: [
        { headerName: 'Bags', field: 'stacK_OUT_BAGS', width: 100},
        { headerName: 'MTS Kgs', field: 'stacK_OUT_WEIGHT', width: 120}
      ]
    },

    {
      headerName: 'Closing Balance', width: 300,
      children: [
        { headerName: 'Bags', field: 'closinG_BALANCE_BAGS', width: 100},
        { headerName: 'MTS Kgs', field: 'closinG_BALANCE_WEIGHT', width: 120}

      ]
    }

  ];



  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private EncrDecr: EncrDecrServiceService,
    private datef: CustomDateParserFormatter,


  ) {


    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }


    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

    this.icons = {
      filter: ' '
    }
    
    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText: true,
      autoHeight: true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };
  }

  ngOnInit(): void {

    
    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }

    this.DepositorForm = this.formBuilder.group({      
      WHtype:[null],
      Whname:[null],
      Farmer: [null, Validators.required],
      cmdity: [null],
      month: [null],
      year: [null],
      FromDate: [],
      ToDate: []

    });

    
    if(this.logUserrole=='101')
    {
      this.isdiv=true;
      this.LoadWhDetails('WHTYPE');
      
    }
    else{
    this.isdiv=false;

    }
    this.LoadFarmers('Farmers')
    this.LoadMonths();
    this.LoadYears();
    

  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //this.LoadDetails();
    params.api.setRowData(this.StockDetails);

  }

  LoadDetails() {
    this.istable = true;
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_02 = this.logUserrole=="101"?this.qualty.Whname.value: this.workLocationCode;
    req.INPUT_03 = this.qualty.month.value;
    req.INPUT_04 = this.qualty.year.value;
    req.INPUT_05 = this.qualty.Farmer.value;
    req.INPUT_06 = this.qualty.cmdity.value;
    req.INPUT_08 = this.qualty.WHtype.value;
    req.INPUT_09 = this.datef.format(this.qualty.FromDate.value);
    req.INPUT_10 = this.datef.format(this.qualty.ToDate.value);

    this.service.postData(req, "GetDepostCmdtys").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
       
        this.StockDetails = data.Details;
        this.gridApi.setRowData(this.StockDetails);
        this.pdfbtn=true;


      }
      else {
        //Swal.fire('warning', data.StatusMessage, 'warning');
        this.gridApi.setRowData(data.Details);
        this.pdfbtn=false;

      }




    },
      error => console.log(error));
    

  }


  LoadWhDetails(val) {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_08 = this.qualty.WHtype.value;
    this.service.postData(req, "GetWHDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if(val=="WHTYPE")
        this.WHTypelist = data.Details;
        else
        this.whlist = data.Details;
      }

    },
      error => console.log(error));
    

  }
  LoadWarehouses(val)
  {
    this.qualty.Whname.setValue(null);
    this.qualty.cmdity.setValue(null);
    this.qualty.month.setValue(null);
    this.qualty.year.setValue(null);
    this.qualty.FromDate.setValue(this.datef.format(null));
    this.qualty.ToDate.setValue(this.datef.format(null));
    this.istable=false;
     this.LoadWhDetails(val);
  }


  LoadFarmers(val) {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_02 = this.logUserrole=="101"?this.qualty.Whname.value: this.workLocationCode;
    this.service.postData(req, "GetFarmersAndCommodites").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.farmerlist = data.Details;
      }

    },
      error => console.log(error));
    

  }

  LoadCommodites(val) {

    this.qualty.cmdity.setValue(null);
    this.qualty.month.setValue(null);
    this.qualty.year.setValue(null);
    this.qualty.FromDate.setValue(this.datef.format(null));
    this.qualty.ToDate.setValue(this.datef.format(null));
    this.istable=false;
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_02 = this.logUserrole=="101"?this.qualty.Whname.value:this.workLocationCode;
    req.INPUT_03 = this.qualty.Farmer.value;
    this.service.postData(req, "GetFarmersAndCommodites").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.cmdtylist = data.Details;

      }

    },
      error => console.log(error));
    

  }

  changeCommodites() {
    this.qualty.month.setValue(null);
    this.qualty.year.setValue(null);
    this.qualty.FromDate.setValue(this.datef.format(null));
    this.qualty.ToDate.setValue(this.datef.format(null));
    this.istable = false;
  }

  LoadMonths() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = 'Months';
    req.INPUT_02 = this.workLocationCode;

    this.service.postData(req, "GetMonthsAndYears").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.mothlist = data.Details;

      }

    },
      error => console.log(error));
    
  }

  LoadYears() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = 'Years';
    req.INPUT_02 = this.workLocationCode;

    this.service.postData(req, "GetMonthsAndYears").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.yearlist = data.Details;

      }

    },
      error => console.log(error));
   

  }


  get qualty() { return this.DepositorForm.controls; }


  GetDepositerCmdtyDetails() {

    this.isSubmit = true;
    this.StockDetails=[];
    

    if(this.logUserrole=="101")
    {
      if(!this.qualty.WHtype.value)
      {
        Swal.fire("warning","Select Warehouse Type","warning");
        return false;
      }
      
      if(!this.qualty.Whname.value)
      {
        Swal.fire("warning","Select Warehouse Name","warning");
        return false;
      }
      if(this.qualty.FromDate.value)  
      {
        if(!this.qualty.ToDate.value)
        {
          Swal.fire("warning","Please Select To Date","warning");
        return false;
        }
        if (this.datef.GreaterDate(this.qualty.FromDate.value, this.qualty.ToDate.value)) {
          Swal.fire('warning', "To Date Shoud be greater than From Date", 'warning');
          return false;
        }
      } 
     

      if (this.DepositorForm.invalid) {
        return false;
      }
      this.LoadDetails();

    }
    else
    {

      if(this.qualty.FromDate.value)  
      {
        if(!this.qualty.ToDate.value)
        {
          Swal.fire("warning","Please Select To Date","warning");
        return false;
        }
        if (this.datef.GreaterDate(this.qualty.FromDate.value, this.qualty.ToDate.value)) {
          Swal.fire('warning', "To Date Shoud be greater than From Date", 'warning');
          return false;
        }
      } 
     

      if (this.DepositorForm.invalid) {
        return false;
      }

      this.LoadDetails();

    }



    
  }

  Downloadpdf()
  {

    this.showloader = true;
    const req = new InputRequest();
    req.DIRECTION_ID="13";
    req.TYPEID="DEPOSITER_COMMODITY";
    req.INPUT_02 = this.logUserrole=="101"?this.qualty.Whname.value: this.workLocationCode;
    req.INPUT_03 = this.qualty.month.value;
    req.INPUT_04 = this.qualty.year.value;
    req.INPUT_05 = this.qualty.Farmer.value;
    req.INPUT_06 = this.qualty.cmdity.value;
    req.INPUT_08 = this.qualty.WHtype.value;
    req.INPUT_09 = this.datef.format(this.qualty.FromDate.value);
    req.INPUT_10 = this.datef.format(this.qualty.ToDate.value);


    this.service.postData(req, "APSWCMapsServiceConsume").subscribe(data => {
      this.showloader = false;
      if (data.Status == "Success") {
        var b = this.service.s_sd(data.CertIssuerData.Base64pdf, 'application/pdf');
        var l = document.createElement('a');
        l.href = window.URL.createObjectURL(b);
        l.download = "Depositor_Commodity"+".pdf";
        document.body.appendChild(l);
        l.click();
        document.body.removeChild(l);

        //console.log(data.Details);Base64pdf
        //this.fillPDFData(data.Details[0])
      }
      else
        Swal.fire('warning', data.Reason, 'warning');

    },
      error => console.log(error));
      

  }

}
