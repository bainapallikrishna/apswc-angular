import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HandtdetailsComponent } from './handtdetails.component';

describe('HandtdetailsComponent', () => {
  let component: HandtdetailsComponent;
  let fixture: ComponentFixture<HandtdetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HandtdetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HandtdetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
