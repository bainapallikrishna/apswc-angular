import { HttpEventType } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { AgGridAngular } from 'ag-grid-angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { InputRequest } from 'src/app/Interfaces/employee';
import { AadharValidateService } from 'src/app/Services/aadhar-validate.service';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { HistorybuttonRendererComponent } from 'src/app/custome-directives/Historybutton-renderer.component';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-handtdetails',
  templateUrl: './handtdetails.component.html',
  styleUrls: ['./handtdetails.component.css']
})
export class HandtdetailsComponent implements OnInit {

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");

  showloader: boolean = true;
  modeltitle: string = "Add";
  isAdmin: boolean = false;
  isEdit: boolean = false;
  frameworkComponents: any;
  public components;
  defaultColDef: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];
  HTForm: FormGroup;
  isSubmit: boolean = false;
  states: any = [];
  districts: any = [];
  urlist: any = [];
  nmandals: any = [];
  nvillages: any = [];
  Pincodedata:any[];
  public Emp_mob_isvalid: boolean = true;
  oursources: any = [];
  wh_history: any = [];

  progress: number = 0;
  message: string = "";
  UploadDocType: string = "";
  UploadDocPath: string = "";

  PanPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  GstPattern = "^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$";
  IFSCPattern = "^([A-Z]{2}/\d{5}/\d{7})|[]$";
  ESICPattern = "^[0-9]{2}[0-9]{2}[0-9]{6}[0-9]{3}[0-9]{4}$";
  EPFOPattern = "^[A-Z]{5}[0-9]{10}";
  vcomname: string = "";
  voffname: string = "";
  vmobile: string = "";
  vemail: string = "";
  vstartdate: string = "";
  venddate: string = "";
  vpanno: string = "";
  vgstno: string = "";
  vstate: string = "";
  vdistrict: string = "";
  varea: string = "";
  vmandal: string = "";
  vvillage: string = "";
  vhno: string = "";
  vstreet: string = "";
  vlanmark: string = "";
  vpincode: string = "";
  vdocumentpath: string = "";
  vastatus: string = "";
  vremarks: string = "";
  vofficals: any = [];
  preview: string = "";
  seldocpath: any;
  seldoctype: string = "";
  seldoccat: string = "";

  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Contract Name', width: 200, field: 'contacT_NAME', cellRenderer: 'actionrender' },
    { headerName: 'Official Name', width: 200, field: 'officiaL_NAME' },
    { headerName: 'Mobile No', width: 100, field: 'mobileno' },
    { headerName: 'Address', width: 250, field: 'address' },
    { headerName: 'Agreement Start Date', width: 150, field: 'starT_DATE', },
    { headerName: 'Agreement End Date', width: 150, field: 'enD_DATE' },
    { headerName: 'Status', width: 100, field: 'recorD_STATUS' },
    { headerName: 'Action', width: 80, floatingFilter: false, cellRenderer: 'buttonRenderer' }
  ];

  constructor(private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private uidservice: AadharValidateService,
    private datef: CustomDateParserFormatter,
    private datecon: NgbDatepickerConfig,
    private sanitizer: DomSanitizer) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }

    if (this.logUserrole == "101" || this.logUserrole == "102")
      this.isAdmin = true;

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      wrapText: true,
      autoHeight: true,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.frameworkComponents = {
      buttonRenderer: HistorybuttonRendererComponent,
    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
      actionrender: function (params) {

        var eSpan3 = document.createElement('div');

        eSpan3.innerHTML = '<u style="color:blue" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + params.value + '</u>' +
          '<div class="dropdown-menu more-option-menu" aria-labelledby="dropdownMenuButton">' +
          '<span class="dropdown-item text-center EditClick"><i class="fa fa-pencil-square-o">Edit</i></span>' +
          '<span class="dropdown-item text-center ViewClick"><i class="fa fa-eye">View</i></span>' +
          '</div>';

        return eSpan3;

      },
    };
  }

  dataclicked(params) {
    if (params.colDef.cellRenderer == "actionrender") {
      if (params.event.path[0].classList.value == "dropdown-item text-center EditClick" || params.event.path[0].classList.value == "fa fa-pencil-square-o") {
        this.EditHT(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center ViewClick" || params.event.path[0].classList.value == "fa fa-eye") {
        this.ViewHT(params);
      }
    }
    if (params.colDef.cellRenderer == "buttonRenderer") {
      this.HistoryHT(params);
    }
  }


  @ViewChild('agGrid') public agGrid: AgGridAngular;
  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('historyModal') public historyModal: ModalDirective;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('previewModal') public previewModal: ModalDirective;

  ngOnInit(): void {
    let now: Date = new Date();

    this.HTForm = this.formBuilder.group({
      HTCode: [''],
      HTComName: ['', Validators.required],
      OffiName: ['', Validators.required],
      CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      CEmail: [''],
      StartDate: ['', Validators.required],
      EndDate: ['', Validators.required],
      PANNo: ['', [Validators.required, Validators.minLength(10), Validators.pattern(this.PanPattern)]],
      GSTNo: ['', [Validators.required, Validators.minLength(10), Validators.pattern(this.GstPattern)]],
      EPFO: ['', [Validators.required, Validators.minLength(15), Validators.pattern(this.EPFOPattern)]],
      ESICNo: ['', [Validators.required, Validators.minLength(17), Validators.pattern(this.ESICPattern)]],
      NState: [null, Validators.required],
      NDistrict: [null, Validators.required],
      NArea: [null, Validators.required],
      NMandal: [null, Validators.required],
      NVillage: [null, Validators.required],
      NHNO: ['', Validators.required],
      NStrName: ['', Validators.required],
      NLandmark: [''],
      NPincode: ['', Validators.required],
      OutDoc: [''],
      IsOutActive: ['1'],
      OutRemarks: [''],
      oldOutDoc: [''],
    });
    this.LoadStates();
    this.LoadDistricts();
    this.LoadurFlags();
    this.LoadStates();
    this.LoadData();
  }

  get reg() { return this.HTForm.controls; }
  //get c() { return this.reg.officials as FormArray; }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    params.api.setRowData(this.oursources);
    //this.gridApi.sizeColumnsToFit();

  }

  LoadData() {
    const req = new InputRequest();
    req.INPUT_01 = null;
    this.service.postData(req, "GetHTDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.oursources = data.Details;
        console.log(this.oursources);
        this.gridApi.setRowData(this.oursources);
      }
    },
      error => console.log(error));

  }

  mob_uid_validate(mobilephone) {
    if (mobilephone.length === 10)
      this.Emp_mob_isvalid = this.uidservice.validatemob(mobilephone);
  }

  AddHT() {
    this.modeltitle = "Add";
    this.resetallData();
    this.isSubmit = false;
    this.isEdit = false;
    this.progress = 0;
    this.message = "";
    this.editModal.show();
  }

  HistoryHT(row) {

    this.showloader = true;

    const req = new InputRequest();
    if (row == 'all') {
      this.isEdit = true;
      req.INPUT_01 = null;
    }
    else {
      this.isEdit = false;
      req.INPUT_01 = row.data.hT_REGISTRATION_ID;
    }

    //req.INPUT_02 = "OUTSOURCING";

    this.service.postData(req, "GetHTHistoryDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.wh_history = data.Details;

        for (let i = 0; i < this.wh_history.length; i++) {
          let colname = this.wh_history[i].columN_NAME;
          let oldvalue = this.wh_history[i].olD_VALUE;
          let newvalue = this.wh_history[i].neW_VALUE;
          if ((colname == "AGREMENT_DOCUMENT") && oldvalue) {
            this.decryptHistoryFile(oldvalue, i, 'oldvalue');
          }
          if ((colname == "AGREMENT_DOCUMENT") && newvalue) {
            this.decryptHistoryFile(newvalue, i, 'newvalue');
          }
        }

        this.historyModal.show();
      }

    },
      error => console.log(error));


  }

  EditHT(row) {

    this.isEdit = true;
    this.isSubmit = false;
    this.modeltitle = "Edit";
    this.resetallData();
    this.GetContractDetails(row, 'edit');
  }

  ViewHT(row) {
    this.GetContractDetails(row, 'view');
  }

  GetContractDetails(row, ptype: string) {
    this.showloader = true;
    const rtype: string = ptype;
    const req = new InputRequest();
    req.INPUT_01 = row.data.hT_REGISTRATION_ID;

    this.service.postData(req, "GetHTDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (rtype == 'edit')
          this.FillEditContractDetails(data.Details[0]);
        else if (rtype == 'view')
          this.FillViewContractDetails(data.Details[0]);
      }

    },
      error => console.log(error));
  }


  resetallData() {
    this.HTForm.reset();
    this.progress = 0;
    this.message = "";
    this.UploadDocType = "";
    this.UploadDocPath = "";

  }

  FillEditContractDetails(data) {
    this.reg.HTCode.setValue(data.hT_REGISTRATION_ID)
    this.reg.HTComName.setValue(data.contacT_NAME);
    this.reg.OffiName.setValue(data.officiaL_NAME);
    this.reg.CMobile.setValue(data.mobileno);
    this.reg.CEmail.setValue(data.emaiL_ADDRESS);
    this.reg.StartDate.setValue(this.datef.parse((data.starT_DATE ? data.starT_DATE : "").replace("T00:00:00", "")))
    this.reg.EndDate.setValue(this.datef.parse((data.enD_DATE ? data.enD_DATE : "").replace("T00:00:00", "")))
    this.reg.PANNo.setValue(data.paN_NO);
    this.reg.GSTNo.setValue(data.gstiN_NO);
    this.reg.NState.setValue(data.statE_CODE);
    this.reg.NDistrict.setValue(data.districT_CODE)
    this.reg.NArea.setValue(data.ruraL_URBAN)

    this.DistrictChange();
    this.reg.NMandal.setValue(data.mmC_CODE?.toString());

    this.MandalChange();
    this.reg.NVillage.setValue(data.warD_VILLAGE_CODE?.toString());

    this.reg.NHNO.setValue(data.dooR_NO);
    this.reg.NStrName.setValue(data.streaT_NAME);
    this.reg.NLandmark.setValue(data.lanD_MARK);
    this.reg.NPincode.setValue(data.pincode);
    this.reg.oldOutDoc.setValue(data.agremenT_DOCUMENT);
    this.reg.IsOutActive.setValue(data.iS_ACTIVE?.toString())
    this.reg.OutRemarks.setValue(data.remarks)

    this.editModal.show();
  }

  FillViewContractDetails(data) {
    this.vcomname = data.contacT_NAME;
    this.voffname = data.officiaL_NAME;
    this.vmobile = data.mobileno;
    this.vemail = data.emaiL_ADDRESS;
    this.vstartdate = data.starT_DATE;
    this.venddate = data.enD_DATE;
    this.vpanno = data.paN_NO;
    this.vgstno = data.gstiN_NO;
    this.vstate = data.statE_NAME;
    this.vdistrict = data.districT_NAME;
    this.varea = data.ruraL_URBAN1;
    this.vmandal = data.mmC_NAME;
    this.vvillage = data.warD_VILLAGE_NAME;
    this.vhno = data.dooR_NO;
    this.vstreet = data.streaT_NAME;
    this.vlanmark = data.lanD_MARK;
    this.vpincode = data.pincode;
    this.decryptFile(data.agremenT_DOCUMENT, 'view');
    this.vdocumentpath = data.agremenT_DOCUMENT;
    this.vastatus = data.recorD_STATUS;
    this.vremarks = data.remarks;
    this.viewModal.show();
  }

  CloseEditModal() {
    this.LoadData();
    this.editModal.hide();
  }

  SubmitHTDetails() {
    this.isSubmit = true;

    // stop here if form is invalid
    if (this.HTForm.invalid) {
      return false;
    }

    if (!this.Emp_mob_isvalid) {
      return false;
    }

    if (!this.UploadDocPath) {
      Swal.fire('warning', "Please upload the document", 'warning');
      return false;
    }

    if (this.datef.GreaterDate(this.reg.StartDate.value, this.reg.EndDate.value)) {
      Swal.fire('warning', "End Date Should be greater than Start Date", 'warning');
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.reg.HTComName.value;
    req.INPUT_02 = this.reg.OffiName.value;
    req.INPUT_03 = this.reg.CMobile.value;
    req.INPUT_04 = this.reg.CEmail.value;
    req.INPUT_05 = this.reg.PANNo.value;
    req.INPUT_06 = this.datef.format(this.reg.StartDate.value);
    req.INPUT_07 = this.datef.format(this.reg.EndDate.value);
    req.INPUT_08 = this.reg.NHNO.value;
    req.INPUT_09 = this.reg.NState.value;
    req.INPUT_10 = this.reg.NDistrict.value;
    req.INPUT_11 = this.reg.NMandal.value;
    req.INPUT_12 = this.reg.NVillage.value;
    req.INPUT_13 = this.reg.NArea.value;
    req.INPUT_14 = this.reg.NStrName.value;
    req.INPUT_15 = this.reg.NLandmark.value;
    req.INPUT_16 = this.reg.NPincode.value;
    req.INPUT_17 = this.UploadDocPath;
    req.INPUT_18 = this.logUserrole;
    req.INPUT_19 = this.reg.GSTNo.value;
    req.INPUT_20 = this.reg.ESICNo.value;
    req.INPUT_21 = this.reg.EPFO.value;


    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveHTDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "H&T Contractor Details Saved Successfully !!!", 'success');
        console.log(data);
        this.editModal.hide();
        this.LoadData();

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }

  UpdateHTDetails() {
    this.isSubmit = true;
    // stop here if form is invalid
    if (this.HTForm.invalid) {
      return false;
    }

    if (!this.Emp_mob_isvalid) {
      return false;
    }

    if (this.reg.IsOutActive.value == "0" && !this.reg.OutRemarks.value) {
      Swal.fire('warning', "Remarks is Required ", 'warning');
      return false;
    }

    if (this.datef.GreaterDate(this.reg.StartDate.value, this.reg.EndDate.value)) {
      Swal.fire('warning', "End Date Should be greater than Start Date", 'warning');
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.reg.NHNO.value;
    req.INPUT_02 = this.reg.NState.value;
    req.INPUT_03 = this.reg.NDistrict.value;
    req.INPUT_04 = this.reg.NMandal.value;
    req.INPUT_05 = this.reg.NVillage.value;
    req.INPUT_06 = this.reg.NArea.value;
    req.INPUT_07 = this.reg.NStrName.value;
    req.INPUT_08 = this.reg.NLandmark.value;
    req.INPUT_09 = this.reg.NPincode.value;
    req.INPUT_10 = this.UploadDocPath ? this.UploadDocPath : this.reg.oldOutDoc.value;
    req.INPUT_11 = this.reg.CMobile.value;
    req.INPUT_12 = this.reg.CEmail.value;
    req.INPUT_13 = this.reg.HTCode.value;
    req.INPUT_14 = this.reg.OutRemarks.value;
    req.INPUT_15 = this.reg.IsOutActive.value;


    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "UpdateHTDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Outsourcing Agency Details Updated Successfully !!!", 'success');
        console.log(data);
        this.LoadData();
        this.editModal.hide();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  uploadFile(event) {

    this.message = "";
    this.UploadDocType = "";
    this.seldocpath = "";
    this.seldoctype = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF') {
        Swal.fire('info', 'Please upload pdf files only', 'info');
        return false;
      }

      this.seldoccat = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype = 'IMAGE';
      else
        this.seldoctype = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath = reader.result as string;
          this.preview = this.seldocpath;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();


      formData.append('file', fileToUpload, fileToUpload.name);

      formData.append('regid', "HTContracts");
      formData.append('category', this.reg.PANNo.value);
      formData.append('pagename', "HTContracts");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)

            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.';
            this.UploadDocType = this.seldoctype;

            this.uploadFinished(event.body);
          }

        });
    }
  }

  public createImgPath = (serverPath: string) => {

    return `${this.service.filebaseUrl.replace("api/FilesUpload", "")}${serverPath}`;

  }

  public uploadFinished = (event) => {
    this.UploadDocPath = event.fullPath;
  }

  LoadStates() {
    this.service.getData("GetStates").subscribe(data => {
      if (data.StatusCode == "100") {
        this.states = data.Details;
      }
    },
      error => console.log(error));
  }


  PincodeDetails() {
    

    if (this.reg.NPincode.value.length == "6") {
      
    this.districts=[];
    this.urlist=[];   
    this.nmandals=[];
    this.reg.NDistrict.setValue(null);
          this.reg.NArea.setValue(null);
          this.reg.NMandal.setValue(null);
      this.showloader = true;
      const req = new InputRequest();
      req.INPUT_01 =this.reg.NPincode.value;
      this.service.postData(req, "GetPincodeDetails").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.Pincodedata = data.Details;

          if (data.Details.length == 1) {
           
              this.districts =[{districT_CODE:data.Details[0].districT_CODE,districT_NAME_ENG:data.Details[0].districT_NAME}];
              this.urlist = data.Details;
              this.nmandals = data.Details;
              this.reg.NDistrict.setValue(data.Details[0].districT_CODE);
              this.reg.NArea.setValue(data.Details[0].id);
              this.reg.NMandal.setValue(data.Details[0].mmC_CODE);
              if (this.reg.NDistrict.value && this.reg.NMandal.value) {
                this.LoadVillages(this.reg.NDistrict.value, this.reg.NMandal.value)
              }
              

            }
            {
             
              this.LoadDistricts();
        this.LoadurFlags();
            }



          

        }
        {
          
          this.LoadDistricts();
    this.LoadurFlags();
        }

      },
        error => console.log(error));
    }


  }
  LoadDistricts() {
    this.service.getData("GetDistricts").subscribe(data => {
      if (data.StatusCode == "100") {
        this.districts = data.Details;
      }
    },
      error => console.log(error));
  }

  LoadurFlags() {
    this.service.getData("GetAreaTypes").subscribe(data => {
      if (data.StatusCode == "100") {
        this.urlist = data.Details;
      }
    },
      error => console.log(error));
  }

  DistrictChange() {
    this.nmandals = [];
    this.nvillages = [];
    this.reg.NMandal.setValue(null);
    this.reg.NVillage.setValue(null);
    if (this.reg.NDistrict.value && this.reg.NArea.value) {
      this.LoadMandals(this.reg.NDistrict.value, this.reg.NArea.value)
    }
  }

  LoadMandals(distval: string, areaval: string) {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = areaval;
    req.INPUT_02 = distval;

    this.service.postData(req, "GetMandlas").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.nmandals = data.Details;
      }

    },
      error => console.log(error));
  }

  MandalChange() {
    this.nvillages = [];
    this.reg.NVillage.setValue(null);
    if (this.reg.NDistrict.value && this.reg.NMandal.value) {
      this.LoadVillages(this.reg.NDistrict.value, this.reg.NMandal.value)
    }
  }

  LoadVillages(distval: string, manval: string) {
    this.showloader = true;
    let obj: any = { "INPUT_02": distval, "INPUT_03": manval }

    this.service.postData(obj, "GetVillages").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.nvillages = data.Details;
      }

    },
      error => console.log(error));
  }

  viewPDF(input): void {
    let pdfWindow = window.open('', '_blank')
    pdfWindow.document.write(`<iframe width='100%' height='100%' src='${encodeURI(input)}'></iframe>`)
  }

  decryptFile(filepath, vtype) {
    const docvtype = vtype;
    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (docvtype == 'view')
            this.vdocumentpath = data.docBase64;
        },
          error => {
            console.log(error);

          });
    }

  }

  decryptHistoryFile(filepath, i, ftype) {
    const coltype: string = ftype;
    const indx = i;
    if (coltype == 'oldvalue')
      this.wh_history[indx].olD_VALUE = "";
    else
      this.wh_history[indx].neW_VALUE = "";

    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (coltype == 'oldvalue')
            this.wh_history[indx].olD_VALUE = data.docBase64;
          else
            this.wh_history[indx].neW_VALUE = data.docBase64;
        })
    }
  };

  Docpriview(val) {

    // const blob = this.service.s_sd((val).split(',')[1], this.seldoccat);
    // const blobUrl = URL.createObjectURL(blob);
    // this.preview = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
    //this.preview = this.sanitizer.bypassSecurityTrustUrl(val);
    //this.preview = this.preview.changingThisBreaksApplicationSecurity;
    this.previewModal.show();

  }

}
