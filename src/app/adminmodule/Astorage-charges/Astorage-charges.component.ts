import { HttpClient } from '@angular/common/http';
import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import {storagecharges} from 'src/app/Interfaces/user';

@Component({
  selector: 'app-Astorage-charges',
  templateUrl: './Astorage-charges.component.html',
  styleUrls: ['./Astorage-charges.component.css']
})
export class AStorageChargesComponent implements OnInit {

  public Items:any;
  public Commoditys:any;
  public Insurances:any;
  public PackageType:any;
  public selecteditemname:string;
  public selectedcommodityname:string;
  public selectedpackagetypename:string;
  public Weight:string;
  public stdrate:string;
  public highrate1:string;
  public highrate2:string;
  public selectedinsurance:string;
  public storagecharge:storagecharges;
  constructor(private http:HttpClient) { }

  ngOnInit(): void {
this.Items=["FOOD GRAINS"];
this.Commoditys=["RAW RICE","NFCL Neem Urea","Kribhco Urea","IFFCO Urea","BOILED RICE"];
this.PackageType=["GUNNIES","Gunnies/HDPE"];
this.Insurances=["Including Insurance","Without Insurance"];
    
  }

  public onSubmit = () => {

    
    this.storagecharge = {
      ItemName:this.selecteditemname,
      CommodityName:this.selectedcommodityname,
      PackageName:this.selectedpackagetypename,
      Weight:this.Weight,
      STDRate:this.stdrate,
      HighRate1:this.highrate1,
      HighRate2:this.highrate2,
      Insurance:this.selectedinsurance  


    }

    
    this.http.post('http://localhost/APSWCAPP/api/APSWC/SectionRegistration', this.storagecharge)
    .subscribe(res => {
      console.log(res);
    });
  }


}
