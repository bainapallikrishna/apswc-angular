import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AStorageChargesComponent } from './Astorage-charges.component';

describe('StorageChargesComponent', () => {
  let component: AStorageChargesComponent;
  let fixture: ComponentFixture<AStorageChargesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AStorageChargesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AStorageChargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
