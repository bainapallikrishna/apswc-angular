import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MDlevelApprovalComponent } from './mdlevel-approval.component';

describe('MDlevelApprovalComponent', () => {
  let component: MDlevelApprovalComponent;
  let fixture: ComponentFixture<MDlevelApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MDlevelApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MDlevelApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
