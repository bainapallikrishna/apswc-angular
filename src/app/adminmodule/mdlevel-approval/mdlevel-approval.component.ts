import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { EmployeeSalaryInputRequest, InputRequest } from 'src/app/Interfaces/employee';
import { AgGridAngular } from 'ag-grid-angular';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { OFFICERAPPREJBUTTONRendererComponent } from 'src/app/custome-directives/OfficerApproveRejectbutton-renderer.component';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-mdlevel-approval',
  templateUrl: './mdlevel-approval.component.html',
  styleUrls: ['./mdlevel-approval.component.css']
})
export class MDlevelApprovalComponent implements OnInit {
  tokens: any;
  frameworkComponents: any;
  progressbar: boolean = false;
  message: string = "";
  employeecode = "";
  emimonths = 0;
  progress: number = 0;
  showloader: boolean = false;
  gridColumnApi: any = [];
  Loantypes: any;
  gridApi: any = [];
  btntype: any;
  cl: any;
  requestcode: any;
  empaddarray: any;
  public rowseledted: object;
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  public components;
  @ViewChild('LoanRejectModal') public LoanRejectModal: ModalDirective;
  @ViewChild('name') inputName: ElementRef;
  columnDefs = [
    { headerName: '#', maxWidth: 100, cellRenderer: 'rowIdRenderer' },
    { headerName: 'Employee Code', maxWidth: 250, field: 'emP_CODE', sortable: true, filter: true },
    { headerName: 'Employee Name', maxWidth: 250, field: 'emP_NAME', sortable: true, filter: true },
    { headerName: 'Employee Type', maxWidth: 250, field: 'emP_TYPE_NAME', sortable: true, filter: true },
    { headerName: 'Loan Type', maxWidth: 250, field: 'loaN_TYPE_NAME', sortable: true, filter: true },
    { headerName: 'Requested Amount', maxWidth: 250, field: 'reQ_AMOUNT', sortable: true, filter: true },
    { headerName: 'Months', maxWidth: 250, field: 'months', sortable: true, filter: true },

    { headerName: 'No of Installments', maxWidth: 250, field: 'installmenT_TYPE', sortable: true, filter: true },
    { headerName: 'Amount For Installments', width: 300, field: 'installmenT_AMOUNT', sortable: true, filter: true },

    { headerName: 'Basic Salary', maxWidth: 250, field: 'basic', sortable: true, filter: true },
    { headerName: 'Net Pay', maxWidth: 250, field: 'neT_PAY', sortable: true, filter: true },

    {
      headerName: 'Action', cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        editClick: this.EditEmployee.bind(this),

      },
    }

  ];

  EditEmployee(row) {

    this.btntype = row.bttype;
    const data = row.rowData;
    this.employeecode = data.emP_CODE;
    this.emimonths = data.months;
    this.requestcode = data.reQ_CODE;

    if (this.btntype == "1") {


      const req = new EmployeeSalaryInputRequest();
      this.showloader = true;

      req.INPUT_01 = this.employeecode;
      req.INPUT_02 = this.requestcode;
      req.INPUT_03 = "3";
      req.USER_NAME = this.logUsercode;
      req.CALL_SOURCE = "WEB";
      this.service.postData(req, "SaveMdlevelapprovalDetails").subscribe(data => {
        this.empaddarray = data;
        // this.empaddarray=data.result;
        if (this.empaddarray.StatusCode == "100") {

          this.showloader = false;

          this.LoanRejectModal.hide();
          Swal.fire("success", "Approved Successfully", "success");
          // this.LoadofficerapprovalData();
          this.reloadCurrentRoute();
        }
        else {
          this.showloader = false;
          Swal.fire("info", this.empaddarray.StatusMessage, "info");
        }
      },

        error => console.log(error))
    }
    else {

      this.LoanRejectModal.show();

    }

  }
  constructor(private formBuilder: FormBuilder, private service: CommonServices, private router: Router, private datef: CustomDateParserFormatter) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {

      this.router.navigate(['/Login']);
      return;
    }


    this.components = {

      rowIdRenderer: function (params) {

        return '' + (params.rowIndex + 1);
      }
    }
    this.frameworkComponents = {

      buttonRenderer: OFFICERAPPREJBUTTONRendererComponent,

    }

  }

  ngOnInit(): void {
  }
  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.LoadofficerapprovalData();
  }
  LoadofficerapprovalData() {
    this.showloader = true;
    const req = new EmployeeSalaryInputRequest();
    // let obj: any = { "INPUT_01": this.logUsercode };

    this.service.postData(req, "GetMDApprovalData").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));

  }
  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
  }


  Rejectsubmit() {


    const req = new EmployeeSalaryInputRequest();
    this.showloader = true;

    if (this.officerRejectform.controls.mdrejremarks.value == "" || this.officerRejectform.controls.mdrejremarks.value == null || this.officerRejectform.controls.mdrejremarks.value == undefined) {
      Swal.fire("info", "Please Enter Remarks For Rejection", "info");
      this.showloader = false;
      return false;
    }
    req.INPUT_01 = this.employeecode;
    req.INPUT_02 = this.requestcode;
    req.INPUT_03 = "4";
    req.INPUT_04 = this.officerRejectform.controls.mdrejremarks.value;
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";
    this.service.postData(req, "SaveMdlevelapprovalDetails").subscribe(data => {
      this.empaddarray = data;
      // this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        this.showloader = false;

        this.LoanRejectModal.hide();
        Swal.fire("success", "Rejected Successfully", "success");
        // this.LoadofficerapprovalData();
        this.reloadCurrentRoute();
      }
      else {
        this.showloader = false;
        Swal.fire("info", this.empaddarray.StatusMessage, "info");
      }
    },

      error => console.log(error));

  }

  CloseEditModal() {
    this.reloadCurrentRoute();
    this.LoanRejectModal.hide();
  }

  officerRejectform = this.formBuilder.group({
    mdrejremarks: ['']
  })
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
}
