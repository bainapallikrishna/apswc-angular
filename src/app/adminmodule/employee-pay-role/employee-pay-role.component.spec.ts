import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePayRoleComponent } from './employee-pay-role.component';

describe('EmployeePayRoleComponent', () => {
  let component: EmployeePayRoleComponent;
  let fixture: ComponentFixture<EmployeePayRoleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePayRoleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePayRoleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
