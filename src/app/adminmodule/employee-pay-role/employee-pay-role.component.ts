
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import { EmployeeSalaryInputRequest } from 'src/app/Interfaces/employee';
import { MustMatch } from 'src/app/custome-directives/must-match.validator';
import Swal from 'sweetalert2';
import { AadharValidateService } from 'src/app/Services/aadhar-validate.service';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { AgGridAngular } from 'ag-grid-angular';
import { EMPSALBUTTONRendererComponent } from 'src/app/custome-directives/Empsalbutton-renderer.component';
import { HttpEventType } from '@angular/common/http';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-employee-pay-role',
  templateUrl: './employee-pay-role.component.html',
  styleUrls: ['./employee-pay-role.component.css']
})
export class EmployeePayRoleComponent implements OnInit {
  Datatype = "";
  Yeararry = [];
  empPayrol: [];
  empaddarray: any;
  istable: boolean = false;
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  Empcodedata: any;
  months: [];
  table: boolean = false;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  constructor(private formBuilder: FormBuilder, private service: CommonServices, private router: Router, private datef: CustomDateParserFormatter) {


    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.frameworkComponents = {

      buttonRenderer: EMPSALBUTTONRendererComponent,

    }

    this.components = {

      rowIdRenderer: function (params) {

        return '' + (params.rowIndex + 1);
      }
    }

    this.getemploylist();

    this.emptype = "1";

    this.LoadEmployeetypedata();

    this.istable = true;
  }
  ngOnInit(): void {
  }
  frameworkComponents: any;
  NgbDateStruct: any;
  Taxtype: any;
  progressbar: boolean = false;
  message: string = "";
  UploadDocType: string = "";
  UploadDocPath: string = "";
  progress: number = 0;
  submitted: boolean = false;
  showloader: boolean = false;
  gridColumnApi: any = [];
  tokens: any = [];
  gridApi: any = [];
  emptype = "";
  public rowseledted: object;
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  public components;

  emptypes: any;
  employeecode: any;
  @ViewChild('TaxPaymentModal') TaxPaymentModal: ModalDirective;
  @ViewChild('RegularModal') public RegularModal: ModalDirective;
  @ViewChild('DeputationModal') public DeputationModal: ModalDirective;
  @ViewChild('ContractModal') public ContractModal: ModalDirective;
  @ViewChild('closeModal') public closeModal: ModalDirective;
  @ViewChild('PayroleModal') public PayroleModal: ModalDirective;

  columnDefs = [
    { headerName: '#', cellRenderer: 'rowIdRenderer' },

    { headerName: 'Employee Code', field: 'emP_CODE', sortable: true, filter: true },
    { headerName: 'Employee Name', field: 'emP_NAME', sortable: true, filter: true },
    { headerName: 'Employee Type', field: 'emP_TYPE_NAME', sortable: true, filter: true },
    { headerName: 'Designation', field: 'designation', sortable: true, filter: true },

    { headerName: 'Warehouse Name', field: 'wH_NAME', sortable: true, filter: true },
    { headerName: 'Region', field: 'region', sortable: true, filter: true },
    {
      headerName: 'Action', cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        editClick: this.EditEmployee.bind(this),

      },
    }

  ];

  getemploylist() {
    this.showloader = true;

    const req = new InputRequest();

    this.service.postData(req, "Getemployeetypes").subscribe(data => {
      if (data.StatusCode == "100") {
        this.emptypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;

        Swal.fire("info", data.StatusMessage, "info");
      }

    },
      error => console.log(error));

  }



  GetempDetails() {
    if (this.emptype == "" || this.emptype == null || this.emptype == undefined) {
      Swal.fire("info", "Please Select Employee type", "info");
      this.istable = false;
      return false;
    }
    else {
      this.LoadEmployeetypedata();
    }
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

  }

  Emptypechange1() {


  }

  LoadEmployeetypedata() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.emptype }

    this.service.postData(obj, "GetEmptypeData").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
        this.istable = true;
      }
      else {
        this.istable = false;
        Swal.fire("info", data.StatusMessage, "info");
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));
  }
  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
  }



  EditEmployee(row) {


    const data = row.rowData;
    this.employeecode = data.emP_CODE;

    this.showloader = true;

    let obj: any = { "INPUT_01": data.emP_CODE }

    this.service.postData(obj, "CheckEmpCodeFound").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.Datatype = "Update";
        this.Empcodedata = data.Details;


        this.employeeRegularform.reset();
        this.employeepayroleform.reset();
        this.employeeOutsourcingform.reset();
        this.employeeContractform.reset();
        this.employeecode = this.Empcodedata[0].emP_CODE;
        if (this.emptype == "1") {


          this.employeeRegularform.controls.Basic.setValue(this.Empcodedata[0].basic);
          this.employeeRegularform.controls.BasicDA.setValue(this.Empcodedata[0].da);
          this.employeeRegularform.controls.BasicHRA.setValue(this.Empcodedata[0].hra);
          this.employeeRegularform.controls.BasicCCA.setValue(this.Empcodedata[0].cca);
          this.employeeRegularform.controls.BasicCA.setValue(this.Empcodedata[0].ca);
          this.employeeRegularform.controls.BasicMALL.setValue(this.Empcodedata[0].m_ALL);
          this.employeeRegularform.controls.BasicWALL.setValue(this.Empcodedata[0].w_ALL);
          this.employeeRegularform.controls.BasicSPPAY.setValue(this.Empcodedata[0].sP_PAY);
          this.employeeRegularform.controls.BasicELAMT.setValue(this.Empcodedata[0].eL_AMT);
          this.employeeRegularform.controls.BasicIR.setValue(this.Empcodedata[0].ir);
          this.employeeRegularform.controls.BasicPR.setValue(this.Empcodedata[0].pr);
          this.employeeRegularform.controls.BasicMISCEAR.setValue(this.Empcodedata[0].misC_EAR);
          this.employeeRegularform.controls.totear.setValue(this.Empcodedata[0].toT_EARN);
          this.employeeRegularform.controls.BasicPF.setValue(this.Empcodedata[0].pf);
          this.employeeRegularform.controls.BasicVPF.setValue(this.Empcodedata[0].vpf);
          this.employeeRegularform.controls.BasicLIC.setValue(this.Empcodedata[0].lic);
          this.employeeRegularform.controls.BasicIT.setValue(this.Empcodedata[0].it);
          this.employeeRegularform.controls.BasicPTAX.setValue(this.Empcodedata[0].p_TAX);
          this.employeeRegularform.controls.BasicGIS.setValue(this.Empcodedata[0].gis);
          this.employeeRegularform.controls.BasicPFIORI.setValue(this.Empcodedata[0].pF_IORI);


          this.employeeRegularform.controls.BasicPFBAL.setValue(this.Empcodedata[0].pF_BAL);
          this.employeeRegularform.controls.BasicVHIORI.setValue(this.Empcodedata[0].vH_IORI);
          this.employeeRegularform.controls.BasicVHBAL.setValue(this.Empcodedata[0].vH_BAL);
          this.employeeRegularform.controls.BasicMRIORI.setValue(this.Empcodedata[0].mR_IORI);
          this.employeeRegularform.controls.BasicMRBAL.setValue(this.Empcodedata[0].mR_BAL);
          this.employeeRegularform.controls.BasicHBAINST.setValue(this.Empcodedata[0].hbA_INST);
          this.employeeRegularform.controls.BasicHBABAL.setValue(this.Empcodedata[0].hbA_BAL);
          this.employeeRegularform.controls.BasicHBARIORI.setValue(this.Empcodedata[0].hbA_R_IORI);
          this.employeeRegularform.controls.BasicHBARBAL.setValue(this.Empcodedata[0].hbA_R_BAL);
          this.employeeRegularform.controls.BasicHBABANK.setValue(this.Empcodedata[0].hbA_BANK);
          this.employeeRegularform.controls.BasicCOMPINST.setValue(this.Empcodedata[0].comP_INST);
          this.employeeRegularform.controls.BasicCOMPBAL.setValue(this.Empcodedata[0].comP_BAL);
          this.employeeRegularform.controls.BasicPLI.setValue(this.Empcodedata[0].pli);
          this.employeeRegularform.controls.BasicCPS.setValue(this.Empcodedata[0].cps);
          this.employeeRegularform.controls.BasicMISCDED.setValue(this.Empcodedata[0].misC_DED);
          this.employeeRegularform.controls.totded.setValue(this.Empcodedata[0].toT_DED);//total ded
          this.employeeRegularform.controls.totnetpat.setValue(this.Empcodedata[0].neT_PAY);//net pay


          this.RegularModal.show();

        }
        if (this.emptype == "2") {


          this.employeeOutsourcingform.controls.outsrcBasic.setValue(this.Empcodedata[0].basic);
          this.employeeOutsourcingform.controls.outsrcSDA.setValue(this.Empcodedata[0].da);
          this.employeeOutsourcingform.controls.OutsrcSHRA.setValue(this.Empcodedata[0].hra);
          this.employeeOutsourcingform.controls.OutsrcCCA.setValue(this.Empcodedata[0].cca);
          this.employeeOutsourcingform.controls.OutsrcMALLW.setValue(this.Empcodedata[0].m_ALL);
          this.employeeOutsourcingform.controls.OutsrcPP.setValue(this.Empcodedata[0].pp);
          this.employeeOutsourcingform.controls.outsrcIR.setValue(this.Empcodedata[0].ir);
          this.employeeOutsourcingform.controls.outsrcMISCEAR.setValue(this.Empcodedata[0].misC_EAR);
          this.employeeOutsourcingform.controls.OustsrcPTAX.setValue(this.Empcodedata[0].p_TAX);
          this.employeeOutsourcingform.controls.OustsrcIT.setValue(this.Empcodedata[0].it);
          this.employeeOutsourcingform.controls.OustsrcMISCDED.setValue(this.Empcodedata[0].misC_DED);
          this.employeeOutsourcingform.controls.totearoutsrc.setValue(this.Empcodedata[0].toT_EARN);
          this.employeeOutsourcingform.controls.totdedoutsrc.setValue(this.Empcodedata[0].toT_DED);//total ded
          this.employeeOutsourcingform.controls.totnetpayoutsrc.setValue(this.Empcodedata[0].neT_PAY);
          this.DeputationModal.show();
        }
        if (this.emptype == "3") {



          this.employeeContractform.controls.contrPAY.setValue(this.Empcodedata[0].basic);
          this.employeeContractform.controls.contrCA.setValue(this.Empcodedata[0].ca);
          this.employeeContractform.controls.CONTRmiscear.setValue(this.Empcodedata[0].misC_EAR);
          this.employeeContractform.controls.contrtotear.setValue(this.Empcodedata[0].toT_EARN);
          this.employeeContractform.controls.contrPF.setValue(this.Empcodedata[0].pf);
          this.employeeContractform.controls.contrit.setValue(this.Empcodedata[0].it);
          this.employeeContractform.controls.contrMISCDED.setValue(this.Empcodedata[0].misC_DED);
          this.employeeContractform.controls.contrtotded.setValue(this.Empcodedata[0].toT_DED);//total ded
          this.employeeContractform.controls.contrnetpay.setValue(this.Empcodedata[0].neT_PAY);


          this.ContractModal.show();
        }

      }
      else {
        this.Datatype = "Add";

        this.employeeRegularform.reset();
        this.employeepayroleform.reset();
        this.employeeOutsourcingform.reset();
        this.employeeContractform.reset();
        if (this.emptype == "1") {
          this.RegularModal.show();
        }
        if (this.emptype == "2") {
          this.DeputationModal.show();

        }
        if (this.emptype == "3") {

          this.ContractModal.show();
        }
        console.log(data.StatusMessage);
        this.Empcodedata = data.Details;

      }
    },
      error => console.log(error));







    // this.resetallforms();
    // this.modeltitle = "Edit";
    // this.showloader = true;
    // this.isEdit = true;
    // this.uid_isvalid = true;
    // this.uidstatus = true;v vcgfdfre5
    // this.contact.EMPCODE.setValue(data.emP_CODE);
    // this.contact.EmpAadhaar.setValue(data.emP_UID);
    // this.contact.EmpName.setValue(data.emP_NAME);
    // this.GetEmployeeDetails(data.emP_CODE,'edit');ed 
    // this.GetEmpFamilyDetails(data.emP_CODE,'edit');
    105
    // this.showloader = false;
    // this.editModal.show();

  }

  RegularModalpay() {

  }
  DeputationModalpay() {

  }
  ContractModalpay() {

  }

  CloseEditModal() {


    this.RegularModal.hide();
    this.DeputationModal.hide();
    this.ContractModal.hide();

  }


  finalSubmit() {
    const req = new EmployeeSalaryInputRequest;

    if (this.Datatype == "Add") {
      //       this.employeeRegularform.controls.totnetpat.setValue (this.employeeRegularform.controls.totear.value-this.employeeRegularform.controls.totded.value);
      this.showloader = true;

      if (this.employeeRegularform.controls.Basic.value == "" || this.employeeRegularform.controls.Basic.value == null) {

        this.employeeRegularform.controls.Basic.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicDA.value == "" || this.employeeRegularform.controls.BasicDA.value == null) {
        this.employeeRegularform.controls.BasicDA.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicHRA.value == "" || this.employeeRegularform.controls.BasicHRA.value == null) {
        this.employeeRegularform.controls.BasicHRA.setValue(0);

      }
      if (this.employeeRegularform.controls.BasicCCA.value == "" || this.employeeRegularform.controls.BasicCCA.value == null) {
        this.employeeRegularform.controls.BasicCCA.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicCA.value == "" || this.employeeRegularform.controls.BasicCA.value == null) {
        this.employeeRegularform.controls.BasicCA.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicMALL.value == "" || this.employeeRegularform.controls.BasicMALL.value == null) {
        this.employeeRegularform.controls.BasicMALL.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicWALL.value == "" || this.employeeRegularform.controls.BasicWALL.value == null) {

        this.employeeRegularform.controls.BasicWALL.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicSPPAY.value == "" || this.employeeRegularform.controls.BasicSPPAY.value == null) {
        this.employeeRegularform.controls.BasicSPPAY.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicELAMT.value == "" || this.employeeRegularform.controls.BasicELAMT.value == null) {
        this.employeeRegularform.controls.BasicELAMT.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicIR.value == "" || this.employeeRegularform.controls.BasicIR.value == null) {

        this.employeeRegularform.controls.BasicIR.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicPR.value == "" || this.employeeRegularform.controls.BasicPR.value == null) {
        this.employeeRegularform.controls.BasicPR.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicMISCEAR.value == "" || this.employeeRegularform.controls.BasicMISCEAR.value == null) {
        this.employeeRegularform.controls.BasicMISCEAR.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicPF.value == "" || this.employeeRegularform.controls.BasicPF.value == null) {
        this.employeeRegularform.controls.BasicPF.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicVPF.value == "" || this.employeeRegularform.controls.BasicVPF.value == null) {
        this.employeeRegularform.controls.BasicVPF.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicLIC.value == "" || this.employeeRegularform.controls.BasicLIC.value == null) {
        this.employeeRegularform.controls.BasicLIC.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicIT.value == "" || this.employeeRegularform.controls.BasicIT.value == null) {
        this.employeeRegularform.controls.BasicIT.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicPTAX.value == "" || this.employeeRegularform.controls.BasicPTAX.value == null) {
        this.employeeRegularform.controls.BasicPTAX.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicGIS.value == "" || this.employeeRegularform.controls.BasicGIS.value == null) {
        this.employeeRegularform.controls.BasicGIS.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicPFIORI.value == "" || this.employeeRegularform.controls.BasicPFIORI.value == null) {
        this.employeeRegularform.controls.BasicPFIORI.setValue(0);

      }

      this.employeeRegularform.controls.totear.setValue(parseFloat(this.employeeRegularform.controls.Basic.value) + parseFloat(this.employeeRegularform.controls.BasicDA.value) + parseFloat(this.employeeRegularform.controls.BasicHRA.value) + parseFloat(this.employeeRegularform.controls.BasicCCA.value) + parseFloat(this.employeeRegularform.controls.BasicCA.value) + parseFloat(this.employeeRegularform.controls.BasicMALL.value) + parseFloat(this.employeeRegularform.controls.BasicWALL.value) + parseFloat(this.employeeRegularform.controls.BasicSPPAY.value) + parseFloat(this.employeeRegularform.controls.BasicELAMT.value) + parseFloat(this.employeeRegularform.controls.BasicIR.value) + parseFloat(this.employeeRegularform.controls.BasicPR.value) + parseFloat(this.employeeRegularform.controls.BasicMISCEAR.value));

      this.employeeRegularform.controls.totded.setValue(parseFloat(this.employeeRegularform.controls.BasicPF.value) + parseFloat(this.employeeRegularform.controls.BasicVPF.value) + parseFloat(this.employeeRegularform.controls.BasicLIC.value) + parseFloat(this.employeeRegularform.controls.BasicIT.value) + parseFloat(this.employeeRegularform.controls.BasicPTAX.value) + parseFloat(this.employeeRegularform.controls.BasicGIS.value) + parseFloat(this.employeeRegularform.controls.BasicPFIORI.value));

      this.employeeRegularform.controls.totnetpat.setValue(this.employeeRegularform.controls.totear.value - this.employeeRegularform.controls.totded.value);


      if (this.employeeRegularform.controls.BasicPFBAL.value == "" || this.employeeRegularform.controls.BasicPFBAL.value == null) {
        this.employeeRegularform.controls.BasicPFBAL.setValue(0);

      }
      if (this.employeeRegularform.controls.BasicVHIORI.value == "" || this.employeeRegularform.controls.BasicVHIORI.value == null) {
        this.employeeRegularform.controls.BasicVHIORI.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicVHBAL.value == "" || this.employeeRegularform.controls.BasicVHBAL.value == null) {
        this.employeeRegularform.controls.BasicVHBAL.setValue(0);
      }
      if (this.employeeRegularform.controls.BasicMRIORI.value == "" || this.employeeRegularform.controls.BasicMRIORI.value == null) {
        this.employeeRegularform.controls.BasicMRIORI.setValue(0);
      }

      if (this.employeeRegularform.controls.BasicMRBAL.value == "" || this.employeeRegularform.controls.BasicMRBAL.value == null) {
        this.employeeRegularform.controls.BasicMRBAL.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicHBAINST.value == "" || this.employeeRegularform.controls.BasicHBAINST.value == null) {
        this.employeeRegularform.controls.BasicHBAINST.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicHBABAL.value == "" || this.employeeRegularform.controls.BasicHBABAL.value == null) {
        this.employeeRegularform.controls.BasicHBABAL.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicHBARIORI.value == null || this.employeeRegularform.controls.BasicHBARIORI.value == "") {
        this.employeeRegularform.controls.BasicHBARIORI.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicHBARBAL.value == "" || this.employeeRegularform.controls.BasicHBARBAL.value == null) {
        this.employeeRegularform.controls.BasicHBARBAL.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicHBABANK.value == "" || this.employeeRegularform.controls.BasicHBABANK.value == null) {
        this.employeeRegularform.controls.BasicHBABANK.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicCOMPINST.value == "" || this.employeeRegularform.controls.BasicCOMPINST.value == null) {
        this.employeeRegularform.controls.BasicCOMPINST.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicCOMPBAL.value == "" || this.employeeRegularform.controls.BasicCOMPBAL.value == null) {
        this.employeeRegularform.controls.BasicCOMPBAL.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicPLI.value == "" || this.employeeRegularform.controls.BasicPLI.value == null) {
        this.employeeRegularform.controls.BasicPLI.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicCPS.value == "" || this.employeeRegularform.controls.BasicCPS.value == null) {
        this.employeeRegularform.controls.BasicCPS.setValue(0);

      }

      if (this.employeeRegularform.controls.BasicMISCDED.value == "" || this.employeeRegularform.controls.BasicMISCDED.value == null) {
        this.employeeRegularform.controls.BasicMISCDED.setValue(0);

      }

      req.INPUT_01 = this.employeecode;
      req.INPUT_03 = this.employeeRegularform.controls.Basic.value;
      req.INPUT_04 = this.employeeRegularform.controls.BasicDA.value;
      req.INPUT_05 = this.employeeRegularform.controls.BasicHRA.value;
      req.INPUT_06 = this.employeeRegularform.controls.BasicCCA.value;
      req.INPUT_07 = this.employeeRegularform.controls.BasicCA.value;
      req.INPUT_08 = this.employeeRegularform.controls.BasicMALL.value;
      req.INPUT_09 = this.employeeRegularform.controls.BasicWALL.value;
      req.INPUT_10 = this.employeeRegularform.controls.BasicSPPAY.value;
      req.INPUT_11 = this.employeeRegularform.controls.BasicELAMT.value;
      req.INPUT_12 = this.employeeRegularform.controls.BasicIR.value;
      req.INPUT_13 = this.employeeRegularform.controls.BasicPR.value;
      req.INPUT_14 = this.employeeRegularform.controls.BasicMISCEAR.value;

      req.INPUT_15 = this.employeeRegularform.controls.totear.value;

      req.INPUT_16 = this.employeeRegularform.controls.BasicPF.value;
      req.INPUT_17 = this.employeeRegularform.controls.BasicVPF.value;
      req.INPUT_18 = this.employeeRegularform.controls.BasicLIC.value;
      req.INPUT_19 = this.employeeRegularform.controls.BasicIT.value;
      req.INPUT_20 = this.employeeRegularform.controls.BasicPTAX.value;
      req.INPUT_21 = this.employeeRegularform.controls.BasicGIS.value;
      req.INPUT_22 = this.employeeRegularform.controls.BasicPFIORI.value;


      req.INPUT_23 = this.employeeRegularform.controls.BasicPFBAL.value;
      req.INPUT_24 = this.employeeRegularform.controls.BasicVHIORI.value;
      req.INPUT_25 = this.employeeRegularform.controls.BasicVHBAL.value;
      req.INPUT_26 = this.employeeRegularform.controls.BasicMRIORI.value;
      req.INPUT_27 = this.employeeRegularform.controls.BasicMRBAL.value;
      req.INPUT_28 = this.employeeRegularform.controls.BasicHBAINST.value;
      req.INPUT_29 = this.employeeRegularform.controls.BasicHBABAL.value;
      req.INPUT_30 = this.employeeRegularform.controls.BasicHBARIORI.value;
      req.INPUT_31 = this.employeeRegularform.controls.BasicHBARBAL.value;
      req.INPUT_32 = this.employeeRegularform.controls.BasicHBABANK.value;
      req.INPUT_33 = this.employeeRegularform.controls.BasicCOMPINST.value;
      req.INPUT_34 = this.employeeRegularform.controls.BasicCOMPBAL.value;
      req.INPUT_35 = this.employeeRegularform.controls.BasicPLI.value;
      req.INPUT_36 = this.employeeRegularform.controls.BasicCPS.value;
      req.INPUT_37 = this.employeeRegularform.controls.BasicMISCDED.value;

      req.INPUT_38 = this.employeeRegularform.controls.totded.value;//total ded
      req.INPUT_39 = this.employeeRegularform.controls.totnetpat.value;//net pay
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      req.INPUT_41 = yyyy.toString();
      req.INPUT_42 = mm.toString();
      req.USER_NAME = this.logUsercode;
      req.CALL_SOURCE = "WEB";
      this.service.postData(req, "SaveEmpSalDetails").subscribe(data => {
        this.empaddarray = data;
        // this.empaddarray=data.result;
        if (this.empaddarray.StatusCode == "100") {

          this.showloader = false;

          this.RegularModal.hide();
          Swal.fire("success", "Data Submitted Successfully", "success");
        }
        else {
          this.showloader = false;
          Swal.fire("info", this.empaddarray.StatusMessage, "info");
        }
      },

        error => console.log(error));
      this.showloader = false;

    }
    if (this.Datatype == "Update") {
      const req = new EmployeeSalaryInputRequest();

      req.INPUT_01 = this.employeecode;
      req.INPUT_03 = this.employeeRegularform.controls.Basic.value;
      req.INPUT_04 = this.employeeRegularform.controls.BasicDA.value;
      req.INPUT_05 = this.employeeRegularform.controls.BasicHRA.value;
      req.INPUT_06 = this.employeeRegularform.controls.BasicCCA.value;
      req.INPUT_07 = this.employeeRegularform.controls.BasicCA.value;
      req.INPUT_08 = this.employeeRegularform.controls.BasicMALL.value;
      req.INPUT_09 = this.employeeRegularform.controls.BasicWALL.value;
      req.INPUT_10 = this.employeeRegularform.controls.BasicSPPAY.value;
      req.INPUT_11 = this.employeeRegularform.controls.BasicELAMT.value;
      req.INPUT_12 = this.employeeRegularform.controls.BasicIR.value;
      req.INPUT_13 = this.employeeRegularform.controls.BasicPR.value;
      req.INPUT_14 = this.employeeRegularform.controls.BasicMISCEAR.value;
      req.INPUT_15 = this.employeeRegularform.controls.totear.value;
      req.INPUT_16 = this.employeeRegularform.controls.BasicPF.value;
      req.INPUT_17 = this.employeeRegularform.controls.BasicVPF.value;
      req.INPUT_18 = this.employeeRegularform.controls.BasicLIC.value;
      req.INPUT_19 = this.employeeRegularform.controls.BasicIT.value;
      req.INPUT_20 = this.employeeRegularform.controls.BasicPTAX.value;
      req.INPUT_21 = this.employeeRegularform.controls.BasicGIS.value;
      req.INPUT_22 = this.employeeRegularform.controls.BasicPFIORI.value;


      req.INPUT_23 = this.employeeRegularform.controls.BasicPFBAL.value;
      req.INPUT_24 = this.employeeRegularform.controls.BasicVHIORI.value;
      req.INPUT_25 = this.employeeRegularform.controls.BasicVHBAL.value;
      req.INPUT_26 = this.employeeRegularform.controls.BasicMRIORI.value;
      req.INPUT_27 = this.employeeRegularform.controls.BasicMRBAL.value;
      req.INPUT_28 = this.employeeRegularform.controls.BasicHBAINST.value;
      req.INPUT_29 = this.employeeRegularform.controls.BasicHBABAL.value;
      req.INPUT_30 = this.employeeRegularform.controls.BasicHBARIORI.value;
      req.INPUT_31 = this.employeeRegularform.controls.BasicHBARBAL.value;
      req.INPUT_32 = this.employeeRegularform.controls.BasicHBABANK.value;
      req.INPUT_33 = this.employeeRegularform.controls.BasicCOMPINST.value;
      req.INPUT_34 = this.employeeRegularform.controls.BasicCOMPBAL.value;
      req.INPUT_35 = this.employeeRegularform.controls.BasicPLI.value;
      req.INPUT_36 = this.employeeRegularform.controls.BasicCPS.value;
      req.INPUT_37 = this.employeeRegularform.controls.BasicMISCDED.value;
      req.INPUT_38 = this.employeeRegularform.controls.totded.value;//total ded
      req.INPUT_39 = this.employeeRegularform.controls.totnetpat.value;//net pay
      req.CALL_SOURCE = "WEB";
      req.USER_NAME = this.logUserrole;
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      req.INPUT_42 = yyyy.toString();
      req.INPUT_43 = mm.toString();

      this.service.postData(req, "UpdateEmpSalDetails").subscribe(data => {
        this.empaddarray = data;
        // this.empaddarray=data.result;
        if (this.empaddarray.StatusCode == "100") {

          this.showloader = false;

          this.RegularModal.hide();
          Swal.fire("success", "Data Updated Successfully", "success");
        }
        else {
          this.showloader = false;
          Swal.fire("info", this.empaddarray.StatusMessage, "info");
        }
      },

        error => console.log(error));
      this.showloader = false;


    }
  }


  Outsrcfinalsubmit() {

    const req = new EmployeeSalaryInputRequest;
    this.showloader = true;

    if (this.Datatype == "Add") {

      if (this.employeeOutsourcingform.controls.outsrcBasic.value == "" || this.employeeOutsourcingform.controls.outsrcBasic.value == null) {

        this.employeeOutsourcingform.controls.outsrcBasic.setValue(0);
      }

      if (this.employeeOutsourcingform.controls.outsrcSDA.value == "" || this.employeeOutsourcingform.controls.outsrcSDA.value == null) {
        this.employeeOutsourcingform.controls.outsrcSDA.setValue(0);
      }

      if (this.employeeOutsourcingform.controls.OutsrcSHRA.value == "" || this.employeeOutsourcingform.controls.OutsrcSHRA.value == null) {
        this.employeeOutsourcingform.controls.OutsrcSHRA.setValue(0);

      }

      if (this.employeeOutsourcingform.controls.OutsrcCCA.value == "" || this.employeeOutsourcingform.controls.OutsrcCCA.value == null) {
        this.employeeOutsourcingform.controls.OutsrcCCA.setValue(0);
      }

      if (this.employeeOutsourcingform.controls.OutsrcMALLW.value == "" || this.employeeOutsourcingform.controls.OutsrcMALLW.value == null) {
        this.employeeOutsourcingform.controls.OutsrcMALLW.setValue(0);
      }

      if (this.employeeOutsourcingform.controls.OutsrcPP.value == "" || this.employeeOutsourcingform.controls.OutsrcPP.value == null) {
        this.employeeOutsourcingform.controls.OutsrcPP.setValue(0);
      }

      if (this.employeeOutsourcingform.controls.outsrcIR.value == "" || this.employeeOutsourcingform.controls.outsrcIR.value == null) {

        this.employeeOutsourcingform.controls.outsrcIR.setValue(0);
      }

      if (this.employeeOutsourcingform.controls.outsrcMISCEAR.value == "" || this.employeeOutsourcingform.controls.outsrcMISCEAR.value == null) {
        this.employeeOutsourcingform.controls.outsrcMISCEAR.setValue(0);
      }
      if (this.employeeOutsourcingform.controls.OustsrcPTAX.value == "" || this.employeeOutsourcingform.controls.OustsrcPTAX.value == null) {
        this.employeeOutsourcingform.controls.OustsrcPTAX.setValue(0);
      }
      if (this.employeeOutsourcingform.controls.OustsrcIT.value == "" || this.employeeOutsourcingform.controls.OustsrcIT.value == null) {

        this.employeeOutsourcingform.controls.OustsrcIT.setValue(0);
      }
      if (this.employeeOutsourcingform.controls.OustsrcMISCDED.value == "" || this.employeeOutsourcingform.controls.OustsrcMISCDED.value == null) {
        this.employeeOutsourcingform.controls.OustsrcMISCDED.setValue(0);
      }

      this.employeeOutsourcingform.controls.totearoutsrc.setValue(parseFloat(this.employeeOutsourcingform.controls.outsrcBasic.value) + parseFloat(this.employeeOutsourcingform.controls.outsrcSDA.value) + parseFloat(this.employeeOutsourcingform.controls.OutsrcSHRA.value) + parseFloat(this.employeeOutsourcingform.controls.OutsrcCCA.value) + parseFloat(this.employeeOutsourcingform.controls.OutsrcMALLW.value) + parseFloat(this.employeeOutsourcingform.controls.OutsrcPP.value) + parseFloat(this.employeeOutsourcingform.controls.outsrcIR.value) + parseFloat(this.employeeOutsourcingform.controls.outsrcMISCEAR.value));

      this.employeeOutsourcingform.controls.totdedoutsrc.setValue(parseFloat(this.employeeOutsourcingform.controls.OustsrcPTAX.value) + parseFloat(this.employeeOutsourcingform.controls.OustsrcIT.value) + parseFloat(this.employeeOutsourcingform.controls.OustsrcMISCDED.value));

      this.employeeOutsourcingform.controls.totnetpayoutsrc.setValue(this.employeeOutsourcingform.controls.totearoutsrc.value - this.employeeOutsourcingform.controls.totdedoutsrc.value);



      req.INPUT_01 = this.employeecode;
      req.INPUT_03 = this.employeeOutsourcingform.controls.outsrcBasic.value;
      req.INPUT_04 = this.employeeOutsourcingform.controls.outsrcSDA.value;
      req.INPUT_05 = this.employeeOutsourcingform.controls.OutsrcSHRA.value;
      req.INPUT_06 = this.employeeOutsourcingform.controls.OutsrcCCA.value;
      req.INPUT_08 = this.employeeOutsourcingform.controls.OutsrcMALLW.value;
      req.INPUT_40 = this.employeeOutsourcingform.controls.OutsrcPP.value;
      req.INPUT_12 = this.employeeOutsourcingform.controls.outsrcIR.value;
      req.INPUT_14 = this.employeeOutsourcingform.controls.outsrcMISCEAR.value;
      req.INPUT_19 = this.employeeOutsourcingform.controls.OustsrcPTAX.value;
      req.INPUT_20 = this.employeeOutsourcingform.controls.OustsrcIT.value;
      req.INPUT_37 = this.employeeOutsourcingform.controls.OustsrcMISCDED.value;
      req.INPUT_15 = this.employeeOutsourcingform.controls.totearoutsrc.value;
      req.INPUT_38 = this.employeeOutsourcingform.controls.totdedoutsrc.value;//total ded
      req.INPUT_39 = this.employeeOutsourcingform.controls.totnetpayoutsrc.value;
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      req.INPUT_41 = yyyy.toString();
      req.INPUT_42 = mm.toString();
      req.USER_NAME = this.logUsercode;
      req.CALL_SOURCE = "WEB";
      this.service.postData(req, "SaveEmpSalDetails").subscribe(data => {
        this.empaddarray = data;
        // this.empaddarray=data.result;
        if (this.empaddarray.StatusCode == "100") {

          this.showloader = false;

          this.RegularModal.hide();
          this.DeputationModal.hide();
          Swal.fire("success", "Data Submitted Successfully", "success");
        }
        else {
          this.showloader = false;
          Swal.fire("info", this.empaddarray.StatusMessage, "info");
        }
      },

        error => console.log(error));
      this.showloader = false;

    }

    if (this.Datatype == "Update") {
      const req = new EmployeeSalaryInputRequest();

      req.INPUT_01 = this.employeecode;
      req.INPUT_03 = this.employeeOutsourcingform.controls.outsrcBasic.value;
      req.INPUT_04 = this.employeeOutsourcingform.controls.outsrcSDA.value;
      req.INPUT_05 = this.employeeOutsourcingform.controls.OutsrcSHRA.value;
      req.INPUT_06 = this.employeeOutsourcingform.controls.OutsrcCCA.value;
      req.INPUT_08 = this.employeeOutsourcingform.controls.OutsrcMALLW.value;
      req.INPUT_40 = this.employeeOutsourcingform.controls.OutsrcPP.value;
      req.INPUT_12 = this.employeeOutsourcingform.controls.outsrcIR.value;
      req.INPUT_14 = this.employeeOutsourcingform.controls.outsrcMISCEAR.value;
      req.INPUT_19 = this.employeeOutsourcingform.controls.OustsrcPTAX.value;
      req.INPUT_20 = this.employeeOutsourcingform.controls.OustsrcIT.value;
      req.INPUT_37 = this.employeeOutsourcingform.controls.OustsrcMISCDED.value;
      req.INPUT_15 = this.employeeOutsourcingform.controls.totearoutsrc.value;
      req.INPUT_38 = this.employeeOutsourcingform.controls.totdedoutsrc.value;//total ded
      req.INPUT_39 = this.employeeOutsourcingform.controls.totnetpayoutsrc.value;
      req.CALL_SOURCE = "WEB";
      req.USER_NAME = this.logUserrole;
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      req.INPUT_42 = yyyy.toString();
      req.INPUT_43 = mm.toString();

      this.service.postData(req, "UpdateEmpSalDetails").subscribe(data => {
        this.empaddarray = data;
        // this.empaddarray=data.result;
        if (this.empaddarray.StatusCode == "100") {

          this.showloader = false;

          this.RegularModal.hide();
          this.DeputationModal.hide();
          Swal.fire("success", "Data Updated Successfully", "success");
        }
        else {
          this.showloader = false;
          this.DeputationModal.hide();
          Swal.fire("info", this.empaddarray.StatusMessage, "info");
        }
      },

        error => console.log(error));
      this.showloader = false;


    }


  }


  contractfinalsubmit() {

    const req = new EmployeeSalaryInputRequest;
    this.showloader = true;

    if (this.Datatype == "Add") {

      if (this.employeeContractform.controls.contrPAY.value == "" || this.employeeContractform.controls.contrPAY.value == null) {
        this.employeeContractform.controls.contrPAY.setValue(0);
      }
      if (this.employeeContractform.controls.contrCA.value == "" || this.employeeContractform.controls.contrCA.value == null) {
        this.employeeContractform.controls.contrCA.setValue(0);
      }
      if (this.employeeContractform.controls.CONTRmiscear.value == "" || this.employeeContractform.controls.CONTRmiscear.value == null) {
        this.employeeContractform.controls.CONTRmiscear.setValue(0);
      }
      if (this.employeeContractform.controls.contrPF.value == "" || this.employeeContractform.controls.contrPF.value == null) {
        this.employeeContractform.controls.contrPF.setValue(0);
      }
      if (this.employeeContractform.controls.contrit.value == "" || this.employeeContractform.controls.contrit.value == null) {
        this.employeeContractform.controls.contrit.setValue(0);
      }
      if (this.employeeContractform.controls.contrMISCDED.value == "" || this.employeeContractform.controls.contrMISCDED.value == null) {
        this.employeeContractform.controls.contrMISCDED.setValue(0);
      }


      this.employeeContractform.controls.contrtotear.setValue(parseFloat(this.employeeContractform.controls.contrPAY.value) + parseFloat(this.employeeContractform.controls.contrCA.value) + parseFloat(this.employeeContractform.controls.CONTRmiscear.value));

      this.employeeContractform.controls.contrtotded.setValue(parseFloat(this.employeeContractform.controls.contrPF.value) + parseFloat(this.employeeContractform.controls.contrit.value) + parseFloat(this.employeeContractform.controls.contrMISCDED.value));

      this.employeeContractform.controls.contrnetpay.setValue(this.employeeContractform.controls.contrtotear.value - this.employeeContractform.controls.contrtotded.value);



      req.INPUT_01 = this.employeecode;
      req.INPUT_03 = this.employeeContractform.controls.contrPAY.value;
      req.INPUT_07 = this.employeeContractform.controls.contrCA.value;
      req.INPUT_14 = this.employeeContractform.controls.CONTRmiscear.value;
      req.INPUT_15 = this.employeeContractform.controls.contrtotear.value;
      req.INPUT_16 = this.employeeContractform.controls.contrPF.value;
      req.INPUT_19 = this.employeeContractform.controls.contrit.value;
      req.INPUT_37 = this.employeeContractform.controls.contrMISCDED.value;
      req.INPUT_38 = this.employeeContractform.controls.contrtotded.value;//total ded
      req.INPUT_39 = this.employeeContractform.controls.contrnetpay.value;
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      req.INPUT_41 = yyyy.toString();
      req.INPUT_42 = mm.toString();
      req.USER_NAME = this.logUsercode;
      req.CALL_SOURCE = "WEB";
      this.service.postData(req, "SaveEmpSalDetails").subscribe(data => {
        this.empaddarray = data;
        // this.empaddarray=data.result;
        if (this.empaddarray.StatusCode == "100") {

          this.showloader = false;

          this.RegularModal.hide();
          this.ContractModal.hide();
          Swal.fire("success", "Data Submitted Successfully", "success");
        }
        else {
          this.showloader = false;
          Swal.fire("info", this.empaddarray.StatusMessage, "info");
        }
      },

        error => console.log(error));
      this.showloader = false;

    }

    if (this.Datatype == "Update") {
      const req = new EmployeeSalaryInputRequest();

      req.INPUT_01 = this.employeecode;
      req.INPUT_03 = this.employeeContractform.controls.contrPAY.value;
      req.INPUT_07 = this.employeeContractform.controls.contrCA.value;
      req.INPUT_14 = this.employeeContractform.controls.CONTRmiscear.value;
      req.INPUT_15 = this.employeeContractform.controls.contrtotear.value;
      req.INPUT_16 = this.employeeContractform.controls.contrPF.value;
      req.INPUT_19 = this.employeeContractform.controls.contrit.value;
      req.INPUT_37 = this.employeeContractform.controls.contrMISCDED.value;
      req.INPUT_38 = this.employeeContractform.controls.contrtotded.value;//total ded
      req.INPUT_39 = this.employeeContractform.controls.contrnetpay.value;
      req.CALL_SOURCE = "WEB";
      req.USER_NAME = this.logUserrole;
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();
      req.INPUT_42 = yyyy.toString();
      req.INPUT_43 = mm.toString();

      this.service.postData(req, "UpdateEmpSalDetails").subscribe(data => {
        this.empaddarray = data;
        // this.empaddarray=data.result;
        if (this.empaddarray.StatusCode == "100") {

          this.showloader = false;
          this.ContractModal.hide();
          this.RegularModal.hide();
          Swal.fire("success", "Data Updated Successfully", "success");
        }
        else {
          this.showloader = false;
          Swal.fire("info", this.empaddarray.StatusMessage, "info");
        }
      },

        error => console.log(error));
      this.showloader = false;


    }

  }


  employeeRegularform = this.formBuilder.group({
    Basic: [''],
    BasicDA: [''],
    BasicHRA: [''],
    BasicCCA: [''],
    BasicCA: [''],
    BasicMALL: [''],
    BasicWALL: [''],
    BasicSPPAY: [''],
    BasicELAMT: [''],
    BasicMISCEAR: [''],
    BasicIR: [''],
    BasicPR: [''],
    MISCEAR: [''],
    BasicPF: [''],
    BasicVPF: [''],
    BasicLIC: [''],
    BasicIT: [''],
    BasicPTAX: [''],
    BasicGIS: [''],
    BasicPFIORI: [''],
    BasicPFBAL: [''],
    BasicVHIORI: [''],
    BasicVHBAL: [''],
    BasicMRIORI: [''],
    BasicMRBAL: [''],
    BasicHBAINST: [''],
    BasicHBABAL: [''],
    BasicHBARIORI: [''],
    BasicHBARBAL: [''],
    BasicHBABANK: [''],
    BasicCOMPINST: [''],
    BasicCOMPBAL: [''],
    BasicPLI: [''],
    BasicCPS: [''],
    BasicMISCDED: [''],
    totear: [''],
    totded: [''],
    totnetpat: ['']
  });
  employeeOutsourcingform = this.formBuilder.group({

    outsrcBasic: [''],
    outsrcSDA: [''],
    OutsrcSHRA: [''],
    OutsrcCCA: [''],
    OutsrcMALLW: [''],
    OutsrcPP: [''],
    outsrcIR: [''],
    outsrcMISCEAR: [''],
    OustsrcPTAX: [''],
    OustsrcIT: [''],
    OustsrcMISCDED: [''],
    totearoutsrc: [''],
    totdedoutsrc: [''],
    totnetpayoutsrc: ['']
  });

  employeeContractform = this.formBuilder.group({

    contrPAY: [''],
    contrCA: [''],
    CONTRmiscear: [''],
    contrPF: [''],
    contrit: [''],
    contrMISCDED: [''],
    contrtotear: [''],
    contrtotded: [''],
    contrnetpay: ['']

  });
  TotalcontractSubmit() {

    if (this.employeeContractform.controls.contrPAY.value == "" || this.employeeContractform.controls.contrPAY.value == null) {
      this.employeeContractform.controls.contrPAY.setValue(0);
    }
    if (this.employeeContractform.controls.contrCA.value == "" || this.employeeContractform.controls.contrCA.value == null) {
      this.employeeContractform.controls.contrCA.setValue(0);
    }
    if (this.employeeContractform.controls.CONTRmiscear.value == "" || this.employeeContractform.controls.CONTRmiscear.value == null) {
      this.employeeContractform.controls.CONTRmiscear.setValue(0);
    }
    if (this.employeeContractform.controls.contrPF.value == "" || this.employeeContractform.controls.contrPF.value == null) {
      this.employeeContractform.controls.contrPF.setValue(0);
    }
    if (this.employeeContractform.controls.contrit.value == "" || this.employeeContractform.controls.contrit.value == null) {
      this.employeeContractform.controls.contrit.setValue(0);
    }
    if (this.employeeContractform.controls.contrMISCDED.value == "" || this.employeeContractform.controls.contrMISCDED.value == null) {
      this.employeeContractform.controls.contrMISCDED.setValue(0);
    }


    this.employeeContractform.controls.contrtotear.setValue(parseFloat(this.employeeContractform.controls.contrPAY.value) + parseFloat(this.employeeContractform.controls.contrCA.value) + parseFloat(this.employeeContractform.controls.CONTRmiscear.value));

    this.employeeContractform.controls.contrtotded.setValue(parseFloat(this.employeeContractform.controls.contrPF.value) + parseFloat(this.employeeContractform.controls.contrit.value) + parseFloat(this.employeeContractform.controls.contrMISCDED.value));

    this.employeeContractform.controls.contrnetpay.setValue(this.employeeContractform.controls.contrtotear.value - this.employeeContractform.controls.contrtotded.value);

  }
  contrbasicchange() {

    this.TotalcontractSubmit();
  }
  contrcachange() {
    this.TotalcontractSubmit();
  }

  contrmiscearchange() {
    this.TotalcontractSubmit();
  }

  contrpfchange() {
    this.TotalcontractSubmit();
  }
  contritchange() {

    this.TotalcontractSubmit();
  }
  contrmiscchangeded() {
    this.TotalcontractSubmit();
  }
  get dep2() { return this.employeeContractform.controls; }


  TotaloutsrcSubmit() {

    if (this.employeeOutsourcingform.controls.outsrcBasic.value == "" || this.employeeOutsourcingform.controls.outsrcBasic.value == null) {

      this.employeeOutsourcingform.controls.outsrcBasic.setValue(0);
    }

    if (this.employeeOutsourcingform.controls.outsrcSDA.value == "" || this.employeeOutsourcingform.controls.outsrcSDA.value == null) {
      this.employeeOutsourcingform.controls.outsrcSDA.setValue(0);
    }

    if (this.employeeOutsourcingform.controls.OutsrcSHRA.value == "" || this.employeeOutsourcingform.controls.OutsrcSHRA.value == null) {
      this.employeeOutsourcingform.controls.OutsrcSHRA.setValue(0);

    }

    if (this.employeeOutsourcingform.controls.OutsrcCCA.value == "" || this.employeeOutsourcingform.controls.OutsrcCCA.value == null) {
      this.employeeOutsourcingform.controls.OutsrcCCA.setValue(0);
    }

    if (this.employeeOutsourcingform.controls.OutsrcMALLW.value == "" || this.employeeOutsourcingform.controls.OutsrcMALLW.value == null) {
      this.employeeOutsourcingform.controls.OutsrcMALLW.setValue(0);
    }

    if (this.employeeOutsourcingform.controls.OutsrcPP.value == "" || this.employeeOutsourcingform.controls.OutsrcPP.value == null) {
      this.employeeOutsourcingform.controls.OutsrcPP.setValue(0);
    }

    if (this.employeeOutsourcingform.controls.outsrcIR.value == "" || this.employeeOutsourcingform.controls.outsrcIR.value == null) {

      this.employeeOutsourcingform.controls.outsrcIR.setValue(0);
    }

    if (this.employeeOutsourcingform.controls.outsrcMISCEAR.value == "" || this.employeeOutsourcingform.controls.outsrcMISCEAR.value == null) {
      this.employeeOutsourcingform.controls.outsrcMISCEAR.setValue(0);
    }
    if (this.employeeOutsourcingform.controls.OustsrcPTAX.value == "" || this.employeeOutsourcingform.controls.OustsrcPTAX.value == null) {
      this.employeeOutsourcingform.controls.OustsrcPTAX.setValue(0);
    }
    if (this.employeeOutsourcingform.controls.OustsrcIT.value == "" || this.employeeOutsourcingform.controls.OustsrcIT.value == null) {

      this.employeeOutsourcingform.controls.OustsrcIT.setValue(0);
    }
    if (this.employeeOutsourcingform.controls.OustsrcMISCDED.value == "" || this.employeeOutsourcingform.controls.OustsrcMISCDED.value == null) {
      this.employeeOutsourcingform.controls.OustsrcMISCDED.setValue(0);
    }

    this.employeeOutsourcingform.controls.totearoutsrc.setValue(parseFloat(this.employeeOutsourcingform.controls.outsrcBasic.value) + parseFloat(this.employeeOutsourcingform.controls.outsrcSDA.value) + parseFloat(this.employeeOutsourcingform.controls.OutsrcSHRA.value) + parseFloat(this.employeeOutsourcingform.controls.OutsrcCCA.value) + parseFloat(this.employeeOutsourcingform.controls.OutsrcMALLW.value) + parseFloat(this.employeeOutsourcingform.controls.OutsrcPP.value) + parseFloat(this.employeeOutsourcingform.controls.outsrcIR.value) + parseFloat(this.employeeOutsourcingform.controls.outsrcMISCEAR.value));

    this.employeeOutsourcingform.controls.totdedoutsrc.setValue(parseFloat(this.employeeOutsourcingform.controls.OustsrcPTAX.value) + parseFloat(this.employeeOutsourcingform.controls.OustsrcIT.value) + parseFloat(this.employeeOutsourcingform.controls.OustsrcMISCDED.value));

    this.employeeOutsourcingform.controls.totnetpayoutsrc.setValue(this.employeeOutsourcingform.controls.totearoutsrc.value - this.employeeOutsourcingform.controls.totdedoutsrc.value);

  }

  outbasicchange() {

    this.TotaloutsrcSubmit();
  }
  outsrcsdachange() {

    this.TotaloutsrcSubmit();
  }
  OutsrcSHRAchange() {
    this.TotaloutsrcSubmit();
  }
  ouccachange() {
    this.TotaloutsrcSubmit();
  }
  outMAllwchange() {
    this.TotaloutsrcSubmit();
  }
  outppchange() {
    this.TotaloutsrcSubmit();
  }
  outirchange() {
    this.TotaloutsrcSubmit();
  }
  outmiscchange() {
    this.TotaloutsrcSubmit();
  }
  outptaxchange() {
    this.TotaloutsrcSubmit();
  }
  outitchnage() {
    this.TotaloutsrcSubmit();
  }
  OustsrcMISCDEDchange() {
    this.TotaloutsrcSubmit();
  }

  get dep1() { return this.employeeOutsourcingform.controls; }

  get dep() { return this.employeeRegularform.controls; }

  Calculatenetpay() {

    if (this.employeeRegularform.controls.Basic.value == "" || this.employeeRegularform.controls.Basic.value == null) {

      this.employeeRegularform.controls.Basic.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicDA.value == "" || this.employeeRegularform.controls.BasicDA.value == null) {
      this.employeeRegularform.controls.BasicDA.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicHRA.value == "" || this.employeeRegularform.controls.BasicHRA.value == null) {
      this.employeeRegularform.controls.BasicHRA.setValue(0);

    }
    if (this.employeeRegularform.controls.BasicCCA.value == "" || this.employeeRegularform.controls.BasicCCA.value == null) {
      this.employeeRegularform.controls.BasicCCA.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicCA.value == "" || this.employeeRegularform.controls.BasicCA.value == null) {
      this.employeeRegularform.controls.BasicCA.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicMALL.value == "" || this.employeeRegularform.controls.BasicMALL.value == null) {
      this.employeeRegularform.controls.BasicMALL.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicWALL.value == "" || this.employeeRegularform.controls.BasicWALL.value == null) {

      this.employeeRegularform.controls.BasicWALL.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicSPPAY.value == "" || this.employeeRegularform.controls.BasicSPPAY.value == null) {
      this.employeeRegularform.controls.BasicSPPAY.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicELAMT.value == "" || this.employeeRegularform.controls.BasicELAMT.value == null) {
      this.employeeRegularform.controls.BasicELAMT.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicIR.value == "" || this.employeeRegularform.controls.BasicIR.value == null) {

      this.employeeRegularform.controls.BasicIR.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicPR.value == "" || this.employeeRegularform.controls.BasicPR.value == null) {
      this.employeeRegularform.controls.BasicPR.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicMISCEAR.value == "" || this.employeeRegularform.controls.BasicMISCEAR.value == null) {
      this.employeeRegularform.controls.BasicMISCEAR.setValue(0);

    }

    if (this.employeeRegularform.controls.BasicPF.value == "" || this.employeeRegularform.controls.BasicPF.value == null) {
      this.employeeRegularform.controls.BasicPF.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicVPF.value == "" || this.employeeRegularform.controls.BasicVPF.value == null) {
      this.employeeRegularform.controls.BasicVPF.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicLIC.value == "" || this.employeeRegularform.controls.BasicLIC.value == null) {
      this.employeeRegularform.controls.BasicLIC.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicIT.value == "" || this.employeeRegularform.controls.BasicIT.value == null) {
      this.employeeRegularform.controls.BasicIT.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicPTAX.value == "" || this.employeeRegularform.controls.BasicPTAX.value == null) {
      this.employeeRegularform.controls.BasicPTAX.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicGIS.value == "" || this.employeeRegularform.controls.BasicGIS.value == null) {
      this.employeeRegularform.controls.BasicGIS.setValue(0);
    }
    if (this.employeeRegularform.controls.BasicPFIORI.value == "" || this.employeeRegularform.controls.BasicPFIORI.value == null) {
      this.employeeRegularform.controls.BasicPFIORI.setValue(0);

    }

    this.employeeRegularform.controls.totear.setValue(parseFloat(this.employeeRegularform.controls.Basic.value) + parseFloat(this.employeeRegularform.controls.BasicDA.value) + parseFloat(this.employeeRegularform.controls.BasicHRA.value) + parseFloat(this.employeeRegularform.controls.BasicCCA.value) + parseFloat(this.employeeRegularform.controls.BasicCA.value) + parseFloat(this.employeeRegularform.controls.BasicMALL.value) + parseFloat(this.employeeRegularform.controls.BasicWALL.value) + parseFloat(this.employeeRegularform.controls.BasicSPPAY.value) + parseFloat(this.employeeRegularform.controls.BasicELAMT.value) + parseFloat(this.employeeRegularform.controls.BasicIR.value) + parseFloat(this.employeeRegularform.controls.BasicPR.value) + parseFloat(this.employeeRegularform.controls.BasicMISCEAR.value));

    this.employeeRegularform.controls.totded.setValue(parseFloat(this.employeeRegularform.controls.BasicPF.value) + parseFloat(this.employeeRegularform.controls.BasicVPF.value) + parseFloat(this.employeeRegularform.controls.BasicLIC.value) + parseFloat(this.employeeRegularform.controls.BasicIT.value) + parseFloat(this.employeeRegularform.controls.BasicPTAX.value) + parseFloat(this.employeeRegularform.controls.BasicGIS.value) + parseFloat(this.employeeRegularform.controls.BasicPFIORI.value));

    this.employeeRegularform.controls.totnetpat.setValue(this.employeeRegularform.controls.totear.value - this.employeeRegularform.controls.totded.value);

  }
  Basicchange() {
    this.Calculatenetpay();
  }
  DAchange() {
    this.Calculatenetpay();
  }
  HRAchange() {
    this.Calculatenetpay();
  }
  CCAchange() {

    this.Calculatenetpay();
  }
  CAchange() {

    this.Calculatenetpay();
  }
  WALLchange() {

    this.Calculatenetpay();
  }
  MALLchange() {
    this.Calculatenetpay();
  }
  SPAPAYchange() {
    this.Calculatenetpay();

  }
  ELAMTchange() {
    this.Calculatenetpay();
  }
  IRchange() {
    this.Calculatenetpay();

  }
  PRchange() {
    this.Calculatenetpay();
  }
  MISCEARchange() {
    this.Calculatenetpay();

  }
  PFchange() {
    this.Calculatenetpay();
  }
  VPFchange() {
    this.Calculatenetpay();
  }
  LICchange() {

    this.Calculatenetpay();
  }
  ITchange() {
    this.Calculatenetpay();
  }
  BasicPTAXchange() {
    this.Calculatenetpay();
  }
  GISchange() {
    this.Calculatenetpay();
  }
  PFIORIchange() {
    this.Calculatenetpay();
  }

  Generatepayroleforemp() {
    this.employeepayroleform.reset();
    this.PayroleModal.show();
    this.employeepayroleform.controls.payroleyear.setValue('');
    this.employeepayroleform.controls.payroleMonth.setValue('');
    this.employeepayroleform.controls.payroleemptype.setValue('');

    this.months = [];
    this.table = false;

  }

  Yearchange() {
    this.showloader = true;

    const req = new EmployeeSalaryInputRequest();
    req.INPUT_01 = this.employeepayroleform.controls.payroleyear.value;
    this.service.postData(req, "GetMonths").subscribe(data => {
      if (data.StatusCode == "100") {
        this.months = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;

        Swal.fire("info", data.StatusMessage, "info");
      }

    },
      error => console.log(error));


  }
  employeepayroleform = this.formBuilder.group({
    payroleyear: [''],
    payroleMonth: [''],
    payroleemptype: ['']
  });



  PayroleFinalsubmit() {
    const req = new EmployeeSalaryInputRequest;
    this.showloader = true;

    if (this.employeepayroleform.controls.payroleyear.value == "" || this.employeepayroleform.controls.payroleyear.value == null || this.employeepayroleform.controls.payroleyear.value == undefined) {
      Swal.fire("info", "Please Select Year", "info");
      this.showloader = false;
      return false;
    }
    if (this.employeepayroleform.controls.payroleMonth.value == "" || this.employeepayroleform.controls.payroleMonth.value == null || this.employeepayroleform.controls.payroleMonth.value == undefined) {

      Swal.fire("info", "Please Select month", "info");
      this.showloader = false;
      return false;
    }

    if (this.emptype == "" || this.emptype == null || this.emptype == undefined) {

      Swal.fire("info", "Please Select Employee type And click on GetDetails", "info");
      this.showloader = false;
      return false;
    }
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";
    req.INPUT_01 = this.employeepayroleform.controls.payroleyear.value;
    req.INPUT_02 = this.employeepayroleform.controls.payroleMonth.value;
    req.INPUT_03 = this.emptype;
    //var input = { Details:this.empPayrol};
    this.service.postData(req, "SavePayroleDetails").subscribe(data => {
      this.empaddarray = data;
      // this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        this.showloader = false;

        this.RegularModal.hide();
        this.ContractModal.hide();

        this.employeepayroleform.reset();


        this.months = [];

        this.PayroleModal.hide();
        Swal.fire("success", "Payroll Generated Successfully", "success");
      }
      else {
        this.showloader = false;
        this.PayroleModal.hide();
        Swal.fire("info", data.StatusMessage, "info");
      }
    },

      error => console.log(error));
  }

  ClosepayroleModal() {
    this.employeepayroleform.reset();

    this.table = false;
    this.PayroleModal.hide();
  }
}



