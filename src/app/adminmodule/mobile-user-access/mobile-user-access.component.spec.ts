import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileUserAccessComponent } from './mobile-user-access.component';

describe('MobileUserAccessComponent', () => {
  let component: MobileUserAccessComponent;
  let fixture: ComponentFixture<MobileUserAccessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileUserAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileUserAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
