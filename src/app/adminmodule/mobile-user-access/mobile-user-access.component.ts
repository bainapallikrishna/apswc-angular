import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import * as _ from 'lodash';
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-mobile-user-access',
  templateUrl: './mobile-user-access.component.html',
  styleUrls: ['./mobile-user-access.component.css']
})
export class MobileUserPermissionComponent implements OnInit {
  type: number = 0;

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  @ViewChild('addpageModal') addpageModal: ModalDirective;
  @ViewChild('userpermissionModal') userpermissionModal: ModalDirective;
  @ViewChild('designationpermissionModal') designationpermissionModal: ModalDirective;
  @ViewChild('historyModal') historyModal: ModalDirective;

  items: any[];
  isEdit: boolean = false;
  pagetitle: string = "Add";
  PagesList: any[];
  dropdownSettings = {};
  selObj:any[];
  Desig_dropdownSettings = {};
  PagesAccessList: any[];
  distinctThings:any[];
  UsersList: any[];
  DesiginationList = [];
  ResultList = [];
  addpageForm: FormGroup;
  UseraccessForm: FormGroup;
  DesigaccessForm: FormGroup;
  Pagetext: any;
  Pagename: any;
  PageDisc: any;
  Menuid: any;
  rlink: any;
  itmorder: any;
  Errormsg: boolean = false;
  isaddvalue: any;
  iseditvalue: any;
  isviewvalue: any;
  FormUservalue: any;
  Uservalue = [];

  pageid: any;
  designation_type = [];
  designation_id = [];
  dropdownList = [];
  selectedItems = [];
  showloader = false;
  public submitted: boolean = false;

  constructor(private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

  }

  ngOnInit(): void {


    this.buildForm();
    this.Loadpages();
    this.UserForm();
    this.DesigForm();

    this.dropdownSettings = {
      singleSelection: false,
      idField: 'useR_NAME',
      textField: 'useR_NAME',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.Desig_dropdownSettings = {
      singleSelection: false,
      idField: 'designation_id',
      textField: 'designation_name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

  }

  UserForm() {

    this.UseraccessForm = this.formBuilder.group({

      FormUservalue: ['', Validators.required],
      isaddvalue: [''],
      iseditvalue: [''],
      isviewvalue: ['']

    }
    )
  }

  DesigForm() {
    this.DesigaccessForm = this.formBuilder.group({
      FormUservalue: ['', Validators.required],
      UserTypevalue:['',Validators.required],
      isaddvalue: [''],
      iseditvalue: [''],
      isviewvalue: ['']
    }
    )
  }
  buildForm() {

    this.addpageForm = this.formBuilder.group({
      Pagename: ['', Validators.required],
      PageDisc: ['', Validators.required],
      PageIcon: [''],
      PageID: [''],
      Menuid: [''],
      rlink: [''],
      itmorder: ['', Validators.required]

    }
    )
  }

  Loadpages() {
    this.showloader = true;
    const req = new InputRequest();
    this.service.postData(req, "GetPageMobDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.PagesList = data.Details;
        console.log(this.PagesList);
      }
      else {
        Swal.fire('warning', data.StatusMessage, 'warning');
      }
    })

  }

  Loadmenuitems() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "DD";
    this.service.postData(req, "GetPageMobDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.items = data.Details;
      }
      else {

        Swal.fire('warning', data.StatusMessage, 'warning');
      }
    })

  }

  addPage() {
    this.isEdit = false;
    this.pagetitle = "Add";

    this.addpageForm.reset();
    this.Loadmenuitems();
    this.addpageModal.show();

  }

  EditPage(page) {
    this.isEdit = true;
    this.pagetitle = "Edit";
    this.addpageForm.reset();
    this.Loadmenuitems();
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = page.pagE_ID;
    this.service.postData(req, "GetPageMobDetailsByID").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (data.Details[0]) {
          const currdata = data.Details[0];
          this.subf.PageID.setValue(currdata.pagE_ID);
          this.subf.Pagename.setValue(currdata.pagE_NAME);
          this.subf.PageDisc.setValue(currdata.pagE_DESCRIPTION);
          this.subf.PageIcon.setValue(currdata.icoN_NAME);
          this.subf.Menuid.setValue(currdata.menU_ITEM_ID);
          this.subf.rlink.setValue(currdata.routE_LINK);
          this.subf.itmorder.setValue(currdata.iteM_ORDER);

          this.addpageModal.show();
        }
      }
      else {
        Swal.fire('warning', data.StatusMessage, 'warning');
      }
    })


  }


  get subf() { return this.addpageForm.controls };
  get Userf() { return this.UseraccessForm.controls };
  get Desiff() { return this.DesigaccessForm.controls };

  SubmitpageDetails() {
    this.submitted = true;
    if (this.addpageForm.invalid) {
      return false;
    }
    // else if (this.subf.Menu.value!=null||this.subf.Menu.value!="") {
    //   this.Errormsg=true;
    //   return false;
    // }
    // else{
    // this.Errormsg=false;
    // }


    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.subf.Pagename.value; /*pagE_NAME*/
    req.INPUT_02 = this.subf.itmorder.value/*MENU ITEM NAME*/
    req.INPUT_03 = this.subf.Menuid.value;/*MENU ITEM ID*/
    req.INPUT_04 = this.subf.rlink.value; /*ROUTE LINK*/
    req.USER_NAME = this.logUserName; /*INSERTED BY*/
    req.INPUT_05 = this.subf.PageDisc.value;/*PAGE DESCRIPTION*/
    req.INPUT_06 = this.subf.PageIcon.value;/*PAGE Icon*/
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "SavePageMobDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Page Details Submitted Successfully !!!", 'success');

          this.addpageModal.hide();
          // this.resetallforms();
          // this.Loadpages();
          this.reloadCurrentRoute();
        }
        else
          Swal.fire('warning', result.StatusMessage, 'warning');
        ;
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
    },
      error => console.log(error));

  }

  UpdatepageDetails() {
    this.submitted = true;
    if (this.addpageForm.invalid) {
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.subf.PageID.value; /*page id*/
    req.INPUT_02 = this.subf.Pagename.value/*Page Name*/
    req.INPUT_03 = this.subf.PageDisc.value;/*Page Description*/
    req.INPUT_04 = this.subf.Menuid.value; /*Menu ID*/
    req.INPUT_05 = this.subf.rlink.value;/*URL Lik*/
    req.INPUT_06 = this.subf.itmorder.value;/*Item Order*/
    req.INPUT_07 = this.subf.PageIcon.value;/*PAGE Icon*/

    req.USER_NAME = this.logUserName; /*INSERTED BY*/
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "UpdatePageMobDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Page Details Updated Successfully !!!", 'success');

          this.addpageModal.hide();
          // this.resetallforms();
          // this.Loadpages();
          this.reloadCurrentRoute();
        }
        else
          Swal.fire('warning', result.StatusMessage, 'warning');
        ;
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
    },
      error => console.log(error));

  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  resetallforms() {
    this.submitted = false;
    this.Errormsg = false;
    this.addpageForm.reset();
    this.UseraccessForm.reset();
    this.DesigaccessForm.reset();
    this.Uservalue = [];
    this.UsersList = [];
    this.DesiginationList = [];
    this.designation_type = [];
    this.designation_id = [];
    this.selectedItems = []

  }
  hideaddpageModal(): void {
    this.addpageModal.hide();
    this.addpageForm.reset();
    this.submitted = false;


  }


  userPermission(data) {
    this.resetallforms()
    //this.Userf.Uservalue.setValue("");
    this.Pagetext = data.menU_ITEM_NAME;
    this.PagesAccessList = null;
    this.loadPageAccesslist(data.pagE_ID);
    this.pageid = data.pagE_ID;


  }


  loadPageAccesslist(id) {

    const req = new InputRequest();
    req.INPUT_01 = id;
    this.service.postData(req, "GetPageMobAccessDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.PagesAccessList = data.Details;
        this.loaduserlist('DD', id);
      }
      else {
        this.PagesAccessList = data.Details;
        this.loaduserlist('DD', id);
      }
    })


  }

  loaduserlist(id, pagid) {

    const req = new InputRequest();
    req.INPUT_01 = id;
    req.INPUT_02 = pagid;
    this.service.postData(req, "GetPageMobAccessDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.UsersList = data.Details;
        this.userpermissionModal.show();
      }
      else {
        Swal.fire('warning', 'No Users Found', 'warning');
      }
    })


  }

  onItemSelect(item: any) {
    this.Errormsg = false;
    this.Uservalue.push(item.useR_NAME);    
    for (let i = 0; i < this.UsersList.length; i++) {

      if(this.UsersList[i].useR_NAME == item.useR_NAME)
      {
        this.designation_type.push(this.UsersList[i].officeR_DESIGNATION);
      } 

      //this.designation_type.push(this.getDropDownText_user(item.useR_NAME, this.UsersList)[0].officeR_DESIGNATION);
    }
    

  }
  onSelectAll(item: any)
  {
    this.Errormsg = false;
    for (let i = 0; i < item.length; i++) {    
    this.Uservalue.push(item[i].useR_NAME);
    this.designation_type.push(this.getDropDownText_user(item[i].useR_NAME, this.UsersList)[0].officeR_DESIGNATION);
    }

  }

  onItemDeSelect(item: any) {

    this.Uservalue = this.Uservalue.filter(m => m != item.useR_NAME);
    for (let i = 0; i < this.UsersList.length; i++) {

      if(this.UsersList[i].useR_NAME == item.useR_NAME)
      {
        this.designation_type = this.designation_type.filter(m => m != this.UsersList[i].officeR_DESIGNATION);
      } 

      // this.designation_type = this.designation_type.filter(m => m != (this.getDropDownText_user(item.useR_NAME, this.UsersList)[0].officeR_DESIGNATION));
    }



   

  }

  onDeSelectAll(item: any)
  {
    this.Uservalue = [];
    this.designation_type = [];

  }
  getDropDownText_user(id, object) {
    
    const selObj = _.filter(object, function (o) {
      return (_.includes(id, o.useR_NAME));
    });
    return selObj;
  }

  Submit_UserPermission() {

    this.submitted = true;
    this.Errormsg = false;

    if (this.UseraccessForm.invalid) {
      return false;
    }
    if (!this.Uservalue.length) {
      this.Errormsg = true;
      return false;
    }
    


    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.Uservalue.toString();
    req.INPUT_02 = this.pageid;
    req.INPUT_03 = this.Userf.iseditvalue.value == true ? "1" : null;
    req.INPUT_04 = this.Userf.isviewvalue.value == true ? "1" : null;
    req.INPUT_05 = this.Userf.isaddvalue.value == true ? "1" : null;
    req.INPUT_06 = this.designation_type.toString();
    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE= "Web";

    this.service.postData(req, "SaveUserMobaccessDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "User Access Details Submitted Successfully !!!", 'success');

          this.userpermissionModal.hide();
          this.resetallforms();

        }
        else
          Swal.fire('warning', result.StatusMessage, 'warning');
        ;
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
    },
      error => console.log(error));

  }



  hideuserPermissionModal(): void {
    this.userpermissionModal.hide();
  }


  update_UserPermission(data) {

    this.showloader = true;
    const req = new InputRequest();
   
    req.INPUT_01 = this.pageid;
    req.INPUT_02 = data.useR_NAME;    
    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE="Web";

    this.service.postData(req, "UpdateUserMobaccessDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "User Access Details Updated Successfully !!!", 'success');
          
          this.loadPageAccesslist(this.pageid);

        }
        else
          Swal.fire('warning', result.StatusMessage, 'warning');
        ;
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
    },
      error => console.log(error));

  }


  DesignationPermission(data) {
    this.resetallforms()
    // this.Desiff.Uservalue.setValue("");    
    this.PagesAccessList = null;
    this.Pagetext = data.menU_ITEM_NAME;
    this.pageid = data.pagE_ID;
    this.load_DesigPageAccesslist(data.pagE_ID);

  }


  load_DesigPageAccesslist(id) {
    this.Desiff.UserTypevalue.setValue("");
    const req = new InputRequest();
    req.INPUT_01 = id;
    this.service.postData(req, "Get_DesigPageMobAccessDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.PagesAccessList = data.Details;
        this.load_desiglist('DD', id);
      }
      else {
        this.PagesAccessList = data.Details;
        this.load_desiglist('DD', id);
      }
    })


  }
  load_desiglist(id, pagid) {

    const req = new InputRequest();
    req.INPUT_01 = id;
    req.INPUT_02 = pagid;
    this.service.postData(req, "Get_DesigPageMobAccessDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.ResultList = data.Details;
        let Desiglist=data.Details;
        this.distinctThings = Desiglist.filter(
          (thing, i, arr) => arr.findIndex(t => t.designation_type === thing.designation_type) === i
        );
       
        this.designationpermissionModal.show();
      }
      else {
        Swal.fire('warning', 'No Desigination Users Found', 'warning');
      }
    })


  }
  get_Desigaccessvalue(val) {
    this.DesiginationList = this.ResultList.filter(m => m.designation_type === val);
  }
  onItemSelect_Desig(item: any) {

    this.Uservalue.push(item.designation_name);
    for (let i = 0; i < this.DesiginationList.length; i++) {

      if(this.DesiginationList[i].designation_name== item.designation_name)
      {
        this.designation_type.push(this.DesiginationList[i].designation_type);
        this.designation_id.push(this.DesiginationList[i].designation_id);
      } 

    //this.designation_type.push(this.getDropDownText_Desig(item.designation_name, this.DesiginationList)[0].designation_type);
    //this.designation_id.push(this.getDropDownText_Desig(item.designation_name, this.DesiginationList)[0].designation_id);
    }
  }
  onSelectAll_Desig(item: any)
  {

    for (let i = 0; i < item.length; i++) {
      this.Uservalue.push(item[i].designation_name);
      this.designation_type.push(this.getDropDownText_Desig(item[i].designation_name, this.DesiginationList)[0].designation_type);
      this.designation_id.push(this.getDropDownText_Desig(item[i].designation_name, this.DesiginationList)[0].designation_id);
      
    }
    
  }

  onItemDeSelect_Desig(item: any) {
    
    this.Uservalue = this.Uservalue.filter(m => m != item.designation_name);
    
    for (let i = 0; i < this.DesiginationList.length; i++) {

      if(this.DesiginationList[i].designation_name== item.designation_name)
      {
        this.designation_type=this.designation_type.filter(m => m !=this.DesiginationList[i].designation_type);
        this.designation_id=this.designation_id.filter(m => m != this.DesiginationList[i].designation_id);
        
      } 

      //this.designation_type = this.designation_type.filter(m => m != (this.getDropDownText_Desig(item.designation_name, this.DesiginationList)[0].designation_type));
     // this.designation_id = this.designation_id.filter(m => m != (this.getDropDownText_Desig(item.designation_name, this.DesiginationList)[0].designation_id));
    }
    
    
  }

  onDeSelectAll_Desig(item: any)
  {
    this.Uservalue=[];
    this.designation_type=[];
    this.designation_id=[];
    

  }




  getDropDownText_Desig(id, object) {
    
    const selObj = _.filter(object, function (o) {
      return (_.includes(id, o.designation_name));
    });
    return selObj;
  }

  submit_DesignationPermissions() {
    this.submitted = true;
    if (this.DesigaccessForm.invalid) {
      return false;
    }
    if (!this.Uservalue.length) {
      this.Errormsg = true;
      return false;
    }
    

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.Uservalue.toString();
    req.INPUT_02 = this.pageid;
    req.INPUT_03 = this.Desiff.iseditvalue.value == true ? "1" : null;
    req.INPUT_04 = this.Desiff.isviewvalue.value == true ? "1" : null;
    req.INPUT_05 = this.Desiff.isaddvalue.value == true ? "1" : null;
    req.INPUT_06 = this.Desiff.UserTypevalue.value;//this.designation_type.toString();
    req.INPUT_07 = this.designation_id.toString();

    req.INPUT_08 = "Designation_Permission";
    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE = "Web";

    this.service.postData(req, "SaveUserMobaccessDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Designation Access Details Submitted Successfully !!!", 'success');

          this.designationpermissionModal.hide();
          this.resetallforms();

        }
        else
          Swal.fire('warning', result.StatusMessage, 'warning');
        ;
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
    },
      error => console.log(error));

  }

  hidedesignationPermissionModal(): void {
    this.designationpermissionModal.hide();
  }

  update_DesignationPermissions(data)
  {   

    this.showloader = true;
    const req = new InputRequest();
   
    req.INPUT_01 = this.pageid;
    req.INPUT_02 = data.designation_name;    
    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE="Web";

    this.service.postData(req, "UpdateUserMobaccessDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Designation Access Details Updated Successfully !!!", 'success');

          //this.designationpermissionModal.hide();
          this.load_DesigPageAccesslist(this.pageid);

        }
        else
          Swal.fire('warning', result.StatusMessage, 'warning');
        ;
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
    },
      error => console.log(error));


  }


  history() {
    //this.title="Employee";
    this.historyModal.show();
  }
  hidehistoryModal(): void {
    this.historyModal.hide();
  }



}
