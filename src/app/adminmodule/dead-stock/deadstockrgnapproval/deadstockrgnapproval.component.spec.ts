import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeadstockrgnapprovalComponent } from './deadstockrgnapproval.component';

describe('DeadstockrgnapprovalComponent', () => {
  let component: DeadstockrgnapprovalComponent;
  let fixture: ComponentFixture<DeadstockrgnapprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeadstockrgnapprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeadstockrgnapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
