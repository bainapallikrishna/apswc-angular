import { Component, OnInit,ViewChild } from '@angular/core';
import{Router}from '@angular/router';
import Swal from 'sweetalert2';
import{InputRequest} from 'src/app/Interfaces/employee';
import{CommonServices} from 'src/app/Services/common.services';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { DSAPPButtonRendererComponent } from 'src/app/custome-directives/DSAPPbutton-renderer.component';

@Component({
  selector: 'app-deadstockrgnapproval',
  templateUrl: './deadstockrgnapproval.component.html',
  styleUrls: ['./deadstockrgnapproval.component.css']
})
export class DeadstockrgnapprovalComponent implements OnInit {

  loguser: string=sessionStorage.getItem("logUserCode");
loguserrole: string = sessionStorage.getItem("logUserrole");
ispwdchanged:string=sessionStorage.getItem("logUserisChnagePassword");
loguserid:string=sessionStorage.getItem("logUserworkLocationCode");
Worklocname:string=sessionStorage.getItem("logUserworkLocationName");
workLocationCode:string=sessionStorage.getItem("logUserworkLocationCode");
public rowseledted: object;
sections:any;
Designationlist:any;
columnDefs = [
  { headerName: '#', width: 60, cellRenderer: 'rowIdRenderer' },
  
  {
    headerName: 'Request Id',width: 200,field: 'form_id',onCellClicked: this.makeCellClicked.bind(this),
    cellRenderer: function (params) {
      return '<u style="color:blue">' + params.value + '</u>';
    },
  },
  { headerName: 'WareHouse Name', width: 200, field: 'warehouse_name', sortable: true, filter: true },
  { headerName: 'From Department', width: 200, field: 'applicatioN_FORWARD', sortable: true, filter: true },
  { headerName: 'Requested Date', width: 200, field: 'submitted_date', sortable: true, filter: true },
  
  {
    headerName: 'Action',field: 'status', cellRenderer: 'buttonRenderer',width:600,
    cellRendererParams: {
      ApproveClick: this.Approve.bind(this),
      ViewClick: this.History.bind(this),
    },
  }
];

frameworkComponents: any;
public components;
gridApi: any;
submitbutton:boolean=false;
gridColumnApi: any;
DeadStockList: any[];
showloader:boolean=false;
Formaddarray: any [];
progressbar:boolean=false;
message: string = "";
progress: number = 0;
submittedrsn:boolean=false;
cancelrcd : any;
Image: any ;
Document:any;
forwardeddivs:boolean=true;

  constructor(private router:Router,private service:CommonServices,private fb:FormBuilder) { 
    
    if (!this.loguserrole || this.ispwdchanged == '0' ) {
      this.router.navigate(['/Login']);
      return;
    }

    this.frameworkComponents = {
      buttonRenderer: DSAPPButtonRendererComponent,
    }
    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }
  @ViewChild('ReasonModal') public ReasonModal: ModalDirective;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('DsApprovalModal') public DsApprovalModal: ModalDirective;
  @ViewChild('historyempModal') public historyempModal: ModalDirective;
  @ViewChild('mrPreviewModal') public mrPreviewModal: ModalDirective;
  
  ngOnInit(): void {
    this.LoadDeadStockList();
    this.LoadSections();
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.DeadStockList);
  }


  DeadstockpreviewForm=this.fb.group({
photo:[''],
document:[''],
SactAmnt:[''],
ApprovalRegDoc:[''],
ApprovalDocpath:['']

  })

  base64sstringpath:any;
 
certificates(val)
{

if(val=="1")
{
  for(var k=0;k<=this.imagelis.length-1;k++){
    var newTab = window.open();
    newTab.document.body.innerHTML = '<img src=' + this.imagelis[k] + ' width="100%" height="100%">';
}
}
if(val=="2")
{
this.base64sstringpath= this.DeadstockpreviewForm.controls.document.value;
this.decryptFile(this.base64sstringpath);
}

  }
  imagelis=[];
  decryptFile(imagepth:string) {
    if (imagepth) {
      //this.pdf="wwwroot\\MedicalReimbursementRequest\\14-06-2021\\sample_14_06_2021_10_49_59.pdf";
         this.service.decryptFile(imagepth, "DecryptFile")
           .subscribe(data => {
             var ext =  imagepth.split('.').pop();
             var filename = imagepth.replace(/^.*[\\\/]/, '');
        

      if(ext=="pdf")
     
      {
        this.downloadPdf(data.docBase64,filename);
       }
       else{
    // var newTab = window.open();
    //  newTab.document.body.innerHTML ='<img src='+data.docBase64+' width="100%" height="100%">'; 
    this.imagelis.push(data.docBase64);
       }
       
           },
             error => {
               console.log(error);
             });
       }
       else{
   
       }
       }
  
       downloadPdf(base64String, fileName) {
        const source = base64String;//`data:application/pdf;base64,${base64String}`;
        const link = document.createElement("a");
        link.href = source;
        link.download = `${fileName}.pdf`
        link.click();
      }
 
 
 
      reqid:any;Sactamt:any;Uploadby:any;bankname:any;  bankbranch:any; whname:any;Itemtype:any;noofunits:any;amount:any;ttamount:any;remarks:any;bankholname:any;accno:any;ifsc:any;
  
      hidepreviewTaxModal()
      {
       this.imagelis=[];
       this.mrPreviewModal.hide();
      }
      approvalstatus:any;
    SubmitApprovalDetails()
      {
    
        if(this.DeadstockpreviewForm.controls.SactAmnt.value==""||this.DeadstockpreviewForm.controls.SactAmnt.value==null||this.DeadstockpreviewForm.controls.SactAmnt.value==undefined){
            Swal.fire("info","Please Enter Sanctioned Amount","info");
          return false;
             }
          
          if(this.DeadstockpreviewForm.controls.ApprovalDocpath.value==""||this.DeadstockpreviewForm.controls.ApprovalDocpath.value==null||this.DeadstockpreviewForm.controls.ApprovalDocpath.value==undefined){
          
            Swal.fire("info","Please Upload Approval Note","info");
            return false;
           }
          if(parseFloat(this.DeadstockpreviewForm.controls.SactAmnt.value)>parseFloat (this.ttamount)){
            Swal.fire("info","Requested Amount Lessthan Sanctioned Amount,Please Enter Valid Amount","info");
            return false; 
          }
      const req=new InputRequest();
      this.showloader=true;
      
     req.INPUT_01=this.reqid;
     req.INPUT_02=this.DeadstockpreviewForm.controls.SactAmnt.value;
     req.INPUT_03= this.DeadstockpreviewForm.controls.ApprovalDocpath.value;
     req.INPUT_04= null;
     req.USER_NAME=this.loguser;
     req.INPUT_05=this.Worklocname;
     req.CALL_SOURCE="WEB";
    this.service.postData(req, "SaveServicereqApprovalDetails").subscribe(data => {
    this.empaddarray=data;
    if (this.empaddarray.StatusCode == "100") 
    {
    
      this.showloader=false;
      Swal.fire("success","Details Submitted Successfully","success");
      this.reloadCurrentRoute();
      }
      else{
          this.showloader= false;
          Swal.fire("info",data.StatusMessage,"info");
          this.reloadCurrentRoute();
        } 
    },
    
    error => console.log(error)
    );
    
    }
    

approvedstat:any;
  makeCellClicked(event) 
  {
    
    //for approval doc sec
this.approvalstatus=event.node.data.approveD_STATUS;
this.Uploadby=event.node.data.approved_by;
this.Sactamt=event.node.data.approveD_AMOUNT

       if(this.Worklocname=="M & QC Section"){
 
      if(this.approvalstatus==1||this.approvalstatus==2)
      {
        myInstance=[];
        this.imagelis=[];
        this.rowseledted={}; 
      this.rowseledted = event.data;
        this.whname=event.node.data.suB_ITEM_NAME;
        this.Itemtype=event.node.data.iteM_type_NAME;
        this.noofunits =event.node.data.no_of_units;
        this.amount=event.node.data.amount_per_unit;
        this.ttamount=event.node.data.chargeS_AMOUNT;
        this.remarks=event.node.data.remarks;
        this.DeadstockpreviewForm.controls.photo.setValue(event.node.data.imagE1_PATH);
        this.base64sstringpath=event.node.data.imagE1_PATH;
        var myInstance:string[]=this.base64sstringpath.split(',');
        for(var st=0;st<=myInstance.length-1;st++){
          this.decryptFile(myInstance[st]);
          }
        this.DeadstockpreviewForm.controls.document.setValue(event.node.data.documenT_PATH);
        this.bankholname=event.node.data.accounT_HOLDER_NAME;
        this.accno=event.node.data.banK_ACCOUNT_NO;
        this.ifsc=event.node.data.ifsC_CODE
        this.bankbranch=event.node.data.banK_BRACH_NAME;
        this.bankname=event.node.data.banK_NAME;
        this.reqid=event.node.data.form_id;
        this.mrPreviewModal.show();
         this.DeadstockpreviewForm.controls['SactAmnt'].disable();
         this.DeadstockpreviewForm.controls['ApprovalRegDoc'].disable();
         this.submitbutton=false;
         return false;
      }
      else{
        myInstance=[];
        this.imagelis=[];
      this.rowseledted={};    
        this.rowseledted = event.data;
        this.whname=event.node.data.suB_ITEM_NAME;
        this.Itemtype=event.node.data.iteM_type_NAME;
        this.noofunits =event.node.data.no_of_units;
        this.amount=event.node.data.amount_per_unit;
        this.ttamount=event.node.data.chargeS_AMOUNT;
        this.remarks=event.node.data.remarks;
        this.DeadstockpreviewForm.controls.photo.setValue(event.node.data.imagE1_PATH);
        this.base64sstringpath=event.node.data.imagE1_PATH;
        var myInstance:string[]=this.base64sstringpath.split(',');
        for(var st=0;st<=myInstance.length-1;st++){
          this.decryptFile(myInstance[st]);
          }
       
        this.DeadstockpreviewForm.controls.document.setValue(event.node.data.documenT_PATH);
        this.bankholname=event.node.data.accounT_HOLDER_NAME;
        this.accno=event.node.data.banK_ACCOUNT_NO;
        this.ifsc=event.node.data.ifsC_CODE
        this.bankbranch=event.node.data.banK_BRACH_NAME;
        this.bankname=event.node.data.banK_NAME;
        this.reqid=event.node.data.form_id;
        this.showloader=false;
          this.mrPreviewModal.show();
          this.submitbutton=true;
          this.DeadstockpreviewForm.controls['SactAmnt'].enable();
          this.DeadstockpreviewForm.controls['ApprovalRegDoc'].enable();
      return false;
      }
    
    }
  else{
    myInstance=[]
    this.imagelis=[];
    this.rowseledted={};    
    this.rowseledted = event.data;
    this.whname=event.node.data.suB_ITEM_NAME;
    this.Itemtype=event.node.data.iteM_type_NAME;
    this.noofunits =event.node.data.no_of_units;
    this.amount=event.node.data.amount_per_unit;
    this.ttamount=event.node.data.chargeS_AMOUNT;
    this.remarks=event.node.data.remarks;
    this.DeadstockpreviewForm.controls.photo.setValue(event.node.data.imagE1_PATH);
    this.base64sstringpath=event.node.data.imagE1_PATH;
    var myInstance:string[]=this.base64sstringpath.split(',');
    for(var st=0;st<=myInstance.length-1;st++){
      this.decryptFile(myInstance[st]);
      }
   
    this.DeadstockpreviewForm.controls.document.setValue(event.node.data.documenT_PATH);
    this.bankholname=event.node.data.accounT_HOLDER_NAME;
    this.accno=event.node.data.banK_ACCOUNT_NO;
    this.ifsc=event.node.data.ifsC_CODE
    this.bankbranch=event.node.data.banK_BRACH_NAME;
    this.bankname=event.node.data.banK_NAME;
    this.reqid=event.node.data.form_id;
    this.submitbutton=false;
    this.showloader=false;
    this.mrPreviewModal.show();
    return false;

  }
    
    }


    Applicartionidlist:any;
 

       Sumbitfun(){

       }

  viewPDF(input): void {
    let pdfWindow = window.open('', '_blank')
    pdfWindow.document.write(`<iframe width='100%' height='100%' src='${encodeURI(input)}'></iframe>`)
  }

  requestid:any;historylist:any;
 
  History(ROW):void{
    this.requestid=ROW.rowData.form_id;
     this.showloader = true;
    // const data = row.rowData;
     const req = new InputRequest();
 this.showloader = true;
    req.INPUT_01=this.requestid;
    req.TYPEID ="208";
     this.service.postData(req, "GetDeadstockhistory1").subscribe(data => {
       this.showloader = false;
       if (data.StatusCode == "100") {
         this.historyempModal.show();
        this.historylist = data.Details;
        }
       else {
         this.historylist="";
         this.showloader = false;
         Swal.fire("info","No History Found","info");
       }
     },
       error => console.log(error));
   }


   hidehistorymasterModal(){
    this.historylist =[];
 this.historyempModal.hide();
     }


  Approve(ROW){
   
    this.requestid=ROW.rowData.form_id;
    this.DsApprovalModal.show();
       // Swal.fire({
        //   title: 'Are you sure?Do you want to approve this record?',
        //   showCancelButton: true,
        //   confirmButtonText: `Save`,
        // }).then((result) => {
        //   if (result.isConfirmed) {
        //     this.ApproveAPI(CancelRcd);
        //   } else {
        //     Swal.fire('info', 'Changes are not saved', 'info')
        //   }
        // })
      }




  empaddarray:any;
  submit()
  {

    this.showloader=true;
      if(this.DeadstockApprovalform.controls.dsApproverej.value==""||this.DeadstockApprovalform.controls.dsApproverej.value==null||this.DeadstockApprovalform.controls.dsApproverej.value==undefined){
    Swal.fire("info","Please Select Approve/Reject","info");
    this.showloader=false;
    return;
       }
if(this.DeadstockApprovalform.controls.dsApproverej.value=="1")
       {
if(this.DeadstockApprovalform.controls.dsSectionName.value==""||this.DeadstockApprovalform.controls.dsSectionName.value==null||this.DeadstockApprovalform.controls.dsSectionName.value==undefined){
          Swal.fire("info","Please select Section Name","info");
          this.showloader=false;
          return;
             }
if(this.DeadstockApprovalform.controls.dsoffremarks.value==""||this.DeadstockApprovalform.controls.dsoffremarks.value==null||this.DeadstockApprovalform.controls.dsoffremarks.value==undefined){
              Swal.fire("info","Please Enter Remarks","info");
              this.showloader=false;
              return;
                 }
          
          
if(this.DeadstockApprovalform.controls.dsOffName.value==""||this.DeadstockApprovalform.controls.dsOffName.value==null||this.DeadstockApprovalform.controls.dsOffName.value==undefined){
                      Swal.fire("info","Please Select OfficerName","info");
                      this.showloader=false;
                      return;
                         }
                         
if(this.DeadstockApprovalform.controls.dsDesigName.value==""||this.DeadstockApprovalform.controls.dsDesigName.value==null||this.DeadstockApprovalform.controls.dsDesigName.value==undefined){
                  Swal.fire("info","Please Select Designation","info");
                  this.showloader=false;
                  return;
                     }
                    }

if(this.DeadstockApprovalform.controls.dsApproverej.value=="0")
{
              if(this.DeadstockApprovalform.controls.dsoffremarks.value==""||this.DeadstockApprovalform.controls.dsoffremarks.value==null||this.DeadstockApprovalform.controls.dsoffremarks.value==undefined){
              Swal.fire("info","Please Enter Remarks","info");
              this.showloader=false;
              return;
                 }
  }if(this.DeadstockApprovalform.controls.dsApproverej.value=="1"){
       const req=new InputRequest();

    this.showloader=true;
    req.TYPEID="206";
   req.INPUT_01=this.requestid;
   req.INPUT_02=this.DeadstockApprovalform.controls.dsSectionName.value;
   req.INPUT_04=this.DeadstockApprovalform.controls.dsoffremarks.value;
   req.INPUT_05=this.DeadstockApprovalform.controls.dsOffName.value;
   req.INPUT_06=this.DeadstockApprovalform.controls.dsDesigName.value;
   req.USER_NAME=this.loguser;
  req.CALL_SOURCE="WEB";
 
this.service.postData(req, "SaveDeadstockApproveDetails").subscribe(data => {
  this.empaddarray=data;
  if (this.empaddarray.StatusCode == "100") 
  {
    
    Swal.fire("success", "Request Forwarded to "+this.empaddarray.Details[0].officeR_NAME, "success");
    this.DsApprovalModal.hide();
    this.showloader=false;
    //this.addModal.hide();
  
 this.reloadCurrentRoute();
    }
    else{
        this.showloader= false;
        Swal.fire("info",data.StatusMessage,"info");
      } 

    },
    error => console.log(error));
    }
    if(this.DeadstockApprovalform.controls.dsApproverej.value=="0"){
    const req=new InputRequest();

    this.showloader=true;
    req.TYPEID="207";
   req.INPUT_01=this.requestid;
   req.INPUT_02=this.Worklocname;
   req.INPUT_03=this.DeadstockApprovalform.controls.dsoffremarks.value;
   req.USER_NAME=this.loguser;
   req.CALL_SOURCE="WEB";
 
this.service.postData(req, "updateDeadstockApproveDetails").subscribe(data => {
  this.empaddarray=data;
  if (this.empaddarray.StatusCode == "100") 
  {
    
    this.showloader=false;

    this.DsApprovalModal.hide();
    Swal.fire("success","Rejected Successfully","success");
  
 this.reloadCurrentRoute();
    }
    else{
        this.showloader= false;
        Swal.fire("info",data.StatusMessage,"info");
      
      } 

    },
    error => console.log(error));
    

  }
  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }


  ApproveAPI(CancelRcd){
    const req = new InputRequest();
  
    req.INPUT_01 = "CANCEL_RECORD";
    req.INPUT_02 = "REGIONAL";
    req.INPUT_03=CancelRcd.rowData.form_id;
    req.INPUT_04=CancelRcd.rowData.wH_ID;
    req.INPUT_07=CancelRcd.rowData.region_code;
    req.INPUT_06="1";
    req.CALL_SOURCE="Web";
    req.USER_NAME=this.loguser;
  
    this.service.postData(req, "DeadStockInsertion").subscribe(data => {
      this.Formaddarray = data.result;
        if (data.StatusCode == 100) {
        this.showloader= false;  
        Swal.fire('success', 'Record Approved Successfully', 'success').then((result)=>{
          if (result.isConfirmed) {
            this.LoadDeadStockList();
          } 
          else if (result.isDenied) {
          }
        })
      }
      else{
        this.showloader= false;  
        Swal.fire("info",data.StatusMessage,"info");
      }
    });
  }

  Reject(CancelRcd){
    this.cancelrcd=CancelRcd;
    this.DeadStockRejectReasonForm.reset();
    this.ReasonModal.show();
  }

  hideReasonModal(){
    this.DeadStockRejectReasonForm.reset();
    this.ReasonModal.hide();
  }

  get subfrsn() { return this.DeadStockRejectReasonForm.controls; }

  DeadStockRejectReasonForm=this.fb.group({
    RejectReason: ['', Validators.required],
  });

  DeadstockApprovalform=this.fb.group({
    dsApproverej:[''],
    dsSectionName:[''],
    dsDesigName:[''],
    dsOffName:[''],
    dsoffremarks:['']

  });
  approverejectfun(apprejval){
    if(apprejval=="1"){
      this.sections=[];
      this.Designationlist=[];
      this.DeadstockApprovalform.controls.dsSectionName.setValue('');
      this.DeadstockApprovalform.controls.dsDesigName.setValue('');
      this.DeadstockApprovalform.controls.dsOffName.setValue('');
      this.LoadSections(); 
      this.forwardeddivs=true;
    }
    else{

      this.sections=[];
      this.Designationlist=[];
      this.DeadstockApprovalform.controls.dsSectionName.setValue('');
      this.DeadstockApprovalform.controls.dsDesigName.setValue('');
      this.DeadstockApprovalform.controls.dsOffName.setValue('');
      this.LoadSections(); 
      this.forwardeddivs=false;
  
    }
  }  sectionchange(){
    this.Designationlist=[];
    this.DeadstockApprovalform.controls.dsDesigName.setValue('');
    this.DeadstockApprovalform.controls.dsOffName.setValue('');
    this.getdesignations();
    //this.nameslist();
    
    }


    LoadSections() {
      this.service.getData("GetSections").subscribe(data => {
  
        if (data.StatusCode == "100") {
          this.sections = data.Details;
  
        }
  
      },
        error => console.log(error));
    }

getdesignations()
{

  this.showloader = true;
  const req=new InputRequest();
req.TYPEID="DD_DESIGNATION";
 req.INPUT_01=this.DeadstockApprovalform.controls.dsSectionName.value;
  this.service.postData(req, "GetFamilynames").subscribe(data => {
    this.showloader = false;
    if (data.StatusCode == "100") {

      this.Designationlist = data.Details;
          }
    else {
      this.showloader = false;
      Swal.fire("info",data.StatusMessage,"info");
      this.Designationlist="";
    }
  },
    error => console.log(error));

}




  RejectAPI()
  {
    this.submittedrsn=true;
    if(this.DeadStockRejectReasonForm.invalid){
      return false;
    }
    const req = new InputRequest();
  
    req.INPUT_01 = "CANCEL_RECORD";
    req.INPUT_02 = "REGIONAL";
    req.INPUT_03=this.cancelrcd.rowData.form_id;
    req.INPUT_04=this.cancelrcd.rowData.wH_ID;
    req.INPUT_05=this.subfrsn.RejectReason.value;
    req.INPUT_06="2";
    req.INPUT_07=this.cancelrcd.rowData.region_code;
    req.CALL_SOURCE="Web";
    req.USER_NAME=this.loguser;
  
    this.service.postData(req, "DeadStockInsertion").subscribe(data => {
      this.Formaddarray = data.result;
        if (data.StatusCode == 100) {
        this.showloader= false;  
        Swal.fire('success', 'Record Rejected Successfully', 'success').then((result)=>{
          if (result.isConfirmed) {
            this.LoadDeadStockList();
            this.hideReasonModal();
          } 
          else if (result.isDenied) {
          }
        })
      }
      else{
        this.showloader= false;  
        Swal.fire("info",data.StatusMessage,"info");
      }
    });
  }

  LoadDeadStockList() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_02 = this.Worklocname;
    req.TYPEID="210";
    this.DeadStockList=[];
    this.service.postData(req, "GetDeadstockhistory1").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.DeadStockList = data.Details;
        this.gridApi.setRowData(this.DeadStockList);
        console.log(this.DeadStockList);
      }
      else{
        this.gridApi.setRowData(this.DeadStockList);
        Swal.fire('warning', "Error Occured while Getting Data", 'warning');
      }
    },
      error => console.log(error));

  }
  CloseEditModal(){
    this.DeadstockApprovalform.reset();
    this.DsApprovalModal.hide();
  }

  ApprovaluploadFile(event)
{
  
  if (event.target.files && event.target.files[0]) {
    let filetype = event.target.files[0].type;
    let filesize = event.target.files[0].size;
    let doctype = "";
    if (event.target.files.length === 0) {
      return false;
    }
    if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
      Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
      return false;
    }

    if (filetype != 'image/jpeg' || filetype != 'image/png')
      doctype = 'IMAGE';
    else
      doctype = 'PDF';

    if (filesize > 2615705) {
      Swal.fire('info', 'File size must be upto 2 MB', 'info');
      this.progressbar=false;
      return false;
    }

    let fileToUpload = <File>event.target.files[0];
    const formData = new FormData();

    formData.append('file', fileToUpload, fileToUpload.name );
    formData.append('pagename', "ServicerequestApproval");

    this.service.encryptUploadFile(formData, "EncryptFileUpload")
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
    
          this.message = 'Upload success.'
          this.ceruploadFinished(event.body);
          //this.uploadFinished(this.regsdoc.tostring());
          this.progressbar=true;
        }

      });
  }
}
public ceruploadFinished = (event) => 
  {
    
      this.DeadstockpreviewForm.controls.ApprovalDocpath.setValue(event.fullPath);

    
  }
}
