import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnggmaintenanceComponent } from './enggmaintenance.component';

describe('EnggmaintenanceComponent', () => {
  let component: EnggmaintenanceComponent;
  let fixture: ComponentFixture<EnggmaintenanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnggmaintenanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnggmaintenanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
