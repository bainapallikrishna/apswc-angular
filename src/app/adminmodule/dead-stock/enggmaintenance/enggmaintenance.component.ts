import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormArray,FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { DSButtonRendererComponent } from 'src/app/custome-directives/DSbutton-renderer.component';
import { DomSanitizer } from '@angular/platform-browser';
import { analyzeAndValidateNgModules, TmplAstRecursiveVisitor } from '@angular/compiler';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { DigiServices } from 'src/app/Services/Digilocker.services';

@Component({

  
  selector: 'app-enggmaintenance',
  templateUrl: './enggmaintenance.component.html',
  styleUrls: ['./enggmaintenance.component.css']
})
export class EnggmaintenanceComponent implements OnInit {

  loguser: string = sessionStorage.getItem("logUserCode");
  loguserrole: string = sessionStorage.getItem("logUserrole");
  ispwdchanged: string = sessionStorage.getItem("logUserisChnagePassword");
  loguserid: string = sessionStorage.getItem("logUserworkLocationCode");



  columnDefs = [
    { headerName: '#', width: 60, cellRenderer: 'rowIdRenderer', floatingFilter: false, },
    { headerName: 'WareHouse Name', width: 200, field: 'warehouse_name' },
    { headerName: 'Sub Category Name', width: 200, field: 'suB_ITEM_NAME' },
    { headerName: 'Item Type', width: 200, field: 'iteM_type_NAME' },
    { headerName: 'No. of Units', width: 150, field: 'no_of_units' },
    { headerName: 'Amount / unit', width: 150, field: 'amount_per_unit' },
    { headerName: 'Total Amount', width: 150, field: 'chargeS_AMOUNT' },
    { headerName: 'Remarks', width: 200, field: 'remarks' },
    { headerName: 'Status', width: 200, field: 'forM_STATUS' },
    { headerName: 'Sanctioned Amount', width: 200, field: 'approveD_AMOUNT' },
    
    // { headerName: 'Rejected Remarks',  width: 300, field: 'rejecteD_REMARKS'  },
    // { headerName: 'Recent Action at Designation', width: 300, field: 'officeR_DESIGNATION'  },
    // { headerName: 'Recent Action at Name', width: 300, field: 'officeR_NAME'  },
    // {
    //   headerName: 'Action',field: 'ediT_STATUS', cellRenderer: 'buttonRenderer',
    //   cellRendererParams: {
    //     CancelClick: this.Cancel.bind(this),
  ];
  imglist: string[] = [];

  modeltitle: string = "Add";
  submitted: boolean = false;
  showloader: boolean = false;
  sectiondd: any[];
  categorydd: any[];
  SubCategorydd: any[];
  Itemdropdown: any[];
  public progress: number;
  public message: string;
  public message1: string;
  doc_change: boolean = false;
  doc_change1: boolean = false;
  response: { dbPath: '' };
  Base64Image: any;
  StockDOC: any;
  ImagePath: any;
  Formaddarray: any;
  progress1: any;
  response1: { dbPath: '' };
  gridApi: any;
  gridColumnApi: any;
  defaultColDef: any = [];
  frameworkComponents: any;
  public components;
  DeadStockList: any[];

  SectionID: any;
  code: any;
  Accesstoken: any;
  FilesDD: any;
  Docpath: any;
  Authstatus: any;
  UploadFilesDD: any;
  IssuedDocView: boolean = true;
  viewbtnname: any;
  uri: any;
  DigiDetails: any[];
  showDocpath: any;
  hrefdata: any;
  Details: any;
  name: any;
  dob: any;
  gender: any;
  preview: string = "";
  seldocpath: any;
  seldoctype: string = "";
  seldoccat: string = "";
  seldocpath2: any;
  seldoctype2: string = "";
  seldoccat2: string = "";

  constructor(private router: Router,
    private service: CommonServices,
    private sanitizer: DomSanitizer,
    private digiservice: DigiServices,
    private fb: FormBuilder) {

    if (!this.loguserrole || this.ispwdchanged == '0') {
      this.router.navigate(['/Login']);
      return;
    }
  
    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      wrapText: true,
      autoHeight: true,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.frameworkComponents = {
      buttonRenderer: DSButtonRendererComponent,
    }
    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }
  @ViewChild('addModal') public addModal: ModalDirective;

  @ViewChild('PreviewModal') public PreviewModal: ModalDirective;
  @ViewChild('mrPreviewModal') public mrPreviewModal: ModalDirective;
  @ViewChild('apprModal') public apprModal: ModalDirective;
  @ViewChild('historyempModal') public historyempModal: ModalDirective;
  @ViewChild('DetailList') public DetailList: ModalDirective;
  @ViewChild('FilesList') public FilesList: ModalDirective;
  @ViewChild('previewModal') public previewModal: ModalDirective;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  ngOnInit(): void {
    this.LoadDeadStockList();
    this.GetSubCategoryDropdown();
    this.GetItemDropdown();
  }
  base64sstringpath: any;;

  certificates(val) {
    if (val == "1") 
    {
      
      for(var k=0;k<=this.imagelis.length-1;k++)
      {
            var newTab = window.open();
            newTab.document.body.innerHTML = '<img src=' + this.imagelis[k] + ' width="100%" height="100%">';
      }
    }
    if (val == "2") {
      this.base64sstringpath = this.subf.StockDOC.value;
      this.decryptFile(this.base64sstringpath);
    }
}
  imagelis=[];
  decryptFile(imagepth: string) {
    if (imagepth) {
      //this.pdf="wwwroot\\MedicalReimbursementRequest\\14-06-2021\\sample_14_06_2021_10_49_59.pdf";
      this.service.decryptFile(imagepth, "DecryptFile")
        .subscribe(data => {
          var ext = imagepth.split('.').pop();
          var filename = imagepth.replace(/^.*[\\\/]/, '');

          if (ext == "pdf") {
            this.downloadPdf(data.docBase64, filename);
          }
          else {
            
            this.imagelis.push(data.docBase64);
           
            // var newTab = window.open();
            // newTab.document.body.innerHTML = '<img src=' + data.docBase64 + ' width="100%" height="100%">';
}

        },
          error => {
            console.log(error);
          });
    }
    else {

    }
  }

  downloadPdf(base64String, fileName) {

    // const blob = this.service.s_sd((base64String).split(',')[1], "application/pdf");
    //     const blobUrl = URL.createObjectURL(blob);    
    const source = base64String;//`data:application/pdf;base64,${base64String}`;
    const link = document.createElement("a");
    link.href = source;
    link.download = `${fileName}.pdf`
    link.click();
  }
    Cancel(CancelRcd) {

    Swal.fire({
      title: 'Are you sure?Do you want to cancel this record?',
      showCancelButton: true,
      confirmButtonText: `Save`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.CancelAPI(CancelRcd);
      } else {
        Swal.fire('info', 'Changes are not saved', 'info')
      }
    })
  }

  CancelAPI(CancelRcd) {
    const req = new InputRequest();

    req.INPUT_01 = "CANCEL_RECORD";
    req.INPUT_02 = "WH-EMPLOYE";
    req.INPUT_03 = CancelRcd.rowData.form_id;
    req.INPUT_04 = this.loguserid;
    req.INPUT_06 = "3";
    req.CALL_SOURCE = "Web";
    req.USER_NAME = this.loguser;

    this.service.postData(req, "DeadStockInsertion").subscribe(data => {
      this.Formaddarray = data.result;
      if (data.StatusCode == 100) {
        this.showloader = false;
        Swal.fire("success", "Record Cancelled Successfully", "success");
        this.LoadDeadStockList();
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    });
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.DeadStockList);
  }

  LoadDeadStockList() {

    this.showloader = true;
    const req = new InputRequest();

    req.INPUT_01 = "DEADSTOCK_LIST";
    req.INPUT_02 = this.loguserid;

    this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.DeadStockList = data.Details;
        this.gridApi.setRowData(this.DeadStockList);
        console.log(this.DeadStockList);
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
    },
      error => console.log(error));

  }

  GetSectionDropdown() {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = "SECTION_DD";
    this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.sectiondd = data.Details;
        this.showloader = false;
      }
      else {
        this.showloader = false;

        Swal.fire("info", data.StatusMessage, "info");
      }

    },
      error => console.log(error));

  }

  GetCategoryDropdown() {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = "CATEGORY_DD";
    this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.categorydd = data.Details;
        this.showloader = false;
        console.log(this.categorydd);
      }
      else {
        this.showloader = false;

        Swal.fire("info", data.StatusMessage, "info");
      }

    },
      error => console.log(error));

  }

  GetSubCategoryDropdown() {
    this.showloader = true;
    this.subf.AddSubCategory.setValue("");
    const req = new InputRequest();
    req.INPUT_01 = "SUBCATEGORY_DD";
    this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.SubCategorydd = data.Details;
        this.showloader = false;
        // this.SectionID=data.Details[0]["wM_SECTION"];
        console.log(this.SubCategorydd);
      }
      else {
        this.showloader = false;

        Swal.fire("info", data.StatusMessage, "info");
      }

    },
      error => console.log(error));

  }

  Getsectionofficercode() {

    this.Itemdropdown = [];

    this.progress = null;
    this.message = null

    this.progress1 = null;
    this.message1 = null
    this.subf.AddItem.setValue('');
    this.subf.AddNoOfUnits.setValue('')
    this.subf.AddAmountperUnit.setValue('');
    this.subf.AddTotalAmount.setValue('');

    this.subf.ImagePath.setValue('');
    this.subf.StockDOC.setValue('');
    this.subf.Base64Image.setValue('');

    this.subf.SectionID.setValue('');
    this.subf.maincategory.setValue('');
    this.GetItemDropdown();

    for (var i = 0; i <= this.SubCategorydd.length - 1; i++) {

      if (this.EnggMaintenanceAddForm.controls.AddSubCategory.value == this.SubCategorydd[i].sub_item_id) {
        this.EnggMaintenanceAddForm.controls.SectionID.setValue(this.SubCategorydd[i].wM_SECTION);
        this.EnggMaintenanceAddForm.controls.maincategory.setValue(this.SubCategorydd[i].main_item_id);

        return;
      }
      else {

      }
    }
  }

  GetItemDropdown() {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = "ITEM_DD";
    this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Itemdropdown = data.Details;
        this.showloader = false;
      }
      else {
        this.showloader = false;

        Swal.fire("info", data.StatusMessage, "info");
      }

    },
      error => console.log(error));

  }


  get subf() { return this.EnggMaintenanceAddForm.controls; }

  EmployeepreviewrequestForm = this.fb.group({

  });


  EnggMaintenanceAddForm = this.fb.group({
    //SectionName: ['', Validators.required],
    //AddCategory: ['', Validators.required],
    AddSubCategory: ['', Validators.required],
    AddItem: ['', Validators.required],
    AddNoOfUnits: ['', Validators.required],
    AddAmountperUnit: ['', Validators.required],
    AddTotalAmount: ['', Validators.required],
    AddRemarks: ['', Validators.required],
    Base64Image: [''],
    StockDOC: [''],
    ImagePath: [''],
    imagep:[''],
    Editactive: [''],
    EditReason: [''],
    SectionID: [''],
    maincategory: [''],
    Bankholdername: [''],
    Accno: [''],
    Ifsccode: [''],
    Bankname: [''],
    Branchname: [''],
    check: [''],
    images: new FormArray([
      this.fb.group({
        ImagePath: [''],
        ImageType: [''],
        ImagCategory: [''],
        ImageBase64: [''],
        PrevPath: [''],
      }),
    ]),
  });
  get asset() {
    return this.EnggMaintenanceAddForm.controls;
  }

  get im() {
    return this.asset.images as FormArray;
  }


  AddImage() {
    this.im.push(
      this.fb.group({
        ImagePath: ['', Validators.required],
        ImageType: [''],
        ImagCategory: [''],
        ImageBase64: [''],
        PrevPath: [''],
      })
    );
  }

  RemoveImage(index) {
    this.im.removeAt(index);
  }
  AddDeadStock() {
    this.ImageDocClear();
    this.EnggMaintenanceAddForm.reset();
    
    this.modeltitle = "Add";
    this.Getbankaccountdetails();
    this.im.clear();
    this.im.reset();
    this.AddImage();
    this.submitted = false;
  }


  Bankaccountdetails: any;

  Getbankaccountdetails() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.loguser;
    this.service.postData(req, "GetBankdetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Bankaccountdetails = data.Details;

        this.EnggMaintenanceAddForm.controls.Bankholdername.setValue(this.Bankaccountdetails[0].accounT_HOLDER_NAME);

        this.EnggMaintenanceAddForm.controls.Accno.setValue(this.Bankaccountdetails[0].accounT_NUMBER);

        this.EnggMaintenanceAddForm.controls.Branchname.setValue(this.Bankaccountdetails[0].banK_BRANCH);


        this.EnggMaintenanceAddForm.controls.Bankname.setValue(this.Bankaccountdetails[0].banK_NAME);


        this.EnggMaintenanceAddForm.controls.Ifsccode.setValue(this.Bankaccountdetails[0].ifsC_CODE);
        this.addModal.show();

      }
      else {
        this.showloader = false;
        Swal.fire("info", "Please Update the bank Details in Employee Details Portal ", "info");
        this.addModal.hide();
      }
    },
      error => console.log(error));


  }



  Sumbitfun() {
    this.apprModal.show();
  }

  OKclick() {



    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "FORM_SUBMISSION";
    req.INPUT_02 = this.subf.AddSubCategory.value;
    req.INPUT_03 = this.subf.AddItem.value;
    req.INPUT_04 = this.subf.AddTotalAmount.value;
    req.INPUT_05 = this.subf.AddRemarks.value;
    req.INPUT_06 = this.imglist.join(',');
    
      req.INPUT_08 = this.subf.StockDOC.value;
    
    req.INPUT_09 = this.subf.AddNoOfUnits.value;
    req.INPUT_10 = this.subf.AddAmountperUnit.value;
    req.INPUT_11 = this.loguserid;
    req.INPUT_12 = this.subf.maincategory.value;
    //  this.subf.AddCategory.value;
    req.INPUT_13 = this.subf.SectionID.value;
    req.INPUT_16 = this.subf.Accno.value;
    req.INPUT_17 = this.subf.Ifsccode.value;
    req.INPUT_20 = this.subf.Bankholdername.value;
    req.INPUT_19 = this.subf.Bankname.value;
    req.INPUT_18 = this.subf.Branchname.value;
    req.CALL_SOURCE = "Web";
    req.USER_NAME = this.loguser;
    req.INPUT_14 = this.loguserrole;
    this.service.postData(req, "DeadStockInsertion").subscribe(data => {
      this.Formaddarray = data.Details;
      if (data.StatusCode == 100) {
        this.showloader = false;
        this.EnggMaintenanceAddForm.reset();
        Swal.fire("success", "Request Forwarded to "+this.Formaddarray[0].officeR_NAME, "success");
        //this.addModal.hide();
        //this.LoadDeadStockList();
        //this.ImageDocClear();
        this.reloadCurrentRoute();

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    });


  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  Cancelclick() {
    this.reloadCurrentRoute();
  }

  Submitcchange() {

  }

  hidepreviewTaxModal() {
    this.imagelis=[];
    this.EmployeepreviewrequestForm.reset();
    this.mrPreviewModal.hide();
  }
  hidemesgModal() {
    this.EnggMaintenanceAddForm.reset();
    this.apprModal.hide();
  }
  bankholname: any; ifsc: any; accno: any; bankname: any; bankbranch: any; dedsubcat: any; itemtpe: any; nofunit: any; amntprunit: any; ttlamnt: any; remak: any;
  sersubcat:any;itemt:any;
  
  
  
  Previewfunction() {

   // this.submitted = true;
   if(this.EnggMaintenanceAddForm.controls.AddSubCategory.value==""||this.EnggMaintenanceAddForm.controls.AddSubCategory.value==undefined||this.EnggMaintenanceAddForm.controls.AddSubCategory.value==null){
    Swal.fire("info", "Please select Subcategory ", "info");
    this.showloader = false;
    return false;
  
  }
  if(this.EnggMaintenanceAddForm.controls.AddItem.value==""||this.EnggMaintenanceAddForm.controls.AddItem.value==null||this.EnggMaintenanceAddForm.controls.AddItem.value==undefined){
    Swal.fire("info", "Please Select itemtype ", "info");
    this.showloader = false;
    return false;
  
  } if(this.EnggMaintenanceAddForm.controls.AddNoOfUnits.value==""||this.EnggMaintenanceAddForm.controls.AddNoOfUnits.value==undefined||this.EnggMaintenanceAddForm.controls.AddNoOfUnits.value==null){
    Swal.fire("info", "Please enter no of units ", "info");
    this.showloader = false;
    return false;
  
  } if(this.EnggMaintenanceAddForm.controls.AddAmountperUnit.value==""||this.EnggMaintenanceAddForm.controls.AddAmountperUnit.value==null||this.EnggMaintenanceAddForm.controls.AddAmountperUnit.value==undefined){
    Swal.fire("info", "Please Enter Amount/Unit ", "info");
    this.showloader = false;
    return false;
  
  } if(this.EnggMaintenanceAddForm.controls.AddRemarks.value==""||this.EnggMaintenanceAddForm.controls.AddRemarks.value==undefined||this.EnggMaintenanceAddForm.controls.AddRemarks.value==null){
    Swal.fire("info", "Please Enter Remarks", "info");
    this.showloader = false;
    return false;
  
  } if(!this.EnggMaintenanceAddForm.controls.StockDOC.value){
    Swal.fire("info", "Please Upload Document ", "info");
    this.showloader = false;
    return false;
  
  }

    if(!this.EnggMaintenanceAddForm.controls.StockDOC.value){
      Swal.fire("info", "Please Upload Document ", "info");
      this.showloader = false;
      return false;
    
    }
    this.imglist = [];
    for (let JK = 0; JK < this.im.length; JK++) {
     if (!this.im.value[JK].ImagePath) {
       Swal.fire('info', 'Please Upload Image', 'info');
       return false;
     }
    this.imglist.push(this.im.value[JK].ImagePath);
   }
 // var myInstance:string[]=this.base64sstringpath.split(',');
for(var st=0;st<=this.imglist.length-1;st++){
 this.decryptFile(this.imglist[st]);
 }


    if (this.EnggMaintenanceAddForm.invalid) {
      return false;
    }

if(!this.EnggMaintenanceAddForm.controls.StockDOC.value){
  Swal.fire("info", "Please Upload Document ", "info");
  this.showloader = false;
  return false;

}

 if (this.EnggMaintenanceAddForm.controls.check.value == "" || this.EnggMaintenanceAddForm.controls.check.value == undefined || this.EnggMaintenanceAddForm.controls.check.value == null) {
      Swal.fire("info", "Please Select checkbox", "info");
      this.showloader = false;
      return false;
  }
   
 
for(var i=0;i<=this.SubCategorydd.length-1;i++){
 if(this.subf.AddSubCategory.value==this.SubCategorydd[i].sub_item_id)
{
  this.dedsubcat=this.SubCategorydd[i].sub_item_name;
  break;
}

  }
for(var j=0;j<=this.Itemdropdown.length-1;j++){
if(this.subf.AddItem.value==this.Itemdropdown[j].item_id){
  this.itemtpe=this.Itemdropdown[j].item_name;
break;
}
  
}

//this.decryptFile(path);

  
    this.sersubcat =this.dedsubcat;
    this.itemt = this.itemtpe;
    this.nofunit = this.subf.AddNoOfUnits.value;
    this.amntprunit = this.subf.AddAmountperUnit.value;
    this.ttlamnt = this.subf.AddTotalAmount.value;
    this.remak = this.subf.AddRemarks.value;
    this.bankholname = this.subf.Bankholdername.value;
    this.accno = this.subf.Accno.value;
    this.ifsc = this.subf.Ifsccode.value;
    this.bankname = this.subf.Bankname.value;
    this.bankbranch = this.subf.Branchname.value;
    this.mrPreviewModal.show();
  }
  ImageDocClear() {
    this.message = "";
    this.message1 = "";
    this.progress = null;
    this.progress1 = null;

    this.subf.AddSubCategory.setValue("");
    this.subf.AddItem.setValue("");
    this.subf.AddTotalAmount.setValue("");
    this.subf.AddRemarks.setValue("");
    this.subf.AddNoOfUnits.setValue("");
    this.subf.AddAmountperUnit.setValue("");
    this.subf.AddNoOfUnits.setValue("");
    this.subf.maincategory.setValue("");
  }

  hideaddModal() {
    this.EnggMaintenanceAddForm.reset();
    this.modeltitle = "Add";
    this.addModal.hide();
    this.ImageDocClear();
  }
  imgesarr = [];
  uploadImage(event, index) {
    this.showloader = true;
    let imgindex = index;
    this.seldocpath = '';
    this.seldoctype = '';
    for (var i = 0; i < this.imgesarr.length - 1; i++) {}
    let url: string;
    if (event.target.files && event.target.files[0]) {
      let imagetype = event.target.files[0].type;
      let imagesize = event.target.files[0].size;

      if (imagetype != 'image/jpeg' && imagetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png files only', 'info');
        this.EnggMaintenanceAddForm.controls.imagep.setValue('');
        this.showloader = false;
        return false;
      }

      if (imagesize > 2097152) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        this.EnggMaintenanceAddForm.controls.imagep.setValue('');
        this.showloader = false;
        return false;
      }

      this.seldoccat = imagetype;
      if (imagetype == 'image/jpeg' || imagetype == 'image/png')
        this.seldoctype = 'IMAGE';
      else this.seldoctype = 'PDF';

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype == 'PDF') {
          const result = reader.result as string;
          const blob = this.service.s_sd(result.split(',')[1], this.seldoccat);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
          this.im.controls[imgindex].patchValue({
            ImageBase64: this.seldocpath,
            ImageType : this.seldoctype,
            PrevPath : this.seldocpath,
          });
        } else {
          this.seldocpath = reader.result as string;
          this.preview = this.seldocpath; 
          this.im.controls[imgindex].patchValue({
            ImageBase64: this.seldocpath,
            ImageType : this.seldoctype,
            PrevPath : this.seldocpath,
          });
        }
      };
      reader.readAsDataURL(<File>event.target.files[0]);

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', 'AssetManagement');

      this.service
        .encryptUploadFile(formData, 'EncryptFileUpload')
        .subscribe((event) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress1 = Math.round((100 * event.loaded) / event.total);
          } else if (event.type === HttpEventType.Response) {
            this.message1 = 'Upload success.';
            this.im.controls[imgindex].patchValue({
              ImagePath: event.body.fullPath
            });

            Swal.fire('success', 'Uploaded Successfully', 'success');
            this.showloader=false;
          }
        });
    }
  }

  uploadFile(event) {
    this.Authstatus = "";
    this.seldocpath2 = "" ;
    this.seldoctype2 = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;

      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF') {
        Swal.fire('info', 'Please upload pdf files only', 'info');
        this.EnggMaintenanceAddForm.controls.StockDOC.setValue('');
        return false;
      }

      if (filesize > 10615705) {
        Swal.fire('info', 'File size must be upto 10 MB', 'info');
        this.EnggMaintenanceAddForm.controls.StockDOC.setValue('');
        return false;
      }

      this.seldoccat2 = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype2 = 'IMAGE';
      else
        this.seldoctype2 = 'PDF';

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype2 == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat2);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath2 = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath2 = reader.result as string;
          this.preview = this.seldocpath2;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', "DeadStockformsubmission");

      this.service.UploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {

          if (event.type === HttpEventType.UploadProgress)
            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.doc_change = true;
            this.message = 'Upload success.';
            this.uploadFinished(event.body, "DS_DOC");


          }

        });


    }
  };

  public uploadFinished = (event, doctype) => {
    if (doctype == 'DS_PHOTO')
      this.subf.ImagePath.setValue(event.fullPath);


    if (doctype == 'DS_DOC')
      this.subf.StockDOC.setValue(event.fullPath);

  }


  CalcTotAmnt(a) {
    this.subf.AddTotalAmount.setValue(this.subf.AddNoOfUnits.value * this.subf.AddAmountperUnit.value);
  }


  hidehistorymasterModal() {
    this.historylist = [];
    this.historyempModal.hide();
  }
  historylist; any;



  DSHistory(): void {

    this.showloader = true;
    // const data = row.rowData;
    const req = new InputRequest();
    this.showloader = true;
    req.INPUT_01 = this.loguser;

    req.TYPEID = "209";
    this.service.postData(req, "GetDeadstockhistory1").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.historyempModal.show();
        this.historylist = data.Details;
      }
      else {
        this.historylist = "";
        this.showloader = false;
        Swal.fire("info", "No History Found", "info");
      }
    },
      error => console.log(error));
  }





  CallDigilock() {
    if (!this.FilesDD && !this.UploadFilesDD) {
      this.showloader = true;
      var pagename = "enggmaintenance";
      //this.digiservice.Digilock(pagename,(function(a) {
      this.digiservice.Digilock(pagename, (a, b) => {
        this.Accesstoken = b;
        this.FilesDD = a.UserFiles.items;
        if (a.UserUploadFiles)
          this.UploadFilesDD = a.UserUploadFiles.items;
        this.FilesList.show();
        this.showloader = false;
      });
    }
    else {
      this.FilesList.show();
    }
  }

  ShowFiles(val, mimer, num, name) {
    this.showloader = true;
    this.digiservice.ShowFiles(val, mimer, num, name, this.Accesstoken, (a, b) => {
      this.Docpath = a;
      this.subf.StockDOC.setValue(a);
      this.decryptFile1(this.Docpath);
      this.uri = b;
      this.Authstatus = "1";
      this.viewbtnname = name.substring(0, 5);
      this.FilesList.hide();
      this.showloader = false;
    });
  }

decryptFile1(filepath) {
  if (filepath) {
    this.service.decryptFile(filepath, "DecryptFile")
      .subscribe(data => {
        let blob : any;
        console.log(data.docBase64);

        if(data.docBase64.includes('application/pdf'))
        {
         blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
        
        }
        else if(data.docBase64.includes('image/png'))
        {
           blob = this.service.s_sd((data.docBase64).split(',')[1], "image/png");
           
        }
        else if(data.docBase64.includes('image/jpg') || data.docBase64.includes('image/jpeg') )
        {
           blob = this.service.s_sd((data.docBase64).split(',')[1], "image/jpg");
        }
        const blobUrl = URL.createObjectURL(blob);  
        this.hrefdata=this.sanitizer.bypassSecurityTrustUrl(blobUrl);
 

      },
        error => {
          console.log(error);
          this.showDocpath = "";
        });
  }
  else
  this.showDocpath = "";
}
 


  ViewDoc() {
    // window.open(this.showDocpath, '_blank');
    // const link = 'data:image/jpeg;base64,'+this.showDocpath;
    // var newWindow = window.open();
    // newWindow.document.write('<img src="' + link + '" />');
    //var splitted = this.showDocpath.split(",", 10);
    //const blob = b64toBlob(this.showDocpath, splitted[0]);
    var path = this.showDocpath.split(',')
    var b = this.service.s_sd(path[1], 'application/pdf');
    var l = document.createElement('a');
    l.href = window.URL.createObjectURL(b);
    l.download = "Document" + ".pdf";
    document.body.appendChild(l);
    l.click();
    document.body.removeChild(l);
  }



  DocDivClick(val) {
    this.IssuedDocView = val;
  }

  hideFilesList() {
    this.FilesList.hide();
  }

  
  Docpriview(val) {

    // const blob = this.service.s_sd((val).split(',')[1], this.seldoccat);
    // const blobUrl = URL.createObjectURL(blob);
    // this.preview = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
    //this.preview = this.sanitizer.bypassSecurityTrustUrl(val);
    //this.preview = this.preview.changingThisBreaksApplicationSecurity;
    this.previewModal.show();

  }
  GetDetails(uri){
    this.showloader=true;
    this.digiservice.GetDetails (uri ,this.Accesstoken, (a) => {
      console.log(a);
      if(a.Status == "Success")
      {
      
      //this.Details=a.CertIssuerData.Certificate.IssuedTo.Person;
  if(this.viewbtnname == "Aadha")
  {
    this.name=a.CertIssuerData.KycRes.UidData.Poi.name;
    this.gender=a.CertIssuerData.KycRes.UidData.Poi.gender;
    this.dob=a.CertIssuerData.KycRes.UidData.Poi.dob;
  }
  else{
  this.name=a.CertIssuerData.Certificate.IssuedTo.Person.name;
  this.gender=a.CertIssuerData.Certificate.IssuedTo.Person.gender;
  this.dob=a.CertIssuerData.Certificate.IssuedTo.Person.dob;
  }
  
  
     
      this.showloader=false;
      this.DetailList.show();
      
      }
      else{
        this.showloader=false;
      }
  });  
  }

  hideDetailList() {
    this.DetailList.hide();

  }
  hidepreviewModal()
  {
    this.PreviewModal.hide();

  }
}
