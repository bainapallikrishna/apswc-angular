import { Component, OnInit, ViewChild } from '@angular/core';
import{Router}from '@angular/router';
import Swal from 'sweetalert2';
import{InputRequest} from 'src/app/Interfaces/employee';
import{CommonServices} from 'src/app/Services/common.services';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';

@Component({
  selector: 'app-dead-stock',
  templateUrl: './dead-stock.component.html',
  styleUrls: ['./dead-stock.component.css']
})
export class DeadStockComponent implements OnInit {
loguser: string=sessionStorage.getItem("logUserCode");
loguserrole: string = sessionStorage.getItem("logUserrole");
ispwdchanged:string=sessionStorage.getItem("logUserisChnagePassword");
showloader:boolean=false;
CategoryList:any [];
SubCategoryList:any [];
ItemList:any [];
submitted:boolean=false;
editsubmitted:boolean=false;
Itemeditsubmitted:boolean=false;
title:string;
SectionName:any;
CategoryName:any;
SessionEditCatDBStatus:any;
SessionEditItemDBStatus:any;
Cataddarray:any;
SubCataddarray:any;
seclist:any [];
categorydd:any [];
CatHistoryList:any[];
isactive_field: any;
isactive_flag: boolean = false;
Itemisactive_flag:boolean=false;

EditCatactive:any;
EditCategoryRemarks:any;

@ViewChild('addCatModal') addCatModal:ModalDirective;
@ViewChild('addSubCatModal') addSubCatModal:ModalDirective;
@ViewChild('editCategoryModal') editCategoryModal:ModalDirective;
@ViewChild('CategoryhistoryModal') CategoryhistoryModal:ModalDirective;
@ViewChild('addItemModal') addItemModal:ModalDirective;
@ViewChild('editItemModal') editItemModal:ModalDirective;



  constructor(private router:Router,private service:CommonServices,private fb:FormBuilder) 
  {
    if (!this.loguserrole || this.ispwdchanged == '0') {
      this.router.navigate(['/Login']);
      return;
    }
   }

  ngOnInit(): void {
    
  }

  i = 0 + 1; j = 0 + 1;k=0+1;


//=============================================================CATEGORY=================================================================

ItemClick() {

  if (this.k == 1) {
    
  
    this.Itemlist();
    this.k++;
  }
  else {

    this.k=0;

  }
}

Itemlist()
{
 this.showloader= true;  
 this.ItemList=[];
 const req = new InputRequest();
 req.INPUT_01 = "ITEM_LIST";
 this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
   if (data.StatusCode == "100") {
     this.ItemList = data.Details;
     this.showloader= false;  
   }
   else{
     this.showloader= false;  

   Swal.fire("info",data.StatusMessage,"info");
   }

 },
   error => console.log(error));

}

addItem() {
  this.Itemaddform.reset();
  this.submitted = false;
  this.title="Item";    
   this.addItemModal.show();
}

hideaddItemModal(){
  this.addItemModal.hide();
}

SubmitItem(){
  this.submitted=true;
  if(this.Itemaddform.invalid){
    return false;
  }

  const req = new InputRequest();
  
  req.INPUT_01 = "WAREHOUSE_ITEM_TYPE";
  req.INPUT_02=this.f4.ItemName.value;
  req.INPUT_03=this.loguserrole;
  req.USER_NAME=this.loguser;
  req.CALL_SOURCE="Web";
  this.service.postData(req, "DeadStockInsertion").subscribe(data => {
    this.Cataddarray = data.result;
      if (data.StatusCode == 100) {
      this.showloader= false;  
      this.Itemaddform.reset();
        this.addItemModal.hide();
        Swal.fire("success", "Item Added Successfully", "success");
        this.Itemlist();
    }
    else{
      this.showloader= false;  
      Swal.fire("info",data.StatusMessage,"info");
    }
  });
  
}

editItem(editid:any, editname:any,actinact:string,type:any) {
  this.title="Item";
  this.ItemeditForm.reset();
  this.Itemeditsubmitted = false;
  this.f5.EditItemID.setValue(editid);
  this.f5.EditItemName.setValue(editname);
  this.SessionEditItemDBStatus=actinact;
  this.f5.EditItemactive.setValue(actinact=='1'?"1":"0");
  this.Itemisactive();
  this.editItemModal.show();
}

SubmitEditItem(){
  this.Itemeditsubmitted=true;
  if(this.ItemeditForm.invalid){
    return false;
  }

  const req = new InputRequest();
  
  req.INPUT_01 = "ACTIVE_INACTIVE";
  req.INPUT_02=this.SessionEditItemDBStatus;
  req.INPUT_03=this.f5.EditItemactive.value;
  req.INPUT_04=this.f5.EditItemRemarks.value;
  req.INPUT_06=this.f5.EditItemID.value;
  req.USER_NAME=this.loguser;
  req.INPUT_05=this.loguserrole;
  req.CALL_SOURCE="Web";

  this.service.postData(req, "DeadStockInsertion").subscribe(data => {
    this.Cataddarray = data.result;
      if (data.StatusCode == 100) {
      this.showloader= false;  
      this.ItemeditForm.reset();
        this.editItemModal.hide();
        Swal.fire("success", "Item Status Changed Successfully", "success");
        this.Itemlist();
        
    }
    else{
      this.showloader= false;  
      Swal.fire("info",data.StatusMessage,"info");
    }
  });
  
}

  CategoryClick() {

    if (this.i == 1) {
      
    
      this.Categorylist();
      this.i++;
    }
    else {
  
      this.i=0;

    }
  }

  Categorylist()
  {
   this.showloader= true;  
   this.CategoryList=[];
   const req = new InputRequest();
   req.INPUT_01 = "CAT_LIST";
   this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
     if (data.StatusCode == "100") {
       this.CategoryList = data.Details;
       this.showloader= false;  
     }
     else{
       this.showloader= false;  

     Swal.fire("info",data.StatusMessage,"info");
     }

   },
     error => console.log(error));

 }

 addCategory() {
  this.Categoryaddform.reset();
  this.CategoryeditForm.reset();
  this.submitted = false;
  this.title="Category";    
  this.GetSectionDropdown();
  this.f1.SectionName.setValue("");
   this.addCatModal.show();
}

hideaddCatModal(){
  this.addCatModal.hide();
}

hideeditItemModal(){
  this.editItemModal.hide();
}

GetSectionDropdown(){
  this.showloader= true;  

   const req = new InputRequest();
   req.INPUT_01 = "SECTION_DD";
   this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
     if (data.StatusCode == "100") {
       this.seclist = data.Details;
       this.showloader= false;  
     }
     else{
       this.showloader= false;  

     Swal.fire("info",data.StatusMessage,"info");
     }

   },
     error => console.log(error));

}

get f1() { return this.Categoryaddform.controls; }
get f3() { return this.CategoryeditForm.controls; }
get f4() { return this.Itemaddform.controls; }
get f5() { return this.ItemeditForm.controls; }

Categoryaddform=this.fb.group({
    SectionName: ['', Validators.required],
    CategoryName:['',Validators.required]
  });

  CategoryeditForm = this.fb.group({
    EditCategoryID:['',Validators.required],
    EditCategoryName:['',Validators.required],
    EditCatactive: [''],
    EditCategoryRemarks:['',Validators.required],
    EditSectionName:['',Validators.required],
  });

  Itemaddform = this.fb.group({
    ItemName:['',Validators.required]
  });

  ItemeditForm = this.fb.group({
    EditItemName:['',Validators.required],
    EditItemactive:['',Validators.required],
    EditItemRemarks:['',Validators.required],
    EditItemID:['',Validators.required]
  });

  
SubmitCategory(){
  this.submitted=true;
  if(this.Categoryaddform.invalid){
    return false;
  }

  const req = new InputRequest();
  
  req.INPUT_01 = "MAINTENANCE_CATEGORY";
  req.INPUT_02=this.f1.CategoryName.value;
  req.INPUT_04=this.f1.SectionName.value;
  req.USER_NAME=this.loguser;
  req.INPUT_06=this.loguserrole;
  req.CALL_SOURCE="Web";

  this.service.postData(req, "DeadStockInsertion").subscribe(data => {
    this.Cataddarray = data.result;
      if (data.StatusCode == 100) {
      this.showloader= false;  
      this.Categoryaddform.reset();
        this.addCatModal.hide();
        Swal.fire("success", "Category Added Successfully", "success");
        this.Categorylist();
    }
    else{
      this.showloader= false;  
      Swal.fire("info",data.StatusMessage,"info");
    }
  });
  
}

editCategory(editid:any, editname:any,actinact:string,type:any,Section:any) {
  if(type=="Category"){
    this.title="Category";
  }
  else if(type=="Sub Category"){
    this.title="Sub Category";
  }
  this.Categoryaddform.reset();
  this.CategoryeditForm.reset();
  this.editsubmitted = false;

  this.f3.EditCategoryID.setValue(editid);
  this.f3.EditCategoryName.setValue(editname);
  this.f3.EditSectionName.setValue(Section);
  this.SessionEditCatDBStatus=actinact;
  this.f3.EditCatactive.setValue(actinact=='1'?"1":"0");
  this.isactive();
  this.editCategoryModal.show();
}

SubmitEditCategory(){
  this.editsubmitted=true;
  if(this.CategoryeditForm.invalid){
    return false;
  }

  const req = new InputRequest();
  
  req.INPUT_01 = "ACTIVE_INACTIVE";
  req.INPUT_02=this.SessionEditCatDBStatus;
  req.INPUT_03=this.f3.EditCatactive.value;
  req.INPUT_04=this.f3.EditCategoryRemarks.value;
  req.INPUT_06=this.f3.EditCategoryID.value;
  req.USER_NAME=this.loguser;
  req.INPUT_05=this.loguserrole;
  req.CALL_SOURCE="Web";

  this.service.postData(req, "DeadStockInsertion").subscribe(data => {
    this.Cataddarray = data.result;
      if (data.StatusCode == 100) {
      this.showloader= false;  
      this.Categoryaddform.reset();
        this.editCategoryModal.hide();
        Swal.fire("success", "Category Status Changed Successfully", "success");
        if(this.title=="Category"){
        this.Categorylist();
        this.SubCategorylist();
        }
        else if(this.title=="Sub Category"){
          this.SubCategorylist();
          }
    }
    else{
      this.showloader= false;  
      Swal.fire("info",data.StatusMessage,"info");
    }
  });
  
}

Itemisactive() {
  if (this.f5.EditItemactive.value != this.SessionEditItemDBStatus)
    this.Itemisactive_flag = true;
  else
    this.Itemisactive_flag = false;
}

isactive() {
  if (this.f3.EditCatactive.value != this.SessionEditCatDBStatus)
    this.isactive_flag = true;
  else
    this.isactive_flag = false;
}

hideeditCategoryModal(){
  this.editCategoryModal.hide();
}



Categoryhistory(val)
{
 const req = new InputRequest();
 if(val=="MAINTENANCE_CATEGORY"){
  this.title="Category";
 }
 else if(val=="MAINTENANCESUB_CATEGORY"){
  this.title="Sub Category";
 }
 else if(val=="WAREHOUSE_ITEM_TYPE"){
  this.title="Item Type";
 }
 req.INPUT_01 = val;
 req.TYPEID="GET_LOG_HISTORY";
 req.INPUT_02="MASTERS";
 this.showloader= true;
 this.service.postData(req, "GetDeadStockHistory").subscribe(data => {
     if (data.StatusCode == "100") {
       this.CatHistoryList = data.Details;
       this.Categoryaddform.reset();
       this.CategoryeditForm.reset();
       this.CategoryhistoryModal.show();
       this.showloader= false;
     }
     else{

       Swal.fire("info",data.StatusMessage,"info");
       this.Categoryaddform.reset();
   this.CategoryeditForm.reset();
   this.CategoryhistoryModal.hide()
       this.showloader= false;  
     }
   },
     error => console.log(error));

 }

 hideCategoryhistoryModal(){
  this.CategoryhistoryModal.hide();
}

//=========================================================SUB CATEGORY==========================================================================

  SubCategoryClick() {
    if (this.j == 1) {
    
      this.SubCategorylist();
      this.j++;
    }
    else {
      this.j=0;
    }
  }

 SubCategorylist() {
   this.showloader= true;  
   const req = new InputRequest();
   req.INPUT_01 = "SUBCAT_LIST";
   this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
     if (data.StatusCode == "100") {
       this.SubCategoryList = data.Details;
       this.showloader= false;  
     }
     else{
       this.showloader= false;
       Swal.fire("info",data.StatusMessage,"info");
       }
   },
     error => console.log(error));

 }

addSubCategory() {
  this.SubCategoryaddform.reset();

  this.submitted = false;
  this.title="Sub Category";    
  this.GetCategoryDropdown();
this.f2.CategoryName.setValue("");
   this.addSubCatModal.show();

}

hideaddSubCatModal(){
  this.addSubCatModal.hide();
}

GetCategoryDropdown(){
  this.showloader= true;  

   const req = new InputRequest();
   req.INPUT_01 = "CATEGORY_DD";
   this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
     if (data.StatusCode == "100") {
       this.categorydd = data.Details;
       this.showloader= false;  
     }
     else{
       this.showloader= false;  

     Swal.fire("info",data.StatusMessage,"info");
     }

   },
     error => console.log(error));

}

  get f2() { return this.SubCategoryaddform.controls; }

  SubCategoryaddform=this.fb.group({
    CategoryName: ['', Validators.required],
    SubCategoryName:['',Validators.required],
    sec:['']
  });



  subcategorychange(sec){
  for(var i=0;i<=this.SubCataddarray.length-1;i++){

if(sec.categorydd==this.SubCataddarray[i].categorydd){
   var sect= this.SubCataddarray[i].wM_SECTION;
    this.f2.sec.setValue(sect);
  }
}
 
  }

SubmitSubCategory(){
  this.submitted=true;
  if(this.SubCategoryaddform.invalid){
    return false;
  }

  const req = new InputRequest();
  
  req.INPUT_01 = "MAINTENANCESUB_CATEGORY";
  req.INPUT_02=this.f2.SubCategoryName.value;
  req.INPUT_05=this.f2.CategoryName.value;
  req.INPUT_04= this.f2.sec.value;
  req.USER_NAME=this.loguser;
  req.INPUT_06=this.loguserrole;
  req.CALL_SOURCE="Web";

  this.service.postData(req, "DeadStockInsertion").subscribe(data => {
    this.SubCataddarray = data.result;
      if (data.StatusCode == 100) {
      this.showloader= false;  
      this.SubCategoryaddform.reset();
        this.addSubCatModal.hide();
        Swal.fire("success", "Sub Category Added Successfully", "success");
        this.SubCategorylist();
    }
    else{
      this.showloader= false;  
      Swal.fire("info",data.StatusMessage,"info");
    }
  });
  
}

}
