import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeadstocksecapprovalComponent } from './deadstocksecapproval.component';

describe('DeadstocksecapprovalComponent', () => {
  let component: DeadstocksecapprovalComponent;
  let fixture: ComponentFixture<DeadstocksecapprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeadstocksecapprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeadstocksecapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
