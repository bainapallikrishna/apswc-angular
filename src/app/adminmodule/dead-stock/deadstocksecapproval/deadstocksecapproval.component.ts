import { Component, OnInit,ViewChild  } from '@angular/core';
import{Router}from '@angular/router';
import Swal from 'sweetalert2';
import{InputRequest} from 'src/app/Interfaces/employee';
import{CommonServices} from 'src/app/Services/common.services';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { DSAPPButtonRendererComponent } from 'src/app/custome-directives/DSAPPbutton-renderer.component';

@Component({
  selector: 'app-deadstocksecapproval',
  templateUrl: './deadstocksecapproval.component.html',
  styleUrls: ['./deadstocksecapproval.component.css']
})
export class DeadstocksecapprovalComponent implements OnInit {

loguser: string=sessionStorage.getItem("logUserCode");
loguserrole: string = sessionStorage.getItem("logUserrole");
ispwdchanged:string=sessionStorage.getItem("logUserisChnagePassword");
loguserid:string=sessionStorage.getItem("logUserworkLocationCode");


columnDefs = [
  { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer' },
  { headerName: 'WareHouse Name', maxWidth: 200, field: 'warehouse_name', sortable: true, filter: true },
  { headerName: 'Sub Category Name', maxWidth: 200, field: 'suB_ITEM_NAME', sortable: true, filter: true },
  { headerName: 'Item Type', maxWidth: 200, field: 'iteM_type_NAME', sortable: true, filter: true },
  { headerName: 'No. of Units', maxWidth: 150, field: 'no_of_units', sortable: true, filter: true },
  { headerName: 'Amount / unit', maxWidth: 150, field: 'amount_per_unit', sortable: true, filter: true },
  { headerName: 'Total Amount', maxWidth: 200, field: 'chargeS_AMOUNT', sortable: true, filter: true },
  { headerName: 'Remarks', maxWidth: 300, field: 'remarks', sortable: true, filter: true },
  {
    headerName: 'Action',field: 'ediT_STATUS', cellRenderer: 'buttonRenderer',
    cellRendererParams: {
      ApproveClick: this.Approve.bind(this),
      RejectClick: this.Reject.bind(this),
      ViewClick: this.View.bind(this),
    },
  }
];

frameworkComponents: any;
public components;
gridApi: any;
gridColumnApi: any;
DeadStockList: any[];
showloader:boolean=false;
Formaddarray: any [];
submittedrsn:boolean=false;
cancelrcd : any;
Image: any ;
Document:any;

  constructor(private router:Router,private service:CommonServices,private fb:FormBuilder) { 
    
    
    if (!this.loguserrole || this.ispwdchanged == '0') {
      this.router.navigate(['/Login']);
      return;
    }

    this.frameworkComponents = {
      buttonRenderer: DSAPPButtonRendererComponent,
    }
    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }

  @ViewChild('ReasonModal') public ReasonModal: ModalDirective;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  ngOnInit(): void {
    this.LoadDeadStockList();

  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.DeadStockList);
  }

  View(CancelRcd)
  {
    CancelRcd.rowData.documenT_PATH ? (this.decryptFile(CancelRcd.rowData.documenT_PATH, "DS_DOC")) : "";
    CancelRcd.rowData.imagE1_PATH ? (this.decryptFile(CancelRcd.rowData.imagE1_PATH, "DS_PHOTO")) : ""; 
    this.viewModal.show();
  }

  decryptFile(filepath,Doctype) {
    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if(Doctype=="DS_DOC"){
            this.Document=data.docBase64;
          }
          else
          {
            this.Image=data.docBase64;
          }
          
        },
          error => {
            console.log(error);
            this.Document="";
            this.Image="";
          });
    }
    else
    this.Document="";
    this.Image="";
  }

  viewPDF(input): void {
    let pdfWindow = window.open('', '_blank')
    pdfWindow.document.write(`<iframe width='100%' height='100%' src='${encodeURI(input)}'></iframe>`)
  }

  Approve(CancelRcd){
    Swal.fire({
      title: 'Are you sure?Do you want to approve this record?',
      showCancelButton: true,
      confirmButtonText: `Save`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.ApproveAPI(CancelRcd);
      } else {
        Swal.fire('info', 'Changes are not saved', 'info')
      }
    })
  }

  ApproveAPI(CancelRcd){
    const req = new InputRequest();
  
    req.INPUT_01 = "CANCEL_RECORD";
    req.INPUT_02 = "SECTION";
    req.INPUT_03=CancelRcd.rowData.form_id;
    req.INPUT_04=CancelRcd.rowData.wH_ID;
    req.INPUT_06="1";
    req.CALL_SOURCE="Web";
    req.USER_NAME=this.loguser;
  
    this.service.postData(req, "DeadStockInsertion").subscribe(data => {
      this.Formaddarray = data.result;
        if (data.StatusCode == 100) {
        this.showloader= false;  
        Swal.fire({
          title: 'Record Approved Successfully',
          confirmButtonText: `Ok`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.LoadDeadStockList();
          } else if (result.isDenied) {
          }
        })
      }
      else{
        this.showloader= false;  
        Swal.fire("info",data.StatusMessage,"info");
      }
    });
  }

  Reject(CancelRcd){
    this.cancelrcd=CancelRcd;
    this.DeadStockRejectReasonForm.reset();
    this.ReasonModal.show();
  }

  hideReasonModal(){
    this.DeadStockRejectReasonForm.reset();
    this.ReasonModal.hide();
  }

  get subfrsn() { return this.DeadStockRejectReasonForm.controls; }

  DeadStockRejectReasonForm=this.fb.group({
    RejectReason: ['', Validators.required],
  });

  RejectAPI(){
    this.submittedrsn=true;
    if(this.DeadStockRejectReasonForm.invalid){
      return false;
    }
    const req = new InputRequest();
  
    req.INPUT_01 = "CANCEL_RECORD";
    req.INPUT_02 = "SECTION";
    req.INPUT_03=this.cancelrcd.rowData.form_id;
    req.INPUT_04=this.cancelrcd.rowData.wH_ID;
    req.INPUT_05=this.subfrsn.RejectReason.value;
    req.INPUT_06="2";
    req.CALL_SOURCE="Web";
    req.USER_NAME=this.loguser;
  
    this.service.postData(req, "DeadStockInsertion").subscribe(data => {
      this.Formaddarray = data.result;
        if (data.StatusCode == 100) {
        this.showloader= false;  
        Swal.fire({
          title: 'Record Rejected Successfully',
          confirmButtonText: `Ok`,
        }).then((result) => {
          if (result.isConfirmed) {
            this.LoadDeadStockList();
            this.hideReasonModal();
          } else if (result.isDenied) {
          }
        })
      }
      else{
        this.showloader= false;  
        Swal.fire("info",data.StatusMessage,"info");
      }
    });
  }

  LoadDeadStockList() {

    this.showloader = true;
    const req = new InputRequest();

    req.INPUT_01 = "SEC_DEADSTOCK_LIST";
    req.INPUT_02 = this.loguserid;
    this.DeadStockList=[];
    this.service.postData(req, "GetDeadStockDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.DeadStockList = data.Details;
        this.gridApi.setRowData(this.DeadStockList);
        console.log(this.DeadStockList);
      }
      else{
        this.gridApi.setRowData(this.DeadStockList);
        Swal.fire('warning', data.StatusMessage, 'warning');
      }
    },
      error => console.log(error));

  }

}