import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalReimbursementRequestComponent } from './medical-reimbursement-request.component';

describe('MedicalReimbursementRequestComponent', () => {
  let component: MedicalReimbursementRequestComponent;
  let fixture: ComponentFixture<MedicalReimbursementRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalReimbursementRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalReimbursementRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
