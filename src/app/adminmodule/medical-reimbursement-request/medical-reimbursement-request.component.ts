import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { EmployeeSalaryInputRequest, InputRequest } from 'src/app/Interfaces/employee';
import { AgGridAngular } from 'ag-grid-angular';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-medical-reimbursement-request',
  templateUrl: './medical-reimbursement-request.component.html',
  styleUrls: ['./medical-reimbursement-request.component.css']
})
export class MedicalReimbursementRequestComponent implements OnInit {


  bankholdername: any;
  accnumber: any;
  branchname: any;
  bankname: any;

  ifsccode: any;
  familrelation: any;
  relation: any;
  district: any;
  hospital: any;
  disease: any;
  treatmentamount: any;
  treatmenttype: any;
  remarks: any;
  base64sstringpath: any;
  tokens: any;
  regsdoc: any;
  Loanmonth: any;
  frameworkComponents: any;
  NgbDateStruct: any;
  Loantype: any;
  progressbar: boolean = false;
  message1: string = "";
  message2: string = "";
  message3: string = "";
  message4: string = "";
  message5: string = "";
  message6: string = "";
  message7: string = "";
  UploadDocType: string = "";
  UploadDocPath: string = "";
  progress1: number = 0;
  progress2: number = 0;
  progress3: number = 0;
  progress4: number = 0;
  progress5: number = 0;
  progress6: number = 0;
  progress7: number = 0;
  submitted: boolean = false;
  showloader: boolean = false;
  gridColumnApi: any = [];
  defaultColDef: any = [];
  Loantypes: any;
  gridApi: any = [];
  public rowseledted: object;
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  Worklocname = sessionStorage.getItem("logUserworkLocationName");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  public components;
  districts: any;
  hospitaltypes: any;
  diseaselist: any;
  treatmentlist: any;
  Familyrelationlist: any;
  Familynameslist: any;
  hiderelname: boolean = false;
  historylist: any;
  Bankaccountdetails: any;
  apprtype: any;
  preview: string = "";
  seldocpath1: any;
  seldoctype1: string = "";
  seldoccat1: string = "";
  seldocpath2: any;
  seldoctype2: string = "";
  seldoccat2: string = "";
  seldocpath3: any;
  seldoctype3: string = "";
  seldoccat3: string = "";
  seldocpath4: any;
  seldoctype4: string = "";
  seldoccat4: string = "";
  seldocpath5: any;
  seldoctype5: string = "";
  seldoccat5: string = "";
  seldocpath6: any;
  seldoctype6: string = "";
  seldoccat6: string = "";
  seldocpath7: any;
  seldoctype7: string = "";
  seldoccat7: string = "";

  @ViewChild('TaxPaymentModal') TaxPaymentModal: ModalDirective;
  @ViewChild('mrPreviewModal') mrPreviewModal: ModalDirective;
  @ViewChild('historyWarehousemodal') historyWarehousemodal: ModalDirective;
  @ViewChild('apprModal') apprModal: ModalDirective;
  @ViewChild('previewModal') public previewModal: ModalDirective;

  columnDefs = [
    { headerName: 'S.no', cellRenderer: 'rowIdRenderer', width: 60, floatingFilter: false },
    { headerName: 'Employee Code', width: 100, field: 'emP_CODE' },
    { headerName: 'Employee Name', width: 200, field: 'emP_NAME' },
    { headerName: 'Employee Type', width: 150, field: 'emP_TYPE' },
    { headerName: 'Employee Relation', width: 150, field: 'familY_RELATION' },
    { headerName: 'Family Member Name', width: 150, field: 'familY_NAME' },
    { headerName: 'District Name', width: 100, field: 'districT_NAME' },
    { headerName: 'Disease', width: 200, fie10ld: 'diseasE_NAME' },
    { headerName: 'Hospital Name', width: 200, field: 'hospitaL_NAME' },
    { headerName: 'Treatment Amount', width: 150, field: 'treatmenT_AMOUNT' },
    { headerName: 'Treatment Type', width: 150, field: 'treatmenT_TYPE' },
    { headerName: 'Status', width: 200, field: 'applicatioN_FORWARD' },
    { headerName: 'Sanctioned Amount', width: 150, field: 'approveD_AMOUNT' },

  ];

  constructor(private formBuilder: FormBuilder,
    private service: CommonServices,
    private router: Router,
    private datef: CustomDateParserFormatter,
    private sanitizer: DomSanitizer) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {

      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      wrapText: true,
      autoHeight: true,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.components = {

      rowIdRenderer: function (params) {

        return '' + (params.rowIndex + 1);
      }
    }

    this.LoadDistricts();
    this.GetDiseaselist();
    this.getfamilyrelation();
  }
  ngOnInit(): void {
  }

  EmployeeMedicalrequestForm = this.formBuilder.group({

    PDistrict: [''],
    Hosname: [''],
    disease: [''],
    TreatmentAmount: [''],
    LoanRemarks: [''],
    AppendixRegDoc: [''],
    AppendixDocPath: [''],
    CERTIFICATEADocPath: [''],
    CERTIFICATEARegDoc: [''],
    NONDRAWALDocPath: [''],
    NONDRAWALRegDoc: [''],
    DECLARATIONDocPath: [''],
    DECLARATIONRegDoc: [''],
    EMERGENCYDocPath: [''],
    EMERGENCYRegDoc: [''],
    ESSENTIALITYDocPath: [''],
    ESSENTIALITYRegDoc: [''],
    CHECKLISTDocPath: [''],
    CHECKLISTRegDoc: [''],
    Familyrel: [''],
    Relname: [''],
    trttype: [''],
    Bankholdername: [''],
    Accno: [''],
    Bankname: [''],
    Ifsccode: [''],
    Branchname: [''],
    check: ['']
  });

  LoadDistricts() {
    this.service.getData("GetDistricts").subscribe(data => {

      if (data.StatusCode == "100") {
        this.districts = data.Details;
      }

    },
      error => console.log(error));
  }


  Hospitalload() {

    this.hospitaltypes = [];

    this.GetHospitalstypes();

  }


  GetHospitalstypes() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.EmployeeMedicalrequestForm.controls.PDistrict.value;
    this.service.postData(req, "GetHospitalslistbydistcode").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.hospitaltypes = data.Details;
      }
      else {
        this.showloader = false;
        this.EmployeeMedicalrequestForm.controls.Hosname.setValue('');
        Swal.fire("info", data.StatusMessage, "info");

      }
    },
      error => console.log(error));

  }


  GetDiseaselist() {
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "TREATMENT";
    this.service.postData(req, "GetHospitalslist").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.diseaselist = data.Details;
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    },
      error => console.log(error));

  }
  Treatmentchange() {

    this.getcgstorarogya();
  }


  Getbankaccountdetails() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.logUsercode;
    this.service.postData(req, "GetBankdetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Bankaccountdetails = data.Details;
        this.bankholdername = this.Bankaccountdetails[0].accounT_HOLDER_NAME;
        this.EmployeeMedicalrequestForm.controls.Bankholdername.setValue(this.bankholdername);
        this.accnumber = this.Bankaccountdetails[0].accounT_NUMBER;
        this.EmployeeMedicalrequestForm.controls.Accno.setValue(this.accnumber);
        this.branchname = this.Bankaccountdetails[0].banK_BRANCH;
        this.EmployeeMedicalrequestForm.controls.Branchname.setValue(this.accnumber);
        this.bankname = this.Bankaccountdetails[0].banK_NAME;
        this.EmployeeMedicalrequestForm.controls.Bankname.setValue(this.bankname);
        this.ifsccode = this.Bankaccountdetails[0].ifsC_CODE;
        this.EmployeeMedicalrequestForm.controls.Ifsccode.setValue(this.ifsccode);
        this.TaxPaymentModal.show();
      }
      else {
        this.showloader = false;
        Swal.fire("info", "Please Update the bank Details in Employee Details Portal ", "info");
        this.TaxPaymentModal.hide();
      }
    },
      error => console.log(error));


  }
  familyname: any;

  getfamilyrelation() {
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "EMP_FAMILY";
    req.INPUT_01 = this.logUsercode;
    this.service.postData(req, "GetFamilynames").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.Familyrelationlist = data.Details;

      }
      else {
        this.showloader = false;
        Swal.fire("info", "No Family Details found", "info");
      }
    },
      error => console.log(error));


  }
  Familyrelationchange() {

    if (this.EmployeeMedicalrequestForm.controls.Familyrel.value == "SELF") {
      this.Familynameslist = [];
      this.districts = [];
      this.diseaselist = [];
      this.hospitaltypes = [];
      this.GetDiseaselist();
      this.LoadDistricts();
      this.EmployeeMedicalrequestForm.controls.PDistrict.setValue('');
      this.EmployeeMedicalrequestForm.controls.Hosname.setValue('');
      this.EmployeeMedicalrequestForm.controls.disease.setValue('');
      this.EmployeeMedicalrequestForm.controls.TreatmentAmount.setValue('');
      this.EmployeeMedicalrequestForm.controls.trttype.setValue('');
      this.EmployeeMedicalrequestForm.controls.AppendixDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.AppendixRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.CERTIFICATEADocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.CERTIFICATEARegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.NONDRAWALDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.NONDRAWALRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.DECLARATIONDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.DECLARATIONRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.EMERGENCYDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.EMERGENCYRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.ESSENTIALITYDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.ESSENTIALITYRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.CHECKLISTDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.CHECKLISTRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.check.setValue('');
      this.progressbar = false;
      this.progress1 = null;
      this.message1 = null;
      this.progress2 = null;
      this.message2 = null;
      this.progress3 = null;
      this.message3 = null;
      this.progress4 = null;
      this.message4 = null;
      this.progress5 = null;
      this.message5 = null;
      this.progress6 = null;
      this.message6 = null;
      this.progress7 = null;
      this.message7 = null;
      this.progressbar = false;

      this.hiderelname = false;

    }
    else {
      this.Familynameslist = [];
      this.districts = [];
      this.diseaselist = [];
      this.hospitaltypes = [];
      this.getfamilyname();
      this.LoadDistricts();
      this.GetDiseaselist();
      this.EmployeeMedicalrequestForm.controls.PDistrict.setValue('');
      this.EmployeeMedicalrequestForm.controls.Hosname.setValue('');
      this.EmployeeMedicalrequestForm.controls.disease.setValue('');
      this.EmployeeMedicalrequestForm.controls.TreatmentAmount.setValue('');
      this.EmployeeMedicalrequestForm.controls.trttype.setValue('');
      this.EmployeeMedicalrequestForm.controls.AppendixDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.AppendixRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.CERTIFICATEADocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.CERTIFICATEARegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.NONDRAWALDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.NONDRAWALRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.DECLARATIONDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.DECLARATIONRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.EMERGENCYDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.EMERGENCYRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.ESSENTIALITYDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.ESSENTIALITYRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.CHECKLISTDocPath.setValue('');
      this.EmployeeMedicalrequestForm.controls.CHECKLISTRegDoc.setValue('');
      this.EmployeeMedicalrequestForm.controls.check.setValue('');
      this.progress1 = null;
      this.message1 = null;
      this.progress2 = null;
      this.message2 = null;
      this.progress3 = null;
      this.message3 = null;
      this.progress4 = null;
      this.message4 = null;
      this.progress5 = null;
      this.message5 = null;
      this.progress6 = null;
      this.message6 = null;
      this.progress7 = null;
      this.message7 = null;


      this.progressbar = false;
    }
  }
  getfamilyname() {

    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "EMP_RELATION";
    req.INPUT_01 = this.logUsercode;
    req.INPUT_02 = this.EmployeeMedicalrequestForm.controls.Familyrel.value;
    this.service.postData(req, "GetFamilynames").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.hiderelname = true;
        this.Familynameslist = data.Details;
        this.familyname = this.Familynameslist[0].fM_NAME;
        this.EmployeeMedicalrequestForm.controls.Relname.setValue(this.familyname);

        if (this.Familynameslist.length > 1) {
          this.EmployeeMedicalrequestForm.controls['Relname'].enable();
        }
        else {
          this.EmployeeMedicalrequestForm.controls['Relname'].disable();
        }
      }
      else {
        this.showloader = false;
        this.hiderelname = false;
        //Swal.fire("info","your" +this.EmployeeMedicalrequestForm.controls.Familyrel.value+"is not eligible Because his age is >25" ,"info");
        Swal.fire("info", "No Data Found", "info");
      }
    },
      error => console.log(error));

  }

  Historyclick() {

    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "EMP_MEDICAL_HISTORY";
    req.INPUT_01 = this.logUsercode;
    this.service.postData(req, "GetFamilyrelation").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.historyWarehousemodal.show();
        this.historylist = data.Details;
      }
      else {
        this.showloader = false;
        Swal.fire("info", "No history Found", "info");
        this.historyWarehousemodal.hide();
      }
    },
      error => console.log(error));

  }
  hidehistorymasterModal() {
    this.historyWarehousemodal.hide();
  }




  getcgstorarogya() {

    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "TREATMENT_AMOUNT";
    req.INPUT_01 = this.EmployeeMedicalrequestForm.controls.TreatmentAmount.value;
    this.service.postData(req, "GetTreatmenttypebytrtamount").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.treatmentlist = data.Details;
        this.reg.trttype.setValue(this.treatmentlist[0].column1);
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    },
      error => console.log(error));

  }

  hospitalchange() {
    for (var i = 0; i <= this.hospitaltypes.length - 1; i++) {
      if (this.EmployeeMedicalrequestForm.controls.Hosname.value == this.hospitaltypes[i].hospitaL_ID)
        this.hospital = this.hospitaltypes[i].hospitaL_NAME;
    }
  }


  diseasechange() {
    for (var i = 0; i <= this.diseaselist.length - 1; i++) {
      if (this.EmployeeMedicalrequestForm.controls.disease.value == this.diseaselist[i].treatmenT_ID)
        this.disease = this.diseaselist[i].treatmenT_NAME;
    }



  }



  Previewfunction() {
    this.showloader = true;

    if (this.EmployeeMedicalrequestForm.controls.Familyrel.value == "" || this.EmployeeMedicalrequestForm.controls.Familyrel.value == undefined || this.EmployeeMedicalrequestForm.controls.Familyrel.value == null) {
      this.showloader = false;
      Swal.fire('info', "Please Select Family Relation", 'info');
      return false;

    }

    if (this.EmployeeMedicalrequestForm.controls.Familyrel.value != "" || this.EmployeeMedicalrequestForm.controls.Familyrel.value != null || this.EmployeeMedicalrequestForm.controls.Familyrel.value != undefined) {
      if (this.EmployeeMedicalrequestForm.controls.Familyrel.value != "SELF") {
        if (this.EmployeeMedicalrequestForm.controls.Relname.value == "" || this.EmployeeMedicalrequestForm.controls.Relname.value == null || this.EmployeeMedicalrequestForm.controls.Relname.value == undefined) {
          Swal.fire("info", "Please Select Relation name", "info");
          this.showloader = false;
          return false;
        }
        else {


        }

      }
      else {

      }

    }

    if (this.EmployeeMedicalrequestForm.controls.PDistrict.value == "" || this.EmployeeMedicalrequestForm.controls.PDistrict.value == undefined || this.EmployeeMedicalrequestForm.controls.PDistrict.value == null) {
      this.showloader = false;
      Swal.fire('info', "Please Select district", 'info');
      return false;

    }


    if (this.EmployeeMedicalrequestForm.controls.Hosname.value == "" || this.EmployeeMedicalrequestForm.controls.Hosname.value == undefined || this.EmployeeMedicalrequestForm.controls.Hosname.value == null) {
      this.showloader = false;
      Swal.fire('info', "Please Select hospital", 'info');
      return false;

    }
    if (this.EmployeeMedicalrequestForm.controls.disease.value == "" || this.EmployeeMedicalrequestForm.controls.disease.value == undefined || this.EmployeeMedicalrequestForm.controls.disease.value == null) {
      this.showloader = false;
      Swal.fire('info', "Please Select Treatment type", 'info');
      return false;

    }

    if (this.EmployeeMedicalrequestForm.controls.TreatmentAmount.value == "" || this.EmployeeMedicalrequestForm.controls.TreatmentAmount.value == undefined || this.EmployeeMedicalrequestForm.controls.TreatmentAmount.value == null) {
      this.showloader = false;
      Swal.fire('info', "Please Enter TreatmentAmount", 'info');
      return false;

    }




    if (!this.EmployeeMedicalrequestForm.controls.AppendixDocPath.value) {
      this.showloader = false;
      Swal.fire('info', "Please Upload APPLICATION FOR MEDICAL REIMBURSEMENT-APPENDIX-2", 'info');
      return false;
    }

    if (!this.reg.CERTIFICATEADocPath.value) {
      this.showloader = false;
      Swal.fire('info', "Please Upload CERTIFICATE-A (OP)", 'info');
      return false;
    }


    if (!this.reg.NONDRAWALDocPath.value) {
      this.showloader = false;
      Swal.fire('info', "Please Upload NON DRAWAL CERTIFICATE", 'info');
      return false;
    }
    if (!this.reg.DECLARATIONDocPath.value) {
      this.showloader = false;
      Swal.fire('info', "Please Upload DECLARATION (DEPENDENCY)CERTIFICATE", 'info');
      return false;
    }
    if (!this.reg.EMERGENCYDocPath.value) {
      this.showloader = false;
      Swal.fire('info', "Please Upload EMERGENCY ADMISSION CERTIFICATE", 'info');
      return false;
    }
    if (!this.reg.ESSENTIALITYDocPath.value) {
      this.showloader = false;
      Swal.fire('info', "Please Upload ESSENTIALITY CERTIFICATE", 'info');
      return false;
    }
    if (!this.reg.CHECKLISTDocPath.value) {
      this.showloader = false;
      Swal.fire('info', "Please Upload CHECKLIST", 'info');
      return false;
    }

    if (this.EmployeeMedicalrequestForm.controls.check.value == "" || this.EmployeeMedicalrequestForm.controls.check.value == null || this.EmployeeMedicalrequestForm.controls.check.value == undefined) {
      Swal.fire("info", "Please Select the checkbox", "info");
      this.showloader = false;
      return false;
    }
    this.showloader = false;
    this.mrPreviewModal.show();
    this.familrelation = this.EmployeeMedicalrequestForm.controls.Familyrel.value;
    this.relation = this.EmployeeMedicalrequestForm.controls.Relname.value;

    for (var i = 0; i <= this.districts.length - 1; i++) {
      if (this.EmployeeMedicalrequestForm.controls.PDistrict.value == this.districts[i].districT_CODE)
        this.district = this.districts[i].districT_NAME_ENG;

    }


    this.treatmentamount = this.EmployeeMedicalrequestForm.controls.TreatmentAmount.value;
    this.treatmenttype = this.EmployeeMedicalrequestForm.controls.trttype.value;
    this.remarks = this.EmployeeMedicalrequestForm.controls.LoanRemarks.value;

    this.bankholdername = this.EmployeeMedicalrequestForm.controls.Bankholdername.value;
    this.accnumber = this.EmployeeMedicalrequestForm.controls.Accno.value;
    this.branchname = this.EmployeeMedicalrequestForm.controls.Branchname.value;
    this.bankname = this.EmployeeMedicalrequestForm.controls.Bankname.value;
    this.ifsccode = this.EmployeeMedicalrequestForm.controls.Ifsccode.value;

  }


  EmployeepreviewrequestForm = this.formBuilder.group({

    check: ['']
  })
  certificates(val) {
    this.base64sstringpath = "";
    if (val == "1") {
      this.base64sstringpath = this.EmployeeMedicalrequestForm.controls.AppendixDocPath.value;
    }

    if (val == "2") {
      this.base64sstringpath = this.EmployeeMedicalrequestForm.controls.CERTIFICATEADocPath.value;
    }
    if (val == "3") {
      this.base64sstringpath = this.EmployeeMedicalrequestForm.controls.NONDRAWALDocPath.value;
    }
    if (val == "4") {
      this.base64sstringpath = this.EmployeeMedicalrequestForm.controls.DECLARATIONDocPath.value;
    }
    if (val == "5") {
      this.base64sstringpath = this.EmployeeMedicalrequestForm.controls.EMERGENCYDocPath.value;
    }
    if (val == "6") {
      this.base64sstringpath = this.EmployeeMedicalrequestForm.controls.ESSENTIALITYDocPath.value;
    }
    if (val == "7") {
      this.base64sstringpath = this.EmployeeMedicalrequestForm.controls.CHECKLISTDocPath.value;
    }

    this.decryptFile(this.base64sstringpath);
  }

  decryptFile(imagepth: string) {
    if (imagepth) {
      //this.pdf="wwwroot\\MedicalReimbursementRequest\\14-06-2021\\sample_14_06_2021_10_49_59.pdf";
      this.service.decryptFile(imagepth, "DecryptFile")
        .subscribe(data => {
          var ext = imagepth.split('.').pop();
          var filename = imagepth.replace(/^.*[\\\/]/, '');

          if (ext == "pdf") {
            this.downloadPdf(data.docBase64, filename);
          }
          else {
            var newTab = window.open();
            newTab.document.body.innerHTML = '<img src=' + data.docBase64 + ' width="100%" height="100%">';

          }

        },
          error => {
            console.log(error);
          });
    }
    else {

    }
  }

  downloadPdf(base64String, fileName) {
    const source = base64String;//`data:application/pdf;base64,${base64String}`;
    const link = document.createElement("a");
    link.href = source;
    link.download = `${fileName}.pdf`
    link.click();
  }

  Submitcchange() {

  }

  viewPDF(input): void {
    var pdfResult = input;

    let pdfWindow = window.open("")
    pdfWindow.document.write("<iframe width='100%' height='100%' src='data:application/pdf;base64, " + encodeURI(pdfResult) + "'></iframe>")
  }

  Cancelclick() {

    this.reloadCurrentRoute();
  }


  OKclick() {
    this.showloader = true;

    const req = new EmployeeSalaryInputRequest();


    req.INPUT_01 = this.logUsercode;//rowsleted.tokeN_ID this.dep.TokenNo.value;
    req.INPUT_02 = this.EmployeeMedicalrequestForm.controls.Hosname.value;
    req.INPUT_03 = this.EmployeeMedicalrequestForm.controls.disease.value;
    req.INPUT_04 = this.EmployeeMedicalrequestForm.controls.TreatmentAmount.value; //Receipt Out
    req.INPUT_05 = this.EmployeeMedicalrequestForm.controls.trttype.value;
    req.INPUT_06 = this.EmployeeMedicalrequestForm.controls.AppendixDocPath.value;
    req.INPUT_07 = this.EmployeeMedicalrequestForm.controls.CERTIFICATEADocPath.value;
    req.INPUT_08 = this.EmployeeMedicalrequestForm.controls.NONDRAWALDocPath.value;
    req.INPUT_09 = this.EmployeeMedicalrequestForm.controls.DECLARATIONDocPath.value;
    req.INPUT_10 = this.EmployeeMedicalrequestForm.controls.EMERGENCYDocPath.value;
    req.INPUT_11 = this.EmployeeMedicalrequestForm.controls.ESSENTIALITYDocPath.value;
    req.INPUT_12 = this.EmployeeMedicalrequestForm.controls.CHECKLISTDocPath.value;
    req.INPUT_13 = this.EmployeeMedicalrequestForm.controls.Familyrel.value;
    req.INPUT_14 = this.EmployeeMedicalrequestForm.controls.Relname.value;
    req.INPUT_15 = this.EmployeeMedicalrequestForm.controls.PDistrict.value;
    req.USER_NAME = this.logUsercode;
    req.INPUT_17 = this.EmployeeMedicalrequestForm.controls.Bankholdername.value;
    req.INPUT_18 = this.EmployeeMedicalrequestForm.controls.Accno.value;
    req.INPUT_19 = this.EmployeeMedicalrequestForm.controls.Branchname.value;
    req.INPUT_20 = this.EmployeeMedicalrequestForm.controls.Bankname.value;
    req.INPUT_21 = this.EmployeeMedicalrequestForm.controls.Ifsccode.value;
    req.CALL_SOURCE = "WEB";
    this.service.postData(req, "SaveMedicalRequest").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        //this.LoadloanData();
        this.reloadCurrentRoute();
        // this.TaxPaymentForm.reset();
        // this.TaxPaymentModal.hide();
        Swal.fire('success', "Your Request Forwarded to P&A Section", 'success');

      }
      else {
        Swal.fire('warning', data.StatusMessage, 'warning');
        this.showloader = false;
      }
    },
      error => console.log(error));


  }



  Sumbitfun() {

    this.apprModal.show()


  }


  get reg() { return this.EmployeeMedicalrequestForm.controls }





  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.LoadmedicalData();
  }
  LoadmedicalData() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.logUsercode, TYPEID: "EMP_MEDICAL" }

    this.service.postData(obj, "GetFamilynames").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));
  }





  Taxclick() {
    this.EmployeepreviewrequestForm.reset();
    this.EmployeeMedicalrequestForm.reset();
    this.submitted = false;
    //this.TaxPaymentModal.show();
    this.Getbankaccountdetails();
    //this.EmployeeLoanrequestForm.reset();
    //this.Getloanmonths();
    //this.EmployeeLoanrequestForm.controls.Loantype.setValue(''); 

    //this.EmployeeLoanrequestForm.controls.LoanMonths.setValue('');

    this.EmployeeMedicalrequestForm.controls.Familyrel.setValue('');
    this.EmployeeMedicalrequestForm.controls.PDistrict.setValue('');
    this.EmployeeMedicalrequestForm.controls.Hosname.setValue('');
    this.EmployeeMedicalrequestForm.controls.disease.setValue('');

  }

  hideTaxModal() {
    this.progressbar = false;
    this.submitted = false;
    //  this.EmployeeLoanrequestForm.reset();
    this.reloadCurrentRoute();
    this.TaxPaymentModal.hide();
    this.EmployeeMedicalrequestForm.reset();

  }

  hidepreviewTaxModal() {

    this.submitted = false;
    //  this.EmployeeLoanrequestForm.reset();


    this.EmployeepreviewrequestForm.reset();
    this.mrPreviewModal.hide();
  }


  hidemesgModal() {


    this.apprModal.hide();
  }


  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  uploadFile(event) {
    this.seldocpath1 = "";
    this.seldoctype1 = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      this.seldoccat1 = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype1 = 'IMAGE';
      else
        this.seldoctype1 = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        this.progressbar = false;
        return false;
      }

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype1 == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat1);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath1 = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath1 = reader.result as string;
          this.preview = this.seldocpath1;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', "MedicalReimbursementRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress1 = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.message1 = 'Upload success.'
            this.uploadFinished(event.body);
            //this.uploadFinished(this.regsdoc.tostring());
            this.progressbar = true;
          }

        });
    }
  }


  certificateuploadFile(event) {
    this.seldocpath2 = "";
    this.seldoctype2 = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      this.seldoccat2 = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype2 = 'IMAGE';
      else
        this.seldoctype2 = 'PDF';
      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        this.progressbar = false;
        return false;
      }

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype2 == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat2);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath2 = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath2 = reader.result as string;
          this.preview = this.seldocpath2;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file1', fileToUpload, fileToUpload.name);
      formData.append('pagename', "MedicalReimbursementRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress2 = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {

            this.message2 = 'Upload success.'
            this.ceruploadFinished(event.body);
            //this.uploadFinished(this.regsdoc.tostring());
            this.progressbar = true;
          }

        });
    }
  }

  NONDRAWALuploadFile(event) {
    this.seldocpath3 = "";
    this.seldoctype3 = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      this.seldoccat3 = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype3 = 'IMAGE';
      else
        this.seldoctype3 = 'PDF';

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype3 == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat3);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath3 = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath3 = reader.result as string;
          this.preview = this.seldocpath3;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        this.progressbar = false;
        return false;
      }

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file3', fileToUpload, fileToUpload.name);
      formData.append('pagename', "MedicalReimbursementRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress3 = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {

            this.message3 = 'Upload success.'
            this.nondrawaluploadFinished(event.body);
            //this.uploadFinished(this.regsdoc.tostring());
            this.progressbar = true;
          }

        });
    }
  }

  DECLARATIONuploadFile(event) {
    this.seldocpath4 = "";
    this.seldoctype4 = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      this.seldoccat4 = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype4 = 'IMAGE';
      else
        this.seldoctype4 = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        this.progressbar = false;
        return false;
      }

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype4 == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat4);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath4 = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath4 = reader.result as string;
          this.preview = this.seldocpath4;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file4', fileToUpload, fileToUpload.name);
      formData.append('pagename', "MedicalReimbursementRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress4 = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {

            this.message4 = 'Upload success.'
            this.decuploadFinished(event.body);
            //this.uploadFinished(this.regsdoc.tostring());
            this.progressbar = true;
          }

        });
    }
  }
  EMERGENCYuploadFile(event) {
    this.seldocpath5 = "";
    this.seldoctype5 = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      this.seldoccat5 = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype5 = 'IMAGE';
      else
        this.seldoctype5 = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        this.progressbar = false;
        return false;
      }

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype5 == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat5);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath5 = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath5 = reader.result as string;
          this.preview = this.seldocpath5;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file5', fileToUpload, fileToUpload.name);
      formData.append('pagename', "MedicalReimbursementRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress5 = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {

            this.message5 = 'Upload success.'
            this.emeruploadFinished(event.body);
            //this.uploadFinished(this.regsdoc.tostring());
            this.progressbar = true;
          }

        });
    }
  }
  ESSENTIALITYuploadFile(event) {
    this.seldocpath6 = "";
    this.seldoctype6 = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      this.seldoccat6 = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype6 = 'IMAGE';
      else
        this.seldoctype6 = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        this.progressbar = false;
        return false;
      }

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype6 == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat6);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath6 = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath6 = reader.result as string;
          this.preview = this.seldocpath6;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file6', fileToUpload, fileToUpload.name);
      formData.append('pagename', "MedicalReimbursementRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress6 = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {

            this.message6 = 'Upload success.'
            this.essenuploadFinished(event.body);
            //this.uploadFinished(this.regsdoc.tostring());
            this.progressbar = true;
          }

        });
    }
  }
  CHECKLISTuploadFile(event) {
    this.seldocpath7 = "";
    this.seldoctype7 = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      this.seldoccat7 = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype7 = 'IMAGE';
      else
        this.seldoctype7 = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        this.progressbar = false;
        return false;
      }

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype7 == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat7);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath7 = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath7 = reader.result as string;
          this.preview = this.seldocpath7;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file7', fileToUpload, fileToUpload.name);
      formData.append('pagename', "MedicalReimbursementRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress7 = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {

            this.message7 = 'Upload success.'
            this.checkuploadFinished(event.body);
            //this.uploadFinished(this.regsdoc.tostring());
            this.progressbar = true;
          }

        });
    }
  }








  public uploadFinished = (event) => {
    this.EmployeeMedicalrequestForm.controls.AppendixDocPath.setValue(event.fullPath);


  }


  public ceruploadFinished = (event) => {

    this.EmployeeMedicalrequestForm.controls.CERTIFICATEADocPath.setValue(event.fullPath);


  }



  public nondrawaluploadFinished = (event) => {
    this.EmployeeMedicalrequestForm.controls.NONDRAWALDocPath.setValue(event.fullPath);


  }


  public decuploadFinished = (event) => {
    this.EmployeeMedicalrequestForm.controls.DECLARATIONDocPath.setValue(event.fullPath);


  }


  public emeruploadFinished = (event) => {
    this.EmployeeMedicalrequestForm.controls.EMERGENCYDocPath.setValue(event.fullPath);

  }


  public essenuploadFinished = (event) => {

    this.EmployeeMedicalrequestForm.controls.ESSENTIALITYDocPath.setValue(event.fullPath);

  }


  public checkuploadFinished = (event) => {
    this.EmployeeMedicalrequestForm.controls.CHECKLISTDocPath.setValue(event.fullPath);


  }
  // this.EmployeeLoanrequestForm.controls.DocPath.setValue(event.fullPath);

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
  }

  Docpriview(val) {

    // const blob = this.service.s_sd((val).split(',')[1], this.seldoccat);
    // const blobUrl = URL.createObjectURL(blob);
    // this.preview = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
    //this.preview = this.sanitizer.bypassSecurityTrustUrl(val);
    this.preview = val;
    this.previewModal.show();

  }
}
