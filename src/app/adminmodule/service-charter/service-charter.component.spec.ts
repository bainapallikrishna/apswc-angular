import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceCharterComponent } from './service-charter.component';

describe('ServiceCharterComponent', () => {
  let component: ServiceCharterComponent;
  let fixture: ComponentFixture<ServiceCharterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServiceCharterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceCharterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
