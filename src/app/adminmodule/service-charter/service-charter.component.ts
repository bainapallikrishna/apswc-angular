import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import { ServiceCharter } from 'src/app/Interfaces/user';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-service-charter',
  templateUrl: './service-charter.component.html',
  styleUrls: ['./service-charter.component.css']
})
export class ServiceCharterComponent implements OnInit {

  public sectiondesc: string;
  public name: string;
  public selectedsectionname: string;
  public Sections: any;
  public sectiondays: string;
  public servicecharter: ServiceCharter
  constructor(private http: HttpClient, private service: CommonServices) { }

  ngOnInit(): void {

    this.Sections = ["B AND L", "M AND QC", "F AND A", "Engineering", "P AND A", "Conference"];
  }

  keyPress(event: any) {
    let charCode = event.keyCode;
    if ((charCode <= 32) || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) {
      return true;
    }
    else {
      Swal.fire('info', 'Enter Only Characters', 'info');
      return false;
    }
    return true;
  }
  public onSectionSubmit = () => {


    this.servicecharter = {
      Sectionname: this.selectedsectionname,
      name: this.name,
      SectionDescription: this.sectiondesc,
      SectionDays: this.sectiondays


    }
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "INS_SERVICECHARTER";
    req.INPUT_01 = this.selectedsectionname;
    req.INPUT_02 = this.sectiondesc;
    req.INPUT_03 = this.sectiondays;
    req.INPUT_04 = this.selectedsectionname;

    this.service.postData(req, "ServiceCharterInsert").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.statusmessage, 'info');
      }
      else {
        Swal.fire('error', data.statusmessage, 'error');
      }
    });

  }

}
