import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalreimbursementMatserComponent } from './medicalreimbursement-matser.component';

describe('MedicalreimbursementMatserComponent', () => {
  let component: MedicalreimbursementMatserComponent;
  let fixture: ComponentFixture<MedicalreimbursementMatserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalreimbursementMatserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalreimbursementMatserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
