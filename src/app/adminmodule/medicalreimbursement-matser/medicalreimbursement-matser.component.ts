import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import Swal from 'sweetalert2';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { FloatingFilterComponent } from 'ag-grid-community/dist/lib/components/framework/componentTypes';
import { ThemeService } from 'ng2-charts';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-medicalreimbursement-matser',
  templateUrl: './medicalreimbursement-matser.component.html',
  styleUrls: ['./medicalreimbursement-matser.component.css']
})
export class MedicalreimbursementMatserComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  Distvalue = [];
  wheditarray:any;
  MasterHistlist:any;
  dissubsubmitted:boolean=false;
  weighbridgeadd: boolean = false;
  Weighbridgelist: any;
  commoditygrparry: any[]
  Quantitytypes: any;
  Storagetypes: any;
  commoditygrouparray: any;
  ddl: any;
  Varietytypes; any;
  regaarray: any;
  Username: string;
  QPcommodityVarietyarry: any[];
  QPCOMMODITYgrpNAME = [];
  reg1dist: any;
  regseldel: any;
  regionname: any;
  COMMODITYNAME = [];
  commoditygrpname = [];
  commodityEDITvardiv: boolean = false
  commGroupEDITdiv: boolean = false;
  editActdctdiv: boolean = false;
  Farmerslist: any[];
  editRemarksdiv: boolean = false;
  eidtupdatebtn: boolean = false;
  Regiontypesmasterwithdist: any;
  commGroupdiv: boolean = false;
  commdgrpivdivvalidation: boolean = false;
  commoditylistypes: any;
  discEDITsubmitted: boolean = false;
  EDITsubmitted:boolean=false;
  commdivdiv: boolean = false;
  commeditdiv: boolean = false;
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  seldselarr = [];
  unique: any;
  submitted: boolean = false;
  regionsubmitted: boolean = false;
  uniquelocations = [];
  commodityvardiv: boolean = false;
  commdivdivvalidationvar: boolean = false;
  commddlvar: any;
  QPCOMMODITYNAME = [];
  showloader:boolean=false;
  QPGradetypes: any;
  Qualityparameterslist: any;
  Chemicallist: any;
  title:any;
  districts:any;
  WHaddarray:any;
  Treatmentlisttypes:any;
  Hospitallisttypes:any;
  @ViewChild('addmedicalModal') addmedicalModal: ModalDirective;
  @ViewChild('editmedicalModal') editmedicalModal: ModalDirective;
  @ViewChild('historyWarehousemodal') historyWarehousemodal: ModalDirective;
  @ViewChild('addDiseaseModal') addDiseaseModal: ModalDirective;
  @ViewChild('editDiseaseModal') editDiseaseModal: ModalDirective;
  constructor(private formBuilder: FormBuilder, private service: CommonServices, private router: Router, private datef: CustomDateParserFormatter) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }


    else {
      if (this.logUserrole == "101" || this.isPasswordChanged == "1") {


        this.Username = sessionStorage.getItem("logUserCode");

      }
      else {


        this.router.navigate(['/Login']);
      }
    }
    this.LoadDistricts();
  }


  ngOnInit(): void {

  }

  Medicaladdform=this.formBuilder.group({
    Hospitaladdtype:['',Validators.required],
    Medicaladdremarks:['',Validators.required],
    PDistrict:['',Validators.required]

  });
  MedicalEDITform=this.formBuilder.group({
    Districtname:[''],
    HospitalEDITId:[''],
    HospitalEDITtype:[''],
    medactive:[''],
    medEDITremarks:['',Validators.required]

  });

  Diseaseaddform=this.formBuilder.group({
    diseaseaddtype:['',Validators.required],
    diseaseaddremarks:['',Validators.required]

  });

  diseaseEDITform=this.formBuilder.group({
    diseseEDITId:[''],
    diseseEDITtype:[''],
    diseseactive:['',Validators.required],
    medEDITremarks:['',Validators.required]
  });

  LoadDistricts() {
    this.service.getData("GetDistricts").subscribe(data => {

      if (data.StatusCode == "100") {
        this.districts = data.Details;

      }

    },
      error => console.log(error));
  }
gethospitallist(){

  
  this.showloader = true;
  const req = new InputRequest();
  req.TYPEID = "HOSPITALS";
  this.service.postData(req, "GetHospitalslist").subscribe(data => {
    if (data.StatusCode == "100") {
      this.Hospitallisttypes = data.Details;

      this.showloader = false;
      // for(var i=0;i<this.emptypes.length;i++){

      //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
      // }
    }
    else {
      this.showloader = false;
      Swal.fire("info", data.StatusMessage, "info");
    }


  },
    error => console.log(error));

}


gettreatmentlist(){

  
  this.showloader = true;
  const req = new InputRequest();
  req.TYPEID = "TREATMENT";
  this.service.postData(req, "GetHospitalslist").subscribe(data => {
    if (data.StatusCode == "100") {
      this.Treatmentlisttypes = data.Details;

      this.showloader = false;
      // for(var i=0;i<this.emptypes.length;i++){

      //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
      // }
    }
    else {
      this.showloader = false;
      Swal.fire("info", data.StatusMessage, "info");
    }


  },
    error => console.log(error));

}

HOS=0+1;trt=0+1;

Hospitalclick()
{
  if (this.HOS == 1) {
    this.gethospitallist();
    this.HOS++;
  }
  else {
    this.showloader = false;
    this.HOS = 0;
  }

}
AddHospital()
{
  
  this.MedicalEDITform.reset();
  this.Medicaladdform.reset();
  this.title="Hospital";
this.addmedicalModal.show();
}




hidehistorymasterModal(){
  this.historyWarehousemodal.hide();
}


Medicalhistory(val)
{
//    this.locationdiv=false;

 const req = new InputRequest();
 if(val=="Hospitalhistory"){

   req.INPUT_02 = "HOSPITALS";
 
  }
  if(val=="Diseasehistory"){
   req.INPUT_02 = "TREATMENT";

  }
  
  req.INPUT_02="MASTERS";
 this.showloader= true;
 this.service.postData(req, "GetMastersHistory").subscribe(data => {
     if (data.StatusCode == "100") {
       this.MasterHistlist = data.Details;
       this.Medicaladdform.reset();
       this.historyWarehousemodal.show();
       this.showloader= false;
       }
     else{
       Swal.fire("info",data.StatusMessage,"info");
       this.Medicaladdform.reset();
       this.historyWarehousemodal.hide();
       this.showloader= false;  
}
   },
     error => console.log(error));

 }



HospitalEdit(hosid,distname,hosname,hosact)
{
  this.MedicalEDITform.reset();
  this.Medicaladdform.reset();
this.title="Hospital";
this.editmedicalModal.show();
this.MedicalEDITform.controls.Districtname.setValue(distname);
this.MedicalEDITform.controls.HospitalEDITId.setValue(hosid);
this.MedicalEDITform.controls.HospitalEDITtype.setValue(hosname);

this.MedicalEDITform.controls.medactive.setValue(hosact.toString());


//this.MedicalEDITform.controls.medactive.setValue(.tostring());



}



Diseaseclick(){
  if (this.HOS == 1) {
    this.gettreatmentlist();
    this.trt++;
  }
  else {
    this.showloader = false;
    this.trt = 0;
  }
}
AddDisease(){
  
  this.MedicalEDITform.reset();
  this.Medicaladdform.reset();
  this.addDiseaseModal.show();
this.title="Disease";
}

DiseaseEdit(t1,t2,t3)
{
  this.MedicalEDITform.reset();
  this.Medicaladdform.reset();
  this.title="Disease";
  this.editDiseaseModal.show();



  this.diseaseEDITform.controls.diseseEDITId.setValue(t1);
  this.diseaseEDITform.controls.diseseEDITtype.setValue(t2);
  this.diseaseEDITform.controls.diseseactive.setValue(t3.toString());
}
Medicaladdclick()
{

  this.showloader = true;
  const req = new InputRequest();
  this.submitted = true;
  if (this.Medicaladdform.invalid) {
    this.showloader = false;
    return;
  }
 
  
  req.INPUT_01=this.Medicaladdform.controls.PDistrict.value;
  
  req.INPUT_02 = this.Medicaladdform.controls.Hospitaladdtype.value;
    req.INPUT_03 = this.logUserrole;
    req.USER_NAME = this.Username;
    req.INPUT_04 =this.Medicaladdform.controls.Medicaladdremarks.value ;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "SaveHospitalDetails").subscribe(data => {
      this.WHaddarray = data.result;
      if (this.WHaddarray.StatusCode == "100") {

        if (this.WHaddarray.Details[0].rtN_ID == "1") {
          this.HOS = 0;
          this.gethospitallist();
          this.Diseaseaddform.reset();
          this.diseaseEDITform.reset();
          this.Medicaladdform.reset();
          this.MedicalEDITform.reset();
          this.addmedicalModal.hide();

          this.showloader = false;
          Swal.fire("success", "Data Added Successfully", "success");

            }
        else {
          Swal.fire("info", "This Master IsAlready Registered", "info");

          this.Medicaladdform.reset();
          this.MedicalEDITform.reset();
          this.addmedicalModal.hide();
          this.showloader = false;

        }
      }

      else {
        Swal.fire("info", this.WHaddarray.StatusMessage, "info");
        this.addmedicalModal.hide();

        this.showloader = false;

      }
    },
      error => console.log(error));





}

Diseaseaddclick(){

  this.showloader = true;
  const req = new InputRequest();
  this.dissubsubmitted = true;
  if (this.Diseaseaddform.invalid) {
    this.showloader = false;
    return;
  }
  req.INPUT_01=this.Diseaseaddform.controls.diseaseaddtype.value;
  req.INPUT_03 = this.Diseaseaddform.controls.diseaseaddremarks.value;
    req.INPUT_02 = this.logUserrole;
    req.USER_NAME = this.Username;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "SaveDiseaseDetails").subscribe(data => {
      this.WHaddarray = data.result;
      if (this.WHaddarray.StatusCode == "100") {

        if (this.WHaddarray.Details[0].rtN_ID == "1") {
          this.trt = 0;
          this.Medicaladdform.reset();
          this.MedicalEDITform.reset();
        
        
          this.Diseaseaddform.reset();
          this.diseaseEDITform.reset();
        
          this.addDiseaseModal.hide();
          this.gettreatmentlist();
          this.showloader = false;
          Swal.fire("success", "Data Added Successfully", "success");

            }
        else {
          Swal.fire("info", "This Master IsAlready Registered", "info");

          this.Medicaladdform.reset();
          this.MedicalEDITform.reset();
          this.Diseaseaddform.reset();
          this.diseaseEDITform.reset();
          this.addDiseaseModal.hide();
          this.showloader = false;

        }
      }

      else {
        Swal.fire("info", this.WHaddarray.StatusMessage, "info");
        this.addDiseaseModal.hide();

        this.showloader = false;

      }
    },
      error => console.log(error));



}
DiseaseEDITclick(){
  this.showloader = true;
  const req = new InputRequest();
  this.discEDITsubmitted = true;
  if (this.diseaseEDITform.invalid) {
    this.showloader = false;
    return;
  }
  req.INPUT_01 = this.diseaseEDITform.controls.diseseEDITId.value;
  req.INPUT_02 = this.diseaseEDITform.controls.diseseactive.value;
  req.INPUT_03 = this.diseaseEDITform.controls.medEDITremarks.value;
  req.USER_NAME = this.Username;
  req.INPUT_04 = this.logUserrole;
  req.CALL_SOURCE = "Web";
  this.service.postData(req, "updateDiseasedetails").subscribe(data => {
    this.wheditarray = data.result;
    if (this.wheditarray.StatusCode == "100") {
      if (this.wheditarray.Details[0].rtN_ID == "1") {
        this.trt = 0;
        this.gettreatmentlist();

        this.Medicaladdform.reset();
        this.MedicalEDITform.reset();
        this.editDiseaseModal.hide();
        this.addmedicalModal.hide();
        this.Diseaseaddform.reset();
        this.diseaseEDITform.reset();
this.showloader=false;
        Swal.fire("success", "Data Updated Successfully", "success");
      }
      else {
        Swal.fire("info", "Data Updation Failed", "info");
        this.Medicaladdform.reset();
        this.MedicalEDITform.reset();
        this.editDiseaseModal.hide();
        this.addmedicalModal.hide();
        this.Diseaseaddform.reset();
        this.diseaseEDITform.reset();
        this.showloader=false;
      }
      // this.f.Godownswarehsetype.setValue(editid);
    }

    else {
      Swal.fire("info", this.wheditarray.StatusMessage, "info");



    }
    // this.f.editemptype.setValue(editname);
  },
    error => console.log(error));




}
hideeditWarehouseModal(){
  this.editDiseaseModal.hide();
  this.editmedicalModal.hide();
}

hideWarehouseModal(){
  this.addmedicalModal.hide();
  this.addDiseaseModal.hide();
}
MedicalEDITclick()
{

  this.showloader = true;

  const req = new InputRequest();

  this.EDITsubmitted = true;


  if (this.MedicalEDITform.invalid) {
    this.showloader = false;
    return;

  }
  
    req.INPUT_01 = this.MedicalEDITform.controls.HospitalEDITId.value;
    req.INPUT_02 = this.MedicalEDITform.controls.medactive.value;
    req.INPUT_03 = this.MedicalEDITform.controls.medEDITremarks.value;
    req.USER_NAME = this.Username;
    req.INPUT_04 = this.logUserrole;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "updatehospitaldetails").subscribe(data => {
      this.wheditarray = data.result;
      if (this.wheditarray.StatusCode == "100") {
        if (this.wheditarray.Details[0].rtN_ID == "1") {
          this.HOS = 0;
          this.gethospitallist();

          this.Medicaladdform.reset();
          this.MedicalEDITform.reset();
          this.editmedicalModal.hide();
          this.addmedicalModal.hide();
this.showloader=false;
          this.Diseaseaddform.reset();
          this.diseaseEDITform.reset();
          Swal.fire("success", "Data Updated Successfully", "success");
        }
        else {
          Swal.fire("info", "Data Updation Failed", "info");
          this.Medicaladdform.reset();
          this.MedicalEDITform.reset();
          this.editmedicalModal.hide();
          this.addmedicalModal.hide();
          this.Diseaseaddform.reset();
          this.diseaseEDITform.reset();

          this.showloader=false;
        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.wheditarray.StatusMessage, "info");


        this.showloader=false;

      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));




}
get f1() { return this.Medicaladdform.controls; }

get f() { return this.MedicalEDITform.controls; }




get dis() { return this.Diseaseaddform.controls; }

get dis1() { return this.diseaseEDITform.controls; }

}
