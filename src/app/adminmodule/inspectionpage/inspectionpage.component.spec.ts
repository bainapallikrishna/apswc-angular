import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectionpageComponent } from './inspectionpage.component';

describe('InspectionpageComponent', () => {
  let component: InspectionpageComponent;
  let fixture: ComponentFixture<InspectionpageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectionpageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectionpageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
