import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpRequest, HttpEventType, HttpResponse } from '@angular/common/http';
import { User, Inspection, WareHouseMaster } from 'src/app/Interfaces/user'
import { from } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';
import { ActivatedRoute, Router } from '@angular/router';

import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-inspectionpage',
  templateUrl: './inspectionpage.component.html',
  styleUrls: ['./inspectionpage.component.css']
})
export class InspectionpageComponent implements OnInit {
  public progress: number;
  public message: string;
  public response: { dbPath: '' };
  public isCreate: boolean;
  public name: string;
  public inspectiondesc: string;
  public selectedWarehousename: any;
  public inspection: Inspection;
  public users: Inspection[] = [];
  public warehouse: WareHouseMaster[] = [];
  public baseurl: string;
  public FileData: any;

  constructor(private http: HttpClient) { }

  keyPress(event: any) {
    let charCode = event.keyCode;
    if ((charCode <= 32) || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) {
      return true;
    }
    else {
      Swal.fire('info', 'Enter Only Characters', 'info');
      return false;
    }
    return true;
  }

  ngOnInit(): void {
    ;

    this.isCreate = true;
    this.baseurl = "https://apswc.ap.gov.in/APSWCAPP" //https://localhost:44310
    this.getWarehouseMaster();
    this.getInspectionlist();
  }
  public onCreate = () => {

    const req = new InputRequest();
    // this.inspection = {
    //   WarehouseId:this.selectedWarehousename.warehouseCode,
    //   WarehouseName: this.selectedWarehousename.warehouseName,
    //   Description: this.inspectiondesc,
    //   uploadeddate:new Date(),
    //   FilePath: this.response.dbPath
    // }
    if (this.name === '' || this.name === null || this.name === undefined) {
      Swal.fire('info', 'Please Enter Name', 'info');
      return false;
    }
    if (this.selectedWarehousename === '' || this.selectedWarehousename === null || this.selectedWarehousename === undefined) {
      //alert("Please Select WareHouse Name");
      Swal.fire('info', 'Please Select WareHouse Name', 'info');
      return false;
    }
    if (this.inspectiondesc === '' || this.inspectiondesc === null || this.inspectiondesc === undefined) {
      //alert("Please Enter Inspection Description");
      Swal.fire('info', 'Please Enter Inspection Description', 'info');
      return false;
    }
    //  if (this.file === '' || this.file === null || this.file === undefined) {
    //   alert("Please Upload File");
    //   return;
    //  }

    console.log(this.selectedWarehousename.warehouseCode);
    this.inspection = {
      WarehouseId: this.selectedWarehousename.warehouseCode,
      WarehouseName: this.selectedWarehousename.warehouseName,
      Description: this.inspectiondesc,
      uploadeddate: new Date(),
      FilePath: this.response.dbPath
    }

    req.DIRECTION_ID = "2";
    req.TYPEID = "201";
    req.INPUT_01 = this.selectedWarehousename.warehouseName;
    req.INPUT_02 = this.inspectiondesc;
    req.INPUT_03 = new Date().toString();
    req.INPUT_04 = this.selectedWarehousename.warehouseCode;
    req.INPUT_05 = "Inspection";
    req.INPUT_06 = this.response.dbPath;

    this.http.post(this.baseurl + '/api/APSWC/InspectionRegistration', this.inspection)
      .subscribe(res => {
        this.getInspectionlist();
        this.isCreate = false;
      });
  }

  public EditInspection = (id: string) => {

  }
  public DeleteInspection = (id: string) => {
    var filtered = this.users.filter(function (item) {
      return item.WarehouseId !== id;

    });
    console.log(filtered);

    this.users = filtered;
  }
  public returnToCreate = () => {
    this.isCreate = true;
    this.selectedWarehousename = '';
    this.inspectiondesc = '';
  }
  private getInspectionlist = () => {
    this.http.get(this.baseurl + '/api/APSWC/InsecpctionList')
      .subscribe(res => {
        this.users = res as Inspection[];
        console.log(this.users);
      });
  }

  private getWarehouseMaster = () => {
    this.http.get(this.baseurl + '/api/APSWC/WareHouseMaster')
      .subscribe(res => {
        this.warehouse = res as WareHouseMaster[];
        console.log(this.warehouse);
      });
  }


  public uploadFinished = (event) => {
    this.response = event;

  }

  public createImgPath = (serverPath: string) => {
    return this.baseurl + `/${serverPath}`;
  }



  // upload(files) {  
  //   if (files.length === 0)  
  //     return;  

  //   const formData = new FormData();  
  //   //formData.append('ImageUpload', this.Inspectionfile.nativeElement.files[0]);  
  //   for (let file of files)  
  //     formData.append(file.name, file);  

  //   const uploadReq = new HttpRequest('POST', `https://localhost:44310/api/FilesUpload/UploadFileDetails/`, formData, {  
  //     reportProgress: true,  
  //   });  

  //   this.http.request(uploadReq).subscribe(event => {  
  //     console.log(event);
  //     if (event.type === HttpEventType.UploadProgress)  
  //       this.progress = Math.round(100 * event.loaded / event.total);  
  //     else if (event.type === HttpEventType.Response)  
  //     {
  //        this.uploadFinished(event);  
  //        this.message="Upload success.";
  //     }

  //   });  
  //}  
}
