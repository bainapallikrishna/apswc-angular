import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { HistorybuttonRendererComponent } from 'src/app/custome-directives/Historybutton-renderer.component';
import { AadharValidateService } from 'src/app/Services/aadhar-validate.service';
import { CommonServices } from 'src/app/Services/common.services';
import { AgGridAngular } from 'ag-grid-angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { DigiServices } from 'src/app/Services/Digilocker.services';

@Component({
  selector: 'app-outsourcing-agencies',
  templateUrl: './outsourcing-agencies.component.html',
  styleUrls: ['./outsourcing-agencies.component.css']
})
export class OutsourcingAgenciesComponent implements OnInit {

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");

  showloader: boolean = true;
  modeltitle: string = "Add";
  isAdmin: boolean = false;
  isEdit: boolean = false;
  frameworkComponents: any;
  public components;
  defaultColDef: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];
  OutSourceForm: FormGroup;
  isSubmit: boolean = false;
  states: any = [];
  districts: any = [];
  urlist: any = [];
  nmandals: any = [];
  nvillages: any = [];
  Pincodedata:any=[];
  public Emp_mob_isvalid: boolean = true;
  oursources: any = [];
  wh_history: any = [];


  SectionID: any;
  code: any;
  Accesstoken: any;
  FilesDD: any;
  Docpath: any;
  Authstatus: any;
  UploadFilesDD: any;
  IssuedDocView: boolean = true;
  viewbtnname: any;
  uri: any;
  DigiDetails: any[];
  showDocpath: any;
  hrefdata: any;
  Details: any;
  name: any;
  dob: any;
  gender: any;


  progress: number = 0;
  message: string = "";
  UploadDocType: string = "";
  UploadDocPath: string = "";

  PanPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  GstPattern = "^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$";
  IFSCPattern = "^[A-Z]{4}0[A-Z0-9]{6}$";

  vname: string = "";
  vstartdate: string = "";
  venddate: string = "";
  vpanno: string = "";
  vgstno: string = "";
  vstate: string = "";
  vdistrict: string = "";
  varea: string = "";
  vmandal: string = "";
  vvillage: string = "";
  vhno: string = "";
  vstreet: string = "";
  vlanmark: string = "";
  vpincode: string = "";
  vdocumentpath: string = "";
  vastatus: string = "";
  vofficals: any = [];

  preview: string = "";
  seldocpath: any;
  seldoctype: string = "";
  seldoccat: string = "";

  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Agency Name', width: 200, field: 'name', cellRenderer: 'actionrender' },
    // { headerName: 'PAN No', width: 150, field: 'paN_NO' },
    // { headerName: 'GST No', width: 250, field: 'gstN_NO' },
    { headerName: 'Address', width: 300, field: 'address' },
    { headerName: 'Agreement Start Date', width: 150, field: 'starT_DATE', },
    { headerName: 'Agreement End Date', width: 150, field: 'enD_DATE' },
    { headerName: 'Status', width: 100, field: 'status' },
    { headerName: 'Action', width: 100, floatingFilter: false, cellRenderer: 'buttonRenderer' }
  ];

  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private uidservice: AadharValidateService,
    private datef: CustomDateParserFormatter,
    private datecon: NgbDatepickerConfig,
    private digiservice: DigiServices,
    private sanitizer: DomSanitizer) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }

    if (this.logUserrole == "101" || this.logUserrole == "102")
      this.isAdmin = true;

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText: true,
      autoHeight: true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.frameworkComponents = {
      buttonRenderer: HistorybuttonRendererComponent,
    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
      actionrender: function (params) {

        var eSpan3 = document.createElement('div');

        eSpan3.innerHTML = '<u style="color:blue" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + params.value + '</u>' +
          '<div class="dropdown-menu more-option-menu" aria-labelledby="dropdownMenuButton">' +
          '<span class="dropdown-item text-center EditClick"><i class="fa fa-pencil-square-o">Edit</i></span>' +
          '<span class="dropdown-item text-center ViewClick"><i class="fa fa-eye">View</i></span>' +
          '</div>';

        return eSpan3;

      },
    };
  }

  dataclicked(params) {
    if (params.colDef.cellRenderer == "actionrender") {
      if (params.event.path[0].classList.value == "dropdown-item text-center EditClick" || params.event.path[0].classList.value == "fa fa-pencil-square-o") {
        this.EditOutSourcing(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center ViewClick" || params.event.path[0].classList.value == "fa fa-eye") {
        this.ViewOutSourcing(params);
      }
    }
    if (params.colDef.cellRenderer == "buttonRenderer") {
      this.HistoryOutSourcing(params);
    }
  }

  @ViewChild('agGrid') public agGrid: AgGridAngular;
  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('historyModal') public historyModal: ModalDirective;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('previewModal') public previewModal: ModalDirective;
  @ViewChild('DetailList') public DetailList: ModalDirective;
  @ViewChild('FilesList') public FilesList: ModalDirective;
  ngOnInit(): void {
    let now: Date = new Date();

    this.OutSourceForm = this.formBuilder.group({
      OursouceCode: [''],
      OutName: ['', Validators.required],
      StartDate: ['', Validators.required],
      EndDate: ['', Validators.required],
      PANNo: ['', [Validators.required, Validators.minLength(10), Validators.pattern(this.PanPattern)]],
      GSTNo: ['', [Validators.required, Validators.minLength(10), Validators.pattern(this.GstPattern)]],
      NState: [null, Validators.required],
      NDistrict: [null, Validators.required],
      NArea: [null, Validators.required],
      NMandal: [null, Validators.required],
      NVillage: [null, Validators.required],
      NHNO: ['', Validators.required],
      NStrName: ['', Validators.required],
      NLandmark: [''],
      NPincode: ['', Validators.required],
      OutDoc: [''],
      IsOutActive: ['1'],
      OutRemarks: [''],
      oldOutDoc: [''],
      officials: new FormArray([
        this.formBuilder.group({
          CName: ['', Validators.required],
          CDesignation: ['', Validators.required],
          CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
          CEmail: [''],
        })
      ])
    });
    this.LoadStates();
    this.LoadDistricts();
    this.LoadurFlags();
    this.LoadStates();
    this.LoadData();

  }

  get reg() { return this.OutSourceForm.controls; }
  get c() { return this.reg.officials as FormArray; }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    params.api.setRowData(this.oursources);
    //this.gridApi.sizeColumnsToFit();

  }

  LoadData() {
    this.service.getData("GetOutsourcingAgencies").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.oursources = data.Details;
        console.log(this.oursources);
        this.gridApi.setRowData(this.oursources);
      }
    },
      error => console.log(error));

  }

  mob_uid_validate(mobilephone) {
    if (mobilephone.length === 10)
      this.Emp_mob_isvalid = this.uidservice.validatemob(mobilephone);
  }

  AddOfficial() {

    this.c.push(this.formBuilder.group({
      CName: ['', Validators.required],
      CDesignation: ['', Validators.required],
      CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      CEmail: [''],
    }));
  }

  RemoveOfficial(index) {

    this.c.removeAt(index);
  }

  AddOutSourcing() {
    this.modeltitle = "Add";
    this.resetallData();
    this.isSubmit = false;
    this.isEdit = false;
    this.progress = 0;
    this.message = "";
    this.c.push(this.formBuilder.group({
      CName: ['', Validators.required],
      CDesignation: ['', Validators.required],
      CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      CEmail: [''],
    }));

    this.editModal.show();
  }

  HistoryOutSourcing(row) {

    this.showloader = true;

    const req = new InputRequest();
    if (row == 'all') {
      this.isEdit = true;
      req.INPUT_01 = null;
    }
    else {
      this.isEdit = false;
      req.INPUT_01 = row.data.outsourcE_ID;
    }

    req.INPUT_02 = "OUTSOURCING";

    this.service.postData(req, "GetEmployeeHistory").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.wh_history = data.Details;
        this.historyModal.show();
      }

    },
      error => console.log(error));


  }

  EditOutSourcing(row) {

    this.isEdit = true;
    this.isSubmit = false;
    this.modeltitle = "Edit";
    this.resetallData();
    this.GetAgencyDetails(row, 'edit');
    this.GetAgencyContactDetails(row, 'edit');


  }

  ViewOutSourcing(row) {
    this.GetAgencyDetails(row, 'view');
    this.GetAgencyContactDetails(row, 'view');
  }

  GetAgencyDetails(row, ptype: string) {
    this.showloader = true;
    const rtype: string = ptype;
    const req = new InputRequest();
    req.INPUT_01 = row.data.outsourcE_ID;

    this.service.postData(req, "GetDetailsofOutsourcing").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (rtype == 'edit')
          this.FillEditAgencyDetails(data.Details[0]);
        else if (rtype == 'view')
          this.FillViewAgencyDetails(data.Details[0]);
      }

    },
      error => console.log(error));
  }

  GetAgencyContactDetails(row, ptype: string) {
    this.showloader = true;
    const rtype: string = ptype;
    const req = new InputRequest();
    req.INPUT_01 = row.data.outsourcE_ID;

    this.service.postData(req, "GetOutsourceContactDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (rtype == 'edit')
          this.FillAgencyContactDetails(data.Details);
        else if (rtype == 'view')
          this.vofficals = data.Details;
      }
      else {
        if (rtype == 'edit') {
          this.c.push(this.formBuilder.group({
            CName: ['', Validators.required],
            CDesignation: ['', Validators.required],
            CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
            CEmail: [''],
          }));
        }
      }

    },
      error => console.log(error));
  }

  resetallData() {
    this.OutSourceForm.reset();
    this.c.reset();
    this.c.clear();
    this.progress = 0;
    this.message = "";
    this.UploadDocType = "";
    this.UploadDocPath = "";
    this.Docpath = "";
  }


  FillEditAgencyDetails(data) {
    this.reg.OursouceCode.setValue(data.outsourcE_ID)
    this.reg.OutName.setValue(data.name);
    this.reg.StartDate.setValue(this.datef.parse((data.starT_DATE ? data.starT_DATE : "").replace("T00:00:00", "")))
    this.reg.EndDate.setValue(this.datef.parse((data.enD_DATE ? data.enD_DATE : "").replace("T00:00:00", "")))
    this.reg.PANNo.setValue(data.paN_NO);
    this.reg.GSTNo.setValue(data.gstN_NO);
    this.reg.NState.setValue(data.state);
    this.reg.NDistrict.setValue(data.districT_CODE)
    this.reg.NArea.setValue(data.ruraL_URBAN)

    this.DistrictChange();
    this.reg.NMandal.setValue(data.mmC_CODE);

    this.MandalChange();
    this.reg.NVillage.setValue(data.warD_VILLAGE_CODE);

    this.reg.NHNO.setValue(data.hno);
    this.reg.NStrName.setValue(data.street);
    this.reg.NLandmark.setValue(data.landmark);
    this.reg.NPincode.setValue(data.pincode);
    this.reg.oldOutDoc.setValue(data.contracT_DOCUMENT);
    this.reg.IsOutActive.setValue(data.status == 'Active' ? '1' : '0')
    this.reg.OutRemarks.setValue(data.remarks)

    this.editModal.show();
  }

  FillAgencyContactDetails(data) {

    for (let i = 0; i < data.length; i++) {
      // if (i != 0) {
      this.c.push(this.formBuilder.group({
        CName: ['', Validators.required],
        CDesignation: ['', Validators.required],
        CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
        CEmail: [''],
      }));
      // }

      this.c.controls[i].patchValue({
        CName: data[i].name,
        CDesignation: data[i].designation,
        CMobile: data[i].mobilE_NUM,
        CEmail: (data[i].email)
      });

    }
  }

  FillViewAgencyDetails(data) {
    this.vname = data.name;
    this.vstartdate = data.starT_DATE;
    this.venddate = data.enD_DATE;
    this.vpanno = data.paN_NO;
    this.vgstno = data.gstN_NO;
    this.vstate = data.statE_NAME;
    this.vdistrict = data.districT_NAME;
    this.varea = data.ruraL_URBAN1;
    this.vmandal = data.mmC_NAME;
    this.vvillage = data.warD_VILLAGE_NAME;
    this.vhno = data.hno;
    this.vstreet = data.street;
    this.vlanmark = data.landmark;
    this.vpincode = data.pincode;
    this.vdocumentpath = data.contracT_DOCUMENT;
    this.vastatus = data.status;

    this.viewModal.show();
  }



  CloseEditModal() {
    this.LoadData();
    this.editModal.hide();
  }

  SubmitOutDetails() {
    this.isSubmit = true;


    // stop here if form is invalid
    if (this.OutSourceForm.invalid) {
      return false;
    }


    if (!this.Emp_mob_isvalid) {
      return false;
    }

    if (!this.UploadDocPath && !this.Docpath) {
      Swal.fire('warning', "Please upload the document", 'warning');
      return false;
    }

    if (this.datef.GreaterDate(this.reg.StartDate.value, this.reg.EndDate.value)) {
      Swal.fire('warning', "End Date Should be greater than Start Date", 'warning');
      return false;
    }

    let namelist: string[] = [];
    let deslist: string[] = [];
    let moblist: string[] = [];
    let mailist: string[] = [];

    for (let i = 0; i < this.c.length; i++) {

      namelist.push(this.c.value[i].CName);
      deslist.push(this.c.value[i].CDesignation);
      moblist.push(this.c.value[i].CMobile);
      mailist.push(this.c.value[i].CEmail);
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.reg.OutName.value;
    req.INPUT_02 = this.datef.format(this.reg.StartDate.value);
    req.INPUT_03 = this.datef.format(this.reg.EndDate.value);
    req.INPUT_04 = this.reg.PANNo.value;
    req.INPUT_05 = this.reg.GSTNo.value;
    req.INPUT_06 = this.reg.NState.value;
    req.INPUT_07 = this.reg.NDistrict.value;
    req.INPUT_08 = this.reg.NArea.value;
    req.INPUT_09 = this.reg.NMandal.value;
    req.INPUT_10 = this.reg.NVillage.value;
    req.INPUT_11 = this.reg.NHNO.value;
    req.INPUT_12 = this.reg.NStrName.value;
    req.INPUT_13 = this.reg.NLandmark.value;
    req.INPUT_14 = this.reg.NPincode.value;
    if(this.Authstatus == "1")
    {
      req.INPUT_15=this.Docpath
    }
    else
    {
      req.INPUT_15 = this.UploadDocPath;
    }
    
    req.INPUT_16 = "1";
    req.INPUT_17 = namelist.join(',');
    req.INPUT_18 = deslist.join(',');
    req.INPUT_19 = moblist.join(',');
    req.INPUT_20 = mailist.join(',');


    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveOutsourcingAgency").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Outsourcing Agency Details Saved Successfully !!!", 'success');
        console.log(data);
        this.LoadData();
        this.editModal.hide();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }

  UpdateOutDetails() {
    this.isSubmit = true;
    // stop here if form is invalid
    if (this.OutSourceForm.invalid) {
      return false;
    }

    if (!this.Emp_mob_isvalid) {
      return false;
    }

    if (this.reg.IsOutActive.value == "0" && !this.reg.OutRemarks.value) {
      Swal.fire('warning', "Remarks is Required ", 'warning');
      return false;
    }

    if (this.datef.GreaterDate(this.reg.StartDate.value, this.reg.EndDate.value)) {
      Swal.fire('warning', "End Date Should be greater than Start Date", 'warning');
      return false;
    }

    let namelist: string[] = [];
    let deslist: string[] = [];
    let moblist: string[] = [];
    let mailist: string[] = [];

    for (let i = 0; i < this.c.length; i++) {

      namelist.push(this.c.value[i].CName);
      deslist.push(this.c.value[i].CDesignation);
      moblist.push(this.c.value[i].CMobile);
      mailist.push(this.c.value[i].CEmail);
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.reg.OursouceCode.value;
    req.INPUT_02 = this.reg.NState.value;
    req.INPUT_03 = this.reg.NDistrict.value;
    req.INPUT_04 = this.reg.NArea.value;
    req.INPUT_05 = this.reg.NMandal.value;
    req.INPUT_06 = this.reg.NVillage.value;
    req.INPUT_07 = this.reg.NHNO.value;
    req.INPUT_08 = this.reg.NStrName.value;
    req.INPUT_09 = this.reg.NLandmark.value;
    req.INPUT_10 = this.reg.NPincode.value;
    req.INPUT_11 = this.UploadDocPath ? this.UploadDocPath : this.reg.oldOutDoc.value;
    req.INPUT_12 = this.reg.IsOutActive.value;
    req.INPUT_13 = this.reg.OutRemarks.value;
    req.INPUT_14 = this.datef.format(this.reg.EndDate.value);
    req.INPUT_15 = namelist.join(',');
    req.INPUT_16 = deslist.join(',');
    req.INPUT_17 = moblist.join(',');
    req.INPUT_18 = mailist.join(',');


    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "UpdateOutsourcingAgency").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Outsourcing Agency Details Updated Successfully !!!", 'success');
        console.log(data);
        this.LoadData();
        this.editModal.hide();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  uploadFile(event) {

    this.message = "";
    this.UploadDocType = "";
    this.UploadDocPath = "";
    this.seldocpath = "";
    this.seldoctype = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }
      this.seldoccat = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype = 'IMAGE';
      else
        this.seldoctype = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath = reader.result as string;
          this.preview = this.seldocpath;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();


      formData.append('file', fileToUpload, fileToUpload.name);

      formData.append('regid', "OustSoucingAgencies");
      formData.append('category', this.reg.PANNo.value);
      formData.append('pagename', "Outsourcing Agencies");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)

            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.';
            this.UploadDocType = this.seldoctype;
            this.Docpath="";
            this.Authstatus="";
            this.uploadFinished(event.body);
          }

        });
    }
  }

  public createImgPath = (serverPath: string) => {

    return `${this.service.filebaseUrl.replace("api/FilesUpload", "")}${serverPath}`;

  }

  public uploadFinished = (event) => {
    this.UploadDocPath = event.fullPath;
  }

  LoadStates() {
    this.service.getData("GetStates").subscribe(data => {
      if (data.StatusCode == "100") {
        this.states = data.Details;
      }
    },
      error => console.log(error));
  }

  PincodeDetails() {

    if (this.reg.NPincode.value.length == "6") {
      this.districts = [];
      this.urlist = [];
      this.nmandals = [];
      this.reg.NDistrict.setValue(null);
      this.reg.NArea.setValue(null);
      this.reg.NMandal.setValue(null);
      this.showloader = true;
      const req = new InputRequest();
      req.INPUT_01 = this.reg.NPincode.value;
      this.service.postData(req, "GetPincodeDetails").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.Pincodedata = data.Details;

          if (data.Details.length == 1) {

            this.districts = [{ districT_CODE: data.Details[0].districT_CODE, districT_NAME_ENG: data.Details[0].districT_NAME }];
            this.urlist = data.Details;
            this.nmandals = data.Details;
            this.reg.NDistrict.setValue(data.Details[0].districT_CODE);
            this.reg.NArea.setValue(data.Details[0].id);
            this.reg.NMandal.setValue(data.Details[0].mmC_CODE);
            if (this.reg.NDistrict.value && this.reg.NMandal.value) {
              this.LoadVillages(this.reg.NDistrict.value, this.reg.NMandal.value)
            }


          }
          {

            this.LoadDistricts();
            this.LoadurFlags();
          }

        }
        else {

          this.LoadDistricts();
          this.LoadurFlags();
        }

      },
        error => console.log(error));
    }


  }

  LoadDistricts() {
    this.service.getData("GetDistricts").subscribe(data => {
      if (data.StatusCode == "100") {
        this.districts = data.Details;
      }
    },
      error => console.log(error));
  }

  LoadurFlags() {
    this.service.getData("GetAreaTypes").subscribe(data => {
      if (data.StatusCode == "100") {
        this.urlist = data.Details;
      }
    },
      error => console.log(error));
  }

  DistrictChange() {
    this.nmandals = [];
    this.nvillages = [];
    this.reg.NMandal.setValue(null);
    this.reg.NVillage.setValue(null);
    if (this.reg.NDistrict.value && this.reg.NArea.value) {
      this.LoadMandals(this.reg.NDistrict.value, this.reg.NArea.value)
    }
  }

  LoadMandals(distval: string, areaval: string) {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = areaval;
    req.INPUT_02 = distval;

    this.service.postData(req, "GetMandlas").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.nmandals = data.Details;
      }

    },
      error => console.log(error));
  }

  MandalChange() {
    this.nvillages = [];
    this.reg.NVillage.setValue(null);
    if (this.reg.NDistrict.value && this.reg.NMandal.value) {
      this.LoadVillages(this.reg.NDistrict.value, this.reg.NMandal.value)
    }
  }

  LoadVillages(distval: string, manval: string) {
    this.showloader = true;
    let obj: any = { "INPUT_02": distval, "INPUT_03": manval }

    this.service.postData(obj, "GetVillages").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.nvillages = data.Details;
      }

    },
      error => console.log(error));
  }

  Docpriview(val) {

    // const blob = this.service.s_sd((val).split(',')[1], this.seldoccat);
    // const blobUrl = URL.createObjectURL(blob);
    // this.preview = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
    //this.preview = this.sanitizer.bypassSecurityTrustUrl(val);
    //this.preview = this.preview.changingThisBreaksApplicationSecurity;
    this.previewModal.show();

  }
  

  CallDigilock() {
    if (!this.FilesDD && !this.UploadFilesDD) {
      this.showloader = true;
      var pagename = "OutsourcingAgencies";
      //this.digiservice.Digilock(pagename,(function(a) {
      this.digiservice.Digilock(pagename, (a, b) => {
        this.Accesstoken = b;
        this.FilesDD = a.UserFiles.items;
        if (a.UserUploadFiles)
          this.UploadFilesDD = a.UserUploadFiles.items;
        this.FilesList.show();
        this.showloader = false;
      });
    }
    else {
      this.FilesList.show();
    }
  }

  ShowFiles(val, mimer, num, name) {
    this.showloader = true;
    this.Docpath="";

    this.digiservice.ShowFiles(val, mimer, num, name, this.Accesstoken, (a, b) => {
      this.Docpath = a;
      this.decryptFile1(this.Docpath);
      this.uri = b;
      this.Authstatus = "1";
      this.viewbtnname = name.substring(0, 5);
      this.FilesList.hide();
      
    });
  }

decryptFile1(filepath) {
  if (filepath) {
    this.showloader = true;
    this.service.decryptFile(filepath, "DecryptFile")
      .subscribe(data => {
        let blob : any;
        console.log(data.docBase64);

        if(data.docBase64.includes('application/pdf'))
        {
         blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
        
        }
        else if(data.docBase64.includes('image/png'))
        {
           blob = this.service.s_sd((data.docBase64).split(',')[1], "image/png");
           
        }
        else if(data.docBase64.includes('image/jpg') || data.docBase64.includes('image/jpeg') )
        {
           blob = this.service.s_sd((data.docBase64).split(',')[1], "image/jpg");
        }
        const blobUrl = URL.createObjectURL(blob);  
        this.hrefdata=this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        this.showloader = false;

      },
        error => {
          console.log(error);
          this.showDocpath = "";
          this.showloader = false;
        });
  }
  else
  this.showDocpath = "";
}


  ViewDoc() {
    // window.open(this.showDocpath, '_blank');
    // const link = 'data:image/jpeg;base64,'+this.showDocpath;
    // var newWindow = window.open();
    // newWindow.document.write('<img src="' + link + '" />');
    //var splitted = this.showDocpath.split(",", 10);
    //const blob = b64toBlob(this.showDocpath, splitted[0]);
    var path = this.showDocpath.split(',')
    var b = this.service.s_sd(path[1], 'application/pdf');
    var l = document.createElement('a');
    l.href = window.URL.createObjectURL(b);
    l.download = "Document" + ".pdf";
    document.body.appendChild(l);
    l.click();
    document.body.removeChild(l);
  }

  DocDivClick(val) {
    this.IssuedDocView = val;
  }

  hideFilesList() {
    this.FilesList.hide();
  }

  hideDetailList() {
    this.DetailList.hide();

  }
  GetDetails(uri){
    this.showloader=true;
    this.digiservice.GetDetails (uri ,this.Accesstoken, (a) => {
      console.log(a);
      if(a.Status == "Success")
      {
      
      //this.Details=a.CertIssuerData.Certificate.IssuedTo.Person;
  if(this.viewbtnname == "Aadha")
  {
    this.name=a.CertIssuerData.KycRes.UidData.Poi.name;
    this.gender=a.CertIssuerData.KycRes.UidData.Poi.gender;
    this.dob=a.CertIssuerData.KycRes.UidData.Poi.dob;
  }
  else{
  this.name=a.CertIssuerData.Certificate.IssuedTo.Person.name;
  this.gender=a.CertIssuerData.Certificate.IssuedTo.Person.gender;
  this.dob=a.CertIssuerData.Certificate.IssuedTo.Person.dob;
  }
  
  
     
      this.showloader=false;
      this.DetailList.show();
      
      }
      else{
        this.showloader=false;
      }
  });  
  }

}