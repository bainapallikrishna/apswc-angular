import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutsourcingAgenciesComponent } from './outsourcing-agencies.component';

describe('OutsourcingAgenciesComponent', () => {
  let component: OutsourcingAgenciesComponent;
  let fixture: ComponentFixture<OutsourcingAgenciesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutsourcingAgenciesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutsourcingAgenciesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
