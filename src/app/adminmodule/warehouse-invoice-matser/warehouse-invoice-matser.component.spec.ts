import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseInvoiceMatserComponent } from './warehouse-invoice-matser.component';

describe('WarehouseInvoiceMatserComponent', () => {
  let component: WarehouseInvoiceMatserComponent;
  let fixture: ComponentFixture<WarehouseInvoiceMatserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseInvoiceMatserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseInvoiceMatserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
