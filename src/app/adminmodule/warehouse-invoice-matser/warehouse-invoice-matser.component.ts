import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import Swal from 'sweetalert2';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { FloatingFilterComponent } from 'ag-grid-community/dist/lib/components/framework/componentTypes';
import { ThemeService } from 'ng2-charts';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { validateVerticalPosition } from '@angular/cdk/overlay';
@Component({
  selector: 'app-warehouse-invoice-matser',
  templateUrl: './warehouse-invoice-matser.component.html',
  styleUrls: ['./warehouse-invoice-matser.component.css']
})
export class WarehouseInvoiceMatserComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  Distvalue = [];
  ovabEDITsubmitted: boolean = false;
  invoicehist: any;
  wheditarray: any;
  heading: any;
  title: any;
  INSURANCElisttypes: any;
  oversubmited: boolean = false;
  comdEDITsubmitted: boolean = false;
  rebsubmited: boolean = false;
  rebateEDITsubmitted: boolean = false;
  Commoditynamelist: any;
  showloader: boolean = false;
  weighbridgeeditsubmit: boolean = false;
  weighbridgeadd: boolean = false;
  Weighbridgelist: any;
  commoditygrparry: any[]
  Quantitytypes: any;
  Storagetypes: any;
  commoditygrouparray: any;
  ddl: any;
  Varietytypes; any;
  regaarray: any;
  Username: string;
  QPcommodityVarietyarry: any[];
  QPCOMMODITYgrpNAME = [];
  reg1dist: any;
  regseldel: any;
  regionname: any;
  COMMODITYNAME = [];
  commoditygrpname = [];
  commodityEDITvardiv: boolean = false
  commGroupEDITdiv: boolean = false;
  editActdctdiv: boolean = false;
  Farmerslist: any[];
  editRemarksdiv: boolean = false;
  eidtupdatebtn: boolean = false;
  Regiontypesmasterwithdist: any;
  commGroupdiv: boolean = false;
  commdgrpivdivvalidation: boolean = false;
  commoditylistypes: any;
  submitted: boolean = false;
  commdsubmited: boolean = false;
  commdivdiv: boolean = false;
  commeditdiv: boolean = false;
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  seldselarr = [];
  unique: any;
  regionsubmitted: boolean = false;
  uniquelocations = [];
  commodityvardiv: boolean = false;
  commdivdivvalidationvar: boolean = false;
  commddlvar: any;

  QPCOMMODITYNAME = [];
  QPGradetypes: any;
  Qualityparameterslist: any;
  Chemicallist: any;
  invoicelisttypes: any;
  hmserviceslist: any;
  EDITsubmitted: boolean = false;
  Rebatewisttypes: any;
  CommodityGroupTypes: any;
  commoditypricelisttypes: any;
  overlisttypes: any;
  Commoditynameslist: any;
  DEPOSITORYTYPE: any;
  invoicesubmited: any;
  rebEDITsubmitted: boolean = false;
  isHMSubmit: boolean = false;
  isHMEdit: boolean = false;

  @ViewChild('addinvoicemodeModal') addinvoicemodeModal: ModalDirective;
  @ViewChild('addcommoditypricerModal') addcommoditypricerModal: ModalDirective;
  @ViewChild('historyWarehousemodal') historyWarehousemodal: ModalDirective;
  @ViewChild('addOverandAboveModal') addOverandAboveModal: ModalDirective;

  @ViewChild('editinvoiceModal') editinvoiceModal: ModalDirective;

  @ViewChild('editcommoditypriceModal') editcommoditypriceModal: ModalDirective;
  @ViewChild('addRebateModal') addRebateModal: ModalDirective;

  @ViewChild('editRebateModal') editRebateModal: ModalDirective;

  @ViewChild('editOverandaboveModal') editOverandaboveModal: ModalDirective;

  @ViewChild('addinvoicebasedModal') addinvoicebasedModal: ModalDirective;
  @ViewChild('addHMServiceModal') addHMServiceModal: ModalDirective;

  constructor(private formBuilder: FormBuilder, private service: CommonServices, private router: Router, private datef: CustomDateParserFormatter) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }


    else {
      if (this.logUserrole == "101" || this.isPasswordChanged == "1") {


        this.Username = sessionStorage.getItem("logUserCode");

      }
      else {


        this.router.navigate(['/Login']);
      }
    }

    this.Getcommoditygroupist();
  }

  ngOnInit(): void {


  }

  InvoiceModeform = this.formBuilder.group({

    tonbag: ['', Validators.required],
    Moderemarks: ['', Validators.required],

  })

  HMServiceform = this.formBuilder.group({
    ServiceCode: [''],
    ItemNo: ['', Validators.required],
    ServiceName: ['', Validators.required],
    ServiceType: [null, Validators.required],
    ServiceCategory: [null, Validators.required],
    HMActive: [''],
    Moderemarks: ['', Validators.required],

  })

  CommodityPriceform = this.formBuilder.group({
    Commprcgroup: ['', Validators.required],
    comdgrpname: ['', Validators.required],
    invoicemode: ['', Validators.required],
    TransStartDate: ['', Validators.required],
    TransEndDate: ['', Validators.required],
    comprice: ['', Validators.required],
    comdremarks: ['', Validators.required],
    unqid: ['']

  });

  OverandAboveform = this.formBuilder.group({
    overCommprcgroup: ['', Validators.required],
    overcomdgrpname: ['', Validators.required],
    week: ['', Validators.required],
    ovmonth: ['', Validators.required],
    ovPercentage: ['', Validators.required],
    ovremarks: ['', Validators.required]
  });
  InvoiceEDITform = this.formBuilder.group({
    invoiceid: [''],
    invoicename: [''],
    invoiceactive: ['', Validators.required],
    InvoiceEDITremarks: ['', Validators.required]

  });



  CommodityPriceEDITForm = this.formBuilder.group({

    editCommGroupname: [''],
    editCommoditpid: [''],
    editCommoditypGroupname: [''],
    Invoicecompname: [''],
    comptranstdate: [''],
    editTransenddate: [''],
    editprice: [''],
    comdeditactive: [''],

    commdpEDITremarks: ['', Validators.required],
    uniq: ['']
  })

  overandaboveEDITForm = this.formBuilder.group({
    editovabGroupname: [''],
    editovabitpid: [''],
    editonabGroupname: [''],
    ovabweek: [''],
    editovabapplicable: [''],
    editovabperc: [''],
    ovabeditactive: [''],
    ovabEDITremarks: ['', Validators.required],
    uncid: ['']
  });

  // InvoiceBasedonform=this.formBuilder.group({

  //   inbaseon:['',Validators.required],
  //   invoicebaseonremarks:['',]
  // });



  overandaboveEDITclick() {
    this.ovabEDITsubmitted = true;
    if (this.overandaboveEDITForm.invalid) {
      return;
    }

    const req = new InputRequest();

    req.INPUT_01 = this.overandaboveEDITForm.controls.editovabitpid.value;
    req.INPUT_02 = this.overandaboveEDITForm.controls.uncid.value;
    req.INPUT_03 = this.overandaboveEDITForm.controls.ovabEDITremarks.value;
    req.INPUT_04 = this.overandaboveEDITForm.controls.ovabeditactive.value;
    req.INPUT_05 = this.logUserrole;
    req.USER_NAME = this.Username;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "updateoverandabovedetails").subscribe(data => {
      this.wheditarray = data.result;
      if (this.wheditarray.StatusCode == "100") {
        if (this.wheditarray.Details[0].rtN_ID == "1") {
          this.ovab = 0;
          this.Getoverandabovelist();
          this.InvoiceEDITform.reset();
          this.InvoiceModeform.reset();
          this.editOverandaboveModal.hide();
          this.overandaboveEDITForm.reset();
          this.showloader = false;
          Swal.fire("success", "Data Updated Successfully", "success");
        }
        else {
          Swal.fire("info", "Data Updation Failed", "info");
          this.InvoiceEDITform.reset();
          this.InvoiceModeform.reset();
          this.editinvoiceModal.hide();
          this.showloader = false;
        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.wheditarray.StatusMessage, "info");
        this.showloader = false;
        this.editinvoiceModal.hide();


      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));




  }

  Getinvoicelist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "INVOICE_MODE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.invoicelisttypes = data.Details;

        this.showloader = false;
      }
      else {
        this.showloader = false;

      }


    },
      error => console.log(error));

  }

  GetHMServiceslist() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = null;
    this.service.postData(req, "GetHamaliesServices").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.hmserviceslist = data.Details;
      }

    },
      error => console.log(error));

  }

  Getcommoditypricelist() {

    this.showloader = true;
    const req = new InputRequest();
    this.service.postData(req, "GetInvoiceMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.commoditypricelisttypes = data.Details;

        this.showloader = false;
      }
      else {

        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }
  Getoverandabovelist() {

    this.showloader = true;
    const req = new InputRequest();

    this.service.postData(req, "Getoverandabovedetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.overlisttypes = data.Details;

        this.showloader = false;
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }





  Getcommoditygroupist() {

    this.showloader = true;
    const req = new InputRequest();
    this.service.postData(req, "Getcommoditiesgrouplist").subscribe(data => {
      if (data.StatusCode == "100") {
        this.CommodityGroupTypes = data.Details;

        this.showloader = false;
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }

  getcommoditynamelist(comval) {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = comval.toString();
    this.service.postData(req, "GetcommoditiesNamelist").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Commoditynameslist = data.Details;

        this.showloader = false;
      }
      else {

        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info")
      }


    },
      error => console.log(error));


  }


  GetRebatelist() {
this.showloader=true;

  const req = new InputRequest();
  this.service.postData(req, "GetRebateListDetails").subscribe(data => {
    if (data.StatusCode == "100") {
      this.Rebatewisttypes = data.Details;

        this.showloader = false;
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));



  }

  GetInsurancedetails() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "INVOICE_BASEDON";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.INSURANCElisttypes = data.Details;

        this.showloader = false;
      }
      else {

        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }


  GetDepositorylist() {

    this.showloader = true;
    const req = new InputRequest();

    this.service.postData(req, "GetDepositoryDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.DEPOSITORYTYPE = data.Details;

        this.showloader = false;
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));



  }





  md = 0 + 1; cmp = 0 + 1; ovab = 0 + 1; rb = 0 + 1; ins = 0 + 1; hm = 0 + 1;

  Insuranceclick() {
    if (this.ins == 1) {

      this.GetInsurancedetails();
      this.ins++;
    }
    else {
      this.showloader = false;
      this.ins = 0;
    }



  }


  Invoiceclick() {

    this.heading = "Invoice Mode";

    if (this.md == 1) {

      this.Getinvoicelist();
      this.md++;
    }
    else {
      this.showloader = false;
      this.md = 0;
    }

  }


  HmServiceclick() {
    this.heading = "Add H&T Services Details";
    if (this.hm == 1) {
      this.GetHMServiceslist();
      this.hm++;
    }
    else {
      this.showloader = false;
      this.hm = 0;
    }
  }

  AddInvoiceMode() {



    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();
    this.InvoiceModeform.reset();
    this.addinvoicemodeModal.show();
  }

  AddHMService() {
    this.isHMEdit = false;
    this.isHMSubmit = false;
    this.heading = "Add H&T Details";
    this.h.HMActive.setValue("1");
    this.HMServiceform.reset();
    this.addHMServiceModal.show();
  }

  Addinsurance() {


    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();
    this.heading = "Invoice Basedon"
    this.title = "Insurance";
    this.addinvoicemodeModal.show();
  }


  insuranceEdit(t1, t2, t3) {

    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();
    this.title = "Insurance";

    this.editinvoiceModal.show();
    this.InvoiceEDITform.controls.invoiceid.setValue(t1);
    this.InvoiceEDITform.controls.invoicename.setValue(t2);
    this.InvoiceEDITform.controls.invoiceactive.setValue(t3.toString());
    this.heading = "Edit Invoice Basedon"
  }

  ModeEdit(invid, invname, invact) {


    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.InvoiceEDITform.reset();
    this.RebateEDITForm.reset();
    this.editinvoiceModal.show();
    this.InvoiceEDITform.controls.invoiceid.setValue(invid);
    this.InvoiceEDITform.controls.invoicename.setValue(invname);
    this.InvoiceEDITform.controls.invoiceactive.setValue(invact.toString());
  }

  HMServicesEdit(editdata) {
    this.isHMEdit = true;
    this.isHMSubmit = false;
    this.heading = "Edit H&T Services Details"
    this.HMServiceform.reset();
    this.h.ServiceCode.setValue(editdata.servicE_ID);
    this.h.ItemNo.setValue(editdata.iteM_NO);
    this.h.ServiceName.setValue(editdata.servicE_NAME);
    this.h.ServiceType.setValue(editdata.servicE_TYPE);
    this.h.ServiceCategory.setValue(editdata.hamalieS_TRANSPORT?.toString());
    this.h.HMActive.setValue(editdata.iS_ACTIVE?.toString());
    this.addHMServiceModal.show();
  }

  Commoditypriceclick() {

    if (this.cmp == 1) {
      this.Getcommoditypricelist();
      this.cmp++;
    }
    else {
      this.showloader = false;
      this.cmp = 0;
    }

  }
  AddComodityprice() {
    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();
    this.Getinvoicelist();
    this.addcommoditypricerModal.show();

  }


  Commoditygroupchange() {
    var com = this.CommodityPriceform.controls.Commprcgroup.value
    this.getcommoditynamelist(com);
  }
  Rebateform = this.formBuilder.group({

    rebCommprcgroup: ['', Validators.required],
    Rebcommname: ['', Validators.required],
    RebateDepositortype: ['', Validators.required],
    Rebatepercentage: ['', Validators.required],
    Rebateremarks: ['', Validators.required]
  })

  Commoditygrouprebatechange() {
    var com = this.Rebateform.controls.rebCommprcgroup.value;
    this.getcommoditynamelist(com);


  }


  Overandaboveedit(uncid, commdtygrpname, comid, cmname, applicable, week, perc, act) {
    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset(); this.InvoiceEDITform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.editOverandaboveModal.show();

    this.overandaboveEDITForm.controls.editovabGroupname.setValue(commdtygrpname);
    this.overandaboveEDITForm.controls.editovabitpid.setValue(comid);
    this.overandaboveEDITForm.controls.editonabGroupname.setValue(cmname);
    this.overandaboveEDITForm.controls.ovabweek.setValue(applicable);
    this.overandaboveEDITForm.controls.editovabapplicable.setValue(week);
    this.overandaboveEDITForm.controls.editovabperc.setValue(perc);
    this.overandaboveEDITForm.controls.ovabeditactive.setValue(act.toString());
    this.overandaboveEDITForm.controls.uncid.setValue(uncid);

  }


  commoditypriceEdit(cmdcode, CMDNAME, cmdgrpNAME, INVNAME, cmdst, cmdend, cmdact, prc, unqid) {
    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();
    this.editcommoditypriceModal.show();
    this.CommodityPriceEDITForm.controls.editCommGroupname.setValue(cmdgrpNAME);
    this.CommodityPriceEDITForm.controls.editCommoditpid.setValue(cmdcode);
    this.CommodityPriceEDITForm.controls.editCommoditypGroupname.setValue(CMDNAME);
    this.CommodityPriceEDITForm.controls.Invoicecompname.setValue(INVNAME);
    this.CommodityPriceEDITForm.controls.comptranstdate.setValue(cmdst);
    this.CommodityPriceEDITForm.controls.editTransenddate.setValue(cmdend);
    this.CommodityPriceEDITForm.controls.editprice.setValue(prc);

    this.CommodityPriceEDITForm.controls.comdeditactive.setValue(cmdact.toString());
    this.CommodityPriceEDITForm.controls.uniq.setValue(unqid);

  }


  CommoditypriceEDITclick() {

    this.showloader = true;
    this.comdEDITsubmitted = true;
    if (this.CommodityPriceEDITForm.invalid) {
      this.showloader = false;
      return;
    }
    const req = new InputRequest();

    req.INPUT_01 = this.CommodityPriceEDITForm.controls.editCommoditpid.value;

    req.INPUT_02 = this.CommodityPriceEDITForm.controls.uniq.value;

    req.INPUT_04 = this.CommodityPriceEDITForm.controls.comdeditactive.value;
    req.INPUT_03 = this.CommodityPriceEDITForm.controls.commdpEDITremarks.value;
    req.INPUT_05 = this.logUserrole;
    req.USER_NAME = this.Username;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "updatecommoditypricededtails").subscribe(data => {
      this.wheditarray = data.result;
      if (this.wheditarray.StatusCode == "100") {
        if (this.wheditarray.Details[0].rtN_ID == "1") {
          this.cmp = 0;
          this.Getcommoditypricelist();
          this.InvoiceEDITform.reset();
          this.InvoiceModeform.reset();
          this.editcommoditypriceModal.hide();
          this.CommodityPriceEDITForm.reset();
          this.showloader = false;
          Swal.fire("success", "Data Updated Successfully", "success");
        }
        else {
          Swal.fire("info", "Data Updation Failed", "info");
          this.InvoiceEDITform.reset();
          this.InvoiceModeform.reset();
          this.editinvoiceModal.hide();
          this.showloader = false;
        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.wheditarray.StatusMessage, "info");
        this.showloader = false;
        this.editinvoiceModal.hide();
        this.editcommoditypriceModal.hide();
        this.CommodityPriceEDITForm.reset();

      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));

  }
  hideeditcommodityModal() {
    this.comdEDITsubmitted = false;
    this.editcommoditypriceModal.hide();
    this.editOverandaboveModal.hide();
    this.editRebateModal.hide();
    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.InvoiceEDITform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();

  }

  overandaboveclick() {
    if (this.ovab == 1) {
      this.Getoverandabovelist();
      this.ovab++;
    }
    else {
      this.showloader = false;
      this.ovab = 0;
    }

  }
  Addoverandabove() {
    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();
    this.OverandAboveform.controls.overCommprcgroup.setValue('');
    this.OverandAboveform.controls.overcomdgrpname.setValue('');
    this.OverandAboveform.controls.week.setValue('');
    this.OverandAboveform.controls.ovmonth.setValue('');
    this.addOverandAboveModal.show();
  }



  Commoditygroupoverchange() {
    var val = this.OverandAboveform.controls.overCommprcgroup.value;
    this.getcommoditynamelist(val);
  }

  Rebateclick() {
    if (this.rb == 1) {
      this.GetRebatelist();
      this.rb++;
    }
    else {
      this.showloader = false;
      this.rb = 0;
    }

  }
  AddRebate() {


    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();
    this.GetDepositorylist();
    this.addRebateModal.show();


    this.Rebateform.controls.rebCommprcgroup.setValue('');
    this.Rebateform.controls.Rebcommname.setValue('');
    this.Rebateform.controls.RebateDepositortype.setValue('');

  }
  RebateEdit(rbid, commgrpname, comid, comname, dep, per, act) {
    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();




    this.editRebateModal.show();
    this.RebateEDITForm.controls.RebateeditCommGroupname.setValue(commgrpname);

    this.RebateEDITForm.controls.rebateeditCommoditpid.setValue(comid);

    this.RebateEDITForm.controls.reabeteditCommoditypGroupname.setValue(comname);
    this.RebateEDITForm.controls.Rebatedepositor.setValue(dep);


    this.RebateEDITForm.controls.rebateeditper.setValue(per);
    this.RebateEDITForm.controls.rebateact.setValue(act.toString());
    this.RebateEDITForm.controls.unqid.setValue(rbid);


  }

  RebateEDITclick() {
    this.showloader = true;
    this.rebEDITsubmitted = true;
    if (this.RebateEDITForm.invalid) {
      this.showloader = false;
      return;
    }
    const req = new InputRequest();
    req.INPUT_03 = this.RebateEDITForm.controls.RebateEDITremarks.value;
    req.INPUT_04 = this.RebateEDITForm.controls.rebateact.value;
    req.INPUT_01 = this.RebateEDITForm.controls.rebateeditCommoditpid.value;
    req.INPUT_02 = this.RebateEDITForm.controls.unqid.value;

    req.INPUT_05 = this.logUserrole;
    req.USER_NAME = this.Username;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "updateRebatededtails").subscribe(data => {
      this.wheditarray = data.result;
      if (this.wheditarray.StatusCode == "100") {
        if (this.wheditarray.Details[0].rtN_ID == "1") {
          this.rb = 0;
          this.GetRebatelist();
          this.InvoiceEDITform.reset();
          this.InvoiceModeform.reset();
          this.editRebateModal.hide();
          this.RebateEDITForm.reset();
          this.showloader = false;
          Swal.fire("success", "Data Updated Successfully", "success");
        }
        else {
          Swal.fire("info", "Data Updation Failed", "info");
          this.InvoiceEDITform.reset();
          this.InvoiceModeform.reset();
          this.editinvoiceModal.hide();
          this.showloader = false;
        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.wheditarray.StatusMessage, "info");
        this.showloader = false;
        this.editinvoiceModal.hide();


      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));

  }

  hidehistorymasterModal() {

    this.historyWarehousemodal.hide();
    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();


  }
  InvoiceHistory(historytype) {
    const req = new InputRequest();
    if (historytype == "Modehistory") {
      req.INPUT_01 = "INVOICE_MODE";
      req.INPUT_02 = "MASTERS";

    }

    if (historytype == "insurancehistory") {
      req.INPUT_01 = "INVOICE_BASEDON";
      req.INPUT_02 = "MASTERS";

    }




    if (historytype == "pricehistory") {
      req.INPUT_01 = null;
      req.INPUT_02 = "INVOICE_PRICES";

    }
    if (historytype == "overandabovehist") {
      req.INPUT_01 = null;
      req.INPUT_02 = "COMMODITY_PERCENTAGE";
    }
    if (historytype == "rebatehist") {
      req.INPUT_01 = null;
      req.INPUT_02 = "COMMODITY_REBATES";
    }

    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.invoicehist = data.Details;

        this.historyWarehousemodal.show();
        this.showloader = false;

      }
      else {

        Swal.fire("info", data.StatusMessage, "info");

        this.historyWarehousemodal.hide();
        this.showloader = false;

      }
    },
      error => console.log(error));
    this.showloader = false;

  }
  hideWarehouseModal() {

    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();
    this.addinvoicemodeModal.hide();
  }


  Rebateaddclick() {
    this.showloader = true;
    const req = new InputRequest();
    this.rebsubmited = true;
    if (this.Rebateform.invalid) {
      this.showloader = false;
      return;
    }
    req.INPUT_01 = this.Rebateform.controls.Rebcommname.value;
    req.INPUT_02 = this.Rebateform.controls.rebCommprcgroup.value;
    req.INPUT_03 = this.Rebateform.controls.RebateDepositortype.value;
    req.INPUT_04 = this.Rebateform.controls.Rebatepercentage.value;
    req.INPUT_05 = this.logUserrole;
    req.INPUT_06 = this.Rebateform.controls.Rebateremarks.value;
    req.CALL_SOURCE = "Web";
    req.USER_NAME = this.Username;
    this.service.postData(req, "SaveRebateDetails").subscribe(data => {
      this.commoditygrouparray = data.result;
      if (this.commoditygrouparray.StatusCode == "100") {
        if (this.commoditygrouparray.Details[0].rtN_ID == "1") {
          this.rb = 0;
          this.GetRebatelist();
          this.Rebateform.reset();

          this.CommodityPriceform.reset();
          this.addRebateModal.hide();
          this.showloader = false;
          Swal.fire("success", "Data Added Successfully", "success");
        }
        else {
          Swal.fire("info", "This Master Is Already Registered", "info");
          this.InvoiceModeform.reset();
          this.addinvoicemodeModal.hide();
          this.showloader = false;
        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.commoditygrouparray.StatusMessage, "info");
        this.addinvoicemodeModal.hide();
        this.showloader = false;
      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));



  }






  invoiceaddclick() {


    const req = new InputRequest();
    this.commdsubmited = true;
    if (this.CommodityPriceform.invalid) {
      this.showloader = false;
      return;
    }


    this.showloader = true;
    req.INPUT_01 = this.CommodityPriceform.controls.Commprcgroup.value;
    req.INPUT_02 = this.CommodityPriceform.controls.comdgrpname.value;
    req.INPUT_03 = this.CommodityPriceform.controls.invoicemode.value;

    req.INPUT_04 = this.datef.format(this.CommodityPriceform.controls.TransStartDate.value);
    req.INPUT_05 = this.datef.format(this.CommodityPriceform.controls.TransEndDate.value);

    req.INPUT_06 = this.CommodityPriceform.controls.comprice.value;
    req.INPUT_08 = this.CommodityPriceform.controls.comdremarks.value;
    req.INPUT_07 = this.logUserrole;
    req.CALL_SOURCE = "Web";
    req.USER_NAME = this.Username;
    this.service.postData(req, "Saveinvoicemasterdetails").subscribe(data => {
      this.commoditygrouparray = data.result;
      if (this.commoditygrouparray.StatusCode == "100") {
        if (this.commoditygrouparray.Details[0].rtN_ID == "1") {
          this.cmp = 0;
          this.Getcommoditypricelist();
          this.InvoiceModeform.reset();
          this.CommodityPriceform.reset();
          this.addcommoditypricerModal.hide();
          this.showloader = false;
          Swal.fire("success", "Data Added Successfully", "success");
        }
        else {
          Swal.fire("info", "This Master Is Already Registered", "info");
          this.InvoiceModeform.reset();
          this.addinvoicemodeModal.hide();
          this.showloader = false;
        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.commoditygrouparray.StatusMessage, "info");
        this.addinvoicemodeModal.hide();
        this.showloader = false;
      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));


  }




  hideeditWarehouseModal() {
    this.EDITsubmitted = false;
    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();
    this.editinvoiceModal.hide();
  }

  Overaddclick() {

    const req = new InputRequest();
    this.oversubmited = true;
    if (this.OverandAboveform.invalid) {
      this.showloader = false;
      return;
    }
    this.showloader = true;
    req.INPUT_01 = this.OverandAboveform.controls.overcomdgrpname.value;
    req.INPUT_02 = this.OverandAboveform.controls.overCommprcgroup.value;
    req.INPUT_03 = this.OverandAboveform.controls.week.value;
    req.INPUT_04 = this.OverandAboveform.controls.ovmonth.value;
    req.INPUT_05 = this.OverandAboveform.controls.ovPercentage.value;
    req.INPUT_07 = this.OverandAboveform.controls.ovremarks.value;
    req.INPUT_06 = this.logUserrole;
    req.CALL_SOURCE = "Web";
    req.USER_NAME = this.Username;
    this.service.postData(req, "SaveOverandAboveDetails").subscribe(data => {
      this.commoditygrouparray = data.result;
      if (this.commoditygrouparray.StatusCode == "100") {
        if (this.commoditygrouparray.Details[0].rtN_ID == "1") {
          this.ovab = 0;
          this.Getoverandabovelist();
          this.InvoiceModeform.reset();
          this.CommodityPriceform.reset();
          this.addOverandAboveModal.hide();
          this.OverandAboveform.reset();
          this.showloader = false;
          Swal.fire("success", "Data Added Successfully", "success");
        }
        else {
          Swal.fire("info", "This Master Is Already Registered", "info");
          this.InvoiceModeform.reset();
          this.addinvoicemodeModal.hide();
          this.showloader = false;
        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.commoditygrouparray.StatusMessage, "info");
        this.addinvoicemodeModal.hide();
        this.showloader = false;
      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));


  }

  invoicemodeaddclick() {
    const req = new InputRequest();
    this.submitted = true;
    if (this.InvoiceModeform.invalid) {
      this.showloader = false;
      return;
    }

    if (this.title == "Insurance") {

      this.showloader = true;
      req.INPUT_01 = "INVOICE_BASEDON";
      req.INPUT_02 = this.InvoiceModeform.controls.tonbag.value;
      req.INPUT_03 = this.InvoiceModeform.controls.Moderemarks.value;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      req.USER_NAME = this.Username;
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.commoditygrouparray = data.result;
        if (this.commoditygrouparray.StatusCode == "100") {
          if (this.commoditygrouparray.Details[0].rtN_ID == "1") {
            this.ins = 0;
            this.GetInsurancedetails();
            this.InvoiceModeform.reset();
            this.addinvoicemodeModal.hide();
            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");
          }
          else {
            Swal.fire("info", "This Master Is Already Registered", "info");
            this.InvoiceModeform.reset();
            this.addinvoicemodeModal.hide();
            this.showloader = false;
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.commoditygrouparray.StatusMessage, "info");
          this.addinvoicemodeModal.hide();
          this.showloader = false;
        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
    }
    else {
      this.showloader = true;
      req.INPUT_01 = "INVOICE_MODE";
      req.INPUT_02 = this.InvoiceModeform.controls.tonbag.value;
      req.INPUT_03 = this.InvoiceModeform.controls.Moderemarks.value;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      req.USER_NAME = this.Username;
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.commoditygrouparray = data.result;
        if (this.commoditygrouparray.StatusCode == "100") {
          if (this.commoditygrouparray.Details[0].rtN_ID == "1") {
            this.md = 0;
            this.Getinvoicelist();
            this.InvoiceModeform.reset();
            this.addinvoicemodeModal.hide();
            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");
          }
          else {
            Swal.fire("info", "This Master Is Already Registered", "info");
            this.InvoiceModeform.reset();
            this.addinvoicemodeModal.hide();
            this.showloader = false;
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.commoditygrouparray.StatusMessage, "info");
          this.addinvoicemodeModal.hide();
          this.showloader = false;
        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
    }

  }

  HMServicesAddClick() {
    this.isHMSubmit = true;
    if (this.HMServiceform.invalid) {
      this.showloader = false;
      return false;
    }

    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.h.ItemNo.value;
    req.INPUT_02 = this.h.ServiceName.value;
    req.INPUT_03 = this.h.ServiceType.value;
    req.INPUT_04 = this.h.Moderemarks.value;
    req.INPUT_05 = this.logUserrole;
    req.INPUT_06 = this.h.ServiceCategory.value;
    req.CALL_SOURCE = "Web";
    req.USER_NAME = this.Username;
    this.service.postData(req, "SaveHamaliesServices").subscribe(data => {
      if (data.StatusCode == "100") {
        this.hm = 0;
        this.GetHMServiceslist();
        this.HMServiceform.reset();
        this.addHMServiceModal.hide();
        this.showloader = false;
        Swal.fire("success", "Data Added Successfully", "success");
      }

      else {
        Swal.fire("info", data.StatusMessage, "info");
        this.addHMServiceModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));
  }

  HMServicesEditClick() {
    this.isHMSubmit = true;
    if (this.HMServiceform.invalid) {
      this.showloader = false;
      return false;
    }
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.h.ServiceCode.value;
    req.INPUT_02 = this.h.HMActive.value;
    req.INPUT_03 = this.h.Moderemarks.value;
   
    req.CALL_SOURCE = "Web";
    req.USER_NAME = this.Username;
    this.service.postData(req, "UpdateHamaliesServices").subscribe(data => {
      if (data.StatusCode == "100") {
        this.hm = 0;
        this.GetHMServiceslist();
        this.HMServiceform.reset();
        this.addHMServiceModal.hide();
        this.showloader = false;
        Swal.fire("success", "Data Updated Successfully", "success");
      }

      else {
        Swal.fire("info", data.StatusMessage, "info");
        this.addHMServiceModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));
  }

  HMServicesHistoryClick(){

    this.showloader = true;
    const req = new InputRequest();
    this.invoicehist =[];
    this.service.postData(req, "GetHMHistoryDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.invoicehist = data.Details;
        this.historyWarehousemodal.show();
      }
    },
      error => console.log(error));
  }

  InvoiceEDITclick() {

    this.showloader = true;
    this.EDITsubmitted = true;
    if (this.InvoiceEDITform.invalid) {
      this.showloader = false;
      return;
    }
    const req = new InputRequest();

    if (this.title == "Insurance") {

      req.INPUT_01 = "INVOICE_BASEDON";
      req.INPUT_02 = this.InvoiceEDITform.controls.invoiceid.value;
      req.INPUT_03 = this.InvoiceEDITform.controls.invoiceactive.value;
      req.INPUT_04 = this.InvoiceEDITform.controls.InvoiceEDITremarks.value;
      req.INPUT_05 = this.logUserrole;
      req.USER_NAME = this.Username;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.ins = 0;
            this.GetInsurancedetails();
            this.editinvoiceModal.hide();
            this.showloader = false;
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.editinvoiceModal.hide();
            this.showloader = false;
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;
          this.editinvoiceModal.hide();


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));




    }


    else {

      req.INPUT_01 = "INVOICE_MODE";
      req.INPUT_02 = this.InvoiceEDITform.controls.invoiceid.value;
      req.INPUT_03 = this.InvoiceEDITform.controls.invoiceactive.value;
      req.INPUT_04 = this.InvoiceEDITform.controls.InvoiceEDITremarks.value;
      req.INPUT_05 = this.logUserrole;
      req.USER_NAME = this.Username;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.md = 0;
            this.Getinvoicelist();
            this.editinvoiceModal.hide();
            this.showloader = false;
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.editinvoiceModal.hide();
            this.showloader = false;
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;
          this.editinvoiceModal.hide();


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
    }
  }
  RebateEDITForm = this.formBuilder.group({

    RebateeditCommGroupname: [''],
    rebateeditCommoditpid: [''],
    reabeteditCommoditypGroupname: [''],
    Rebatedepositor: [''],
    rebateeditper: [''],
    rebateact: ['', Validators.required],
    RebateEDITremarks: ['', Validators.required],
    unqid: ['']
  })

  get f1() { return this.InvoiceModeform.controls; }

  get comdity() { return this.CommodityPriceform.controls; }

  get ovandab() { return this.OverandAboveform.controls; }

  get f() { return this.InvoiceEDITform.controls; }

  get comdity1() { return this.CommodityPriceEDITForm.controls; }

  get reb() { return this.Rebateform.controls; }

  get reb1() { return this.RebateEDITForm.controls; }

  get ovandab1() { return this.overandaboveEDITForm.controls }

  get h() { return this.HMServiceform.controls; }



  hideaddCommodityModal() {
    this.addRebateModal.hide();
    this.addOverandAboveModal.hide();
    this.addcommoditypricerModal.hide();
    this.InvoiceModeform.reset();
    this.CommodityPriceform.reset();
    this.OverandAboveform.reset();
    this.Rebateform.reset();
    this.CommodityPriceEDITForm.reset();
    this.overandaboveEDITForm.reset();
    this.RebateEDITForm.reset();
    this.InvoiceEDITform.reset();
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

}
