import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { DSButtonRendererComponent } from 'src/app/custome-directives/DSbutton-renderer.component';
import { TmplAstRecursiveVisitor } from '@angular/compiler';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { DomSanitizer } from '@angular/platform-browser';
import { DigiServices } from 'src/app/Services/Digilocker.services';

@Component({
  selector: 'app-assetmanagement',
  templateUrl: './assetmanagement.component.html',
  styleUrls: ['./assetmanagement.component.css'],
})
export class AssetmanagementComponent implements OnInit {
  loguser: string = sessionStorage.getItem('logUserCode');
  loguserrole: string = sessionStorage.getItem('logUserrole');
  ispwdchanged: string = sessionStorage.getItem('logUserisChnagePassword');
  loguserid: string = sessionStorage.getItem('logUserworkLocationCode');
  Worklocname = sessionStorage.getItem('logUserworkLocationName');

  columnDefs = [
    { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer' },
    {
      headerName: 'WareHouse Name',
      maxWidth: 200,
      field: 'warehousE_NAME',
      sortable: true,
      filter: true,
    },
    {
      headerName: 'Sub Category Name',
      maxWidth: 200,
      field: 'subcategorY_NAME',
      sortable: true,
      filter: true,
    },
    {
      headerName: 'No. of Units',
      maxWidth: 150,
      field: 'nO_OF_UNITS',
      sortable: true,
      filter: true,
    },
    {
      headerName: 'Purchase Date',
      maxWidth: 150,
      field: 'purchasE_DATE',
      sortable: true,
      filter: true,
    },
    {
      headerName: 'Expiry Date',
      maxWidth: 200,
      field: 'expirY_DATE',
      sortable: true,
      filter: true,
    },
    {
      headerName: 'Maintenance Status',
      maxWidth: 300,
      field: 'maintenancE_STATUS',
      sortable: true,
      filter: true,
    },
    {
      headerName: 'Maintenance upto',
      maxWidth: 300,
      field: 'maintenancE_UPTO',
      sortable: true,
      filter: true,
    },
    {
      headerName: 'Inserted On',
      maxWidth: 300,
      field: 'inserteD_ON',
      sortable: true,
      filter: true,
    },
    {
      headerName: 'Inserted By',
      maxWidth: 300,
      field: 'inserteD_BY',
      sortable: true,
      filter: true,
    },
    {
      headerName: 'Action',
      field: 'value',
      cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        CancelClick: this.View.bind(this),
      },
    },
  ];

  preview: string = '';
  seldocpath: any;
  seldoctype: string = '';
  seldoccat: string = '';

  submitted: boolean = false;
  showloader: boolean = false;
  AssetList: any[];
  NgbDateStruct: any;
  MaintenanceAvailableList: any[];
  imgesarr = [];
  sectiondd: any[];
  categorydd: any[];
  SubCategorydd: any[];
  Itemdropdown: any[];
  public progress: number;
  public message: string;
  public message1: string;
  doc_change: boolean = false;
  doc_change1: boolean = false;
  response: { dbPath: '' };
  Base64Image: any;
  StockDOC: any;
  ImagePath: any;
  Formaddarray: any;
  progress1: any;
  response1: { dbPath: '' };
  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  public components;
  DeadStockList: any[];
  SectionID: any;
  showMaintenance: boolean = false;
  Image: any;
  Document: any;


   
  code: any;
  Accesstoken: any;
  FilesDD: any;
  Docpath: any;
  Authstatus: any;
  UploadFilesDD: any;
  IssuedDocView: boolean = true;
  viewbtnname: any;
  uri: any;
  DigiDetails: any[];
  showDocpath: any;
  hrefdata: any;
  Details: any;
  name: any;
  dob: any;
  gender: any;


  constructor(
    private router: Router,
    private service: CommonServices,
    private fb: FormBuilder,
    private datef: CustomDateParserFormatter,
    private digiservice: DigiServices,
     
    private sanitizer: DomSanitizer
  ) {
    if (!this.loguserrole || this.ispwdchanged == '0') {
      this.router.navigate(['/Login']);
      return;
    }

    this.frameworkComponents = {
      buttonRenderer: DSButtonRendererComponent,
    };
    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
    //this.imgesarr.push({simages:""});
  }
  @ViewChild('addModal') public addModal: ModalDirective;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('previewModal') public previewModal: ModalDirective;
  @ViewChild('DetailList') public DetailList: ModalDirective;
  @ViewChild('FilesList') public FilesList: ModalDirective;
  ngOnInit(): void {
    let now: Date = new Date();
    this.NgbDateStruct = {
      year: now.getFullYear(),
      month: now.getMonth() + 1,
      day: now.getDate(),
    };
    this.MaintenanceAvailableMethod();
    // this.GetCategoryDropdown();
    this.GetSubCategoryDropdown();

    this.LoadAssetsList();
  }

  View(CancelRcd) {
    this.Image = '';
    this.Document = '';
    //CancelRcd.rowData.doC_PATH ? (this.decryptFile(CancelRcd.rowData.doC_PATH, "DS_DOC")) : "";
    //CancelRcd.rowData.imagE_PATH ? (this.decryptFile(CancelRcd.rowData.imagE_PATH, "DS_PHOTO")) : "";
    this.viewModal.show();
  }

  MaintenanceAvailableMethod() {
    this.MaintenanceAvailableList = [
      { MainID: '0', MainName: 'Yes' },
      { MainID: '1', MainName: 'No' },
    ];
  }

  ChangeMaintenance(a) {
    if (a == '0') {
      this.showMaintenance = true;
    } else if (a == '1') {
      this.showMaintenance = false;
    }
  }

  LoadAssetsList() {
    this.showloader = true;
    const req = new InputRequest();

    req.INPUT_01 = 'ASSET_MANAGEMENT';
    req.INPUT_02 = this.loguserid;
    this.AssetList = [];
    this.service.postData(req, 'GetHelpDetails').subscribe(
      (data) => {
        this.showloader = false;
        if (data.StatusCode == '100') {
          this.AssetList = data.Details;
          this.gridApi.setRowData(this.AssetList);
          console.log(this.AssetList);
        } else {
          this.gridApi.setRowData(this.AssetList);
          Swal.fire(
            'warning',
            'Assets not submitted for this Warehouse',
            'warning'
          );
        }
      },
      (error) => console.log(error)
    );
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.AssetList);
  }

  GetSectionDropdown() {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = 'SECTION_DD';
    this.service.postData(req, 'GetDeadStockDetails').subscribe(
      (data) => {
        if (data.StatusCode == '100') {
          this.sectiondd = data.Details;
          this.showloader = false;
        } else {
          this.showloader = false;

          Swal.fire('info', data.StatusMessage, 'info');
        }
      },
      (error) => console.log(error)
    );
  }

  GetCategoryDropdown() {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = 'CATEGORY_DD';
    this.service.postData(req, 'GetDeadStockDetails').subscribe(
      (data) => {
        if (data.StatusCode == '100') {
          this.categorydd = data.Details;
          this.showloader = false;
          console.log(this.categorydd);
        } else {
          this.showloader = false;

          Swal.fire('info', 'Category Dropdown loading failed', 'info');
        }
      },
      (error) => console.log(error)
    );
  }

  GetSubCategoryDropdown() {
    this.showloader = true;
    //this.subf.AddSubCategory.setValue("");
    const req = new InputRequest();
    this.service
      .postData(req, 'GetAssetmanagementSubcategoryDetails')
      .subscribe(
        (data) => {
          if (data.StatusCode == '100') {
            this.SubCategorydd = data.Details;
            this.showloader = false;
            //this.SectionID=data.Details[0]["wM_SECTION"];
            console.log(this.SubCategorydd);
          } else {
            this.showloader = false;

            Swal.fire('info', 'Sub Category Dropdown loading failed', 'info');
          }
        },
        (error) => console.log(error)
      );
  }

  get subf() {
    return this.AssetManagementAddForm.controls;
  }

  get f() {
    return this.subf.commodities as FormArray;
  }

  //get f() { return this.subf.commodities as FormArray; }

  // AssetManagementAddForm = this.formBuilder.group({
  //   commodities: new FormArray([
  //     this.formBuilder.group({

  //     })
  //   ])

  // })

  AssetManagementAddForm = this.fb.group({
    // AddCategory: ['', Validators.required],

    AddSubCategory: ['', Validators.required],

    AddNoOfUnits: ['', Validators.required],
    PurchasedDate: ['', Validators.required],
    ExpiryDate: [''],
    MaintenanceAvailable: ['', Validators.required],
    Maintenanceupto: [''],
    Base64Image: [''],
    StockDOC: [''],
    ImagePath: [''],
    Editactive: [''],
    EditReason: [''],
    SerialNo: [''],
    images: new FormArray([
      this.fb.group({
        ImagePath: ['', Validators.required],
        ImageType: [''],
        ImagCategory: [''],
        ImageBase64: [''],
        PrevPath: [''],
      }),
    ]),
  });

  get asset() {
    return this.AssetManagementAddForm.controls;
  }
  get im() {
    return this.asset.images as FormArray;
  }

  AddImage() {
    this.im.push(
      this.fb.group({
        ImagePath: ['', Validators.required],
        ImageType: [''],
        ImagCategory: [''],
        ImageBase64: [''],
        PrevPath: [''],
      })
    );
  }

  RemoveImage(index) {
    this.im.removeAt(index);
  }

  AddDeadStock() {
    this.ImageDocClear();
    this.AssetManagementAddForm.reset();
    this.im.clear();
    this.im.reset();
    this.AddImage();
    this.addModal.show();
    this.submitted = false;
  }

  SubmitAddDetails() {
    this.submitted = true;
    let imglist: string[] = [];

    for (let i = 0; i < this.im.length; i++) {
      if (!this.im.value[i].ImagePath) {
        Swal.fire('error', 'Please Upload Image', 'error');
        return false;
      }
      imglist.push(this.im.value[i].ImagePath);
    }
    if (this.AssetManagementAddForm.invalid) {
      return false;
    }
    if (
      this.subf.MaintenanceAvailable.value == '0' &&
      !this.subf.Maintenanceupto.value
    ) {
      Swal.fire('error', 'Maintenance Upto Date is required', 'error');
      return false;
    }
    if (
      this.datef.format(this.subf.PurchasedDate.value) >
      this.datef.format(this.subf.ExpiryDate.value)
    ) {
      Swal.fire(
        'error',
        'Expiry date should be greater than purchase date',
        'error'
      );
      return false;
    }
    if (
      this.datef.format(this.subf.PurchasedDate.value) >
      this.datef.format(this.subf.Maintenanceupto.value)
    ) {
      Swal.fire(
        'error',
        'Maintenance date should be greater than purchase date',
        'error'
      );
      return false;
    }

    

    const req = new InputRequest();

    req.INPUT_01 = 'ASSET_FORM_SUBMISSION';
    req.INPUT_02 = this.loguserid;
    req.INPUT_03 = null;
    //this.subf.AddCategory.value
    req.INPUT_04 = this.subf.AddSubCategory.value;
    req.INPUT_05 = this.subf.AddNoOfUnits.value;
    req.INPUT_06 = this.datef.format(this.subf.PurchasedDate.value);
    req.INPUT_07 = this.datef.format(this.subf.ExpiryDate.value);
    req.INPUT_08 = this.subf.MaintenanceAvailable.value;
    req.INPUT_09 = this.datef.format(this.subf.Maintenanceupto.value);
    req.INPUT_10 = imglist.join(',');
    req.INPUT_11 = this.subf.StockDOC.value;
    req.INPUT_12 = this.loguserrole;
    req.INPUT_13 = this.subf.SerialNo.value;

    req.USER_NAME = this.loguser;
    req.CALL_SOURCE = 'Web';

    this.service.postData(req, 'HelpInsertion').subscribe((data) => {
      this.Formaddarray = data.result;
      if (data.StatusCode == 100) {
        this.showloader = false;
        this.AssetManagementAddForm.reset();
        Swal.fire('success', 'Data Submitted Successfully', 'success');
        this.addModal.hide();
        this.LoadAssetsList();
        this.ImageDocClear();
      } else {
        this.showloader = false;
        Swal.fire('info', data.StatusMessage, 'info');
      }
    });
  }

  Mail() {
    const req = new InputRequest();

    req.INPUT_01 = 'gowtham222222@gmail.com';
    req.INPUT_03 = 'Subject';
    req.INPUT_04 = 'Body';

    this.service.postData(req, 'SendMail').subscribe((data) => {
      if (data.StatusCode == 100) {
        this.showloader = false;
        Swal.fire('success', 'Mail sent Successfully', 'success');
      } else {
        this.showloader = false;
        Swal.fire('info', data.StatusMessage, 'info');
      }
    });
  }

  ImageDocClear() {
    this.message = '';
    this.message1 = '';
    this.progress = null;
    this.progress1 = null;

    // this.subf.AddCategory.setValue("");
    this.subf.AddSubCategory.setValue('');
    this.subf.AddNoOfUnits.setValue('');
    this.subf.PurchasedDate.setValue('');
    this.subf.ExpiryDate.setValue('');
    this.subf.MaintenanceAvailable.setValue('');
    this.subf.Maintenanceupto.setValue('');
  }

  hideaddModal() {
    this.AssetManagementAddForm.reset();
    this.addModal.hide();
    this.ImageDocClear();
  }

  uploadImage(event, index) {
    
    this.showloader=true;
    let imgindex = index;
    this.seldocpath = '';
    this.seldoctype = '';
    for (var i = 0; i < this.imgesarr.length - 1; i++) {}
    let url: string;
    if (event.target.files && event.target.files[0]) {
      let imagetype = event.target.files[0].type;
      let imagesize = event.target.files[0].size;

      if (imagetype != 'image/jpeg' && imagetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png files only', 'info');
        return false;
      }

      if (imagesize > 2097152) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      this.seldoccat = imagetype;
      if (imagetype == 'image/jpeg' || imagetype == 'image/png')
        this.seldoctype = 'IMAGE';
      else this.seldoctype = 'PDF';

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype == 'PDF') {
          const result = reader.result as string;
          const blob = this.service.s_sd(result.split(',')[1], this.seldoccat);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
          this.im.controls[imgindex].patchValue({
            ImageBase64: this.seldocpath,
            ImageType : this.seldoctype,
            PrevPath : this.seldocpath,
          });
        } else {
          this.seldocpath = reader.result as string;
          this.preview = this.seldocpath; 
          this.im.controls[imgindex].patchValue({
            ImageBase64: this.seldocpath,
            ImageType : this.seldoctype,
            PrevPath : this.seldocpath,
          });
        }
      };
      reader.readAsDataURL(<File>event.target.files[0]);

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', 'AssetManagement');

      this.service
        .encryptUploadFile(formData, 'EncryptFileUpload')
        .subscribe((event) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress1 = Math.round((100 * event.loaded) / event.total);
          } else if (event.type === HttpEventType.Response) {
            this.message1 = 'Upload success.';
            this.im.controls[imgindex].patchValue({
              ImagePath: event.body.fullPath
            });

            Swal.fire('success', 'Uploaded Successfully', 'success');
          this.showloader=false;
          }
        });
    }
  }

  uploadFile(event) {
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;

      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF') {
        Swal.fire('info', 'Please upload pdf files only', 'info');
        return false;
      }

      if (filesize > 10615705) {
        Swal.fire('info', 'File size must be upto 10 MB', 'info');
        return false;
      }

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', 'AssetManagement');

      this.service
        .UploadFile(formData, 'EncryptFileUpload')
        .subscribe((event) => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress = Math.round((100 * event.loaded) / event.total);
          else if (event.type === HttpEventType.Response) {
            this.doc_change = true;
            this.message = 'Upload success.';
            this.uploadFinished(event.body, 'DS_DOC', '');
          }
        });
    }
  }
  imagespath = [];
  public uploadFinished = (event, doctype, index) => {
    if (doctype == 'DS_PHOTO')
      this.imagespath.forEach((value) => {
        if (value.id == index.simages)
          this.imagespath = this.imagespath.filter(
            (m) => m.id != index.simages
          );
      });
    var obj = { id: index.simages, value: event.fullPath };
    this.imagespath.push(obj);
    Swal.fire('success', 'Uploaded Successfully', 'success');
    //this.f.push(event.fullPath);
    //this.subf.ImagePath.setValue();
    if (doctype == 'DS_DOC') this.subf.StockDOC.setValue(event.fullPath);
  };

  // decryptFile(filepath,Doctype) {
  //   if (filepath) {
  //     this.service.decryptFile(filepath, "DecryptFile")
  //       .subscribe(data => {

  //         if(Doctype=="DS_DOC"){
  //           this.Document=data.docBase64;
  //         }
  //         else
  //         {
  //           this.Image=data.docBase64;
  //         }

  //       },
  //         error => {
  //           console.log(error);
  //           this.Document="";
  //           this.Image="";
  //         });
  //   }
  //   else
  //   this.Document="";
  //   this.Image="";
  // }

  viewPDF(input): void {
    if (!input) {
      Swal.fire('info', 'File not uploaded for this record', 'info');
    } else {
      let pdfWindow = window.open('', '_blank');
      pdfWindow.document.write(
        `<iframe width='100%' height='100%' src='${encodeURI(input)}'></iframe>`
      );
    }
  }
  i = 0;
  addmoreimgesfun() {
    this.i = this.i + 1;
    this.imgesarr.push({ simages: this.i });
  }
  removeimages(id, ind) {
    //this.imgesarr = this.imgesarr.filter(m => m.id !=ind);
    this.imgesarr.splice(id, 1);

    this.imagespath.forEach((value) => {
      if (value.id == ind.simages)
        this.imagespath = this.imagespath.filter((m) => m.id != ind.simages);
    });
  }

  Base64path: any;
  foundarry: any;
  Getimages(ide) {
    for (var k = 0; k <= this.imagespath.length - 1; k++) {
      if (ide.simages == this.imagespath[k].id) {
        this.Base64path = this.imagespath[k].value;
        this.foundarry = 'found';
        return;
      } else {
        this.foundarry = 'notfound';
      }
    }
    if (this.foundarry == 'found') {
      this.decryptFile(this.Base64path);
      this.foundarry = '';
    }
    if (this.foundarry == 'notfound') {
      Swal.fire('info', 'Please upload image', 'info');
      this.foundarry = '';
      return;
    }
  }

  foundfunction() {}

  decryptFile(imagepth: string) {
    if (imagepth) {
      this.service.decryptFile(imagepth, 'DecryptFile').subscribe(
        (data) => {
          var ext = imagepth.split('.').pop();
          var filename = imagepth.replace(/^.*[\\\/]/, '');

          var newTab = window.open();
          newTab.document.body.innerHTML =
            '<img src=' + data.docBase64 + ' width="100%" height="100%">';
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
    }
  }

  Addmoreimages1() {
    this.f.push(
      this.fb.group({
        ImagePath: [''],
      })
    );
  }
  RemoveSpaceReservation(ind) {
    this.f.removeAt(ind);
  }

  Docpriview(val) {
    // const blob = this.service.s_sd((val).split(',')[1], this.seldoccat);
    // const blobUrl = URL.createObjectURL(blob);
    // this.preview = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
    //this.preview = this.sanitizer.bypassSecurityTrustUrl(val);
    //this.preview = this.preview.changingThisBreaksApplicationSecurity;
    this.previewModal.show();
  }

  
  CallDigilock() {
    if (!this.FilesDD && !this.UploadFilesDD) {
      this.showloader = true;
      var pagename = "OutsourcingAgencies";
      //this.digiservice.Digilock(pagename,(function(a) {
      this.digiservice.Digilock(pagename, (a, b) => {
        this.Accesstoken = b;
        this.FilesDD = a.UserFiles.items;
        if (a.UserUploadFiles)
          this.UploadFilesDD = a.UserUploadFiles.items;
        this.FilesList.show();
        this.showloader = false;
      });
    }
    else {
      this.FilesList.show();
    }
  }

  ShowFiles(val, mimer, num, name) {
    this.showloader = true;
    this.Docpath="";

    this.digiservice.ShowFiles(val, mimer, num, name, this.Accesstoken, (a, b) => {
      this.Docpath = a;
      this.decryptFile1(this.Docpath);
      this.uri = b;
      this.Authstatus = "1";
      this.viewbtnname = name.substring(0, 5);
      this.FilesList.hide();
      
    });
  }

decryptFile1(filepath) {
  if (filepath) {
    this.showloader = true;
    this.service.decryptFile(filepath, "DecryptFile")
      .subscribe(data => {
        let blob : any;
        console.log(data.docBase64);

        if(data.docBase64.includes('application/pdf'))
        {
         blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
        
        }
        else if(data.docBase64.includes('image/png'))
        {
           blob = this.service.s_sd((data.docBase64).split(',')[1], "image/png");
           
        }
        else if(data.docBase64.includes('image/jpg') || data.docBase64.includes('image/jpeg') )
        {
           blob = this.service.s_sd((data.docBase64).split(',')[1], "image/jpg");
        }
        const blobUrl = URL.createObjectURL(blob);  
        this.hrefdata=this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        this.showloader = false;

      },
        error => {
          console.log(error);
          this.showDocpath = "";
          this.showloader = false;
        });
  }
  else
  this.showDocpath = "";
}
 


  ViewDoc() {
    // window.open(this.showDocpath, '_blank');
    // const link = 'data:image/jpeg;base64,'+this.showDocpath;
    // var newWindow = window.open();
    // newWindow.document.write('<img src="' + link + '" />');
    //var splitted = this.showDocpath.split(",", 10);
    //const blob = b64toBlob(this.showDocpath, splitted[0]);
    var path = this.showDocpath.split(',')
    var b = this.service.s_sd(path[1], 'application/pdf');
    var l = document.createElement('a');
    l.href = window.URL.createObjectURL(b);
    l.download = "Document" + ".pdf";
    document.body.appendChild(l);
    l.click();
    document.body.removeChild(l);
  }



  DocDivClick(val) {
    this.IssuedDocView = val;
  }

  hideFilesList() {
    this.FilesList.hide();
  }

  hideDetailList() {
    this.DetailList.hide();

  }
  GetDetails(uri){
    this.showloader=true;
    this.digiservice.GetDetails (uri ,this.Accesstoken, (a) => {
      console.log(a);
      if(a.Status == "Success")
      {
      
      //this.Details=a.CertIssuerData.Certificate.IssuedTo.Person;
  if(this.viewbtnname == "Aadha")
  {
    this.name=a.CertIssuerData.KycRes.UidData.Poi.name;
    this.gender=a.CertIssuerData.KycRes.UidData.Poi.gender;
    this.dob=a.CertIssuerData.KycRes.UidData.Poi.dob;
  }
  else{
  this.name=a.CertIssuerData.Certificate.IssuedTo.Person.name;
  this.gender=a.CertIssuerData.Certificate.IssuedTo.Person.gender;
  this.dob=a.CertIssuerData.Certificate.IssuedTo.Person.dob;
  }
  
  
     
      this.showloader=false;
      this.DetailList.show();
      
      }
      else{
        this.showloader=false;
      }
  });  
  }
}
