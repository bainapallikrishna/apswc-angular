import { Component, OnInit, ViewChild,Pipe,PipeTransform } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import Swal from 'sweetalert2';
import { analyzeAndValidateNgModules } from '@angular/compiler';

@Component({
  
  selector: 'app-employeemaster',
  templateUrl: './employeemaster.component.html',
  styleUrls: ['./employeemaster.component.css'],
  
})


export class EmployeemasterComponent implements OnInit 
{
  emploanarray:any;
  LoanTYPE:any; 
  unique:any;
  Empreldiv:boolean=false;
  uniquelocations=[];
  loc:{};
  locationvalidation:boolean=false;
locval:any;
  loceditarray:any;
  logUserrole:string = sessionStorage.getItem("logUserrole");
  Username:string ;
  showloader: boolean = false;  
  empleavearray:any;
  isPasswordChanged:string = sessionStorage.getItem("logUserisChangePassword");
  locationsTYPE:any;
  empaddarray:any;
  natltype:any;
  title="";
  locedittype:any;
  empaeditarray:any;
  emptypes: any[];
  Desigtypes: any[];
  gendertype: any[];
  edutype: any; gendertpe: any; BGtype: any; MStype: any; rltype: any; reltype: any;
  editemptype:any;
  LEAVESTYPE:any;
  editempidandemptype:boolean=false;
  disableTextbox:boolean = false;
  MasterHistlist:any;
  locationdiv:boolean=false;  
  empaddtype:any;
  locddl="";
  LOANeditarray:any;
  locarry:any;
  //emptype
  EmpId: any;
  submitted = false;
  editsubmitted = false;
  empidedit: boolean = false;
  empid: boolean = false;
  empadd1: boolean = false;
  emptypes1:any[];
  Sectype:any;
  emphistcount=0;
  desighistcount=0;
  genhiscount=0;
  eduhiscount=0
  sechiscont=0;
  nathiscont=0;
  wlhiscount=0;
  bghistcount=0;
  mshistcount=0;
  rlhistcount=0;
  LEAVEeditarray:any;
 // Godownswarehsetype: any;
 // empeditremarks:any;
  //empedittype:any;
  empactive: any;
  @ViewChild('addempModal') addempModal: ModalDirective;
  @ViewChild('editempModal') editempModal: ModalDirective;
  @ViewChild('historyempModal') historyempModal: ModalDirective;

  constructor(private service: CommonServices, private formBuilder: FormBuilder, private router: Router,) 
  {
    if(!this.logUserrole|| this.isPasswordChanged == "0")
    {
         this.router.navigate(['/Login']);
       }
   
       
       else
       {
         if(this.logUserrole=="101"||this.isPasswordChanged == "1")
       {
         
         
         this.Username = sessionStorage.getItem("logUserCode");
         
       }
   else
   {
   
   
         this.router.navigate(['/Login']);
   }
       }     
         
   
   }

  ngOnInit(): void {
    this.uniquelocations=new Array<string>();
  }

  getemploylist()
   {
    this.showloader= true;  

    const req = new InputRequest();
    req.INPUT_01 = "EMP_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.emptypes = data.Details;
        this.showloader= false;  
        this.locationdiv=false;
        
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else{
        this.showloader= false;  
        this.locationdiv=false;
      Swal.fire("info",data.StatusMessage,"info");
      }

    },
      error => console.log(error));

  }
  getdesignationlist() {
    this.showloader= true;  
    const req = new InputRequest();
    req.INPUT_01 = "DESIGNATION";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Desigtypes = data.Details;
        this.showloader= false;
        this.locationdiv=false;  
        // for(var i=0;i<this.Desigtypes.length;i++){

        //   this.desighistcount=this.desighistcount+this.Desigtypes[i].hiS_COUNT;
        // }

      }
      else{
        this.showloader= false;
        this.locationdiv=false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));

  }
  getEducationallist() {
    this.showloader= true;
    const req = new InputRequest();
    req.INPUT_01 = "EDU";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.edutype = data.Details;
        this.showloader= false;
        this.locationdiv=false;
        // for(var i=0;i<this.edutype.length;i++){

        //   this.eduhiscount=this.eduhiscount+this.edutype[i].hiS_COUNT;
        // }
      }
      else{
        this.locationdiv=false;
        this.showloader= false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));

  }

  getGenderlist() {
    this.showloader= true;
    const req = new InputRequest();
    req.INPUT_01 = "GENDER";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.gendertpe = data.Details;
        this.showloader= false;
        this.locationdiv=false;
        // for(var i=0;i<this.gendertpe.length;i++){

        //   this.genhiscount=this.genhiscount+this.gendertpe[i].hiS_COUNT;
        // }
      }
      else{
        this.showloader= false;
        this.locationdiv=false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));

  }

  getBloodgrouplist() {
    this.showloader= true;
    const req = new InputRequest();
    req.INPUT_01 = "BLOOD_GROUP";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.BGtype = data.Details;
        this.showloader= false;
        this.locationdiv=false;
        // for(var i=0;i<this.BGtype.length;i++){

        //   this.bghistcount=this.bghistcount+this.BGtype[i].hiS_COUNT;
        // }
      }
      else{
        this.showloader= false;
        this.locationdiv=false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));

  }

  getmaritalstatuslist() {
    this.showloader= true;
    const req = new InputRequest();
    req.INPUT_01 = "MARITAL_STATUS";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MStype = data.Details;
        this.showloader= false;
        this.locationdiv=false;
        // for(var i=0;i<this.MStype.length;i++){

        //   this.mshistcount=this.mshistcount+this.MStype[i].hiS_COUNT;
        // }
      }
      else{
        this.showloader= false;
        this.locationdiv=false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));

  }

  getrelationlist() {
    this.showloader= true;
    const req = new InputRequest();
    req.INPUT_01 = "RELATION";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.reltype = data.Details;
        this.showloader= false;
        this.locationdiv=false;
        // for(var i=0;i<this.reltype.length;i++){

        //   this.rlhistcount=this.rlhistcount+this.reltype[i].hiS_COUNT;
        // }
      }
      else{
        this.showloader= false;
        this.locationdiv=false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));

  }

  getworkloclist() {
    this.showloader= true;
    const req = new InputRequest();
    req.INPUT_01 = "WORK_LOCATION";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.rltype = data.Details;
        this.showloader= false;
      
        // for(var i=0;i<this.rltype.length;i++){

        //   this.wlhiscount=this.wlhiscount+this.rltype[i].hiS_COUNT;
        // }
      }
      else{
        this.showloader= false;
      //  this.locationdiv=false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));

  }

  Getsectionlist() {
    this.showloader= true;
    const req = new InputRequest();
    req.INPUT_01 = "SECTION";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Sectype = data.Details;
        this.showloader= false;
        this.locationdiv=false;
        // for(var i=0;i<this.Sectype.length;i++){

        //   this.sechiscont=this.sechiscont+this.Sectype[i].hiS_COUNT;
        // }
      }
      else{
        this.showloader= false;
        this.locationdiv=false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));

  }

  GetNationalitylist() 
  {
    this.showloader= true;
    const req = new InputRequest();
    req.INPUT_01 = "NATIONALITY";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.natltype = data.Details;
        this.showloader= false;
        this.locationdiv=false;
        // for(var i=0;i<this.natltype.length;i++){

        //   this.nathiscont=this.nathiscont+this.natltype[i].hiS_COUNT;
        // }
        
      }
      else{
        this.showloader= false;
        this.locationdiv=false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));

  }

  Getleaveslist(){

    this.showloader= true;
    const req = new InputRequest();
    req.INPUT_01 = "LEAVES";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.LEAVESTYPE = data.Details;
        this.showloader= false;
        this.locationdiv=false;
        // for(var i=0;i<this.natltype.length;i++){

        //   this.nathiscont=this.nathiscont+this.natltype[i].hiS_COUNT;
        // }
        
      }
      else{
        this.showloader= false;
        this.locationdiv=false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));
 
  }

  Getloanlist(){

    this.showloader= true;
    const req = new InputRequest();
    req.INPUT_01 = "LOAN_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.LoanTYPE = data.Details;
        this.showloader= false;
        this.locationdiv=false;
        // for(var i=0;i<this.natltype.length;i++){

        //   this.nathiscont=this.nathiscont+this.natltype[i].hiS_COUNT;
        // }
        
      }
      else{
        this.showloader= false;
        this.locationdiv=false;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));
 
  }
  GetLocationlist()
  {
    
    this.uniquelocations=[];
    this.showloader= true;
    const req = new InputRequest();
    req.TYPEID = "work_location_sub_cat";
    this.service.postData(req, "GetLocationMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.locationsTYPE = data.Details;
      
        this.showloader= false;
        this.locationdiv=true;
        this.unique = [...new Set(this.locationsTYPE.map( item => item.work_location_type))];
        for(var i=0;i<this.unique.length;i++){

         
          this.uniquelocations.push({"Worklocname":this.unique[i].toString()});
        }

        }
      else{
        this.showloader= false;
        this.locationdiv=true;
        Swal.fire("info",data.StatusMessage,"info");
        }
    },
      error => console.log(error));
 

  }


 lon=0+1; local=0+1;i = 0 + 1; j = 0 + 1; k = 0 + 1; l = 0 + 1; m = 0 + 1; g = 0 + 1; bg = 0 + 1; ms = 0 + 1; rl = 0 + 1; wl = 0 + 1; fl = 0 + 1;nl=0+1;lev=0+1;
  Employclick() {

    if (this.i == 1) {
    
      this.getemploylist();
      this.i++;
    }
    else {
  
      this.i = 0;

    }
  }

  Designationclick() {
    if (this.j == 1) {
    
      this.getdesignationlist();
      this.j++;
    }
    else {
      this.j = 0;
    }
  }

  Genderclick() {

    if (this.k == 1) {
      this.getGenderlist();
      this.k++;
    }
    else {
      this.k = 0;

    }

  }
  Sectionclick() {
    if (this.l == 1) {
     
     
      this.Getsectionlist();
     
      //alert("called");
      this.l++;

    }
    else {

      this.l = 0;
      // alert("not called");
    }
  }
  EducationQualificationclick() {


    if (this.m == 1) {
      this.getEducationallist();
      this.m++;
    }
    else {
      this.m = 0;

    }

  }

  bloodgroupclick() {

    if (this.bg == 1) {
      this.getBloodgrouplist();
      this.bg++;
    }
    else {
      this.bg = 0;

    }

  }

  maritalstatusclick() {

    if (this.ms == 1) {
      this.getmaritalstatuslist();
      this.ms++;
    }
    else {
      this.ms = 0;

    }

  }

  worklocationclick() {

    if (this.wl == 1) {
      this.getworkloclist();
      this.wl++;
    }
    else {
      this.wl = 0;

    }
  }
  familyrelationclick() {

    if (this.fl == 1) {
      this.getrelationlist();
      this.fl++;
    }
    else {
      this.fl = 0;

    }

  }

  Nationalityclick(){

    if (this.nl == 1) {
      this.GetNationalitylist();
      this.nl++;
    }
    else {
      this.nl = 0;

    }

  }



  Leavesclick(){
    if (this.lev == 1) {
      this.Getleaveslist();
      this.lev++;
    }
    else {
      this.lev = 0;

    }



  }

  Loanclick(){
    if (this.lon == 1) {
      this.Getloanlist();
      this.lon++;
    }
    else {
      this.lon = 0;

    }

  }

  Locationclick(){
    if (this.local == 1) {
      this.GetLocationlist();
      this.local++;
    }
    else {
      this.local = 0;

    }



  }
  employeeaddform = this.formBuilder.group({
    empremarks: ['', Validators.required],
    empaddtype:['',Validators.required],
    locddl:[''],
    Emprelddl:['']
  });

  employeeeditForm = this.formBuilder.group({
    Godownswarehsetype:[''],
    editemptype:[''],
    empactive:['',Validators.required],
   empeditremarks:['', Validators.required],
   editlocddl:['']
  });


  Locationadd(val){

    
   this.GetLocationlist();    
    this.locval=val;
    this.locationdiv=true;
    this.employeeaddform.reset();
    this.employeeeditForm.reset();
    this.employeeaddform.controls.locddl.setValue("0");
    this.submitted = false;
    this.title="Location";
     this.addempModal.show();
  }


  Locationedit(loclvid,loclvname,loclvact,wloctype,locvalt){

    this.editsubmitted = false;
    this.editempidandemptype=true;
    const req = new InputRequest();
    req.TYPEID = "work_location_sub_cat";
    this.service.postData(req, "GetLocationMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.locedittype=data.Details;
        this.title="Location";
        this.f.Godownswarehsetype.setValue(loclvid);
        this.f.editemptype.setValue(loclvname);
        this.f.empactive.setValue(loclvact.toString());
        this.f.editlocddl.setValue(wloctype.toString());
        this.locval=locvalt;
        this.locationdiv=true;
      }

    },
      error => console.log(error));


    this.editempModal.show();



  }
  
  
  
  Leavesadd(){

    this.employeeaddform.reset();
    this.employeeeditForm.reset();
    this.submitted = false;
    this.title="Leaves";    
  this.addempModal.show();
  this.Empreldiv=false;
  }

  Leavesedit(levid,levname,levact){

    this.editsubmitted = false;
    this.editempidandemptype=true;
  //
    const req = new InputRequest();
    req.INPUT_01 = "LEAVES";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.title="Leaves";
        this.f.Godownswarehsetype.setValue(levid);
        this.f.editemptype.setValue(levname);
        this.f.empactive.setValue(levact.toString());
       // alert(actinact);
        //alert(this.f.empactive.setValue(actinact));
      }

    },
      error => console.log(error));


    this.editempModal.show();



  }

  addEmployee() {
    this.employeeaddform.reset();
    this.employeeeditForm.reset();
    this.submitted = false;
    this.title="Employee";    
    this.Empreldiv=true;
    this.locationdiv=false;
    this.employeeaddform.controls.Emprelddl.setValue('');
    this.addempModal.show();
  
  }

  Loanadd(){

    this.employeeaddform.reset();
    this.employeeeditForm.reset();
    this.submitted = false;
    this.title="Loan";    
    this.Empreldiv=false;
    this.locationdiv=false;
    this.addempModal.show();
  }


  hideaddempModal(): void {
    this.employeeeditForm.reset();
    this.employeeaddform.reset();
   this.locationvalidation=false;
    this.addempModal.hide();
    this.Empreldiv=false;
//    this.submitted =false;
  }

  Loanedit(lonid,lonname,lonact){

    this.editsubmitted = false;
    this.locationdiv=false;
    this.Empreldiv=false;
    this.editempidandemptype=true;
    const req = new InputRequest();
    req.INPUT_01 = "LOAN_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.title="Loan";
        this.f.Godownswarehsetype.setValue(lonid);
        this.f.editemptype.setValue(lonname);
        this.f.empactive.setValue(lonact.toString());
       // alert(actinact);
        //alert(this.f.empactive.setValue(actinact));
      }

    },
      error => console.log(error));


    this.editempModal.show();


  }


  editEmp(editid:any, editname:any,actinact:any) {
    this.editsubmitted = false;
    this.locationdiv=false;
    this.Empreldiv=false;
    this.editempidandemptype=true;
  //
    const req = new InputRequest();
    req.INPUT_01 = "EMP_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.title="Employee";
        this.f.Godownswarehsetype.setValue(editid);
        this.f.editemptype.setValue(editname);
        this.f.empactive.setValue(actinact.toString());
       // alert(actinact);
        //alert(this.f.empactive.setValue(actinact));
      }

    },
      error => console.log(error));


    this.editempModal.show();


  }

  

  hideeditempModal(): void {
    this.editempModal.hide();
  this.employeeeditForm.reset();
  this.locationdiv=false;
this.locationvalidation=false;
  this.employeeaddform.reset();
 // this.submitted =false;
  }

  historyEmployee(val)
   {
//    this.locationdiv=false;

    const req = new InputRequest();
    if(val=="emphistory"){

      req.INPUT_01 = "EMP_TYPE";
    
     }
     else if(val=="Desighistory"){
      req.INPUT_01 = "DESIGNATION";
  
     }
     else if(val=="Gndhistory"){
      req.INPUT_01 = "GENDER";
  
    }
    else if(val=="Sechistory"){

      req.INPUT_01 = "SECTION";
      
    }
    else if(val=="Eduhistory"){

      req.INPUT_01 = "EDU";
    }
    else if(val=="BGhistory"){
      req.INPUT_01 = "BLOOD_GROUP";
    }
    else if(val=="Nathistory"){
      req.INPUT_01 = "NATIONALITY";
    }
    else if(val=="MShistory"){
      req.INPUT_01 = "MARITAL_STATUS";
  
    }
    else if(val=="WLhistory"){

      req.INPUT_01 = "WORK_LOCATION";
    }
    else if(val=="FRhistory"){

      req.INPUT_01 = "RELATION";
    }
    else if(val=="leaveshistory"){

      req.INPUT_01 = "LEAVES";

    }
   else if(val=="locationshistory")
   {


if(val=="loanshistory")
{

  req.INPUT_01 = "LOAN_TYPE";

}
    
if(this.locval=="addloc")
  {
    req.INPUT_01 = this.employeeaddform.controls.locddl.value;

  }
else{
  req.INPUT_01 = this.employeeeditForm.controls.editlocddl.value;

}


   }
    else{
      this.employeeeditForm.reset();
      this.employeeaddform.reset();
      this.historyempModal.hide();
       return false;
    }
    req.INPUT_02="MASTERS";
    this.showloader= true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
        if (data.StatusCode == "100") {
          this.MasterHistlist = data.Details;
          this.employeeaddform.reset();
          this.employeeeditForm.reset();
          this.historyempModal.show();
          this.showloader= false;
          this.locationdiv=false;

        }
        else{
  
          Swal.fire("info",data.StatusMessage,"info");
          this.employeeaddform.reset();
          this.employeeeditForm.reset();
          this.historyempModal.hide();
          this.showloader= false;  
          this.locationdiv=false;

        }
      },
        error => console.log(error));
  
    }
    
  

  hidehistoryempModal(): void {
    this.employeeaddform.reset();
    this.employeeeditForm.reset();
    this.historyempModal.hide();
    this.locationvalidation=false;
    
  }


  get f1() { return this.employeeaddform.controls; }

  get f() { return this.employeeeditForm.controls; }

  //get riceformval() { return this.riceForm.controls; }
  employaddclick() {

    var ccm=this.employeeaddform.controls.Emprelddl.value;
   
    this.locationvalidation=false;
    this.showloader= true;

    this.submitted = true;

   
  if (this.employeeaddform.invalid) {
    this.showloader= false;    
    return;
    
  }
  const req = new InputRequest();
  
    if(this.title=="Employee")
    {
   
   
      if(this.employeeaddform.controls.Emprelddl.value==""||this.employeeaddform.controls.Emprelddl.value==null||this.employeeaddform.controls.Emprelddl.value==undefined)
      {
  
        Swal.fire("info","Please select Employee type ","info");
        this.showloader= false;  
       
        return false;
  
      }
  
       
    req.INPUT_01 = this.employeeaddform.controls.Emprelddl.value;
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empaddarray=data.result;
     // this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        if(this.empaddarray.Details[0].rtN_ID=="1"){
        this.i=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.getemploylist();
        this.locationdiv=false;
        this.showloader= false;

        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{

            this.showloader= false;
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.locationdiv=false;
            Swal.fire("info","This Master IsAlready Registered","info");

           }
          }
           
           else{
            this.showloader= false;
            Swal.fire("info",this.empaddarray.StatusMessage,"info");
            this.locationdiv=false;


           } 
  },
      error => console.log(error));

  }

  if(this.title=="Designation"){
    
    req.INPUT_01 = "DESIGNATION";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";

    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        if(this.empaddarray.Details[0].rtN_ID=="1"){
        this.j=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.getdesignationlist();
        this.locationdiv=false;
        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            
            Swal.fire("info","This Master IsAlready Registered","info");
            this.showloader= false;
            this.locationdiv=false;
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            
           }
          }
           
           else{
            this.locationdiv=false;

            this.showloader= false;
            Swal.fire("info",this.empaddarray.StatusMessage,"info");
            

           }     // this.f.editemptype.setValue(editname);
      },
      error => console.log(error));



  }

  if(this.title=="Gender"){
   
    req.INPUT_01 = "GENDER";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        if(this.empaddarray.Details[0].rtN_ID=="1"){
        this.k=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.getGenderlist();
        this.locationdiv=false;

        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.locationdiv=false;

            this.showloader= false;
           }
          }
           
           else{
            this.locationdiv=false;

            this.showloader= false;
            Swal.fire("info",this.empaddarray.StatusMessage,"info");
       


           }      },
      error => console.log(error));



  }

  if(this.title=="EducationQualification"){
    
    req.INPUT_01 = "EDU";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        if(this.empaddarray.Details[0].rtN_ID=="1"){
        this.m=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.getEducationallist();
        this.locationdiv=false;

        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.locationdiv=false;

            this.showloader= false;
           }
          }
           
           else{
            Swal.fire("info",this.empaddarray.StatusMessage,"info");
            this.locationdiv=false;
       
            this.showloader= false;

           }      },
      error => console.log(error));



  }

  if(this.title=="BloodGroup"){
   
    req.INPUT_01 = "BLOOD_GROUP";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        if(this.empaddarray.Details[0].rtN_ID=="1"){
        this.bg=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.getBloodgrouplist();
        this.locationdiv=false;

        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();

            this.locationdiv=false;
            this.showloader= false;
           }
          }
           
           else{
            Swal.fire("info",this.empaddarray.StatusMessage,"info");
            this.locationdiv=false;
       
            this.showloader= false;

           } 
      },
      error => console.log(error));



  }

  if(this.title=="MaritalStatus"){
    
    req.INPUT_01 = "MARITAL_STATUS";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        if(this.empaddarray.Details[0].rtN_ID=="1"){
        this.ms=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.getmaritalstatuslist();
        this.locationdiv=false;

        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.locationdiv=false;

            this.showloader= false;
           }
          }
           
           else{
            Swal.fire("info",this.empaddarray.StatusMessage,"info");
            this.locationdiv=false;
       
            this.showloader= false;

           }
      },
      error => console.log(error));



  }

  if(this.title=="WorkLocation"){
   
    req.INPUT_01 = "WORK_LOCATION";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        if(this.empaddarray.Details[0].rtN_ID=="1"){
        this.wl=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.getworkloclist();
        this.locationdiv=false;

        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
        
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.locationdiv=false;

            this.showloader= false;
           }
          }
           
           else{
            Swal.fire("info",this.empaddarray.StatusMessage,"info");
            this.locationdiv=false;

            this.showloader= false;


           } 
      },
      error => console.log(error));



  }

  if(this.title=="Relation"){
  
    req.INPUT_01 = "RELATION";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        if(this.empaddarray.Details[0].rtN_ID=="1"){
        this.fl=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.getrelationlist();
        this.locationdiv=false;

        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.locationdiv=false;

            this.showloader= false;
           }
          }
           
           else{
            Swal.fire("info",this.empaddarray.StatusMessage,"info");
            this.locationdiv=false;
       
            this.showloader= false;

           } 
      },
      error => console.log(error));



  }

  if(this.title=="Nationality"){
 
    req.INPUT_01 = "NATIONALITY";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        if(this.empaddarray.Details[0].rtN_ID=="1"){
        this.fl=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.GetNationalitylist();
        this.locationdiv=false;

        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.locationdiv=false;

            this.showloader= false;
           }
          }
           
           else{
            Swal.fire("info",this.empaddarray.StatusMessage,"info");
            this.locationdiv=false;

            this.showloader= false;


           } 
      },
      error => console.log(error));



  }


  if(this.title=="Section"){
 
    req.INPUT_01 = "SECTION";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        if(this.empaddarray.Details[0].rtN_ID=="1"){
        this.l=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.Getsectionlist();
        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.locationdiv=false;

            this.showloader= false;
           }
          }
           
           else{
            Swal.fire("info",this.empaddarray.StatusMessage,"info");
            this.locationdiv=false;
       
            this.showloader= false;

           } 
      },
      error => console.log(error));



  }


  if(this.title=="Leaves"){

    req.INPUT_01 = "LEAVES";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.empleavearray=data.result;
      if (this.empleavearray.StatusCode == "100") {

        if(this.empleavearray.Details[0].rtN_ID=="1"){
        this.lev=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.Getleaveslist();
        this.locationdiv=false;

        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.locationdiv=false;

            this.showloader= false;
           }
          }
           
           else{
            Swal.fire("info",this.empleavearray.StatusMessage,"info");
            this.locationdiv=false;
       
            this.showloader= false;

           } 
      },
      error => console.log(error));



  }

  if(this.title=="Loan"){

    req.INPUT_01 = "LOAN_TYPE";
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.emploanarray=data.result;
      if (this.emploanarray.StatusCode == "100") {

        if(this.emploanarray.Details[0].rtN_ID=="1"){
        this.lon=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.Getloanlist();
        this.locationdiv=false;

        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.locationdiv=false;

            this.showloader= false;
           }
          }
           
           else{
            Swal.fire("info",this.emploanarray.StatusMessage,"info");
            this.locationdiv=false;
       
            this.showloader= false;

           } 
      },
      error => console.log(error));



  }

  if(this.title=="Location")
  {

  var loccheck=this.employeeaddform.controls.locddl.value
  
  if(loccheck==""|| loccheck=="0"||loccheck==null){
    this.locationvalidation=true;
    this.showloader=false;
    return false;}
    else{
      this.locationvalidation=false;
      this.submitted=false
      this.showloader=false;
    }
    req.INPUT_01 =this.employeeaddform.controls.locddl.value
    
    req.INPUT_02=this.employeeaddform.controls.empaddtype.value;
    req.INPUT_03=this.employeeaddform.controls.empremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_04=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "employeemasterreg").subscribe(data => {
      this.locarry=data.result;
      if (this.locarry.StatusCode == "100") {

        if(this.locarry.Details[0].rtN_ID=="1"){
        this.local=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        Swal.fire("success","Data Added Successfully","success");
        this.GetLocationlist();
        this.showloader= false;
        // this.f.Godownswarehsetype.setValue(editid);
           }  
           else{
            Swal.fire("info","This Master IsAlready Registered","info");
            this.employeeaddform.reset();
            this.employeeeditForm.reset();
            this.addempModal.hide();
            this.showloader= false;
           }
          }
           
           else{
            Swal.fire("info",this.locarry.StatusMessage,"info");
     //       this.locationdiv=false;
       
            this.showloader= false;

           } 
      },
      error => console.log(error));



  }

}

  
  employeditclick() {
    this.showloader= true;
    this.editsubmitted = true;
    const req = new InputRequest();
    if (this.employeeeditForm.invalid) {
      this.showloader= false;
      return;
    }
    
    if(this.title=="Employee"){
    req.INPUT_01 = "EMP_TYPE";
    req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
   req.INPUT_03=this.employeeeditForm.controls.empactive.value;
    req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
    req.USER_NAME=this.Username;
    req.INPUT_05=this.logUserrole;
    req.CALL_SOURCE="Web";
    this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
      this.empaeditarray=data.result;
      if (this.empaeditarray.StatusCode == "100") {
        if(this.empaeditarray.Details[0].rtN_ID=="1"){
        this.i=0;
        this.employeeaddform.reset();
        this.employeeeditForm.reset();
        this.addempModal.hide();
        this.editempModal.hide();
        this.getemploylist();
        this.locationdiv=false;

        this.showloader= false;
        Swal.fire("success","Data Updated Successfully","success");
      }  
      else{
       Swal.fire("info","Data Updation Failed","info");
       this.employeeaddform.reset();
       this.employeeeditForm.reset();
       this.addempModal.hide();
       this.editempModal.hide();
       this.locationdiv=false;

       this.showloader= false;
      }
        // this.f.Godownswarehsetype.setValue(editid);
           }
           
           else{
            Swal.fire("info",this.empaeditarray.StatusMessage,"info");
            this.locationdiv=false;

            this.showloader= false;


           } 
                // this.f.editemptype.setValue(editname);
      },
      error => console.log(error));
    }
    if(this.title=="Designation"){

      req.INPUT_01 = "DESIGNATION";
      req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
     req.INPUT_03=this.employeeeditForm.controls.empactive.value;
      req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
      req.USER_NAME=this.Username;
    req.INPUT_05=this.logUserrole;
    req.CALL_SOURCE="Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.empaeditarray=data.result;
        if (this.empaeditarray.StatusCode == "100") {
          if(this.empaeditarray.Details[0].rtN_ID=="1"){
          this.j=0;
          this.employeeaddform.reset();
          this.employeeeditForm.reset();
          this.addempModal.hide();
          this.editempModal.hide();
          this.getdesignationlist();
          Swal.fire("success","Data Updated Successfully","success");

          this.locationdiv=false;
          this.showloader= false;
          // this.f.Godownswarehsetype.setValue(editid);
             }
             
            else{
             Swal.fire("info","Data Updation Failed","info");
             this.employeeaddform.reset();
             this.employeeeditForm.reset();
             this.addempModal.hide();
             this.editempModal.hide();
             this.locationdiv=false;

             this.showloader= false;
            }
          }
 else{
             Swal.fire("info",this.empaeditarray.StatusMessage,"info");
             this.locationdiv=false;
        
             this.showloader= false;
 
            }     // this.f.editemptype.setValue(editname);
        },
        error => console.log(error));
        
    }
    
    if(this.title=="Gender"){

      req.INPUT_01 = "GENDER";
      req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
     req.INPUT_03=this.employeeeditForm.controls.empactive.value;
      req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
      req.USER_NAME=this.Username;
      req.INPUT_05=this.logUserrole;
      req.CALL_SOURCE="Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.empaeditarray=data.result;
        if (this.empaeditarray.StatusCode == "100") {
          if(this.empaeditarray.Details[0].rtN_ID=="1"){
          this.k=0;
          this.employeeaddform.reset();
          this.employeeeditForm.reset();
          this.addempModal.hide();
          this.editempModal.hide();
          this.getGenderlist();
          this.locationdiv=false;

          Swal.fire("success","Data Updated Successfully","success");
          this.showloader= false;
          // this.f.Godownswarehsetype.setValue(editid);
        }  
        else{
         Swal.fire("info","Data Updation Failed","info");
         this.employeeaddform.reset();
         this.employeeeditForm.reset();
         this.addempModal.hide();
         this.editempModal.hide();
         this.locationdiv=false;

         this.showloader= false;
        }
       }
else{
         Swal.fire("info",this.empaeditarray.StatusMessage,"info");
         this.locationdiv=false;
    
         this.showloader= false;

        }
// this.f.editemptype.setValue(editname);
        },
        error => console.log(error));
        
    }

    if(this.title=="EducationQualification"){

      req.INPUT_01 = "EDU";
      req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
     req.INPUT_03=this.employeeeditForm.controls.empactive.value;
      req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
      req.USER_NAME=this.Username;
    req.INPUT_05=this.logUserrole;
    req.CALL_SOURCE="Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.empaeditarray=data.result;
        if (this.empaeditarray.StatusCode == "100") {
          if(this.empaeditarray.Details[0].rtN_ID=="1"){
          this.m=0;
          this.employeeaddform.reset();
          this.employeeeditForm.reset();
          this.addempModal.hide();
          this.editempModal.hide();
          this.getEducationallist();
          Swal.fire("success","Data Updated Successfully","success");
          this.locationdiv=false;

          this.showloader= false;
          // this.f.Godownswarehsetype.setValue(editid);
        }  
        else{
         Swal.fire("info","Data Updation Failed","info");
         this.employeeaddform.reset();
         this.employeeeditForm.reset();
         this.addempModal.hide();
         this.editempModal.hide();
         this.locationdiv=false;

         this.showloader= false;
        }
       }
else{
         Swal.fire("info",this.empaeditarray.StatusMessage,"info");
         this.locationdiv=false;
    
         this.showloader= false;

        }    // this.f.editemptype.setValue(editname);
        },
        error => console.log(error));
        
    }
    if(this.title=="BloodGroup"){

      req.INPUT_01 = "BLOOD_GROUP";
      req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
     req.INPUT_03=this.employeeeditForm.controls.empactive.value;
      req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
      req.USER_NAME=this.Username;
      req.INPUT_05=this.logUserrole;
      req.CALL_SOURCE="Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.empaeditarray=data.result;
        if (this.empaeditarray.StatusCode == "100") {
          if(this.empaeditarray.Details[0].rtN_ID=="1"){
          this.bg=0;
          this.employeeaddform.reset();
          this.employeeeditForm.reset();
          this.addempModal.hide();
          this.editempModal.hide();
          this.getBloodgrouplist();
          Swal.fire("success","Data Updated Successfully","success");
          this.locationdiv=false;

          this.showloader= false;
          // this.f.Godownswarehsetype.setValue(editid);
        }  
        else{
         Swal.fire("info","Data Updation Failed","info");
         this.employeeaddform.reset();
         this.employeeeditForm.reset();
         this.addempModal.hide();
         this.editempModal.hide();
         this.locationdiv=false;

         this.showloader= false;
        }
       }
else{
         Swal.fire("info",this.empaeditarray.StatusMessage,"info");
         this.locationdiv=false;
    
         this.showloader= false;

        }    // this.f.editemptype.setValue(editname);
     
  // this.f.editemptype.setValue(editname);
        },
        error => console.log(error));
        
    }
    if(this.title=="MaritalStatus"){

      req.INPUT_01 = "MARITAL_STATUS";
      req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
     req.INPUT_03=this.employeeeditForm.controls.empactive.value;
      req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
      req.USER_NAME=this.Username;
      req.INPUT_05=this.logUserrole;
      req.CALL_SOURCE="Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.empaeditarray=data.result;
        if (this.empaeditarray.StatusCode == "100") {
          if(this.empaeditarray.Details[0].rtN_ID=="1"){
          this.ms=0;
          this.employeeaddform.reset();
          this.employeeeditForm.reset();
          this.addempModal.hide();
          this.editempModal.hide();
          this.getmaritalstatuslist();
          this.locationdiv=false;

          Swal.fire("success","Data Updated Successfully","success");
          this.showloader= false;
          // this.f.Godownswarehsetype.setValue(editid);
        }  
        else{
         Swal.fire("info","Data Updation Failed","info");
         this.employeeaddform.reset();
         this.employeeeditForm.reset();
         this.addempModal.hide();
         this.editempModal.hide();
         this.locationdiv=false;

         this.showloader= false;
        }
       }
else{
         Swal.fire("info",this.empaeditarray.StatusMessage,"info");
         this.locationdiv=false;

         this.showloader= false;


        }    // this.f.editemptype.setValue(editname);
          // this.f.editemptype.setValue(editname);
        },
        error => console.log(error));
        
    }
  
    if(this.title=="WorkLocation"){

      req.INPUT_01 = "WORK_LOCATION";
      req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
     req.INPUT_03=this.employeeeditForm.controls.empactive.value;
      req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
      req.USER_NAME=this.Username;
    req.INPUT_05=this.logUserrole;
    req.CALL_SOURCE="Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.empaeditarray=data.result;
        if (this.empaeditarray.StatusCode == "100") {
          if(this.empaeditarray.Details[0].rtN_ID=="1"){
          this.wl=0;
          this.employeeaddform.reset();
          this.employeeeditForm.reset();
          this.addempModal.hide();
          this.editempModal.hide();
          this.getworkloclist();
          this.locationdiv=false;

          Swal.fire("success","Data Updated Successfully","success");
          this.showloader= false;
          // this.f.Godownswarehsetype.setValue(editid);
        }  
        else{
         Swal.fire("info","Data Updation Failed","info");
         this.employeeaddform.reset();
         this.employeeeditForm.reset();
         this.addempModal.hide();
         this.editempModal.hide();
         this.locationdiv=false;

         this.showloader= false;
        }
       }
else{
         Swal.fire("info",this.empaeditarray.StatusMessage,"info");
         this.locationdiv=false;
    
         this.showloader= false;

        }    // this.f.editemptype.setValue(editname);
            // this.f.editemptype.setValue(editname);
        },
        error => console.log(error));
        
    }
  
    if(this.title=="Relation"){

      req.INPUT_01 = "RELATION";
      req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
      req.INPUT_03=this.employeeeditForm.controls.empactive.value;
      req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
      req.USER_NAME=this.Username;
      req.INPUT_05=this.logUserrole;
      req.CALL_SOURCE="Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.empaeditarray=data.result;
        if (this.empaeditarray.StatusCode == "100") {
          if(this.empaeditarray.Details[0].rtN_ID=="1"){
          this.fl=0;
          this.employeeaddform.reset();
          this.employeeeditForm.reset();
          this.addempModal.hide();
          this.editempModal.hide();
          this.getrelationlist();
          Swal.fire("success","Data Updated Successfully","success");
          this.locationdiv=false;

          this.showloader= false;
          // this.f.Godownswarehsetype.setValue(editid);
        }  
        else{
         Swal.fire("info","Data Updation Failed","info");
         this.employeeaddform.reset();
         this.employeeeditForm.reset();
         this.addempModal.hide();
         this.editempModal.hide();
         this.locationdiv=false;

         this.showloader= false;
        }
       }
else{
         Swal.fire("info",this.empaeditarray.StatusMessage,"info");
         this.locationdiv=false;
    
         this.showloader= false;

        }    // this.f.editemptype.setValue(editname);
           // this.f.editemptype.setValue(editname);
        },
        error => console.log(error));
        
    }

if(this.title=="Nationality")
{
  req.INPUT_01 = "NATIONALITY";
  req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
  req.INPUT_03=this.employeeeditForm.controls.empactive.value;
  req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
  req.USER_NAME=this.Username;
  req.INPUT_05=this.logUserrole;
  req.CALL_SOURCE="Web";
  this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
    this.empaeditarray=data.result;
    if (this.empaeditarray.StatusCode == "100") {
      if(this.empaeditarray.Details[0].rtN_ID=="1"){
      this.nl=0;
      this.employeeaddform.reset();
      this.employeeeditForm.reset();
      this.addempModal.hide();
      this.editempModal.hide();
      this.GetNationalitylist();
      this.locationdiv=false;

      Swal.fire("success","Data Updated Successfully","success");
      this.showloader= false;
      // this.f.Godownswarehsetype.setValue(editid);
    }  
    else{
     Swal.fire("info","Data Updation Failed","info");
     this.employeeaddform.reset();
     this.employeeeditForm.reset();
     this.addempModal.hide();
     this.editempModal.hide();
     this.locationdiv=false;

     this.showloader= false;
    }
   }
else{
     Swal.fire("info",this.empaeditarray.StatusMessage,"info");
     this.locationdiv=false;

     this.showloader= false;


    }    // this.f.editemptype.setValue(editname);
         // this.f.editemptype.setValue(editname);
    },
    error => console.log(error));

}
if(this.title=="Section")
{
  req.INPUT_01 = "SECTION";
  req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
  req.INPUT_03=this.employeeeditForm.controls.empactive.value;
  req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
  req.USER_NAME=this.Username;
  req.INPUT_05=this.logUserrole;
  req.CALL_SOURCE="Web";
  this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
    this.empaeditarray=data.result;
    if (this.empaeditarray.StatusCode == "100") {
      if(this.empaeditarray.Details[0].rtN_ID=="1"){

      this.l=0;
      this.employeeaddform.reset();
      this.employeeeditForm.reset();
      this.addempModal.hide();
      this.editempModal.hide();
      this.Getsectionlist();
      Swal.fire("success","Data Updated Successfully","success");
      this.locationdiv=false;

      this.showloader= false;
      // this.f.Godownswarehsetype.setValue(editid);
    }  
    else{
     Swal.fire("info","Data Updation Failed","info");
     this.employeeaddform.reset();
     this.employeeeditForm.reset();
     this.addempModal.hide();
     this.editempModal.hide();
     this.locationdiv=false;

     this.showloader= false;
    }
   }
else{
     Swal.fire("info",this.empaeditarray.StatusMessage,"info");
     this.locationdiv=false;

     this.showloader= false;


    }    // this.f.editemptype.setValue(editname);
         // this.f.editemptype.setValue(editname);
    },
    error => console.log(error));

}

if(this.title=="Leaves")
{
  req.INPUT_01 = "LEAVES";
  req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
  req.INPUT_03=this.employeeeditForm.controls.empactive.value;
  req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
  req.USER_NAME=this.Username;
  req.INPUT_05=this.logUserrole;
  req.CALL_SOURCE="Web";
  this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
    this.LEAVEeditarray=data.result;
    if (this.LEAVEeditarray.StatusCode == "100") {
      if(this.LEAVEeditarray.Details[0].rtN_ID=="1"){

      this.lev=0;
      this.employeeaddform.reset();
      this.employeeeditForm.reset();
      this.addempModal.hide();
      this.editempModal.hide();
      this.Getleaveslist();
      this.locationdiv=false;

      Swal.fire("success","Data Updated Successfully","success");
      this.showloader= false;
      // this.f.Godownswarehsetype.setValue(editid);
    }  
    else{
     Swal.fire("info","Data Updation Failed","info");
     this.employeeaddform.reset();
     this.employeeeditForm.reset();
     this.addempModal.hide();
     this.editempModal.hide();
     this.locationdiv=false;

     this.showloader= false;
    }
   }
else{
     Swal.fire("info",this.LEAVEeditarray.StatusMessage,"info");
     this.locationdiv=false;

     this.showloader= false;



    }    // this.f.editemptype.setValue(editname);
         // this.f.editemptype.setValue(editname);
    },
    error => console.log(error));

}
 

if(this.title=="Loan")
{
  req.INPUT_01 = "LOAN_TYPE";
  req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
  req.INPUT_03=this.employeeeditForm.controls.empactive.value;
  req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
  req.USER_NAME=this.Username;
  req.INPUT_05=this.logUserrole;
  req.CALL_SOURCE="Web";
  this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
    this.LOANeditarray=data.result;
    if (this.LOANeditarray.StatusCode == "100") {
      if(this.LOANeditarray.Details[0].rtN_ID=="1"){

      this.lon=0;
      this.employeeaddform.reset();
      this.employeeeditForm.reset();
      this.addempModal.hide();
      this.editempModal.hide();
      this.Getloanlist();
      this.locationdiv=false;

      Swal.fire("success","Data Updated Successfully","success");
      this.showloader= false;
      // this.f.Godownswarehsetype.setValue(editid);
    }  
    else{
     Swal.fire("info","Data Updation Failed","info");
     this.employeeaddform.reset();
     this.employeeeditForm.reset();
     this.addempModal.hide();
     this.editempModal.hide();
     this.locationdiv=false;

     this.showloader= false;
    }
   }
else{
     Swal.fire("info",this.LOANeditarray.StatusMessage,"info");
     this.locationdiv=false;

     this.showloader= false;



    }    // this.f.editemptype.setValue(editname);
         // this.f.editemptype.setValue(editname);
    },
    error => console.log(error));

}



if(this.title=="Location")
{
  req.INPUT_01 = this.employeeeditForm.controls.editlocddl.value;
  req.INPUT_02=this.employeeeditForm.controls.Godownswarehsetype.value;
  req.INPUT_03=this.employeeeditForm.controls.empactive.value;
  req.INPUT_04=this.employeeeditForm.controls.empeditremarks.value;
  req.USER_NAME=this.Username;
  req.INPUT_05=this.logUserrole;
  req.CALL_SOURCE="Web";
  this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
    this.loceditarray=data.result;
    if (this.loceditarray.StatusCode == "100") {
      if(this.loceditarray.Details[0].rtN_ID=="1"){

      this.local=0;
      this.employeeaddform.reset();
      this.employeeeditForm.reset();
      this.addempModal.hide();
      this.editempModal.hide();
      this.GetLocationlist();
     

      Swal.fire("success","Data Updated Successfully","success");
      this.showloader= false;
      // this.f.Godownswarehsetype.setValue(editid);
    }  
    else{
     Swal.fire("info","Data Updation Failed","info");
     this.employeeaddform.reset();
     this.employeeeditForm.reset();
     this.addempModal.hide();
     this.editempModal.hide();
     

     this.showloader= false;
    }
   }
else{
     Swal.fire("info",this.loceditarray.StatusMessage,"info");
     this.locationdiv=false;

     this.showloader= false;



    }    // this.f.editemptype.setValue(editname);
         // this.f.editemptype.setValue(editname);
    },
    error => console.log(error));

}

}
  
  Designationadd()
  {
    this.employeeaddform.reset();
    this.employeeeditForm.reset();
    this.submitted =false;
    this.title="Designation";
    this.locationdiv=false;
    this.Empreldiv=false;
    this.addempModal.show();

  }
Designationedit(dmid:any,dmname:any,desigradio:any){

  this.editsubmitted =false;
  this.Empreldiv=false;
  this.title="Designation";
  this.editempModal.show();
  const req = new InputRequest();
  req.INPUT_01 = "DESIGNATION";
  this.service.postData(req, "GetMasterDetails").subscribe(data => {
    if (data.StatusCode == "100") {
      this.locationdiv=false;

      this.editempModal.show();
      this.f.Godownswarehsetype.setValue(dmid);
      this.f.editemptype.setValue(dmname);
      
      this.f.empactive.setValue(desigradio.toString());

    }

  },
    error => console.log(error));



}

Genderadd()

{this.submitted =false;
  this.employeeaddform.reset();
  this.employeeeditForm.reset();
  this.locationdiv=false;
  this.Empreldiv=false;
  this.title="Gender";

  this.addempModal.show();

}
Genderedit(gndid:any,gndname:any,gndact:any){
  this.editsubmitted = false;
  this.Empreldiv=false;
this.title="Gender";
this.editempModal.show();
const req = new InputRequest();
req.INPUT_01 = "GENDER";
this.service.postData(req, "GetMasterDetails").subscribe(data => {
  if (data.StatusCode == "100") {

    this.locationdiv=false;
    this.editempModal.show();
    this.f.Godownswarehsetype.setValue(gndid);
    this.f.editemptype.setValue(gndname);
    
    this.f.empactive.setValue(gndact.toString());

  }

},
  error => console.log(error));



}

EducationQualificationadd()
{this.submitted =false;
  this.employeeaddform.reset();
  this.employeeeditForm.reset();
  this.locationdiv=false;
  this.Empreldiv=false;
  this.title="EducationQualification";

  this.addempModal.show();

}
EducationQualificationedit(Eqid:any,Eqname:any,Eqact:any){
  this.editsubmitted = false;
  this.Empreldiv=false;
  this.title="EducationQualification";
  this.editempModal.show();
const req = new InputRequest();
req.INPUT_01 = "EDU";
this.service.postData(req, "GetMasterDetails").subscribe(data => {
  if (data.StatusCode == "100") {
    this.locationdiv=false;

    this.editempModal.show();
    this.f.Godownswarehsetype.setValue(Eqid);
    this.f.editemptype.setValue(Eqname);
    
    this.f.empactive.setValue(Eqact.toString());

  }

},
  error => console.log(error));



}


bloodgroupadd()
{
  this.submitted =false;
  this.employeeaddform.reset();
  this.employeeeditForm.reset();
  this.Empreldiv=false;
  this.title="BloodGroup";
  this.locationdiv=false;

  this.addempModal.show();

}
Bloodgroupedit(bgid:any,bgname:any,bgact:any){
  this.Empreldiv=false;
  this.editsubmitted = false;
  this.title="BloodGroup";
  this.editempModal.show();
const req = new InputRequest();
req.INPUT_01 = "BLOOD_GROUP";
this.service.postData(req, "GetMasterDetails").subscribe(data => {
  if (data.StatusCode == "100") {
    this.locationdiv=false;

    this.editempModal.show();
    this.f.Godownswarehsetype.setValue(bgid);
    this.f.editemptype.setValue(bgname);
    
    this.f.empactive.setValue(bgact.toString());

  }

},
  error => console.log(error));



}

Maritalstatusadd()
{this.submitted =false;
  this.employeeaddform.reset();
  this.employeeeditForm.reset();
  this.Empreldiv=false;
  this.title="MaritalStatus";
  this.locationdiv=false;
  this.addempModal.show();
}


Maritalstatusedit(msid:any,msname:any,msact:any){
  this.editsubmitted = false;
  this.Empreldiv=false;
  this.title="MaritalStatus";
  this.editempModal.show();
const req = new InputRequest();
req.INPUT_01 = "MARITAL_STATUS";
this.service.postData(req, "GetMasterDetails").subscribe(data => {
  if (data.StatusCode == "100") {
    this.locationdiv=false;

    this.editempModal.show();
    this.f.Godownswarehsetype.setValue(msid);
    this.f.editemptype.setValue(msname);
    
    this.f.empactive.setValue(msact.toString());

  }

},
  error => console.log(error));



}

WorklocAdd()
{
  this.Empreldiv=false;
  this.submitted =false;
  this.employeeaddform.reset();
  this.employeeeditForm.reset();
  this.title="WorkLocation";
  this.locationdiv=false;
  
  this.addempModal.show();

}


Worklocedit(wlid:any,wlname:any,wlact:any){
  this.editsubmitted = false;
  this.Empreldiv=false;
  this.title="WorkLocation";
  this.editempModal.show();
const req = new InputRequest();
req.INPUT_01 = "WORK_LOCATION";
this.service.postData(req, "GetMasterDetails").subscribe(data => {
  if (data.StatusCode == "100") {
    this.locationdiv=false;

    this.editempModal.show();
    this.f.Godownswarehsetype.setValue(wlid);
    this.f.editemptype.setValue(wlname);
    
    this.f.empactive.setValue(wlact.toString());

  }

},
  error => console.log(error));



}


Familyrelationadd()
{
  this.submitted =false;
  this.employeeaddform.reset();
  this.employeeeditForm.reset();
  this.locationdiv=false;
  this.Empreldiv=false;
  this.title="Relation";

  this.addempModal.show();

}


Familyreledit(flid:any,flname:any,flact:any){
  this.editsubmitted = false;
  this.Empreldiv=false;
  this.title="Relation";
  this.editempModal.show();
const req = new InputRequest();
req.INPUT_01 = "RELATION";
this.service.postData(req, "GetMasterDetails").subscribe(data => {
  if (data.StatusCode == "100") {
    this.locationdiv=false;

    this.editempModal.show();
    this.f.Godownswarehsetype.setValue(flid);
    this.f.editemptype.setValue(flname);
    
    this.f.empactive.setValue(flact.toString());

  }

},
  error => console.log(error));



}
nationaladd()
{
  this.submitted =false;
  this.employeeaddform.reset();
  this.employeeeditForm.reset();
  this.locationdiv=false;
  this.Empreldiv=false;
  this.title="Nationality";

  this.addempModal.show();


}
Nationalityedit(nlid:any,nlname:any,nlact:any){
  this.editsubmitted = false;
  this.Empreldiv=false;
this.title="Nationality";
this.editempModal.show();
const req = new InputRequest();
req.INPUT_01 = "NATIONALITY";
this.service.postData(req, "GetMasterDetails").subscribe(data => {
  if (data.StatusCode == "100") {
    this.locationdiv=false;

    this.editempModal.show();
    this.f.Godownswarehsetype.setValue(nlid);
    this.f.editemptype.setValue(nlname);
    
    this.f.empactive.setValue(nlact.toString());

  }

},
  error => console.log(error));


}

Sectionadd(){
  this.submitted =false;
  this.employeeaddform.reset();
  this.employeeeditForm.reset();
  this.locationdiv=false;
  this.Empreldiv=false;
  this.title="Section";

  this.addempModal.show();


}
sectionedit(SECid:any,SECname:any,SECact:any){
  this.editsubmitted = false;
  this.Empreldiv=false;
this.title="Section";
this.editempModal.show();
const req = new InputRequest();
req.INPUT_01 = "SECTION";
this.service.postData(req, "GetMasterDetails").subscribe(data => {
  if (data.StatusCode == "100") {
    this.locationdiv=false;

    this.editempModal.show();
    this.f.Godownswarehsetype.setValue(SECid);
    this.f.editemptype.setValue(SECname);
    
    this.f.empactive.setValue(SECact.toString());

  }

},
  error => console.log(error));


}


}







