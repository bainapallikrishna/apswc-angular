import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ThemeService } from 'ng2-charts';
import { retryWhen } from 'rxjs/operators';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { Validators, Editor, Toolbar, toHTML } from "ngx-editor";
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  // editordoc = jsonDoc;

  editor: Editor;
  toolbar: Toolbar = [
    ["bold", "italic"],
    ["underline", "strike"],
    ["code", "blockquote"],
    ["ordered_list", "bullet_list"],
    [{ heading: ["h1", "h2", "h3", "h4", "h5", "h6"] }],
    ["link", "image"],
    ["text_color", "background_color"],
    ["align_left", "align_center", "align_right", "align_justify"]
  ];


  formedit = true;
  formabout: FormGroup;
  apswcabout: string;
  divapswcabout: string;

  constructor(private http: HttpClient, private service: CommonServices) {

  }


  ngOnInit(): void {


    this.editor = new Editor();



    this.formabout = new FormGroup({
      apswcabout: new FormControl("", Validators.required())

    });

    this.getcontentdata()

  }
  getcontentdata() {
    this.service.getData("GetHomePageConent").subscribe(data => {
      if (data.StatusCode == "100") {
        this.divapswcabout = data.Details[0]["abouT_APSWC"];

        if (!this.divapswcabout) {
          this.formedit = false;
        }
        else {
          this.formedit = true;
        }
      }
      else {
        Swal.fire('error', data.statusmessage, 'error');
      }
    });
  }
  apswcedit() {
    this.formedit = false;
    this.formabout.value.apswcabout = this.divapswcabout;
  }


  onSubmit() {
    if (!this.formabout.value.apswcabout) {
      Swal.fire('info', 'Please Enter the apswc about', 'info');
      return;
    }
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "UPD_ABOUT";
    req.INPUT_01 = this.formabout.value.apswcabout;
    this.service.postData(req, "HomePageConentInsert").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'info');

        this.getcontentdata();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });
  }


}
