import { Component, OnInit,ViewChild } from '@angular/core';
import{Router}from '@angular/router';
import Swal from 'sweetalert2';
import{InputRequest} from 'src/app/Interfaces/employee';
import{CommonServices} from 'src/app/Services/common.services';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormBuilder, FormGroup,PatternValidator,Validators } from '@angular/forms';
import { MustMatch } from 'src/app/custome-directives/must-match.validator';
import{EncrDecrServiceService} from 'src/app/Services/encr-decr-service'
import { HttpClient, HttpEventType } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { DSButtonRendererComponent } from 'src/app/custome-directives/DSbutton-renderer.component';
import { TmplAstRecursiveVisitor } from '@angular/compiler';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';

@Component({
  selector: 'app-mail-settings',
  templateUrl: './mail-settings.component.html',
  styleUrls: ['./mail-settings.component.css']
})
export class MailSettingsComponent implements OnInit {

  loguser: string=sessionStorage.getItem("logUserCode");
  loguserrole: string = sessionStorage.getItem("logUserrole");
  ispwdchanged:string=sessionStorage.getItem("logUserisChnagePassword");
  loguserid:string=sessionStorage.getItem("logUserworkLocationCode");

  submitted:boolean=false;
  showloader:boolean=false;
  MailDomainDD : any [];
  SendButton:boolean=false;

  constructor(private router:Router,private service:CommonServices,private fb:FormBuilder,private EncrDecr:EncrDecrServiceService) { 
    if (!this.loguserrole || this.ispwdchanged == '0') {
      this.router.navigate(['/Login']);
      return;
    }
  }

  @ViewChild('addModal') public addModal: ModalDirective;
  ngOnInit(): void {
    this.MailDomain();
    this.MailSettingsAddForm.reset();
  }

  get subf() { return this.MailSettingsAddForm.controls; }

  MailSettingsAddForm=this.fb.group({
    MailID: ['', [Validators.required, Validators.pattern(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)]],
    Password:['',[Validators.required,Validators.minLength(8),Validators.maxLength(20),Validators.pattern(/^(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{8,20})/)]],
    ConfirmPassword:['',[Validators.required,Validators.minLength(8),Validators.maxLength(20),Validators.pattern(/^(?=.*[A-Z])(?=.*[!@#\$%\^&\*])(?=.{8,20})/)]],
    MailDomain: ['', Validators.required],
    PortNumber:['', Validators.required],
  },
   {
    validator: MustMatch('Password', 'ConfirmPassword')
  });



  MailDomain(){
    this.MailDomainDD = [
    {Value:"@ap.gov.in",Name:"@ap.gov.in"},
    {Value:"@gmail.com",Name:"@gmail.com"},
    {Value:"@hotmail.com",Name:"@hotmail.com"}
  ];
  }

  ChangeMailDomain(val){
  if(val=="@ap.gov.in")
  this.subf.PortNumber.setValue("25");
  else if(val=="@gmail.com" || val=="@hotmail.com")
  this.subf.PortNumber.setValue("587");
  }

  SubmitAddDetails(){
    this.submitted=true;
    if(this.MailSettingsAddForm.invalid){
      return false;
    }
    if(this.subf.Password.value != this.subf.ConfirmPassword.value){
      Swal.fire("info","Password and Confirm Password should be same","info");
      return;
    }
    
    const req = new InputRequest();
  
  req.INPUT_01 = "301";
  req.INPUT_02=this.EncrDecr.set(this.subf.MailID.value);
  req.INPUT_03=this.EncrDecr.set(this.subf.Password.value);
  req.INPUT_04=this.subf.MailDomain.value;
  req.INPUT_05=this.subf.PortNumber.value;
  req.INPUT_06=this.loguserrole;
  req.USER_NAME=this.loguser;
  req.CALL_SOURCE="Web";

  this.service.postData(req,"HelpInsertion").subscribe(data => {
      if (data.StatusCode == 100) {
      this.showloader= false;  
      this.submitted=false;
      this.SendButton=false;
      this.MailSettingsAddForm.reset();
        Swal.fire("success", "Data Submitted Successfully", "success");
    }
    else{
      this.showloader= false;  
      this.SendButton=true;
      Swal.fire("info",data.StatusMessage,"info");
    }
  });
  }

  TestMail() {
    this.submitted=true;
    if(this.MailSettingsAddForm.invalid){
      return false;
    }
    if(this.subf.Password.value != this.subf.ConfirmPassword.value){
      Swal.fire("info","Password and Confirm Password should be same","info");
      return;
    }
    
    const req = new InputRequest();
  
    req.INPUT_02=this.EncrDecr.set(this.subf.MailID.value);
    req.INPUT_03=this.EncrDecr.set(this.subf.Password.value);
    req.INPUT_04=this.subf.MailDomain.value;
    req.INPUT_05=this.subf.PortNumber.value;
    req.INPUT_06="Test Mail Subject";
    req.INPUT_07="Test MailBody";
  

  this.service.postData(req,"TestMail").subscribe(data => {
      if (data.StatusCode == 100) {
      this.showloader= false;  
        Swal.fire("success", "Test Mail sent Successfully,Please click the save button to continue...", "success");
        this.SendButton=true;
    }
    else{
      this.showloader= false;  
      Swal.fire("info",data.StatusMessage,"info");
      this.SendButton=false;
    }
  });
  }

}
