import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-scrolling-message',
  templateUrl: './scrolling-message.component.html',
  styleUrls: ['./scrolling-message.component.css']
})
export class ScrollingMessageComponent implements OnInit {

  scrollForm: FormGroup;
  public scrollmessage: string;
  submitted = false;
  Scrollmsglist: any = [];
  formsm = true;
  constructor(private http: HttpClient, private formBuilder: FormBuilder, private service: CommonServices) { }

  ngOnInit(): void {


    this.scrollForm = this.formBuilder.group({

      scrollmessage: ['', Validators.required]

    });
    this.GetScrollmessage();
  }
  get f() { return this.scrollForm.controls; }

  GetScrollmessage() {
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "GET_SCROLL";

    this.service.postData(req, "GetScrollNewMessage").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Scrollmsglist = data.Details;
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });

  }
  AddMeessage() {
    this.formsm = false;
  }

  editmessage(msg: any) {
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "UPD_SCROLL";
    req.INPUT_01 = msg.contenT_ID;
    req.INPUT_02 = msg.contenT_TYPE;
    req.INPUT_03 = msg.contenT_TITLE;
    req.INPUT_04 = msg.contenT_BODY;
    req.INPUT_05 = msg.contenT_LINK;
    req.INPUT_06 = msg.contenT_PHOTO;
    req.INPUT_07 = "0";
    this.service.postData(req, "ScrollNewMessageInsert").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'info');
        this.formsm = true;
        this.GetScrollmessage();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });


  }

  onSubmit() {
    this.submitted = true;

    if (this.scrollForm.invalid) {
      return false;
    }

    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "INS_SCROLL";
    req.INPUT_01 = "S";
    req.INPUT_02 = "";
    req.INPUT_03 = this.scrollForm.value.scrollmessage;

    this.service.postData(req, "ScrollNewMessageInsert").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'info');

        this.GetScrollmessage();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });
    // alert('Success!! :-)\n\n'+JSON.stringify(this.scrollForm.value,null,4));


  }

  onReset() {
    this.submitted = false;
    this.scrollForm.reset();
  }
}
