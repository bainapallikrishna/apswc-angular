import { Component, ElementRef, OnInit ,ViewChild} from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { EmployeeSalaryInputRequest, InputRequest } from 'src/app/Interfaces/employee';
import { AgGridAngular } from 'ag-grid-angular';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import{MedicalAPPREJBUTTONRendererComponent} from 'src/app/custome-directives/MedicalApproveRejectbutton-renderer.component';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-medical-reimbursement-approval',
  templateUrl: './medical-reimbursement-approval.component.html',
  styleUrls: ['./medical-reimbursement-approval.component.css']
})
export class MedicalReimbursementApprovalComponent implements OnInit {
  totalmonthsinterset:any;
  type:any;
  Applicationid:any;
  historylist:any;
  Designationlist:any;
  array2=[];
  values=[];
  installmenttxtb:boolean=false;
  tokens:any;
  Loanmonth:any;
  frameworkComponents: any;
  NgbDateStruct:any;
  Loantype:any;
  frameworkComponents1: any;
  array=[];
  public components1;
  progressbar:boolean=false;
  message: string = "";
  employeecode="";
  emimonths=0;
  UploadDocType: string = "";
  UploadDocPath: string = "";
  progress: number = 0;
  submitted:boolean=false;
  showloader: boolean = false;
  gridColumnApi: any = [];
  Loantypes:any;
  gridApi: any = [];
  gridApi1: any = [];
  gridColumnApi1: any = [];
  tokens1:any;
  btntype:any;
  cl:any;
  requestcode:any;
  lastamount:any;
  istallmenttotal:any;
  empaddarray:any;
  installmentvalue=[];
  sections:any;
  Medicalid:any;
  public rowseledted: object;
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  Worklocname = sessionStorage.getItem("logUserworkLocationName");
  imagepath:any;
  empname:any;
  nameslist:any;
  public components;
  medicalreqid:any;
  Applicartionidlist:any;
  forwardeddivs:boolean=true;
  submitbutton:boolean=true;
  @ViewChild('TaxPaymentModal') TaxPaymentModal: ModalDirective;
  @ViewChild('LoanApprovalModal') public LoanApprovalModal: ModalDirective;
  @ViewChild('LoanRejectModal') public LoanRejectModal: ModalDirective;

  @ViewChild('PreviewModal') public PreviewModal: ModalDirective;
  @ViewChild('historyempModal') public historyempModal: ModalDirective;
  

  @ViewChild('GGHApprovalModal') public GGHApprovalModal: ModalDirective;

  

  @ViewChild('name') inputName:ElementRef;
  
  columnDefs = [


    { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Request Id',width: 200,field: 'medicaL_REIM_ID',onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
    },
    { headerName: 'Employee Name',width: 200, field: 'emP_NAME', sortable: true, filter: true },   
    
    { headerName: 'From Department',  width: 200,field: 'department', sortable: true, filter: true },
    
    { headerName: 'Requested Date',  width: 200,field: 'requesteD_DATE', sortable: true, filter: true },
  
    {
      headerName: 'Action',field:'status', cellRenderer: 'buttonRenderer',width:600,
      cellRendererParams: {
        editClick: this.EditEmployee.bind(this),
        historyClick:this.HistoryEmployee.bind(this),
        viewClick:this.viewEmployee.bind(this),
      },
    }
  
  ];
  EditEmployee(row) 
  {
   
this.btntype=row.bttype;
const data = row.rowData;
this.Medicalid= data.medicaL_REIM_ID;
this.requestcode=data.reQ_CODE;
this.LoanApprovalModal.show();

this.LoanApprovalModal.show();
this.historyempModal.hide();
this.PreviewModal.hide();
this.PreviewModal.hide(); 
}

HistoryEmployee(row):void{
 
 this.type="history";
 this.PreviewModal.hide(); 
  this.showloader = true;
  const data = row.rowData;
  const req = new InputRequest();
  this.medicalreqid = data.medicaL_REIM_ID;


  this.showloader = true;
req.TYPEID="EMP_APPROVAL_HISTORY";
 req.INPUT_01=this.medicalreqid;

  this.service.postData(req, "GetFamilynames").subscribe(data => {
    this.showloader = false;
    if (data.StatusCode == "100") {
      this.historyempModal.show();
      this.type="";
      this.historylist = data.Details;
      this.PreviewModal.hide();
      return false;
    }
    else {
      this.historylist="";
      this.type="";
      this.showloader = false;
      Swal.fire("info","No History Found","info");
      this.PreviewModal.hide();
      return false;
    }
  },
    error => console.log(error));
}

viewEmployee(row):void{


    this.type="View";
    this.PreviewModal.hide(); 
     this.showloader = true;
     const data = row.rowData;
    
     this.medicalreqid = data.medicaL_REIM_ID;
     this.historyempModal.hide();
     this.GGHApprovalModal.show();
     const req=new EmployeeSalaryInputRequest();
     req.TYPEID="GGH_DOC_UPLOAD_GET";
    req.INPUT_01=data.medicaL_REIM_ID;
  this.approvaldocupload(req.TYPEID, req.INPUT_01);
  
 
 }



  constructor(private formBuilder: FormBuilder, private service: CommonServices, private router: Router,private datef: CustomDateParserFormatter) {
if(this.Worklocname=="P & A Section")
{

  this.submitbutton=true;

}
else{
  this.submitbutton=false;

}
   // this.imagepath="wwwroot\\MedicalReimbursementRequest\\13-06-2021\\download_13_06_2021_02_56_11.jpg";
   }

  ngOnInit(): void {

    if (!this.logUserrole || this.isPasswordChanged == "0") {

      this.router.navigate(['/Login']);
      return;
    }


    this.components={

      rowIdRenderer:function(params){
    
       return ''+(params.rowIndex+1);
      }
    }
    this.frameworkComponents = {

      buttonRenderer: MedicalAPPREJBUTTONRendererComponent,
      
    }
    
    this.LoadSections();

  }

  approverejectclick()
  {
    this.LoanApprovalModal.show();
  }
Previewclick()
{
this.PreviewModal.show();
}





  LoadSections() {
    this.service.getData("GetSections").subscribe(data => {

      if (data.StatusCode == "100") {
        this.sections = data.Details;

      }

    },
      error => console.log(error));
  }
  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.LoadmedicalData();
  }
 

  LoadmedicalData() 
  {
    this.showloader = true;
    let obj: any = { "INPUT_02": this.Worklocname ,TYPEID:"EMP_APPROVAL"}

    this.service.postData(obj, "GetFamilynames").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {
        Swal.fire("info",data.StatusMessage,"info");
        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

        
      }
    },
      error => console.log(error));
  }
  
  downloadPdf(base64String, fileName) {
    const source = base64String;//`data:application/pdf;base64,${base64String}`;
    const link = document.createElement("a");
    link.href = source;
    link.download = `${fileName}.pdf`
    link.click();
  }
  


  decryptFile(imagepth:string) {
 if (imagepth) {
   //this.pdf="wwwroot\\MedicalReimbursementRequest\\14-06-2021\\sample_14_06_2021_10_49_59.pdf";
      this.service.decryptFile(imagepth, "DecryptFile")
        .subscribe(data => {
          var ext =  imagepth.split('.').pop();
          var filename = imagepth.replace(/^.*[\\\/]/, '');
   
   if(ext=="pdf")
  
   {

    this.downloadPdf(data.docBase64,filename);
    // this.viewPDF(data.docBase64);
    }
    else{
 var newTab = window.open();
  newTab.document.body.innerHTML ='<img src='+data.docBase64+' width="100%" height="100%">'; 
    
    }
    
        },
          error => {
            console.log(error);
          });
    }
    else{

    }
    }


    viewPDF(input): void {
      var pdfResult = input;
                    
      let pdfWindow = window.open("")
      pdfWindow.document.write("<iframe width='100%' height='100%' src='data:application/pdf;base64, " + encodeURI(pdfResult) + "'></iframe>")  }


    RejectApproval(){

    }
    base64sstringpath:any;empcode:any;emptype:any;emprelation;familymemname:any;distname:any;disname:any;treatamount:any;treattype:any;fromdept:any;
    
    

makeCellClicked(event) 
    {
  this.rowseledted={};    
        
        this.rowseledted = event.data;
        this.empname=event.node.data.emP_NAME;
        this.empcode=event.node.data.emP_CODE;
      
        this.emptype=event.node.data.emP_TYPE;
        this.emprelation=event.node.data.familY_RELATION;
      
        this.familymemname=event.node.data.familY_NAME;
        this.distname=event.node.data.districT_NAME;
      
        this.disname=event.node.data.diseasE_NAME;
        this.treatamount=event.node.data.treatmenT_AMOUNT;
      
        this.treattype=event.node.data.treatmenT_TYPE;
        this.fromdept=event.node.data.applicatioN_FORWARD_FROM;

         this.MedicalPreviewform.controls.certificate1.setValue(event.node.data.applicatioN_MEDICAL_DOCUMENT);
         this.MedicalPreviewform.controls.certificate2.setValue(event.node.data.certificatE_OP_DOCUMENT);
         this.MedicalPreviewform.controls.certificate3.setValue(event.node.data.noN_DRAWAL_DOCUMENT);
         this.MedicalPreviewform.controls.certificate4.setValue(event.node.data.declaratioN_CERTIFICATE_DOCUMENT);
         this.MedicalPreviewform.controls.certificate5.setValue(event.node.data.emergencY_ADMISSION_DOCUMENT);
         this.MedicalPreviewform.controls.certificate6.setValue(event.node.data.essentialitY_CERTIFICATE_DOCUMENT);
         this.MedicalPreviewform.controls.certificate7.setValue(event.node.data.checK_LIST_DOCUMENT);
         this.PreviewModal.show();
        }
    
  
    certificates(val)
    {
      this.base64sstringpath="";
      if(val=="1")
      {
        this.base64sstringpath=this.MedicalPreviewform.controls.certificate1.value;
      }

      if(val=="2")
      {
        this.base64sstringpath=this.MedicalPreviewform.controls.certificate2.value;
      }
      if(val=="3")
      {
        this.base64sstringpath=this.MedicalPreviewform.controls.certificate3.value;
      }
      if(val=="4")
      {
        this.base64sstringpath=this.MedicalPreviewform.controls.certificate4.value;
      }
      if(val=="5")
      {
        this.base64sstringpath=this.MedicalPreviewform.controls.certificate5.value;
      }
      if(val=="6")
      {
        this.base64sstringpath=this.MedicalPreviewform.controls.certificate6.value;
      }
      if(val=="7")
      {
        this.base64sstringpath=this.MedicalPreviewform.controls.certificate7.value;
      }

      this.decryptFile(this.base64sstringpath);    
    }


      
      MedicalRejectform=this.formBuilder.group({
    
        offrejremarks:[''],
        imagep:['']
        });
        MedicalApprovalform=this.formBuilder.group({
    
          SectionName:[''],
          offremarks:[''],
          Approverej:[''],
          OffName:[''],
          DesigName:['']
      
      
        });
    
MedicalPreviewform=this.formBuilder.group({
certificate1:[''],
certificate2:[''],
certificate3:[''],
certificate4:[''],
certificate5:[''],
certificate6:[''],
certificate7:[''],

      });



      submit()
      {

        this.showloader=true;
 if(this.MedicalApprovalform.controls.Approverej.value==""||this.MedicalApprovalform.controls.Approverej.value==null||this.MedicalApprovalform.controls.Approverej.value==undefined){
        Swal.fire("info","Please Select Approve/Reject","info");
        this.showloader=false;
        return;
           }

if(this.MedicalApprovalform.controls.Approverej.value=="1")
           {


            if(this.MedicalApprovalform.controls.SectionName.value==""||this.MedicalApprovalform.controls.SectionName.value==null||this.MedicalApprovalform.controls.SectionName.value==undefined){
              Swal.fire("info","Please select Section Name","info");
              this.showloader=false;
              return;
                 }
              
              
                 if(this.MedicalApprovalform.controls.offremarks.value==""||this.MedicalApprovalform.controls.offremarks.value==null||this.MedicalApprovalform.controls.offremarks.value==undefined){
                  Swal.fire("info","Please Enter Remarks","info");
                  this.showloader=false;
                  return;
                     }
              
              
                         if(this.MedicalApprovalform.controls.OffName.value==""||this.MedicalApprovalform.controls.OffName.value==null||this.MedicalApprovalform.controls.OffName.value==undefined){
                          Swal.fire("info","Please Select OfficerName","info");
                          this.showloader=false;
                          return;
                             }
                             
                     if(this.MedicalApprovalform.controls.DesigName.value==""||this.MedicalApprovalform.controls.DesigName.value==null||this.MedicalApprovalform.controls.DesigName.value==undefined){
                      Swal.fire("info","Please Select Designation","info");
                      this.showloader=false;
                      return;
                         }

                      
              
               }
    
if(this.MedicalApprovalform.controls.Approverej.value=="0"){


                if(this.MedicalApprovalform.controls.offremarks.value==""||this.MedicalApprovalform.controls.offremarks.value==null||this.MedicalApprovalform.controls.offremarks.value==undefined){
                  Swal.fire("info","Please Enter Remarks","info");
                  this.showloader=false;
                  return;
                     }
              

               }
  

       
          if(this.MedicalApprovalform.controls.Approverej.value=="1"){
           const req=new EmployeeSalaryInputRequest();
   
        this.showloader=true;
        req.TYPEID="206";
       req.INPUT_01=this.Medicalid;
       req.INPUT_02=this.MedicalApprovalform.controls.SectionName.value;
       req.INPUT_04=this.MedicalApprovalform.controls.offremarks.value;
       req.INPUT_05=this.MedicalApprovalform.controls.OffName.value;
       req.INPUT_06=this.MedicalApprovalform.controls.DesigName.value;
       req.USER_NAME=this.logUsercode;
      req.CALL_SOURCE="WEB";
     
    this.service.postData(req, "SaveMedivcalDetails").subscribe(data => {
      this.empaddarray=data;
      if (this.empaddarray.StatusCode == "100") 
      {

         this.showloader=false;
        this.LoanApprovalModal.hide();
        Swal.fire("success", "Request Forwarded to "+this.empaddarray.Details[0].officeR_NAME, "success");
        //this.addModal.hide(); 
        this.reloadCurrentRoute();
        }
        else{
            this.showloader= false;
            Swal.fire("info",data.StatusMessage,"info");
          } 
 
        },
        error => console.log(error));
        }
        if(this.MedicalApprovalform.controls.Approverej.value=="0"){
        const req=new EmployeeSalaryInputRequest();
   
        this.showloader=true;
        req.TYPEID="207";
       req.INPUT_01=this.Medicalid;
       req.INPUT_02=this.Worklocname;
       req.INPUT_03=this.MedicalApprovalform.controls.offremarks.value;
       req.USER_NAME=this.logUsercode;
       req.CALL_SOURCE="WEB";
     
    this.service.postData(req, "UpdateMedicalDetails").subscribe(data => {
      this.empaddarray=data;
      if (this.empaddarray.StatusCode == "100") 
      {
        
        this.showloader=false;
    
        this.LoanApprovalModal.hide();
        Swal.fire("success","Rejected Successfully","success");
      
     this.reloadCurrentRoute();
        }
        else{
            this.showloader= false;
            Swal.fire("info",data.StatusMessage,"info");
          
          } 
 
        },
        error => console.log(error));
        }
      }
       CloseEditModal()
       {
         this.MedicalApprovalform.reset();
         this.MedicalRejectform.reset();
          this.LoanApprovalModal.hide();
          this.LoanRejectModal.hide();
        
         this.progressbar=false;
     this.reloadCurrentRoute();
       }
      
       reloadCurrentRoute() {
         let currentUrl = this.router.url;
         this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
             this.router.navigate([currentUrl]);
         });
       }
       onSelectionChanged(event) {    
        var rowCount = event.api.getSelectedNodes().length;
    }   

get f(){return this.MedicalRejectform.controls
}
       
sectionchange(){

  this.Designationlist=[];
  this.MedicalApprovalform.controls.DesigName.setValue('');
  this.MedicalApprovalform.controls.OffName.setValue('');
  this.getdesignations();


 
//this.nameslist();

}

hidehistorymasterModal(){
  this.reloadCurrentRoute();
}

// getnames()
// {

//   this.showloader = true;
//   const req=new InputRequest();
//   req.TYPEID="DD_OFFICER_NAME";
//   req.INPUT_01=this.MedicalApprovalform.controls.SectionName.value;
//   this.service.postData(req, "GetFamilynames").subscribe(data => {
//     this.showloader = false;
//     if (data.StatusCode == "100") {
  
//       this.nameslist = data.Details;
//           }
//     else {
//       this.showloader = false;
//       Swal.fire("info","No data Found" ,"info");
      
//     }
//   },
//     error => console.log(error));

// }



getdesignations()
{

  this.showloader = true;
  const req=new InputRequest();
req.TYPEID="DD_DESIGNATION";
 req.INPUT_01=this.MedicalApprovalform.controls.SectionName.value;
  this.service.postData(req, "GetFamilynames").subscribe(data => {
    this.showloader = false;
    if (data.StatusCode == "100") {

      this.Designationlist = data.Details;
          }
    else {
      this.showloader = false;
      Swal.fire("info",data.StatusMessage,"info");
      this.Designationlist="";
    }
  },
    error => console.log(error));

}
treatname:any;hosname:any;




Getapplicationiddetails(){
 
}




GGHApprovalform=this.formBuilder.group({
  SactAmnt:[''],
  ApprovalDocpath:[''],
  ApprovalRegDoc:['']

});
ApprovaluploadFile(event)
{
  
  if (event.target.files && event.target.files[0]) {
    let filetype = event.target.files[0].type;
    let filesize = event.target.files[0].size;
    let doctype = "";
    if (event.target.files.length === 0) {
      return false;
    }
    if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
      Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
      return false;
    }

    if (filetype != 'image/jpeg' || filetype != 'image/png')
      doctype = 'IMAGE';
    else
      doctype = 'PDF';

    if (filesize > 2615705) {
      Swal.fire('info', 'File size must be upto 2 MB', 'info');
      this.progressbar=false;
      return false;
    }

    let fileToUpload = <File>event.target.files[0];
    const formData = new FormData();

    formData.append('file', fileToUpload, fileToUpload.name );
    formData.append('pagename', "MedicalReimbursementApproval");

    this.service.encryptUploadFile(formData, "EncryptFileUpload")
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
        this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
    
          this.message = 'Upload success.'
          this.ceruploadFinished(event.body);
          //this.uploadFinished(this.regsdoc.tostring());
          this.progressbar=true;
        }

      });
  }
}


public ceruploadFinished = (event) => 
  {
    
      this.GGHApprovalform.controls.ApprovalDocpath.setValue(event.fullPath);

    
  }



  SubmitApprovalDetails()
  {

    if(this.GGHApprovalform.controls.SactAmnt.value==""||this.GGHApprovalform.controls.SactAmnt.value==null||this.GGHApprovalform.controls.SactAmnt.value==undefined){

      Swal.fire("info","Please Enter Sanctioned Amount","info");
      return false;
         }
      
      if(this.GGHApprovalform.controls.ApprovalDocpath.value==""||this.GGHApprovalform.controls.ApprovalDocpath.value==null||this.GGHApprovalform.controls.ApprovalDocpath.value==undefined){
      
        Swal.fire("info","Please Upload Approval Document","info");
        return false;
       }
    if(this.treattype=="GGH")
    {
if(parseFloat(this.GGHApprovalform.controls.SactAmnt.value)>50000){
  Swal.fire("info","Please Enter Valid amount","info");
  this.GGHApprovalform.controls.SactAmnt.setValue('');
  return false;
}
if(parseFloat(this.GGHApprovalform.controls.SactAmnt.value)>parseFloat(this.treatamount)){
  Swal.fire("info","Please Enter Valid amount","info");
  this.GGHApprovalform.controls.SactAmnt.setValue('');
  return false;
}

}
if(this.treattype=="AROGYA SRI")
{
if(parseFloat(this.GGHApprovalform.controls.SactAmnt.value)<50000)
{
Swal.fire("info","Please Enter Valid amount","info");
this.GGHApprovalform.controls.SactAmnt.setValue('');
return false;
}
if(parseFloat(this.GGHApprovalform.controls.SactAmnt.value)>parseFloat(this.treatamount)){
  Swal.fire("info","Please Enter Valid amount","info");
  this.GGHApprovalform.controls.SactAmnt.setValue('');
  return false;
}



}
   // this.GGHApprovalModal.show();


  const req=new EmployeeSalaryInputRequest();
  this.showloader=true;
  req.TYPEID="208";
 req.INPUT_01=this.medicalreqid;
 req.INPUT_02=this.GGHApprovalform.controls.SactAmnt.value;
 req.INPUT_03= this.GGHApprovalform.controls.ApprovalDocpath.value
 req.USER_NAME=this.logUsercode;
 req.INPUT_04=this.Worklocname;
 req.CALL_SOURCE="WEB";
this.service.postData(req, "SaveApprovalDetails").subscribe(data => {
this.empaddarray=data;
if (this.empaddarray.StatusCode == "100") 
{

  this.showloader=false;``
  this.GGHApprovalModal.hide();
  Swal.fire("success","Details Submitted Successfully","success");
  this.reloadCurrentRoute();
  }
  else{
      this.showloader= false;
      Swal.fire("info",data.StatusMessage,"info");
      this.reloadCurrentRoute();
    } 
},

error => console.log(error)
);

}



approverejectfun(apprejval){
  if(apprejval=="1"){

    this.sections=[];
    this.Designationlist=[];
    this.MedicalApprovalform.controls.SectionName.setValue('');
    this.MedicalApprovalform.controls.DesigName.setValue('');
    this.MedicalApprovalform.controls.OffName.setValue('');
    this.LoadSections(); 
    this.forwardeddivs=true;

  }
  else{

    this.sections=[];
    this.Designationlist=[];
    this.MedicalApprovalform.controls.SectionName.setValue('');
    this.MedicalApprovalform.controls.DesigName.setValue('');
    this.MedicalApprovalform.controls.OffName.setValue('');
    this.LoadSections(); 
    this.forwardeddivs=false;

  }
}
uploadedby:any;sctamount:any;emprel:any;dist:any;
deptstatus:any;


approvaldocupload(type,mdid)
{

this.showloader = true;
  const req=new InputRequest();
  req.INPUT_01=mdid;
req.TYPEID="GGH_DOC_UPLOAD_GET";

  this.service.postData(req, "GetFamilynames").subscribe(data => {


    this.showloader = false;
    if (data.StatusCode == "100") {
this.GGHApprovalModal.show();
this.PreviewModal.hide();

      this.Applicartionidlist = data.Details;

this.dist=this.Applicartionidlist[0].emP_NAME;
      this.empname= this.Applicartionidlist[0].emP_NAME;
this.emprel=this.Applicartionidlist[0].familY_RELATION;
      this.empcode=this.Applicartionidlist[0].emP_CODE;
      this.familymemname=this.Applicartionidlist[0].familY_NAME;
      this.treatamount=this.Applicartionidlist[0].treatmenT_AMOUNT;
    this.treattype=this.Applicartionidlist[0].treatmenT_TYPE;
    this.treatname=this.Applicartionidlist[0].treatmenT_NAME;
    this.hosname=this.Applicartionidlist[0].hospitaL_NAME;
    this.sctamount=this.Applicartionidlist[0].approveD_AMOUNT;
    this.uploadedby=this.Applicartionidlist[0].ggH_UPLOAD_DEPARTMENT;
this.deptstatus=this.Applicartionidlist[0].statuS_DEPT;
    if(this.Worklocname=="P & A Section"){
 
      if(this.Applicartionidlist[0].status=="1"||this.Applicartionidlist[0].status=="2"){
        this.GGHApprovalModal.show();
        this.PreviewModal.hide();
         this.GGHApprovalform.controls['SactAmnt'].disable();
         this.GGHApprovalform.controls['ApprovalRegDoc'].disable();
         this.submitbutton=false;
         return false;
      
      }
      else{
        this.showloader=false;
          this.PreviewModal.hide();
          this.submitbutton=true;
          this.GGHApprovalform.controls['SactAmnt'].enable();
          this.GGHApprovalform.controls['ApprovalRegDoc'].enable();
      return false;
      }
    
    }
  else{

    this.submitbutton=false;
    this.showloader=false;
    this.PreviewModal.hide();
    return false;

  }
    }
    else {
      this.showloader = false;
      Swal.fire("info","Error While Load (GGH/Aarogyasri) Approval Details","info");
      //this.GGHApprovalModal.hide(); 
      this.Applicartionidlist="";
      this.PreviewModal.hide();
      return false;
      
    }
  },
    error => console.log(error));


}

UPLOADEDSTUS:any;uploadeddept:any;Approvedamt:any;
reqtype:any;mid:any;
getapprovalstatus(medicalid)
{
  this.PreviewModal.hide();
this.showloader = true;
  const req=new EmployeeSalaryInputRequest();
  req.INPUT_01=medicalid;
  req.TYPEID="ALREADY_UPLOAD_MSG";
  this.service.postData(req, "GetFamilynames").subscribe(data => {
    this.showloader = false;
    if (data.StatusCode == "100") {
      this.UPLOADEDSTUS = data.Details;
if(this.UPLOADEDSTUS[0].status=="1"){
  this.GGHApprovalModal.show();
  this.PreviewModal.hide();
  
   this.uploadeddept=this.UPLOADEDSTUS[0].uploadeD_DEPARTMENT;
   this.GGHApprovalform.controls['SactAmnt'].disable();
   this.GGHApprovalform.controls['ApprovalRegDoc'].disable();
   this.Approvedamt=this.UPLOADEDSTUS[0].approveD_AMOUNT;
   this.submitbutton=true;
}
    
else{
  this.showloader=false;
    this.PreviewModal.hide();
    this.submitbutton=false;
return false;

}



}   
    
    else {
      this.submitbutton=true;
      this.showloader = false;
      this.Applicartionidlist="";
      this.PreviewModal.hide();
    }
  },
    error => console.log(error));


}


    }
