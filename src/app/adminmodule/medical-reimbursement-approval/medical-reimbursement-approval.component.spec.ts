import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MedicalReimbursementApprovalComponent } from './medical-reimbursement-approval.component';

describe('MedicalReimbursementApprovalComponent', () => {
  let component: MedicalReimbursementApprovalComponent;
  let fixture: ComponentFixture<MedicalReimbursementApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MedicalReimbursementApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MedicalReimbursementApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
