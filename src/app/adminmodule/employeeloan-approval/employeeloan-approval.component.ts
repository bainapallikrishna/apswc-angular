import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { EmployeeSalaryInputRequest, InputRequest } from 'src/app/Interfaces/employee';
import { AgGridAngular } from 'ag-grid-angular';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { OFFICERAPPREJBUTTONRendererComponent } from 'src/app/custome-directives/OfficerApproveRejectbutton-renderer.component';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
@Component({
  selector: 'app-employeeloan-approval',
  templateUrl: './employeeloan-approval.component.html',
  styleUrls: ['./employeeloan-approval.component.css']
})
export class EmployeeloanApprovalComponent implements OnInit {
  totalmonthsinterset: any;
  type: any;
  submitbutton:boolean=false;
  array2 = [];
  values = [];
  installmenttxtb: boolean = false;
  tokens: any;
  Loanmonth: any;
  frameworkComponents: any;
  NgbDateStruct: any;
  forwardeddivs:boolean=true;
  Loantype: any;
  frameworkComponents1: any;
  array = [];
  public components1;
  progressbar: boolean = false;
  message: string = "";
  employeecode = "";
  emimonths = 0;
  UploadDocType: string = "";
  UploadDocPath: string = "";
  progress: number = 0;
  submitted: boolean = false;
  showloader: boolean = false;
  gridColumnApi: any = [];
  Loantypes: any;
  gridApi: any = [];
  gridApi1: any = [];
  gridColumnApi1: any = [];
  tokens1: any;
  btntype: any;
  cl: any;
  requestcode: any;
  lastamount: any;
  istallmenttotal: any;
  empaddarray: any;
  installmentvalue = [];
  install:boolean=false;
  public rowseledted: object;
  logUserrole: string = sessionStorage.getItem("logUserrole");
  Worklocname = sessionStorage.getItem("logUserworkLocationName");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  public components;
  @ViewChild('TaxPaymentModal') TaxPaymentModal: ModalDirective;
  @ViewChild('LoanApprovalModal') public LoanApprovalModal: ModalDirective;
  @ViewChild('LoanRejectModal') public LoanRejectModal: ModalDirective;
  @ViewChild('mrPreviewModal') public mrPreviewModal: ModalDirective;
  @ViewChild('historyempModal') public historyempModal: ModalDirective;
  
  @ViewChild('DsApprovalModal') public DsApprovalModal: ModalDirective;
  
  @ViewChild('name') inputName: ElementRef;
  columnDefs = [
    { headerName: '#', cellRenderer: 'rowIdRenderer' },
    {
      headerName: 'Request Id',width: 200,field: 'reQ_CODE',onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>'
      }},
    
    { headerName: 'Employee Name', maxWidth: 250, field: 'emP_NAME', sortable: true, filter: true },
    { headerName: 'From Department', maxWidth: 250, field: 'department', sortable: true, filter: true },
    { headerName: 'Requested Date', maxWidth: 250, field: 'loaN_REQ_DATE', sortable: true, filter: true },
    {
      headerName: 'Action',field: 'status', cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        editClick: this.EditEmployee.bind(this),
        historyClick: this.History.bind(this),
      },
    }

  ];
  approvalstatus:any;Uploadby:any;Sactamt:any;empcode:any;Loantpe:any;reqamount:any;month:any;Status:any;remarks:any;
  bankholname:any;accno:any;ifsc:any;bankbranch:any;bankname:any;reqid:any;
  
  
  LoanApprovalpreviewForm=this.formBuilder.group({
    photo:[''],
    checkbx: [''],
    instlno: [''],
    offloanamount: [''],
    Rateofinterest: [''],
    offmonths: [''],
    offremarks: [''],
    totalamtwtinterest: ['']

  });

  base64sstringpath:any;

  hidepreviewTaxModal(){
    this.mrPreviewModal.hide();
    this.LoanApprovalpreviewForm.reset();
  }
  requestid:any;historylist:any;
  History(ROW):void{
  
    this.requestid=ROW.rowData.reQ_CODE;
     this.showloader = true;
    // const data = row.rowData;
     const req = new InputRequest();
 this.showloader = true;
    req.INPUT_01=this.requestid;
    req.TYPEID ="EMP_LOAN_REQ_HISTORY";
     this.service.postData(req, "GetFamilyrelation").subscribe(data => {
       this.showloader = false;
       if (data.StatusCode == "100") {
         this.historyempModal.show();
        this.historylist = data.Details;
        }
       else {
         this.historylist="";
         this.showloader = false;
         Swal.fire("info","No History Found","info");
       }
     },
       error => console.log(error));
   }
  certificates()
  {
 
  this.base64sstringpath=this.LoanApprovalpreviewForm.controls.photo.value;
  
 
  this.decryptFile(this.base64sstringpath);
    }
    decryptFile(imagepth:string) {
      if (imagepth) {
        //this.pdf="wwwroot\\MedicalReimbursementRequest\\14-06-2021\\sample_14_06_2021_10_49_59.pdf";
           this.service.decryptFile(imagepth, "DecryptFile")
             .subscribe(data => {
               var ext =  imagepth.split('.').pop();
               var filename = imagepth.replace(/^.*[\\\/]/, '');
                     if(ext=="pdf")
       
        {
          this.downloadPdf(data.docBase64,filename);
         }
         else{
      var newTab = window.open();
       newTab.document.body.innerHTML ='<img src='+data.docBase64+' width="100%" height="100%">'; 
         
         }
         
             },
               error => {
                 console.log(error);
               });
         }
         else{
     
         }
         }
    
         downloadPdf(base64String, fileName) {
          const source = base64String;//`data:application/pdf;base64,${base64String}`;
          const link = document.createElement("a");
          link.href = source;
          link.download = `${fileName}.pdf`
          link.click();
        }


        sectionchange(){
          this.Designationlist=[];
          this.LoanapprejApprovalform.controls.dsDesigName.setValue('');

          this.LoanapprejApprovalform.controls.dsOffName.setValue('');
          this.getdesignations();
          //this.nameslist();
          
          }


  SubmitApprovalDetails()
  {
    this.installmentvalue = [];

    if (this.LoanApprovalpreviewForm.controls.offloanamount.value == "" || this.LoanApprovalpreviewForm.controls.offloanamount.value == null || this.LoanApprovalpreviewForm.controls.offloanamount.value == undefined) {
      Swal.fire("info", "Please Enter Loan amount", "info");
      this.showloader = false;
      return false;
    }
    if (this.LoanApprovalpreviewForm.controls.Rateofinterest.value == "" || this.LoanApprovalpreviewForm.controls.Rateofinterest.value == null || this.LoanApprovalpreviewForm.controls.Rateofinterest.value == undefined) {
      Swal.fire("info", "Please Enter Rateof Interest", "info");
      this.showloader = false;
      return false;
    }

    const req = new EmployeeSalaryInputRequest();
    var istallmenttotal2 = 0;
    if (this.type == "checkboxchecked") {

      if (this.LoanApprovalpreviewForm.controls.instlno.value == "" || this.LoanApprovalpreviewForm.controls.instlno.value == null || this.LoanApprovalpreviewForm.controls.instlno.value == undefined) {
        Swal.fire("info", "Please Enter No of Installments", "info");
        this.showloader = false;
        return false;
      }

      var index = 0;
      for (var k = 0; k < this.array.length; k++) {
        index = index + 1;
        this.installmentvalue.push(this.array[k].id);
        this.array2.push(this.array[k].value);
        var install = parseFloat(this.array[k].value);
        istallmenttotal2 = istallmenttotal2 + install;
      }
      if (parseFloat(this.LoanApprovalpreviewForm.controls.offloanamount.value) == istallmenttotal2) {

      }

      else {
        Swal.fire("info", "Please Enter Valid installment amount", "info");
        this.showloader = false;
        this.values = [];
        this.array = [];
        this.installmentvalue = [];
        var index1 = this.LoanApprovalpreviewForm.controls.instlno.value;
        this.array2 = [];
        for (var i = 1; i <= index1; i++) {
          this.values.push({ value: "" });
        }

        istallmenttotal2 = 0;
        return false;
      }
      req.INPUT_08 = this.installmentvalue.join();
      req.INPUT_09 = this.array2.join();
    }
    req.INPUT_01 = this.employeecode;
    req.INPUT_02 = this.reqid;
    req.INPUT_03 = this.LoanApprovalpreviewForm.controls.offloanamount.value;
    req.INPUT_04 = this.LoanApprovalpreviewForm.controls.offmonths.value;
    req.INPUT_05 = this.LoanApprovalpreviewForm.controls.Rateofinterest.value;
    req.INPUT_06 = this.totalmonthsinterset;
    req.INPUT_07 = this.LoanApprovalpreviewForm.controls.totalamtwtinterest.value;
    req.INPUT_11 = this.LoanApprovalpreviewForm.controls.offremarks.value;
    req.USER_NAME = this.logUsercode;
    req.INPUT_12 = this.logUserrole;
    req.INPUT_15=this.Worklocname;
    req.CALL_SOURCE = "WEB";
    //  alert(req.INPUT_08);
    //  alert(req.INPUT_09);
    //  this.reloadCurrentRoute();
    this.service.postData(req, "SaveemployeeofficerloanDetails").subscribe(data => {
      this.empaddarray = data;
      if (this.empaddarray.StatusCode == "100") {

        this.showloader = false;

        this.LoanApprovalModal.hide();
        Swal.fire("success", "Details Submitted Successfully", "success");
    
        this.reloadCurrentRoute();
      }
      else {
        this.showloader = false;
        Swal.fire("info", this.empaddarray.StatusMessage, "info");
        this.reloadCurrentRoute();
      }
    },

      error => console.log(error)
    );

  }

  CloseEditModal(){

  }

  statname:any;basesl:any;netpy:any;installtype:any;
  installmen=[];amt:any;totalamount:any;
  makeCellClicked(event) 
  {
    
  var installmentamount=[]; //for approval doc sec

  this.approvalstatus=event.node.data.loaN_STATUS_NUM;
  this.Uploadby=event.node.data.section;
  this.rowseledted={};
  this.totalamount=event.node.data.totaL_INSTALLMENT_AMT
  this.Sactamt=event.node.data.apR_AMOUNT;

  
  this.basesl=event.node.data.basic;
  this.netpy=event.node.data.neT_PAY;
  this.statname=event.node.data.loaN_STATUS;
  this.installtype=event.node.data.installmenT_TYPE
  this.rowseledted = event.data;
  this.empcode=event.node.data.emP_CODE;
  this.Loantpe=event.node.data.loaN_TYPE_NAME;
  this.reqamount =event.node.data.reQ_AMOUNT;
  this.month=event.node.data.months;
  this.LoanApprovalpreviewForm.controls.offmonths.setValue( this.month);
  this.Status=event.node.data.loaN_STATUS;
  this.remarks=event.node.data.remarks;
  this.LoanApprovalpreviewForm.controls.photo.setValue(event.node.data.emP_DOC);
  
  this.bankholname=event.node.data.banK_HOLDER_NAME;
  this.accno=event.node.data.accounT_NO_MASK;
  this.ifsc=event.node.data.ifsC_CODE;
  this.bankbranch=event.node.data.brancH_NAME;
  this.bankname=event.node.data.banK_NAME;
  this.reqid=event.node.data.reQ_CODE;


       if(this.Worklocname=="P & A Section"){
 
      if(this.approvalstatus==1||this.approvalstatus==2)
      {
        this.mrPreviewModal.show();
       
        this.LoanApprovalpreviewForm.controls['Rateofinterest'].disable();
        this.LoanApprovalpreviewForm.controls['checkbx'].disable();
     
        this.LoanApprovalpreviewForm.controls['offloanamount'].disable();
  var myInstance:string[]=this.Sactamt.split(',');
  if(myInstance.length>1)
  {
    this.installmen=[];
    for(var i=0;i<=myInstance.length-1;i++){
      var obj={amt:myInstance[i]}
      this.installmen.push(obj);
     }
     this.install=true;
  }
  
    else{
      this.install=false;
      this.totalamount=event.node.data.totaL_INSTALLMENT_AMT
     }
     
       this.submitbutton=false;
         return false;
       }
      else{
       
          this.mrPreviewModal.show();
          this.submitbutton=true;
          this.LoanApprovalpreviewForm.controls.offloanamount.setValue(this.reqamount);
          this.submitbutton=true;
      return false;
      }
    
    }
  else{
    this.submitbutton=false;
 
    this.showloader=false;
    this.mrPreviewModal.show();
    return false;

  }
    
    
  }
  // columnDefs1 = [
  //   { headerName: '#', cellRenderer: 'rowIdRenderer' },
  //   { headerName: 'Employee Code', maxWidth: 250, field: 'emP_CODE', sortable: true, filter: true },
  //   { headerName: 'Employee Name', maxWidth: 250, field: 'emP_NAME', sortable: true, filter: true },
  //   { headerName: 'Employee Type', maxWidth: 250, field: 'emP_TYPE_NAME', sortable: true, filter: true },
  //   { headerName: 'Loan Type', maxWidth: 250, field: 'loaN_TYPE_NAME', sortable: true, filter: true },
  //   { headerName: 'Requested Amount', maxWidth: 250, field: 'reQ_AMOUNT', sortable: true, filter: true },
  //   { headerName: 'Months', maxWidth: 250, field: 'months', sortable: true, filter: true },
  //   { headerName: 'Basic Salary', maxWidth: 250, field: 'basic', sortable: true, filter: true },
  //   { headerName: 'Net Pay', maxWidth: 250, field: 'neT_PAY', sortable: true, filter: true },
  //   { headerName: 'Sanctioned Amount', maxWidth: 250, field: 'reQ_AMOUNT', sortable: true, filter: true },
  //   { headerName: 'No of Installments', maxWidth: 250, field: 'installmenT_TYPE', sortable: true, filter: true },
  //   { headerName: 'Installment Amount', maxWidth: 250, field: 'installmenT_AMOUNT', sortable: true, filter: true },
  // ];

  EditEmployee(row) {

    this.btntype = row.bttype;
    const data = row.rowData;
    this.employeecode = data.emP_CODE;
    this.emimonths = data.months;
    this.requestcode = data.reQ_CODE;
    this.DsApprovalModal.show();

  }

  constructor(private formBuilder: FormBuilder, private service: CommonServices, private router: Router, private datef: CustomDateParserFormatter) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {

      this.router.navigate(['/Login']);
      return;
    }


    this.components = {

      rowIdRenderer: function (params) {

        return '' + (params.rowIndex + 1);
      }
    }
    this.frameworkComponents = {

      buttonRenderer: OFFICERAPPREJBUTTONRendererComponent,

    }
    this.components1 = {

      rowIdRenderer: function (params) {

        return '' + (params.rowIndex + 1);
      }
    }
this.LoadSections();
  }

  ngOnInit(): void {
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.LoadofficerapprovalData();
  }

  // BindData1(params) {
  //   this.gridApi1 = params.api;
  //   this.gridColumnApi1 = params.columnApi;
  //   this.LoadPAYMENTapprovalData();
  // }

  LoanapprejApprovalform=this.formBuilder.group({
    dsApproverej:[''],
    dsSectionName:[''],
    dsDesigName:[''],
    dsOffName:[''],
    dsoffremarks:['']

  });

  LoadPAYMENTapprovalData() {
    this.showloader = true;
    const req = new EmployeeSalaryInputRequest();
    // let obj: any = { "INPUT_01": this.logUsercode };

    this.service.postData(req, "GetPaymentApprovalData").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens1 = data.Details;
        this.gridApi1.setRowData(this.tokens1);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi1.setRowData(this.tokens1);

      }
    },
      error => console.log(error));

  }

 
  LoadofficerapprovalData() {
    this.showloader = true;
    const req = new EmployeeSalaryInputRequest();
    let obj: any = { "INPUT_02": this.Worklocname };

    this.service.postData(obj, "GetOfficerApprovalData").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));

  }
  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
  }

  onSelectionChanged1(event) {
    var rowCount = event.api.getSelectedNodes().length;
  }

  interestratecalculation() {
    if (this.LoanApprovalpreviewForm.controls.Rateofinterest.value == "" || this.LoanApprovalpreviewForm.controls.Rateofinterest.value == null || this.LoanApprovalpreviewForm.controls.Rateofinterest.value == undefined) {

      Swal.fire("info", "Please Enter interest Amount", "info");
      this.showloader = false;
      return false;
    }
    else {
      var intp = 12;
      var intestper = parseFloat(this.LoanApprovalpreviewForm.controls.Rateofinterest.value) / intp;
      var value = parseFloat(this.LoanApprovalpreviewForm.controls.offloanamount.value) * intestper;
      var month = value / 100;
      var totalamountinterest = month * this.month;
      this.totalmonthsinterset = totalamountinterest;
      this.LoanApprovalpreviewForm.controls.totalamtwtinterest.setValue(parseFloat(this.LoanApprovalpreviewForm.controls.offloanamount.value) + totalamountinterest);
      this.lastamount = this.LoanApprovalpreviewForm.controls.totalamtwtinterest.value;
    }
  }
  loanamountchnage() 
  {
if(parseFloat(this.LoanApprovalpreviewForm.controls.offloanamount.value)>parseFloat(this.totalamount))
{
  Swal.fire("info", "You have entered morethan Requested amount,Please enter Valid Amount", "info");
  this.LoanApprovalpreviewForm.controls.offloanamount.setValue('');
  this.showloader = false;
  return false;

}

  
    else {
      if (this.LoanApprovalpreviewForm.controls.Rateofinterest.value == "" || this.LoanApprovalpreviewForm.controls.Rateofinterest.value == null || this.LoanApprovalpreviewForm.controls.Rateofinterest.value == undefined) {

        Swal.fire("info", "Please Enter Interest Rate", "info");
        this.showloader = false;
        return false;
      }

      var intp = 12;
      var intestper = parseFloat(this.LoanApprovalpreviewForm.controls.Rateofinterest.value) / intp;
      var value = parseFloat(this.LoanApprovalpreviewForm.controls.offloanamount.value) * intestper;
      var month = value / 100;
      var totalamountinterest = month * this.month;
      this.totalmonthsinterset = totalamountinterest;
      this.LoanApprovalpreviewForm.controls.totalamtwtinterest.setValue(parseFloat(this.LoanApprovalpreviewForm.controls.offloanamount.value) + totalamountinterest);
      this.lastamount = this.LoanApprovalpreviewForm.controls.totalamtwtinterest.value;
    }

  }

  textboxchange(event, index) {
    var obj = { id: index, value: event }
    //alert(index);
    if (this.array.length > 0) {
      for (var gk = 1; gk <= this.array.length; gk++) {
        var arrindex = gk;
        if (arrindex == index) {
          this.array = this.array.filter(m => m.id != obj.id);
          this.array.push(obj);
          return;
        }
        else {
          this.array.push(obj);

        }
      }

    }
    else {

      this.array.push(obj);

    }


  }

  addFieldValue() {

  }


  Rejectsubmit() {


    const req = new EmployeeSalaryInputRequest();
    this.showloader = true;

    if (this.officerRejectform.controls.offrejremarks.value == "" || this.officerRejectform.controls.offrejremarks.value == null || this.officerRejectform.controls.offrejremarks.value == undefined) {
      Swal.fire("info", "Please Enter Remarks For Rejection", "info");
      this.showloader = false;
      return false;
    }
    req.INPUT_01 = this.employeecode;
    req.INPUT_02 = this.requestcode;
    req.INPUT_10 = this.btntype;
    req.INPUT_11 = this.officerRejectform.controls.offrejremarks.value;
    req.USER_NAME = this.logUsercode;
    req.INPUT_12 = this.logUserrole;
    req.CALL_SOURCE = "WEB";
    this.service.postData(req, "SaveemployeeofficerloanDetails").subscribe(data => {
      this.empaddarray = data;
      // this.empaddarray=data.result;
      if (this.empaddarray.StatusCode == "100") {

        this.showloader = false;

        this.LoanRejectModal.hide();
        Swal.fire("success", "Rejected Successfully", "success");
        this.LoadofficerapprovalData();
        this.reloadCurrentRoute();
      }
      else {
        this.showloader = false;
        Swal.fire("info", this.empaddarray.StatusMessage, "info");
        this.reloadCurrentRoute();
      }
    },

      error => console.log(error));

  }

  officerRejectform = this.formBuilder.group({

    offrejremarks: [''],
  });
  officerApprovalform = this.formBuilder.group({

    checkbx: [''],
    instlno: [''],
    offloanamount: [''],
    Rateofinterest: [''],
    offmonths: [''],
    offremarks: [''],
    totalamtwtinterest: ['']
  });


  submit()
  {

    this.showloader=true;
      if(this.LoanapprejApprovalform.controls.dsApproverej.value==""||this.LoanapprejApprovalform.controls.dsApproverej.value==null||this.LoanapprejApprovalform.controls.dsApproverej.value==undefined){
    Swal.fire("info","Please Select Approve/Reject","info");
    this.showloader=false;
    return;
       }
if(this.LoanapprejApprovalform.controls.dsApproverej.value=="1")
       {
if(this.LoanapprejApprovalform.controls.dsSectionName.value==""||this.LoanapprejApprovalform.controls.dsSectionName.value==null||this.LoanapprejApprovalform.controls.dsSectionName.value==undefined){
          Swal.fire("info","Please select Section Name","info");
          this.showloader=false;
          return;
             }
if(this.LoanapprejApprovalform.controls.dsoffremarks.value==""||this.LoanapprejApprovalform.controls.dsoffremarks.value==null||this.LoanapprejApprovalform.controls.dsoffremarks.value==undefined){
              Swal.fire("info","Please Enter Remarks","info");
              this.showloader=false;
              return;
                 }
          
          
if(this.LoanapprejApprovalform.controls.dsOffName.value==""||this.LoanapprejApprovalform.controls.dsOffName.value==null||this.LoanapprejApprovalform.controls.dsOffName.value==undefined){
                      Swal.fire("info","Please Select OfficerName","info");
                      this.showloader=false;
                      return;
                         }
                         
if(this.LoanapprejApprovalform.controls.dsDesigName.value==""||this.LoanapprejApprovalform.controls.dsDesigName.value==null||this.LoanapprejApprovalform.controls.dsDesigName.value==undefined){
                  Swal.fire("info","Please Select Designation","info");
                  this.showloader=false;
                  return;
                     }
                    }

if(this.LoanapprejApprovalform.controls.dsApproverej.value=="0")
{
              if(this.LoanapprejApprovalform.controls.dsoffremarks.value==""||this.LoanapprejApprovalform.controls.dsoffremarks.value==null||this.LoanapprejApprovalform.controls.dsoffremarks.value==undefined){
              Swal.fire("info","Please Enter Remarks","info");
              this.showloader=false;
              return;
                 }
  }if(this.LoanapprejApprovalform.controls.dsApproverej.value=="1"){
       const req=new EmployeeSalaryInputRequest();

    this.showloader=true;
 
    req.TYPEID="202";
   req.INPUT_01=this.LoanapprejApprovalform.controls.dsOffName.value;
   req.INPUT_02=this.requestcode;
   req.INPUT_03=this.LoanapprejApprovalform.controls.dsSectionName.value;
   req.INPUT_04=this.LoanapprejApprovalform.controls.dsoffremarks.value;
   req.INPUT_05=this.LoanapprejApprovalform.controls.dsDesigName.value;
   req.USER_NAME=this.logUsercode;
  req.CALL_SOURCE="WEB";
 
this.service.postData(req, "SaveemployeeloanApproveDetails").subscribe(data => {
  this.empaddarray=data;
  if (this.empaddarray.StatusCode == "100") 
  {
    
    this.showloader=false;

    this.DsApprovalModal.hide();
    Swal.fire("success", "Request Forwarded to "+this.empaddarray.Details[0].officeR_NAME, "success");
    //this.addModal.hide();
  
 this.reloadCurrentRoute();
    }
    else{
        this.showloader= false;
        Swal.fire("info",data.StatusMessage,"info");
      } 

    },
    error => console.log(error));
    }
    if(this.LoanapprejApprovalform.controls.dsApproverej.value=="0"){
    const req=new EmployeeSalaryInputRequest();

    this.showloader=true;
    req.TYPEID="203";
    req.INPUT_02=this.requestcode;
   req.INPUT_05=this.Worklocname;
   req.INPUT_04=this.LoanapprejApprovalform.controls.dsoffremarks.value;
   req.USER_NAME=this.logUsercode;
   req.CALL_SOURCE="WEB";
 
this.service.postData(req, "updateemployeeloanApproveDetails").subscribe(data => {
  this.empaddarray=data;
  if (this.empaddarray.StatusCode == "100") 
  {
    
    this.showloader=false;

    this.DsApprovalModal.hide();
    Swal.fire("success","Rejected Successfully","success");
  
 this.reloadCurrentRoute();
    }
    else{
        this.showloader= false;
        Swal.fire("info",data.StatusMessage,"info");
      
      } 

    },
    error => console.log(error));
    

  }
  }



  checkboxfun(event) {

    if (this.LoanApprovalpreviewForm.controls.offloanamount.value == "" || this.LoanApprovalpreviewForm.controls.offloanamount.value == null || this.LoanApprovalpreviewForm.controls.offloanamount.value == undefined) {
      Swal.fire("info", "Please Enter Loan amount", "info");
      this.LoanApprovalpreviewForm.controls.checkbx.setValue('');
      return false;
    }
    if (this.LoanApprovalpreviewForm.controls.Rateofinterest.value == "" || this.LoanApprovalpreviewForm.controls.Rateofinterest.value == null || this.LoanApprovalpreviewForm.controls.Rateofinterest.value == undefined) {
      Swal.fire("info", "Please Enter Rateof Interest", "info");
      this.LoanApprovalpreviewForm.controls.checkbx.setValue('');
      return false;
    }

    var checkvalue = this.LoanApprovalpreviewForm.controls.checkbx.value;
    if (checkvalue == true) {
      this.installmenttxtb = true;
      this.type = "checkboxchecked";
      this.LoanApprovalpreviewForm.controls['offloanamount'].disable();
      //disable
    }
    else {
      this.LoanApprovalpreviewForm.controls['offloanamount'].enable();
      this.type = "";
      this.installmenttxtb = false;
      // this.officerApprovalform.controls.checkbx.setValue('');
      this.LoanApprovalpreviewForm.controls.instlno.setValue('');
      this.values = [];
      this.array = [];
      this.array2 = [];
    }

  }


  Installmentclick() {

    if (this.LoanApprovalpreviewForm.controls.instlno.value == "" || this.LoanApprovalpreviewForm.controls.instlno.value == null || this.LoanApprovalpreviewForm.controls.instlno.value == undefined) {
      Swal.fire("info", "Please Enter No of Installments", "info");
      this.showloader = false;
      return false;
    }

    this.values = [];
    this.array = [];
    var index = this.LoanApprovalpreviewForm.controls.instlno.value;
    for (var i = 1; i <= index; i++) {
      this.values.push({ value: "" });
    }

  }

  installmentchange(event) {
    this.values = [];
    this.array = [];
    var index = this.LoanApprovalpreviewForm.controls.instlno.value;
    for (var i = 1; i <= index; i++) {
      this.values.push({ value: "" });
    }
  }




  LoadSections() {
    this.service.getData("GetSections").subscribe(data => {

      if (data.StatusCode == "100") {
        this.sections = data.Details;

      }

    },
      error => console.log(error));
  }
  Designationlist:any;sections:any;
getdesignations()
{

this.showloader = true;
const req=new InputRequest();
req.TYPEID="DD_DESIGNATION";
req.INPUT_01=this.LoanapprejApprovalform.controls.dsSectionName.value;
this.service.postData(req, "GetFamilynames").subscribe(data => {
  this.showloader = false;
  if (data.StatusCode == "100") {

    this.Designationlist = data.Details;
        }
  else {
    this.showloader = false;
    Swal.fire("info",data.StatusMessage,"info");
    this.Designationlist="";
  }
},
  error => console.log(error));

}

  approverejectfun(apprejval){
    if(apprejval=="1"){
  
      this.sections=[];
      this.Designationlist=[];
      this.LoanapprejApprovalform.controls.dsSectionName.setValue('');
      this.LoanapprejApprovalform.controls.dsDesigName.setValue('');
      this.LoanapprejApprovalform.controls.dsOffName.setValue('');
      this.LoadSections(); 
      this.forwardeddivs=true;
  
    }
    else{
  
      this.sections=[];
      this.Designationlist=[];
      this.LoanapprejApprovalform.controls.dsSectionName.setValue('');
      this.LoanapprejApprovalform.controls.dsDesigName.setValue('');
      this.LoanapprejApprovalform.controls.dsOffName.setValue('');
      this.LoadSections(); 
      this.forwardeddivs=false;
  
    }
  }

  CloseDsApprovalModal() {

    this.LoanapprejApprovalform.reset();
    this.DsApprovalModal.hide();
    this.progressbar = false;
    this.reloadCurrentRoute();
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
  hidehistorymasterModal()
  {
this.historyempModal.hide();
  }
}
