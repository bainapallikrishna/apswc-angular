import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeloanApprovalComponent } from './employeeloan-approval.component';

describe('EmployeeloanApprovalComponent', () => {
  let component: EmployeeloanApprovalComponent;
  let fixture: ComponentFixture<EmployeeloanApprovalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeloanApprovalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeloanApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
