import { stringify } from '@angular/compiler/src/util';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { AgGridAngular } from 'ag-grid-angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { ConditionalRenderer } from 'src/app/custome-directives/conditional-renderer.component';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { InputRequest } from 'src/app/Interfaces/employee';
import { AadharValidateService } from 'src/app/Services/aadhar-validate.service';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-space-reservation',
  templateUrl: './space-reservation.component.html',
  styleUrls: ['./space-reservation.component.css']
})
export class SpaceReservationComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('AddSection') public AddSection: ModalDirective;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('apprModal') apprModal: ModalDirective;

  showloader: boolean = true;
  gridApi: any;
  gridColumnApi: any;
  public defaultColDef;
  frameworkComponents: any;
  public components;
  public icons;
  Userslist: any = [];
  NgbDateStruct: any;
  startdate: NgbDateStruct;
  reservsubmi: boolean = false;
  SpaceReservForm: FormGroup;
  SpaceReservPrevForm: FormGroup;
  Commoditylist: any = [];
  Commodityrate_list: any = [];
  rebatelist: any = [];
  Commoditygrouplist: any = [];
  invModelist: any = [];
  contractlist: any = [];
  RUserType: string = "";
  RUserCode: string = "";
  RUserName: string = "";
  RUserTypeName: string = "";
  public rowseledted: object;
  vcommogroup: string = "";

  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer' },
    {
      headerName: 'Name', width: 200, field: 'name', onCellClicked: this.makeCellClicked.bind(this), cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      }
    },
    { headerName: 'Depositor Type', maxWidth: 200, field: 'registratioN_TYPE_NAME' },
    { headerName: 'User Email', maxWidth: 200, field: 'emaiL_ID' },
    { headerName: 'Phone Number', maxWidth: 150, field: 'mobilE_NUMBER' },
    { headerName: 'Address', width: 300, field: 'address' },

  ];

  constructor(private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer,
    private datef: CustomDateParserFormatter,
    private uidservice: AadharValidateService) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.frameworkComponents = {
      // buttonRenderer: GBbuttonRendererComponent,
      customRenderer: ConditionalRenderer,
    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

    this.icons = {
      filter: ' '
    }
  }

  ngOnInit(): void {
    let now: Date = new Date();
    this.NgbDateStruct = { day: now.getDate(), month: now.getMonth() + 1, year: now.getFullYear() }

    this.SpaceReservForm = this.formBuilder.group({
      commodities: new FormArray([
        this.formBuilder.group({
          commoditygroup: [null, Validators.required],
          commodity: [null, Validators.required],
          InvoiceMode: [null, Validators.required],
          startdate: [null, Validators.required],
          enddate: [null, Validators.required],
          SPrice: [null, Validators.required],
          InvoiceBasedOn: [null, Validators.required],
          storagetype: [null, Validators.required],
          isinsurance: [null, Validators.required],
          InsuranceCollection: [''],
          istax: [null, Validators.required],
          CGST: [''],
          SGST: [''],
          isrebate: [null, Validators.required],
          Rebate: [null],
          isexamption: [null, Validators.required],
          OverAboveExamption: [''],
          isquality: [null, Validators.required],
          issupervision: [null, Validators.required],
          SupervisionCharges: [''],
          issepinsinvoice: [null, Validators.required],
          isstorageloss: [null, Validators.required],
          NoOfDays: [''],
          AcceptableLoss: [''],
          numberofbags: [''],
          BagWeight: [''],
          TotalWeight: [''],
          TotalArea: [''],
        })
      ])

    })

    this.SpaceReservPrevForm = this.formBuilder.group({
      commodities: new FormArray([
        this.formBuilder.group({
          commoditygroup: [''],
          commodity: [''],
          InvoiceMode: [''],
          startdate: [''],
          enddate: [''],
          SPrice: [''],
          InvoiceBasedOn: [''],
          storagetype: [''],
          isinsurance: [null],
          InsuranceCollection: [''],
          istax: [null],
          CGST: [''],
          SGST: [''],
          isrebate: [null],
          Rebate: [null],
          isexamption: [null],
          OverAboveExamption: [''],
          isquality: [null],
          issupervision: [null],
          SupervisionCharges: [''],
          issepinsinvoice: [null],
          isstorageloss: [null],
          NoOfDays: [''],
          AcceptableLoss: [''],
          numberofbags: [''],
          BagWeight: [''],
          TotalWeight: [''],
          TotalArea: [''],
        })
      ])

    })

    this.loaduserlist();
    this.loadComiditydetails();
    this.LoadInvoiceMode();
    this.loadContractlist();

    this.showloader = false;

  }

  get subf() { return this.SpaceReservForm.controls; }
  get f() { return this.subf.commodities as FormArray; }

  get prevsubf() { return this.SpaceReservPrevForm.controls; }
  get prevf() { return this.prevsubf.commodities as FormArray; }



  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //params.api.setRowData(this.Userslist);
    //this.gridApi.sizeColumnsToFit();
  }

  loaduserlist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "COMPLETED";
    this.service.postData(req, "GetExistingUser_Details").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.Userslist = data.Details;
        this.gridApi.setRowData(this.Userslist);
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
    },

      error => console.log(error));

  }

  AddSpace() {

  }


  makeCellClicked(event) {

    if (event.node.selected == true) {
      this.reservsubmi = false;
      this.rowseledted = {};
      this.rowseledted = event.node.data;
      this.f.clear();
      this.f.reset();
      this.AddSpaceReservation();
      this.RUserType = event.node.data.registratioN_TYPE;
      this.RUserCode = event.node.data.registratioN_ID;
      this.RUserName = event.node.data.name;
      this.RUserTypeName = event.node.data.registratioN_TYPE_NAME;
      this.AddSection.show();

    }



  }


  AddSpaceReservation() {
    this.f.push(this.formBuilder.group({
      commoditygroup: [null, Validators.required],
      commodity: [null, Validators.required],
      InvoiceMode: [null, Validators.required],
      startdate: [null, Validators.required],
      enddate: [null, Validators.required],
      SPrice: [null, Validators.required],
      InvoiceBasedOn: [null],
      storagetype: [null, Validators.required],
      isinsurance: [null, Validators.required],
      InsuranceCollection: [''],
      istax: [null, Validators.required],
      CGST: [''],
      SGST: [''],
      isrebate: [null, Validators.required],
      Rebate: [null],
      isexamption: [null, Validators.required],
      OverAboveExamption: null,
      isquality: [null, Validators.required],
      issupervision: [null, Validators.required],
      SupervisionCharges: null,
      issepinsinvoice: [null, Validators.required],
      isstorageloss: [null, Validators.required],
      NoOfDays: [''],
      AcceptableLoss: [''],
      numberofbags: [''],
      BagWeight: [''],
      TotalWeight: [''],
      TotalArea: [''],
    }));
  }

  AddSpaceReservPrev() {
    this.prevf.push(this.formBuilder.group({
      commoditygroup: [''],
      commodity: [''],
      InvoiceMode: [''],
      startdate: [''],
      enddate: [''],
      SPrice: [''],
      InvoiceBasedOn: [''],
      storagetype: [''],
      isinsurance: [null],
      InsuranceCollection: [''],
      istax: [null],
      CGST: [''],
      SGST: [''],
      isrebate: [null],
      Rebate: [''],
      isexamption: [null],
      OverAboveExamption: [''],
      isquality: [null],
      issupervision: [null],
      SupervisionCharges: [''],
      issepinsinvoice: [null],
      isstorageloss: [null],
      NoOfDays: [''],
      AcceptableLoss: [''],
      numberofbags: [''],
      BagWeight: [''],
      TotalWeight: [''],
      TotalArea: [''],
    }));
  }

  RemoveSpaceReservation(index) {
    this.f.removeAt(index);
  }

  ShowPreview() {
    this.reservsubmi = true;

    // stop here if form is invalid
    if (this.SpaceReservForm.invalid) {
      return false;
    }

    for (let i = 0; i < this.f.length; i++) {
      if (this.datef.GreaterDate(this.f.value[i].startdate, this.f.value[i].enddate)) {
        Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
        return false;
      }

      if (this.f.value[i].isinsurance == "1" && !this.f.value[i].InsuranceCollection) {
        Swal.fire('warning', "Please Enter Insurance Collecion %", 'warning');
        return false;
      }

      if (this.f.value[i].istax == "1" && !this.f.value[i].CGST) {
        Swal.fire('warning', "Please Enter CGST TAX %", 'warning');
        return false;
      }

      if (this.f.value[i].istax == "1" && !this.f.value[i].SGST) {
        Swal.fire('warning', "Please Enter SGST TAX %", 'warning');
        return false;
      }

      if (this.f.value[i].isrebate == "1" && !this.f.value[i].Rebate) {
        Swal.fire('warning', "Please Select Rebate %", 'warning');
        return false;
      }

      if (this.f.value[i].isexamption == "1" && !this.f.value[i].OverAboveExamption) {
        Swal.fire('warning', "Please Enter Over & Above Exemption %", 'warning');
        return false;
      }

      if (this.f.value[i].issupervision == "1" && !this.f.value[i].SupervisionCharges) {
        Swal.fire('warning', "Please Enter Supervision Charges %", 'warning');
        return false;
      }

      if (this.f.value[i].isstorageloss == "1" && !this.f.value[i].NoOfDays) {
        Swal.fire('warning', "Please Enter No Of Days %", 'warning');
        return false;
      }

      if (this.f.value[i].isstorageloss == "1" && !this.f.value[i].AcceptableLoss) {
        Swal.fire('warning', "Please Enter Acceptable Loss %", 'warning');
        return false;
      }
    }

    this.FillPreviewForm();
  }

  FillPreviewForm() {

    this.prevf.clear();
    this.prevf.reset();

    for (let i = 0; i < this.f.length; i++) {
      this.AddSpaceReservPrev();

      this.prevf.controls[i].patchValue({
        commoditygroup: this.f.value[i].commoditygroup?.split(":")[1],
        commodity: this.f.value[i].commodity?.split(":")[2],
        InvoiceMode: this.f.value[i].InvoiceMode.split(":")[1],
        startdate: this.datef.format(this.f.value[i].startdate),
        enddate: this.datef.format(this.f.value[i].enddate),
        SPrice: this.f.value[i].SPrice?.split(":")[1],
        InvoiceBasedOn: null,
        storagetype: this.f.value[i].storagetype?.split(":")[1],
        isinsurance: (this.f.value[i].isinsurance == "1" ? "Yes" : "No"),
        InsuranceCollection: this.f.value[i].InsuranceCollection,
        istax: (this.f.value[i].istax == "1" ? "Yes" : "No"),
        CGST: this.f.value[i].CGST,
        SGST: this.f.value[i].SGST,
        isrebate: (this.f.value[i].isrebate == "1" ? "Yes" : "No"),
        Rebate: this.f.value[i].Rebate,
        isexamption: (this.f.value[i].isexamption == "1" ? "Yes" : "No"),
        OverAboveExamption: this.f.value[i].OverAboveExamption,
        isquality: (this.f.value[i].isquality == "1" ? "Yes" : "No"),
        issupervision: (this.f.value[i].issupervision == "1" ? "Yes" : "No"),
        SupervisionCharges: this.f.value[i].SupervisionCharges,
        issepinsinvoice: (this.f.value[i].issepinsinvoice == "1" ? "Yes" : "No"),
        isstorageloss: (this.f.value[i].isstorageloss == "1" ? "Yes" : "No"),
        NoOfDays: this.f.value[i].NoOfDays,
        AcceptableLoss: this.f.value[i].AcceptableLoss,
        numberofbags: this.f.value[i].numberofbags,
        BagWeight: this.f.value[i].BagWeight,
        TotalWeight: this.f.value[i].TotalWeight,
        TotalArea: this.f.value[i].TotalArea,
      });
    }

    this.viewModal.show();
  }

  SaveSpaceReservation() {

    //   Swal.fire({
    //     title: "alert!",
    //     text: "Are you sure you want to close?",
    //     icon: "warning",
    //     buttons: true,
    //     dangerMode: true,
    // }).then((willDelete) => {
    //     if (willDelete) {
    //         window.location.reload();

    //     } else {
    //         swal("ok");
    //     }
    // });

    if (confirm("Are you sure want to save Space Request\Reservation Details")) {
      this.showloader = true;

      let cmdgrpcodelist: string[] = [];
      let cmdgrplist: string[] = [];
      let cmdtycodelist: string[] = [];
      let cmdtylist: string[] = [];
      let packlist: string[] = [];
      let netweiglist: string[] = [];
      let ratetypelist: string[] = [];
      let invmodelist: string[] = [];
      let startdatelist: string[] = [];
      let enddatelist: string[] = [];
      let pricelist: string[] = [];
      let invbaselist: string[] = [];
      let inscolllist: string[] = [];
      let cgstlist: string[] = [];
      let sgstlist: string[] = [];
      let rebatelist: string[] = [];
      let bagslist: string[] = [];
      let bagweilist: string[] = [];
      let quntitylist: string[] = [];
      let contralist: string[] = [];
      let isinslist: string[] = [];
      let istaxlist: string[] = [];
      let isrebatelist: string[] = [];
      let isexamlist: string[] = [];
      let examplist: string[] = [];
      let isqualist: string[] = [];
      let issupervision: string[] = [];
      let supvischarges: string[] = [];
      let issepinsinvoice: string[] = [];
      let isstorageloss: string[] = [];
      let NoOfDays: string[] = [];
      let AcceptableLoss: string[] = [];

      for (let i = 0; i < this.f.length; i++) {
        if (this.f.value[i].InvoiceMode.split(":")[0] == '1' && !this.f.value[i].TotalWeight) {
          Swal.fire('warning', "Total Weight(MT's) is Requird", 'warning');
          return false;
        }
        cmdgrpcodelist.push(this.f.value[i].commoditygroup.split(":")[0]);
        cmdgrplist.push(this.f.value[i].commoditygroup.split(":")[1]);
        cmdtycodelist.push(this.f.value[i].commodity.split(":")[1]);
        cmdtylist.push(this.f.value[i].commodity.split(":")[2]);
        packlist.push(this.f.value[i].commodity.split(":")[3]);
        netweiglist.push(this.f.value[i].commodity.split(":")[4]);
        ratetypelist.push(this.f.value[i].SPrice.split(":")[0]);
        invmodelist.push(this.f.value[i].InvoiceMode.split(":")[0]);
        startdatelist.push(this.datef.format(this.f.value[i].startdate));
        enddatelist.push(this.datef.format(this.f.value[i].enddate));
        pricelist.push(this.f.value[i].SPrice.split(":")[1]);
        invbaselist.push(this.f.value[i].InvoiceBasedOn);
        inscolllist.push(this.f.value[i].InsuranceCollection);
        cgstlist.push(this.f.value[i].CGST);
        sgstlist.push(this.f.value[i].SGST);
        rebatelist.push(this.f.value[i].Rebate);
        bagslist.push(this.f.value[i].numberofbags);
        bagweilist.push(this.f.value[i].BagWeight);
        quntitylist.push(this.f.value[i].TotalWeight ?? this.f.value[i].TotalArea);
        contralist.push(this.f.value[i].storagetype.split(":")[0]);
        isinslist.push(this.f.value[i].isinsurance);
        istaxlist.push(this.f.value[i].istax);
        isrebatelist.push(this.f.value[i].isrebate);
        isexamlist.push(this.f.value[i].isexamption);
        examplist.push(this.f.value[i].OverAboveExamption);
        isqualist.push(this.f.value[i].isquality);
        issupervision.push(this.f.value[i].issupervision);
        supvischarges.push(this.f.value[i].SupervisionCharges);
        issepinsinvoice.push(this.f.value[i].issepinsinvoice);
        isstorageloss.push(this.f.value[i].isstorageloss);
        NoOfDays.push(this.f.value[i].NoOfDays);
        AcceptableLoss.push(this.f.value[i].AcceptableLoss);
      }

      const req = new InputRequest();
      req.INPUT_01 = cmdgrpcodelist.join(',');
      req.INPUT_02 = cmdgrplist.join(',');
      req.INPUT_03 = cmdtylist.join(',');
      req.INPUT_04 = packlist.join(',');
      req.INPUT_05 = netweiglist.join(',');
      req.INPUT_06 = ratetypelist.join(',');
      req.INPUT_07 = pricelist.join(',');
      req.INPUT_08 = startdatelist.join(',');
      req.INPUT_09 = enddatelist.join(',');
      req.INPUT_10 = bagslist.join(',');
      req.INPUT_11 = this.RUserCode;
      req.INPUT_12 = this.workLocationCode;//whid
      req.INPUT_13 = contralist.join(',');
      req.INPUT_14 = cmdtycodelist.join(',');
      req.INPUT_15 = invmodelist.join(',');
      req.INPUT_16 = invbaselist.join(',');
      req.INPUT_17 = isinslist.join(',');
      req.INPUT_18 = inscolllist.join(',');
      req.INPUT_19 = istaxlist.join(',');
      req.INPUT_20 = cgstlist.join(',');
      req.INPUT_21 = sgstlist.join(',');
      req.INPUT_22 = isrebatelist.join(',');
      req.INPUT_23 = rebatelist.join(',');
      req.INPUT_24 = pricelist.join(',');
      req.INPUT_25 = bagweilist.join(',');
      req.INPUT_26 = quntitylist.join(',');
      req.INPUT_27 = isexamlist.join(',');
      req.INPUT_28 = examplist.join(',');
      req.INPUT_29 = isqualist.join(',');
      req.INPUT_30 = issupervision.join(',');
      req.INPUT_31 = supvischarges.join(',');
      req.INPUT_32 = issepinsinvoice.join(',');
      req.INPUT_33 = isstorageloss.join(',');
      req.INPUT_34 = NoOfDays.join(',');
      req.INPUT_35 = AcceptableLoss.join(',');

      req.USER_NAME = this.logUsercode;
      req.CALL_SOURCE = "Web";

      this.service.postData(req, "SaveSpaceReservation").subscribe(data => {
        this.showloader = false;

        if (data.StatusCode == "100") {
          let result = data.Details[0];
          if (result.rtN_ID === 1) {
            Swal.fire('success', "Depositor " + this.RUserCode + " , Space Request\Reservation Details Saved Successfully !!!", 'success');
            this.AddSection.hide();
            this.viewModal.hide();
          }
          else
            Swal.fire('warning', result.statuS_TEXT, 'warning');

        }
        else
          Swal.fire('warning', data.StatusMessage, 'warning');

      },
        error => console.log(error));
    }
  }

  loadComiditydetails() {
    this.showloader = true;
    const req = new InputRequest();

    this.service.postData(req, "GetCommodityGroup_Details").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Commoditygrouplist = data.Details;
      }
    },
      error => console.log(error));
    this.showloader = false;

  }

  LoadCommodity(i) {

    this.Commoditylist = [];
    this.Commodityrate_list = [];
    this.rebatelist = [];
    this.f.controls[i].patchValue({
      commodity: null,
      InvoiceMode: null,
      startdate: null,
      enddate: null,
      SPrice: null,
      InvoiceBasedOn: null,
      storagetype: null,
      isinsurance: null,
      InsuranceCollection: null,
      istax: null,
      CGST: null,
      SGST: null,
      isrebate: null,
      Rebate: null,
      isexamption: null,
      OverAboveExamption: null,
      isquality: null,
      issupervision: null,
      SupervisionCharges: null,
      issepinsinvoice: null,
      isstorageloss: null,
      NoOfDays: null,
      AcceptableLoss: null,
      numberofbags: null,
      BagWeight: null,
      TotalWeight: null,
      TotalArea: null,
    });

    if (this.f.value[i].commoditygroup) {
      this.showloader = true;
      const req = new InputRequest();
      req.INPUT_01 = this.f.value[i].commoditygroup.split(":")[0];
      req.INPUT_02 = "COMMODITY";
      this.service.postData(req, "GetCommodityGroup_Details").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.Commoditylist = data.Details;
        }
      },

        error => console.log(error));
      this.showloader = false;

    }
  }

  LoadRebate(i) {

    this.Commodityrate_list = [];
    this.rebatelist = [];
    this.f.controls[i].patchValue({
      InvoiceMode: null,
      startdate: null,
      enddate: null,
      SPrice: null,
      InvoiceBasedOn: null,
      storagetype: null,
      isinsurance: null,
      InsuranceCollection: null,
      istax: null,
      CGST: null,
      SGST: null,
      isrebate: null,
      Rebate: null,
      isexamption: null,
      OverAboveExamption: null,
      isquality: null,
      issupervision: null,
      SupervisionCharges: null,
      issepinsinvoice: null,
      isstorageloss: null,
      NoOfDays: null,
      AcceptableLoss: null,
      numberofbags: null,
      BagWeight: null,
      TotalWeight: null,
      TotalArea: null,

    });


    if (this.f.value[i].commodity) {
      this.showloader = true;
      const req = new InputRequest();
      req.INPUT_01 = this.f.value[i].commoditygroup.split(":")[0];
      req.INPUT_02 = this.f.value[i].commodity.split(":")[1];
      req.INPUT_03 = this.RUserType;
      this.service.postData(req, "GetRebateDetails").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.rebatelist = data.Details;
        }
      },
        error => console.log(error));
      this.showloader = false;
    }

  }

  LoadCommodity_Rate(i) {

    this.f.controls[i].patchValue({
      enddate: null,
      SPrice: null,
      storagetype: null,
      isinsurance: null,
      InsuranceCollection: null,
      istax: null,
      CGST: null,
      SGST: null,
      isrebate: null,
      Rebate: null,
      isexamption: null,
      OverAboveExamption: null,
      isquality: null,
      issupervision: null,
      SupervisionCharges: null,
      issepinsinvoice: null,
      isstorageloss: null,
      NoOfDays: null,
      AcceptableLoss: null,
      numberofbags: null,
      BagWeight: null,
      TotalWeight: null,
      TotalArea: null,
    });

    if (this.f.value[i].commodity && this.f.value[i].startdate) {
      this.Commodityrate_list = [];
      this.showloader = true;
      const req = new InputRequest();
      req.INPUT_01 = this.f.value[i].commodity.split(":")[0];
      req.INPUT_02 = "RATE";
      req.INPUT_03 = this.datef.format(this.f.value[i].startdate);
      this.service.postData(req, "GetCommodityGroup_Details").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.Commodityrate_list = data.Details;
        }
      },
        error => console.log(error));
      this.showloader = false;
    }

  }

  ChnageInvMode(i) {
    this.f.controls[i].patchValue({
      storagetype: null,
      isinsurance: null,
      InsuranceCollection: null,
      istax: null,
      CGST: null,
      SGST: null,
      isrebate: null,
      Rebate: null,
      isexamption: null,
      OverAboveExamption: null,
      isquality: null,
      issupervision: null,
      SupervisionCharges: null,
      issepinsinvoice: null,
      isstorageloss: null,
      NoOfDays: null,
      AcceptableLoss: null,
      numberofbags: null,
      BagWeight: null,
      TotalWeight: null,
      TotalArea: null,
    });
  }

  loadContractlist() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "CONTRACT_TYPE"
    this.service.postData(req, "GetQuantity_contract_Details").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.contractlist = data.Details;

      }
      else
        console.log(data);

    },

      error => console.log(error));
    this.showloader = false;



  }

  LoadInvoiceMode() {
    this.showloader = true;
    const req = new InputRequest();

    this.service.getData("GetInvoiceModeDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.invModelist = data.Details;
      }

    },

      error => console.log(error));
    this.showloader = false;
  }

  Calculated_amt(i) {

    if (this.f.value[i].InvoiceMode.split(":")[0] == '2' && this.f.value[i].numberofbags && this.f.value[i].BagWeight) {
      let val = (((this.f.value[i].BagWeight) * (this.f.value[i].numberofbags)) / 1000).toFixed(3);
      this.f.controls[i].patchValue({
        TotalWeight: val
      });
    }

  }
}
