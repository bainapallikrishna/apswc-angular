import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpaceReservationComponent } from './space-reservation.component';

describe('SpaceReservationComponent', () => {
  let component: SpaceReservationComponent;
  let fixture: ComponentFixture<SpaceReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpaceReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpaceReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
