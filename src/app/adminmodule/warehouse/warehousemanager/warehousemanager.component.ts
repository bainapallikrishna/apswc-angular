import { Component, OnInit , ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import {
  ApexNonAxisChartSeries,
  ApexAxisChartSeries,
  ApexPlotOptions,
  ApexChart,
  ApexFill,
  ChartComponent,
  ApexDataLabels,
  ApexXAxis,
  ApexResponsive
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  labels: string[];
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  xaxis: ApexXAxis;
  fill: ApexFill;
  responsive: ApexResponsive[];
};
export type ChartOptions1 = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  labels: string[];
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  xaxis: ApexXAxis;
  fill: ApexFill;
  responsive: ApexResponsive[];
};
@Component({
  selector: 'app-warehousemanager',
  templateUrl: './warehousemanager.component.html',
  styleUrls: ['./warehousemanager.component.css']
})
export class WarehousemanagerComponent implements OnInit {
  
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  designationid=sessionStorage.getItem("logUserSectionID");
  showloader: boolean = true;
  icon1:any;
  icon2:any;
  icon3:any;
  icon4:any;
  icon5:any; 
  icon6:any;
  icon7:any;

@ViewChild("chart") chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;

  
@ViewChild("chart2") chart2: ChartComponent;
public chartOptions2: Partial<ChartOptions>;


@ViewChild("chart3") chart3: ChartComponent;
  public chartOptions3: Partial<ChartOptions>;
  

@ViewChild("chart4") chart4: ChartComponent;
  public chartOptions4: Partial<ChartOptions>;
  
@ViewChild("chart5") chart5: ChartComponent;
public chartOptions5: Partial<ChartOptions1>;
 
  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,    
    private EncrDecr: EncrDecrServiceService,
    private datef: CustomDateParserFormatter,
  ) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }

    this.chartOptions = {
      series: [14, 55, 67, 83],
      chart: {
        height: 350,
        type: "radialBar"
      },
      plotOptions: {
        radialBar: {
          dataLabels: {
            name: {
              fontSize: "22px"
            },
            value: {
              fontSize: "16px"
            },
            total: {
              show: true,
              label: "Total",
              formatter: function(w) {
                return "50,000 MT";
              }
            }
          }
        }
      },
      labels: ["Available", "Pending", "", "Occupied"]
    };

    this.chartOptions2 = {
      series: [44, 55, 13, 43, 22],
      chart: {
        type: "donut"
      },
      labels: ["Team A", "Team B", "Team C", "Team D", "Team E"],
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };

    this.chartOptions3 = {
      series: [44, 55, 13, 43, 22],
      chart: {
        type: "donut"
      },
      labels: ["Team A", "Team B", "Team C", "Team D", "Team E"],
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };

    this.chartOptions4 = {
      series: [44, 55, 13, 43, 22],

      chart: {
        height: 350,
        type: "donut"
      },
      labels: ["Team A", "Team B", "Team C", "Team D", "Team E"],
      responsive: [
        { breakpoint: 1200, options: { chart: { width: 300 }, legend: { position: "bottom" } } },
        { breakpoint: 768, options: { chart: { width: 520 }, legend: { position: "bottom" } } },
        { breakpoint: 620, options: { chart: { width: 450 }, legend: { position: "bottom" } } },
        { breakpoint: 480, options: { chart: { width: 250 }, legend: { position: "bottom" } } },
    ],
    };

    this.chartOptions5 = {  
      series: [
        {
          name: "basic",
          data: [400, 430, 448]
        }
      ],
      chart: {
        type: "bar",
        height: 350
      },
      plotOptions: {
        bar: {
          horizontal: true
        }
      },
      dataLabels: {
        enabled: false
      },
      xaxis: {
        categories: [
          "Total",
          "Payment",
          "Contracts"
        ]
      }
    };

    

   
}

ngOnInit(): void {
   //throw new Error('Method not implemented.');
    this.loadicon1data('WAREHOUSE');
    this.loadicon2data('RECEIPTS');
    this.loadicon3data('INVOICES');
    this.loadicon4data('WAREHOUSE_CAPACITY');
    this.loadicon5data('COMMODITY_CAPACITY');    
    this.loadicon6data('DEPOSITORS');
    this.loadicon7data('SPACE_RESERVATION');
    
  }

  loadicon1data(val)
  {
    this.loadiconsdata(val);
  }
  loadicon2data(val)
  {
    this.loadiconsdata(val);
  }
  loadicon3data(val)
  {
    this.loadiconsdata(val);
  }
  loadicon4data(val)
  {
    this.loadiconsdata(val);
  }
  loadicon5data(val)
  {
    this.loadiconsdata(val);
  }
  loadicon6data(val)
  {
    this.loadiconsdata(val);
  }
  loadicon7data(val)
  {
    this.loadiconsdata(val);
  }

  loadiconsdata(val)
  {
    this.showloader=true;
    const req= new InputRequest();
    req.INPUT_01=val;
    req.INPUT_02=this.workLocationCode;
    req.INPUT_03=this.designationid;
    req.INPUT_04=this.logUserName;
    this.service.postData(req,"DashboardData").subscribe(data=> {
      this.showloader=false;
      if (data.StatusCode == "100") {       

        val=="WAREHOUSE"?this.icon1= data.Details:val=="RECEIPTS"?this.icon2=data.Details[0]:
        val=="INVOICES"?this.icon3= data.Details[0]:val=="WAREHOUSE_CAPACITY"?this.icon4=data.Details[0]:   
        val=="COMMODITY_CAPACITY"?this.icon5=data.Details[0]:val=="SPACE_RESERVATION"?this.icon6=data.Details[0]:
        val=="DEPOSITORS"?this.icon7=data.Details[0]:"";


      }
     
  
    },
    error => console.log(error));
    

  }
}
