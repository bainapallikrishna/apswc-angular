import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import { HttpEventType } from '@angular/common/http';
import Swal from 'sweetalert2';
import { GBbuttonRendererComponent } from 'src/app/custome-directives/GBbutton-renderer.component';
import { ConditionalRenderer } from 'src/app/custome-directives/conditional-renderer.component';
import { AgGridAngular } from 'ag-grid-angular';
//import "ag-grid-enterprise";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DomSanitizer } from '@angular/platform-browser';
import { AadharValidateService } from 'src/app/Services/aadhar-validate.service';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { NgbDateCustomParserFormatter } from 'src/app/custome-directives/dateddmmyyyyformat'
import { NgbDateParserFormatter, NgbDate, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';



@Component({
  selector: 'app-generalbooking',
  templateUrl: './generalbooking.component.html',
  styleUrls: ['./generalbooking.component.css']
})
export class GeneralbookingComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  public components;
  showloader: boolean = false;
  btndiv: boolean = true;
  validtndiv = true;
  Userslist: any[];
  Doclist: any[];
  Commulist: any[];
  Commoditygrouplist: any[];
  Commoditylist: any[];
  Commodityrate_list: any[];
  quntityDD: any[];
  contractlist: any[];
  RegisterId: any;
  clickactn: string = "Existinguser";
  status_userbtn = "Existinguser";
  currtab: string = "accounttab";
  commoditygroup: any;
  commodity: any;
  commodityrate: any;
  NgbDateStruct: any;
  NgbEndDate: any;
  preview: any;
  invModelist: any = [];
  priceslist: any = [];
  rebatelist: any = [];
  public reservsubmi: boolean = false;

  //New User
  RUserType: string;//= sessionStorage.getItem("UserRType");  
  public regsub: boolean = false;
  public docsub: boolean = false;
  public commusubmi: boolean = false;
  public Emp_mob_isvalid: boolean = true;
  RegForm: FormGroup;
  DocForm: FormGroup;
  ComForm: FormGroup;
  FarComForm: FormGroup;
  FARForm: FormGroup;
  IndForm: FormGroup;
  GovtForm: FormGroup;
  public defaultColDef: any = [];


  PanPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  states: any = [];
  districts: any = [];
  urlist: any = [];
  nmandals: any = [];
  Pincodedata:any=[];
  nvillages: any = [];
  doculist: any = [];
  response: { dbPath: '' };
  Spc_rsevkeyword: string = "name";
  uidstatus: boolean = true;
  uid_isvalid: boolean = true;
  relations: any = [];
  RegNameSelect(event): void {
    if (this.currtab == "Newuser")
      this.reg.RegName.setValue("");
    else if (this.currtab == "Govt")
      this.govtreg.RegName.setValue("");
    else if (this.currtab == "FAR")
      this.farreg.RegName.setValue("");
    else if (this.currtab == "IND")
      this.indreg.RegName.setValue("");

  }
  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer' },

    { headerName: 'Name', width: 200, field: 'name' },
    { headerName: 'Depositor Type', width: 150, field: 'registratioN_TYPE_NAME' },
    { headerName: 'User Email', width: 200, field: 'emaiL_ID' },
    { headerName: 'Phone Number', width: 100, field: 'mobilE_NUMBER' },
    { headerName: 'Address', width: 250, field: 'address' },
    {
      headerName: 'Registration Process', width: 350, cellStyle: { 'text-align': "center" },
      children: [
        {
          headerName: 'Registration',
          maxWidth: 100,
          field: 'is_reg',
          cellRenderer: 'customRenderer',
          floatingFilter: false,


        },
        {
          headerName: 'Documents',
          maxWidth: 100,
          field: 'is_doc',
          cellRenderer: 'customRenderer',
          floatingFilter: false,
        },
        { headerName: 'Communication', width: 120, field: 'is_com', cellRenderer: 'customRenderer', floatingFilter: false, }
      ]
    },
    {
      headerName: 'Action', width: 60, floatingFilter: false, field: 'registratioN_STATUS', cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        nextClick: this.Docload.bind(this),

      },
    }
  ];

  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private sanitizer: DomSanitizer,
    private datef: CustomDateParserFormatter,
    private uidservice: AadharValidateService

  ) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
      cellStyle: { 'text-align': "left" },
    };

    this.frameworkComponents = {
      buttonRenderer: GBbuttonRendererComponent,
      customRenderer: ConditionalRenderer,
    }
    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

  }
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('previewModal') public previewModal: ModalDirective;

  startdate: NgbDateStruct;

  onDateSelection(date: NgbDate) {
    let selectedDate = NgbDateCustomParserFormatter.formatDate(date);


  }


  ngOnInit(): void {
    let now: Date = new Date();
    this.NgbDateStruct = { day: now.getDate(), month: now.getMonth() + 1, year: now.getFullYear() }

    this.loaduserlist();



    this.RegForm = this.formBuilder.group({
      REGCODE: [''],
      RegName: ['', Validators.required],
      RegEmail: ['', [Validators.required, Validators.email]],
      RegMobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      PANNo: ['', [Validators.required, Validators.minLength(10), Validators.pattern(this.PanPattern)]],
      NState: [null, Validators.required],
      NDistrict: [null, Validators.required],
      NArea: [null, Validators.required],
      NMandal: [null, Validators.required],
      NVillage: [null, Validators.required],
      NHNO: ['', Validators.required],
      NStrName: ['', Validators.required],
      NLandmark: [''],
      NPincode: ['', [Validators.required, Validators.minLength(6)]],

    });


    this.FARForm = this.formBuilder.group({
      REGCODE: [''],
      FarType: [null, Validators.required],
      RegName: ['', Validators.required],
      FarAadhaar: ['', Validators.required],
      RegEmail: [''],
      RegMobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      PANNo: [''],
      NState: [null, Validators.required],
      NDistrict: [null, Validators.required],
      NArea: [null, Validators.required],
      NMandal: [null, Validators.required],
      NVillage: [null, Validators.required],
      NHNO: ['', Validators.required],
      NStrName: ['', Validators.required],
      NLandmark: [''],
      NPincode: ['', [Validators.required, Validators.minLength(6)]],


    });


    this.IndForm = this.formBuilder.group({
      REGCODE: [''],
      RegName: ['', Validators.required],
      FarAadhaar: ['', Validators.required],
      RegEmail: [''],
      RegMobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      PANNo: [''],
      NState: [null, Validators.required],
      NDistrict: [null, Validators.required],
      NArea: [null, Validators.required],
      NMandal: [null, Validators.required],
      NVillage: [null, Validators.required],
      NHNO: ['', Validators.required],
      NStrName: ['', Validators.required],
      NLandmark: [''],
      NPincode: ['', [Validators.required, Validators.minLength(6)]],
    })

    this.GovtForm = this.formBuilder.group({
      REGCODE: [''],
      RegName: ['', Validators.required],
      RegEmail: ['', [Validators.required, Validators.email]],
      RegMobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      PANNo: ['', [Validators.required, Validators.minLength(10), Validators.pattern(this.PanPattern)]],
      NState: [null, Validators.required],
      NDistrict: [null, Validators.required],
      NArea: [null, Validators.required],
      NMandal: [null, Validators.required],
      NVillage: [null, Validators.required],
      NHNO: ['', Validators.required],
      NStrName: ['', Validators.required],
      NLandmark: [''],
      NPincode: ['', [Validators.required, Validators.minLength(6)]],
    })

    this.DocForm = this.formBuilder.group({
      Documents: new FormArray([
        this.formBuilder.group({
          DocID: ['', Validators.required],
          DocName: ['', Validators.required],
          DocType: ['', Validators.required],
          DocPath: ['', Validators.required],
          RegDoc: [''],
          progress: [0],
          message: [''],
        })
      ])
    });

    this.ComForm = this.formBuilder.group({
      Communication: new FormArray([
        this.formBuilder.group({
          CName: ['', Validators.required],
          CDesignation: ['', Validators.required],
          CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
          CEmail: [''],
        })
      ])
    });

    this.FarComForm = this.formBuilder.group({
      NName: ['', Validators.required],
      NAadhaar: ['', Validators.required],
      NRelation: [null, Validators.required],
      NMobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      NState: [null, Validators.required],
      NDistrict: [null, Validators.required],
      NArea: [null, Validators.required],
      NMandal: [null, Validators.required],
      NVillage: [null, Validators.required],
      NHNO: ['', Validators.required],
      NStrName: ['', Validators.required],
      NLandmark: [''],
      NPincode: ['', Validators.required],
      Communication: new FormArray([
        this.formBuilder.group({
          CName: ['', Validators.required],
          CDesignation: ['', Validators.required],
          CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
          CEmail: [''],

        })
      ])
    });

    this.LoadStates();
    this.LoadDistricts(); // load districts
    this.LoadurFlags(); // load areas
    this.LoadRelations();
    this.showloader = false;

  }



  loaduserlist() {

    this.showloader = true;
    const req = new InputRequest();
    this.service.postData(req, "GetExistingUser_Details").subscribe(data => {
      if (data.StatusCode == "100") {
        $("#Existinguser").addClass("active");
        this.Userslist = data.Details;
        this.gridApi.setRowData(this.Userslist);
        this.showloader = false;

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },

      error => console.log(error));
    this.showloader = false;
  }
  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.Userslist);
    //this.gridApi.sizeColumnsToFit();
  }

  Docload(exuserlist) {
    const data = exuserlist.rowData;
    this.RegisterId = data.registratioN_ID;

    // if (data.registratioN_STATUS == "Com") {

    //   this.btndiv = false;
    //   this.currtab = "Resrv";
    //   this.resetallforms();
    //   this.RUserType = data.registratioN_ID.substring(0, 3);

    //   this.LoadDoclist();
    //   this.loadcommudetails();
    //   this.loadComiditydetails();
    //   this.LoadInvoiceMode();
    //   this.loadquantity();
    //   this.loadContractlist();
    //   $("#account").addClass("active");
    //   $("#doctab").addClass("active");
    //   $("#commtab").addClass("active");
    //   $("#Resevtab").addClass("current");

    // }
    if (data.registratioN_STATUS == "Doc") {
      this.resetallforms();
      this.RUserType = data.registratioN_ID.substring(0, 3);
      this.btndiv = false;
      this.currtab = "Com";
      this.clickactn = "Newuser";

      if (this.RUserType == 'FAR')
        this.AddFarCommu();
      else
        this.AddCommu();
      //this.AddSpaceReservation();
      this.LoadDoclist();

      $("#account").addClass("active");
      $("#doctab").addClass("active");
      $("#commtab").addClass("current");

    }
    if (data.registratioN_STATUS == "Reg") {
      this.resetallforms();
      this.RUserType = data.registratioN_ID.substring(0, 3);
      this.Adddoc();
      this.LoadDocuList();
      this.btndiv = false;
      this.currtab = "Doc";
      this.clickactn = "Newuser";
      $("#account").addClass("active");
      $("#doctab").addClass("current");

    }


  }
  LoadDoclist() {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.RegisterId;


    this.service.postData(req, "GetSubmitted_Docs").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.Doclist = data.Details;
        //this.currtab = "Doc";
        //$("#doctab").addClass("active");

      }
      else
        console.log(data);


    },

      error => console.log(error));
    this.showloader = false;
  }

  loadcommudetails() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.RegisterId;
    this.service.postData(req, "GetCommu_Details").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Commulist = data.Details;

      }
      else
        console.log(data);

    },

      error => console.log(error));
    this.showloader = false;


  }

  loadquantity() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "QUANTITY_TYPE"
    this.service.postData(req, "GetQuantity_contract_Details").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.quntityDD = data.Details;

      }
      else
        console.log(data);

    },

      error => console.log(error));
    this.showloader = false;



  }

  loadContractlist() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "CONTRACT_TYPE"
    this.service.postData(req, "GetQuantity_contract_Details").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.contractlist = data.Details;

      }
      else
        console.log(data);

    },

      error => console.log(error));
    this.showloader = false;



  }

  loadComiditydetails() {


    this.showloader = true;
    const req = new InputRequest();

    this.service.postData(req, "GetCommodityGroup_Details").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.Commoditygrouplist = data.Details;


      }

    },

      error => console.log(error));
    this.showloader = false;


  }

  LoadInvoiceMode() {
    this.showloader = true;
    const req = new InputRequest();

    this.service.getData("GetInvoiceModeDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.invModelist = data.Details;
      }

    },

      error => console.log(error));
    this.showloader = false;
  }

  LoadCommodity(i) {

    // this.Commoditylist = [];

    // this.priceslist = [];
    // this.rebatelist = [];
    // this.f.controls[i].patchValue({
    //   commodity: null,
    //   InvoiceMode: null,
    //   startdate: null,
    //   enddate: null,
    //   SPrice: null,
    //   InvoiceBasedOn: null,
    //   Rebate: null,
    //   numberofbags: null,
    //   BagWeight: null,
    //   TotalWeight: null,
    // });

    // if (this.f.value[i].commoditygroup) {
    //   this.showloader = true;
    //   const req = new InputRequest();
    //   req.INPUT_01 = this.f.value[i].commoditygroup.split(":")[0];
    //   req.INPUT_02 = "COMMODITY";
    //   this.service.postData(req, "GetCommodityGroup_Details").subscribe(data => {
    //     this.showloader = false;
    //     if (data.StatusCode == "100") {

    //       this.Commoditylist = data.Details;


    //     }


    //   },

    //     error => console.log(error));
    //   this.showloader = false;

    // }
  }

  LoadRebate(i) {

    this.priceslist = [];
    this.rebatelist = [];
    // this.f.controls[i].patchValue({
    //   InvoiceMode: null,
    //   startdate: null,
    //   enddate: null,
    //   SPrice: null,
    //   InvoiceBasedOn: null,
    //   Rebate: null,
    //   numberofbags: null,
    //   BagWeight: null,
    //   TotalWeight: null,

    // });


    // if (this.f.value[i].commodity) {
    //   this.showloader = true;
    //   const req = new InputRequest();
    //   req.INPUT_01 = this.subf.commodities.value[i].commoditygroup.split(":")[0];
    //   req.INPUT_02 = this.subf.commodities.value[i].commodity.split(":")[0];
    //   req.INPUT_03 = this.RUserType;
    //   this.service.postData(req, "GetRebateDetails").subscribe(data => {
    //     this.showloader = false;
    //     if (data.StatusCode == "100") {
    //       this.rebatelist = data.Details;
    //     }
    //   },
    //     error => console.log(error));
    //   this.showloader = false;
    // }

  }

  LoadCommodity_Rate(i) {

    // this.f.controls[i].patchValue({
    //   BagWeight: null,
    //   numberofbags: null,
    //   TotalWeight: null,
    // });

    // if (this.f.value[i].commodity && this.f.value[i].commoditygroup && this.f.value[i].InvoiceMode && this.f.value[i].startdate && this.f.value[i].enddate) {
    //   this.priceslist = [];
    //   this.showloader = true;
    //   const req = new InputRequest();
    //   req.INPUT_01 = this.f.value[i].commoditygroup.split(":")[0];
    //   req.INPUT_02 = this.f.value[i].commodity.split(":")[0];
    //   req.INPUT_03 = this.f.value[i].InvoiceMode;
    //   req.INPUT_04 = this.datef.format(this.f.value[i].startdate);
    //   this.service.postData(req, "GetInvPriceDetails").subscribe(data => {
    //     this.showloader = false;
    //     if (data.StatusCode == "100") {
    //       this.priceslist = data.Details;
    //     }
    //   },
    //     error => console.log(error));
    //   this.showloader = false;
    // }

  }

  Userclick(clickval) {
    if (clickval == "Existinguser" || clickval == "Reset") {
      this.loaduserlist();
      this.status_userbtn = clickval == 'Reset' ? "Existinguser" : clickval;
      this.clickactn = clickval == 'Reset' ? "Existinguser" : clickval;
      this.currtab = "accounttab";
      this.btndiv = true;
      this.resetallforms();
      $("#account").removeClass("active");
      $("#doctab").removeClass("active");
      $("#commtab").removeClass("active");
      $("#Resevtab").removeClass("active");
      $("#Existinguser").addClass("active");
      $("#Newuser").removeClass("active");

    }
    else {
      this.status_userbtn = clickval;
      this.clickactn = clickval;
      this.currtab = "accounttab";
      this.resetallforms();
      $("#Existinguser").removeClass("active");
      $("#Newuser").addClass("active");
      $("#account").removeClass("active");
      $("#doctab").removeClass("active");
      $("#commtab").removeClass("active");
      $("#Resevtab").removeClass("active");
    }

  }

  resetallforms() {

    $("#account").removeClass("active");
    $("#doctab").removeClass("active");
    $("#commtab").removeClass("active");
    // $("#Resevtab").removeClass("active");
    $("#account").removeClass("current");
    $("#doctab").removeClass("current");
    $("#commtab").removeClass("current");
    // $("#Resevtab").removeClass("current");

    this.DocForm.reset();
    this.RegForm.reset();
    this.ComForm.reset();
    this.FarComForm.reset();
    this.FARForm.reset();
    this.IndForm.reset();
    this.GovtForm.reset();

    this.d.reset();
    this.d.clear();
    this.c.reset();
    this.c.clear();
    //this.f.reset();
    //this.f.clear();
    this.fc.reset();
    this.fc.clear();
    this.regsub = false;
    this.reservsubmi = false;
    this.commusubmi = false;
    this.RUserType = "";

  }

  back(val) {
    if (val == "Existing") {
      this.currtab = "accounttab";
      this.btndiv = true;
      this.loaduserlist();
      this.resetallforms();
      $("#account").removeClass("active");
      $("#doctab").removeClass("active");
      $("#commtab").removeClass("active");
      $("#Resevtab").removeClass("active");
    }
    if (val == "Docs") {
      this.currtab = "Doc";
      $("#doctab").addClass("active");
      $("#commtab").removeClass("active");
    }
    if (val == "ContDetails") {
      this.currtab = "Com";
      $("#commtab").addClass("active");
      $("#Resevtab").removeClass("active");
    }




  }
  skip(val) {
    if (val == "commu") {
      this.currtab = "Com";
      this.loadcommudetails();
      $("#commtab").addClass("active");
    }
    if (val == "resrvtn") {
      this.currtab = "Resrv";
      this.loadComiditydetails();
      this.LoadInvoiceMode();
      // this.loadComiditydetails();
      $("#Resevtab").addClass("active");
    }
  }

  Calculated_amt(i) {

    // if (this.f.value[i].InvoiceMode == '2' && this.f.value[i].numberofbags && this.f.value[i].BagWeight) {
    //   let val = (((this.f.value[i].BagWeight) * (this.f.value[i].numberofbags)) / 1000).toFixed(3);
    //   this.f.controls[i].patchValue({
    //     TotalWeight: val
    //   });
    // }

  }

  validationforweight(i) {
    // this.f.controls[i].patchValue({
    //   numberofbags: null
    // });
    // let val = (this.f.value[i].quantitytype)
    // if (val == '1')
    //   this.validtndiv = false;
    // else
    //   this.validtndiv = true;

  }


  // get subf() { return this.SpaceReservForm.controls; }
  // get f() { return this.subf.commodities as FormArray; }
  // Submit_spaceresevation() {
  //   this.reservsubmi = true;

  //   // stop here if form is invalid
  //   if (this.SpaceReservForm.invalid) {
  //     return false;
  //   }

  //   for (let i = 0; i < this.f.length; i++) {
  //     if (this.datef.GreaterDate(this.f.value[i].startdate, this.f.value[i].enddate)) {
  //       Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
  //       return false;
  //     }
  //   }


  //   this.showloader = true;

  //   let cmdgrpcodelist: string[] = [];
  //   let cmdgrplist: string[] = [];
  //   let cmdtycodelist: string[] = [];
  //   let cmdtylist: string[] = [];
  //   let invmodelist: string[] = [];
  //   let startdatelist: string[] = [];
  //   let enddatelist: string[] = [];
  //   let pricelist: string[] = [];
  //   let invbaselist: string[] = [];
  //   let inscolllist: string[] = [];
  //   let taxlist: string[] = [];
  //   let rebatelist: string[] = [];
  //   let bagslist: string[] = [];
  //   let bagweilist: string[] = [];
  //   let quntitylist: string[] = [];



  //   for (let i = 0; i < this.f.length; i++) {
  //     cmdgrpcodelist.push(this.f.value[i].commoditygroup.split(":")[0]);
  //     cmdgrplist.push(this.f.value[i].commoditygroup.split(":")[1]);
  //     cmdtycodelist.push(this.f.value[i].commodity.split(":")[0]);
  //     cmdtylist.push(this.f.value[i].commodity.split(":")[1]);
  //     invmodelist.push(this.f.value[i].InvoiceMode);
  //     startdatelist.push(this.datef.format(this.f.value[i].startdate));
  //     enddatelist.push(this.datef.format(this.f.value[i].enddate));
  //     pricelist.push(this.f.value[i].SPrice ?? '');
  //     invbaselist.push(this.f.value[i].InvoiceBasedOn ?? '');
  //     inscolllist.push(this.f.value[i].InsuranceCollection ?? '');
  //     taxlist.push(this.f.value[i].RTax ?? '');
  //     rebatelist.push(this.f.value[i].Rebate ?? '');
  //     bagslist.push(this.f.value[i].numberofbags ?? '');
  //     bagweilist.push(this.f.value[i].BagWeight ?? '');
  //     quntitylist.push(this.f.value[i].TotalWeight ?? '');

  //   }

  //   //alert(this.clickactn);
  //   const req = new InputRequest();
  //   req.INPUT_01 = cmdgrpcodelist.join(',');
  //   req.INPUT_02 = cmdgrplist.join(',');
  //   req.INPUT_03 = cmdtylist.join(',');
  //   req.INPUT_04 = bagweilist.join(',');
  //   req.INPUT_05 = startdatelist.join(',');
  //   req.INPUT_06 = enddatelist.join(',');
  //   req.INPUT_07 = bagslist.join(',');
  //   req.INPUT_08 = this.status_userbtn == 'Existinguser' ? this.RegisterId : this.reg.REGCODE.value;//regid
  //   req.INPUT_09 = this.workLocationCode;//whid
  //   req.INPUT_10 = cmdtycodelist.join(',');
  //   req.INPUT_11 = invmodelist.join(',');
  //   req.INPUT_12 = invbaselist.join(',');
  //   req.INPUT_13 = inscolllist.join(',');
  //   req.INPUT_14 = taxlist.join(',');
  //   req.INPUT_15 = pricelist.join(',');
  //   req.INPUT_16 = rebatelist.join(',');
  //   req.INPUT_17 = quntitylist.join(',');

  //   req.USER_NAME = this.logUserName;
  //   req.CALL_SOURCE = "Web";

  //   this.service.postData(req, "SaveSpaceReservation").subscribe(data => {
  //     this.showloader = false;

  //     if (data.StatusCode == "100") {
  //       let result = data.Details[0];
  //       if (result.rtN_ID === 1) {

  //         Swal.fire('success', "Depositor " + (this.status_userbtn == 'Existinguser' ? this.RegisterId : this.reg.REGCODE.value) + " , Space Reservation Details Saved Successfully !!!", 'success');
  //         this.resetallforms();
  //         this.loaduserlist();
  //         this.currtab = "accounttab";
  //         this.btndiv = true;
  //         this.clickactn = this.status_userbtn;
  //         if (this.status_userbtn == 'Newuser') {
  //           $("#Newuser").addClass("active");
  //           $("#Existinguser").removeClass("active");
  //         }
  //         else {
  //           $("#Newuser").removeClass("active");
  //           $("#Existinguser").addClass("active");
  //         }


  //       }
  //       else
  //         Swal.fire('warning', result.statuS_TEXT, 'warning');

  //     }
  //     else
  //       Swal.fire('warning', data.StatusMessage, 'warning');

  //   },
  //     error => console.log(error));




  // }


  // AddSpaceReservation() {
  //   this.f.push(this.formBuilder.group({

  //     commoditygroup: [null, Validators.required],
  //     commodity: [null, Validators.required],
  //     InvoiceMode: [null, Validators.required],
  //     startdate: [null, Validators.required],
  //     enddate: [null, Validators.required],
  //     SPrice: [null, Validators.required],
  //     InvoiceBasedOn: [null, Validators.required],
  //     //storagetype: [null, Validators.required],
  //     InsuranceCollection: [''],
  //     RTax: [''],
  //     Rebate: [null],
  //     numberofbags: [''],
  //     //quantitytype: [null, Validators.required],
  //     BagWeight: [''],
  //     TotalWeight: [null, Validators.required],



  //   }));
  // }

  // RemoveSpaceReservation(index) {
  //   this.f.removeAt(index);
  // }


  Docpriview(val) {

    this.preview = this.sanitizer.bypassSecurityTrustResourceUrl(val);
    this.previewModal.show();

  }


  //New User

  RegisterUser(type: string) {

    this.RUserType = type
    $("#account").addClass("active");
    $("#account").addClass("current");
    this.Adddoc();
    this.LoadDocuList();
    if (type == 'FER' || type == 'PVT' || type == 'PRO')
      this.currtab = "Newuser";
    else if (type == 'STA' || type == 'CEN' || type == 'PSU')
      this.currtab = "Govt";
    else if (type == 'FAR')
      this.currtab = "FAR";
    else if (type == 'IND')
      this.currtab = "IND";


    //alert(this.RUserType);
    //this.router.navigate(['/RegistrationUser']);
  }
  get reg() { return this.RegForm.controls; }
  get farreg() { return this.FARForm.controls; }
  get indreg() { return this.IndForm.controls; }
  get govtreg() { return this.GovtForm.controls; }
  get doc() { return this.DocForm.controls; }
  get com() { return this.ComForm.controls; }
  get farcom() { return this.FarComForm.controls; }

  get d() { return this.doc.Documents as FormArray; }
  get c() { return this.com.Communication as FormArray; }
  get fc() { return this.farcom.Communication as FormArray; }

  mob_uid_validate(mobilephone) {
    if (mobilephone.length === 10)
      this.Emp_mob_isvalid = this.uidservice.validatemob(mobilephone);
  }

  uidvalidate(FarAadhaar) {
    if (FarAadhaar.length == 12) {
      this.uidstatus = this.uidservice.validate(this.currtab == 'FAR' ? this.farreg.FarAadhaar.value : this.indreg.FarAadhaar.value);
    }
  }

  FarmerTypeChange(ftype) {
    this.d.reset();
    this.d.clear();
    this.Adddoc();
    if (ftype == "1") {
      let fardoclist = this.doculist.filter(doc => doc.iteM_ID != '4')
      this.FillDocDetails(fardoclist);
    }
    else if (ftype == "2") {
      let fardoclist = this.doculist.filter(doc => doc.iteM_ID != '5')
      this.FillDocDetails(fardoclist);
    }

  }



  AddCommu() {
    this.c.push(this.formBuilder.group({
      CName: ['', Validators.required],
      CDesignation: ['', Validators.required],
      CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      CEmail: [''],
    }));
  }

  RemoveCommu(index) {
    this.c.removeAt(index);
  }

  AddFarCommu() {
    this.fc.push(this.formBuilder.group({
      CName: ['', Validators.required],
      CDesignation: ['', Validators.required],
      CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      CEmail: [''],
    }));
  }

  RemoveFarCommu(index) {
    this.fc.removeAt(index);
  }

  UserRegistration() {
    this.regsub = true;

    if (this.RegForm.invalid) {
      return false;
    }


    if (!this.Emp_mob_isvalid) {
      return false;
    }
    if (!this.RUserType) {
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.RUserType;
    req.INPUT_02 = this.reg.RegName.value;
    req.INPUT_03 = this.reg.RegEmail.value;
    req.INPUT_04 = this.reg.RegMobile.value;
    req.INPUT_05 = this.reg.PANNo.value;
    req.INPUT_06 = this.reg.NState.value;
    req.INPUT_07 = this.reg.NDistrict.value;
    req.INPUT_08 = this.reg.NMandal.value;
    req.INPUT_09 = this.reg.NVillage.value;
    req.INPUT_10 = this.reg.NArea.value;
    req.INPUT_11 = this.reg.NHNO.value;
    req.INPUT_12 = this.reg.NStrName.value;
    req.INPUT_13 = this.reg.NLandmark.value;
    req.INPUT_14 = this.reg.NPincode.value;

    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveRegistrationDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.reg.REGCODE.setValue(result.registratioN_ID);
          Swal.fire('success', "Registration Details Saved Successfully !!!", 'success');
          this.currtab = "Doc";
          $("#account").addClass("active");
          $("#account").removeClass("current");

          $("#doctab").addClass("current");

        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }


  FarRegistration() {
    this.regsub = true;

    if (this.FARForm.invalid) {
      return false;
    }



    if (!this.Emp_mob_isvalid) {
      return false;
    }
    if (!this.RUserType) {
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.RUserType;
    req.INPUT_02 = this.farreg.RegName.value;
    req.INPUT_03 = this.farreg.RegEmail.value;
    req.INPUT_04 = this.farreg.RegMobile.value;
    req.INPUT_05 = this.farreg.PANNo.value;
    req.INPUT_06 = this.farreg.NState.value;
    req.INPUT_07 = this.farreg.NDistrict.value;
    req.INPUT_08 = this.farreg.NMandal.value;
    req.INPUT_09 = this.farreg.NVillage.value;
    req.INPUT_10 = this.farreg.NArea.value;
    req.INPUT_11 = this.farreg.NHNO.value;
    req.INPUT_12 = this.farreg.NStrName.value;
    req.INPUT_13 = this.farreg.NLandmark.value;
    req.INPUT_14 = this.farreg.NPincode.value;
    req.INPUT_15 = this.farreg.FarType.value;
    req.INPUT_16 = this.farreg.FarAadhaar.value;

    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveRegistrationDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.reg.REGCODE.setValue(result.registratioN_ID);
          Swal.fire('success', "Registration Details Saved Successfully !!!", 'success');
          this.currtab = "Doc";
          $("#account").addClass("active");
          $("#account").removeClass("current");

          $("#doctab").addClass("current");

        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  GovtRegistration() {
    this.regsub = true;

    // stop here if form is invalid
    if (this.GovtForm.invalid) {
      return false;
    }

    if (!this.Emp_mob_isvalid) {
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.RUserType;
    req.INPUT_02 = this.govtreg.RegName.value;
    req.INPUT_03 = this.govtreg.RegEmail.value;
    req.INPUT_04 = this.govtreg.RegMobile.value;
    req.INPUT_05 = this.govtreg.PANNo.value;
    req.INPUT_06 = this.govtreg.NState.value;
    req.INPUT_07 = this.govtreg.NDistrict.value;
    req.INPUT_08 = this.govtreg.NMandal.value;
    req.INPUT_09 = this.govtreg.NVillage.value;
    req.INPUT_10 = this.govtreg.NArea.value;
    req.INPUT_11 = this.govtreg.NHNO.value;
    req.INPUT_12 = this.govtreg.NStrName.value;
    req.INPUT_13 = this.govtreg.NLandmark.value;
    req.INPUT_14 = this.govtreg.NPincode.value;

    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveRegistrationDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.reg.REGCODE.setValue(result.registratioN_ID);
          Swal.fire('success', "Registration Details Saved Successfully !!!", 'success');

          this.currtab = "Doc";
          $("#account").addClass("active");
          $("#account").removeClass("current");

          $("#doctab").addClass("current");
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  IndRegistration() {
    this.regsub = true;

    if (this.IndForm.invalid) {
      return false;
    }



    if (!this.Emp_mob_isvalid) {
      return false;
    }
    if (!this.RUserType) {
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.RUserType;
    req.INPUT_02 = this.indreg.RegName.value;
    req.INPUT_03 = this.indreg.RegEmail.value;
    req.INPUT_04 = this.indreg.RegMobile.value;
    req.INPUT_05 = this.indreg.PANNo.value;
    req.INPUT_06 = this.indreg.NState.value;
    req.INPUT_07 = this.indreg.NDistrict.value;
    req.INPUT_08 = this.indreg.NMandal.value;
    req.INPUT_09 = this.indreg.NVillage.value;
    req.INPUT_10 = this.indreg.NArea.value;
    req.INPUT_11 = this.indreg.NHNO.value;
    req.INPUT_12 = this.indreg.NStrName.value;
    req.INPUT_13 = this.indreg.NLandmark.value;
    req.INPUT_14 = this.indreg.NPincode.value;
    //req.INPUT_15 = this.indreg.FarType.value;
    req.INPUT_16 = this.indreg.FarAadhaar.value;

    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveRegistrationDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.reg.REGCODE.setValue(result.registratioN_ID);
          Swal.fire('success', "Registration Details Saved Successfully !!!", 'success');

          this.currtab = "Doc";
          $("#account").addClass("active");
          $("#account").removeClass("current");
          $("#doctab").addClass("current");

        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }
  Adddoc() {
    this.d.push(this.formBuilder.group({
      DocID: ['', Validators.required],
      DocName: ['', Validators.required],
      DocType: ['', Validators.required],
      DocPath: ['', Validators.required],
      RegDoc: [''],
      progress: [0],
      message: [''],
    }));

  }

  SaveDocDetails() {
    this.docsub = true;
    for (let i = 0; i < this.d.length; i++) {
      if (!this.d.value[i].DocPath) {
        Swal.fire('warning', "Please Upload all Required Documents", 'warning');
        return false;
      }
    }

    if (this.DocForm.invalid) {
      return false;
    }

    if (this.status_userbtn == "Existinguser") {
      if (!this.RegisterId) {
        Swal.fire('warning', "Please submit registration first", 'warning');
        return false;
      }
    }
    else {
      if (!this.reg.REGCODE.value) {
        Swal.fire('warning', "Please submit registration first", 'warning');
        return false;
      }
    }



    let idslist: string[] = [];
    let pathlist: string[] = [];
    let typelist: string[] = [];

    for (let i = 0; i < this.d.length; i++) {
      if (!this.d.value[i].DocPath) {
        Swal.fire('warning', "Please Upload all Required Documents", 'warning');
        return false;
      }

      idslist.push(this.d.value[i].DocID);
      pathlist.push(this.d.value[i].DocPath);
      typelist.push(this.d.value[i].DocType);
    }

    const req = new InputRequest();
    req.INPUT_01 = this.status_userbtn == 'Existinguser' ? this.RegisterId : this.reg.REGCODE.value;
    req.INPUT_02 = idslist.join(',');
    req.INPUT_03 = pathlist.join(',');
    req.INPUT_04 = typelist.join(',');

    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveRegDocDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Document Upload Details Saved Successfully !!!", 'success');

          this.AddCommu();
          this.AddFarCommu();
          this.currtab = "Com";
          $("#account").addClass("active");
          $("#doctab").addClass("active");
          $("#account").removeClass("current");
          $("#doctab").removeClass("current");

          $("#commtab").addClass("current");

        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  SaveCommDetails() {
    this.commusubmi = true;
    if (this.ComForm.invalid) {
      return false;
    }
    if (this.status_userbtn == "Existinguser") {
      if (!this.RegisterId) {
        Swal.fire('warning', "Please submit registration first", 'warning');
        return false;
      }
    }
    else {
      if (!this.reg.REGCODE.value) {
        Swal.fire('warning', "Please submit registration first", 'warning');
        return false;
      }
    }

    if (!this.Emp_mob_isvalid) {
      return false;
    }

    let namelist: string[] = [];
    let desilist: string[] = [];
    let moblist: string[] = [];
    let mailist: string[] = [];

    for (let i = 0; i < this.c.length; i++) {
      namelist.push(this.c.value[i].CName);
      desilist.push(this.c.value[i].CDesignation);
      moblist.push(this.c.value[i].CMobile);
      mailist.push(this.c.value[i].CEmail);
    }

    const req = new InputRequest();
    req.INPUT_01 = this.status_userbtn == 'Existinguser' ? this.RegisterId : this.reg.REGCODE.value;
    req.INPUT_02 = namelist.join(',');
    req.INPUT_03 = desilist.join(',');
    req.INPUT_04 = moblist.join(',');
    req.INPUT_05 = mailist.join(',');

    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveRegCommuDetails").subscribe((data) => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Communication Details Saved Successfully !!!", 'success');
          //this.AddSpaceReservation();
          // this.loadComiditydetails();
          // this.loadContractlist();
          // this.loadquantity();
          // this.currtab = "Resrv";
          // this.clickactn = 'Existinguser';
          // $("#account").addClass("active");
          // $("#doctab").addClass("active");
          // $("#commtab").addClass("active");
          // $("#account").removeClass("current");
          // $("#doctab").removeClass("current");
          // $("#commtab").removeClass("current");

          // $("#Resevtab").addClass("current");

          //this.router.navigate(['/Registration'])
          this.Userclick('Reset');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  SaveFarCommDetails() {
    this.commusubmi = true;
    if (this.FarComForm.invalid) {
      return false;
    }
    if (this.status_userbtn == "Existinguser") {
      if (!this.RegisterId) {
        Swal.fire('warning', "Please submit registration first", 'warning');
        return false;
      }
    }
    else {
      if (!this.reg.REGCODE.value) {
        Swal.fire('warning', "Please submit registration first", 'warning');
        return false;
      }
    }

    if (!this.Emp_mob_isvalid) {
      return false;
    }

    let namelist: string[] = [];
    let desilist: string[] = [];
    let moblist: string[] = [];
    let mailist: string[] = [];

    for (let i = 0; i < this.fc.length; i++) {
      namelist.push(this.fc.value[i].CName);
      desilist.push(this.fc.value[i].CDesignation);
      moblist.push(this.fc.value[i].CMobile);
      mailist.push(this.fc.value[i].CEmail);
    }

    const req = new InputRequest();
    req.INPUT_01 = this.status_userbtn == 'Existinguser' ? this.RegisterId : this.reg.REGCODE.value;
    req.INPUT_02 = namelist.join(',');
    req.INPUT_03 = desilist.join(',');
    req.INPUT_04 = moblist.join(',');
    req.INPUT_05 = mailist.join(',');

    req.INPUT_06 = this.farcom.NName.value;
    req.INPUT_07 = this.farcom.NAadhaar.value;
    req.INPUT_08 = this.farcom.NRelation.value;
    req.INPUT_09 = this.farcom.NMobile.value;
    req.INPUT_10 = this.farcom.NState.value;
    req.INPUT_11 = this.farcom.NDistrict.value;
    req.INPUT_12 = this.farcom.NMandal.value;
    req.INPUT_13 = this.farcom.NVillage.value;
    req.INPUT_14 = this.farcom.NHNO.value;
    req.INPUT_15 = this.farcom.NArea.value;
    req.INPUT_16 = this.farcom.NStrName.value;
    req.INPUT_17 = this.farcom.NLandmark.value;
    req.INPUT_18 = this.farcom.NPincode.value;

    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveFarCommuDetails").subscribe((data) => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Communication Details Saved Successfully !!!", 'success');
          this.Userclick('Reset');
          //this.AddSpaceReservation();
          // this.loadComiditydetails();
          // this.LoadInvoiceMode();
          // this.loadContractlist();
          // this.loadquantity();
          // this.currtab = "Resrv";
          // this.clickactn = 'Existinguser';
          // $("#account").addClass("active");
          // $("#doctab").addClass("active");
          // $("#commtab").addClass("active");
          // $("#account").removeClass("current");
          // $("#doctab").removeClass("current");
          // $("#commtab").removeClass("current");

          // $("#Resevtab").addClass("current");

          //this.router.navigate(['/Registration'])

        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  LoadDocuList() {
    //alert(this.RUserType);
    this.showloader = true;
    let obj: any = { "INPUT_01": this.RUserType }

    this.service.postData(obj, "GetReqDocByReg").subscribe(data => {


      if (data.StatusCode == "100") {
        this.doculist = data.Details;
        this.FillDocDetails(this.doculist);
        this.showloader = false;
      }

    },
      error => console.log(error));
    this.showloader = false;
  }

  FillDocDetails(docdata: any) {
    for (let i = 0; i < docdata.length; i++) {
      if (i != 0) {
        this.d.push(this.formBuilder.group({
          DocID: ['', Validators.required],
          DocName: ['', Validators.required],
          DocType: ['', Validators.required],
          DocPath: ['', Validators.required],
          RegDoc: [''],
          progress: [0],
          message: [''],
        }));
      }

      this.d.controls[i].patchValue({
        DocID: docdata[i].iteM_ID,
        DocName: docdata[i].iteM_NAME
      });

    }
  }

  uploadFile(event, index: number) {
    if (event.target.files && event.target.files[0]) {
      let indx: number = index;
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      if (filetype != 'image/jpeg' || filetype != 'image/png')
        doctype = 'IMAGE';
      else
        doctype = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('regid', this.reg.REGCODE.value);
      formData.append('category', this.d.value[indx].DocName);
      formData.append('pagename', "General Booking Registration");

      this.service.UploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.d.controls[indx].patchValue({ progress: Math.round(100 * event.loaded / event.total) });
          else if (event.type === HttpEventType.Response) {
            this.d.controls[indx].patchValue({ message: 'Upload success.' });
            this.d.controls[indx].patchValue({ DocType: doctype });
            this.uploadFinished(event.body, indx);
          }

        });
    }
  }

  public createImgPath = (serverPath: string) => {
    return `${this.service.filebaseUrl.replace("api/FilesUpload", "")}${serverPath}`;
  }

  public uploadFinished = (event, indx) => {
    //alert(event.dbPath);
    this.d.controls[indx].patchValue({ DocPath: event.fullPath });
    //this.response = event.dbPath;
    //alert(this.response.dbPath)
  }

  LoadStates() {
    this.service.getData("GetStates").subscribe(data => {

      if (data.StatusCode == "100") {
        this.states = data.Details;
      }

    },
      error => console.log(error));
  }

  PincodeDetails(type: string) {
    
    let typevlaue: string = type;
    let pin=typevlaue == "reg" ? this.reg.NPincode.value : typevlaue == "govtreg" ? this.govtreg.NPincode.value : typevlaue == "farreg" ? this.farreg.NPincode.value: this.indreg.NPincode.value;

    if (pin.length == "6") {

      this.districts=[];
      this.urlist=[];   
      this.nmandals=[];
      if (typevlaue == 'reg') {

        this.reg.NDistrict.setValue(null);
        this.reg.NArea.setValue(null);
        this.reg.NMandal.setValue(null);

      }
      else if (typevlaue == 'govtreg') {

        this.govtreg.NDistrict.setValue(null);
        this.govtreg.NArea.setValue(null);
        this.govtreg.NMandal.setValue(null);


      }
      else if (typevlaue == 'farreg') {

        this.farreg.NDistrict.setValue(null);
        this.farreg.NArea.setValue(null);
        this.farreg.NMandal.setValue(null);


      }
      else if (typevlaue == 'indreg') {

        this.indreg.NDistrict.setValue(null);
        this.indreg.NArea.setValue(null);
        this.indreg.NMandal.setValue(null);


      }
      this.showloader = true;
      const req = new InputRequest();
      req.INPUT_01 = pin;
      this.service.postData(req, "GetPincodeDetails").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.Pincodedata = data.Details;

          if (data.Details.length == 1) {
            if (typevlaue == 'reg') {
              this.districts =[{districT_CODE:data.Details[0].districT_CODE,districT_NAME_ENG:data.Details[0].districT_NAME}];
              this.urlist = data.Details;
              this.nmandals = data.Details;
              this.reg.NDistrict.setValue(data.Details[0].districT_CODE);
              this.reg.NArea.setValue(data.Details[0].id);
              this.reg.NMandal.setValue(data.Details[0].mmC_CODE);
              this.MandalChange(typevlaue);
            }
            else if (typevlaue == 'govtreg') {
              this.districts =[{districT_CODE:data.Details[0].districT_CODE,districT_NAME_ENG:data.Details[0].districT_NAME}];
              this.urlist = data.Details;
              this.nmandals = data.Details;
              this.govtreg.NDistrict.setValue(data.Details[0].districT_CODE);
              this.govtreg.NArea.setValue(data.Details[0].id);
              this.govtreg.NMandal.setValue(data.Details[0].mmC_CODE);
              this.MandalChange(typevlaue);

            }
            else if (typevlaue == 'farreg') {
              this.districts =[{districT_CODE:data.Details[0].districT_CODE,districT_NAME_ENG:data.Details[0].districT_NAME}];
              this.urlist = data.Details;
              this.nmandals = data.Details;
              this.farreg.NDistrict.setValue(data.Details[0].districT_CODE);
              this.farreg.NArea.setValue(data.Details[0].id);
              this.farreg.NMandal.setValue(data.Details[0].mmC_CODE);
              this.MandalChange(typevlaue);

            }

            else if (typevlaue == 'indreg') {
              this.districts =[{districT_CODE:data.Details[0].districT_CODE,districT_NAME_ENG:data.Details[0].districT_NAME}];
              this.urlist = data.Details;
              this.nmandals = data.Details;
              this.indreg.NDistrict.setValue(data.Details[0].districT_CODE);
              this.indreg.NArea.setValue(data.Details[0].id);
              this.indreg.NMandal.setValue(data.Details[0].mmC_CODE);
              this.MandalChange(typevlaue);

            }



          }
          else {
            
            this.LoadDistricts(); // load districts
            this.LoadurFlags(); // load areas
          }

        }
        else {
         

          this.LoadDistricts(); // load districts
          this.LoadurFlags(); // load areas
        }

      },
        error => console.log(error));
    }


  }

  LoadDistricts() {
    this.service.getData("GetDistricts").subscribe(data => {

      if (data.StatusCode == "100") {
        this.districts = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadurFlags() {
    this.service.getData("GetAreaTypes").subscribe(data => {

      if (data.StatusCode == "100") {
        this.urlist = data.Details;

      }

    },
      error => console.log(error));
  }

  DistrictChange(type: string) {
    this.nmandals = [];
    this.nvillages = [];
    if (this.currtab == 'FAR') {
      this.farreg.NMandal.setValue(null);
      this.farreg.NVillage.setValue(null);
      if (this.farreg.NDistrict.value && this.farreg.NArea.value) {
        this.LoadMandals(this.farreg.NDistrict.value, this.farreg.NArea.value)
      }
    }
    if (this.currtab == 'IND') {
      this.indreg.NMandal.setValue(null);
      this.indreg.NVillage.setValue(null);
      if (this.indreg.NDistrict.value && this.indreg.NArea.value) {
        this.LoadMandals(this.indreg.NDistrict.value, this.indreg.NArea.value)
      }
    }

    if (this.currtab == 'Govt') {
      this.govtreg.NMandal.setValue(null);
      this.govtreg.NVillage.setValue(null);
      if (this.govtreg.NDistrict.value && this.govtreg.NArea.value) {
        this.LoadMandals(this.govtreg.NDistrict.value, this.govtreg.NArea.value)
      }
    }
    if (this.currtab == 'Newuser') {
      this.reg.NMandal.setValue(null);
      this.reg.NVillage.setValue(null);
      if (this.reg.NDistrict.value && this.reg.NArea.value) {
        this.LoadMandals(this.reg.NDistrict.value, this.reg.NArea.value)
      }
    }

  }

  NDistrictChange(type: string) {
    this.nmandals = [];
    this.nvillages = [];
    this.farcom.NMandal.setValue(null);
    this.farcom.NVillage.setValue(null);
    if (this.farcom.NDistrict.value && this.farcom.NArea.value) {
      this.NLoadMandals(this.farcom.NDistrict.value, this.farcom.NArea.value)
    }
  }

  LoadMandals(distval: string, areaval: string) {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = areaval;
    req.INPUT_02 = distval;

    this.service.postData(req, "GetMandlas").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.nmandals = data.Details;
      }

    },
      error => console.log(error));
  }

  NLoadMandals(distval: string, areaval: string) {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = areaval;
    req.INPUT_02 = distval;

    this.service.postData(req, "GetMandlas").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.nmandals = data.Details;
      }

    },
      error => console.log(error));
  }

  MandalChange(type: string) {
    this.nvillages = [];
    if (this.currtab == 'Newuser') {
      this.reg.NVillage.setValue(null);
      if (this.reg.NDistrict.value && this.reg.NMandal.value) {
        this.LoadVillages(this.reg.NDistrict.value, this.reg.NMandal.value)
      }
    }
    if (this.currtab == 'FAR') {
      this.farreg.NVillage.setValue(null);
      if (this.farreg.NDistrict.value && this.farreg.NMandal.value) {
        this.LoadVillages(this.farreg.NDistrict.value, this.farreg.NMandal.value)
      }
    }
    if (this.currtab == 'IND') {
      this.indreg.NVillage.setValue(null);
      if (this.indreg.NDistrict.value && this.indreg.NMandal.value) {
        this.LoadVillages(this.indreg.NDistrict.value, this.indreg.NMandal.value)
      }
    }

    if (this.currtab == 'Govt') {
      this.govtreg.NVillage.setValue(null);
      if (this.govtreg.NDistrict.value && this.govtreg.NMandal.value) {
        this.LoadVillages(this.govtreg.NDistrict.value, this.govtreg.NMandal.value)
      }
    }
  }

  NMandalChange(type: string) {
    this.nvillages = [];
    this.farcom.NVillage.setValue(null);
    if (this.farcom.NDistrict.value && this.farcom.NMandal.value) {
      this.LoadVillages(this.farcom.NDistrict.value, this.farcom.NMandal.value)
    }
  }


  LoadVillages(distval: string, manval: string) {
    this.showloader = true;
    let obj: any = { "INPUT_02": distval, "INPUT_03": manval }

    this.service.postData(obj, "GetVillages").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.nvillages = data.Details;
      }

    },
      error => console.log(error));
  }



  LoadRelations() {
    this.service.getData("GetRelations").subscribe(data => {

      if (data.StatusCode == "100") {
        this.relations = data.Details;

      }

    },
      error => console.log(error));
  }
}
