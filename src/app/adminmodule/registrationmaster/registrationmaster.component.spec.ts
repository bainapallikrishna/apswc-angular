import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationmasterComponent } from './registrationmaster.component';

describe('RegistrationmasterComponent', () => {
  let component: RegistrationmasterComponent;
  let fixture: ComponentFixture<RegistrationmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrationmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrationmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
