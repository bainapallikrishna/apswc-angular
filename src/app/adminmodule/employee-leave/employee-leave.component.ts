import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { CommonServices } from 'src/app/Services/common.services';
//import { gridbuttonrenderer } from 'src/app/custome-directives/gridbutton-renderer.component';
import { AgGridAngular } from 'ag-grid-angular';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { Router } from '@angular/router';
import { HistorybuttonRendererComponent } from 'src/app/custome-directives/Historybutton-renderer.component';


@Component({
  selector: 'app-employee-leave',
  templateUrl: './employee-leave.component.html',
  styleUrls: ['./employee-leave.component.css']
})
export class EmployeeLeaveComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('historyempModal') historyempModal: ModalDirective;
  @ViewChild('LeaveDetailsempModal') LeaveDetailsempModal: ModalDirective;
  @ViewChild('encashModal') encashModal: ModalDirective;



  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");


  columnDefs = [
    { headerName: '#', maxWidth: 50, cellRenderer: 'rowIdRenderer' },
    { headerName: 'Application ID', maxWidth: 150, field: 'applicatioN_ID', cellRenderer: 'actionrender', sortable: true, filter: true },
    { headerName: 'Name', maxWidth: 150, field: 'emP_NAME', sortable: true, filter: true },
    { headerName: 'From Date', maxWidth: 120, field: 'froM_DATE', sortable: true, filter: true },
    { headerName: 'To Date', maxWidth: 120, field: 'tO_DATE', sortable: true, filter: true },
    { headerName: 'Leave Type', maxWidth: 150, field: 'leavE_TYPE', sortable: true, filter: true },
    { headerName: 'No.Of Days', maxWidth: 150, field: 'nO_OF_LEAVES', sortable: true, filter: true },
    { headerName: 'Status', maxWidth: 120, field: 'status', sortable: true, filter: true },
    {
      headerName: 'Action', maxWidth: 100, cellRenderer: 'buttonRenderer',

    }
  ];


  Application_ID: string;
  LeaveType_View: string;
  FromDate_VIEW: string;
  ToDate_VIEW: string;
  NoOfDays_VIEW: string;
  ReportingMgr_VIEW: string;
  ContactNo_VIEW: string;
  status_VIEW: string;
  Reason_VIEW: string;
  EmpName_VIEW: string;
  Designation_VIEW: string;
  LeaveDuration_VIEW: string;
  Encashment_VIEW: string;
  Approved_by: string;
  Designation: string;
  ApprovedDate_VIEW: string;
  currentdate = new Date();

  frameworkComponents: any;
  public components;
  showloader: boolean = true;

  employeeLeaveForm: FormGroup;
  employeeencashForm: FormGroup;

  consubmitted: boolean = true;
  encconsubmitted: boolean = true;

  encleavetypes: any = [];
  leavetypes: any = [];

  NgbDateStruct: any;


  MasterHistlist: any[];

  EMP_CODE: string;
  EMP_NAME: string;
  REPORTING_MGR: string;
  EMR_NUM: string;

  GridFill: any[];
  GridFillRowData: any[];

  gridApi: any = [];
  defaultColDef: any = [];
  gridColumnApi: any = [];

  currentyear: number;
  EmpLeaveDetails: any = [];

  get EmpLeaveCtrl() { return this.employeeLeaveForm.controls; }
  get EmpencashCtrl() { return this.employeeencashForm.controls; }

  constructor(private formBuilder: FormBuilder, private service: CommonServices, private datef: CustomDateParserFormatter,
    private datecon: NgbDatepickerConfig, private router: Router,) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.frameworkComponents = {
      buttonRenderer: HistorybuttonRendererComponent,

    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },

      actionrender: function (params) {

        var eSpan3 = document.createElement('div');
        if (params.data.status == "Pending") {

          eSpan3.innerHTML = '<u style="color:blue" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + params.value + '</u>' +
            '<div class="dropdown-menu more-option-menu" aria-labelledby="dropdownMenuButton">' +
            '<span class="dropdown-item text-center ViewClick"><i class="fa fa-eye"></i> View</span>' +
            '<span class="dropdown-item text-center CancelClick"><i class="fa fa-close"></i> Cancel</span>' +
            
            '</div>';

        }
        else {

          eSpan3.innerHTML = '<u style="color:blue" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + params.value + '</u>' +
            '<div class="dropdown-menu more-option-menu" aria-labelledby="dropdownMenuButton">' +
            '<span class="dropdown-item text-center ViewClick"><i class="fa fa-eye"></i> View</span>' +
            '</div>';
        }
        return eSpan3;

      },
    };



  }


  dataclicked(params) {
    //buttonRenderer
    if (params.colDef.cellRenderer == "actionrender") {
      if (params.event.path[0].classList.value == "dropdown-item text-center CancelClick" || params.event.path[0].classList.value == "fa fa-close") {
        this.EditEmpLeaveMaster(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center ViewClick" || params.event.path[0].classList.value == "fa fa-eye") {
        this.ViewEmpLeave(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center HistoryClick" || params.event.path[0].classList.value == "fa fa-history") {
        this.HistoryLeave(params);
      }
    }
    if (params.colDef.cellRenderer == "buttonRenderer") {
      this.HistoryLeave(params);
    }
  }
  ngOnInit(): void {

    this.showloader = false;

    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }


    this.currentyear = now.getFullYear();



    this.employeeLeaveForm = this.formBuilder.group({
      LeaveType: ['', Validators.required],
      LeaveDay: ['', Validators.required],
      FromDate: ['', Validators.required],
      ToDate: ['', Validators.required],
      NoOfdays: ['', Validators.required],
      RptManager: ['', Validators.required],
      Reason: ['', Validators.required],
      EmgNumber: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      RptManagerID: [''],
      leavebalance: ['']

    });

    this.employeeencashForm = this.formBuilder.group({
      encashLeaveType: ['', Validators.required],
      encNoOfdays: ['', Validators.required],
      encbalance: ['', Validators.required],
      encRptManager: ['', Validators.required],
      encReason: ['', Validators.required],
      encEmgNumber: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      encRptManagerID: ['']

    });


    this.fillGriddata();

    this.LoadLeaveTypes();

    this.LoadLeaveTypes_enc();

    this.LeaveDetails();


  }


  resetallforms() {

    //this.employeeLeaveForm.reset();

    this.EmpLeaveCtrl.LeaveDay.setValue("");
    this.EmpLeaveCtrl.LeaveType.setValue("");

    this.EmpLeaveCtrl.FromDate.setValue("");
    this.EmpLeaveCtrl.ToDate.setValue("");
    this.EmpLeaveCtrl.NoOfdays.setValue("");
    this.EmpLeaveCtrl.Reason.setValue("");



    this.consubmitted = false;
  }
  resetallforms_encah() {

    this.EmpencashCtrl.encashLeaveType.setValue("");

    this.EmpencashCtrl.encNoOfdays.setValue("0");
    this.EmpencashCtrl.encbalance.setValue("0");

    this.EmpencashCtrl.encReason.setValue("");



    this.encconsubmitted = false;
  }
  CloseEditModal() { this.editModal.hide(); }

  LoadLeaveTypes() {

    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = "0";

    this.showloader = true;
    //410
    this.service.postData(req, "EmpLeaveTypes_Get").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (data.Details[0] != null && data.Details.length > 0) {
          this.leavetypes = data.Details;
          if (data.Details[0].reportinG_OFFICER != null && data.Details[0].reportinG_OFFICER != "") {
            this.EMP_NAME = data.Details[0].emP_NAME;
            this.EmpLeaveCtrl.RptManager.setValue(data.Details[0].reportinG_OFFICER_NAME);
            this.EmpLeaveCtrl.EmgNumber.setValue(data.Details[0].emergencY_CONTACT_NO);
            this.EmpLeaveCtrl.RptManagerID.setValue(data.Details[0].reportinG_OFFICER);
          }
          else { Swal.fire('warning', "Reporting Manager Not Mapped..!", 'warning'); }
        }
        else { Swal.fire('warning', "Employee Type Not Mapped to Leave Type..! OR Reporting Manager Not Mapped..!", 'warning'); }

      }
    },
      error => console.log(error));
  }

  LoadLeaveTypes_enc() {

    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = "true";

    this.showloader = true;
    //410
    this.service.postData(req, "EmpLeaveTypes_Get").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (data.Details[0] != null && data.Details.length > 0) {
          this.encleavetypes = data.Details;
          if (data.Details[0].reportinG_OFFICER != null && data.Details[0].reportinG_OFFICER != "") {
            this.EMP_NAME = data.Details[0].emP_NAME;
            this.EmpencashCtrl.encRptManager.setValue(data.Details[0].reportinG_OFFICER_NAME);
            this.EmpencashCtrl.encEmgNumber.setValue(data.Details[0].emergencY_CONTACT_NO);
            this.EmpencashCtrl.encRptManagerID.setValue(data.Details[0].reportinG_OFFICER);
          }
          else { Swal.fire('warning', "Reporting Manager Not Mapped..!", 'warning'); }
        }
        else { Swal.fire('warning', "Employee Type Not Mapped to Leave Type..! OR Reporting Manager Not Mapped..!", 'warning'); }

      }
    },
      error => console.log(error));
  }

  EditEmpLeaveMaster(row: any) {
    const rowdata = row.data;

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to cancel the Application " + rowdata.applicatioN_ID + "!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {
        const req = new InputRequest();
        req.INPUT_01 = rowdata.applicatioN_ID;
        //req.INPUT_01 = "2021";
        req.INPUT_02 = "Cancelled";
        req.CALL_SOURCE = "WEB";
        req.USER_NAME = this.logUserName;

        this.showloader = true;

        this.service.postData(req, "EmpLeave_Cancel").subscribe(data => {
          this.showloader = false;

          if (data.StatusCode == "100") {
            let result = data.Details[0];
            if (result.rtN_ID === 1) {

              this.fillGriddata();
              this.editModal.hide();
              Swal.fire('success', "your Application has been Cancelled Successfully !!!", 'success');
            }
            else {
              Swal.fire('warning', result.statuS_TEXT, 'warning');
            }

          }
          else
            Swal.fire('warning', data.StatusMessage, 'warning');

        },
          error => console.log(error));



      }
    })
  }
  ViewEmpLeave(row: any) {
    this.showloader = true;
    const datarow = row.data;
    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = "0";
    req.INPUT_03 = datarow.applicatioN_ID;
    this.showloader = true;
    //408     
    this.service.postData(req, "GetEmpLeaves").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Application_ID = data.Details[0].applicatioN_ID;
        this.LeaveType_View = data.Details[0].leavE_TYPE;

        this.FromDate_VIEW = data.Details[0].froM_DATE;
        this.ToDate_VIEW = data.Details[0].tO_DATE;
        this.NoOfDays_VIEW = data.Details[0].nO_OF_LEAVES;
        this.ReportingMgr_VIEW = data.Details[0].reportinG_OFFICER;
        this.ContactNo_VIEW = data.Details[0].emergencY_CONTACT_NO;


        //this.LeaveDuration_VIEW = "FromDate : " + data.Details[0].froM_DATE + "<br>" + "To Date" + data.Details[0].tO_DATE;
        this.EmpName_VIEW = data.Details[0].emP_NAME;
        this.Approved_by = data.Details[0].approveD_BY;
        this.Designation = data.Details[0].designation;
        this.status_VIEW = data.Details[0].status;
        this.Reason_VIEW = data.Details[0].reason;
        this.ApprovedDate_VIEW = data.Details[0].approveD_DATE;
        this.Encashment_VIEW = data.Details[0].encashment;


        this.viewModal.show();

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));


  }
  HistoryLeave(row: any) { this.Historyrow(row); }

  HistoryAll() { this.history_All(); }
  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.GridFillRowData);

  }

  AddEmpLeave() {

    //this.EditEmpLeaveMaster.res();

    this.resetallforms();
    this.editModal.show();
  }

  AddEmpencashment() {

    //this.EditEmpLeaveMaster.res();

    this.resetallforms_encah();
    this.encashModal.show();
  }

  fillGriddata() {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = "0";
    req.INPUT_03 = "0";
    this.showloader = true;
    this.service.postData(req, "GetEmpLeaves").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.GridFill = data.Details;
        this.GridFillRowData = data.Details;
        this.gridApi.setRowData(this.GridFillRowData);


      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));


  }



  LeaveDetails() {
    const req = new InputRequest();

    req.INPUT_01 = this.logUserName;

    this.showloader = true;

    this.service.postData(req, "GetEmpLeaveDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.EmpLeaveDetails = data.Details;
        //this.LeaveDetailsempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        ///this.LeaveDetailsempModal.hide();
        this.EmpLeaveDetails = [];
        this.showloader = false;
      }
    },
      error => console.log(error));

  }



  history_All() {
    const req = new InputRequest();

    req.INPUT_02 = "EMP_LEAVES";
    req.INPUT_01 = this.logUserName;

    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }
  Historyrow(data): void {
    this.showloader = true;
    //const data = row.rowData;
    const req = new InputRequest();

    req.INPUT_02 = "EMP_LEAVES";
    req.INPUT_01 = this.logUserName;
    req.INPUT_03 = data.applicatioN_ID;


    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }


  SubmitEmpLeave() {
    this.consubmitted = true;

    let itemname: string;

    var leavetypename = this.leavetypes.find(item => item.leaveS_TYPE === this.EmpLeaveCtrl.LeaveType.value);

    // itemname = this.leavetypes.map(
    //   (elem) =>{ elem.iteM_NAME = this.EmpLeaveCtrl.LeaveType.value == elem.leaveS_TYPE
    // return elem});

    if (this.employeeLeaveForm.invalid) {
      return false;
    }


    if ((this.datef.format(this.EmpLeaveCtrl.FromDate.value) != this.datef.format(this.EmpLeaveCtrl.ToDate.value)) && (leavetypename.iteM_NAME).toUpperCase() == "OPTIONAL") {
      Swal.fire('warning', "Optional Holiday From Date and To Date Should be Same", 'warning');
      return false;
    }

    if (this.leaveDays <= 0) {
      Swal.fire('warning', "To Date Shoud be greater than  From Date", 'warning');
      return false;
    }

    if (this.EmpLeaveCtrl.leavebalance.value <= 0) {
      Swal.fire('warning', "Your Leave Balance is exceeded", 'warning');
      return false;
    }

    if (this.EmpLeaveCtrl.leavebalance.value < parseFloat(this.EmpLeaveCtrl.NoOfdays.value)) {
      Swal.fire('warning', "Your Leave Balance is exceeded", 'warning');
      return false;
    }


    if (this.EmpLeaveCtrl.NoOfdays.value <= 0) {
      Swal.fire('warning', "Error in Caluculating No.of Days...!", 'warning');
      return false;
    }


    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = this.EMP_NAME;
    req.INPUT_03 = this.EmpLeaveCtrl.LeaveType.value;
    req.INPUT_04 = this.EmpLeaveCtrl.LeaveDay.value;
    req.INPUT_05 = this.datef.format(this.EmpLeaveCtrl.FromDate.value);
    req.INPUT_06 = this.datef.format(this.EmpLeaveCtrl.ToDate.value);
    req.INPUT_07 = this.EmpLeaveCtrl.NoOfdays.value;
    req.INPUT_08 = this.EmpLeaveCtrl.RptManagerID.value;
    req.INPUT_09 = this.EmpLeaveCtrl.Reason.value;
    req.INPUT_10 = "Pending";
    req.INPUT_11 = this.EmpLeaveCtrl.EmgNumber.value;
    req.INPUT_12 = "false";

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;


    this.showloader = true;

    this.service.postData(req, "SaveEmpLeave").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          this.fillGriddata();

          this.editModal.hide();
          Swal.fire('success', "Employee Leave Application Saved Successfully !!!", 'success');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  getbalance(value) {


  }
  SubmitEncahment() {
    this.encconsubmitted = true;

    let itemname: string;

    if (this.employeeencashForm.invalid) {
      return false;
    }


    if (this.EmpencashCtrl.encbalance.value <= 0) {
      Swal.fire('warning', "your Leave Balance is exceeded", 'warning');
      return false;
    }

    if (this.EmpencashCtrl.encNoOfdays.value <= 0) {
      Swal.fire('warning', "your Leave Balance is exceeded", 'warning');
      return false;
    }


    if (this.EmpencashCtrl.encbalance.value > this.EmpencashCtrl.encNoOfdays.value) {
      Swal.fire('warning', "your Leave Balance is exceeded", 'warning');
      return false;
    }





    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = this.EMP_NAME;
    req.INPUT_03 = this.EmpencashCtrl.encashLeaveType.value;
    req.INPUT_04 = "Full Day";
    req.INPUT_05 = "";//this.datef.format(this.EmpLeaveCtrl.FromDate.value);
    req.INPUT_06 = "";//this.datef.format(this.EmpLeaveCtrl.ToDate.value);
    req.INPUT_07 = this.EmpencashCtrl.encNoOfdays.value;
    req.INPUT_08 = this.EmpencashCtrl.encRptManagerID.value;
    req.INPUT_09 = this.EmpencashCtrl.encReason.value;
    req.INPUT_10 = "Pending";
    req.INPUT_11 = this.EmpencashCtrl.encEmgNumber.value;
    req.INPUT_12 = "true";

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;


    this.showloader = true;

    this.service.postData(req, "SaveEmpLeave").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          this.fillGriddata();

          this.encashModal.hide();
          Swal.fire('success', "Employee Leave Encashment Saved Successfully !!!", 'success');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }


  LeaveTypeChange() {
    this.EmpLeaveCtrl.LeaveDay.setValue("");
    this.EmpLeaveCtrl.ToDate.setValue("");
    this.EmpLeaveCtrl.FromDate.setValue("");
    this.EmpLeaveCtrl.NoOfdays.setValue("");

    this.GetBalance("leave");

  }
  DayTypeChange() {
    if (this.EmpLeaveCtrl.LeaveType.value == "") {
      Swal.fire('warning', "Leave Type is required", 'warning');
      return false;
    }


    this.EmpLeaveCtrl.ToDate.setValue("");
    this.EmpLeaveCtrl.FromDate.setValue("");
    this.EmpLeaveCtrl.NoOfdays.setValue("");

  }
  FromDateChange() {
    if (this.EmpLeaveCtrl.LeaveType.value == "") {
      Swal.fire('warning', "Leave Type is required", 'warning');
      return false;
    }


    if (this.EmpLeaveCtrl.LeaveDay.value == "") {
      Swal.fire('warning', "Day Type is required", 'warning');
      return false;
    }

    this.EmpLeaveCtrl.ToDate.setValue("");
    this.EmpLeaveCtrl.NoOfdays.setValue("");


  }
  ToDateChange() {

    if (this.EmpLeaveCtrl.LeaveType.value == "") {
      Swal.fire('warning', "Leave Type is required", 'warning');
      return false;
    }


    if (this.EmpLeaveCtrl.LeaveDay.value == "") {
      Swal.fire('warning', "Day Type is required", 'warning');
      return false;
    }


    if (this.EmpLeaveCtrl.FromDate.value == null || this.EmpLeaveCtrl.FromDate.value == "") {
      Swal.fire('warning', "From Date is required", 'warning');
      return false;
    }

    if (this.EmpLeaveCtrl.ToDate.value == null || this.EmpLeaveCtrl.ToDate.value == "") {
      Swal.fire('warning', "To Date is required", 'warning');
      return false;
    }

    this.leaveDays = this.datef.NoofDays(this.EmpLeaveCtrl.FromDate.value, this.EmpLeaveCtrl.ToDate.value) + 1;

    if (this.leaveDays <= 0) {
      Swal.fire('warning', "To Date Shoud be greater than  From Date", 'warning');
      return false;
    }

    this.GetNoOfdays();

  }


  GetBalance(value) {

    const req = new InputRequest();
    if (value == "encashment") {
      req.INPUT_01 = this.EmpencashCtrl.encashLeaveType.value;

    }
    else {
      req.INPUT_01 = this.EmpLeaveCtrl.LeaveType.value;

    }
    req.CALL_SOURCE = "WEB";
    req.INPUT_02 = this.logUserName;

    req.DIRECTION_ID = "2";
    req.TYPEID = "423";

    this.service.postData(req, "SERVICE_EVENTS_GET").subscribe(data => {
      if (data.StatusCode == "100") {
        if (value == "encashment") {
          this.EmpencashCtrl.encbalance.setValue(data.Details[0].leaveS_BALANCE);

        }
        else { this.EmpLeaveCtrl.leavebalance.setValue(data.Details[0].leaveS_BALANCE); }
      }

    },
      error => console.log(error));
  }

  GetNoOfdays() {

    const req = new InputRequest();
    req.INPUT_01 = this.datef.format(this.EmpLeaveCtrl.FromDate.value);
    req.INPUT_02 = this.datef.format(this.EmpLeaveCtrl.ToDate.value);
    req.INPUT_04 = this.EmpLeaveCtrl.LeaveType.value;
    req.INPUT_03 = this.EmpLeaveCtrl.LeaveDay.value;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;

    this.showloader = true;

    this.service.postData(req, "GetNoOfDays").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.nO_OF_DAYS != null && result.nO_OF_DAYS != "" && result.nO_OF_DAYS > 0) {
          this.EmpLeaveCtrl.NoOfdays.setValue(result.nO_OF_DAYS.toString());
        }
        else {

          this.EmpLeaveCtrl.NoOfdays.setValue("");

          Swal.fire('warning', "Error in Get NoDays...!", 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  leaveDays = 0;
  onKeyUptoDate() {

    if (this.EmpLeaveCtrl.LeaveType.value == "") {
      Swal.fire('warning', "Leave Type is required", 'warning');
      return false;
    }


    if (this.EmpLeaveCtrl.LeaveDay.value == "") {
      Swal.fire('warning', "Day Type is required", 'warning');
      return false;
    }


    if (this.EmpLeaveCtrl.FromDate.value == null || this.EmpLeaveCtrl.FromDate.value == "") {//Swal.fire('warning', "From Date is required", 'warning');
      return false;
    }

    if (this.EmpLeaveCtrl.ToDate.value == null || this.EmpLeaveCtrl.ToDate.value == "") {//Swal.fire('warning', "To Date is required", 'warning');
      return false;
    }

    this.leaveDays = this.datef.NoofDays(this.EmpLeaveCtrl.FromDate.value, this.EmpLeaveCtrl.ToDate.value) + 1;

    if (this.leaveDays <= 0) {
      Swal.fire('warning', "To Date Shoud be greater than  From Date", 'warning');
      return false;
    }
    let dayscalu = this.EmpLeaveCtrl.LeaveDay.value == "Full Day" ? 1 : 2

    this.EmpLeaveCtrl.NoOfdays.setValue(((this.leaveDays) / dayscalu).toString());


  }
}


