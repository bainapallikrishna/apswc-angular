import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehousemasterComponent } from './warehousemaster.component';

describe('WarehousemasterComponent', () => {
  let component: WarehousemasterComponent;
  let fixture: ComponentFixture<WarehousemasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehousemasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehousemasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
