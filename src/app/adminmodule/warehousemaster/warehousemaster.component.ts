import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { AadharValidateService } from 'src/app/Services/aadhar-validate.service';
import { WHButtonRendererComponent } from 'src/app/custome-directives/whbutton-renderer.component';
import { AgGridAngular } from 'ag-grid-angular';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';
import { MustMatch } from 'src/app/custome-directives/must-match.validator';
import { DigiServices } from 'src/app/Services/Digilocker.services';




@Component({
  selector: 'app-warehousemaster',
  templateUrl: './warehousemaster.component.html',
  styleUrls: ['./warehousemaster.component.css']
})
export class WarehousemasterComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  public componenisPasswordChangedts;
  public myAngularxQrCode: string = null;
  @Output() public onUploadFinished = new EventEmitter();

  whkeyword: string = "warehousE_NAME";
  WHNameSelect(event): void {
    this.subf.Warehouse_Name.setValue("");
  }
  columnDefs = [
    { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer', floatingFilter: false },
    { headerName: 'Warehouse Name', maxWidth: 150, field: 'warehousE_NAME',cellRenderer: 'actionrender' },
    { headerName: 'Warehouse Address', maxWidth: 180, field: 'address' },
    { headerName: 'Regional Office', maxWidth: 120, field: 'region' },
    { headerName: 'Warehouse Type', maxWidth: 120, field: 'warehousE_TYPE' },
    { headerName: 'Total Storage Capacity', maxWidth: 150, field: 'totaL_STORAGE' },
    { headerName: 'Manager Contact Details', width: 180, field: 'contacT_DETAILS' },
    { headerName: 'Facilities Available', maxWidth: 150, field: 'facilitY_AVALIABLE' },
    { headerName: 'Status', maxWidth: 60, field: 'iS_ACTIVE' },
    {
      headerName: 'Action', width:80,cellRenderer: 'buttonRenderer', floatingFilter: false,
      // cellRendererParams: {
      //   editClick: this.EditWareHouse.bind(this),
      //   historyClick: this.HistoryWareHouse.bind(this),
      //   viewClick: this.ViewWareHouse.bind(this),
      // },
    }
  ];

  modeltitle: string = "Add";
  bindtext: string;

  bindtextmob: string;
  WH_Type: any;
  Region_Type: any;
  warehouses: any[];
  Facilities_seltd: string[];
  Facilities_append: string[];
  Facilites: any[];
  DistList: any[];

  whb_maitn_period_dd: any[];
  whb_maint_cmpny_dd: any[];
  Pincodedata:any[];
  RUList: any[]; mandalslist: any[]; villagelist: any[]; WH_TYPES: any[]; WH_R_Offices: any[]; wh_history: any[];
  District: any;
  RU: any;
  Muncipal_Mandal: any;
  Village_Town: any;
  Area: any;
  Door_no: any;
  Street_Name: any;
  Landmark: any;
  Pincode: any;
  Owner_Name: any;
  Owner_Mob_no: any;
  Warehouse_Id: any;
  Warehouse_Name: any;
  Capacity: any;
  ImagePath: any;
  Base64Image: any;
  SYSTEM_WH_CODE: any;
  Latitude: any;
  Longitude: any;
  IsWHActive: any;
  WH_inactive_rmks: any;
  isactive_field: any;
  HiredDate: any;
  Commencementdate: any;

  NgbDateStruct: any;
  preview_doc: any;
  preview_pohoto: any;
  seldocpath: any ;
  seldoctype: string = "";
  seldoccat: string = "";


  SectionID: any;
  code: any;
  Accesstoken: any;
  FilesDD: any;
  Docpath: any;
  Authstatus: any;
  UploadFilesDD: any;
  IssuedDocView: boolean = true;
  viewbtnname: any;
  uri: any;
  DigiDetails: any[];
  showDocpath: any;
  hrefdata: any;
  Details: any;
  name: any;
  dob: any;
  gender: any;
  preview: string = "";
  //seldocpath2: any;
  seldoctype2: string = "";
  //seldoccat2: string = "";


  latlngValpattern = "/^-?([0-8]?[0-9]|90)\.[0-9]{1,6},-?((1?[0-7]?|[0-9]?)[0-9]|180)\.[0-9]{1,6}$/";
  IFSCPattern = "^[A-Z]{4}0[A-Z0-9]{6}$";

  showloader: boolean = true;
  mob_uid_isvalid: Boolean = true;
  facilitydiv: boolean = true;
  doc_change: boolean = false;
  public isEdit: boolean = false;
  isactive_flag: boolean = false;
  hireddatediv: boolean = false;
  whaddcomple: boolean = false;
  WeighBridgecomple: boolean = false;
  public WHsubmitted: boolean = false;
  public whbsubmi: boolean = false
  response: { dbPath: '' };
  WHDOC: any;
  isTextBoxDisabled: any;
  WareHouseMasterForm: FormGroup;
  WeighbridgeForm: FormGroup;
  gridApi: any;
  gridColumnApi: any;
  public defaultColDef;
  public icons;
  frameworkComponents: any;
  public components;
  public progress: number;
  public message: string;

  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private uidservice: AadharValidateService,
    private datef: CustomDateParserFormatter,
    private EncrDecr: EncrDecrServiceService,
    private sanitizer: DomSanitizer,
    private digiservice: DigiServices,

  ) {


    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }


    this.frameworkComponents = {
      buttonRenderer: WHButtonRendererComponent,
    }
    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },


      actionrender: function (params) {

        var eSpan3 = document.createElement('div');      
        
          eSpan3.innerHTML = '<u style="color:blue" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + params.value + '</u>' +
          '<div class="dropdown-menu more-option-menu" aria-labelledby="dropdownMenuButton">' +
          '<span class="dropdown-item text-center editClick"><i class="fa fa-edit"></i> Edit</span>' +
          '<span class="dropdown-item text-center ViewClick"><i class="fa fa-eye"></i> View</span>' +       
          '<span class="dropdown-item text-center HistoryClick"><i class="fa fa-history"></i> History</span>' +
          '</div>';
         
          return eSpan3;
      },
    };
    this.icons = {
      filter: ' '
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText: true,
      autoHeight: true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    
  }


  @ViewChild('addModal') public addModal: ModalDirective;
  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('historyModal') public historyModal: ModalDirective;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('previewModal') public previewModal: ModalDirective;
  @ViewChild('DetailList') public DetailList: ModalDirective;
  @ViewChild('FilesList') public FilesList: ModalDirective;




  dataclicked(params) {
    //buttonRenderer
    if (params.colDef.cellRenderer == "actionrender") {
    if (params.event.path[0].classList.value == "dropdown-item text-center editClick" || params.event.path[0].classList.value == "fa fa-edit") {
        this.EditWareHouse(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center ViewClick" || params.event.path[0].classList.value == "fa fa-eye"){
        this.ViewWareHouse(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center HistoryClick" || params.event.path[0].classList.value == "fa fa-history"){
        this.HistoryWareHouse(params);
      }
    }
    if (params.colDef.cellRenderer == "buttonRenderer") {
      this.HistoryWareHouse(params);
      }
    
  }

  ngOnInit(): void {
    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }
    this.Facilities_seltd = new Array<string>();
    this.FacilityAvalb();
    this.LoadWareHousList();
    this.LoadWareHousTypes();
    this.Load_WH_Regional_offices();
    //this.LoadDistricts();
    this.ruralurbanlist();
    this.load_WB_cmpnyies();


    this.WareHouseMasterForm = this.formBuilder.group({

      WH_Type: [null, Validators.required],
      Region_Type: [null, Validators.required],
      District: [null, Validators.required],
      RU: [null, Validators.required],
      Muncipal_Mandal: [null, Validators.required],
      Village_Town: [null, Validators.required],
      Area: ['', Validators.required],
      Door_no: ['', Validators.required],
      Street_Name: ['', Validators.required],
      Landmark: ['', Validators.required],
      Pincode: ['', [Validators.required, Validators.minLength(6)]],
      Owner_Name: ['', Validators.required],
      Owner_Mob_no: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      HiredDate: [''],
      Warehouse_Id: ['', Validators.required],
      Warehouse_Name: ['', Validators.required],
      Capacity: ['', [Validators.required, Validators.minLength(3)]],
      ImagePath: [''],
      Base64Image: [''],
      SYSTEM_WH_CODE: [''],
      Latitude: ['', Validators.required],
      Longitude: ['', Validators.required],
      IsWHActive: [''],
      WH_inactive_rmks: [''],
      Commencementdate: ['', Validators.required],
      WHvaliddate: [''],
      WHDOC: [''],
      RegDoc: [''],
      //Facilities_seltd:['',Validators.required]
      AccLength: [],
      AccountHName: ['', Validators.required],
      AccountNumber: ['', Validators.required],
      ConfirmAccountNumber: ['', Validators.required],
      IFSCCode: ['', [Validators.required, Validators.minLength(11), Validators.pattern(this.IFSCPattern)]],
      BankName: ['', Validators.required],
      BranchName: ['', Validators.required],

    }
    , {
      validator: MustMatch('AccountNumber', 'ConfirmAccountNumber')
    }
    )

    this.WeighbridgeForm = this.formBuilder.group({
      WHBridgeDetails: new FormArray([
        this.formBuilder.group({
          whblicenseno: ['', Validators.required],
          whbmanfby: ['', Validators.required],
          whbcapacity: ['', Validators.required],
          whbcommencdate: ['', Validators.required],
          whb_maintnc_cmpny: [null, Validators.required],
          whb_maintnc_period: [null, Validators.required],

        })
      ])


    })


  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.warehouses);
    //this.gridApi.sizeColumnsToFit();
  }
  LoadWareHousList() {

    this.showloader = true;
    const req = new InputRequest();


    this.service.postData(req, "GetWH_List").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.warehouses = data.Details;
        this.gridApi.setRowData(this.warehouses);

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');




    },
      error => console.log(error));

  }
  LoadWareHousTypes() {

    this.service.getData("GetWH_Type").subscribe(data => {

      if (data.StatusCode == "100") {
        this.WH_TYPES = data.Details;

      } else
        Swal.fire('warning', data.StatusMessage, 'warning');



    },
      error => console.log(error));


  }

  Load_WH_Regional_offices() {

    this.service.getData("GetWH_Regionalofficers").subscribe(data => {

      if (data.StatusCode == "100") {
        this.WH_R_Offices = data.Details;

      } else
        Swal.fire('warning', data.StatusMessage, 'warning');



    },
      error => console.log(error));


  }

  FacilityAvalb() {
    this.Facilites = [
      { Facility_Name: 'Providing Space for Storage', isSelected: false },
      { Facility_Name: 'Disinfestation Service', isSelected: false },
      //{ Facility_Name: 'Pest Control Service',isSelected: false },
      { Facility_Name: 'Cold Storage', isSelected: false }

    ]
  }

  changewarehous(val) {
    if (val == "1" || val == "2") {
      this.bindtext = "Warehouse Manger";
      this.bindtextmob = "Warehouse Manger";
      this.subf.Owner_Mob_no.setValue("");
      this.subf.Owner_Name.setValue("");
      this.hireddatediv = false;
    }
    else {
      this.bindtext = "Owner";
      this.bindtextmob = "Owner";
      this.subf.Owner_Mob_no.setValue("");
      this.subf.Owner_Name.setValue("");
      this.hireddatediv = true;

    }

  }
  LoadDistricts() {

    this.DistList = [];
    this.mandalslist = [];
    this.villagelist = [];
    this.Facilities_seltd = [];
    this.FacilityAvalb();
    this.subf.RU.setValue(null);
    this.subf.Village_Town.setValue(null);
    this.subf.Muncipal_Mandal.setValue(null);
    this.subf.Area.setValue("")
    this.subf.Door_no.setValue("")
    this.subf.Street_Name.setValue("")
    this.subf.Landmark.setValue("")
    this.subf.Pincode.setValue("")
    this.subf.Owner_Name.setValue("")
    this.subf.Owner_Mob_no.setValue("")
    this.subf.Warehouse_Id.setValue("")
    this.subf.Capacity.setValue("")
    this.subf.ImagePath.setValue("")
    this.subf.Latitude.setValue("")
    this.subf.Longitude.setValue("")
    this.subf.Warehouse_Name.setValue("")
    const req = new InputRequest();
    req.INPUT_01 = this.subf.Region_Type.value;
    this.service.postData(req, "GetRegionDistricts").subscribe(data => {
      if (data.StatusCode == "100") {
        this.DistList = data.Details;
      }
    },
      error => console.log(error));

  }

  PincodeDetails() {
    if (this.subf.Pincode.value.length == "6") {
      if (!this.subf.Region_Type.value) {
        Swal.fire("info", "Please Select Regional Office", "info");
        this.subf.Pincode.setValue(null);
        return true;
      }
      this.DistList = [];
      this.RUList=[];
      this.subf.District.setValue(null);
      this.subf.RU.setValue(null);
      this.subf.Muncipal_Mandal.setValue(null);
      this.showloader = true;
      const req = new InputRequest();
      req.INPUT_01 = this.subf.Pincode.value;
      this.service.postData(req, "GetPincodeDetails").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.Pincodedata = data.Details;
          if (this.subf.Region_Type.value == data.Details[0].regioN_CODE) {
            if (data.Details.length == 1) {
              this.DistList = data.Details;
              this.RUList = data.Details;
              this.mandalslist = data.Details;
              this.subf.District.setValue(data.Details[0].districT_CODE);
              this.subf.RU.setValue(data.Details[0].id);
              this.subf.Muncipal_Mandal.setValue(data.Details[0].mmC_CODE);
              this.LoadVillages();

            }
            else
            {
              
              this.LoadDistricts();
              this.ruralurbanlist();
            }
          }
          else {
            Swal.fire("info", "Entered Pincode is Out Of Region Please Enter With in the Region Pincode !!!", "info");
            this.subf.Pincode.setValue(null);
            return true;

          }
        }
        else
        {
         
          this.LoadDistricts();
          this.ruralurbanlist();
        }


      },
        error => console.log(error));
    }


  }

  Districtchnge() {
    this.mandalslist = [];
    this.villagelist = [];
    this.subf.RU.setValue(null);
    this.subf.Village_Town.setValue(null);
    this.subf.Muncipal_Mandal.setValue(null);

  }
  ruralurbanlist() {

    this.service.getData("GetAreaTypes").subscribe(data => {
      if (data.StatusCode == "100") {
        this.RUList = data.Details;

      }

    })
  }

  mob_uid_validate(mobilephone) {
    if (mobilephone.length === 10)
      this.mob_uid_isvalid = this.uidservice.validatemob(this.subf.Owner_Mob_no.value);

  }

  Loadmandls() {
    this.mandalslist = [];
    this.villagelist = [];
    this.subf.Village_Town.setValue(null);
    this.subf.Muncipal_Mandal.setValue(null);
    if (this.subf.District.value && this.subf.RU.value) {
      this.showloader = true;
      const req = new InputRequest();
      req.INPUT_01 = this.subf.RU.value;
      req.INPUT_02 = this.subf.District.value;
      this.service.postData(req, "GetMandlas").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {

          this.mandalslist = data.Details;

        }

      },
        error => console.log(error));
    }

  }

  LoadVillages() {
    this.villagelist = [];
    this.subf.Village_Town.setValue(null);
    this.showloader = true;
    let obj: any = { "INPUT_02": this.subf.District.value, "INPUT_03": this.subf.Muncipal_Mandal.value }
    this.service.postData(obj, "GetVillages").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.villagelist = data.Details;
      }

    },
      error => console.log(error));
  }

  load_WB_cmpnyies() {

    this.showloader = true;
    let obj: any = { "INPUT_01": "WB_COMPANY" }
    this.service.postData(obj, "GetWeigh_Bridge_Details").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.whb_maint_cmpny_dd = data.Details;
      }

    },
      error => console.log(error));
  }



  load_WB_periods(val) {

    this.showloader = true;
    let obj: any = { "INPUT_01": "WB_PERIOD", "INPUT_02": val }
    this.service.postData(obj, "GetWeigh_Bridge_Details").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.whb_maitn_period_dd = data.Details;
      }

    },
      error => console.log(error));

  }

  // check box change
  oncheckboxchange(checkedname: string, e: any) {

    if (e.target.checked) {
      this.Facilities_seltd.push(checkedname);

      console.log(this.Facilities_seltd.toString());
      this.facilitydiv = true;

    }
    else {
      this.Facilities_seltd = this.Facilities_seltd.filter(m => m != checkedname);

      console.log(this.Facilities_seltd.toString());



    }


  }


  GetIFSCDetails() {
    if (this.subf.IFSCCode.value.length == 11) {
      this.showloader = true;
      var data = { 'INPUT_01': this.subf.IFSCCode.value };
      this.service.postData(data, "GetIFSCCodeDetails").subscribe(data => {
        this.showloader = false;

        if (data.StatusCode == "100") {
          let result = data.Details[0];

          this.subf.BankName.setValue(result.banK_NAME);
          this.subf.BranchName.setValue(result.branch);
          this.subf.AccLength.setValue(result.aC_LEANTH);

        }
        else
          Swal.fire('warning', data.StatusMessage, 'warning');

      },
        error => console.log(error));
    }

  }

  onSelectFile(event) {
    let url: string;
    if (event.target.files && event.target.files[0]) {
      let imagetype = event.target.files[0].type;
      let imagesize = event.target.files[0].size;

      if (imagetype != 'image/jpeg' && imagetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png files only', 'info');
        return false;
      }
      
      if (imagetype == 'image/jpeg' || imagetype == 'image/png')
        this.seldoctype = 'IMAGE';
     
      if (imagesize > 2097152) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      
      this.seldoccat = imagetype;
      const reader = new FileReader();
      reader.onload = () => {
        if(this.seldoctype == "PDF"){
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat);
          const blobUrl = URL.createObjectURL(blob);
          //this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
          this.preview_doc = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
          
        }
        else{
          this.seldocpath = reader.result as string;
          this.preview_pohoto = this.seldocpath;

        }
        
      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', "WarehouseDetails");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.doc_change = false;
            this.message = 'Upload success.'
            this.uploadFinished(event.body, "WH_PHOTO");
          }

        });


      // var reader = new FileReader();

      // reader.readAsDataURL(event.target.files[0]); // read file as data url

      // reader.onload = (event) => { // called once readAsDataURL is completed
      //   this.subf.Base64Image.setValue(event.target.result);
      // }
    }
  }

  uploadFile(event) {
    this.seldocpath = "";
    this.seldoctype = "";
    this.seldoctype2 = "";
    this.Authstatus=0;
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;

      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF') {
        Swal.fire('info', 'Please upload pdf files only', 'info');
        return false;
      }
      
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype = 'IMAGE';
      else
        this.seldoctype = 'PDF';

      if (filesize > 10615705) {
        Swal.fire('info', 'File size must be upto 10 MB', 'info');
        return false;
      }
      this.seldoccat = filetype;

      const reader = new FileReader();
      reader.onload = () => {
        if(this.seldoctype == "PDF"){
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat);
          const blobUrl = URL.createObjectURL(blob);
          this.preview_doc = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
          //this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else{
          this.seldocpath = reader.result as string;
         this.preview_pohoto = this.seldocpath;
        }
        
      }
     
     reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', "WarehouseDetails");

      this.service.UploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {

          if (event.type === HttpEventType.UploadProgress)
            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.doc_change = true;
            this.message = 'Upload success.';
            this.uploadFinished(event.body, "WH_DOC");


          }

        });


    }
  };

  public createImgPath = (serverPath: string) => {
    //return `http://uat.apswc.ap.gov.in/APSWCAPP/${serverPath}`;
    return `${this.service.filebaseUrl.replace("api/FilesUpload", "")}${serverPath}`;
  }

  public uploadFinished = (event, doctype) => {
    // this.response = event;
    //alert(this.response.dbPath)
    if (doctype == 'WH_PHOTO')
      this.subf.Base64Image.setValue(event.fullPath);
    else if (doctype == 'WH_DOC')
      this.subf.WHDOC.setValue(event.fullPath);

  }

  AddWareHouse() {
    this.resetallforms();
    this.modeltitle = "Add";
    this.isEdit = false;
    this.subf.District.setValue(null);
    this.subf.RU.setValue(null);
    this.subf.WH_Type.setValue(null);
    this.subf.Region_Type.setValue(null);
    this.subf.Village_Town.setValue(null);
    this.subf.Muncipal_Mandal.setValue(null);

    this.subf.SYSTEM_WH_CODE.setValue("");
    this.f.push(this.formBuilder.group({
      whblicenseno: null,
      whbmanfby: null,
      whbcapacity: null,
      whbcommencdate: null,
      whb_maintnc_cmpny: null,
      whb_maintnc_period: null,


    }));


    //this.AddWHB();
    this.addModal.show();

  }

  EditWareHouse(whlist) {

    //this.logUserrole="103";
    if (this.logUserrole == "102" || this.logUserrole == "101") {
      this.isTextBoxDisabled = null;
      this.isEdit = false
    }
    else {
      this.isTextBoxDisabled = true;
      this.isEdit = true
    }



    const data = whlist.data;
    this.resetallforms();
    this.modeltitle = "Edit";
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = data.warehousE_CODE;

    this.service.postData(req, "GetWH_View").subscribe(data => {

      if (data.StatusCode == "100") {

        this.fillWarehouse_details(data.Details[0]);
        this.fillWB_details(data.Details);

        this.editModal.show();
      }
      this.showloader = false;

    },
      error => console.log(error));


  }

  fillWarehouse_details(whdata: any) {

    this.subf.SYSTEM_WH_CODE.setValue(whdata.warehousE_CODE)
    this.subf.WH_Type.setValue(whdata.warehousE_TYPE_CODE.toString())
    this.changewarehous(whdata.warehousE_TYPE_CODE);
    this.subf.Region_Type.setValue(whdata.regioN_CODE)
    this.LoadDistricts();
    this.subf.District.setValue(whdata.districT_CODE == null ? null : whdata.districT_CODE)
    this.subf.RU.setValue(whdata.ruraL_URBAN == null ? null : whdata.ruraL_URBAN)
    this.Loadmandls();
    this.subf.Muncipal_Mandal.setValue(whdata.mmC_CODE == null ? null : whdata.mmC_CODE)
    this.LoadVillages();
    this.subf.Village_Town.setValue(whdata.warD_VILLAGE_CODE == null ? null : whdata.warD_VILLAGE_CODE)
    this.subf.Area.setValue(whdata.area)
    this.subf.Door_no.setValue(whdata.dooR_NO)
    this.subf.Street_Name.setValue(whdata.streeT_NAME)
    this.subf.Landmark.setValue(whdata.landmark)
    this.subf.Pincode.setValue(whdata.pincode)
    this.subf.Owner_Name.setValue(whdata.owneR_NAME)
    this.subf.Owner_Mob_no.setValue(whdata.owneR_MOBILE_NO)
    this.subf.HiredDate.setValue(this.datef.parse((whdata.hireD_ON ? whdata.hireD_ON : "").replace("T00:00:00", "")))
    this.subf.Warehouse_Id.setValue(whdata.warehousE_ID)
    this.subf.Capacity.setValue(whdata.totaL_STORAGE)
    this.subf.Base64Image.setValue(whdata.warehousE_PHOTO)
    whdata.warehousE_PHOTO ? (this.decryptFile(whdata.warehousE_PHOTO, "WH_PHOTO")) : "";
    
    
    this.subf.Latitude.setValue(whdata.latitude)
    this.subf.Longitude.setValue(whdata.longitude)
    this.subf.Warehouse_Name.setValue(whdata.warehousE_NAME)
    //this.WHDOC = whdata.documenT_UPLOAD;    
    //this.response = { dbPath: whdata.documenT_UPLOAD };
    this.subf.WHDOC.setValue(whdata.documenT_UPLOAD)
    whdata.documenT_UPLOAD ? (this.decryptFile(whdata.documenT_UPLOAD, "WH_DOC")) : "";
    
    this.subf.Commencementdate.setValue(this.datef.parse((whdata.commencE_DATE ? whdata.commencE_DATE : "").replace("T00:00:00", "")))
    this.subf.WHvaliddate.setValue(this.datef.parse((whdata.wH_VALIDTY_DATE ? whdata.wH_VALIDTY_DATE : "").replace("T00:00:00", "")))
    if (whdata.facilitY_AVALIABLE != null && whdata.facilitY_AVALIABLE != "") {
      this.Facilities_append = whdata.facilitY_AVALIABLE.split(",");
      this.Facilites = this.Facilites.map(
        (elem) => {
          elem.isSelected = this.Facilities_append.indexOf(elem.Facility_Name) != -1 ? true : false;
          return elem
        });
    }
    else {
      this.Facilities_append = [];
    }
    this.subf.IsWHActive.setValue(whdata.iS_ACTIVE == "Active" ? "1" : "0");
    this.isactive_field = (whdata.iS_ACTIVE == "Active" ? "1" : "0");


    this.subf.IFSCCode.setValue(whdata.ifsC_CODE);
    whdata.ifsC_CODE?this.GetIFSCDetails():"";
    this.subf.AccountHName.setValue(whdata.accounT_HOLDER_NAME);
    this.subf.BankName.setValue(whdata.banK_NAME);
    this.subf.BranchName.setValue(whdata.banK_BRANCH);
    this.subf.AccountNumber.setValue(whdata.accounT_NUMBER);
    this.subf.ConfirmAccountNumber.setValue(whdata.accounT_NUMBER);



  }

  fillWB_details(whdata: any) {

    for (let i = 0; i < whdata.length; i++) {

      this.f.push(this.formBuilder.group({
        whblicenseno: ['', Validators.required],
        whbmanfby: ['', Validators.required],
        whbcapacity: ['', Validators.required],
        whbcommencdate: ['', Validators.required],
        whb_maintnc_cmpny: [null, Validators.required],
        whb_maintnc_period: [null, Validators.required],

      }));


      this.f.controls[i].patchValue({
        whblicenseno: whdata[i].wB_LICENCE,
        whbmanfby: whdata[i].wB_MANUFACTURER,
        whbcapacity: whdata[i].wB_CAPACITY,
        whbcommencdate: whdata[i].wB_COMMENCE_DATE != null ? this.datef.parse(whdata[i].wB_COMMENCE_DATE.replace("T00:00:00", "")) : "",
        whb_maintnc_cmpny: whdata[i].wB_MAINTENANCE_ID,
        whb_maintnc_period: (whdata[i].wB_MAINTENANCE_PERIOD),

      });


    }

  }

  isactive() {

    if (this.subf.IsWHActive.value != this.isactive_field)
      this.isactive_flag = true;
    else
      this.isactive_flag = false;



  }

  HistoryWareHouse(whlist) {
    this.showloader = true;
    const req = new InputRequest();
    const data = whlist.data;
    if (whlist != "all") {
      this.isEdit = false;
      req.INPUT_01 = data.warehousE_CODE;
    }
    else {
      this.isEdit = true;
      req.INPUT_02 = "WAREHOUSE";
    }
    this.service.postData(req, "GetWH_History").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.wh_history = data.Details;
        for (let i = 0; i < this.wh_history.length; i++) {
          let colname = this.wh_history[i].columN_NAME;
          let oldvalue = this.wh_history[i].olD_VALUE;
          let newvalue = this.wh_history[i].neW_VALUE;
          if ((colname == "WAREHOUSE_PHOTO" || colname == "DOCUMENT_UPLOAD") && oldvalue) {
            this.decryptHistoryFile(oldvalue, i, 'oldvalue');
          }
          if ((colname == "WAREHOUSE_PHOTO" || colname == "DOCUMENT_UPLOAD") && newvalue) {
            this.decryptHistoryFile(newvalue, i, 'newvalue');
          }
        }
        console.log(this.wh_history);
        this.historyModal.show();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }

  decryptHistoryFile(filepath, i, ftype) {
    const coltype: string = ftype;
    const indx = i;
    if (coltype == 'oldvalue')
      this.wh_history[indx].olD_VALUE = "";
    else
      this.wh_history[indx].neW_VALUE = "";

    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (coltype == 'oldvalue') {

            if (filepath.split(".")[1] == "pdf") {
              const blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
              const blobUrl = URL.createObjectURL(blob);
              this.wh_history[indx].olD_VALUE = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
            }
            else {
              this.wh_history[indx].olD_VALUE = data.docBase64;
            }
          }
          else {
            if (filepath.split(".")[1] == "pdf") {
              const blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
              const blobUrl = URL.createObjectURL(blob);
              this.wh_history[indx].neW_VALUE = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
            }
            else {
              this.wh_history[indx].neW_VALUE = data.docBase64;
            }

          }
        })
    }
  };

  ViewWareHouse(row) {
    const data = row.data;
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = data.warehousE_CODE;
    this.service.postData(req, "GetWH_View").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.viewWarehouse_details(data.Details[0]);
        this.viewModal.show();
      }

    },
      error => console.log(error));


  }

  viewWarehouse_details(whdata: any) {
    var encrypted = this.EncrDecr.set(whdata.warehousE_CODE);
    this.myAngularxQrCode = encrypted;
    this.subf.WH_Type.setValue(whdata.warehousE_TYPE)
    this.subf.Region_Type.setValue(whdata.region)
    this.subf.District.setValue(whdata.districT_NAME)
    this.subf.RU.setValue(whdata.ruraL_URBAN)
    this.subf.Muncipal_Mandal.setValue(whdata.mmC_NAME)
    this.subf.Village_Town.setValue(whdata.warD_VILLAGE_NAME)
    this.subf.Area.setValue(whdata.area)
    this.subf.Door_no.setValue(whdata.dooR_NO)
    this.subf.Street_Name.setValue(whdata.streeT_NAME)
    this.subf.Landmark.setValue(whdata.landmark)
    this.subf.Pincode.setValue(whdata.pincode)
    this.subf.Owner_Name.setValue(whdata.owneR_NAME)
    this.subf.Owner_Mob_no.setValue(whdata.owneR_MOBILE_NO)
    this.subf.HiredDate.setValue(whdata.hireD_ON)
    this.subf.Commencementdate.setValue(whdata.commencE_DATE)
    this.subf.WHvaliddate.setValue(whdata.wH_VALIDTY_DATE)
    //this.WHDOC = whdata.documenT_UPLOAD;
    whdata.documenT_UPLOAD ? (this.decryptFile(whdata.documenT_UPLOAD, "WH_DOC")) : "";
    this.subf.Warehouse_Id.setValue(whdata.warehousE_ID)
    this.subf.Capacity.setValue(whdata.totaL_STORAGE)
    //this.subf.Base64Image.setValue(whdata.warehousE_PHOTO)
    whdata.warehousE_PHOTO ? (this.decryptFile(whdata.warehousE_PHOTO, "WH_PHOTO")) : "";
    this.subf.Latitude.setValue(whdata.latitude)
    this.subf.Longitude.setValue(whdata.longitude)
    this.subf.Warehouse_Name.setValue(whdata.warehousE_NAME)
    
    
    this.subf.IFSCCode.setValue(whdata.ifsC_CODE);    
    this.subf.AccountHName.setValue(whdata.accounT_HOLDER_NAME);
    this.subf.BankName.setValue(whdata.banK_NAME);
    this.subf.BranchName.setValue(whdata.banK_BRANCH);
    this.subf.AccountNumber.setValue(whdata.accounT_NUMBER);
    this.subf.ConfirmAccountNumber.setValue(whdata.accounT_NUMBER);

  }



  resetallforms() {
    this.WHsubmitted = false;
    this.whbsubmi = false;
    this.whaddcomple = false;
    this.WeighBridgecomple = false;
    this.isactive_flag = false;
    this.Facilities_seltd = [];
    this.Facilities_append = [];
    
    this.mandalslist = [];
    this.villagelist = [];
    this.preview_doc=null;
    this.preview_pohoto=null;
    this.Authstatus=0;
    this.Docpath="";
    this.seldoctype="";
    this.seldoctype2 = "";


    this.progress = 0;
    this.message = "";
    this.doc_change = false;

    this.facilitydiv = true;
    this.hireddatediv = false;
    this.FacilityAvalb();
    this.WareHouseMasterForm.reset();
    this.WeighbridgeForm.reset();
    this.f.reset();
    this.f.clear();
  }

  get subf() { return this.WareHouseMasterForm.controls }
  get whbf() { return this.WeighbridgeForm.controls }

  get f() { return this.whbf.WHBridgeDetails as FormArray; }

  SubmitWHMDetails() {
    this.WHsubmitted = true;

    // stop here if form is invalid
    if (this.WareHouseMasterForm.invalid) {
      return false;
    }

    if (this.hireddatediv == true) {
      if (!this.subf.HiredDate.value) {

        Swal.fire('warning', "Please Enter Hired On Date", 'warning');
        return false;

      }

    }
    // if (!this.subf.Commencementdate.value) {

    //   Swal.fire('warning', "Please select Year of Commencement On Date", 'warning');
    //   return false;

    // }

    if (this.subf.WHvaliddate.value) {
      if (this.datef.GreaterDate(this.subf.Commencementdate.value, this.subf.WHvaliddate.value)) {
        Swal.fire('warning', "Warehouse Validity Date Shoud be greater than Commencementdate Date", 'warning');
        return false;
      }
    }
    if (this.subf.AccountNumber.value.length != this.subf.AccLength.value) {
      Swal.fire('warning', this.subf.AccountHName.value + " Account Number Should be " + this.subf.AccLength.value + " digits", 'warning');
      return false;
    }

    if (this.Facilities_seltd.length === [].length) {
      this.facilitydiv = false;
      return false;
    }





    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.subf.WH_Type.value;
    req.INPUT_02 = this.subf.Region_Type.value;
    req.INPUT_03 = this.subf.District.value;
    //req.INPUT_04=(this.DistList.find(x=>x.id===this.subf.District.value));
    req.INPUT_04 = this.subf.RU.value;
    req.INPUT_05 = this.subf.Muncipal_Mandal.value;
    req.INPUT_06 = this.subf.Village_Town.value;
    req.INPUT_07 = this.subf.Area.value;
    req.INPUT_08 = this.subf.Door_no.value;
    req.INPUT_09 = this.subf.Street_Name.value;
    req.INPUT_10 = this.subf.Landmark.value;
    req.INPUT_11 = this.subf.Pincode.value;
    req.INPUT_12 = this.subf.Owner_Name.value;
    req.INPUT_13 = this.subf.Owner_Mob_no.value;
    req.INPUT_14 = this.subf.Warehouse_Id.value;
    req.INPUT_15 = this.subf.Capacity.value;
    req.INPUT_35 = this.subf.Base64Image.value;
    req.INPUT_17 = this.subf.Latitude.value;
    req.INPUT_18 = this.subf.Longitude.value;
    req.INPUT_19 = this.subf.Warehouse_Name.value;
    req.INPUT_20 = this.Facilities_seltd.toString();
    req.USER_NAME = this.logUserName;
    req.INPUT_21 = this.logUserrole;
    req.INPUT_22 = this.datef.format(this.subf.HiredDate.value);
    req.INPUT_23 = this.subf.WHDOC.value;//this.response == undefined ? null : this.createImgPath(this.response.dbPath);
    req.INPUT_24 = this.datef.format(this.subf.Commencementdate.value);
    req.INPUT_25 = this.datef.format(this.subf.WHvaliddate.value);
    
    req.INPUT_26 = this.subf.AccountHName.value;
    req.INPUT_27 = this.subf.AccountNumber.value;
    req.INPUT_28 = this.subf.IFSCCode.value;
    req.INPUT_29 = this.subf.BankName.value;
    req.INPUT_30 = this.subf.BranchName.value;
    req.CALL_SOURCE = "Web";

    this.service.postData(req, "SaveWareHouseDetails").subscribe(data => {

      this.showloader = false;
      if (data.StatusCode == "100") {

        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.subf.SYSTEM_WH_CODE.setValue(result.warehousE_CODE);
          Swal.fire('success', "Warehouse Master Details Saved Successfully !!!", 'success');

          this.whaddcomple = true;

        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
    },
      error => console.log(error));





  }
  CloseAddModal() {
    this.resetallforms();
    this.addModal.hide();
    this.LoadWareHousList();
  }
  CloseeditModal() {
    this.resetallforms();
    this.editModal.hide();
    this.LoadWareHousList();
  }

  decryptFile(filepath, Doctype) {
    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (Doctype == "WH_DOC") {

            const blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
            const blobUrl = URL.createObjectURL(blob);
            this.preview_doc=this.sanitizer.bypassSecurityTrustUrl(blobUrl);
            //this.subf.WHDOC.setValue(data.docBase64);

          }
          else {
            this.preview_pohoto=data.docBase64;
            //this.subf.Base64Image.setValue(data.docBase64)
            //this.subf.ImagePath.setValue(data.docBase64)
          }

        },
          error => {
            console.log(error);
            this.subf.WHDOC.setValue("");
            this.subf.Base64Image.setValue("");
            this.preview_pohoto="";
            this.preview_doc="";

          });
    }
    else
      this.subf.WHDOC.setValue("");
    this.subf.Base64Image.setValue("");
  }

  Editcheckboxchange(checkedname: string, e: any, index: any) {
    if (e.target.checked) {
      this.Facilities_append.push(checkedname);

      console.log(this.Facilities_append.toString());
      this.facilitydiv = true;

    }
    else {

      //this.Facilities_append.splice(index,1);
      this.Facilities_append = this.Facilities_append.filter(m => m != checkedname);
      console.log(this.Facilities_append.toString());



    }
  }

  UpdateWHMDetails() {

    if (this.logUserrole == "102" || this.logUserrole == "101") {

      this.WHsubmitted = true;
      // stop here if form is invalid
      if (this.WareHouseMasterForm.invalid) {
        return false;
      }

      if (this.hireddatediv == true) {
        if (!this.subf.HiredDate.value) {
          Swal.fire('warning', "Please Enter Hired On Date", 'warning');
          return false;

        }

      }
      // if (!this.subf.Commencementdate.value) {

      //   Swal.fire('warning', "Please select Year of Commencement On Date", 'warning');
      //   return false;

      // }
      if (this.subf.WHvaliddate.value) {
        if (this.datef.GreaterDate(this.subf.Commencementdate.value, this.subf.WHvaliddate.value)) {
          Swal.fire('warning', "Warehouse Validity Date Shoud be greater than Commencementdate Date", 'warning');
          return false;
        }
      }
      
    if (this.subf.AccountNumber.value.length != this.subf.AccLength.value) {
      Swal.fire('warning', this.subf.AccountHName.value + " Account Number Should be " + this.subf.AccLength.value + " digits", 'warning');
      return false;
    }


      if (this.Facilities_append.length === [].length) {
        this.facilitydiv = false;
        return false;
      }
      if (this.subf.IsWHActive.value && this.isactive_flag == true) {
        if (!this.subf.WH_inactive_rmks.value) {
          Swal.fire('warning', "Please Enter Remarks", 'warning');
          return false;
        }
      }



      this.showloader = true;
      const req = new InputRequest();
      req.INPUT_01 = this.subf.SYSTEM_WH_CODE.value;
      req.INPUT_02 = this.subf.WH_Type.value;
      req.INPUT_03 = this.subf.Region_Type.value;
      req.INPUT_04 = this.subf.District.value;
      //req.INPUT_04=(this.DistList.find(x=>x.id===this.subf.District.value));
      req.INPUT_05 = this.subf.RU.value;
      req.INPUT_06 = this.subf.Muncipal_Mandal.value;
      req.INPUT_07 = this.subf.Village_Town.value;
      req.INPUT_08 = this.subf.Area.value;
      req.INPUT_09 = this.subf.Door_no.value;
      req.INPUT_10 = this.subf.Street_Name.value;
      req.INPUT_11 = this.subf.Landmark.value;
      req.INPUT_12 = this.subf.Pincode.value;
      req.INPUT_13 = this.subf.Owner_Name.value;
      req.INPUT_14 = this.subf.Owner_Mob_no.value;
      req.INPUT_15 = this.subf.Warehouse_Id.value;
      req.INPUT_16 = this.subf.Capacity.value;
      req.INPUT_35 = this.subf.Base64Image.value;
      req.INPUT_18 = this.subf.Latitude.value;
      req.INPUT_19 = this.subf.Longitude.value;
      req.INPUT_20 = this.subf.Warehouse_Name.value;
      req.INPUT_21 = this.Facilities_append.toString();
      req.INPUT_22 = this.subf.IsWHActive.value;
      req.INPUT_23 = this.subf.WH_inactive_rmks.value;
      req.USER_NAME = this.logUserName;
      req.INPUT_24 = this.logUserrole;
      req.INPUT_25 = this.datef.format(this.subf.HiredDate.value);
      req.CALL_SOURCE = "Web";
      req.INPUT_26 = this.subf.WHDOC.value;//this.doc_change == true ? this.createImgPath(this.response.dbPath) : this.response.dbPath;
      // if(this.Authstatus==0){
      //   req.INPUT_26 = this.subf.WHDOC.value;
      // }
      // if(this.Authstatus==1){
      //   req.INPUT_26=this.Docpath;
    
      // }
      req.INPUT_27 = this.datef.format(this.subf.Commencementdate.value);
      req.INPUT_28 = this.datef.format(this.subf.WHvaliddate.value);

      req.INPUT_29 = this.subf.AccountHName.value;
      req.INPUT_30 = this.subf.AccountNumber.value;
      req.INPUT_31 = this.subf.IFSCCode.value;
      req.INPUT_32 = this.subf.BankName.value;
      req.INPUT_33 = this.subf.BranchName.value;






      this.service.postData(req, "updateWareHouseDetails_all").subscribe(data => {

        this.showloader = false;
        if (data.StatusCode == "100") {

          let result = data.Details[0];
          if (result.rtN_ID === 1) {

            Swal.fire('success', "Warehouse Master Details Updated Successfully !!!", 'success');
            this.whaddcomple = true;



          }
          else
            Swal.fire('warning',"Error Occured while Update WareHouse Details", 'warning');
          ;
        }
        else
          Swal.fire('warning', "Error Occured while Update WareHouse Details", 'warning');
      },
        error => console.log(error));



    }
    else {

      if (!this.subf.Owner_Name.value) {
        Swal.fire('warning', "Please Enter Owner/Warehouse Manager Name", 'warning');
        return false;
      }
      if (!this.subf.Owner_Mob_no.value) {
        Swal.fire('warning', "Please Enter Owner/Warehouse Manager Mobile No.", 'warning');
        return false;
      }
      if (!this.subf.HiredDate.value) {
        Swal.fire('warning', "Please Enter Hired On Date", 'warning');
        return false;
      }


      if (!this.subf.Capacity.value) {
        Swal.fire('warning', "Please Enter Storage Capacity", 'warning');
        return false;
      }
      if (!this.subf.Commencementdate.value) {

        Swal.fire('warning', "Please select Year of Commencement On Date", 'warning');
        return false;

      }
      if (!this.subf.WHvaliddate.value && this.subf.WH_Type.value != "1") {

        Swal.fire('warning', "Please select Warehouse Validity Date", 'warning');
        return false;

      }
      if (this.subf.WHvaliddate.value) {
        if (this.datef.GreaterDate(this.subf.Commencementdate.value, this.subf.WHvaliddate.value)) {
          Swal.fire('warning', "Warehouse Validity Date Shoud be greater than Commencementdate Date", 'warning');
          return false;
        }
      }

      
    if (this.subf.AccountNumber.value.length != this.subf.AccLength.value) {
      Swal.fire('warning', this.subf.AccountHName.value + " Account Number Should be " + this.subf.AccLength.value + " digits", 'warning');
      return false;
    }
      // if (!this.subf.Base64Image.value) {
      //   if (!this.subf.ImagePath.value) {
      //     Swal.fire('warning', "Please Upload Warehouse Photo", 'warning');
      //     return false;
      //   }
      // }

      if (this.Facilities_append.length === [].length) {
        this.facilitydiv = false;
        return false;
      }
      if (!this.subf.IsWHActive.value) {
        Swal.fire('warning', "Please Select Active/In-active button", 'warning');
        return false;
      }
      if (this.subf.IsWHActive.value && this.isactive_flag == true) {
        if (!this.subf.WH_inactive_rmks.value) {
          Swal.fire('warning', "Please Enter Remarks", 'warning');
          return false;
        }
      }

      this.showloader = true;
      const req = new InputRequest();
      req.INPUT_01 = this.subf.SYSTEM_WH_CODE.value;
      req.INPUT_35 = this.subf.Base64Image.value;
      req.INPUT_03 = this.subf.Capacity.value;
      req.INPUT_04 = this.Facilities_append.toString();
      req.INPUT_05 = this.subf.Owner_Name.value;
      req.INPUT_06 = this.subf.Owner_Mob_no.value;
      req.INPUT_07 = this.subf.IsWHActive.value;
      req.INPUT_08 = this.subf.WH_inactive_rmks.value;
      req.USER_NAME = this.logUserName;
      req.INPUT_09 = this.logUserrole;
      req.INPUT_10 = this.datef.format(this.subf.HiredDate.value)
      req.CALL_SOURCE = "Web";
      req.INPUT_11 = this.subf.WHDOC.value;//this.doc_change == true ? this.createImgPath(this.response.dbPath) : this.response.dbPath;
      // if(this.Authstatus==0){
      //   req.INPUT_11 = this.subf.WHDOC.value;
      // }
      // if(this.Authstatus==1){
      //   req.INPUT_11=this.Docpath;
    
      // }
     
      req.INPUT_12 = this.datef.format(this.subf.Commencementdate.value);
      req.INPUT_13 = this.datef.format(this.subf.WHvaliddate.value);

      req.INPUT_14 = this.subf.AccountHName.value;
      req.INPUT_15 = this.subf.AccountNumber.value;
      req.INPUT_16 = this.subf.IFSCCode.value;
      req.INPUT_17 = this.subf.BankName.value;
      req.INPUT_18 = this.subf.BranchName.value;

      this.service.postData(req, "updateWareHouseDetails").subscribe(data => {

        this.showloader = false;
        if (data.StatusCode == "100") {

          let result = data.Details[0];
          if (result.rtN_ID === 1) {

            Swal.fire('success', "Warehouse Master Details Updated Successfully !!!", 'success');
            this.whaddcomple = true;

          }
          else
            Swal.fire('warning', result.statuS_TEXT, 'warning');
          ;
        }
        else
          Swal.fire('warning', data.StatusMessage, 'warning');
      },
        error => console.log(error));

    }
  }

  SubmitWHBDetails() {
    this.whbsubmi = true;

    if (!this.subf.SYSTEM_WH_CODE.value) {
      Swal.fire('warning', "Please submit Warehouse Details section first", 'warning');
      return false;
    }
    // stop here if form is invalid
    if (this.WeighbridgeForm.invalid) {
      return false;
    }


    this.showloader = true;


    let whblicenclist: string[] = [];
    let whbcmpnylist: string[] = [];
    let whbperiodlist: string[] = [];
    let whbvaluelist: string[] = [];
    let whbcmmncedatelist: string[] = [];
    let whbmanfbylist: string[] = [];
    let whbcapacitylist: string[] = [];

    for (let i = 0; i < this.f.length; i++) {

      whblicenclist.push(this.f.value[i].whblicenseno);
      whbmanfbylist.push(this.f.value[i].whbmanfby);
      whbcapacitylist.push(this.f.value[i].whbcapacity);
      whbcmmncedatelist.push(this.datef.format(this.f.value[i].whbcommencdate));
      whbcmpnylist.push(this.f.value[i].whb_maintnc_cmpny);
      whbperiodlist.push(this.f.value[i].whb_maintnc_period);



    }


    const req = new InputRequest();
    req.INPUT_01 = this.subf.SYSTEM_WH_CODE.value;
    req.INPUT_02 = whblicenclist.join(',');
    req.INPUT_03 = whbcmpnylist.join(',');
    req.INPUT_04 = whbperiodlist.join(',');
    req.INPUT_05 = whbcapacitylist.join(',');
    req.INPUT_06 = whbcmmncedatelist.join(',');
    req.INPUT_07 = whbmanfbylist.join(',');
    req.USER_NAME = this.logUserName;
    req.INPUT_08 = this.logUserrole;
    req.CALL_SOURCE = "Web";

    this.service.postData(req, "SaveWBDetails").subscribe(data => {

      this.showloader = false;
      if (data.StatusCode == "100") {

        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Weigh Bridge Details Saved Successfully !!!", 'success');

          this.resetallforms();
          this.editModal.hide();
          this.addModal.hide();
          this.LoadWareHousList();

        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
    },
      error => console.log(error));








  }
  AddWHB() {
    this.f.push(this.formBuilder.group({
      whblicenseno: ['', Validators.required],
      whbmanfby: ['', Validators.required],
      whbcapacity: ['', Validators.required],
      whbcommencdate: ['', Validators.required],
      whb_maintnc_cmpny: [null, Validators.required],
      whb_maintnc_period: [null, Validators.required],


    }));

  }

  RemoveWHB(index) {
    this.f.removeAt(index);
  }

  viewPDF(input): void {
    //let pdfWindow = window.open('', '_blank')
    //pdfWindow.document.write(`<iframe class="e2e-iframe-trusted-src" width='100%' height='100%' src='${encodeURI(input)}'></iframe>`)
    // pdfWindow.document.write(
    //   `<iframe class="e2e-iframe-trusted-src" width='100%' height='100%' src='data:application/pdf;base64, " +
    //   ${encodeURI(input)} + "'></iframe>`
    //)

    var b = this.service.s_sd(input.split(",")[1], 'application/pdf');
    var l = document.createElement('a');
    l.href = window.URL.createObjectURL(b);
    l.download = "Warehouse_Document" + ".pdf";
    document.body.appendChild(l);
    l.click();
    document.body.removeChild(l);
  }

  Docpriview(val) {
   // this.preview_doc = this.sanitizer.bypassSecurityTrustResourceUrl(val);
    this.previewModal.show();

  }



  //// DigiLocker code
  CallDigilock(val){

    val=="WHPhoto"?this.seldoctype=val:this.seldoctype2=val;

   
    if(!this.FilesDD && !this.UploadFilesDD){
  this.showloader=true;
  var pagename = "WarehouseDetails";
  //this.digiservice.Digilock(pagename,(function(a) {
    this.digiservice.Digilock (pagename , (a,b) => {
        this.Accesstoken=b;
        this.FilesDD = a.UserFiles.items;
        if(a.UserUploadFiles)
        this.UploadFilesDD = a.UserUploadFiles.items;
        this.FilesList.show();
        this.showloader=false;
    });  
  }
  else{
    this.FilesList.show();
  }
}

ShowFiles(val,mimer,num,name){
  this.showloader=true;
  this.digiservice.ShowFiles (val,mimer,num,name,this.Accesstoken , (a,b) => {

    this.Docpath=a;
    this.seldoctype=="WHPhoto"?this.subf.Base64Image.setValue(this.Docpath):this.subf.WHDOC.setValue(this.Docpath)
    this.decryptFile1(this.Docpath);
    this.uri=b;
    this.Authstatus="1";
    this.viewbtnname=name.substring(0,5);
    this.FilesList.hide();
    this.showloader=false;
});  
}


decryptFile1(filepath) {
  if (filepath) {
    this.showloader = true;
    this.service.decryptFile(filepath, "DecryptFile")
      .subscribe(data => {
        let blob : any;
        console.log(data.docBase64);

        if(data.docBase64.includes('application/pdf'))
        {
         blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
        
        }
        else if(data.docBase64.includes('image/png'))
        {
           blob = this.service.s_sd((data.docBase64).split(',')[1], "image/png");
           
        }
        else if(data.docBase64.includes('image/jpg') || data.docBase64.includes('image/jpeg') )
        {
           blob = this.service.s_sd((data.docBase64).split(',')[1], "image/jpg");
        }
        const blobUrl = URL.createObjectURL(blob);  
        this.hrefdata=this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        this.showloader = false;

      },
        error => {
          console.log(error);
          this.showDocpath = "";
          this.showloader = false;
        });
  }
  else
  this.showDocpath = "";
}

GetDetails(uri){
  this.showloader=true;
  this.digiservice.GetDetails (uri ,this.Accesstoken, (a) => {
    console.log(a);
    if(a.Status == "Success")
    {
    
    //this.Details=a.CertIssuerData.Certificate.IssuedTo.Person;
if(this.viewbtnname == "Aadha")
{
  this.name=a.CertIssuerData.KycRes.UidData.Poi.name;
  this.gender=a.CertIssuerData.KycRes.UidData.Poi.gender;
  this.dob=a.CertIssuerData.KycRes.UidData.Poi.dob;
}
else{
this.name=a.CertIssuerData.Certificate.IssuedTo.Person.name;
this.gender=a.CertIssuerData.Certificate.IssuedTo.Person.gender;
this.dob=a.CertIssuerData.Certificate.IssuedTo.Person.dob;
}


   
    this.showloader=false;
    this.DetailList.show();
    
    }
    else{
      this.showloader=false;
    }
});  
}



  ViewDoc(){
    // window.open(this.showDocpath, '_blank');
    // const link = 'data:image/jpeg;base64,'+this.showDocpath;
    // var newWindow = window.open();
    // newWindow.document.write('<img src="' + link + '" />');
    //var splitted = this.showDocpath.split(",", 10);
    //const blob = b64toBlob(this.showDocpath, splitted[0]);
    var path=this.showDocpath.split(',')
    var b = this.service.s_sd(path[1], 'application/pdf');
        var l = document.createElement('a');
        l.href = window.URL.createObjectURL(b);
        l.download = "Document"+".pdf";
        document.body.appendChild(l);
        l.click();
        document.body.removeChild(l);
  }
  
   

  DocDivClick(val){
    this.IssuedDocView=val;
  }

  hideFilesList(){
    this.FilesList.hide();
  }

  hideDetailList(){
    this.DetailList.hide();
  
  }


}
