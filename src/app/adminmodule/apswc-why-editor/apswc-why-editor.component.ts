import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Editor, Toolbar, Validators } from 'ngx-editor';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-apswc-why-editor',
  templateUrl: './apswc-why-editor.component.html',
  styleUrls: ['./apswc-why-editor.component.css']
})
export class ApswcWhyEditorComponent implements OnInit {




  editor1: Editor;

  toolbar1: Toolbar = [
    ["bold", "italic"],
    ["underline", "strike"],
    ["code", "blockquote"],
    ["ordered_list", "bullet_list"],
    [{ heading: ["h1", "h2", "h3", "h4", "h5", "h6"] }],
    ["link", "image"],
    ["text_color", "background_color"],
    ["align_left", "align_center", "align_right", "align_justify"]
  ];

  formedit = true;
  formwhy: FormGroup;
  apswcwhy: string;
  divapswcwhy: string;
  constructor(private http: HttpClient, private service: CommonServices) { }

  ngOnInit(): void {
    this.editor1 = new Editor();
    this.formwhy = new FormGroup({
      apswcwhy: new FormControl("", Validators.required())

    });
    this.getcontentdata();
  }

  getcontentdata() {
    this.service.getData("GetHomePageConent").subscribe(data => {
      if (data.StatusCode == "100") {
        this.divapswcwhy = data.Details[0]["whY_APSWC"];

        if (!this.divapswcwhy) {
          this.formedit = false;
        }
        else {
          this.formedit = true;
        }
      }
      else {
        Swal.fire('error', data.statusmessage, 'error');
      }
    });
  }
  apswcedit() {
    this.formedit = false;
  }
  onSubmit() {
    if (!this.formwhy.value.apswcwhy) {
      Swal.fire('info', 'Please Enter the our objective', 'info');
      return;
    }
    if (this.formwhy.value.apswcwhy.length < 150) {
      Swal.fire('info', 'Please Enter our objective minimum 150 characters', 'info');
      return;
    }
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "UPD_WHY";
    req.INPUT_01 = this.formwhy.value.apswcwhy;
    this.service.postData(req, "HomePageConentInsert").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'info');

        this.getcontentdata();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });
  }

}
