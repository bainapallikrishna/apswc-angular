import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApswcWhyEditorComponent } from './apswc-why-editor.component';

describe('ApswcWhyEditorComponent', () => {
  let component: ApswcWhyEditorComponent;
  let fixture: ComponentFixture<ApswcWhyEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApswcWhyEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApswcWhyEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
