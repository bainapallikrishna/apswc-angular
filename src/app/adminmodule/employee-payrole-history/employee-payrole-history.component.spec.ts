import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeePayroleHistoryComponent } from './employee-payrole-history.component';

describe('EmployeePayroleHistoryComponent', () => {
  let component: EmployeePayroleHistoryComponent;
  let fixture: ComponentFixture<EmployeePayroleHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeePayroleHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeePayroleHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
