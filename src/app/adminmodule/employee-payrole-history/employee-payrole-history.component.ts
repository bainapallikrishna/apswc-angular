
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import { EmployeeSalaryInputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { AgGridAngular } from 'ag-grid-angular';
import { HttpEventType } from '@angular/common/http';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import {EMPVIEWSALBUTTONRendererComponent} from 'src/app/custome-directives/Empviewsalbutton-renderer.component';
@Component({
  selector: 'app-employee-payrole-history',
  templateUrl: './employee-payrole-history.component.html',
  styleUrls: ['./employee-payrole-history.component.css']
})
export class EmployeePayroleHistoryComponent implements OnInit {

  logUserrole:string = sessionStorage.getItem("logUserrole");
  isPasswordChanged:string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode:string = sessionStorage.getItem("logUserCode");
  gridColumnApi: any = [];
  tokens: any = [];
  gridApi: any = [];
  emptype="";
  year="";
  histMonth="";
  frameworkComponents: any;
  public components;
  showloader: boolean = false;
  emptypes:any;
  months="";
  itemtype="";
  empview:any;
  employeecpde="";
  basic:any;da:any;
  ca:any;
  istable:boolean=false;
  cca:any;
  comP_BAL:any;
  comP_INST:any;
  cps:any;
  eL_AMT: any
  emP_CODE:any;
  emP_NAME: any;
  emP_TYPE:any;
  gis:any;
  hbA_BAL:any;
  hbA_BANK:any;
  hbA_INST: any;
  hbA_R_BAL: any;
  hbA_R_IORI:any;
  hra:any;
  iS_ACTIVE: any;  iS_WEB_MOBILE:any;
  inserteD_BY: any;
  inserteD_ON:any;                   
  ir:any;
  it: any;
  iteM_NAME:any;
  lic:any;
  mR_BAL:any;  mR_IORI: any;
  m_ALL: any;
  misC_DED:any;
  misC_EAR: any;
  neT_PAY: any;
  pF_BAL:any;
  pF_IORI: any;
  p_TAX: any;
  paY_MONTH:any;
  paY_YEAR: any;
  pf: any;
  pli:any;
  pp:any;
  pr:any;
  sP_PAY:any;
  toT_DED:any;
  toT_EARN: any;
  vH_BAL:any;
  vH_IORI:any;
  vpf:any;
  w_ALL:any;
  
  @ViewChild('RegularviewModal') public RegularviewModal: ModalDirective;
  @ViewChild('DeputationviewModal') public DeputationviewModal: ModalDirective;
  @ViewChild('ContarctviewModal') public ContarctviewModal: ModalDirective;
  constructor(private formBuilder: FormBuilder, private service: CommonServices, private router: Router,private datef: CustomDateParserFormatter) { 
    
    
  if (!this.logUserrole || this.isPasswordChanged == "0") 
  {
    this.router.navigate(['/Login']);
    return;
  }
    
    this.frameworkComponents = {

      buttonRenderer: EMPVIEWSALBUTTONRendererComponent
      
    }
    this.components={

      rowIdRenderer:function(params){
    
       return ''+(params.rowIndex+1);
      }
    }
  }

  ngOnInit(): void {
  }
  columnDefs = [
    { headerName: '#', cellRenderer: 'rowIdRenderer' },

    { headerName: 'Employee Code', field: 'emP_CODE', sortable: true, filter: true },
    { headerName: 'Employee Name', field: 'emP_NAME', sortable: true, filter: true },
    { headerName: 'Employee Type',  field: 'iteM_NAME', sortable: true, filter: true },
    { headerName: 'Designation',  field: 'designation', sortable: true, filter: true },

    { headerName: 'Basic Salary',field: 'basic', sortable: true, filter: true },
    { headerName: 'Total Earnings',  field: 'toT_EARN', sortable: true, filter: true },
    { headerName: 'Total Deductions',  field: 'toT_DED', sortable: true, filter: true },
    { headerName: 'Net Pay',  field: 'neT_PAY', sortable: true, filter: true },
  {
      headerName: 'Action', cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        viewClick: this.Viewemployee.bind(this),
       
      },
    }
    
  ];
  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
  }
  Viewemployee(row) 
      {
        

        this.basic= "";
        this.da="";
        
        this.ca="";
        this.cca="";
        this.comP_BAL="";
        this.comP_INST="";
        this.cps="";
       this.eL_AMT="";
      
        this.gis="";
       this. hbA_BAL="";
        this.hbA_BANK="";
        this.hbA_INST="";
        this.hbA_R_BAL="";
        this.hbA_R_IORI="";
        this.hra="";
        this.iS_ACTIVE="";
        this.ir="";
        this.it="";
        this.iteM_NAME="";
        this.lic="";
        this.mR_BAL="";
        this.mR_IORI="";
        this.m_ALL="";
        this.misC_DED="";
        this.misC_EAR="";
        this.neT_PAY="";
        this.pF_BAL="";
        this.pF_IORI="";
        this.p_TAX="";
        this.paY_MONTH="";
        this.paY_YEAR="";
        this.pf="";
        this.pli="";
        this.pp="";
        this.pr="";
        this.sP_PAY="";
        this.toT_DED="";
        this.toT_EARN="";
        this.vH_BAL="";
        this.vH_IORI="";
        this.vpf="";
        this.w_ALL="";
        
  const data = row.rowData;
  this.itemtype= data.iteM_NAME;
  this.employeecpde=data.emP_CODE;
  if(this.itemtype=="Regular")
  {
this.RegularviewModal.show();
this.viewemphistory();
  }
  if(this.itemtype=="Outsourcing"){
    this.DeputationviewModal.show();
    this.viewemphistory();
  }
  if(this.itemtype=="Contract")
  {
   this.ContarctviewModal.show();   
   this.viewemphistory();
  }

   
}   


viewemphistory()
  {
    
   this.showloader= true;  
   const req = new InputRequest();
   req.INPUT_01=this.year;
   req.INPUT_02=this.histMonth;
   req.INPUT_03=this.employeecpde;
   this.service.postData(req, "GetEmployeeviewdata").subscribe(data => {
     if (data.StatusCode == "100") {
       this.empview= data.Details;
       this.showloader= false;  
      this.basic= this.empview[0].basic;
      this.da=this.empview[0].da;
      
      this.ca=this.empview[0].ca;
      this.cca=this.empview[0].cca;
      this.comP_BAL=this.empview[0].comP_BAL;
      this.comP_INST=this.empview[0].comP_INST;
      this.cps=this.empview[0].cps;
     this.eL_AMT=this.empview[0].eL_AMT;
    
      this.gis=this.empview[0].gis;
     this. hbA_BAL=this.empview[0].hbA_BAL;
      this.hbA_BANK=this.empview[0].hbA_BANK;
      this.hbA_INST=this.empview[0].hbA_INST;
      this.hbA_R_BAL=this.empview[0].hbA_R_BAL;
      this.hbA_R_IORI=this.empview[0].hbA_R_IORI;
      this.hra=this.empview[0].hra;
      this.iS_ACTIVE=this.empview[0].iS_ACTIVE;
      this.ir=this.empview[0].ir;
      this.it=this.empview[0].it;
      this.iteM_NAME=this.empview[0].iteM_NAME;
      this.lic=this.empview[0].lic;
      this.mR_BAL=this.empview[0].mR_BAL;
      this.mR_IORI=this.empview[0].mR_IORI;
      this.m_ALL=this.empview[0].m_ALL;
      this.misC_DED=this.empview[0].misC_DED;
      this.misC_EAR=this.empview[0].misC_EAR;
      this.neT_PAY=this.empview[0].neT_PAY;
      this.pF_BAL=this.empview[0].pF_BAL;
      this.pF_IORI=this.empview[0].pF_IORI;
      this.p_TAX=this.empview[0].p_TAX;
      this.paY_MONTH=this.empview[0].paY_MONTH;
      this.paY_YEAR=this.empview[0].paY_YEAR;
      this.pf=this.empview[0].pf;
      this.pli=this.empview[0].pli;
      this.pp=this.empview[0].pp;
      this.pr=this.empview[0].pr;
      this.sP_PAY=this.empview[0].sP_PAY;
      this.toT_DED=this.empview[0].toT_DED;
      this.toT_EARN=this.empview[0].toT_EARN;
      this.vH_BAL=this.empview[0].vH_BAL;
      this.vH_IORI=this.empview[0].vH_IORI;
      this.vpf=this.empview[0].vpf;
      this.w_ALL=this.empview[0].w_ALL;
      

     }
     else{
       this.showloader= false;  
       
     Swal.fire("info",data.StatusMessage,"info");
     }

   },
     error => console.log(error));

 }


CloseViewModal(){
  this.RegularviewModal.hide();
  this.DeputationviewModal.hide();
  this.ContarctviewModal.hide();
}


BindData(params) {
  this.gridApi = params.api;
  this.gridColumnApi = params.columnApi;
}
GetDetails() 
{
 
  this.showloader = true;
  const req=new EmployeeSalaryInputRequest;
  if(this.year==""||this.year==null||this.year==undefined){
    Swal.fire("info","Please Select Year","info");
    this.showloader=false;
  return false;
  }
  if(this.histMonth==""||this.histMonth==null||this.histMonth==undefined){
    Swal.fire("info","Please Select Month","info");
    this.showloader=false;
    return false;
  }
req.INPUT_01=this.year;
req.INPUT_02=this.histMonth;
  this.service.postData(req, "GetEmployeetypesData").subscribe(data => {
    this.showloader = false;
    if (data.StatusCode == "100") {
      this.istable=true;
      this.tokens = data.Details;
      this.gridApi.setRowData(this.tokens);
    }
    else {
      this.istable=false;
     Swal.fire("info",data.StatusMessage,"info");
      this.gridApi.setRowData(this.tokens);
      
    }
  },
    error => console.log(error));
}

Yearchange()
{
  this.showloader= true;  

  const req = new EmployeeSalaryInputRequest();
  req.INPUT_01 = this.year;
  this.service.postData(req, "GetMonths").subscribe(data => {
    if (data.StatusCode == "100") {
      this.months = data.Details;
      
      this.showloader= false;  

    }
    else{
      this.showloader= false;  
      
    Swal.fire("info",data.StatusMessage,"info");
    }

  },
    error => console.log(error));


}

}
