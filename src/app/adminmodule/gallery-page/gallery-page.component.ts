import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User, Gallery } from 'src/app/Interfaces/user';
import { from } from 'rxjs';
import Swal from 'sweetalert2';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-gallery-page',
  templateUrl: './gallery-page.component.html',
  styleUrls: ['./gallery-page.component.css']
})
export class GalleryPageComponent implements OnInit {
  public progress: number;
  public message: string;
  public response: { dbPath: '' };
  public isCreate: boolean;
  public name: string;
  public description: string;
  public selectedWarehousename: any;
  public gallery: Gallery;
  GalleryForm: FormGroup;
  Base64Image: string;
  public users: Gallery[] = [];
  galleryhidden = true;
  gallerylist: any = [];
  preview: string = "";
  seldocpath: any;
  seldoctype: string = "";
  seldoccat: string = "";

  constructor(private http: HttpClient, private formBuilder: FormBuilder, private service: CommonServices, private sanitizer: DomSanitizer) { }

  @ViewChild('previewModal') public previewModal: ModalDirective;

  ngOnInit(): void {
    this.isCreate = true;

    this.getGalleryImages();
    this.GalleryForm = this.formBuilder.group({

      name: ['', Validators.required],
      description: ['', Validators.required],
      Base64Image: ['']
    });
  }
  get gall() { return this.GalleryForm.controls; }
  AddGallery() {
    this.galleryhidden = false;
  }

  onSelectFile(event, ptype: string) {
    let url: string;
    let Phototype = ptype;
    this.seldocpath = "";
    this.seldoctype = "";
    if (event.target.files && event.target.files[0]) {
      let imagetype = event.target.files[0].type;
      let imagesize = event.target.files[0].size;

      if (imagetype != 'image/jpeg' && imagetype != 'image/png') {
        Swal.fire('info', 'Please Upload jpeg,jpg,png files only', 'info');
        return false;
      }

      if (imagesize > 2097152) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      this.seldoccat = imagetype;
      if (imagetype == 'image/jpeg' || imagetype == 'image/png')
        this.seldoctype = 'IMAGE';
      else
        this.seldoctype = 'PDF';


      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        if (Phototype == 'photo')
          this.gall.Base64Image.setValue(event.target.result);
        if (this.seldoctype == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath = reader.result as string;
          this.preview = this.seldocpath;
        }
      }

      reader.readAsDataURL(<File>event.target.files[0])
    }
  }

  getGalleryImages() {
    this.service.getData("GetGalleryImages").subscribe(data => {
      if (data.StatusCode == "100") {
        this.gallerylist = data.Details;
        console.log(this.gallerylist);
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });
  }
  onSubmit() {
    if (!this.GalleryForm.value.name) {
      Swal.fire("info", "Please Enter Name", "info");
      return;
    }
    if (!this.GalleryForm.value.description) {
      Swal.fire("info", "Please Enter Description", "info");
      return;
    }
    if (!this.GalleryForm.value.Base64Image) {
      Swal.fire("info", "Please Upload Images", "info");
      return;
    }

    const req = new InputRequest();
    req.DIRECTION_ID = "2";
    req.TYPEID = "201";
    req.INPUT_01 = this.GalleryForm.value.name;
    req.INPUT_02 = this.GalleryForm.value.description;
    req.INPUT_03 = "1";
    req.INPUT_04 = "Gallery";
    req.INPUT_35 = this.GalleryForm.value.Base64Image;
    //
    this.service.postData(req, "GalleryRegistration").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'info');

        //this.Getnewsmessage();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });
  }

  editGallery(gallitem: any) {
    const req = new InputRequest();
    req.DIRECTION_ID = "2";
    req.TYPEID = "203";
    req.INPUT_01 = gallitem.iteM_ID;
    req.INPUT_02 = "0";
    //
    this.service.postData(req, "GalleryRegistration").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'info');

        this.getGalleryImages();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });
  }

  public returnToCreate = () => {
    this.isCreate = true;
    this.selectedWarehousename = '';
    this.description = '';
  }
  private getInspectionlist = () => {
    //this.http.get('https://localhost:44310/api/APSWC/InsecpctionList')
    this.http.get('https://apswc.ap.gov.in/api/APSWC/InsecpctionList')
      .subscribe(res => {
        this.users = res as Gallery[];
        console.log(this.users);
      });
  }
  public uploadFinished = (event) => {
    this.response = event;
  }

  public createImgPath = (serverPath: string) => {
    //return `https://localhost:44310/${serverPath}`;
    return `https://apswc.ap.gov.in/${serverPath}`;
  }

  Docpriview(val) {

    // const blob = this.service.s_sd((val).split(',')[1], this.seldoccat);
    // const blobUrl = URL.createObjectURL(blob);
    // this.preview = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
    //this.preview = this.sanitizer.bypassSecurityTrustUrl(val);
    //this.preview = this.preview.changingThisBreaksApplicationSecurity;
    this.previewModal.show();

  }
}
