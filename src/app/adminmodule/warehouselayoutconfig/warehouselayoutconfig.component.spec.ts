import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouselayoutconfigComponent } from './warehouselayoutconfig.component';

describe('WarehouselayoutconfigComponent', () => {
  let component: WarehouselayoutconfigComponent;
  let fixture: ComponentFixture<WarehouselayoutconfigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouselayoutconfigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouselayoutconfigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
