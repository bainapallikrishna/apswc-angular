import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApswcOurvisionComponent } from './apswc-ourvision.component';

describe('ApswcOurvisionComponent', () => {
  let component: ApswcOurvisionComponent;
  let fixture: ComponentFixture<ApswcOurvisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApswcOurvisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApswcOurvisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
