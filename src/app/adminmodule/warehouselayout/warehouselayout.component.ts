import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as $ from "jquery";
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Alphabets, InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-warehouselayout',
  templateUrl: './warehouselayout.component.html',
  styleUrls: ['./warehouselayout.component.css']
})
export class WarehouselayoutComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  workLocationName = sessionStorage.getItem("logUserworkLocationName");

  currWarehouseID: string = this.workLocationCode;
  showloader: boolean = true;
  isSubmit: boolean = false;
  isLayout:boolean = false;
  isWarehouseMap:boolean=false;
  isactive_flag: boolean = false;
  isdiv: boolean = false;  
   

  JASection:boolean=false;
  WHMSection:boolean=false;
  RMSection:boolean=false;

  isWHMStatus:boolean=false;
  isRMStatus:boolean=false;
  isJAStatus:boolean=false;

  WM_rejectstatus:boolean=false;
  RM_rejectstatus:boolean=false;

  WH_Manager_Name:any;
  RM_Name:any;


  LayoutForm: FormGroup;
  usersForm: FormGroup;
  rejectForm:FormGroup;
  RMForm:FormGroup;

  godownslist: any = [];
  layoutdata:any=[];
  WareshouseByrm:any[];
  distgodownlist:any=[];

  @ViewChild('addconfigmodal') public addconfigmodal: ModalDirective;

  constructor(private service: CommonServices, private router: Router, private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }

    this.LayoutForm = this.formBuilder.group({
      GCount: ['', Validators.required],
      godowns: this.formBuilder.array([])
    });

    this.rejectForm = this.formBuilder.group({
      reject_rmks: ['', Validators.required],
      
    });
    this.RMForm = this.formBuilder.group({
      Whname: [null],
      
    });

    // this.LayoutForm = this.formBuilder.group({
    //   GCount: ['', Validators.required],
    //   godowns: this.formBuilder.array([
    //     this.formBuilder.group({
    //       CCount: ['', Validators.required],
    //       compartments: this.formBuilder.array([
    //         this.formBuilder.group({
    //           SCount: ['', Validators.required],
    //         })
    //       ])
    //     })
    //   ])
    // })

  }

  ngOnInit(): void {
     this.showloader = false;
    
    if(this.logUserrole=='105')
    {
      this.isdiv=true;      
      this.LoadWhbyRM();
      
    }
    else
    {
      this.LoadLayoutData();
    }
     
  }

  get layout() { return this.LayoutForm.controls; }
  get g() { return this.layout.godowns as FormArray; }

  SaveCofiguration() {
    this.isSubmit = true;


    // stop here if form is invalid
    if (this.LayoutForm.invalid) {
      return false;
    }

    let goudownids: any = [];
    let goudownnames: any = [];
    let compartmentids: any = [];
    let compartmentnames: any = [];
    let stacksids: any = [];
    let stacknames: any = [];
    let nogodows: any = [];
    let nocomps: any = [];
    let nostacks: any = [];

    this.showloader = true;

    for (let i = 0; i < this.layout.GCount.value; i++) {
      for (let j = 0; j < this.g.value[i].CCount; j++) {
        for (let k = 0; k < this.g.value[i].compartments[j].SCount; k++) {

          goudownids.push(this.currWarehouseID + "G" + (i + 1));
          goudownnames.push("Godown - " + (i + 1));

          compartmentids.push(this.currWarehouseID + "G" + (i + 1) + "C" + (j + 1));
          compartmentnames.push("Compartment - " + (i + 1) + Alphabets[j]);
          nocomps.push(this.g.value[i].CCount);

          stacksids.push(this.currWarehouseID + "G" + (i + 1) + "C" + (j + 1) + "S" + (k + 1));
          stacknames.push("" + (k + 1));
          nostacks.push(this.g.value[i].compartments[j].SCount)
        }
      }
    }

    const req = new InputRequest();
    req.INPUT_01 = this.currWarehouseID;
    req.INPUT_02 = this.layout.GCount.value;
    req.INPUT_03 = goudownids.join(',');
    req.INPUT_04 = goudownnames.join(',');
    req.INPUT_05 = nocomps.join(',');
    req.INPUT_06 = compartmentids.join(',');
    req.INPUT_07 = compartmentnames.join(',');
    req.INPUT_08 = nostacks.join(',');
    req.INPUT_09 = stacksids.join(',');
    req.INPUT_10 = stacknames.join(',');
    req.INPUT_11 = this.logUsercode;
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    console.log(req);

    this.service.postData(req, "SaveLayoutConfiguration").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Warehouse Layout Configuration Saved Successfully !!!", 'success');
        this.LoadLayoutData();
        this.addconfigmodal.hide();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


    // this.layout.godowns.controls[0].controls.compartments.value[0].SCount
  }

  LoadLayoutData(){
    this.showloader = true;
    this.isactive_flag = false;
    const req = new InputRequest();
    req.INPUT_01 = this.logUserrole=="105"?this.srmf.Whname.value: this.currWarehouseID;
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";
    console.log(req);

    this.service.postData(req, "GetLayoutConfiguration").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {

        if (data.Details.length > 1) {
          this.RM_Name="";
          this.WH_Manager_Name="";
          this.isLayout = false;
          this.isWarehouseMap = true;
          this.layoutdata = data.Details;
          this.distgodownlist = this.layoutdata.filter(
            (thing, i, arr) => arr.findIndex(t => t.godowN_NAME === thing.godowN_NAME) === i
          );
          this.distgodownlist.sort(this.sortByProperty('godowN_ID'));
          this.LoadSections(data.Details, this.logUserrole)
        }
        else if(data.Details.length == 1) {
         
          if (this.logUserrole == "106") {
            if (this.workLocationCode == 'null') {
              this.isWarehouseMap = false;
            }
            else {
              this.isWarehouseMap = false;
              this.isJAStatus = false;
              this.isLayout = false;
              this.JASection = true;
              
              this.WM_rejectstatus=data.Details[0].wM_STATUS==2?true:false;
              this.RM_rejectstatus=data.Details[0].rM_STATUS==2?true:false;

            }
          }
          else if (this.logUserrole == "104") {
            this.WHMSection = true;
            this.isJAStatus = false;
            this.isWHMStatus = false;
            this.isWarehouseMap = false;
            
           this.WM_rejectstatus=data.Details[0].wM_STATUS==2?true:false;
           this.RM_rejectstatus=data.Details[0].rM_STATUS==2?true:false;


          }
          else if (this.logUserrole == "105") {
            this.RMSection = true;
            this.isJAStatus = false;
            this.isWHMStatus = false;
            this.isRMStatus = false;
            this.isWarehouseMap = false;
            
            this.WM_rejectstatus=data.Details[0].wM_STATUS==2?true:false;
            this.RM_rejectstatus=data.Details[0].rM_STATUS==2?true:false;


          }
          else {


          }
        }

      }
      else {

        if (this.logUserrole == "106") {
          if (this.workLocationCode == 'null') {
            this.isWarehouseMap = false;
          }
          else {
            this.isWarehouseMap = false;
            this.isJAStatus = false;
            this.isLayout = false;
            this.JASection = true;

          }
        }
        else if (this.logUserrole == "104") {
          this.WHMSection = true;
          this.isJAStatus = false;
          this.isWHMStatus = false;


        }
        else if (this.logUserrole == "105") {
          this.RMSection = true;
          this.isJAStatus = false;
          this.isWHMStatus = false;
          this.isRMStatus = false;


        }
        else {


        }
      }

    },
      error => console.log(error));
  }

  LoadWhbyRM() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01=this.workLocationCode;
    this.service.postData(req, "GetWHDetailsByRM").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.WareshouseByrm = data.Details;
      }

    })

  }

  LoadSections(layoutdata:any,useraccess:string)
  {
    if (useraccess == "106") {
      this.JASection=true;
      this.isJAStatus=layoutdata[0].jA_STATUS==1?true:false;
      this.isWHMStatus=layoutdata[0].wM_STATUS==1?true:false;
      this.isRMStatus=layoutdata[0].rM_STATUS==1?true:false;
     
      this.WM_rejectstatus=layoutdata[0].wM_STATUS==2?true:false;
      this.RM_rejectstatus=layoutdata[0].rM_STATUS==2?true:false;
      this.WHMSection=false;
      this.RMSection=false;
      this.WH_Manager_Name=layoutdata[0].wM_NAME;
      this.RM_Name=layoutdata[0].rM_NAME;

    }
    else if (useraccess == "104") {
      this.WHMSection=true;
      this.JASection=false;
      this.RMSection=false;
      this.isJAStatus=layoutdata[0].jA_STATUS==1?true:false;
      this.isWHMStatus=layoutdata[0].wM_STATUS==1?true:false;
      this.isRMStatus=layoutdata[0].rM_STATUS==1?true:false;
      this.WM_rejectstatus=layoutdata[0].wM_STATUS==2?true:false;
      this.RM_rejectstatus=layoutdata[0].rM_STATUS==2?true:false;
      this.WH_Manager_Name=layoutdata[0].wM_NAME;
      this.RM_Name=layoutdata[0].rM_NAME;


     }
    else {

      this.RMSection=true;
      this.JASection=false;
      this.WHMSection=false;
      this.isJAStatus=layoutdata[0].jA_STATUS==1?true:false;
      this.isWHMStatus=layoutdata[0].wM_STATUS==1?true:false;
      this.isRMStatus=layoutdata[0].rM_STATUS==1?true:false;
      this.WM_rejectstatus=layoutdata[0].wM_STATUS==2?true:false;
      this.RM_rejectstatus=layoutdata[0].rM_STATUS==2?true:false;
      this.WH_Manager_Name=layoutdata[0].wM_NAME;
     }


  }


  get sf() { return this.rejectForm.controls; }
  get srmf() { return this.RMForm.controls; }

  AcceptorReject(stype) {
    
    if (stype == "1") {    
    
    this.isactive_flag = false;
    this.sf.reject_rmks.setValue(null);
      this.updateWHApprovalStatus(stype);
    }
    else if(stype == "2") {
      this.isactive_flag = true;
      this.isSubmit = true;
      // stop here if form is invalid
      if (this.rejectForm.invalid) {
        return false;
      }
      this.updateWHApprovalStatus(stype);
    }
    else
    {    
    
     this.isactive_flag = false;
     this.sf.reject_rmks.setValue(null);
      this.isactive_flag = true;
    }


  }

  updateWHApprovalStatus(stype) {
    const savetype: string = stype;
    this.showloader = true;
    const req = new InputRequest();

    req.INPUT_01 = this.logUserrole == "105" ? this.srmf.Whname.value : this.workLocationCode;//warehose id
    req.INPUT_02 = this.logUserrole == "105" ? this.layoutdata[0].wM_CODE : this.logUsercode;//WM Id
    req.INPUT_03 = this.logUserrole == "105" ? this.layoutdata[0].wM_STATUS : savetype;//WM Staus Approve/reject
    req.INPUT_04 = this.logUserrole == "104"?this.sf.reject_rmks.value:null;
    req.INPUT_05 = this.logUserrole == "104" ? null : this.logUsercode;//RM Id
    req.INPUT_06 = this.logUserrole == "104" ? null : savetype;//RM Staus Approve/reject
    req.INPUT_07 = this.logUserrole == "104" ? null : this.sf.reject_rmks.value; //RM remarks

    this.service.postData(req, "UpdateLayoutApproveorRejectStatus").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (savetype == "1")
          Swal.fire('success', "Warehouse Layout Approved Successfully !!!", 'success');
        else if (savetype == "2")
          Swal.fire('success', "Warehouse Layout Rejected Successfully !!!", 'success');

        this.LoadLayoutData();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');


    })


  }






  sortByProperty(property){  
    return function(a,b){  
       if(a[property] > b[property])  
          return 1;  
       else if(a[property] < b[property])  
          return -1;  
   
       return 0;  
    }  
  }

  AddGodowns() {
    this.g.reset();
    this.g.clear();

    // if (!this.layout.GCount.value) {
    //   Swal.fire('warning', "Please Enter No Of Godowns", 'warning');
    //   return false;
    // }

    for (let i = 0; i < this.layout.GCount.value; i++) {
      this.addGodown();
    }
  }

  AddCompartments(gindex: number) {
    // this.g[gindex].controls.reset();
    // this.g[gindex].controls.clear();
    let cnt: number = this.godowncompartments(gindex).value.length;
    if (cnt > 0) {
      this.resetcompartment(gindex, cnt);
    }

    if (!this.g.value[gindex].CCount) {
      Swal.fire('warning', "Please Enter No Of Compartments for Godown " + (gindex + 1), 'warning');
      return false;
    }
    for (let i = 0; i < this.g.value[gindex].CCount; i++) {
      this.addCompartment(gindex);
    }
  }

  resetcompartment(gindex: number, cnt: number) {
    for (let i = (cnt - 1); i >= 0; i--) {
      this.godowncompartments(gindex).removeAt(i);
    }
  }

  godowns(): FormArray {
    return this.LayoutForm.get("godowns") as FormArray
  }

  newGodown(): FormGroup {
    return this.formBuilder.group({
      CCount: ['', Validators.required],
      compartments: new FormArray([])
    })
  }

  addGodown() {
    this.godowns().push(this.newGodown());
  }

  removeGodown(godIndex: number) {
    this.godowns().removeAt(godIndex);
  }

  godowncompartments(godIndex: number): FormArray {
    return this.godowns().at(godIndex).get("compartments") as FormArray
  }

  newCompartment(): FormGroup {
    return this.formBuilder.group({
      SCount: ['', Validators.required],
    })
  }

  addCompartment(comIndex: number) {
    this.godowncompartments(comIndex).push(this.newCompartment());
  }

  removeCompartment(godIndex: number, comIndex: number) {

    this.godowncompartments(godIndex).removeAt(comIndex);
  }


  addConfigSetting() {

    this.LayoutForm.reset();
    this.g.reset();
    this.g.clear();

    this.addconfigmodal.show();

  }

  CloseConfig() {
    this.addconfigmodal.hide();
  }


  }
