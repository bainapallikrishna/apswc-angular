import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouselayoutComponent } from './warehouselayout.component';

describe('WarehouselayoutComponent', () => {
  let component: WarehouselayoutComponent;
  let fixture: ComponentFixture<WarehouselayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouselayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouselayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
