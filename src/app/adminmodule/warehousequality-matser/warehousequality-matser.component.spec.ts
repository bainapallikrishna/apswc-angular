import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehousequalityMatserComponent } from './warehousequality-matser.component';

describe('WarehousequalityMatserComponent', () => {
  let component: WarehousequalityMatserComponent;
  let fixture: ComponentFixture<WarehousequalityMatserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehousequalityMatserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehousequalityMatserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
