import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import Swal from 'sweetalert2';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { FloatingFilterComponent } from 'ag-grid-community/dist/lib/components/framework/componentTypes';
import { ThemeService } from 'ng2-charts';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';

@Component({
  selector: 'app-warehousequality-matser',
  templateUrl: './warehousequality-matser.component.html',
  styleUrls: ['./warehousequality-matser.component.css']
})
export class WarehousequalityMatserComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  Distvalue = [];
  weighbridgeeditsubmit: boolean = false;
  weighbridgeadd: boolean = false;
  Weighbridgelist: any;
  commoditygrparry: any[]
  Quantitytypes: any;
  Storagetypes: any;
  commoditygrouparray: any;
  ddl: any;
  Varietytypes; any;
  regaarray: any;
  Username: string;
  QPcommodityVarietyarry: any[];
  QPCOMMODITYgrpNAME = [];
  reg1dist: any;
  regseldel: any;
  regionname: any;
  COMMODITYNAME = [];
  commoditygrpname = [];
  commodityEDITvardiv: boolean = false
  commGroupEDITdiv: boolean = false;
  editActdctdiv: boolean = false;
  Farmerslist: any[];
  editRemarksdiv: boolean = false;
  eidtupdatebtn: boolean = false;
  Regiontypesmasterwithdist: any;
  commGroupdiv: boolean = false;
  commdgrpivdivvalidation: boolean = false;
  commoditylistypes: any;
  comdEDITsubmitted: boolean = false;
  commdivdiv: boolean = false;
  commeditdiv: boolean = false;
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  seldselarr = [];
  unique: any;
  regionsubmitted: boolean = false;
  uniquelocations = [];
  commodityvardiv: boolean = false;
  commdivdivvalidationvar: boolean = false;
  commddlvar: any;
  QPCOMMODITYNAME = [];
  QPGradetypes: any;
  Qualityparameterslist: any;
  Chemicallist: any;

  qid = [];
  cmdst = [];
  cmoigrp = [];
  vartie = [];
  grds = [];
  @ViewChild('addWarehouseModal') addWarehouseModal: ModalDirective;
  @ViewChild('editWarehouseModal') editWarehouseModal: ModalDirective;
  @ViewChild('historyWarehousemodal') historyWarehousemodal: ModalDirective;
  @ViewChild('regiondistmodal') regiondistmodal: ModalDirective;

  @ViewChild('addcommodityparameterModal') addcommodityparameterModal: ModalDirective;

  @ViewChild('addQualityparameterModal') addQualityparameterModal: ModalDirective;
  @ViewChild('editQualityparameterModal') editQualityparameterModal: ModalDirective;

  @ViewChild('editcommodityparameterModal') editcommodityparameterModal: ModalDirective;

  @ViewChild('addWeighbridgeModal') addWeighbridgeModal: ModalDirective;

  @ViewChild('editWeighbridgeModal') editWeighbridgeModal: ModalDirective;

  constructor(private formBuilder: FormBuilder, private service: CommonServices, private router: Router, private datef: CustomDateParserFormatter) { 


    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }


    else {
      if (this.logUserrole == "101" || this.isPasswordChanged == "1") {


        this.Username = sessionStorage.getItem("logUserCode");

      }
      else {


        this.router.navigate(['/Login']);
      }
    }
  }

  ngOnInit(): void {
  }


  // UsersList:IDropdownSettings[];
  submitted: boolean = false;
  EDITsubmitted: boolean = false;
  QPEDITsubmitted: boolean = false;
  QPsubmitted: boolean = false;
  commsubmited: boolean = false;
  Warehouseaddremarks: any;
  regionadddropdown: boolean = false;
  regioneditdrpdwn: boolean = false;
  Warehouseaddtype: any;
  Regiontypes: any;
  WreHousetypes: any;
  showloader: boolean = false;
  Commoditytypes: any;
  Title: any;
  WHaddarray: any;
  wheditarray: any;
  MasterHistlist: any;
  Shedtypes: any;
  Gatetypes: any;
  Transporttypes: any;
  Godowntypes: any;
  compartTYPES: any;
  staacktypes: any;
  patchtypes: any;
  stcknotypes: any;
  trucktypes: any;
  Gradetypes: any;
  pcategorytypes: any;
  Districtname = [];
  commodityVARTname = [];
  commdsubmited: boolean = false;
  regiondistricts: IDropdownSettings[];
  dropdownSettings = {};
  editreiondistricts: IDropdownSettings[];
  editdropdownSettings = {};
  regdist: IDropdownSettings[];
  dropdownList = [];
  selectedItems = [];
  selectedItems1 = [];
  QPcommoditygrparry: any[];
  selectedIregistertems = [];
  getregionsbydistcode: any;
  selectedItems11 = [];
  commdivdivvalidation: boolean = false;
  commoditygradename = [];
  p: number = 1;



  Warehouseaddform = this.formBuilder.group({

    Warehouseaddtype: ['', Validators.required],

    Warehouseaddremarks: ['', Validators.required],
    commddl: [''],
    commddlvar: [''],
    commgrpddl: ['']

  });


  WarehouseEDITform = this.formBuilder.group({
    WarehouseEDITId: [''],

    WarehouseEDITtype: [''],

    WHactive: ['', Validators.required],

    WarehouseEDITremarks: ['', Validators.required],

    commEDITddl: [''],
    commEDITddlvar: [''],
    commgrpEDITddl: []
  });


  
  QualityParameterAddForm = this.formBuilder.group({

    QPcommddlQ: ['', Validators.required],
    QPcommgrpddl: ['', Validators.required],
    QPcommvarddl: ['', Validators.required],
    QPGradeddl: ['', Validators.required],
    QpName: ['', Validators.required],
    QPValue: ['', Validators.required],
    QPDescription: ['', Validators.required],
    QPPercentage: ['', Validators.required],
    QPCharacterstics: ['', Validators.required],
    QPRemarks: ['', Validators.required]
  });

  QualityParameterEDITForm = this.formBuilder.group({

    QpEDITCommodity: [''],
    QPeditGroupname: [''],
    QPeditVariety: [''],
    QPeditgrade: [''],
    QPeditName: [''],
    QPeditvalue: [''],
    QPeditdescription: [''],
    QPeditPercentage: [''],
    QPeditCharacterstics: [''],
    QPactive: ['', Validators.required],
    QPEDITremarks: ['', Validators.required]
  });
  
  GetGradeslist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "GRADE_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Gradetypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }
  
  Getqualityparameters() {
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "QUALITY_PARAMETERS";
    this.service.postData(req, "GetVarietylist").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Qualityparameterslist = data.Details;
        this.GetCommoditieslist();
        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));



  }
  GetCommoditieslist() {


    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "COMMODITY_LIST";
    this.service.postData(req, "Getcommodities").subscribe(data => {
      if (data.StatusCode == "100") {
        this.commoditylistypes = data.Details;

        this.showloader = false;
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }
  wb = 0 + 1; far = 0 + 1; qualprt = 0 + 1; var = 0 + 1; Qtc = 0 + 1; strg = 0 + 1; comd = 0 + 1; comdg = 0 + 1; re = 0 + 1; Wr = 0 + 1; shc = 0 + 1; otc = 0 + 1; com = 0 + 1; gt = 0 + 1; trns = 0 + 1; GOD = 0 + 1; comp = 0 + 1; sta = 0 + 1; pt = 0 + 1; stkno = 0 + 1; trk = 0 + 1; GRD = 0 + 1; pctg = 0 + 1; che = 0 + 1;imp = 0 + 1;
  Gradeclick() {

    if (this.GRD == 1) {
      this.GetGradeslist();
      this.GRD++;
    }
    else {

      this.GRD = 0;
      this.showloader = false;
    }



  }
  Qualityparametersclick() {

    if (this.qualprt == 1) {
      this.Getqualityparameters();
      this.qualprt++;
    }
    else {

      this.qualprt = 0;
      this.showloader = false;
    }

  }
  Warehouseaddclick() {

    this.showloader = true;
    const req = new InputRequest();
    this.submitted = true;
    if (this.Warehouseaddform.invalid) {
      this.showloader = false;
      return;
    }
       if (this.Title == "Grade") {
      this.showloader = true;
      req.INPUT_01 = "GRADE_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.GRD = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.GetGradeslist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));
      this.showloader = false;
    }
  }

  WarehouseEDITclick() {
    this.showloader = true;

    const req = new InputRequest();

    this.EDITsubmitted = true;


    if (this.WarehouseEDITform.invalid) {
      this.showloader = false;
      return;

    }    if (this.Title == "Grade") {

      req.INPUT_01 = "GRADE_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.GRD = 0;
            this.GetGradeslist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");

          this.showloader = false;

        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }
  
   

   
  }


  
  QualityParameteraddclick() {

    this.showloader = true;
    const req = new InputRequest();
    this.QPsubmitted = true;
    if (this.QualityParameterAddForm.invalid) {
      this.showloader = false;
      return;
    }
    for (var i = 0; i < this.Gradetypes.length; i++) {
      if (this.QualityParameterAddForm.controls.QPGradeddl.value == this.Gradetypes[i].iteM_ID) {
        this.commoditygradename = [];
        var commoditygradenamebyid = this.Gradetypes[i].iteM_NAME;
        this.commoditygradename.push(commoditygradenamebyid);
      }


    }

    for (var i = 0; i < this.commoditylistypes.length; i++) {
      if (this.QualityParameterAddForm.controls.QPcommddlQ.value == this.commoditylistypes[i].iteM_ID) {
        this.QPCOMMODITYNAME = [];

        this.QPCOMMODITYNAME.push(this.commoditylistypes[i].iteM_NAME);
      }
      else {


      }

    }

    for (var i = 0; i < this.QPcommodityVarietyarry.length; i++) {
      if (this.QualityParameterAddForm.controls.QPcommvarddl.value == this.QPcommodityVarietyarry[i].varietY_ID) {
        this.commodityVARTname = [];
        var commodityVARTnamebyid = this.QPcommodityVarietyarry[i].varietY_NAME;
        this.commodityVARTname.push(commodityVARTnamebyid);
      }

    }

    for (var i = 0; i < this.QPcommoditygrparry.length; i++) {
      if (this.QualityParameterAddForm.controls.QPcommgrpddl.value == this.QPcommoditygrparry[i].commoditY_CODE) {
        this.QPCOMMODITYgrpNAME = [];
        var QPcommodityGrpnamebyid = this.QPcommoditygrparry[i].commodity;
        this.QPCOMMODITYgrpNAME.push(QPcommodityGrpnamebyid);
      }
    }
    req.INPUT_01 = this.QualityParameterAddForm.controls.QPGradeddl.value;
    req.INPUT_02 = this.commoditygradename.toString();

    req.INPUT_03 = this.QualityParameterAddForm.controls.QPcommvarddl.value;
    req.INPUT_04 = this.commodityVARTname.toString();

    req.INPUT_05 = this.QualityParameterAddForm.controls.QPcommgrpddl.value;
    req.INPUT_06 = this.QPCOMMODITYgrpNAME.toString();

    req.INPUT_07 = this.QPCOMMODITYNAME.toString();
    req.INPUT_08 = this.QualityParameterAddForm.controls.QPcommddlQ.value;
    req.INPUT_09 = this.QualityParameterAddForm.controls.QPRemarks.value;


    req.INPUT_10 = this.logUserrole;
    req.INPUT_11 = this.QualityParameterAddForm.controls.QpName.value;

    req.INPUT_12 = this.QualityParameterAddForm.controls.QPDescription.value;
    req.INPUT_13 = this.QualityParameterAddForm.controls.QPValue.value;

    req.INPUT_14 = this.QualityParameterAddForm.controls.QPPercentage.value;


    req.INPUT_15 = this.QualityParameterAddForm.controls.QPCharacterstics.value;

    req.USER_NAME = this.Username;

    req.CALL_SOURCE = "Web";
    this.service.postData(req, "SaveQualityParameter").subscribe(data => {
      this.WHaddarray = data.result;
      if (this.WHaddarray.StatusCode == "100") {

        if (this.WHaddarray.Details[0].rtN_ID == "1") {
          this.qualprt = 0;
          this.WarehouseEDITform.reset();
          this.Warehouseaddform.reset();
          this.addQualityparameterModal.hide();
          //this.QualityParameterAddForm.reset();
          this.Getqualityparameters();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.commodityvardiv = false;
          this.commdivdivvalidationvar = false;
          this.showloader = false;
          Swal.fire("success", "Data Added Successfully", "success");

          // this.f.Godownswarehsetype.setValue(editid);
        }
        else {
          Swal.fire("info", "This Master IsAlready Registered", "info");

          this.Warehouseaddform.reset();
          this.WarehouseEDITform.reset();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.addQualityparameterModal.hide();

          this.showloader = false;

        }
      }

      else {
        Swal.fire("info", this.WHaddarray.StatusMessage, "info");
        this.QualityParameterEDITForm.reset();
        this.QualityParameterAddForm.reset();
        this.addQualityparameterModal.hide();

        this.showloader = false;

      }
    },
      error => console.log(error));

  }


  QualityparameterEDITclick() {

    this.showloader = true;

    const req = new InputRequest();

    this.QPEDITsubmitted = true;


    if (this.QualityParameterEDITForm.invalid) {
      this.showloader = false;
      return;

    }
    req.INPUT_01 = this.vartie.toString();
    req.INPUT_02 = this.cmoigrp.toString();
    req.INPUT_03 = this.grds.toString();
    req.INPUT_04 = this.cmdst.toString();
    req.INPUT_08 = this.qid.toString();
    req.INPUT_05 = this.QualityParameterEDITForm.controls.QPactive.value;
    req.INPUT_06 = this.QualityParameterEDITForm.controls.QPEDITremarks.value;
    req.USER_NAME = this.Username;
    req.INPUT_07 = this.logUserrole;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "updateQualityParameter").subscribe(data => {
      this.wheditarray = data.result;
      if (this.wheditarray.StatusCode == "100") {
        if (this.wheditarray.Details[0].rtN_ID == "1") {
          this.qualprt = 0;
          this.Getqualityparameters();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.WarehouseEDITform.reset();
          this.Warehouseaddform.reset();
          this.editQualityparameterModal.hide();
          this.editWarehouseModal.hide();
          this.showloader = false;
          Swal.fire("success", "Data Updated Successfully", "success");
        }
        else {
          Swal.fire("info", "Data Updation Failed", "info");
          this.Warehouseaddform.reset();
          this.WarehouseEDITform.reset();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.editQualityparameterModal.hide();
          this.editWarehouseModal.hide();
          this.showloader = false;
        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.wheditarray.StatusMessage, "info");
        this.showloader = false;
        this.editQualityparameterModal.hide();

      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));
    this.showloader = false;
  }


  
  WHHistory(val) {
    const req = new InputRequest();

   if (val == "Gradehistory") {
      req.INPUT_01 = "GRADE_TYPE";
      req.INPUT_02 = "MASTERS";
    }
  
    else if (val == "Qualityparamterhistory") {

      req.INPUT_01 = null;
      req.INPUT_02 = "QUALITY_PARAMETER";
    }
    else {
      req.INPUT_01 = "";
      req.INPUT_02 = "";
      Swal.fire("info", "No history found", "info");
      return false;
    }

    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.WarehouseEDITform.reset();
        this.Warehouseaddform.reset();
        this.historyWarehousemodal.show();
        this.showloader = false;

      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.WarehouseEDITform.reset();
        this.Warehouseaddform.reset();
        this.historyWarehousemodal.hide();
        this.showloader = false;

      }
    },
      error => console.log(error));
    this.showloader = false;
  }



  hidehistoryempModal(): void {
    this.Warehouseaddform.reset();
    this.commdivdivvalidation = false;
    this.WarehouseEDITform.reset();
    this.historyWarehousemodal.hide();
  }
  AddWhGrade() {

    this.Title = "Grade";
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commdivdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.addWarehouseModal.show();
    this.submitted = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
  }

  WHGradeEdit(gradeid, gradename, gradeact) {


    this.Title = "Grade";
    const req = new InputRequest();
    req.INPUT_01 = "GRADE_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(gradeid);
        this.f.WarehouseEDITtype.setValue(gradename);
        this.f.WHactive.setValue(gradeact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commdivdiv = false;
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.editWarehouseModal.show();



  }

  get f1() { return this.Warehouseaddform.controls; }

  get f() { return this.WarehouseEDITform.controls; }


  get QP() { return this.QualityParameterAddForm.controls }

  get QP1() { return this.QualityParameterEDITForm.controls }




  hideeditQualityparameterModal() {

    this.QPsubmitted = false;
    this.QPEDITsubmitted = false;
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.Warehouseaddform.reset();
    this.addQualityparameterModal.hide();
    this.editQualityparameterModal.hide();

  }

  WHQualityparameterEdit(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15) {
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.Warehouseaddform.reset();

    this.QPEDITsubmitted = false;
    this.QPsubmitted = false;

    this.qid = [];
    this.cmdst = [];
    this.cmoigrp = [];
    this.vartie = [];
    this.grds = [];

    this.qid.push(t11);
    this.cmdst.push(t15);
    this.cmoigrp.push(t14);
    this.vartie.push(t13);
    this.grds.push(t12);

    this.QualityParameterEDITForm.controls.QpEDITCommodity.setValue(t1);
    this.QualityParameterEDITForm.controls.QPeditGroupname.setValue(t2);
    this.QualityParameterEDITForm.controls.QPeditVariety.setValue(t3);
    this.QualityParameterEDITForm.controls.QPeditgrade.setValue(t4);
    this.QualityParameterEDITForm.controls.QPeditName.setValue(t5);
    this.QualityParameterEDITForm.controls.QPeditvalue.setValue(t6);
    this.QualityParameterEDITForm.controls.QPeditdescription.setValue(t7);
    this.QualityParameterEDITForm.controls.QPeditPercentage.setValue(t8);
    this.QualityParameterEDITForm.controls.QPeditCharacterstics.setValue(t9);
    this.QualityParameterEDITForm.controls.QPactive.setValue(t10.toString());
    this.editQualityparameterModal.show();

  }
  QPGradechange() {



  }
  commoditygroupchange() {


  }
  commoditychange(val: string) {

    this.COMMODITYGROUPFUNCTION(this.Warehouseaddform.controls.commddlvar.value);

  }
  hideWarehouseModal() {
    this.submitted = false;
    this.EDITsubmitted = false;
    this.Warehouseaddform.reset();
    this.addWarehouseModal.hide();
    this.commdivdivvalidation = false;
  }
  hideeditWarehouseModal() {
    this.EDITsubmitted = false;
    this.Warehouseaddform.reset();
    this.editWarehouseModal.hide();
    this.commdivdivvalidation = false;
  }
  COMMODITYGROUPFUNCTION(val: any) {
    this.commoditygrparry = [];
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "COMMODITY_CODE_VIEW";
    req.INPUT_01 = val.toString();
    this.service.postData(req, "Getcommoditygrpbycomdty").subscribe(data => {
      if (data.StatusCode == "100") {
        this.commoditygrparry = data.Details;


        this.showloader = false;
      }
      else {
        this.commoditygrparry = [];
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }

  QPchangeCommoditygroup() {

    this.QPcommodityVarietyarry = [];
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "VARIETY";
    req.INPUT_01 = this.QualityParameterAddForm.controls.QPcommddlQ.value;
    req.INPUT_02 = this.QualityParameterAddForm.controls.QPcommgrpddl.value;
    this.service.postData(req, "Getcommoditygrpbycomdty").subscribe(data => {
      if (data.StatusCode == "100") {
        this.QPcommodityVarietyarry = data.Details;


        this.QualityParameterAddForm.controls.QPcommvarddl.setValue('');


        this.showloader = false;
      }
      else {
        this.commoditygrparry = [];
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }
  
  AddQualityparametertype() {
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.Warehouseaddform.reset();
  
    this.QPEDITsubmitted = false;
    this.QPsubmitted = false;
    this.QPcommoditygrparry = [];
    this.QPcommodityVarietyarry = [];
    this.QualityParameterAddForm.reset();
    this.QualityParameterAddForm.controls.QPcommddlQ.setValue('');
    this.QualityParameterAddForm.controls.QPcommgrpddl.setValue('');
    this.QualityParameterAddForm.controls.QPcommvarddl.setValue('');
    this.QualityParameterAddForm.controls.QPGradeddl.setValue('');
    this.addQualityparameterModal.show();

  }
  hidequalityparameteraddModal() {
    this.QPsubmitted = false;
    this.QPEDITsubmitted = false;
    this.QualityParameterAddForm.reset();
    this.addQualityparameterModal.hide();
    this.editQualityparameterModal.hide();
    this.QualityParameterEDITForm.reset();
    this.QPEDITsubmitted = false;
    this.QPsubmitted = false;
  }
  QPVarietychange() {


    this.GetGradeslist();
    this.QualityParameterAddForm.controls.QPGradeddl.setValue('');

  }
  changeCommodity() {

    this.QPcommoditygrparry = [];
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "COMMODITY_CODE_VIEW";
    req.INPUT_01 = this.QualityParameterAddForm.controls.QPcommddlQ.value;
    this.service.postData(req, "Getcommoditygrpbycomdty").subscribe(data => {
      if (data.StatusCode == "100") {
        this.QPcommoditygrparry = data.Details;
        this.QualityParameterAddForm.controls.QPcommgrpddl.setValue('');

        this.showloader = false;
      }
      else {
        this.commoditygrparry = [];
        this.QPcommodityVarietyarry = [];
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }
  hidehistorymasterModal() {

    this.historyWarehousemodal.hide();
    this.submitted = false;
    this.EDITsubmitted = false;
    this.Warehouseaddform.reset();
    this.WarehouseEDITform.reset();
  
    this.commdivdivvalidation = false;
  }
}
