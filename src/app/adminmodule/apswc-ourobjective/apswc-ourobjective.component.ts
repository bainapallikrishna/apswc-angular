import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Editor, Toolbar, Validators } from 'ngx-editor';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-apswc-ourobjective',
  templateUrl: './apswc-ourobjective.component.html',
  styleUrls: ['./apswc-ourobjective.component.css']
})
export class ApswcOurobjectiveComponent implements OnInit {

  editor: Editor;
  toolbar: Toolbar = [
    ["bold", "italic"],
    ["underline", "strike"],
    ["code", "blockquote"],
    ["ordered_list", "bullet_list"],
    [{ heading: ["h1", "h2", "h3", "h4", "h5", "h6"] }],
    ["link", "image"],
    ["text_color", "background_color"],
    ["align_left", "align_center", "align_right", "align_justify"]
  ];


  formedit = true;
  formourobjective: FormGroup;
  ourobjective: string;
  divourobjective: string;
  constructor(private http: HttpClient, private service: CommonServices) { }

  ngOnInit(): void {
    this.editor = new Editor();
    this.formourobjective = new FormGroup({
      ourobjective: new FormControl("", Validators.required())

    });
    this.getcontentdata();
  }

  getcontentdata() {
    this.service.getData("GetHomePageConent").subscribe(data => {
      if (data.StatusCode == "100") {
        this.divourobjective = data.Details[0]["ouR_OBJECTIVES"];

        if (!this.divourobjective) {
          this.formedit = false;
        }
        else {
          this.formedit = true;
        }
      }
      else {
        Swal.fire('error', data.statusmessage, 'error');
      }
    });
  }
  apswcedit() {
    this.formedit = false;
  }
  onSubmit() {
    if (!this.formourobjective.value.ourobjective) {
      Swal.fire('info', 'Please Enter the our objective', 'info');
      return;
    }
    if (this.formourobjective.value.ourobjective.length < 150) {
      Swal.fire('info', 'Please Enter our objective minimum 150 characters', 'info');
      return;
    }
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "UPD_OBJECTIVES";
    req.INPUT_01 = this.formourobjective.value.ourobjective;
    this.service.postData(req, "HomePageConentInsert").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'info');

        this.getcontentdata();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });
  }

}
