import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApswcOurobjectiveComponent } from './apswc-ourobjective.component';

describe('ApswcOurobjectiveComponent', () => {
  let component: ApswcOurobjectiveComponent;
  let fixture: ComponentFixture<ApswcOurobjectiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApswcOurobjectiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApswcOurobjectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
