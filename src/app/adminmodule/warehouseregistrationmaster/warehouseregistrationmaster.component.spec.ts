import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseregistrationmasterComponent } from './warehouseregistrationmaster.component';

describe('WarehouseregistrationmasterComponent', () => {
  let component: WarehouseregistrationmasterComponent;
  let fixture: ComponentFixture<WarehouseregistrationmasterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseregistrationmasterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseregistrationmasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
