import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
import Swal from 'sweetalert2';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { FloatingFilterComponent } from 'ag-grid-community/dist/lib/components/framework/componentTypes';
import { ThemeService } from 'ng2-charts';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';

@Component({
  selector: 'app-warehouse-reservation-matser',
  templateUrl: './warehouse-reservation-matser.component.html',
  styleUrls: ['./warehouse-reservation-matser.component.css']
})
export class WarehouseReservationMatserComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  Distvalue = [];
  weighbridgeeditsubmit: boolean = false;
  weighbridgeadd: boolean = false;
  Weighbridgelist: any;
  commoditygrparry: any[]
  Quantitytypes: any;
  Storagetypes: any;
  commoditygrouparray: any;
  ddl: any;
  Varietytypes; any;
  regaarray: any;
  Username: string;
  QPcommodityVarietyarry: any[];
  QPCOMMODITYgrpNAME = [];
  reg1dist: any;
  regseldel: any;
  regionname: any;
  COMMODITYNAME = [];
  commoditygrpname = [];
  commodityEDITvardiv: boolean = false
  commGroupEDITdiv: boolean = false;
  editActdctdiv: boolean = false;
  Farmerslist: any[];
  editRemarksdiv: boolean = false;
  eidtupdatebtn: boolean = false;
  Regiontypesmasterwithdist: any;
  commGroupdiv: boolean = false;
  commdgrpivdivvalidation: boolean = false;
  commoditylistypes: any;
  comdEDITsubmitted: boolean = false;
  commdivdiv: boolean = false;
  commeditdiv: boolean = false;
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  seldselarr = [];
  unique: any;
  regionsubmitted: boolean = false;
  uniquelocations = [];
  commodityvardiv: boolean = false;
  commdivdivvalidationvar: boolean = false;
  commddlvar: any;
  QPCOMMODITYNAME = [];
  QPGradetypes: any;
  Qualityparameterslist: any;
  Chemicallist: any;

  qid = [];
  cmdst = [];
  cmoigrp = [];
  vartie = [];
  grds = [];
  @ViewChild('addWarehouseModal') addWarehouseModal: ModalDirective;
  @ViewChild('editWarehouseModal') editWarehouseModal: ModalDirective;
  @ViewChild('historyWarehousemodal') historyWarehousemodal: ModalDirective;
  @ViewChild('regiondistmodal') regiondistmodal: ModalDirective;

  @ViewChild('addcommodityparameterModal') addcommodityparameterModal: ModalDirective;
  @ViewChild('apprModal') apprModal: ModalDirective;
  
  @ViewChild('addQualityparameterModal') addQualityparameterModal: ModalDirective;
  @ViewChild('editQualityparameterModal') editQualityparameterModal: ModalDirective;

  @ViewChild('editcommodityparameterModal') editcommodityparameterModal: ModalDirective;

  @ViewChild('addWeighbridgeModal') addWeighbridgeModal: ModalDirective;

  @ViewChild('editWeighbridgeModal') editWeighbridgeModal: ModalDirective;

  constructor(private formBuilder: FormBuilder, private service: CommonServices, private router: Router, private datef: CustomDateParserFormatter) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }


    else {
      if (this.logUserrole == "101" || this.isPasswordChanged == "1") {


        this.Username = sessionStorage.getItem("logUserCode");

      }
      else {


        this.router.navigate(['/Login']);
      }
    }
  }

  // UsersList:IDropdownSettings[];
  submitted: boolean = false;
  EDITsubmitted: boolean = false;
  QPEDITsubmitted: boolean = false;
  QPsubmitted: boolean = false;
  commsubmited: boolean = false;
  Warehouseaddremarks: any;
  regionadddropdown: boolean = false;
  regioneditdrpdwn: boolean = false;
  Warehouseaddtype: any;
  Regiontypes: any;
  WreHousetypes: any;
  showloader: boolean = false;
  Commoditytypes: any;
  Title: any;
  WHaddarray: any;
  wheditarray: any;
  MasterHistlist: any;
  Shedtypes: any;
  Gatetypes: any;
  Transporttypes: any;
  Godowntypes: any;
  compartTYPES: any;
  staacktypes: any;
  patchtypes: any;
  stcknotypes: any;
  trucktypes: any;
  Gradetypes: any;
  pcategorytypes: any;
  Districtname = [];
  commodityVARTname = [];
  commdsubmited: boolean = false;
  regiondistricts: IDropdownSettings[];
  dropdownSettings = {};
  editreiondistricts: IDropdownSettings[];
  editdropdownSettings = {};
  regdist: IDropdownSettings[];
  dropdownList = [];
  selectedItems = [];
  selectedItems1 = [];
  QPcommoditygrparry: any[];
  selectedIregistertems = [];
  getregionsbydistcode: any;
  selectedItems11 = [];
  commdivdivvalidation: boolean = false;
  commoditygradename = [];
  p: number = 1;
  ngOnInit(): void {


    this.dropdownSettings = {
      singleSelection: true,
      idField: 'districT_CODE',
      textField: 'districT_NAME_ENG',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };


    this.editdropdownSettings = {
      singleSelection: false,
      idField: 'iteM_ID',
      textField: 'iteM_NAME',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.CommodityEDITForm.controls['CommodityEDITFormddl'].disable();

    this.WarehouseEDITform.controls['commEDITddlvar'].disable();
    this.WarehouseEDITform.controls['commgrpEDITddl'].disable();
  }

  QualityParameterAddForm = this.formBuilder.group({

    QPcommddlQ: ['', Validators.required],
    QPcommgrpddl: ['', Validators.required],
    QPcommvarddl: ['', Validators.required],
    QPGradeddl: ['', Validators.required],
    QpName: ['', Validators.required],
    QPValue: ['', Validators.required],
    QPDescription: ['', Validators.required],
    QPPercentage: ['', Validators.required],
    QPCharacterstics: ['', Validators.required],
    QPRemarks: ['', Validators.required]
  });

  QualityParameterEDITForm = this.formBuilder.group({

    QpEDITCommodity: [''],
    QPeditGroupname: [''],
    QPeditVariety: [''],
    QPeditgrade: [''],
    QPeditName: [''],
    QPeditvalue: [''],
    QPeditdescription: [''],
    QPeditPercentage: [''],
    QPeditCharacterstics: [''],
    QPactive: ['', Validators.required],
    QPEDITremarks: ['', Validators.required]
  });

  WarehouseRegioneditform = this.formBuilder.group({

    newregddl: ['', Validators.required],

    WarehouseregionEDITremarks: ['', Validators.required]

  });

  Warehouseaddform = this.formBuilder.group({

    Warehouseaddtype: ['', Validators.required],

    Warehouseaddremarks: ['', Validators.required],
    commddl: [''],
    commddlvar: [''],
    commgrpddl: ['']

  });


  WarehouseEDITform = this.formBuilder.group({
    WarehouseEDITId: [''],

    WarehouseEDITtype: [''],

    WHactive: ['', Validators.required],

    WarehouseEDITremarks: ['', Validators.required],

    commEDITddl: [''],
    commEDITddlvar: [''],
    commgrpEDITddl: []
  });

  commodityGroupAddForm = this.formBuilder.group({

    commddddltype: ['', Validators.required],
    comdgrpnametype: ['', Validators.required],
    comdpkgtype: ['', Validators.required],
    comdWeight: ['', Validators.required],
    comdRate: ['', Validators.required],
    comdhighrated1: ['', Validators.required],
    comdhighrated2: ['', Validators.required],
    comdremarks: ['', Validators.required],
    addcomdact:['',Validators.required],
    TransStartDate:['',Validators.required],
    TransEndDate:['',Validators.required]
  });


  CommodityEDITForm = this.formBuilder.group({

    CommodityEDITFormddl: [''],
    editCommodityGroupid: [''],
    editCommodityGroupname: [''],
    editPackageType: [''],
    editNetWeight: [''],
    editStandardRate: ['',Validators.required],
    editHighrated1: ['',Validators.required],
    editHighrated2: ['',Validators.required],
    edittransstdate:['',Validators.required],

    editinsuranc:['',Validators.required],
    //comdeditactive: ['', Validators.required],

    commdEDITremarks: ['', Validators.required],
    unqid:[''],
    commoditycode:['']
  });


  WeighbridgeAddForm = this.formBuilder.group({

    Weighbridgenametype: ['', Validators.required],
    MaintenanceStartDate: ['', Validators.required],
    MaintenanceEndDate: ['', Validators.required],
    MaintenancePeriod: ['', Validators.required],
    MaintenanceValue: ['', Validators.required],
    Weighbridgeremarks: ['', Validators.required]

  })
  WeighbridgeEDITForm = this.formBuilder.group({

    Weighbridgecmpid: [''],
    WeighbridgeCompanyname: [''],
    MaintenanceStartDate: [''],
    MaintenanceEndDate: [''],
    MaintenancePeriod: [''],
    MaintenanceValue: [''],
    weighbridgeactive: ['', Validators.required],
    weighbridgeeditremarks: ['', Validators.required]


  })


  hideregistmasterModal() {

    this.WHRegionEdit(this.WarehouseEDITform.controls.WarehouseEDITId.value, this.WarehouseEDITform.controls.WarehouseEDITtype.value, this.WarehouseEDITform.controls.WHactive.value)
    this.regiondistmodal.hide();

  }
  onItemSelect(item: any) {
    this.showloader = true;
    this.Distvalue.push({
      distcode: item.districT_CODE,
      DistNAME: item.districT_NAME_ENG
    })


  }

  onItemaddDeSelect(item: any) {

    this.Distvalue = this.Distvalue.filter(m => m.distcode != item.distcode);
  }

  onItemEditSelect(item: any) {

    this.seldselarr = this.seldselarr.filter(m => m != item.iteM_ID);

  }

  onItemEditDeSelect(item: any) {


    this.regiondistmodal.show();

    this.seldselarr.push(item.iteM_ID);

  }

  onDeSelectAll(item: any) {

    this.seldselarr = [];

    for (var del = 0; del < this.selectedItems11.length; del++) {
      this.seldselarr.push(

        this.selectedItems11[del].iteM_ID

      );

    }

    this.regiondistmodal.show();
  }
  onSelectAll(items: any) {
    this.seldselarr = [];
  }

  getInactivedist() {

    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "INACTIVE_DISTRICTS";
    this.service.postData(req, "GetRegionMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.regiondistricts = data.Details;

        this.showloader = false;
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else {
        this.showloader = false;

        //Swal.fire("info", "no District Found for mapping if you want to add districts please delselect the districts in another Region ", "info");
      }


    },
      error => console.log(error));

  }


  hidemesgModal(){
    this.apprModal.hide();
  }
  getregionlist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "REGIONAL";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Regiontypes = data.Details;

        //this.getregionmasterwithdist();
        this.showloader = false;
        this.editActdctdiv = false;
        this.editRemarksdiv = false;
        this.eidtupdatebtn = false;
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else {
        this.editActdctdiv = false;
        this.editRemarksdiv = false;
        this.eidtupdatebtn = false;
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));
  }



  // getregiondistrictslist() {
  //   this.showloader = true;

  //   this.service.getData("GetDistricts").subscribe(data => {

  //     if (data.StatusCode == "100") {
  //       this.regiondistricts = data.Details;

  //       this.editreiondistricts=data.Details;
  //       this.regionadddropdown=true;

  //     }
  //     else {
  //       this.showloader = false;
  //       Swal.fire("info", data.StatusMessage, "info");
  //       this.regionadddropdown=false;
  //     }


  //   },
  //     error => console.log(error));
  // }

  getwarehouselist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "WH_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.WreHousetypes = data.Details;

        this.showloader = false;
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }


  Getstocklist() {


    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "ST_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Regiontypes = data.Details;

        this.showloader = false;
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }


  GetGodownlist() {


    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "GODOWN_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Godowntypes = data.Details;

        this.showloader = false;
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }

  Getcompartmentlist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "COMPARTMENT_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.compartTYPES = data.Details;

        this.showloader = false;
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }


  Getshedslist() {


    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "SH_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Shedtypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }


  Getgatenolist() {


    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "GATE_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Gatetypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }


  GetModeoftransportlist() {


    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "TRANSPORT_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Transporttypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }

  getstacklist() {


    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "STACK_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.staacktypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }


  Getpatchlist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "PACK_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.patchtypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }

  Getstacknolist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "STACKNO_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.stcknotypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }

  Gettrucktypelist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "TRUCK_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.trucktypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }

  GetGradeslist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "GRADE_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Gradetypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }




  GetpCategorylist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "PCATEGORY_TYPE";

    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.pcategorytypes = data.Details;

        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }



  GetCommoditieslist() {


    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "COMMODITY_LIST";
    this.service.postData(req, "Getcommodities").subscribe(data => {
      if (data.StatusCode == "100") {
        this.commoditylistypes = data.Details;

        this.showloader = false;
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }


  GetcommoditiesGrouplist() {

    this.uniquelocations = [];
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "COMMODITY_VIEW";
    this.service.postData(req, "Getcommodities").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Commoditytypes = data.Details;

        this.GetCommoditieslist();

        // this.unique = [...new Set(this.Commoditytypes.map( item => item.storagE_TYPE))];
        // for(var i=0;i<this.unique.length;i++){


        //   this.uniquelocations.push({"Uniquecommodity":this.unique[i].toString()});
        // }


        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));




  }

  Commodityrates:any;
  GetcommoditiesRateslist(COMCODE) {

  
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "COMMODITY_RATES";
    req.INPUT_01=COMCODE;
    this.service.postData(req, "Getcommodities").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Commodityrates = data.Details;

        this.showloader = false;

      }
      else {
        this.Commodityrates=[];
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));




  }


  GetStoragetypeslist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "CONTRACT_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Storagetypes = data.Details;

        this.showloader = false;
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }
  GetQuantitylist() {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "QUANTITY_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Quantitytypes = data.Details;

        this.showloader = false;
        // for(var i=0;i<this.emptypes.length;i++){

        //   this.emphistcount=this.emphistcount+this.emptypes[i].hiS_COUNT;
        // }
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }


  GetVarietylist() {

    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "VARIETY_VIEW";
    this.service.postData(req, "GetVarietylist").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Varietytypes = data.Details;
        this.GetCommoditieslist();
        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }


  Getqualityparameters() {
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "QUALITY_PARAMETERS";
    this.service.postData(req, "GetVarietylist").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Qualityparameterslist = data.Details;
        this.GetCommoditieslist();
        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));



  }



  GetFarmerslist() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "FARMER_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Farmerslist = data.Details;
        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));



  }


  GetWeighbridgelist() {
    this.showloader = true;
    const req = new InputRequest();
    this.service.postData(req, "GetWeighbridgedata").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Weighbridgelist = data.Details;
        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));
    this.showloader = false;


  }

  GetChemicalslist() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "CHEMICALS";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Chemicallist = data.Details;
        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    },
      error => console.log(error));

  }

  GetImprestlist() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = "IMPREST_PAYMENT";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Chemicallist = data.Details;
        this.showloader = false;

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    },
      error => console.log(error));

  }


  wb = 0 + 1; far = 0 + 1; qualprt = 0 + 1; var = 0 + 1; Qtc = 0 + 1; strg = 0 + 1; comd = 0 + 1; comdg = 0 + 1; re = 0 + 1; Wr = 0 + 1; shc = 0 + 1; otc = 0 + 1; com = 0 + 1; gt = 0 + 1; trns = 0 + 1; GOD = 0 + 1; comp = 0 + 1; sta = 0 + 1; pt = 0 + 1; stkno = 0 + 1; trk = 0 + 1; GRD = 0 + 1; pctg = 0 + 1; che = 0 + 1;imp = 0 + 1;


  Weighbridgeclick() {

    if (this.wb == 1) {
      this.GetWeighbridgelist();
      this.wb++;
    }
    else {
      this.showloader = false;
      this.wb = 0;
    }
  }



  Farmerclick() {

    if (this.far == 1) {
      this.GetFarmerslist();
      this.far++;
    }
    else {
      this.showloader = false;
      this.far = 0;
    }
  }

  Regionclick() {

    if (this.re == 1) {
      this.getregionlist();
      this.re++;
    }
    else {
      this.showloader = false;
      this.re = 0;
    }

  }
  Wahouseclick() {

    if (this.Wr == 1) {
      this.getwarehouselist();
      this.Wr++;
    }
    else {

      this.Wr = 0;
      this.showloader = false;
    }

  }
  Shedclick() {

    if (this.shc == 1) {
      this.Getshedslist();
      this.shc++;
    }
    else {

      this.shc = 0;
      this.showloader = false;
    }

  }
  Gatenoclick() {

    if (this.gt == 1) {
      this.Getgatenolist();
      this.gt++;
    }
    else {

      this.gt = 0;
      this.showloader = false;
    }


  }
  transportclick() {

    if (this.trns == 1) {
      this.GetModeoftransportlist();
      this.trns++;
    }
    else {

      this.trns = 0;
      this.showloader = false;
    }

  }
  Godownclick() {

    if (this.GOD == 1) {
      this.GetGodownlist();
      this.GOD++;
    }
    else {

      this.GOD = 0;
      this.showloader = false;
    }

  }
  Compartmentclick() {

    if (this.comp == 1) {
      this.Getcompartmentlist();
      this.comp++;
    }
    else {

      this.comp = 0;
      this.showloader = false;
    }

  }
  Stackclick() {

    if (this.sta == 1) {
      this.getstacklist();
      this.sta++;
    }
    else {

      this.sta = 0;
      this.showloader = false;
    }


  }
  Patchclick() {

    if (this.pt == 1) {
      this.Getpatchlist();
      this.pt++;
    }
    else {

      this.pt = 0;
      this.showloader = false;
    }


  }

  Stacknoclick() {

    if (this.stkno == 1) {
      this.Getstacknolist();
      this.stkno++;
    }
    else {

      this.stkno = 0;
      this.showloader = false;
    }



  }

  Truckclick() {

    if (this.trk == 1) {
      this.Gettrucktypelist();
      this.trk++;
    }
    else {

      this.trk = 0;
      this.showloader = false;
    }



  }

  Gradeclick() {

    if (this.GRD == 1) {
      this.GetGradeslist();
      this.GRD++;
    }
    else {

      this.GRD = 0;
      this.showloader = false;
    }



  }

  pcategoryclick() {

    if (this.pctg == 1) {
      this.GetpCategorylist();
      this.pctg++;
    }
    else {

      this.pctg = 0;
      this.showloader = false;
    }


  }

  Commodityclick() {


    if (this.comd == 1) {
      this.GetCommoditieslist();
      this.comd++;
    }
    else {

      this.comd = 0;
      this.showloader = false;
    }


  }
  Commoditygroupclick() {

    if (this.comdg == 1) {
      this.GetcommoditiesGrouplist();
      this.comdg++;
    }
    else {

      this.comdg = 0;
      this.showloader = false;
    }


  }

  Storageclick() {


    if (this.strg == 1) {
      this.GetStoragetypeslist();
      this.strg++;
    }
    else {

      this.strg = 0;
      this.showloader = false;
    }



  }
  Quantityclick() {
    if (this.Qtc == 1) {
      this.GetQuantitylist();
      this.Qtc++;
    }
    else {

      this.Qtc = 0;
      this.showloader = false;
    }


  }
  Varietyclick() {

    if (this.var == 1) {
      this.GetVarietylist();
      this.var++;
    }
    else {

      this.var = 0;
      this.showloader = false;
    }
  }

  Qualityparametersclick() {

    if (this.qualprt == 1) {
      this.Getqualityparameters();
      this.qualprt++;
    }
    else {

      this.qualprt = 0;
      this.showloader = false;
    }

  }

  Chemicalclick() {

    if (this.che == 1) {
      this.GetChemicalslist();
      this.che++;
    }
    else {
      this.showloader = false;
      this.che = 0;
    }
  }

  Imprestclick() {

    if (this.imp == 1) {
      this.GetImprestlist();
      this.imp++;
    }
    else {
      this.showloader = false;
      this.imp = 0;
    }
  }

  changeCommodity() {

    this.QPcommoditygrparry = [];
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "COMMODITY_CODE_VIEW";
    req.INPUT_01 = this.QualityParameterAddForm.controls.QPcommddlQ.value;
    this.service.postData(req, "Getcommoditygrpbycomdty").subscribe(data => {
      if (data.StatusCode == "100") {
        this.QPcommoditygrparry = data.Details;
        this.QualityParameterAddForm.controls.QPcommgrpddl.setValue('');

        this.showloader = false;
      }
      else {
        this.commoditygrparry = [];
        this.QPcommodityVarietyarry = [];
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));


  }

  QPchangeCommoditygroup() {

    this.QPcommodityVarietyarry = [];
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "VARIETY";
    req.INPUT_01 = this.QualityParameterAddForm.controls.QPcommddlQ.value;
    req.INPUT_02 = this.QualityParameterAddForm.controls.QPcommgrpddl.value;
    this.service.postData(req, "Getcommoditygrpbycomdty").subscribe(data => {
      if (data.StatusCode == "100") {
        this.QPcommodityVarietyarry = data.Details;


        this.QualityParameterAddForm.controls.QPcommvarddl.setValue('');


        this.showloader = false;
      }
      else {
        this.commoditygrparry = [];
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }

  QPVarietychange() {


    this.GetGradeslist();
    this.QualityParameterAddForm.controls.QPGradeddl.setValue('');

  }
  QPGradechange() {



  }
  commoditygroupchange() {


  }

  COMMODITYGROUPFUNCTION(val: any) {
    this.commoditygrparry = [];
    this.showloader = true;
    const req = new InputRequest();
    req.TYPEID = "COMMODITY_CODE_VIEW";
    req.INPUT_01 = val.toString();
    this.service.postData(req, "Getcommoditygrpbycomdty").subscribe(data => {
      if (data.StatusCode == "100") {
        this.commoditygrparry = data.Details;


        this.showloader = false;
      }
      else {
        this.commoditygrparry = [];
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }


    },
      error => console.log(error));

  }

  commoditychange(val: string) {

    this.COMMODITYGROUPFUNCTION(this.Warehouseaddform.controls.commddlvar.value);

  }

  OKclick(){
    const req=new InputRequest();
    this.showloader = true;
    req.INPUT_01 = this.CommodityEDITForm.controls.commoditycode.value;
    req.INPUT_02 = this.CommodityEDITForm.controls.editStandardRate.value;
    req.INPUT_03 = this.CommodityEDITForm.controls.editHighrated1.value;
    req.INPUT_04 = this.CommodityEDITForm.controls.editHighrated2.value;
    req.INPUT_06 = this.CommodityEDITForm.controls.unqid.value;
    req.INPUT_09= this.CommodityEDITForm.controls.commdEDITremarks.value;
    req.USER_NAME = this.Username;
    req.INPUT_05 =this.datef.format( this.CommodityEDITForm.controls.edittransstdate.value);
    req.INPUT_07 = this.logUserrole;
    req.INPUT_08 =  this.CommodityEDITForm.controls.editinsuranc.value;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "updateCommodityGroupDetails").subscribe(data => {
      this.WHaddarray = data.result;
      // this.empaddarray=data.result;
      if (this.WHaddarray.StatusCode == "100") {

        if (this.WHaddarray.Details[0].rtN_ID == "1") {
          this.comdg = 0;
          this.WarehouseEDITform.reset();
          this.Warehouseaddform.reset();
          this.WarehouseRegioneditform.reset();
          this.CommodityEDITForm.reset();
          this.commodityGroupAddForm.reset();
          this.editcommodityparameterModal.hide();
          this.GetcommoditiesGrouplist();

          this.showloader = false;
          Swal.fire("success", "Data Updated Successfully", "success");

          // this.f.Godownswarehsetype.setValue(editid);
        }
        else {
          Swal.fire("info", "Data Updation Failed", "info");
          this.Warehouseaddform.reset();
          this.WarehouseEDITform.reset();
          this.WarehouseRegioneditform.reset();
          this.CommodityEDITForm.reset();
          this.commodityGroupAddForm.reset();
          this.editcommodityparameterModal.hide();
          this.showloader = false;


        }
      }

      else {
        Swal.fire("info", this.WHaddarray.StatusMessage, "info");
        this.editcommodityparameterModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));
    this.showloader = false;

  }

Cancelclick(){
this.reloadCurrentRoute();
}
reloadCurrentRoute() {
  let currentUrl = this.router.url;
  this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
      this.router.navigate([currentUrl]);
  });
}


  CommodityEDITclick() {
  
    const req = new InputRequest();
    this.comdEDITsubmitted = true;
    if (this.CommodityEDITForm.invalid) {
    
      return;
    }
  
  this.apprModal.show();
  }



  Weighbridgeaddclick() {

    this.showloader = true;
    const req = new InputRequest();
    this.weighbridgeadd = true;
    if (this.WeighbridgeAddForm.invalid) {
      this.showloader = false;
      return;
    }
    req.INPUT_01 = "WB_COMPANIES";
    req.INPUT_02 = this.WeighbridgeAddForm.controls.Weighbridgenametype.value;;
    req.INPUT_03 = this.WeighbridgeAddForm.controls.Weighbridgeremarks.value;
    req.INPUT_04 = this.datef.format(this.WeighbridgeAddForm.controls.MaintenanceStartDate.value);

    req.INPUT_05 = this.datef.format(this.WeighbridgeAddForm.controls.MaintenanceEndDate.value);
    req.INPUT_06 = this.WeighbridgeAddForm.controls.MaintenancePeriod.value;

    req.INPUT_07 = this.WeighbridgeAddForm.controls.MaintenanceValue.value;
    req.INPUT_08 = this.logUserrole;
    req.USER_NAME = this.Username;

    req.CALL_SOURCE = "Web";
    this.service.postData(req, "SaveWeighbridgeDetails").subscribe(data => {
      this.WHaddarray = data;
      if (this.WHaddarray.StatusCode == "100") {

        if (this.WHaddarray.Details[0].rtN_ID == "1") {
          this.wb = 0;
          this.WarehouseEDITform.reset();
          this.Warehouseaddform.reset();
          this.addWeighbridgeModal.hide();
          this.WeighbridgeAddForm.reset();
          this.WeighbridgeEDITForm.reset();
          //this.QualityParameterAddForm.reset();
          this.GetWeighbridgelist();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.commodityvardiv = false;
          this.commdivdivvalidationvar = false;
          this.showloader = false;
          Swal.fire("success", "Data Added Successfully", "success");

          // this.f.Godownswarehsetype.setValue(editid);
        }
        else {
          Swal.fire("info", "This Master IsAlready Registered", "info");

          this.Warehouseaddform.reset();
          this.WarehouseEDITform.reset();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.WeighbridgeAddForm.reset();
          this.WeighbridgeEDITForm.reset();
          this.addWeighbridgeModal.hide();

          this.showloader = false;

        }
      }

      else {
        Swal.fire("info", this.WHaddarray.StatusMessage, "info");
        this.QualityParameterEDITForm.reset();
        this.QualityParameterAddForm.reset();
        this.WeighbridgeAddForm.reset();
        this.WeighbridgeEDITForm.reset();
        this.addWeighbridgeModal.hide();

        this.showloader = false;

      }
    },
      error => console.log(error));

    this.showloader = false;

  }



  Warehouseaddclick() {

    this.showloader = true;
    const req = new InputRequest();
    this.submitted = true;
    if (this.Warehouseaddform.invalid) {
      this.showloader = false;
      return;
    }
    if (this.Title == "Region") {
      this.showloader = true;
      req.INPUT_01 = "REGIONAL";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.re = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.getregionlist();

            this.commodityvardiv = false;
            this.commdivdivvalidationvar = false;
            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");

            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;

            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();

          this.showloader = false;

        }
      },
        error => console.log(error));

    }
    if (this.Title == "Warehouse") {
      req.INPUT_01 = "WH_TYPE";
      this.showloader = true;
      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.Wr = 0;
            this.getwarehouselist();

            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            Swal.fire("success", "Data Added Successfully", "success");
            this.showloader = false;
            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();

          this.showloader = false;

        }
      },
        error => console.log(error));

    }

    if (this.Title == "Shed") {
      this.showloader = true;
      req.INPUT_01 = "SH_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.shc = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.Getshedslist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();

          this.showloader = false;

        }
      },
        error => console.log(error));

    }

    if (this.Title == "Gate") {
      this.showloader = true;
      req.INPUT_01 = "GATE_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.gt = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.Getgatenolist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();

          this.showloader = false;

        }
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Mode Of Transport") {
      this.showloader = true;
      req.INPUT_01 = "TRANSPORT_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.trns = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.GetModeoftransportlist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();

          this.showloader = false;

        }
      },
        error => console.log(error));

    }

    if (this.Title == "Godown") {
      this.showloader = true;
      req.INPUT_01 = "GODOWN_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.GOD = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.GetGodownlist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();

          this.showloader = false;

        }
      },
        error => console.log(error));
      this.showloader = false;
    }

    if (this.Title == "Compartment") {
      this.showloader = true;
      req.INPUT_01 = "COMPARTMENT_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.comp = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.Getcompartmentlist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();

          this.showloader = false;

        }
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Stack") {
      this.showloader = true;
      req.INPUT_01 = "STACK_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.sta = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.getstacklist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();

          this.showloader = false;

        }
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Packing") {
      this.showloader = true;
      req.INPUT_01 = "PACK_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.pt = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.Getpatchlist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();

          this.showloader = false;

        }
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Stackno") {
      this.showloader = true;
      req.INPUT_01 = "STACKNO_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.stkno = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.Getstacknolist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();

          this.showloader = false;

        }
      },
        error => console.log(error));
      this.showloader = false;
    }

    if (this.Title == "Truck") {
      this.showloader = true;
      req.INPUT_01 = "TRUCK_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.trk = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.Gettrucktypelist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));
      this.showloader = false;
    }

    if (this.Title == "Grade") {
      this.showloader = true;
      req.INPUT_01 = "GRADE_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.GRD = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.GetGradeslist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));
      this.showloader = false;
    }

    if (this.Title == "Category") {
      this.showloader = true;
      req.INPUT_01 = "PCATEGORY_TYPE";

      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.pctg = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.GetpCategorylist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));
      this.showloader = false;
    }

    if (this.Title == "Commodity") {
      this.showloader = true;
      req.INPUT_01 = "STORAGE_TYPE";
      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_04 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        // this.empaddarray=data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.comd = 0;
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.GetCommoditieslist();

            this.showloader = false;
            Swal.fire("success", "Data Added Successfully", "success");

            // this.f.Godownswarehsetype.setValue(editid);
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
            this.addWarehouseModal.hide();

          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));
      this.showloader = false;
    }


    if (this.Title == "Contract") {
      this.showloader = true;
      req.INPUT_01 = "CONTRACT_TYPE";
      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.INPUT_04 = this.logUserrole;
      req.USER_NAME = this.Username;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.strg = 0;
            this.GetStoragetypeslist();
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            Swal.fire("success", "Data Added Successfully", "success");
            this.showloader = false;
          }
          else {
            Swal.fire("info", "This Master IsAlready Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));

    }
    if (this.Title == "Quantity") {
      this.showloader = true;
      req.INPUT_01 = "QUANTITY_TYPE";
      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.INPUT_04 = this.logUserrole;
      req.USER_NAME = this.Username;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.Qtc = 0;
            this.GetQuantitylist();
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            Swal.fire("success", "Data Added Successfully", "success");
            this.showloader = false;
          }
          else {
            Swal.fire("info", "This Master Is Already Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));

    }



    if (this.Title == "Commodity Variety") {
      var ccm = this.Warehouseaddform.controls.commddlvar.value;
      var ccmg = this.Warehouseaddform.controls.commgrpddl.value;
      if (ccm == null || ccm == undefined || ccm == "") {
        this.commdivdivvalidationvar = true;
        this.showloader = false;
        return false;
      }
      if (ccmg == null || ccmg == undefined || ccmg == "") {
        this.commdgrpivdivvalidation = true;
        this.commdivdivvalidationvar = false;
        this.showloader = false;
        return false;
      }

      for (var i = 0; i < this.commoditylistypes.length; i++) {
        if (this.Warehouseaddform.controls.commddlvar.value == this.commoditylistypes[i].iteM_ID) {
          this.COMMODITYNAME = [];
          var commoditynamebyid = this.commoditylistypes[i].iteM_NAME;
          this.COMMODITYNAME.push(commoditynamebyid);
        }
        else {


        }

      }
      for (var i = 0; i < this.commoditygrparry.length; i++) {
        if (this.Warehouseaddform.controls.commgrpddl.value == this.commoditygrparry[i].commoditY_CODE) {
          this.commoditygrpname = [];
          var commoditygrpnamebyid = this.commoditygrparry[i].commodity;
          this.commoditygrpname.push(commoditygrpnamebyid);
        }

      }


      this.showloader = true;
      req.INPUT_01 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_02 = this.Warehouseaddform.controls.commgrpddl.value;
      req.INPUT_03 = this.commoditygrpname.toString();
      req.INPUT_04 = this.COMMODITYNAME.toString();
      req.INPUT_05 = this.Warehouseaddform.controls.commddlvar.value;
      req.INPUT_06 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.INPUT_07 = this.logUserrole;
      req.USER_NAME = this.Username;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "SavecommodityVarietyMaster").subscribe(data => {
        this.WHaddarray = data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.var = 0;
            this.GetVarietylist();
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            Swal.fire("success", "Data Added Successfully", "success");
            this.showloader = false;
          }
          else {
            Swal.fire("info", "This Master Is Already Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));

    }


    if (this.Title == "Farmer") {
      this.showloader = true;
      req.INPUT_01 = "FARMER_TYPE";
      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.INPUT_04 = this.logUserrole;
      req.USER_NAME = this.Username;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.far = 0;
            this.GetFarmerslist();
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            Swal.fire("success", "Data Added Successfully", "success");
            this.showloader = false;
          }
          else {
            Swal.fire("info", "This Master Is Already Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));

    }

    if (this.Title == "Chemical") {
      this.showloader = true;
      req.INPUT_01 = "CHEMICALS";
      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.INPUT_04 = this.logUserrole;
      req.USER_NAME = this.Username;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.che = 0;
            this.GetChemicalslist();
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            Swal.fire("success", "Data Added Successfully", "success");
            this.showloader = false;
          }
          else {
            Swal.fire("info", "This Master Is Already Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));

    }

    if (this.Title == "Imprest Payment") {
      this.showloader = true;
      req.INPUT_01 = "IMPREST_PAYMENT";
      req.INPUT_02 = this.Warehouseaddform.controls.Warehouseaddtype.value;
      req.INPUT_03 = this.Warehouseaddform.controls.Warehouseaddremarks.value;
      req.INPUT_04 = this.logUserrole;
      req.USER_NAME = this.Username;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "employeemasterreg").subscribe(data => {
        this.WHaddarray = data.result;
        if (this.WHaddarray.StatusCode == "100") {

          if (this.WHaddarray.Details[0].rtN_ID == "1") {
            this.imp = 0;
            this.GetImprestlist();
            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            Swal.fire("success", "Data Added Successfully", "success");
            this.showloader = false;
          }
          else {
            Swal.fire("info", "This Master Is Already Registered", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.addWarehouseModal.hide();
            this.showloader = false;
          }
        }

        else {
          Swal.fire("info", this.WHaddarray.StatusMessage, "info");
          this.addWarehouseModal.hide();
          this.showloader = false;
        }
      },
        error => console.log(error));

    }

  }

  WarehouseEDITclick() {
    this.showloader = true;

    const req = new InputRequest();

    this.EDITsubmitted = true;


    if (this.WarehouseEDITform.invalid) {
      this.showloader = false;
      return;

    }
    if (this.Title == "Region") {
      req.INPUT_01 = "REGIONAL";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.re = 0;
            this.getregionlist();

            this.WarehouseEDITform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");



        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
    }


    if (this.Title == "Warehouse") {

      req.INPUT_01 = "WH_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.Wr = 0;
            this.getwarehouselist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");



        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
    }

    if (this.Title == "shed") {

      req.INPUT_01 = "SH_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.shc = 0;
            this.Getshedslist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }

    if (this.Title == "Gate") {

      req.INPUT_01 = "GATE_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.gt = 0;
            this.Getgatenolist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }

    if (this.Title == "Mode Of Transport") {

      req.INPUT_01 = "TRANSPORT_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.trns = 0;
            this.GetModeoftransportlist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }

    if (this.Title == "Godown") {

      req.INPUT_01 = "GODOWN_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.GOD = 0;
            this.GetGodownlist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }

    if (this.Title == "Compartment") {

      req.INPUT_01 = "COMPARTMENT_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.comp = 0;
            this.Getcompartmentlist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Stack") {

      req.INPUT_01 = "STACK_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.sta = 0;
            this.getstacklist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Packing") {

      req.INPUT_01 = "PACK_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.pt = 0;
            this.Getpatchlist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Stackno") {

      req.INPUT_01 = "STACKNO_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.stkno = 0;
            this.Getstacknolist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Truck") {

      req.INPUT_01 = "TRUCK_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.trk = 0;
            this.Gettrucktypelist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");

          this.showloader = false;

        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Grade") {

      req.INPUT_01 = "GRADE_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.GRD = 0;
            this.GetGradeslist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");

          this.showloader = false;

        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Category") {

      req.INPUT_01 = "PCATEGORY_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.pctg = 0;
            this.GetpCategorylist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");

          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }
    if (this.Title == "Commodity") {

      req.INPUT_01 = "STORAGE_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.comd = 0;
            this.GetCommoditieslist();

            this.WarehouseEDITform.reset();

            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");

          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;
    }

    if (this.Title == "Contract") {

      req.INPUT_01 = "CONTRACT_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.strg = 0;
            this.GetStoragetypeslist();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.showloader = false;

            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.WarehouseRegioneditform.reset();
            this.showloader = false;

          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");

          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;

    }

    if (this.Title == "Quantity") {

      req.INPUT_01 = "QUANTITY_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.Qtc = 0;
            this.GetQuantitylist();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.showloader = false;

            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.WarehouseRegioneditform.reset();
            this.showloader = false;

          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;



        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;

    }


    if (this.Title == "Farmer") {

      req.INPUT_01 = "FARMER_TYPE";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.far = 0;
            this.GetFarmerslist();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.showloader = false;

            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.WarehouseRegioneditform.reset();
            this.showloader = false;

          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;



        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;

    }

    if (this.Title == "Commodity Variety") {

      req.INPUT_01 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_02 = this.WarehouseEDITform.controls.commEDITddlvar.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.commgrpEDITddl.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_05 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_06 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateCommodityVariety").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.var = 0;
            this.GetVarietylist();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            Swal.fire("success", "Data Updated Successfully", "success");

            this.showloader = false;
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.WarehouseRegioneditform.reset();

            this.showloader = false;
          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;


        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;

    }

    if (this.Title == "Chemical") {

      req.INPUT_01 = "CHEMICALS";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.che = 0;
            this.GetChemicalslist();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.showloader = false;

            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.WarehouseRegioneditform.reset();
            this.showloader = false;

          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;



        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;

    }

    if (this.Title == "Imprest Payment") {

      req.INPUT_01 = "IMPREST_PAYMENT";
      req.INPUT_02 = this.WarehouseEDITform.controls.WarehouseEDITId.value;
      req.INPUT_03 = this.WarehouseEDITform.controls.WHactive.value;
      req.INPUT_04 = this.WarehouseEDITform.controls.WarehouseEDITremarks.value;
      req.USER_NAME = this.Username;
      req.INPUT_05 = this.logUserrole;
      req.CALL_SOURCE = "Web";
      this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
        this.wheditarray = data.result;
        if (this.wheditarray.StatusCode == "100") {
          if (this.wheditarray.Details[0].rtN_ID == "1") {
            this.imp = 0;
            this.GetImprestlist();
            this.WarehouseEDITform.reset();
            this.WarehouseRegioneditform.reset();
            this.Warehouseaddform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.showloader = false;

            Swal.fire("success", "Data Updated Successfully", "success");
          }
          else {
            Swal.fire("info", "Data Updation Failed", "info");
            this.Warehouseaddform.reset();
            this.WarehouseEDITform.reset();
            this.addWarehouseModal.hide();
            this.editWarehouseModal.hide();
            this.WarehouseRegioneditform.reset();
            this.showloader = false;

          }
          // this.f.Godownswarehsetype.setValue(editid);
        }

        else {
          Swal.fire("info", this.wheditarray.StatusMessage, "info");
          this.showloader = false;



        }
        // this.f.editemptype.setValue(editname);
      },
        error => console.log(error));
      this.showloader = false;

    }
  }

  warehouseregionclick() {


    this.showloader = true;

    const req = new InputRequest();
    this.regionsubmitted = true;
    if (this.WarehouseRegioneditform.invalid) {
      this.showloader = false;
      return;

    }
    req.INPUT_01 = this.WarehouseEDITform.controls.WarehouseEDITId.value;;
    req.INPUT_02 = this.seldselarr.toString();
    req.INPUT_03 = this.WarehouseRegioneditform.controls.newregddl.value;
    req.INPUT_04 = this.WarehouseRegioneditform.controls.WarehouseregionEDITremarks.value;
    req.INPUT_05 = this.logUserrole;
    req.USER_NAME = this.Username;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "updateEmpmasterregs").subscribe(data => {
      this.regaarray = data.result;
      // this.empaddarray=data.result;
      if (this.regaarray.StatusCode == "100") {

        if (this.regaarray.Details[0].rtN_ID == "1") {

          this.Warehouseaddform.reset();

          this.regiondistmodal.hide();
          this.regionaldistricts(req.INPUT_01);

          this.seldselarr = [];

          this.showloader = false;
          Swal.fire("success", "Data updated Successfully", "success");
          // this.f.Godownswarehsetype.setValue(editid);
        }
        else {
          Swal.fire("info", this.regaarray.StatusMessage, "info");
          this.Warehouseaddform.reset();

          //  this.addWarehouseModal.hide();
          this.showloader = false;
          this.regiondistmodal.hide();

        }
      }

      else {
        Swal.fire("info", this.regaarray.StatusMessage, "info");
        this.regiondistmodal.hide();
        this.showloader = false;

      }
    },
      error => console.log(error));
  }




  AddWeighbridgetype() {

    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.QPEDITsubmitted = false;
    this.QPsubmitted = false;
    this.weighbridgeadd = false;
    this.WeighbridgeAddForm.reset();
    this.WeighbridgeEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.addWeighbridgeModal.show();
    this.WeighbridgeAddForm.controls.MaintenancePeriod.setValue('');
  }



  WeighBridgeEdit(weid, wename, westdate, weenddate, weiperiod, weivalue, weiact) {

    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.QPEDITsubmitted = false;
    this.QPsubmitted = false;
    this.weighbridgeadd = false;
    this.WeighbridgeEDITForm.controls.Weighbridgecmpid.setValue(weid);
    this.WeighbridgeEDITForm.controls.WeighbridgeCompanyname.setValue(wename);
    this.WeighbridgeEDITForm.controls.MaintenanceStartDate.setValue(westdate);
    this.WeighbridgeEDITForm.controls.MaintenanceEndDate.setValue(weenddate);
    this.WeighbridgeEDITForm.controls.MaintenancePeriod.setValue(weiperiod);
    this.WeighbridgeEDITForm.controls.MaintenanceValue.setValue(weivalue);

    this.WeighbridgeEDITForm.controls.weighbridgeactive.setValue(weiact.toString());
    this.editWeighbridgeModal.show();
  }


  QualityParameteraddclick() {

    this.showloader = true;
    const req = new InputRequest();
    this.QPsubmitted = true;
    if (this.QualityParameterAddForm.invalid) {
      this.showloader = false;
      return;
    }
    for (var i = 0; i < this.Gradetypes.length; i++) {
      if (this.QualityParameterAddForm.controls.QPGradeddl.value == this.Gradetypes[i].iteM_ID) {
        this.commoditygradename = [];
        var commoditygradenamebyid = this.Gradetypes[i].iteM_NAME;
        this.commoditygradename.push(commoditygradenamebyid);
      }


    }

    for (var i = 0; i < this.commoditylistypes.length; i++) {
      if (this.QualityParameterAddForm.controls.QPcommddlQ.value == this.commoditylistypes[i].iteM_ID) {
        this.QPCOMMODITYNAME = [];

        this.QPCOMMODITYNAME.push(this.commoditylistypes[i].iteM_NAME);
      }
      else {


      }

    }

    for (var i = 0; i < this.QPcommodityVarietyarry.length; i++) {
      if (this.QualityParameterAddForm.controls.QPcommvarddl.value == this.QPcommodityVarietyarry[i].varietY_ID) {
        this.commodityVARTname = [];
        var commodityVARTnamebyid = this.QPcommodityVarietyarry[i].varietY_NAME;
        this.commodityVARTname.push(commodityVARTnamebyid);
      }

    }

    for (var i = 0; i < this.QPcommoditygrparry.length; i++) {
      if (this.QualityParameterAddForm.controls.QPcommgrpddl.value == this.QPcommoditygrparry[i].commoditY_CODE) {
        this.QPCOMMODITYgrpNAME = [];
        var QPcommodityGrpnamebyid = this.QPcommoditygrparry[i].commodity;
        this.QPCOMMODITYgrpNAME.push(QPcommodityGrpnamebyid);
      }
    }
    req.INPUT_01 = this.QualityParameterAddForm.controls.QPGradeddl.value;
    req.INPUT_02 = this.commoditygradename.toString();

    req.INPUT_03 = this.QualityParameterAddForm.controls.QPcommvarddl.value;
    req.INPUT_04 = this.commodityVARTname.toString();

    req.INPUT_05 = this.QualityParameterAddForm.controls.QPcommgrpddl.value;
    req.INPUT_06 = this.QPCOMMODITYgrpNAME.toString();

    req.INPUT_07 = this.QPCOMMODITYNAME.toString();
    req.INPUT_08 = this.QualityParameterAddForm.controls.QPcommddlQ.value;
    req.INPUT_09 = this.QualityParameterAddForm.controls.QPRemarks.value;


    req.INPUT_10 = this.logUserrole;
    req.INPUT_11 = this.QualityParameterAddForm.controls.QpName.value;

    req.INPUT_12 = this.QualityParameterAddForm.controls.QPDescription.value;
    req.INPUT_13 = this.QualityParameterAddForm.controls.QPValue.value;

    req.INPUT_14 = this.QualityParameterAddForm.controls.QPPercentage.value;


    req.INPUT_15 = this.QualityParameterAddForm.controls.QPCharacterstics.value;

    req.USER_NAME = this.Username;

    req.CALL_SOURCE = "Web";
    this.service.postData(req, "SaveQualityParameter").subscribe(data => {
      this.WHaddarray = data.result;
      if (this.WHaddarray.StatusCode == "100") {

        if (this.WHaddarray.Details[0].rtN_ID == "1") {
          this.qualprt = 0;
          this.WarehouseEDITform.reset();
          this.Warehouseaddform.reset();
          this.addQualityparameterModal.hide();
          //this.QualityParameterAddForm.reset();
          this.Getqualityparameters();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.commodityvardiv = false;
          this.commdivdivvalidationvar = false;
          this.showloader = false;
          Swal.fire("success", "Data Added Successfully", "success");

          // this.f.Godownswarehsetype.setValue(editid);
        }
        else {
          Swal.fire("info", "This Master IsAlready Registered", "info");

          this.Warehouseaddform.reset();
          this.WarehouseEDITform.reset();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.addQualityparameterModal.hide();

          this.showloader = false;

        }
      }

      else {
        Swal.fire("info", this.WHaddarray.StatusMessage, "info");
        this.QualityParameterEDITForm.reset();
        this.QualityParameterAddForm.reset();
        this.addQualityparameterModal.hide();

        this.showloader = false;

      }
    },
      error => console.log(error));

  }


  QualityparameterEDITclick() {

    this.showloader = true;

    const req = new InputRequest();

    this.QPEDITsubmitted = true;


    if (this.QualityParameterEDITForm.invalid) {
      this.showloader = false;
      return;

    }
    req.INPUT_01 = this.vartie.toString();
    req.INPUT_02 = this.cmoigrp.toString();
    req.INPUT_03 = this.grds.toString();
    req.INPUT_04 = this.cmdst.toString();
    req.INPUT_08 = this.qid.toString();
    req.INPUT_05 = this.QualityParameterEDITForm.controls.QPactive.value;
    req.INPUT_06 = this.QualityParameterEDITForm.controls.QPEDITremarks.value;
    req.USER_NAME = this.Username;
    req.INPUT_07 = this.logUserrole;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "updateQualityParameter").subscribe(data => {
      this.wheditarray = data.result;
      if (this.wheditarray.StatusCode == "100") {
        if (this.wheditarray.Details[0].rtN_ID == "1") {
          this.qualprt = 0;
          this.Getqualityparameters();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.WarehouseEDITform.reset();
          this.Warehouseaddform.reset();
          this.editQualityparameterModal.hide();
          this.editWarehouseModal.hide();
          this.showloader = false;
          Swal.fire("success", "Data Updated Successfully", "success");
        }
        else {
          Swal.fire("info", "Data Updation Failed", "info");
          this.Warehouseaddform.reset();
          this.WarehouseEDITform.reset();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.editQualityparameterModal.hide();
          this.editWarehouseModal.hide();
          this.showloader = false;
        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.wheditarray.StatusMessage, "info");
        this.showloader = false;
        this.editQualityparameterModal.hide();

      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));
    this.showloader = false;
  }



  Weighbridgeeditclick() {

    this.WeighbridgeAddForm.reset();

    this.showloader = true;

    const req = new InputRequest();

    this.weighbridgeeditsubmit = true;


    if (this.WeighbridgeEDITForm.invalid) {
      this.showloader = false;
      return;

    }
    req.INPUT_01 = "WB_COMPANIES";;
    req.INPUT_02 = this.WeighbridgeEDITForm.controls.Weighbridgecmpid.value;
    req.INPUT_03 = this.WeighbridgeEDITForm.controls.weighbridgeeditremarks.value;
    req.INPUT_04 = this.WeighbridgeEDITForm.controls.weighbridgeactive.value;
    req.INPUT_05 = this.logUserrole;
    req.USER_NAME = this.Username;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "updateWeighbridgedetails").subscribe(data => {
      this.wheditarray = data.result;
      if (this.wheditarray.StatusCode == "100") {
        if (this.wheditarray.Details[0].rtN_ID == "1") {
          this.wb = 0;
          this.GetWeighbridgelist();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.WarehouseEDITform.reset();
          this.Warehouseaddform.reset();
          this.editQualityparameterModal.hide();
          this.editWeighbridgeModal.hide();
          this.showloader = false;
          Swal.fire("success", "Data Updated Successfully", "success");
        }
        else {
          Swal.fire("info", "Data Updation Failed", "info");
          this.Warehouseaddform.reset();
          this.WarehouseEDITform.reset();
          this.QualityParameterEDITForm.reset();
          this.QualityParameterAddForm.reset();
          this.editQualityparameterModal.hide();
          this.editWeighbridgeModal.hide();

          this.showloader = false;
        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.wheditarray.StatusMessage, "info");
        this.showloader = false;
        this.editWeighbridgeModal.hide();


      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));
    this.showloader = false;

  }






  AddQualityparametertype() {
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.QPEDITsubmitted = false;
    this.QPsubmitted = false;
    this.QPcommoditygrparry = [];
    this.QPcommodityVarietyarry = [];
    this.QualityParameterAddForm.reset();
    this.QualityParameterAddForm.controls.QPcommddlQ.setValue('');
    this.QualityParameterAddForm.controls.QPcommgrpddl.setValue('');
    this.QualityParameterAddForm.controls.QPcommvarddl.setValue('');
    this.QualityParameterAddForm.controls.QPGradeddl.setValue('');
    this.addQualityparameterModal.show();

  }
  hidequalityparameteraddModal() {
    this.QPsubmitted = false;
    this.QPEDITsubmitted = false;
    this.QualityParameterAddForm.reset();
    this.addQualityparameterModal.hide();
    this.editQualityparameterModal.hide();
    this.QualityParameterEDITForm.reset();
    this.QPEDITsubmitted = false;
    this.QPsubmitted = false;
  }



  hideeditWeighbridgeModal() {
    this.WeighbridgeEDITForm.reset();
    this.QPsubmitted = false;
    this.QPEDITsubmitted = false;
    this.weighbridgeadd = false;
    this.weighbridgeeditsubmit = false;
    this.QualityParameterAddForm.reset();
    this.addQualityparameterModal.hide();
    this.editWeighbridgeModal.hide();
    this.QualityParameterEDITForm.reset();
    this.QPEDITsubmitted = false;
    this.QPsubmitted = false;
  }


  hideWeighbridgeModal() {
    this.QPsubmitted = false;
    this.QPEDITsubmitted = false;
    this.weighbridgeadd = false;
    this.weighbridgeeditsubmit = false;
    this.QualityParameterAddForm.reset();
    this.addQualityparameterModal.hide();
    this.addWeighbridgeModal.hide();
    this.QualityParameterEDITForm.reset();
    this.QPEDITsubmitted = false;
    this.QPsubmitted = false;
  }






  hideeditQualityparameterModal() {

    this.QPsubmitted = false;
    this.QPEDITsubmitted = false;
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.addQualityparameterModal.hide();
    this.editQualityparameterModal.hide();

  }

  WHQualityparameterEdit(t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14, t15) {
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.QPEDITsubmitted = false;
    this.QPsubmitted = false;

    this.qid = [];
    this.cmdst = [];
    this.cmoigrp = [];
    this.vartie = [];
    this.grds = [];

    this.qid.push(t11);
    this.cmdst.push(t15);
    this.cmoigrp.push(t14);
    this.vartie.push(t13);
    this.grds.push(t12);

    this.QualityParameterEDITForm.controls.QpEDITCommodity.setValue(t1);
    this.QualityParameterEDITForm.controls.QPeditGroupname.setValue(t2);
    this.QualityParameterEDITForm.controls.QPeditVariety.setValue(t3);
    this.QualityParameterEDITForm.controls.QPeditgrade.setValue(t4);
    this.QualityParameterEDITForm.controls.QPeditName.setValue(t5);
    this.QualityParameterEDITForm.controls.QPeditvalue.setValue(t6);
    this.QualityParameterEDITForm.controls.QPeditdescription.setValue(t7);
    this.QualityParameterEDITForm.controls.QPeditPercentage.setValue(t8);
    this.QualityParameterEDITForm.controls.QPeditCharacterstics.setValue(t9);
    this.QualityParameterEDITForm.controls.QPactive.setValue(t10.toString());
    this.editQualityparameterModal.show();

  }

  AddStoragetype() {

    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.Title = "Contract";
    this.submitted = false;
    this.addWarehouseModal.show();
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;

  }
  WHpStorageEdit(stgid, stgname, stgact) {
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.commdivdivvalidation = false;
    const req = new InputRequest();
    this.Title = "Contract";
    this.EDITsubmitted = false;
    req.INPUT_01 = "CONTRACT_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.f.WarehouseEDITId.setValue(stgid);
        this.f.WarehouseEDITtype.setValue(stgname);
        this.f.WHactive.setValue(stgact.toString());
        this.editWarehouseModal.show();
      }
    },
      error => console.log(error));
    this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseRegioneditform.reset();
  }


  AddVarietytype() {

    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.Title = "Commodity Variety";
    this.submitted = false;
    this.addWarehouseModal.show();
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = true;
    this.commGroupdiv = true;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.Warehouseaddform.controls.commddlvar.setValue('');
    this.Warehouseaddform.controls.commgrpddl.setValue('');
    this.GetCommoditieslist();
    this.commoditygrparry = [];
  }


  WHVarietyEdit(cmdstgid, cmdgrpid, varietY_ID, varietY_NAME, cmdvaract) {

    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    //this.GetCommoditieslist();
    this.WarehouseEDITform.controls.commEDITddlvar.setValue(cmdstgid);
    this.COMMODITYGROUPFUNCTION(cmdstgid);
    this.commdivdivvalidation = false;
    const req = new InputRequest();
    this.Title = "Commodity Variety";
    this.EDITsubmitted = false;
    req.TYPEID = "VARIETY_VIEW";
    this.service.postData(req, "GetVarietylist").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.commgrpEDITddl.setValue(cmdgrpid.toString());
        this.f.WarehouseEDITId.setValue(varietY_ID);
        this.f.WarehouseEDITtype.setValue(varietY_NAME);
        this.f.WHactive.setValue(cmdvaract.toString());
        this.editWarehouseModal.show();
      }
    },
      error => console.log(error));
    this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.commodityvardiv = true;
    this.commdivdivvalidationvar = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.WarehouseRegioneditform.reset();
    this.commodityvardiv = false;
    this.commGroupdiv = false;

    this.commodityEDITvardiv = true;
    this.commGroupEDITdiv = true;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

  }



  AddFarmertype() {

    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.Title = "Farmer";
    this.submitted = false;
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset(); this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.addWarehouseModal.show();
    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;

  }


  AddQuantitytype() {

    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.Title = "Quantity";
    this.submitted = false;
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset(); this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.addWarehouseModal.show();
    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
  }

  WHFarmerEdit(FARID, FARNAME, FARACT) {


    this.commdivdivvalidation = false;
    const req = new InputRequest();
    this.Title = "Farmer";
    this.EDITsubmitted = false;
    req.INPUT_01 = "FARMER_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(FARID);
        this.f.WarehouseEDITtype.setValue(FARNAME);
        this.f.WHactive.setValue(FARACT.toString());
        this.editWarehouseModal.show();
      }
    },
      error => console.log(error));
    this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.WarehouseRegioneditform.reset();
    this.commodityvardiv = false;
    this.commGroupdiv = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
  }



  WHQuantityEdit(qtyid, qtyname, qtyact) {

    this.commdivdivvalidation = false;
    const req = new InputRequest();
    this.Title = "Quantity";
    this.EDITsubmitted = false;
    req.INPUT_01 = "QUANTITY_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(qtyid);
        this.f.WarehouseEDITtype.setValue(qtyname);
        this.f.WHactive.setValue(qtyact.toString());
        this.editWarehouseModal.show();
      }
    },
      error => console.log(error));
    this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.WarehouseRegioneditform.reset();
    this.commodityvardiv = false;
    this.commGroupdiv = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
  }


  AddCommoditygrouptpype() {

    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.Title = "CommodityPrice";
    this.submitted = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.addcommodityparameterModal.show();
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    //    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset(); this.commdsubmited = false
    this.comdEDITsubmitted = false;
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commGroupdiv = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.ddl = "commoditygroupadd";
    this.commodityGroupAddForm.controls.commddddltype.setValue('');
  }

  AddChemical() {

    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.Title = "Chemical";
    this.submitted = false;
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset(); this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.addWarehouseModal.show();
    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;

  }

  ChemicalEdit(CHEID, CHENAME, CHEACT) {


    this.commdivdivvalidation = false;
    const req = new InputRequest();
    this.Title = "Chemical";
    this.EDITsubmitted = false;
    req.INPUT_01 = "CHEMICALS";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(CHEID);
        this.f.WarehouseEDITtype.setValue(CHENAME);
        this.f.WHactive.setValue(CHEACT.toString());
        this.editWarehouseModal.show();
      }
    },
      error => console.log(error));
    this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.WarehouseRegioneditform.reset();
    this.commodityvardiv = false;
    this.commGroupdiv = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
  }

  AddImprest() {

    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.Title = "Imprest Payment";
    this.submitted = false;
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset(); this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.addWarehouseModal.show();
    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;

  }

  ImprestEdit(IMPID, IMPNAME, IMPACT) {


    this.commdivdivvalidation = false;
    const req = new InputRequest();
    this.Title = "Imprest Payment";
    this.EDITsubmitted = false;
    req.INPUT_01 = "IMPREST_PAYMENT";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(IMPID);
        this.f.WarehouseEDITtype.setValue(IMPNAME);
        this.f.WHactive.setValue(IMPACT.toString());
        this.editWarehouseModal.show();
      }
    },
      error => console.log(error));
    this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.WarehouseRegioneditform.reset();
    this.commodityvardiv = false;
    this.commGroupdiv = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
  }

  commoditygroupEdit(rateid,storageid, comdgrpid, comdgrpname, comdpackage, comdntwt, comdrate, comdrate1, comdrate2, comdact,startd,enddate,unqid,insurance) {
    this.Warehouseaddform.reset();
    this.WarehouseEDITform.reset();
    this.commdsubmited = false
    this.comdEDITsubmitted = false;
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.WarehouseRegioneditform.reset();
    this.CommodityEDITForm.controls.CommodityEDITFormddl.setValue(storageid);

    this.CommodityEDITForm.controls.editCommodityGroupid.setValue(comdgrpid);
    this.CommodityEDITForm.controls.editCommodityGroupname.setValue(comdgrpname);
    this.CommodityEDITForm.controls.editPackageType.setValue(comdpackage);
    this.CommodityEDITForm.controls.editNetWeight.setValue(comdntwt);
    //this.CommodityEDITForm.controls.editStandardRate.setValue(comdrate);
    //this.CommodityEDITForm.controls.editHighrated1.setValue(comdrate1);
    //this.CommodityEDITForm.controls.editHighrated2.setValue(comdrate2);
    //this.CommodityEDITForm.controls.comdeditactive.setValue(comdact.toString());

    
    //this.CommodityEDITForm.controls.edittransstdate.setValue(startd);
    //this.CommodityEDITForm.controls.edittransenddate.setValue(enddate);

    //this.CommodityEDITForm.controls.editinsuranc.setValue(insurance);
    this.CommodityEDITForm.controls.unqid.setValue(rateid);
    this.CommodityEDITForm.controls.commoditycode.setValue(comdgrpid);

    this.EDITsubmitted = false;
    this.GetcommoditiesRateslist(rateid);
    this.editcommodityparameterModal.show();

  }

  AddCommoditytpype() {
    this.commdivdivvalidation = false;
    this.Title = "Commodity Group";
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.WarehouseEDITform.reset();
    this.QualityParameterEDITForm.reset();
    this.QualityParameterAddForm.reset();
    this.WarehouseRegioneditform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.submitted = false;
    this.addWarehouseModal.show();
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
  }
  CommodityEdit(comdid, comdname, comdact) {

    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.commdivdivvalidation = false;
    const req = new InputRequest();
    this.Title = "Commodity Group";
    this.EDITsubmitted = false;
    req.TYPEID = "COMMODITY_LIST";
    this.service.postData(req, "Getcommodities").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(comdid);
        this.f.WarehouseEDITtype.setValue(comdname);
        this.f.WHactive.setValue(comdact.toString());
        this.editWarehouseModal.show();

      }

    },
      error => console.log(error));
    this.regionadddropdown = false;;
    this.regioneditdrpdwn = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.WarehouseRegioneditform.reset();
  }


  AddWHRegion() {

    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.Title = "Region";
    this.commdivdiv = false;
    this.commdivdivvalidation = false;
    this.getInactivedist();
    this.addWarehouseModal.show();
    this.regionadddropdown = true;;
    this.regioneditdrpdwn = false;
    this.commeditdiv = false;
    this.commdivdiv = false;
    this.editActdctdiv = false;
    this.editRemarksdiv = false;
    this.eidtupdatebtn = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
  }


  WHHistory(val) {
    const req = new InputRequest();

    if (val == "regionhistory") {

      req.INPUT_01 = "REGIONAL";
      req.INPUT_02 = "MASTERS";

    }
    else if (val == "Warehousehist") {
      req.INPUT_01 = "WH_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Shedhistory") {
      req.INPUT_01 = "SH_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Gateshistory") {
      req.INPUT_01 = "GATE_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Transporthistory") {
      req.INPUT_01 = "TRANSPORT_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Godownhistory") {
      req.INPUT_01 = "GODOWN_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Compartmenthistory") {
      req.INPUT_01 = "COMPARTMENT_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Stackhistory") {
      req.INPUT_01 = "STACK_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Patchinghistory") {
      req.INPUT_01 = "PACK_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "stacknohistory") {
      req.INPUT_01 = "STACKNO_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "truckhistory") {
      req.INPUT_01 = "TRUCK_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Gradehistory") {
      req.INPUT_01 = "GRADE_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Categoryhistory") {
      req.INPUT_01 = "PCATEGORY_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Commodityhist") {
      req.INPUT_01 = "STORAGE_TYPE";
      req.INPUT_02 = "MASTERS";
    }

    else if (val == "Commoditygrouphist") {

      req.INPUT_01 = null;
      req.INPUT_02 = "STORAGE_CHARGES";
    }
    else if (val == "Storagehistory") {

      req.INPUT_01 = "CONTRACT_TYPE";
      req.INPUT_02 = "MASTERS";
    }
    else if (val == "Quantityhistory") {

      req.INPUT_01 = "QUANTITY_TYPE";
      req.INPUT_02 = "MASTERS";
    }


    else if (val == "Varietyhistory") {

      req.INPUT_01 = null;
      req.INPUT_02 = "VARIETY";
    }
    else if (val == "Farmerhistory") {

      req.INPUT_01 = "FARMER_TYPE";
      req.INPUT_02 = "MASTERS";
    }

    else if (val == "Qualityparamterhistory") {

      req.INPUT_01 = null;
      req.INPUT_02 = "QUALITY_PARAMETER";
    }
    else if (val == "Weighbridgehistory") {

      req.INPUT_01 = "WB_COMPANIES";
      req.INPUT_02 = "MASTERS";
    }

    else if (val == "Chemicalhistory") {

      req.INPUT_01 = "CHEMICALS";
      req.INPUT_02 = "MASTERS";
    }

    else if (val == "Impresthistory") {

      req.INPUT_01 = "IMPREST_PAYMENT";
      req.INPUT_02 = "MASTERS";
    }

    else {
      req.INPUT_01 = "";
      req.INPUT_02 = "";
      Swal.fire("info", "No history found", "info");
      return false;
    }

    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.WarehouseEDITform.reset();
        this.Warehouseaddform.reset();
        this.WarehouseRegioneditform.reset();
        this.historyWarehousemodal.show();
        this.showloader = false;

      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.WarehouseEDITform.reset();
        this.Warehouseaddform.reset();
        this.WarehouseRegioneditform.reset();
        this.historyWarehousemodal.hide();
        this.showloader = false;

      }
    },
      error => console.log(error));
    this.showloader = false;
  }



  hidehistoryempModal(): void {
    this.Warehouseaddform.reset();
    this.commdivdivvalidation = false;
    this.WarehouseEDITform.reset();
    this.historyWarehousemodal.hide();
  }
  WHRegionEdit(regid, regname, regact) {
    //this.selectedItems1 = [];

    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.commodityGroupAddForm.reset();
    this.CommodityEDITForm.reset();
    this.WarehouseRegioneditform.reset();
    this.commdivdiv = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commeditdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commdivdivvalidation = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.editActdctdiv = false;
    this.editRemarksdiv = false;
    this.eidtupdatebtn = false;
    this.WarehouseRegioneditform.controls.newregddl.setValue('0');

    this.Title = "Region";
    const req = new InputRequest();
    req.INPUT_01 = "REGIONAL";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(regid);
        this.f.WarehouseEDITtype.setValue(regname);
        this.f.WHactive.setValue(regact.toString());
        this.regionaldistricts(regid);

      }

    },
      error => console.log(error));





    req.INPUT_02 = regid;
    this.service.postData(req, "GetRegionDetailsbydistcode").subscribe(data => {

      if (data.StatusCode == "100") {
        //this.regiondistricts = data.Details;
        this.selectedItems11 = [];

        //this.regdist = data.Details;
        this.reg1dist = data.Details;
        this.regionadddropdown = false;
        //     //this.selectedIregistertems.push({ disCODE: item.districT_CODE, districTNAME: item.districT_NAME_ENG })
        this.editWarehouseModal.show();
        for (var j = 0; j < this.reg1dist.length; j++) {
          var distid = parseInt(this.reg1dist[j].districT_CODE);

          this.selectedItems11.push({
            iteM_ID: distid,
            iteM_NAME: this.reg1dist[j].districT_NAME
          });
        }
        console.log(this.selectedItems11);

        this.regioneditdrpdwn = true;
        this.commdivdivvalidation = false;

      }


      else {
        this.showloader = false;
        this.regionadddropdown = false;
        this.regioneditdrpdwn = false;
        this.commdivdivvalidation = false;
        Swal.fire("info", data.StatusMessage, "info");
      }



    });


  }


  regionaldistricts(reg1id) {
    const req = new InputRequest();

    req.TYPEID = "REGIONAL_DISTRICTS";
    req.INPUT_01 = reg1id;
    this.service.postData(req, "GetRegionMasterDetails").subscribe(data => {

      if (data.StatusCode == "100") {
        //this.regiondistricts = data.Details;
        //  this.selectedItems11 = [];
        this.regdist = data.Details;
        //      this.reg1dist = data.Details;
        this.regionadddropdown = false;
        this.editWarehouseModal.show();
        //    console.log(this.selectedItems11);

        this.regioneditdrpdwn = true;
        this.commdivdivvalidation = false;

      }


      else {
        this.showloader = false;
        this.commdivdivvalidation = false;
        this.regdist = [];
        //Swal.fire("info", data.StatusMessage, "info");
      }



    });


  }

  hidehistorymasterModal() {

    this.historyWarehousemodal.hide();
    this.submitted = false;
    this.EDITsubmitted = false;
    this.Warehouseaddform.reset();
    this.WarehouseEDITform.reset();
    this.WarehouseRegioneditform.reset();
    this.commdivdivvalidation = false;
  }

  hideWarehouseModal() {
    this.submitted = false;
    this.EDITsubmitted = false;
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
    this.addWarehouseModal.hide();
    this.commdivdivvalidation = false;
  }
  hideeditWarehouseModal() {
    this.EDITsubmitted = false;
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
    this.editWarehouseModal.hide();
    this.commdivdivvalidation = false;
  }





  AddWHtpype() {
    this.Title = "Warehouse";
    this.addWarehouseModal.show();
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.commdivdivvalidation = false;
    this.commdivdiv = false;
    this.WarehouseRegioneditform.reset();
    this.commeditdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
  }

  WHTypeHistory() {
    this.Title = "Warehouse";
    this.regionadddropdown = false;
    this.historyWarehousemodal.show();
    this.regioneditdrpdwn = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commdivdiv = false;
    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseRegioneditform.reset();
  }
  WHTypeEdit(whid, whname, whact) {

    this.Title = "Warehouse";
    const req = new InputRequest();
    req.INPUT_01 = "WH_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(whid);
        this.f.WarehouseEDITtype.setValue(whname);
        this.f.WHactive.setValue(whact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commdivdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.editWarehouseModal.show();
    this.WarehouseRegioneditform.reset();
  }



  Addshed() {

    this.Title = "Shed";
    this.addWarehouseModal.show();
    this.submitted = false;
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commdivdiv = false;
    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseRegioneditform.reset();
  }
  AddGateno() {

    this.Title = "Gate";
    this.addWarehouseModal.show();
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.submitted = false;
    this.commdivdivvalidation = false;
    this.commdivdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commeditdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseRegioneditform.reset();
  }

  AddWhGodown() {
    this.Title = "Godown";
    this.addWarehouseModal.show();
    this.submitted = false;
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commdivdiv = false;
    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseRegioneditform.reset();

  }

  WHGodownEdit(godnid, godnname, godnact) {

    this.Title = "Godown";
    const req = new InputRequest();
    req.INPUT_01 = "GODOWN_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(godnid);
        this.f.WarehouseEDITtype.setValue(godnname);
        this.f.WHactive.setValue(godnact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commdivdiv = false;
    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseRegioneditform.reset();
    this.editWarehouseModal.show();


  }


  WHShedEdit(shedid, shedname, shedact) {
    this.Title = "Shed";
    const req = new InputRequest();
    req.INPUT_01 = "SH_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(shedid);
        this.f.WarehouseEDITtype.setValue(shedname);
        this.f.WHactive.setValue(shedact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commdivdiv = false;
    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseRegioneditform.reset();
    this.editWarehouseModal.show();

  }

  WHGatenoEdit(gateid, gatename, gateact) {

    this.Title = "Gate";
    const req = new InputRequest();
    req.INPUT_01 = "GATE_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(gateid);
        this.f.WarehouseEDITtype.setValue(gatename);
        this.f.WHactive.setValue(gateact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commdivdiv = false;
    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseRegioneditform.reset();
    this.editWarehouseModal.show();


  }

  AddWhtransport() {
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
    this.commdivdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.Title = "Mode Of Transport";

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.addWarehouseModal.show();
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.submitted = false;
  }


  WHTransportEdit(tnsid, tnsname, tnsact) {

    this.Title = "Mode Of Transport";
    const req = new InputRequest();
    req.INPUT_01 = "TRANSPORT_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(tnsid);
        this.f.WarehouseEDITtype.setValue(tnsname);
        this.f.WHactive.setValue(tnsact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commdivdiv = false;
    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.editWarehouseModal.show();
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
  }


  AddWhCompartment() {

    this.Title = "Compartment";

    this.commdivdiv = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
    this.commeditdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commdivdivvalidation = false;
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.addWarehouseModal.show();
    this.submitted = false;

  }

  WHCompartmentEdit(cmpid, cmpname, cmpact) {

    this.Title = "Compartment";
    const req = new InputRequest();
    req.INPUT_01 = "COMPARTMENT_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(cmpid);
        this.f.WarehouseEDITtype.setValue(cmpname);
        this.f.WHactive.setValue(cmpact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commdivdiv = false;
    this.commdivdivvalidation = false;
    this.commeditdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.editWarehouseModal.show();
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
  }

  AddWhStack() {


    this.Title = "Stack";
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.addWarehouseModal.show();

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commdivdiv = false;

    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
    this.submitted = false;
  }
  WHstackEdit(stkid, stkname, stkact) {

    this.Title = "Stack";
    const req = new InputRequest();
    req.INPUT_01 = "STACK_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(stkid);
        this.f.WarehouseEDITtype.setValue(stkname);
        this.f.WHactive.setValue(stkact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commdivdiv = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.editWarehouseModal.show()
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
  }
  AddWhPatching() {

    this.submitted = false;
    this.Title = "Packing";
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commdivdiv = false;
    this.WarehouseRegioneditform.reset();
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.addWarehouseModal.show();

  }

  WHPatchEdit(pchid, pchname, pchact) {

    this.Title = "Packing";
    const req = new InputRequest();
    req.INPUT_01 = "PACK_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(pchid);
        this.f.WarehouseEDITtype.setValue(pchname);
        this.f.WHactive.setValue(pchact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commdivdiv = false;

    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
    this.editWarehouseModal.show();
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
  }

  AddWhstackno() {
    this.Title = "Stackno";
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.WarehouseRegioneditform.reset();
    this.commdivdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.addWarehouseModal.show();
    this.submitted = false;

  }
  WHstacknoEdit(stcknoid, stackname, stackact) {


    this.Title = "Stackno";
    const req = new InputRequest();
    req.INPUT_01 = "STACKNO_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(stcknoid);
        this.f.WarehouseEDITtype.setValue(stackname);
        this.f.WHactive.setValue(stackact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commdivdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.editWarehouseModal.show();
  }

  AddWhtruck() {

    this.Title = "Truck";
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commdivdiv = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.addWarehouseModal.show();
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.submitted = false;
  }
  WHtruckEdit(trkid, trkname, trkact) {


    this.Title = "Truck";
    const req = new InputRequest();
    req.INPUT_01 = "TRUCK_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(trkid);
        this.f.WarehouseEDITtype.setValue(trkname);
        this.f.WHactive.setValue(trkact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commdivdiv = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.commodityvardiv = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.editWarehouseModal.show();

  }


  AddWhGrade() {

    this.Title = "Grade";
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commdivdiv = false;
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.addWarehouseModal.show();
    this.submitted = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.WarehouseRegioneditform.reset();
  }

  WHGradeEdit(gradeid, gradename, gradeact) {


    this.Title = "Grade";
    const req = new InputRequest();
    req.INPUT_01 = "GRADE_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(gradeid);
        this.f.WarehouseEDITtype.setValue(gradename);
        this.f.WHactive.setValue(gradeact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commdivdiv = false;
    this.WarehouseRegioneditform.reset();
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.editWarehouseModal.show();



  }
  AddWhpcategory() {

    this.Title = "Category";
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;
    this.addWarehouseModal.show();
    this.WarehouseRegioneditform.reset();
    this.commdivdiv = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.eidtupdatebtn = true;
    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.commodityvardiv = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commdivdivvalidationvar = false;
    this.commodityvardiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.submitted = false;

  }

  WHpcategoryEdit(pctgid, pctgname, pctgact) {


    this.Title = "Category";
    const req = new InputRequest();
    req.INPUT_01 = "PCATEGORY_TYPE";
    this.service.postData(req, "GetMasterDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        this.f.WarehouseEDITId.setValue(pctgid);
        this.f.WarehouseEDITtype.setValue(pctgname);
        this.f.WHactive.setValue(pctgact.toString());
      }

    },
      error => console.log(error));
    this.regionadddropdown = false;
    this.regioneditdrpdwn = false;

    this.commdivdiv = false;

    this.commeditdiv = false;
    this.commdivdivvalidation = false;
    this.WarehouseEDITform.reset();
    this.Warehouseaddform.reset();
    this.editActdctdiv = true;
    this.editRemarksdiv = true;
    this.commodityvardiv = false;
    this.commdivdivvalidationvar = false;
    this.eidtupdatebtn = true;
    this.commodityvardiv = false;

    this.commodityEDITvardiv = false;
    this.commGroupEDITdiv = false;
    this.commGroupdiv = false;
    this.commdgrpivdivvalidation = false;
    this.commdivdivvalidationvar = false;
    this.editWarehouseModal.show();
    this.WarehouseRegioneditform.reset();
  }


  hideeditcommodityModal() {

    this.comdEDITsubmitted = false;
    this.commdsubmited = false;
    this.Warehouseaddform.reset();
    this.addWarehouseModal.hide();
    this.WarehouseRegioneditform.reset();
    this.CommodityEDITForm.reset();
    this.commodityGroupAddForm.reset();
    this.addcommodityparameterModal.hide();
    this.editcommodityparameterModal.hide();
  }

  hideaddCommodityModal() {

    this.WarehouseEDITform.reset();
    this.comdEDITsubmitted = false;
    this.commdsubmited = false;
    this.Warehouseaddform.reset();
    this.addWarehouseModal.hide();
    this.WarehouseRegioneditform.reset();
    this.CommodityEDITForm.reset();
    this.commodityGroupAddForm.reset();
    this.addcommodityparameterModal.hide();


  }
  commoditytypeaddclick() {



    const req = new InputRequest();

    this.commdsubmited = true;


    if (this.commodityGroupAddForm.invalid) {
      this.showloader = false;
      return;

    }
    this.showloader = true;
    req.INPUT_01 = this.commodityGroupAddForm.controls.comdgrpnametype.value;
    req.INPUT_02 = this.commodityGroupAddForm.controls.commddddltype.value;
    req.INPUT_03 = this.commodityGroupAddForm.controls.comdpkgtype.value;
    req.INPUT_04 = this.commodityGroupAddForm.controls.comdWeight.value;
    req.INPUT_05 = this.commodityGroupAddForm.controls.comdRate.value;
    req.INPUT_06 = this.commodityGroupAddForm.controls.comdhighrated1.value;
    req.INPUT_07 = this.commodityGroupAddForm.controls.comdhighrated2.value;
    req.INPUT_08 = this.commodityGroupAddForm.controls.comdremarks.value;
    req.INPUT_09 = this.logUserrole;
    req.INPUT_10 = this.datef.format( this.commodityGroupAddForm.controls.TransStartDate.value);
   // req.INPUT_11 =this.datef.format( this.commodityGroupAddForm.controls.TransEndDate.value);
    req.INPUT_12 = this.commodityGroupAddForm.controls.addcomdact.value;
    

    req.USER_NAME = this.Username;
    req.CALL_SOURCE = "Web";
    this.service.postData(req, "SaveCommodityGroupDetails").subscribe(data => {
      this.commoditygrouparray = data.result;
      if (this.commoditygrouparray.StatusCode == "100") {
        if (this.commoditygrouparray.Details[0].rtN_ID == "1") {
          this.comd = 0;
          this.GetcommoditiesGrouplist();

          this.WarehouseEDITform.reset();

          this.Warehouseaddform.reset();
          this.addWarehouseModal.hide();
          this.WarehouseRegioneditform.reset();
          this.CommodityEDITForm.reset();
          this.commodityGroupAddForm.reset();
          this.addcommodityparameterModal.hide();
          this.showloader = false;

          Swal.fire("success", "Data Added Successfully", "success");

        }
        else {
          Swal.fire("info", "This Master Is Already Registered", "info");
          this.Warehouseaddform.reset();
          this.WarehouseEDITform.reset();
          this.addWarehouseModal.hide();
          this.WarehouseRegioneditform.reset();
          this.CommodityEDITForm.reset();
          this.commodityGroupAddForm.reset();
          this.addcommodityparameterModal.hide();
          this.showloader = false;

        }
        // this.f.Godownswarehsetype.setValue(editid);
      }

      else {
        Swal.fire("info", this.commoditygrouparray.StatusMessage, "info");
        this.addcommodityparameterModal.hide();
        this.showloader = false;


      }
      // this.f.editemptype.setValue(editname);
    },
      error => console.log(error));
    this.showloader = false;


  }



  get f1() { return this.Warehouseaddform.controls; }

  get f() { return this.WarehouseEDITform.controls; }


  get f2() { return this.WarehouseRegioneditform.controls }

  get QP() { return this.QualityParameterAddForm.controls }

  get QP1() { return this.QualityParameterEDITForm.controls }


  get comdity() { return this.commodityGroupAddForm.controls }

  get comdity1() { return this.CommodityEDITForm.controls }

  get reg() { return this.WeighbridgeAddForm.controls }
  get weiedit() { return this.WeighbridgeEDITForm.controls }
}
