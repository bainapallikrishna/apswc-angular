import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseReservationMatserComponent } from './warehouse-reservation-matser.component';

describe('WarehouseReservationMatserComponent', () => {
  let component: WarehouseReservationMatserComponent;
  let fixture: ComponentFixture<WarehouseReservationMatserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseReservationMatserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseReservationMatserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
