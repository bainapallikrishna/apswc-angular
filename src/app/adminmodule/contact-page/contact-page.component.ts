import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import {WareHouseMaster} from 'src/app/Interfaces/user';
import { FormBuilder, FormGroup,Validators} from '@angular/forms';
import {CommonServices} from 'src/app/Services/common.services';
import {AadharValidateService} from 'src/app/Services/aadhar-validate.service';
import Swal from 'sweetalert2';
import { InputRequest } from 'src/app/Interfaces/employee';

@Component({
  selector: 'app-contact-page',
  templateUrl: './contact-page.component.html',
  styleUrls: ['./contact-page.component.css']
})
export class ContactPageComponent implements OnInit {

  contactForm:FormGroup;
  submitted =false;
  PanPattern="[A-Z]{5}[0-9]{4}[A-Z]{1}";
  IFSCPattern="^[A-Z]{4}0[A-Z0-9]{6}$";
 
  public warehouse:WareHouseMaster[]=[];
  districts: any[];
  uidstatus: Boolean= true;
  mob_uid_isvalid:Boolean =true;
  public districtList:any;
  public selecteddistrictname="0";
  public selectedWarehousename="0";
  public whmanagername:string;
  public capacity:string;
  public Telephoneno:string;
  public mobilephone:string;
  public whRegmanagername:string;
  public whdeemanagername:string;
  public selectedWarehouseType:string;
  public warehouseTypelist:string[]=[];
  constructor(private http:HttpClient ,private formBuilder:FormBuilder ,private service:CommonServices,private uidservice:AadharValidateService) { }

  ngOnInit(): void {
    
   
    this.warehouseTypelist=["OWN GODOWNS","Headoffice","HIRED/AMC GODOWNS"];
    this.contactForm= this.formBuilder.group({
      selecteddistrictname:['',Validators.required],
      selectedWarehousename:['',Validators.required],
      selectedWarehouseType:['',Validators.required],      
     // whRegmanagername:['',Validators.required],
      //whdeemanagername:['',Validators.required],
      whmanagername:['',Validators.required],
      capacity:['',Validators.required,Validators.minLength(5)],
      Telephoneno:['',Validators.required],
      mobilephone:['',[Validators.required,Validators.minLength(10),Validators.pattern('[6-9]\\d{9}')]],
      //ifsccode:['',[Validators.required,Validators.minLength(11),Validators.pattern(this.IFSCPattern)]],
      //pancardnumber:['',[Validators.required,Validators.minLength(10),
     // Validators.pattern(this.PanPattern)]],
      //uidnum:['',[Validators.required,Validators.minLength(12)]]     
    
    }   

    );  

    this.districtList=[

      {districtCode:502,districtName:'ANANTAPUR'},
      {districtCode:503,districtName:'CHITTOOR'},
      {districtCode:505,districtName:'EAST GODAVARI'},
      {districtCode:506,districtName:'GUNTUR'},
      {districtCode:510,districtName:'KRISHNA'},
      {districtCode:511,districtName:'KURNOOL'},
      {districtCode:517,districtName:'PRAKASAM'},
      {districtCode:515,districtName:'SPSR NELLORE'},
      {districtCode:519,districtName:'SRIKAKULAM'},
      {districtCode:520,districtName:'VISAKHAPATANAM'},
      {districtCode:521,districtName:'VIZIANAGARAM'},
      {districtCode:523,districtName:'WEST GODAVARI'},
      {districtCode:504,districtName:'	Y.S.R.KADAPA'}
    ]
    //this.getWarehouseMaster();
  }

     // convenience getter for easy access to form fields
     get f() { return this.contactForm.controls; }

  onSubmit() {
    this.submitted=true;
    
    if(this.contactForm.invalid)
    {
      return false;
    }
    
    else{
      const req=new InputRequest();
      req.DIRECTION_ID="3";
      req.TYPEID="INS_CONTACT";
      req.INPUT_01=this.selecteddistrictname;
      req.INPUT_02=this.selectedWarehouseType;
      req.INPUT_03=this.selectedWarehousename;
      req.INPUT_04=this.capacity;  
      req.INPUT_05=this.whmanagername;  
      req.INPUT_06=this.Telephoneno; 
      req.INPUT_07=this.mobilephone;  
    
      this.service.postData(req,"ContactRegistration").subscribe(data => {

         if (data.StatusCode == "100") {  
           Swal.fire('info', data.StatusMessage,'info');   
         }
         else
         {
          Swal.fire('error', data.StatusMessage,'error');   
         }
        });
      }

}
uidvalidate(val){  
  
  if(val.length===12)
  {
    this.mob_uid_validate(val);
    this.uidstatus=this.uidservice.validate(this.contactForm.controls.uidnum.value);
    // if(this.uidstatus==false)      

  }
}
mob_uid_validate(mobilephone){
  if (mobilephone.length===10) 
  this.mob_uid_isvalid=this.uidservice.validatemob(this.contactForm.controls.mobilephone.value);
  else if(mobilephone.length===12)
  this.mob_uid_isvalid=this.uidservice.validatemob(this.contactForm.controls.uidnum.value);
}
onReset() {
  this.submitted = false;
  this.contactForm.reset();
}
LoadDistricts() {
  this.service.getData("GetDistricts").subscribe(data => {
    console.log(data);
    if (data.StatusCode == "100") {
      this.districts = data.Details;
      console.log(this.districts);
    }

  },
    error => console.log(error));
}


private getWarehouseMaster = () => {
  this.service.getData('WareHouseMaster')
  .subscribe(res => {
    this.warehouse = res as WareHouseMaster[];
    console.log(this.warehouse);
  });
}
}
