import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-tenders-page',
  templateUrl: './tenders-page.component.html',
  styleUrls: ['./tenders-page.component.css']
})
export class TendersPageComponent implements OnInit {
  tenderForm:FormGroup;
  tenderlist:any=[];
  public tendername:string;
  public tenderenddate:string;
  public tenderdownloaddate:string;
  public response: {dbPath: ''};
  submitted =false;
  formtender=true;

  constructor(private http:HttpClient ,private formBuilder:FormBuilder ,private service:CommonServices) { }

  ngOnInit(): void {
  

  this.tenderForm=this.formBuilder.group({
     
    tendername:['',Validators.required],  
    tenderdownloaddate:['',Validators.required],  
    tenderenddate:['',Validators.required]    
  
  });  
  this.Gettendermessage();
}
get f() { return this.tenderForm.controls; }

Gettendermessage()
{
  const req=new InputRequest();
  req.DIRECTION_ID="3";
  req.TYPEID="GET_TENDERS";
    
  this.service.postData(req,"GetScrollNewMessage").subscribe(data => {
     if (data.StatusCode == "100") {  
     this.tenderlist=data.Details;
   
     }
     else
     {
      Swal.fire('error', data.StatusMessage,'error');   
     }
    });

}
AddTender()
{
  this.formtender=false;
}

edittender(msg:any)
{
  const req=new InputRequest();
  req.DIRECTION_ID="3";
  req.TYPEID="UPD_TENDERS";
  req.INPUT_01=msg.contenT_ID;
  req.INPUT_02=msg.contenT_TYPE;
  req.INPUT_03=msg.contenT_TITLE;
  req.INPUT_04=msg.contenT_BODY;
  req.INPUT_05=msg.contenT_LINK;
  req.INPUT_06=msg.contenT_PHOTO;
  req.INPUT_07="0";
  this.service.postData(req,"ScrollNewMessageInsert").subscribe(data => {
     if (data.StatusCode == "100") {  
       Swal.fire('info', data.StatusMessage,'info');   
       this.formtender=true;
       this.tenderForm.reset();
      this.Gettendermessage();
     }
     else
     {
      Swal.fire('error', data.StatusMessage,'error');   
     }
    });


}

  onSubmit() {
    
    if(!this.tenderForm.value.tendername)
    {
    Swal.fire("info","Please Enter Tender Name","info");
    return;
    }
    else if(!this.tenderForm.value.tenderdownloaddate)
    {
      Swal.fire("info","Please  select Tender download date","info");
      return;
    }
    else if(!this.tenderForm.value.tenderenddate)
    {
      Swal.fire("info","Please select tender end date","info");
      return;
    }
    else if(!this.response.dbPath)
    {
      Swal.fire("info","Please upload Tender","info");
      return;
    }
    const req=new InputRequest();
    req.DIRECTION_ID="3";
    req.TYPEID="INS_TENDERS";
    req.INPUT_01="T";
    req.INPUT_02=this.tenderForm.value.tendername;
    req.INPUT_03="";
    req.INPUT_04="";
    req.INPUT_05= this.createImgPath(this.response.dbPath);
    req.INPUT_06=this.tenderForm.value.tenderdownloaddate;
    req.INPUT_07=this.tenderForm.value.tenderenddate;
    this.service.postData(req,"ScrollNewMessageInsert").subscribe(data => {
       if (data.StatusCode == "100") {  
         Swal.fire('info', data.StatusMessage,'info');   
         this.tenderForm.reset();
        this.Gettendermessage();
       }
       else
       {
        Swal.fire('error', data.StatusMessage,'error');   
       }
      });
}
  public uploadFinished = (event) => {
    this.response = event;
  }

  public createImgPath = (serverPath: string) => {
    //return `http://uat.apswc.ap.gov.in/APSWCAPP/${serverPath}`;
    return `${this.service.filebaseUrl.replace("api/FilesUpload","")}${serverPath}`;
    
  }

}
