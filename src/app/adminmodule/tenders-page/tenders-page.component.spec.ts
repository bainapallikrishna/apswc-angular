import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TendersPageComponent } from './tenders-page.component';

describe('TendersPageComponent', () => {
  let component: TendersPageComponent;
  let fixture: ComponentFixture<TendersPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TendersPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TendersPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
