import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';


@Component({
  selector: 'app-boardofdirectors-reg',
  templateUrl: './boardofdirectors-reg.component.html',
  styleUrls: ['./boardofdirectors-reg.component.css']
})
export class BoardofdirectorsRegComponent implements OnInit {

  boardofdirectorsForm:FormGroup;
  public groupgeneralmanager:string;
  public regionalmanager:string;
  public dirctorcost:string;
  public indiranagar:string;
  submitted=false;
  bodshiden=true;
  boardofDirectorsList:any=[];
  constructor(private http:HttpClient ,private formBuilder:FormBuilder ,private service:CommonServices) { }

  ngOnInit(): void {

    this.getboarddirectories();
    this.boardofdirectorsForm=this.formBuilder.group({
     
      groupgeneralmanager:['',Validators.required],    
      regionalmanager:['',Validators.required],    
      dirctorcost:['',Validators.required],    
      indiranagar:['',Validators.required]
    
    });  
  }
  EditDirectories(bod:any)
  {
     const req=new InputRequest();
     req.DIRECTION_ID="3";
     req.TYPEID="UPD_BOARD_OF_DIRECTORS";
     req.INPUT_01=bod.emP_CODE;
     req.INPUT_02=bod.category;
     req.INPUT_03="N";
     this.service.postData(req,"ScrollNewMessageInsert").subscribe(data => {
       if (data.StatusCode == "100") {  
         Swal.fire('info', data.StatusMessage,'info');   
         
        this.getboarddirectories();
       }
       else
       {
        Swal.fire('error', data.StatusMessage,'error');   
       }
      });

  }
  getboarddirectories()
  {
    this.service.getData("GetBoardofDirectors").subscribe(data => {
        this.boardofDirectorsList=data.Details;    
     },
       error => console.log(error));
  }

  get f() { return this.boardofdirectorsForm.controls; }
  Adddirectories()
  {
    this.bodshiden=false;
  }
  onSubmit() {
    this.submitted=true;
    
    if(this.boardofdirectorsForm.invalid)
    {
      return false;
    }
    else{
      const req=new InputRequest();
      req.DIRECTION_ID="3";
      req.TYPEID="INS_BOARDOFDIRECTORS";
      req.INPUT_01=this.groupgeneralmanager;
      req.INPUT_02=this.regionalmanager;
      req.INPUT_03=this.dirctorcost;
      req.INPUT_04=this.indiranagar;  
  
      this.service.postData(req,"BoardofDirectorsRegistration").subscribe(data => {
         if (data.StatusCode == "100") {  
           Swal.fire('info', data.StatusMessage,'info');   
           this.bodshiden=false;
           this.getboarddirectories();
         }
         else
         {
          Swal.fire('error', data.StatusMessage,'error');   
         }
        });
    }
  
}

onReset() {
  this.submitted = false;
  this.boardofdirectorsForm.reset();
}


}
