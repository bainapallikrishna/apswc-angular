import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoardofdirectorsRegComponent } from './boardofdirectors-reg.component';

describe('BoardofdirectorsRegComponent', () => {
  let component: BoardofdirectorsRegComponent;
  let fixture: ComponentFixture<BoardofdirectorsRegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoardofdirectorsRegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoardofdirectorsRegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
