import { Component, OnInit,ViewChild  } from '@angular/core';
import{Router}from '@angular/router';
import Swal from 'sweetalert2';
import{InputRequest} from 'src/app/Interfaces/employee';
import{CommonServices} from 'src/app/Services/common.services';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormControl,FormBuilder, FormGroup,Validators } from '@angular/forms';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { AgGridAngular } from 'ag-grid-angular';
import { ButtonRendererComponent } from 'src/app/custome-directives/button-renderer.component';
import { Editor, Toolbar,toHTML } from "ngx-editor";

@Component({
  selector: 'app-adminhelp',
  templateUrl: './adminhelp.component.html',
  styleUrls: ['./adminhelp.component.css']
})
export class AdminhelpComponent implements OnInit{

  editor: Editor;
  toolbar: Toolbar = [
    ["bold", "italic"],
    ["underline", "strike"],
    ["code", "blockquote"],
    ["ordered_list", "bullet_list"],
    [{ heading: ["h1", "h2", "h3", "h4", "h5", "h6"] }],
    ["link", "image"],
    ["text_color", "background_color"],
    ["align_left", "align_center", "align_right", "align_justify"]
  ];


  formedit=true;

  loguser: string=sessionStorage.getItem("logUserCode");
  loguserrole: string = sessionStorage.getItem("logUserrole");
  ispwdchanged:string=sessionStorage.getItem("logUserisChnagePassword");
  loguserid:string=sessionStorage.getItem("logUserworkLocationCode");
  
  
  columnDefs = [
    { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer' },
    { headerName: 'Page ID', maxWidth: 300, field: 'pagE_ID', sortable: true, filter: true },
    { headerName: 'Page Name', maxWidth: 300, field: 'pagE_NAME', sortable: true, filter: true },
    { headerName: 'Description', maxWidth: 300, field: 'helP_DESCRIPTION', sortable: true, filter: true },
    { headerName: 'Updated On', maxWidth: 300, field: 'updateD_ON', sortable: true, filter: true },
    { headerName: 'Updated By', maxWidth: 350, field: 'updateD_BY', sortable: true, filter: true },
    {
      headerName: 'Action',field: 'ediT_STATUS', cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        editClick: this.Edit.bind(this),
      },
    }
    ];
  
  frameworkComponents: any;
  public components;
  gridApi: any;
  gridColumnApi: any;
  showloader:boolean=false;
  submitted:boolean=false;
  PageDD : any [];
  PageList : any [];
  Formaddarray : any [];
  Description: any;
  isEdit:boolean=false;
  
    constructor(private router:Router,private service:CommonServices,private fb:FormBuilder) { 
      
      if (!this.loguserrole || this.ispwdchanged == '0') {
        this.router.navigate(['/Login']);
        return;
      }
  
      this.frameworkComponents = {
        buttonRenderer: ButtonRendererComponent,
      }
      this.components = {
        rowIdRenderer: function (params) {
          return '' + (params.rowIndex + 1);
        },
      };
    }
  
    @ViewChild('addModal') public addModal: ModalDirective;
    @ViewChild('agGrid') agGrid: AgGridAngular;
    ngOnInit(): void {
      this.editor = new Editor();
      this.LoadPageList();
    }

    Edit(Cancelrcd){
      this.LoadPageDropDown();
      this.addModal.show();
      this.submitted=false;
      this.HelpAddForm.reset();
      this.isEdit=true;
      this.subf.PageName.setValue(Cancelrcd.rowData.pagE_ID);
      this.subf.Description.setValue(Cancelrcd.rowData.helP_DESCRIPTION);
    }
  
    BindData(params) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
      params.api.setRowData(this.PageList);
    }

  
    get subf() { return this.HelpAddForm.controls; }
  
    HelpAddForm=this.fb.group({
      PageName: ['', Validators.required],
      Description: ['', Validators.required],
    });
  
    LoadPageDropDown(){
      this.showloader = true;
      const req = new InputRequest();
  
      req.INPUT_01 = "PAGE_DROPDOWN";
      this.PageDD=[];
      this.service.postData(req, "GetHelpDetails").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.PageDD = data.Details;
          console.log(this.PageDD);
        }
        else{
          Swal.fire('warning', data.StatusMessage, 'warning');
        }
      },
        error => console.log(error));
    }

    LoadPageList() {
  
      this.showloader = true;
      const req = new InputRequest();
  
      req.INPUT_01 = "SUBMITTED_PAGES";
      this.PageList=[];
      this.service.postData(req, "GetHelpDetails").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.PageList = data.Details;
          this.gridApi.setRowData(this.PageList);
          console.log(this.PageList);
        }
        else{
          this.gridApi.setRowData(this.PageList);
          Swal.fire('warning', data.StatusMessage, 'warning');
        }
      },
        error => console.log(error));
  
    }

    OnchangePage(){
      this.showloader = true;
      const req = new InputRequest();
  
      req.INPUT_01 = "PAGE";
      req.INPUT_02 = this.subf.PageName.value;

      this.service.postData(req, "GetHelpDetails").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.subf.Description.setValue(data.Details[0]["description"]);
        }
        else{
          Swal.fire('warning', data.StatusMessage, 'warning');
        }
      },
        error => console.log(error));
    }

    AddHelp(){
      this.LoadPageDropDown();
      this.addModal.show();
      this.submitted=false;
      this.HelpAddForm.reset();
      this.isEdit=false;
      this.subf.Description.setValue("");
    }

    hideaddModal(){
      this.addModal.hide();
      this.submitted=false;
      this.HelpAddForm.reset();
    }

    SubmitAddDetails() {
      this.submitted=true;
      if(this.HelpAddForm.invalid){
        return false;
      }

      if(!this.subf.Description || this.subf.Description.value=="<p></p>"){
        Swal.fire("error", "Please enter description", "error");
        return;
      }
      const req = new InputRequest();
    
    req.INPUT_01 = "HELP_SUBMISSION";
    req.INPUT_02=this.subf.PageName.value;
    req.INPUT_03=this.subf.Description.value;
    req.CALL_SOURCE="Web";
    req.USER_NAME=this.loguser;
  
    this.service.postData(req, "HelpInsertion").subscribe(data => {
      this.Formaddarray = data.result;
        if (data.StatusCode == 100) {
        this.showloader= false;  
        this.HelpAddForm.reset();
          Swal.fire("success", "Data Submitted Successfully", "success");
          this.addModal.hide();
          this.LoadPageList();
      }
      else{
        this.showloader= false;  
        Swal.fire("info",data.StatusMessage,"info");
      }
    });
    }
  
  }