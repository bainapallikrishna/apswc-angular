import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminhelpComponent } from './adminhelp.component';

describe('AdminhelpComponent', () => {
  let component: AdminhelpComponent;
  let fixture: ComponentFixture<AdminhelpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminhelpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminhelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
