import { Component, OnInit } from '@angular/core';

import Swal from 'sweetalert2/dist/sweetalert2.js';
import { FormBuilder, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { swalProviderToken } from '@sweetalert2/ngx-sweetalert2/lib/di';
//import { FormControl, FormGroup, ReactiveFormsModule, FormBuilder, Validators } from '@angular//forms'
@Component({
  selector: 'app-stgothchrg-registration',
  templateUrl: './stgothchrg-registration.component.html',
  styleUrls: ['./stgothchrg-registration.component.css']
})
export class StgothchrgRegistrationComponent implements OnInit {
 
  ricebuttondiv:boolean=false;

  godownsubmitdiv:boolean=false;
  onsubmitted=false;
  submitted = false;
  Godownswarehsetype:FormControlName;
  storagetypevalue="0";
  Godownsdiv: boolean = false;
  Ricediv:boolean=false;
  submitevalue:any;
  storageotherarray=[
    {

      "value":"1",
    "Text":"TYPES OF GODOWNS"
    },
    {

    "value":"2",
    "Text":"RICE"
    },
    {

      "value":"3",
    "Text":"FOOD GRAINS"
    },
    {

      "value":"4",
    "Text":"FERTILISERS"
    },
    {

      "value":"5",
    "Text":"FOOD PRODUCTS"
    },
    {

      "value":"6",
    "Text":"PULSES"
    },
    {

      "value":"7",
    "Text":"SPICES"
    },
    {

      "value":"8",
    "Text":"FOOD STUFFS"
    },
    {

      "value":"9",
    "Text":"OILSEEDS"
    },
    {

      "value":"10",
    "Text":"OILs"
    },
    {

      "value":"11",
    "Text":"FIBERS"
    },
    {

      "value":"12",
    "Text":"SEEDS"
    },
    {

      "value":"13",
    "Text":"FEED FODDER"
    },
    {

      "value":"14",
    "Text":"NOTIFIED COMMODITIES"
    },
    {

      "value":"15",
    "Text":"TEA & COFFE"
    },
    {

      "value":"16",
    "Text":"MISCELLANEOUS"
    },
  ];

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit():void{

  }
  registerForm = this.formBuilder.group({
    Godownswarehsetype: ['', Validators.required],
    GodownsNos: ['', [Validators.required, Validators.pattern("^[0-9-_ ]*$")]],
    GodownCapacity: ['', [Validators.required,Validators.pattern("^[0-9-_,.(){} ]*$")]],
    GodownOccupancy: ['', [Validators.required,Validators.pattern("^[0-9-_,.(){} ]*$")]]
       //  email: ['', [Validators.required, Validators.email]],
    //password: ['', [Validators.required, Validators.minLength(6)]],
    //confirmPassword: ['', Validators.required],
    //acceptTerms: [false, Validators.requiredTrue]
});
    //validator: MustMatch('password', 'confirmPassword')

   riceForm = this.formBuilder.group({
    Ricecommodity: ['', Validators.required],
    Ricetypepackage: ['', Validators.required],
    Ricenetweight: ['', [Validators.required,Validators.pattern("^[0-9-_,.(){} ]*$")]],
    Ricestdrate: ['', [Validators.required,Validators.pattern("^[0-9-_,.(){} ]*$")]],
    Ricehighrated: ['', [Validators.required,Validators.pattern("^[0-9-_,.(){} ]*$")]],
    Ricehighrated1: ['', [Validators.required,Validators.pattern("^[0-9-_,.(){} ]*$")]],
    Riceremarks: ['', Validators.required]

});

 
 
 
 storagechargefun()
 {
  if(this.storagetypevalue=="0")
  {
  this.Godownsdiv=false;
  this.Ricediv=false;  
  this.godownsubmitdiv=false;
  this.ricebuttondiv=false;  
}
  if(this.storagetypevalue=="1")
  {
   // alert(this.storagetypevalue);
  
    this.Godownsdiv=true;
    this.Ricediv=false;
    this.godownsubmitdiv=true;
    this.ricebuttondiv=false;
    }  
if(this.storagetypevalue=="2"||this.storagetypevalue=="3"||this.storagetypevalue=="4"||this.storagetypevalue=="5"||this.storagetypevalue=="6"||this.storagetypevalue=="7"||this.storagetypevalue=="8"||this.storagetypevalue=="9"||this.storagetypevalue=="10"||this.storagetypevalue=="11"||this.storagetypevalue=="12"||this.storagetypevalue=="13"||this.storagetypevalue=="14"||this.storagetypevalue=="15")
{
 // alert(this.storagetypevalue);
this.Godownsdiv=false;
this.Ricediv=true;
this.godownsubmitdiv=false;
this.ricebuttondiv=true;
}

}

godownsubmit()
{
  this. riceForm.reset();;
  this.submitted = true;

        //alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  
        
          if (this.registerForm.invalid) 
          {
            return;
        }
        Swal.fire("submitted successfully");
      }
      
      
      onsubmit(){
   //     alert("submit2");

        this.onsubmitted = true;
      
        if (this.riceForm.invalid) 
        {
          this. registerForm.reset();;
          return;
      }
      }
      

      get f() { return this.registerForm.controls; }      
  
      get riceformval() { return this.riceForm.controls; }
    }

