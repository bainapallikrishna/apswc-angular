import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-layout-structure',
  templateUrl: './layout-structure.component.html',
  styleUrls: ['./layout-structure.component.css']
})
export class LayoutStructureComponent implements OnInit {

  @ViewChild('receiptModal') public receiptModal: ModalDirective;
   @ViewChild('spModal') public spModal: ModalDirective;
  showloader: boolean = true;
  DepForm:FormGroup;
columnDefs = [
        { field: 'make' },
        { field: 'model' },
        { field: 'price' }
    ];

    rowData = [
        { make: 'Toyota', model: 'Celica', price: 35000 },
        { make: 'Ford', model: 'Mondeo', price: 32000 },
        { make: 'Porsche', model: 'Boxter', price: 72000 }
    ];
  constructor() { }

  ngOnInit(): void {
  }
  viewReceipt() {
    this.receiptModal.show();
  }

  closeReceipt() {
    this.receiptModal.hide();
  }

  viewSp() {
    this.spModal.show();
  }

  closeSp() {
    this.spModal.hide();
  }
}
