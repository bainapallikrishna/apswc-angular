import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

import { CommonServices } from 'src/app/Services/common.services';
import { WHButtonRendererComponent } from 'src/app/custome-directives/whbutton-renderer.component';
import { AgGridAngular } from 'ag-grid-angular';
import { EmployeeTypes } from 'src/app/Interfaces/user';


@Component({
  selector: 'app-leavemaster',
  templateUrl: './leavemaster.component.html',
  styleUrls: ['./leavemaster.component.css']
})
export class LeavemasterComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('historyempModal') historyModal: ModalDirective;


  EmpLeaveType: EmployeeTypes;
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");

  columnDefs = [
    { headerName: '#', maxWidth: 50, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Leave Type', maxWidth: 200, field: 'leavetype', hide: true, sortable: true, filter: true },
    { headerName: 'Leave Type', maxWidth: 150, field: 'leave', sortable: true, filter: true },
    { headerName: 'Leaves Per Year', maxWidth: 150, field: 'leavesperyear', sortable: true, filter: true },
    { headerName: 'Is Carry Forward', maxWidth: 200, field: 'carryforword', sortable: true, filter: true },
    { headerName: 'Applicable To', maxWidth: 1000, field: 'emptypes', sortable: true, filter: true },
    { headerName: 'MonthlyYearlyvalue', maxWidth: 20, field: 'monthlyyearlyvalue', hide: true, sortable: true, filter: true },
    { headerName: 'Employee Type', maxWidth: 20, field: 'emptypeids', hide: true, sortable: true, filter: true },
    { headerName: 'Is Holiday Calendar Applicable', width: 250, field: 'holidaY_CALENDER_TEXT', sortable: true, filter: true },
    { headerName: 'Holiday Calendar', maxWidth: 100, field: 'holidaY_CALENDER_VALUE', hide: true, sortable: true, filter: true },

    { headerName: 'Is Encashment', width: 150, field: 'encashmenT_TEXT', sortable: true, filter: true },
    { headerName: 'Encashment', maxWidth: 10, field: 'encashmenT_VALUE', hide: true },


    { headerName: 'Status', width: 70, field: 'iS_ACTIVE', hide: true, sortable: true, filter: true },
    { headerName: 'Status', maxWidth: 100, field: 'iS_ACTIVE_Text', sortable: true, filter: true },

    {
      headerName: 'Action', maxWidth: 300, cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        editClick: this.EditLeaveMaster.bind(this),
        viewClick: this.Viewrowdata.bind(this),
        historyClick: this.Historyrowdata.bind(this),
      },
    }
  ];


  constructor(private formBuilder: FormBuilder, private router: Router, private service: CommonServices) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.frameworkComponents = {
      buttonRenderer: WHButtonRendererComponent,

    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

    this.form = this.formBuilder.group({
      orders: new FormArray([])
    });

  }

  LeaveMasterForm: FormGroup;
  form: FormGroup;

  public consubmitted: boolean = false;
  frameworkComponents: any;
  public components;
  showloader: boolean = true;
  public isEdit: boolean = false;
  gridApi: any = [];
  defaultColDef: any = [];
  gridColumnApi: any = [];
  LeaveMasterData = [];
  leavetypes: any = [];
  LeaveData: any;
  emptypes: any = [];
  modeltitle: string = "Add";

  LeaveMasters: any[];
  emptypes_append: any[];
  employeeleavetypelist: any = [];
  emptypes_seltd: string[];
  emptypeerrordiv: boolean = true;

  get LeaveMaster() { return this.LeaveMasterForm.controls; }
  get ordersFormArray() {
    return this.form.controls.orders as FormArray;
  }
  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.LeaveMasterData);

  }

  ngOnInit(): void {

    //this.Facilities_seltd=new Array<string>();
    this.emptypes_seltd = new Array<string>();

    this.showloader = false;

    //this.LoadLeaveTypes();
    this.fillLeaveMasterdata();

    this.LoadEmployeeTypes();

    this.LeaveMasterForm = this.formBuilder.group({
      LeaveType: ['', Validators.required],
      NoofLeaves: ['', Validators.required],
      Carryforward: [''],
      LeavesAddMonthyear: ['', Validators.required],
      HolidayCalender: [''],
      status: ['1'],
      Encashment: ['']

    });

    // this.form = this.formBuilder.group({
    //   orders: new FormArray([], this.minSelectedCheckboxes(1))
    // });

  }

  MasterHistlist: any[];
  LeaveType_VIEW: string;
  Peryear_VIEW: string;
  ApplicableTo_VIEW: string;
  Year_Month_VIEW: string;
  carryforward_VIEW: string;
  Holidaycal_VIEW: string;
  status_VIEW: string;
  Encashment_VIEW: string;

  Allhistory() { this.historyholiday(); }
  Viewrowdata(row: any) {
    const data = row.rowData;
    this.ApplicableTo_VIEW = data.emptypes;
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = data.leavetype;
    this.service.postData(req, "GetLeaveMaster_VIEW").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.viewLeave_details(data.Details[0]);
        this.viewModal.show();
      }

    },
      error => console.log(error));
  }

  viewLeave_details(rowdata: any) {
    this.LeaveType_VIEW = rowdata.leaveS_TYPE;
    this.Peryear_VIEW = rowdata.leaveS_PER_YEAR;

    this.Year_Month_VIEW = rowdata.monthlyyearly;
    this.carryforward_VIEW = rowdata.carrY_FORWORD == "true" ? "Yes" : "";

    this.Holidaycal_VIEW = rowdata.holidaY_CALENDER == "true" ? "Yes" : "";
    this.status_VIEW = rowdata.iS_ACTIVE;

    this.Encashment_VIEW = rowdata.encashment == "true" ? "Yes" : "";
  }


  historyholiday() {
    const req = new InputRequest();

    req.INPUT_02 = "LEAVES";

    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }

  Historyrowdata(row: any): void {

    this.showloader = true;
    const data = row.rowData;
    const req = new InputRequest();
    req.INPUT_01 = data.leavetype;
    req.INPUT_02 = "LEAVES";
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }
  hidehistoryrowModal() { };

  oncheckboxchange(checkedname: string, e: any) {
    if (e.target.checked) {
      this.emptypes_seltd.push(checkedname);
      this.emptypeerrordiv = true;
      this.emptypes_append.push(checkedname);

    }
    else {
      this.emptypes_seltd = this.emptypes_seltd.filter(m => m != checkedname);
      this.emptypes_append = this.emptypes_append.filter(m => m != checkedname);

    }
    if (this.emptypes_append.length === [].length) {
      this.emptypeerrordiv = false;
      return false;
    }
    else { this.emptypeerrordiv = true; }


  }


  EditLeaveMaster(row) {
    this.modeltitle = "Edit";

    this.isEdit = true;

    const data = row.rowData;

    this.LeaveMaster.Carryforward.setValue(data.carryforword == "YES" ? true : false);

    this.LeaveMaster.NoofLeaves.setValue(data.leavesperyear);

    this.LeaveMaster.HolidayCalender.setValue(data.holidaY_CALENDER_VALUE == "true" ? true : false);
    this.LeaveMaster.Encashment.setValue(data.encashmenT_VALUE == "true" ? true : false);

    this.LeaveMaster.status.setValue(data.iS_ACTIVE.toString());

    this.leavetypes.push({ 'id': data.leavetype, 'leave': data.leave });

    this.LeaveMaster.LeaveType.setValue(data.leavetype);

    this.LeaveMaster.LeavesAddMonthyear.setValue(data.monthlyyearlyvalue);

    this.emptypes_append = (data.emptypeids).split(",", data.emptypeids.length);

    this.emptypes_seltd = (data.emptypeids).split(",", data.emptypeids.length);

    this.employeeleavetypelist = this.employeeleavetypelist.map(
      (elem) => {
        elem.Isselected = data.emptypes.split(",").filter(x => x == elem.EmployeeTYpe &&
          data.emptypeids.split(",").filter(x => x == elem.EmployeeID)).length > 0 ? true : false;
        return elem
      });




    this.editModal.show();

  }

  ViewLeaveMaster(row) {
  }

  LoadEmployeeTypes() {
    this.service.getData("GetEmployeeTypes").subscribe(data => {
      if (data.StatusCode == "100") {
        this.emptypes = [];
        this.emptypes = data.Details;
        this.employeeleavetypelist = [];
        this.emptypes.forEach((val, i) => {
          this.EmpLeaveType = {
            EmployeeID: val.id,
            EmployeeTYpe: val.emP_TYPE,
            Isselected: false
          };
          this.employeeleavetypelist.push(this.EmpLeaveType);

        })


      }
    },
      error => console.log(error));
  }

  LoadLeaveTypes() {
    this.service.getData("GetLeaveTypes_Settings").subscribe(data => {
      if (data.StatusCode == "100") {
        this.leavetypes = data.Details;
      }
    },
      error => console.log(error));
  }


  resetallforms() {

    this.LoadLeaveTypes();


    this.consubmitted = false;

    this.LeaveMasterForm.reset();

    this.form.reset();


    this.emptypes_seltd = [];

    this.emptypeerrordiv = true;

    this.LeaveMaster.status.setValue("1");

    this.LeaveMaster.LeavesAddMonthyear.setValue("Y");

    this.LeaveMaster.LeaveType.setValue("");

    this.LoadEmployeeTypes();




  }

  fillLeaveMasterdata() {

    this.showloader = true;
    this.service.getData("GetLeaveDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.LeaveMasters = data.Details;
        this.LeaveMasterData = data.Details;
        this.gridApi.setRowData(this.LeaveMasterData);


      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));


  }


  AddLeaveMaster() {

    this.isEdit = false;
    this.modeltitle = "Add";

    this.resetallforms();

    this.editModal.show();
  }
  CloseEditModal() {

    this.leavetypes = [];

    this.editModal.hide();

  }
  SubmitLeaveMaster() {

    this.consubmitted = true;

    if (this.emptypes_seltd.length === [].length) {
      this.emptypeerrordiv = false;
      return false;
    }
    else { this.emptypeerrordiv = true; }
    if (this.LeaveMasterForm.invalid) {
      return false;
    }

    const req = new InputRequest();
    req.INPUT_01 = this.LeaveMaster.LeaveType.value;
    req.INPUT_02 = this.LeaveMaster.NoofLeaves.value;
    req.INPUT_03 = this.LeaveMaster.Carryforward.value;
    req.INPUT_04 = this.LeaveMaster.LeavesAddMonthyear.value;
    //req.INPUT_05 = this.LeaveMaster.LeavesAddMonthyear.value;
    req.INPUT_06 = this.emptypes_seltd.toString();
    req.INPUT_07 = this.LeaveMaster.HolidayCalender.value;
    req.INPUT_08 = this.LeaveMaster.status.value;
    req.INPUT_09 = this.LeaveMaster.Encashment.value;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;

    this.showloader = true;
    this.service.postData(req, "SaveLeaveMaster").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          this.fillLeaveMasterdata();
          this.editModal.hide();
          Swal.fire('success', "Leave Master Details Saved Successfully !!!", 'success');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }


  UpdateLeaveMaster() {

    this.consubmitted = true;

    if (this.emptypes_seltd.length === [].length) {
      this.emptypeerrordiv = false;
      return false;
    }
    else { this.emptypeerrordiv = true; }


    // stop here if form is invalid
    if (this.LeaveMasterForm.invalid) {

      return false;
    }


    const req = new InputRequest();
    req.INPUT_01 = this.LeaveMaster.LeaveType.value;
    req.INPUT_02 = this.LeaveMaster.NoofLeaves.value;
    req.INPUT_03 = this.LeaveMaster.Carryforward.value;
    req.INPUT_04 = this.LeaveMaster.LeavesAddMonthyear.value;
    //req.INPUT_05 = this.LeaveMaster.LeavesAddMonthyear.value;
    req.INPUT_06 = this.emptypes_append.toString();
    req.INPUT_07 = this.LeaveMaster.HolidayCalender.value;
    req.INPUT_08 = this.LeaveMaster.status.value;
    req.INPUT_09 = this.LeaveMaster.Encashment.value;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;

    this.showloader = true;

    this.service.postData(req, "UpdateLeaveMaster").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          //this.contact.EMPCODE.setValue(result.emP_CODE);
          this.fillLeaveMasterdata();
          this.editModal.hide();

          Swal.fire('success', "Leave Master Details Updated Successfully !!!", 'success');
          //this.contactcomple = true;
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }


}
