import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { WHButtonRendererComponent } from 'src/app/custome-directives/whbutton-renderer.component';
import { AgGridAngular } from 'ag-grid-angular';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
@Component({
  selector: 'app-holidaymaster',
  templateUrl: './holidaymaster.component.html',
  styleUrls: ['./holidaymaster.component.css']
})
export class HolidaymasterComponent implements OnInit {


  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('historyempModal') historyempModal: ModalDirective;
  @ViewChild('officeModal') officeModal: ModalDirective;



  columnDefs = [
    { headerName: '#', maxWidth: 50, cellRenderer: 'rowIdRenderer', },
    { headerName: 'ID', maxWidth: 150, field: 'holidaY_ID', sortable: true, filter: true },
    { headerName: 'Name', maxWidth: 200, field: 'holidaY_NAME', sortable: true, filter: true },
    { headerName: 'Date', maxWidth: 150, field: 'holidaY_FROM_DATE', sortable: true, filter: true },
    { headerName: 'No.of Days', maxWidth: 150, field: 'nO_OF_DAYS', sortable: true, filter: true },
    { headerName: 'Is Optional', maxWidth: 150, field: 'iS_OPTIONAL_TEXT', sortable: true, filter: true },
    { headerName: 'Status', maxWidth: 150, field: 'iS_ACTIVE_TEXT', sortable: true, filter: true },

    { headerName: 'Remarks', maxWidth: 200, field: 'remarks', sortable: true, hide: true, filter: true },
    { headerName: 'To Date', maxWidth: 150, field: 'holidaY_TO_DATE', hide: true, sortable: true, filter: true },
    { headerName: 'Optional', maxWidth: 150, field: 'optionaL_HOLIDAY', hide: true, sortable: true, filter: true },
    { headerName: 'Status', maxWidth: 20, field: 'iS_ACTIVE', hide: true, sortable: true, filter: true },

    {
      headerName: 'Action', maxWidth: 400, cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        editClick: this.EditHolidayMaster.bind(this),
        viewClick: this.ViewHolidayCalender.bind(this),
        historyClick: this.HistoryHolidayrow.bind(this),
      },
    }
  ];




  frameworkComponents: any;
  public components;
  showloader: boolean = true;

  Holidaycalenderform: FormGroup;
  officeform: FormGroup;
  consubmitted: boolean = true;
  consubmitted_office: boolean = true;

  leavetypes: any = [];
  NgbDateStruct: any;

  HolidayMastersGrid: any[];
  Mainyear: string;
  public isEdit: boolean = false;
  gridApi: any = [];
  defaultColDef: any = [];
  gridColumnApi: any = [];
  HolidayMasterRowData: any[];
  modeltitle: string;
  HolidayID: string;

  holidaymaxdate: any[];
  Holidayhistory: any = [];
  MasterHistlist: any[]
  get holidaycalenderCtrl() { return this.Holidaycalenderform.controls; }
  get OfficeTimingsCtrl() { return this.officeform.controls; }

  currentyear: number;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private service: CommonServices,
    private datef: CustomDateParserFormatter,
    private datecon: NgbDatepickerConfig) {


    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }
    this.frameworkComponents = {
      buttonRenderer: WHButtonRendererComponent,

    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };



  }
  ngOnInit(): void {

    this.showloader = false;

    let now: Date = new Date();

    this.Mainyear = now.getFullYear().toString();

    this.currentyear = now.getFullYear();

    this.fillHolidayMastersdata(this.currentyear.toString());
    (this.currentyear.toString());
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }


    this.GetOfficeTimings();
    this.Holidaycalenderform = this.formBuilder.group({
      holidayyear: [],
      HolidayName: ['', Validators.required],
      FromDate: ['', Validators.required],
      ToDate: ['', Validators.required],
      holidaytype: ['R', Validators.required],
      // Optional:['0'],
      status: ['1'],
      Remarks: ['', Validators.required]
    });

    this.officeform = this.formBuilder.group({
      Fromhours: ['', Validators.required],
      FromMin: ['', Validators.required],
      FromAMPM: ['', Validators.required],
      ToHours: ['', Validators.required],
      ToMin: ['', Validators.required],
      ToAMPM: ['', Validators.required],
      FromDay: ['', Validators.required],
      ToDay: ['', Validators.required],
    });

  }

  CloseEditModal() { this.editModal.hide(); }

  Mainyearchange(event) { this.fillHolidayMastersdata(event); }

  ViewHolidayCalender(row) {
    const data = row.rowData;
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = data.holidaY_ID;
    this.service.postData(req, "GetHolidayMaster_VIEW").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.viewHoliday_details(data.Details[0]);
        this.viewModal.show();
      }

    },
      error => console.log(error));
  }

  HolidayID_VIEW: string;
  HolidayName: string;
  holidayyear: string;
  FromDate: string;
  NoofDays: string;
  ToDate: string;
  Regular: string;
  Optional: string;
  Remarks: string;
  status: string;

  IsOfficeSettingAdd: boolean = false;

  viewHoliday_details(holidaydata: any) {
    this.HolidayID_VIEW = holidaydata.holidaY_ID;
    this.HolidayName = holidaydata.holidaY_NAME;

    this.holidayyear = holidaydata.year;
    this.FromDate = holidaydata.holidaY_FROM_DATE;
    this.NoofDays = holidaydata.nO_OF_DAYS;

    this.ToDate = holidaydata.holidaY_TO_DATE;
    this.Regular = holidaydata.regulaR_OPTIONAL == "O" ? "Optional" : "Regular";
    this.Remarks = holidaydata.remarks;
    this.status = holidaydata.iS_ACTIVE_TEXT;

  }



  EditHolidayMaster(row) {
    this.modeltitle = "Edit";
    this.isEdit = true;
    this.editModal.show();
    const data = row.rowData;
    this.holidaycalenderCtrl.HolidayName.setValue(data.holidaY_NAME);
    this.holidaycalenderCtrl.FromDate.setValue(this.datef.parse((data.holidaY_FROM_DATE ? data.holidaY_FROM_DATE : "").replace("T00:00:00", "")));
    this.holidaycalenderCtrl.ToDate.setValue(this.datef.parse((data.holidaY_TO_DATE ? data.holidaY_TO_DATE : "").replace("T00:00:00", "")));

    this.holidaycalenderCtrl.holidaytype.setValue(data.regulaR_OPTIONAL);
    this.holidaycalenderCtrl.Remarks.setValue(data.remarks);
    this.holidaycalenderCtrl.status.setValue(data.iS_ACTIVE.toString());

    this.HolidayID = data.holidaY_ID;

  }
  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.HolidayMasterRowData);

  }
  resetallforms() {
    this.consubmitted = false;
    this.Holidaycalenderform.reset();
    this.holidaycalenderCtrl.status.setValue("1");
    this.holidaycalenderCtrl.holidaytype.setValue("R");
    //this.holidaycalenderCtrl.Optional.setValue(false);
    this.isEdit = false;
  }
  AddHolidayCalender() {
    if (this.IsOfficeSettingAdd == false) {
      Swal.fire('warning', "Please Add Office Setting..", 'warning');
      this.officeModal.show();
      return false;
    }

    this.modeltitle = "Add";
    this.resetallforms();
    this.editModal.show();
  }

  fillHolidayMastersdata(yearvalue: string) {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = yearvalue;


    this.service.postData(req, "GetHolidayMaster").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.HolidayMastersGrid = data.Details;
        this.HolidayMasterRowData = data.Details;
        this.gridApi.setRowData(this.HolidayMasterRowData);
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));


  }
  UpdateHolidayMaster() {
    this.consubmitted = true;

    if (this.Holidaycalenderform.invalid) {
      return false;
    }

    if (this.datef.HolidayGreaterDate(this.holidaycalenderCtrl.FromDate.value, this.holidaycalenderCtrl.ToDate.value)) {
      Swal.fire('warning', "Holiday To Date Shoud be greater than Holiday From Date", 'warning');
      return false;
    }

    if ((this.datef.format(this.holidaycalenderCtrl.FromDate.value) != this.datef.format(this.holidaycalenderCtrl.ToDate.value)) && this.holidaycalenderCtrl.holidaytype.value == "O") {
      Swal.fire('warning', "Optional Holiday From Date and To Date Should be Same", 'warning');
      return false;
    }
    const req = new InputRequest();
    req.INPUT_01 = this.HolidayID;
    req.INPUT_02 = this.holidaycalenderCtrl.HolidayName.value;
    req.INPUT_03 = this.datef.format(this.holidaycalenderCtrl.FromDate.value);
    req.INPUT_04 = this.datef.format(this.holidaycalenderCtrl.ToDate.value);
    req.INPUT_05 = this.holidaycalenderCtrl.holidaytype.value;
    req.INPUT_06 = this.holidaycalenderCtrl.status.value;
    req.INPUT_07 = this.holidaycalenderCtrl.Remarks.value;
    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;

    this.showloader = true;
    this.service.postData(req, "UpdateHolidayMaster").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          this.fillHolidayMastersdata(this.currentyear.toString());

          this.editModal.hide();
          Swal.fire('success', "Holiday Master Details Saved Successfully !!!", 'success');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  SubmitHolidayMaster() {
    this.consubmitted = true;

    if (this.Holidaycalenderform.invalid) {
      return false;
    }

    if (this.datef.HolidayGreaterDate(this.holidaycalenderCtrl.FromDate.value, this.holidaycalenderCtrl.ToDate.value)) {
      Swal.fire('warning', "Holiday To Date Shoud be greater than Holiday From Date", 'warning');
      return false;
    }

    if ((this.datef.format(this.holidaycalenderCtrl.FromDate.value) != this.datef.format(this.holidaycalenderCtrl.ToDate.value)) && this.holidaycalenderCtrl.holidaytype.value == "O") {
      Swal.fire('warning', "Optional Holiday From Date and To Date Should be Same", 'warning');
      return false;
    }
    // if(this.holidaycalenderCtrl.Regular.value=="false" && this.holidaycalenderCtrl.Optional.value=="false"){     
    //   Swal.fire('warning', "Should select atleast one Regular Or  Optional", 'warning');
    //   return false;
    // }


    const req = new InputRequest();
    req.INPUT_01 = this.currentyear.toString();
    req.INPUT_02 = this.holidaycalenderCtrl.HolidayName.value;
    req.INPUT_03 = this.datef.format(this.holidaycalenderCtrl.FromDate.value);
    req.INPUT_04 = this.datef.format(this.holidaycalenderCtrl.ToDate.value);
    req.INPUT_05 = this.holidaycalenderCtrl.holidaytype.value;
    req.INPUT_06 = this.holidaycalenderCtrl.Remarks.value;
    req.INPUT_07 = this.holidaycalenderCtrl.status.value;
    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;
    this.showloader = true;
    this.service.postData(req, "SaveHolidayMaster").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          this.fillHolidayMastersdata(this.currentyear.toString());

          this.editModal.hide();
          Swal.fire('success', "Holiday Master Details Saved Successfully !!!", 'success');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  historyholiday() {
    const req = new InputRequest();

    req.INPUT_02 = "HOLIDAYS";

    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }

  Officehistory() {
    const req = new InputRequest();

    req.INPUT_02 = "OFFICE_TIME";

    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }


  HistoryHolidayrow(row): void {
    this.showloader = true;
    const data = row.rowData;
    const req = new InputRequest();
    req.INPUT_01 = data.holidaY_ID;
    req.INPUT_02 = "HOLIDAYS";
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }


  hidehistoryempModal(): void {
    this.historyempModal.hide();
  }

  resetoffice() {

    this.OfficeTimingsCtrl.Fromhours.setValue("");
    this.OfficeTimingsCtrl.FromMin.setValue("");
    this.OfficeTimingsCtrl.FromAMPM.setValue("AM");

    this.OfficeTimingsCtrl.ToHours.setValue("");
    this.OfficeTimingsCtrl.ToMin.setValue("");
    this.OfficeTimingsCtrl.ToAMPM.setValue("PM");

    this.OfficeTimingsCtrl.FromDay.setValue("");
    this.OfficeTimingsCtrl.ToDay.setValue("");

  }

  AddofficeSetting() {


    this.officeModal.show();

    this.consubmitted_office = false;

    this.officeform.reset();

    this.resetoffice();

    this.GetOfficeTimings();


  }

  SubmitofficeTimings() {

    this.consubmitted_office = true;

    if (this.officeform.invalid) {
      return false;
    }


    if (this.OfficeTimingsCtrl.FromDay.value == this.OfficeTimingsCtrl.ToDay.value) {
      Swal.fire('warning', "Week Start Day And End Day Should Not Same..!", 'warning');

      return false;
    }

    const req = new InputRequest();
    req.INPUT_01 = this.OfficeTimingsCtrl.Fromhours.value + ":" + this.OfficeTimingsCtrl.FromMin.value + " " + this.OfficeTimingsCtrl.FromAMPM.value;
    req.INPUT_02 = this.OfficeTimingsCtrl.ToHours.value + ":" + this.OfficeTimingsCtrl.ToMin.value + " " + this.OfficeTimingsCtrl.ToAMPM.value;
    req.INPUT_03 = this.OfficeTimingsCtrl.FromDay.value;
    req.INPUT_04 = this.OfficeTimingsCtrl.ToDay.value;
    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;

    this.showloader = true;

    this.service.postData(req, "SaveOfficeTimings").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.officeModal.hide();

          Swal.fire('success', "Office Working Hours  Saved Successfully !!!", 'success');

        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }
  UpdateOfficeTimings() {
    this.consubmitted_office = true;

    if (this.officeform.invalid) {
      return false;
    }

    if (this.OfficeTimingsCtrl.FromDay.value == this.OfficeTimingsCtrl.ToDay.value) {
      Swal.fire('warning', "Week Start Day And End Day Should Not Same..!", 'warning');
      return false;
    }


    const req = new InputRequest();
    req.INPUT_02 = this.OfficeTimingsCtrl.Fromhours.value + ":" + this.OfficeTimingsCtrl.FromMin.value + " " + this.OfficeTimingsCtrl.FromAMPM.value;
    req.INPUT_03 = this.OfficeTimingsCtrl.ToHours.value + ":" + this.OfficeTimingsCtrl.ToMin.value + " " + this.OfficeTimingsCtrl.ToAMPM.value;
    req.INPUT_04 = this.OfficeTimingsCtrl.FromDay.value;
    req.INPUT_05 = this.OfficeTimingsCtrl.ToDay.value;
    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;
    req.INPUT_01 = this.ofiiceid;


    this.showloader = true;

    this.service.postData(req, "UpdateOfficeTimings").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          //this.fillHolidayMastersdata(this.currentyear.toString());

          this.officeModal.hide();

          Swal.fire('success', "Office Working Hours Updated Successfully !!!", 'success');

        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }
  ofiiceid: string;
  GetOfficeTimings() {
    const req = new InputRequest();
    req.INPUT_01 = "";
    req.CALL_SOURCE = "WEB";

    req.USER_NAME = this.logUserName;

    this.showloader = true;

    this.service.postData(req, "GetOfficeTimings").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (data.Details != null && data.Details.length > 0) {
          let result = data.Details[0];

          this.IsOfficeSettingAdd = true;
          this.ofiiceid = result.officE_ID;
          this.OfficeTimingsCtrl.Fromhours.setValue(result.officE_FROM_TIME.split(':')[0].replace(" ", ""));
          this.OfficeTimingsCtrl.FromMin.setValue(result.officE_FROM_TIME.split(':')[1].replace("AM", "").replace("PM", "").replace(" ", ""));
          this.OfficeTimingsCtrl.FromAMPM.setValue(result.officE_FROM_TIME.split(' ')[1].replace(" ", ""));
          this.OfficeTimingsCtrl.ToHours.setValue(result.officE_TO_TIME.split(':')[0].replace(" ", ""));
          this.OfficeTimingsCtrl.ToMin.setValue(result.officE_TO_TIME.split(':')[1].replace("AM", "").replace("PM", "").replace(" ", ""));
          this.OfficeTimingsCtrl.ToAMPM.setValue(result.officE_TO_TIME.split(' ')[1].replace(" ", ""));
          this.OfficeTimingsCtrl.FromDay.setValue(result.officE_FROM_DAY);
          this.OfficeTimingsCtrl.ToDay.setValue(result.officE_TO_DAY);
        }
        else { this.officeModal.show(); this.IsOfficeSettingAdd = false; this.consubmitted_office = false; this.resetoffice(); }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

}


