import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { EmployeeSalaryInputRequest, InputRequest } from 'src/app/Interfaces/employee';
import { AgGridAngular } from 'ag-grid-angular';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { DigiServices } from 'src/app/Services/Digilocker.services';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-employeeloan-request',
  templateUrl: './employeeloan-request.component.html',
  styleUrls: ['./employeeloan-request.component.css']
})

export class EmployeeloanRequestComponent implements OnInit {
  tokens: any;
  Loanmonth: any;
  frameworkComponents: any;
  NgbDateStruct: any;
  Loantype: any;
  progressbar: boolean = false;
  message1: string = "";

  message: string = "";
  UploadDocType: string = "";
  UploadDocPath: string = "";
  progress: number = 0;
  submitted: boolean = false;
  showloader: boolean = false;
  gridColumnApi: any = [];
  defaultColDef: any = [];
  Loantypes: any;
  gridApi: any = [];
  public rowseledted: object;

  bankholdername: any; accnumber: any; ifsccode: any; bankname: any; branchname: any; loantpe: any; lonamount: any; month: any; remarks: any;

  SectionID: any;
  code: any;
  Accesstoken: any;
  FilesDD: any;
  Docpath: any;
  Authstatus: any;
  UploadFilesDD: any;
  IssuedDocView: boolean = true;
  viewbtnname: any;
  uri: any;
  DigiDetails: any[];
  showDocpath: any;
  hrefdata: any;
  Details: any;
  name: any;
  dob: any;
  gender: any;



  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  public components;
  preview: string = "";
  seldocpath: any;
  seldoctype: string = "";
  seldoccat: string = "";

  @ViewChild('TaxPaymentModal') TaxPaymentModal: ModalDirective;
  @ViewChild('previewModal') public previewModal: ModalDirective;
  @ViewChild('apprModal') public apprModal: ModalDirective;
  @ViewChild('mrPreviewModal') public mrPreviewModal: ModalDirective;
  @ViewChild('FilesList') public FilesList: ModalDirective;
  @ViewChild('DetailList') public DetailList: ModalDirective;
  @ViewChild('historyWarehousemodal') public historyWarehousemodal: ModalDirective;



  columnDefs = [
    { headerName: '#', with: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer' },
    { headerName: 'Employee Code', width: 150, field: 'emP_CODE' },
    { headerName: 'Employee Name', width: 200, field: 'emP_NAME' },
    { headerName: 'Employee Type', width: 150, field: 'emP_TYPE_NAME' },
    { headerName: 'Loan Type', width: 150, field: 'loaN_TYPE_NAME' },
    { headerName: 'Requested Amount', width: 150, field: 'reQ_AMOUNT' },
    { headerName: 'Months', width: 100, field: 'months' },
    { headerName: 'Approved Amount', width: 150, field: 'apR_AMOUNT' },
    { headerName: 'Status', width: 200, field: 'loaN_STATUS' },
  ];

  ngOnInit(): void {

    this.GetLoantypes();
    this.Getloanmonths();

  }
  constructor(private formBuilder: FormBuilder,
    private service: CommonServices,
    private router: Router,
    private datef: CustomDateParserFormatter,
    private sanitizer: DomSanitizer, private digiservice: DigiServices) {


    if (!this.logUserrole || this.isPasswordChanged == "0") {

      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      wrapText: true,
      autoHeight: true,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };


    this.components = {

      rowIdRenderer: function (params) {

        return '' + (params.rowIndex + 1);
      }
    }
  }

  EmployeepreviewrequestForm = this.formBuilder.group({

  })

  EmployeeLoanrequestForm = this.formBuilder.group({

    Loantype: ['', Validators.required],
    loanAmount: ['', Validators.required],
    LoanMonths: ['', Validators.required],
    DocPath: [''],
    LoanRemarks: [''],
    RegDoc: [''],
    Bankholdername: [''],
    Accno: [''],
    Ifsccode: [''],
    Bankname: [''],
    Branchname: [''],
    check: ['']
  });

  GetLoantypes() {
    this.showloader = true;
    const req = new EmployeeSalaryInputRequest();

    this.service.postData(req, "GetEmployeeLoanData").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Loantypes = data.Details;
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");

      }
    },
      error => console.log(error));

  }


  Getloanmonths() {
    this.showloader = true;
    const req = new EmployeeSalaryInputRequest;

    this.service.postData(req, "GetEmployeeMonthsData").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Loanmonth = data.Details;
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    },
      error => console.log(error));

  }
  Sumbitfun() {


    this.apprModal.show();
  }


  get reg() { return this.EmployeeLoanrequestForm.controls }




  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.LoadloanData();
  }
  LoadloanData() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.logUsercode }

    this.service.postData(obj, "GetEmployeeData").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));
  }


  Bankaccountdetails: any;
  Getbankaccountdetails() {

    this.showloader = true;
    const req=new InputRequest();
  req.INPUT_01=this.logUsercode;
     this.service.postData(req, "GetBankdetails").subscribe(data => {
       this.showloader = false;
       if (data.StatusCode == "100") {
         this.Bankaccountdetails = data.Details;
         this.bankholdername=this.Bankaccountdetails[0].accounT_HOLDER_NAME;
        this.EmployeeLoanrequestForm.controls. Bankholdername.setValue(this.bankholdername);
         this.accnumber=this.Bankaccountdetails[0].accounT_NUMBER;
         this.EmployeeLoanrequestForm.controls.Accno.setValue(this.accnumber);
         this.branchname=this.Bankaccountdetails[0].banK_BRANCH;
         this.EmployeeLoanrequestForm.controls.Branchname.setValue(this.accnumber);
         this.bankname=this.Bankaccountdetails[0].banK_NAME;
         this.EmployeeLoanrequestForm.controls.Bankname.setValue(this.bankname);
         this.ifsccode=this.Bankaccountdetails[0].ifsC_CODE;
         this.EmployeeLoanrequestForm.controls.Ifsccode.setValue(this.ifsccode);
         this.TaxPaymentModal.show();
         }
       else {
         this.showloader = false;
         this.Bankaccountdetails=[];
         Swal.fire("info","Please Update the bank Details in Employee Details Portal ","info");
         this.TaxPaymentModal.hide();
       }
     },
       error => console.log(error));
  
  
  }
  hidemesgModal()
  {
    this.apprModal.hide();
  }
  Taxclick(){
    this.submitted=false;
    this.EmployeeLoanrequestForm.reset();

    this.EmployeeLoanrequestForm.controls.Loantype.setValue('');
    this.TaxPaymentModal.show();
    this.EmployeeLoanrequestForm.controls.LoanMonths.setValue('');
    this.Getbankaccountdetails();
  }

  hideTaxModal() {
    this.progressbar = false;
    this.submitted = false;
    this.EmployeeLoanrequestForm.reset();
    this.TaxPaymentModal.hide();
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }




  uploadFile(event) {
    this.Authstatus = "";
    this.seldocpath = "";
    this.seldoctype = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      this.seldoccat = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype = 'IMAGE';
      else
        this.seldoctype = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        this.progressbar = false;
        return false;
      }

      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath = reader.result as string;
          this.preview = this.seldocpath;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', "EmployeeLoanRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.'
            this.uploadFinished(event.body);
            this.progressbar = true;
          }

        });
    }
  }
  public uploadFinished = (event) => {
    this.EmployeeLoanrequestForm.controls.DocPath.setValue(event.fullPath);
  }
  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
  }
  base64sstringpath: any;
  certificates() {

    this.base64sstringpath = this.EmployeeLoanrequestForm.controls.DocPath.value;

    this.decryptFile(this.base64sstringpath);

  }

  Docpriview(val) {

    // const blob = this.service.s_sd((val).split(',')[1], this.seldoccat);
    // const blobUrl = URL.createObjectURL(blob);
    // this.preview = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
    //this.preview = this.sanitizer.bypassSecurityTrustUrl(val);
    //this.preview = this.preview.changingThisBreaksApplicationSecurity;
    this.previewModal.show();

  }


  downloadPdf(base64String, fileName) {

    // const blob = this.service.s_sd((base64String).split(',')[1], "application/pdf");
    //     const blobUrl = URL.createObjectURL(blob);    
    const source = base64String;//`data:application/pdf;base64,${base64String}`;
    const link = document.createElement("a");
    link.href = source;
    link.download = `${fileName}.pdf`
    link.click();
  }

  decryptFile(imagepth: string) {
    if (imagepth) {
      //this.pdf="wwwroot\\MedicalReimbursementRequest\\14-06-2021\\sample_14_06_2021_10_49_59.pdf";
      this.service.decryptFile(imagepth, "DecryptFile")
        .subscribe(data => {
          var ext = imagepth.split('.').pop();
          var filename = imagepth.replace(/^.*[\\\/]/, '');

          if (ext == "pdf") {
            this.downloadPdf(data.docBase64, filename);
          }
          else {
            var newTab = window.open();
            newTab.document.body.innerHTML = '<img src=' + data.docBase64 + ' width="100%" height="100%">';

          }

        },
          error => {
            console.log(error);
          });
    }
  }
       Previewfunction()
    {
            const req = new InputRequest();
    
      this.submitted=true;
    
      if (this.EmployeeLoanrequestForm.invalid) 
      {
        return;
    }
    
    if(!this.reg.DocPath.value)
      {
        Swal.fire('info', "Please Upload Document", 'info');
        return false;
      }
      if (this.EmployeeLoanrequestForm.controls.check.value == "" || this.EmployeeLoanrequestForm.controls.check.value == null || this.EmployeeLoanrequestForm.controls.check.value == undefined) {
        Swal.fire("info", "Please Select the checkbox", "info");
        this.showloader = false;
        return false;
      }
     this.loantpe= this.reg.Loantype.value;
     this.lonamount= this.reg.loanAmount.value;
     this.month= this.EmployeeLoanrequestForm.controls.LoanMonths.value;
     this.remarks=  this.reg.LoanRemarks.value;
     this.mrPreviewModal.show();
}



  Loanchange()
  {
this.EmployeeLoanrequestForm.controls.loanAmount.setValue('');
this.Loanmonth=[];
this.EmployeeLoanrequestForm.controls.LoanMonths.setValue('');
this.EmployeeLoanrequestForm.controls.DocPath.setValue('');
this.EmployeeLoanrequestForm.controls.RegDoc.setValue('');
this.Getloanmonths();
}

OKclick()
{
  this.showloader = true;
    
  const req = new InputRequest();

  req.INPUT_01 = this.logUsercode;//rowsleted.tokeN_ID this.dep.TokenNo.value;
  req.INPUT_02 = this.EmployeeLoanrequestForm.controls.Loantype.value;
  req.INPUT_03 = this.EmployeeLoanrequestForm.controls.LoanMonths.value;
  req.INPUT_04 = this.EmployeeLoanrequestForm.controls.loanAmount.value; //Receipt Out
  //  req.INPUT_05= this.EmployeeLoanrequestForm.controls.DocPath.value;
if(this.Authstatus==0){
    req.INPUT_05=this.EmployeeLoanrequestForm.controls.DocPath.value;
  }
  if(this.Authstatus==1){
    req.INPUT_05=this.Docpath;

  }
  req.INPUT_06= this.EmployeeLoanrequestForm.controls.LoanRemarks.value;
  req.INPUT_07= this.logUserrole;
  req.INPUT_08=this.EmployeeLoanrequestForm.controls.Bankname.value;
  req.INPUT_09=this.EmployeeLoanrequestForm.controls.Branchname.value;
  req.INPUT_10=this.EmployeeLoanrequestForm.controls.Accno.value;
  req.INPUT_11=this.EmployeeLoanrequestForm.controls.Ifsccode.value;
  req.INPUT_12=this.EmployeeLoanrequestForm.controls.Bankholdername.value;
  
  req.USER_NAME =this.logUsercode;
  req.CALL_SOURCE = "WEB";

  this.service.postData(req, "SaveEmployeeloanDetails").subscribe(data => {
    this.showloader = false;

    if (data.StatusCode == "100") {
      //this.LoadloanData();
      this.reloadCurrentRoute();
      // this.TaxPaymentForm.reset();
      // this.TaxPaymentModal.hide();
      Swal.fire('success', "Request Forwarded to P&A Section", 'success');
      
    }
    else{
    Swal.fire('warning', data.StatusMessage, 'warning');
    }
  },
    error => console.log(error));
}
Cancelclick()
{
this.reloadCurrentRoute();
}

    hidepreviewTaxModal(){
    
      this.mrPreviewModal.hide();
    }
    
 
  Submitcchange() {

  }


  CallDigilock() {
    if (!this.FilesDD && !this.UploadFilesDD) {
      this.showloader = true;
      var pagename = "Employeeloan";
      //this.digiservice.Digilock(pagename,(function(a) {
      this.digiservice.Digilock(pagename, (a, b) => {
        this.Accesstoken = b;
        this.FilesDD = a.UserFiles.items;
        if (a.UserUploadFiles)
          this.UploadFilesDD = a.UserUploadFiles.items;
        this.FilesList.show();
        this.showloader = false;
      });
    }
    else {
      this.FilesList.show();
    }
  }

  ShowFiles(val, mimer, num, name) {
    this.showloader = true;
    this.digiservice.ShowFiles(val, mimer, num, name, this.Accesstoken, (a, b) => {
      this.Docpath = a;
      this.decryptFile1(this.Docpath);
      this.uri = b;
      this.Authstatus = "1";
      this.viewbtnname = name.substring(0, 5);
      this.FilesList.hide();
      this.showloader = false;
    });
  }

  decryptFile1(filepath) {
    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          console.log(data.docBase64);


          const blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
          const blobUrl = URL.createObjectURL(blob);
          this.hrefdata = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        },
          error => {
            console.log(error);
            this.showDocpath = "";
          });
    }
    else
      this.showDocpath = "";
  }

  GetDetails(uri) {
    this.showloader = true;
    this.digiservice.GetDetails(uri, this.Accesstoken, (a) => {
      console.log(a);
      if (a.Status == "Success") {

        //this.Details=a.CertIssuerData.Certificate.IssuedTo.Person;
        if (this.viewbtnname == "Aadha") {
          this.name = a.CertIssuerData.KycRes.UidData.Poi.name;
          this.gender = a.CertIssuerData.KycRes.UidData.Poi.gender;
          this.dob = a.CertIssuerData.KycRes.UidData.Poi.dob;
        }
        else {
          this.name = a.CertIssuerData.Certificate.IssuedTo.Person.name;
          this.gender = a.CertIssuerData.Certificate.IssuedTo.Person.gender;
          this.dob = a.CertIssuerData.Certificate.IssuedTo.Person.dob;
        }



        this.showloader = false;
        this.DetailList.show();

      }
      else {
        this.showloader = false;
      }
    });
  }



  ViewDoc() {
    // window.open(this.showDocpath, '_blank');
    // const link = 'data:image/jpeg;base64,'+this.showDocpath;
    // var newWindow = window.open();
    // newWindow.document.write('<img src="' + link + '" />');
    //var splitted = this.showDocpath.split(",", 10);
    //const blob = b64toBlob(this.showDocpath, splitted[0]);
    var path = this.showDocpath.split(',')
    var b = this.service.s_sd(path[1], 'application/pdf');
    var l = document.createElement('a');
    l.href = window.URL.createObjectURL(b);
    l.download = "Document" + ".pdf";
    document.body.appendChild(l);
    l.click();
    document.body.removeChild(l);
  }



  DocDivClick(val) {
    this.IssuedDocView = val;
  }

  hideFilesList() {
    this.FilesList.hide();
  }

  hideDetailList() {
    this.DetailList.hide();

  }
  historylist: any;


  Historyclick() {

    this.showloader = true;
    const req = new EmployeeSalaryInputRequest();
    req.TYPEID = "EMP_LOAN_HISTORY";
    req.INPUT_01 = this.logUsercode;
    this.service.postData(req, "GetFamilyrelation").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.historyWarehousemodal.show();
        this.historylist = data.Details;
      }
      else {
        this.showloader = false;
        Swal.fire("info", "No history Found", "info");
        this.historyWarehousemodal.hide();
      }
    },
      error => console.log(error));

  }
  hidehistorymasterModal() {
    this.historyWarehousemodal.hide();
  }

}
