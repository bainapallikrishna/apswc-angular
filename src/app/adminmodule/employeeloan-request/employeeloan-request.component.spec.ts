import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeloanRequestComponent } from './employeeloan-request.component';

describe('EmployeeloanRequestComponent', () => {
  let component: EmployeeloanRequestComponent;
  let fixture: ComponentFixture<EmployeeloanRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeloanRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeloanRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
