import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-news-update',
  templateUrl: './news-update.component.html',
  styleUrls: ['./news-update.component.css']
})
export class NewsUpdateComponent implements OnInit {
  newsForm: FormGroup;
  public scrollmessage: string;
  submitted = false;
  newslist: any = [];
  newstitle: string;
  newscontent: string;
  newslink: string;
  public response: { dbPath: '' };
  baseurl: string;
  newshiden = true;
  constructor(private http: HttpClient, private formBuilder: FormBuilder, private service: CommonServices) {
    //this.baseurl="http://uat.apswc.ap.gov.in/apswcapp"
  }

  ngOnInit(): void {

    this.newsForm = this.formBuilder.group({

      newstitle: ['', Validators.required],
      newscontent: ['', Validators.required]


    });

    this.Getnewsmessage();
  }
  AddNews() {
    this.newshiden = false;
  }
  keyPress(event: any) {
    let charCode = event.keyCode;
    if ((charCode <= 32) || (charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123)) {
      return true;
    }
    else {
      Swal.fire('info', 'Enter Only Characters', 'info');
      return false;
    }
    return true;
  }

  Getnewsmessage() {
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "GET_NEWS";

    this.service.postData(req, "GetScrollNewMessage").subscribe(data => {
      if (data.StatusCode == "100") {
        this.newslist = data.Details;
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });

  }

  get f() { return this.newsForm.controls; }

  editnews(msg: any) {
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "UPD_NEWS";
    req.INPUT_01 = msg.contenT_ID;
    req.INPUT_02 = msg.contenT_TYPE;
    req.INPUT_03 = msg.contenT_TITLE;
    req.INPUT_04 = msg.contenT_BODY;
    req.INPUT_05 = msg.contenT_LINK;
    req.INPUT_06 = msg.contenT_PHOTO;
    req.INPUT_07 = "0";
    this.service.postData(req, "ScrollNewMessageInsert").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'success');
        this.newshiden = true;
        this.Getnewsmessage();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });


  }
  onSubmit() {
    this.submitted = true;

    if (!this.newsForm.value.newstitle) {
      Swal.fire("info", "Please Enter News Title", "info")
      return;
    }
    else if (!this.newsForm.value.newscontent) {
      Swal.fire("info", "Please Enter News content", "info")
      return;
    }


    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "INS_NEWS";
    req.INPUT_01 = "N";
    req.INPUT_02 = this.newsForm.value.newstitle;
    req.INPUT_03 = this.newsForm.value.newscontent;
    req.INPUT_04 = this.newsForm.value.newslink;
    req.INPUT_05 = this.createImgPath(this.response.dbPath);

    this.service.postData(req, "ScrollNewMessageInsert").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'info');

        this.Getnewsmessage();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });

  }

  public uploadFinished = (event) => {
    this.response = event;

  }

  public createImgPath = (serverPath: string) => {
    //return this.baseurl+`/${serverPath}`;
    return `${this.service.filebaseUrl.replace("api/FilesUpload", "")}${serverPath}`;
  }
  onReset() {
    this.submitted = false;
    this.newsForm.reset();
  }

}
