import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ApexNonAxisChartSeries,
  ApexAxisChartSeries,
  ApexPlotOptions,
  ApexChart,
  ApexFill,
  ChartComponent,
  ApexDataLabels,
  ApexXAxis,
  ApexResponsive,
  ApexLegend,
  ApexTitleSubtitle,
  ApexMarkers,
  ApexYAxis,
  ApexTooltip,
  ApexStroke,
  ApexGrid,
} from "ng-apexcharts";

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  labels: string[];
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  xaxis: ApexXAxis;
  fill: ApexFill;
  legend: ApexLegend;
  responsive: ApexResponsive[];
};
export type ChartOptions1 = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  labels: string[];
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  xaxis: ApexXAxis;
  fill: ApexFill;
  legend: ApexLegend;
  responsive: ApexResponsive[];
};
export type LineChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  stroke: ApexStroke;
  dataLabels: ApexDataLabels;
  markers: ApexMarkers;
  colors: string[];
  yaxis: ApexYAxis;
  grid: ApexGrid;
  legend: ApexLegend;
  title: ApexTitleSubtitle;
};
export type PolarChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: any;
  stroke: ApexStroke;
  legend: ApexLegend;
  fill: ApexFill;
};

@Component({
  selector: 'app-finance-dashboard',
  templateUrl: './finance-dashboard.component.html',
  styleUrls: ['./finance-dashboard.component.css']
})
export class FinanceDashboardComponent implements OnInit {


  @ViewChild("linechart") linechart: ChartComponent;
  public lineChartOptions: Partial<LineChartOptions>;

  @ViewChild("ploarchart") polarchart: ChartComponent;
  public polarChartOptions: Partial<PolarChartOptions>;
 
  constructor() {

    this.lineChartOptions = {
      series: [
        {
          name: "Total Loan Amount",
          data: [28, 29, 33, 36, 32, 32, 33]
        },
        {
          name: "Recovered Amount",
          data: [12, 11, 14, 18, 17, 13, 13]
        }
      ],
      chart: {
        height: 350,
        type: "line",
        dropShadow: {
          enabled: true,
          color: "#000",
          top: 18,
          left: 7,
          blur: 10,
          opacity: 0.2
        },
        toolbar: {
          show: false
        }
      },
      colors: ["#77B6EA", "#16D39A"],
      dataLabels: {
        enabled: false
      },
      stroke: {
        curve: "smooth"
      },
      // title: {
      //   text: "Average High & Low Temperature",
      //   align: "left"
      // },
      grid: {
        borderColor: "#e7e7e7",
        row: {
          colors: ["#f3f3f3", "transparent"], // takes an array which will be repeated on columns
          opacity: 0.5
        }
      },
      markers: {
        size: 1
      },
      xaxis: {
        categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul"],
        title: {
          text: "Month"
        }
      },
      yaxis: {
        title: {
          text: "Amount"
        },
        min: 5,
        max: 40
      },
      legend: {
        show: true,
        position: "top",
        horizontalAlign: "right",
        floating: true,
      }
    };

    this.polarChartOptions = {
      series: [14, 23, 21, 17],
      chart: {
        height: 300,
        type: "polarArea"
      },
      stroke: {
        colors: ["#fff"]
      },
      fill: {
        opacity: 0.8
      },
      labels: ["Home Loan", "Car Loan", "Personal Loan", "Health Loan"],
      legend: {
        show: true,
        position: 'bottom',
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };
}

  ngOnInit(): void {
  }

}
