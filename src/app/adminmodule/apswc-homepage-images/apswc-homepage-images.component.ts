import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-apswc-homepage-images',
  templateUrl: './apswc-homepage-images.component.html',
  styleUrls: ['./apswc-homepage-images.component.css']
})
export class ApswcHomepageImagesComponent implements OnInit {

  formimages: FormGroup;
  cmphoto: string;
  cmphoto1: string;
  ministerphoto1: string;
  cmname: string;
  cmdesignation: string;
  ministerphoto: string;
  ministername: string;
  ministerdesiganation: string;
  public cmresponse: { dbPath: '' };
  public ministerresponse: { dbPath: '' };

  progress: number;
  message: string;
  progress1: number;
  message1: string;
  baseurl: string;
  constructor(private http: HttpClient, private service: CommonServices) {
    //  this.baseurl="http://localhost/APSWCAPP/";
    this.baseurl = "http://uat.apswc.ap.gov.in/apswcapp/";
  }

  ngOnInit(): void {
    this.getcontentdata();
    this.formimages = new FormGroup({
      cmname: new FormControl("", Validators.required),
      cmdesignation: new FormControl("", Validators.required),
      cmphoto: new FormControl("", Validators.required),
      ministerphoto: new FormControl("", Validators.required),
      ministername: new FormControl("", Validators.required),
      ministerdesiganation: new FormControl("", Validators.required)

    });
  }

  get subf() { return this.formimages.controls }
  getcontentdata() {
    this.service.getData("GetHomePageConent").subscribe(data => {
      if (data.StatusCode == "100") {
        this.cmphoto = data.Details[0]["cM_PHOTO"];
        this.subf.ministername.setValue(data.Details[0]["ministeR_NAME"])
        this.ministerphoto = data.Details[0]["ministeR_PHOTO"];

        this.subf.ministerdesiganation.setValue(data.Details[0]["ministeR_DESIGNATION"]);
      }
      else {
        Swal.fire('error', data.statusmessage, 'error');
      }
    });
  }


  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }

    let fileToUpload = <File>files[0];
    const formData = new FormData();
    //for (let file of files)  
    //formData.append(file.name, file);  
    formData.append('file', fileToUpload, fileToUpload.name);
    //formData.append('username', "chandu");

    this.http.post(this.baseurl + 'api/FilesUpload/GalleryUploadFileDetails', formData, { reportProgress: true, observe: 'events' })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.message = 'Upload success.';

          this.onUploadcmFinished(event.body);
        }
      });
  }

  public onUploadcmFinished = (event) => {
    this.cmresponse = event;
    this.cmphoto1 = this.cmresponse.dbPath;

  }

  public uploadMinisterFile = (files) => {
    if (files.length === 0) {
      return;
    }

    let fileToUpload = <File>files[0];
    const formData = new FormData();
    //for (let file of files)  
    //formData.append(file.name, file);  
    formData.append('file', fileToUpload, fileToUpload.name);
    //formData.append('username', "chandu");
    this.http.post(this.baseurl + 'api/FilesUpload/GalleryUploadFileDetails', formData, { reportProgress: true, observe: 'events' })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress1 = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.message1 = 'Upload success.';

          this.onUploadministerFinished(event.body);
        }
      });
  }

  public onUploadministerFinished = (event) => {
    this.ministerresponse = event;
    this.ministerphoto1 = this.ministerresponse.dbPath;
  }

  public createImgPath = (serverPath: string) => {
    return this.baseurl + `${serverPath}`;
  }
  UpdateCMDetails() {
    if (!this.formimages.value.cmname) {
      Swal.fire("info", "Please Enter CM Name", "info");
      return;
    }
    else if (!this.cmphoto1) {
      Swal.fire("info", "Please upload cm photo", "info");
      return;
    }

    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "UPD_CM";
    req.INPUT_01 = this.formimages.value.cmname;
    req.INPUT_02 = this.formimages.value.cmdesignation;
    req.INPUT_03 = this.createImgPath(this.cmphoto1);


    this.service.postData(req, "HomePageConentInsert").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'info');

        this.getcontentdata();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });


  }

  UpdateMinisterDetails() {

    if (!this.formimages.value.ministername) {
      Swal.fire("info", "Please Enter CM Name", "info");
      return;
    }
    else if (!this.formimages.value.ministerdesiganation) {
      Swal.fire("info", "Please Enter Minister", "info");
      return;
    }

    else if (!this.ministerphoto1) {
      Swal.fire("info", "Please upload Minister Photo", "info");
      return;
    }




    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "UPD_MINISTER";
    req.INPUT_01 = this.formimages.value.ministername;
    req.INPUT_02 = this.formimages.value.ministerdesiganation;
    req.INPUT_03 = this.createImgPath(this.ministerphoto1);



    this.service.postData(req, "HomePageConentInsert").subscribe(data => {
      if (data.StatusCode == "100") {
        Swal.fire('info', data.StatusMessage, 'info');

        this.getcontentdata();
      }
      else {
        Swal.fire('error', data.StatusMessage, 'error');
      }
    });




  }

}
