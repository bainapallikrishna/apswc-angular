import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApswcHomepageImagesComponent } from './apswc-homepage-images.component';

describe('ApswcHomepageImagesComponent', () => {
  let component: ApswcHomepageImagesComponent;
  let fixture: ComponentFixture<ApswcHomepageImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApswcHomepageImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApswcHomepageImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
