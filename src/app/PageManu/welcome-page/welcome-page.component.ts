import { Component, Output,EventEmitter,OnInit } from '@angular/core';
import {HttpClient,HttpEventType} from '@angular/common/http';
@Component({
  selector: 'app-welcome-page',
  templateUrl: './welcome-page.component.html',
  styleUrls: ['./welcome-page.component.css']
})
export class WelcomePageComponent implements OnInit {

  public progress: number;
  public message: string;
  //@Output() public onUploadFinished = new EventEmitter();
  constructor(private http:HttpClient) { }

  ngOnInit(): void {

  }

  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    console.log(fileToUpload),
     console.log(fileToUpload.name);
    //this.http.post('https://localhost:44310/api/upload', formData, {reportProgress: true, observe: 'events'})
      //.subscribe(event => {
        //if (event.type === HttpEventType.UploadProgress)
          //this.progress = Math.round(100 * event.loaded / event.total);
        //else if (event.type === HttpEventType.Response) {
          this.message = 'Upload success.';
          //this.onUploadFinished.emit(event.body);
       // }
      //});
  }

}
