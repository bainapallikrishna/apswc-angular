import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { DynamicbuttonRendererComponent } from 'src/app/custome-directives/dynamic-button-render.components';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-existing-stock-entry',
  templateUrl: './existing-stock-entry.component.html',
  styleUrls: ['./existing-stock-entry.component.css']
})
export class ExistingStockEntryComponent implements OnInit {

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    
    { headerName: 'Depositor Name', width: 200, field: 'depositoR_NAME' },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID' },
    { headerName: 'Commodity Name', width: 200, field: 'commodity' },
    { headerName: 'Stack In Date', width: 100, field: 'stacK_IN_DATE' },
    { headerName: 'Godown Name', width: 100, field: 'godowN_NAME' },
    { headerName: 'Compartment Name', width: 150, field: 'compartmenT_NAME' },
    { headerName: 'Stack Name', width: 100, field: 'stacK_NAME' },
    { headerName: 'Bags', width: 100, field: 'no_of_bags' },
    { headerName: "Weight(MT's)", width: 100, field: 'weight' },
  ];

  StockEntryForm: FormGroup;
  isSubmit: boolean = false;
  showloader: boolean = true;
  defaultColDef: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];
  frameworkComponents: any;
  public components;
  NgbDateStruct: any;
  DeposiList: any = [];
  StocksList: any = [];
  BookingList: any = [];
  CommoList: any = [];
  DodownsList: any = [];
  CompartsList: any = [];
  StacksList: any = [];
  StackTypeList: any = [];

  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('editModal') public editModal: ModalDirective;

  constructor(private service: CommonServices,
    private router: Router,
    private datef: CustomDateParserFormatter,
    private EncrDecr: EncrDecrServiceService,
    private formBuilder: FormBuilder) {
    this.frameworkComponents = {
      buttonRenderer: DynamicbuttonRendererComponent,
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }

  ngOnInit(): void {
    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }

    this.StockEntryForm = this.formBuilder.group({
      DepositorName: [null, Validators.required],
      ReservationID: [null, Validators.required],
      CommodityName: [null, Validators.required],
      GodownName: [null, Validators.required],
      CompartmentName: [null, Validators.required],
      StackName: [null, Validators.required],
      StackType: [null, Validators.required],
      StackInDate: ['', Validators.required],
      NoOfGunnyBags: [''],
      NoOfBags: ['', Validators.required],
      BagWeight: ['', Validators.required],
      MarketValue: ['', Validators.required]
    });
    this.LoadDepositors();
    this.LoadStackTypes();
    this.LoadGodowns();
    this.showloader = false;
  }

  get stock() { return this.StockEntryForm.controls; }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.StocksList);
    this.LoadStocksData();
  }

  AddStockEntry() {
    this.StockEntryForm.reset();
    this.isSubmit = false;
    this.editModal.show();
  }

  SaveStockDetails() {
    this.isSubmit = true;
    // stop here if form is invalid
    if (this.StockEntryForm.invalid) {
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.workLocationCode;
    req.INPUT_03 = this.stock.ReservationID.value;
    req.INPUT_04 = this.stock.CommodityName.value;
    req.INPUT_05 = this.stock.GodownName.value;
    req.INPUT_06 = this.stock.CompartmentName.value;
    req.INPUT_07 = this.stock.StackName.value;
    req.INPUT_08 = this.stock.StackType.value;
    req.INPUT_09 = this.stock.NoOfBags.value;
    req.INPUT_11 = "1";
    req.INPUT_12 = this.stock.DepositorName.value.split(':')[0];
    req.INPUT_13 = this.stock.DepositorName.value.split(':')[1];
    req.INPUT_19 = this.stock.BagWeight.value;
    req.INPUT_21 = this.stock.MarketValue.value;
    req.INPUT_22 = this.stock.NoOfBags.value;
    req.INPUT_24 = this.datef.format(this.stock.StackInDate.value);

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = 'WEB';
    this.service.postData(req, "SaveExistingStockDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Stock Deatils Saved Successfully !!!", 'success');
        this.LoadStocksData();
        this.editModal.hide();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => {
        this.showloader = false;
        console.log(error)
      });

  }

  LoadStocksData() {
    this.showloader = true;
    this.StocksList = [];
    var data = { "INPUT_01": this.workLocationCode };
    this.service.postData(data, "GetStocksList").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.StocksList = data.Details;
        this.gridApi.setRowData(this.StocksList);
      }
    },
      error => {
        console.log(error);
        this.showloader = false;
      })
  }

  LoadGodowns() {
    this.showloader = true;
    this.DodownsList = [];
    var data = { "INPUT_01": this.workLocationCode };
    this.service.postData(data, "GetGodowns").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.DodownsList = data.Details;
      }
    },
      error => {
        console.log(error);
        this.showloader = false;
      })
  }

  LoadStackTypes() {
    this.showloader = true;
    this.StackTypeList = [];
    this.service.getData("GetStackTypes").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.StackTypeList = data.Details;
      }
    },
      error => console.log(error));
  }

  LoadDepositors() {
    this.showloader = true;
    this.DeposiList = [];
    var data = { "INPUT_01": this.workLocationCode };
    this.service.postData(data, "GetDepositorList").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.DeposiList = data.Details;
      }
    },
      error => {
        console.log(error);
        this.showloader = false;
      })
  }

  LoadBookings() {
    this.BookingList = [];
    if (this.stock.DepositorName.value) {
      this.showloader = true;
      var data = { "INPUT_01": this.workLocationCode, "INPUT_02": this.stock.DepositorName.value.split(':')[0] };
      this.service.postData(data, "GetBookingList").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.BookingList = data.Details;
        }
      },
        error => {
          console.log(error);
          this.showloader = false;
        })
    }
  }

  LoadCommodities() {
    this.CommoList = [];
    if (this.stock.ReservationID.value) {
      this.showloader = true;
      var data = { "INPUT_01": this.stock.ReservationID.value };
      this.service.postData(data, "GetCommodityList").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.CommoList = data.Details;
        }
      },
        error => {
          console.log(error);
          this.showloader = false;
        })
    }
  }

  LoadCompartments() {
    this.CompartsList = [];
    if (this.stock.GodownName.value) {
      this.showloader = true;
      var data = { "INPUT_01": this.workLocationCode, "INPUT_02": this.stock.GodownName.value };
      this.service.postData(data, "GetCompartments").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.CompartsList = data.Details;
        }
      },
        error => {
          console.log(error);
          this.showloader = false;
        })
    }
  }

  LoadStacks() {
    this.StacksList = [];
    if (this.stock.CompartmentName.value) {
      this.showloader = true;
      var data = { "INPUT_01": this.workLocationCode, "INPUT_02": this.stock.GodownName.value, "INPUT_03": this.stock.CompartmentName.value };
      this.service.postData(data, "GetStacks").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.StacksList = data.Details;
        }
      },
        error => {
          console.log(error);
          this.showloader = false;
        })
    }
  }

}
