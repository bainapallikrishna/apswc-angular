import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExistingStockEntryComponent } from './existing-stock-entry.component';

describe('ExistingStockEntryComponent', () => {
  let component: ExistingStockEntryComponent;
  let fixture: ComponentFixture<ExistingStockEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExistingStockEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistingStockEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
