import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { DynamicbuttonRendererComponent } from 'src/app/custome-directives/dynamic-button-render.components';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';

@Component({
  selector: 'app-storage-loss-entry',
  templateUrl: './storage-loss-entry.component.html',
  styleUrls: ['./storage-loss-entry.component.css']
})
export class StorageLossEntryComponent implements OnInit {

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },

    { headerName: 'Depositor Name', width: 200, field: 'depositoR_NAME' },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID' },
    { headerName: 'Commodity Name', width: 200, field: 'commodity' },
    { headerName: 'Stack In Date', width: 100, field: 'stacK_IN_DATE' },
    { headerName: 'Godown Name', width: 100, field: 'godowN_NAME' },
    { headerName: 'Compartment Name', width: 150, field: 'compartmenT_NAME' },
    { headerName: 'Stack Name', width: 100, field: 'stacK_NAME' },
    { headerName: 'Bags', width: 100, field: 'no_of_bags' },
    { headerName: "Weight(MT's)", width: 100, field: 'weight' },
  ];

  LossEntryForm: FormGroup;
  isSubmit: boolean = false;
  showloader: boolean = true;
  defaultColDef: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];
  frameworkComponents: any;
  public components;
  NgbDateStruct: any;
  LossList: any = [];
  WHTypelist: any = [];
  whlist: any = [];
  DeposiList: any = [];
  BookingList: any = [];
  CommoList: any = [];

  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('editModal') public editModal: ModalDirective;

  constructor(private service: CommonServices,
    private router: Router,
    private datef: CustomDateParserFormatter,
    private EncrDecr: EncrDecrServiceService,
    private formBuilder: FormBuilder) {
    this.frameworkComponents = {
      buttonRenderer: DynamicbuttonRendererComponent,
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }

  ngOnInit(): void {
    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }

    this.LossEntryForm = this.formBuilder.group({
      WHtype: [null, Validators.required],
      Whname: [null, Validators.required],
      DepositorName: [null, Validators.required],
      ReservationID: [null, Validators.required],
      CommodityName: [null, Validators.required],
      GodownName: [null, Validators.required],
      CompartmentName: [null, Validators.required],
      StackName: [null, Validators.required],
      StackType: [null, Validators.required],
      StackInDate: ['', Validators.required],
      NoOfGunnyBags: [''],
      NoOfBags: ['', Validators.required],
      BagWeight: ['', Validators.required],
      MarketValue: ['', Validators.required]
    });
    //this.WareHousesData();
    // this.LoadStackTypes();
    // this.LoadGodowns();
    this.showloader = false;
    this.LoadWhDetails('WHTYPE');
  }

  get loss() { return this.LossEntryForm.controls; }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.LossList);
    this.LoadLossesData();
  }

  AddStorageLoss() {
    this.LossEntryForm.reset();
    this.isSubmit = false;
    this.editModal.show();
  }

  LoadLossesData() {
    this.showloader = true;
    this.LossList = [];
    var data = { "INPUT_01": this.workLocationCode };
    this.service.postData(data, "GetStocksList").subscribe(data => {
      this.showloader = false;
      this.gridApi.setRowData(this.LossList);
      if (data.StatusCode == "100") {
        this.LossList = data.Details;
        this.gridApi.setRowData(this.LossList);
      }
    },
      error => {
        console.log(error);
        this.showloader = false;
      })
  }

  LoadWarehouses(val) {
    this.loss.Whname.setValue(null);
    this.LoadWhDetails(val);
  }

  LoadWhDetails(val) {

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_08 = this.loss.WHtype.value;
    this.service.postData(req, "GetWHDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (val == "WHTYPE")
          this.WHTypelist = data.Details;
        else
          this.whlist = data.Details;
      }

    },
      error => console.log(error));


  }

  LoadDepositors() {
    this.showloader = true;
    this.DeposiList = [];
    var data = { "INPUT_01": this.loss.Whname.value };
    this.service.postData(data, "GetDepositorList").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.DeposiList = data.Details;
      }
    },
      error => {
        console.log(error);
        this.showloader = false;
      })
  }

  LoadBookings() {
    this.BookingList = [];
    if (this.loss.DepositorName.value) {
      this.showloader = true;
      var data = { "INPUT_01": this.workLocationCode, "INPUT_02": this.loss.DepositorName.value.split(':')[0] };
      this.service.postData(data, "GetBookingList").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.BookingList = data.Details;
        }
      },
        error => {
          console.log(error);
          this.showloader = false;
        })
    }
  }

  LoadCommodities() {
    this.CommoList = [];
    if (this.loss.ReservationID.value) {
      this.showloader = true;
      var data = { "INPUT_01": this.loss.ReservationID.value };
      this.service.postData(data, "GetCommodityList").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.CommoList = data.Details;
        }
      },
        error => {
          console.log(error);
          this.showloader = false;
        })
    }
  }

  SaveStockDetails() {

  }

}
