import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorageLossEntryComponent } from './storage-loss-entry.component';

describe('StorageLossEntryComponent', () => {
  let component: StorageLossEntryComponent;
  let fixture: ComponentFixture<StorageLossEntryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorageLossEntryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorageLossEntryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
