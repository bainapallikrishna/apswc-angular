import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { FormControl, FormGroup, ReactiveFormsModule, FormBuilder, Validators } from '@angular//forms'
import { BehaviorSubject, from, Observable } from 'rxjs';
import { ThrowStmt } from '@angular/compiler';
import { Captcha, Login } from '../Interfaces/login';
import { map, first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesCommonService } from '../services-common.service';
import { AuthService } from 'src/app/Services/auth.service';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { CommonServices } from 'src/app/Services/common.services';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  insertForm: FormGroup;
  loading = false;
  Username: FormControl;
  Password: FormControl;
  recaptchaReactive: FormControl;
  loginModel: Login;
  InputModel: InputRequest;
  captchvalue: string;
  imageBase64: string;
  captresponse: Observable<Captcha>;
  capres: Captcha;
  constructor(private http: HttpClient, private fb: FormBuilder, private objservice: ServicesCommonService,
    private route: ActivatedRoute, private router: Router, private authService: AuthService, private service: CommonServices) { }


  lattitude: string;
  longitute: string;


  ngOnInit(): void {

    // Initialize FormGroup using FormBuilder
    this.insertForm = this.fb.group({
      Username: this.Username,
      Password: this.Password,
      recaptchaReactive: this.recaptchaReactive
    });

    this.LoadCaptcha();

    this.CheckLocation();
  }

  Locataion() {

    this.service.getPosition().then(pos => {
      this.longitute = pos.lat;
      this.lattitude = pos.lng;
    });
  }

  CheckLocation() {
    if (navigator.geolocation) {
      this.service.CheckLocation().then(pos => {
        console.log(pos);
        if (pos.state == 'prompt') {
          this.Locataion();
          Swal.fire({
            title: 'APSWC need location acces permission for Capturing Attendance...Please click Allow to access!',
            showDenyButton: false,
            showCancelButton: false,
            confirmButtonText: `OK`,
            
          }).then((result) => {
            if (result.isConfirmed) {
              //Swal.fire('Saved!', '', 'success')
            } else if (result.isDenied) {
              //Swal.fire('Changes are not saved', '', 'info')
            }
          })

          return false;
        }
        if (pos.state == 'denied') {
          
          this.Locataion();
          
          Swal.fire({
            title: 'This site has been blocked from accessing your location...!',
            showDenyButton: false,
            showCancelButton: false,
            confirmButtonText: `OK`,
            
          }).then((result) => {
            if (result.isConfirmed) {
              //Swal.fire('Saved!', '', 'success')
            } else if (result.isDenied) {
              //Swal.fire('Changes are not saved', '', 'info')
            }
          })

          return false;
        }
        if (pos.state == 'granted') {
          //Swal.fire('warning', "granted", 'warning');
          return true;
        }
      });
    }
    else {
      Swal.fire('Geolocation is not supported for this Browser/OS.');
    }

  }


  LoadCaptcha(): void {
    this.captresponse = this.authService.Getcaptchload("Captcha");
    this.captresponse.subscribe((res) => {
      this.capres = res;
      this.imageBase64 = this.capres.imgurl;

    });
  }

  onSubmit() {
    let userlogin = this.insertForm.value;
    
    this.CheckLocation();
    
    const req = new InputRequest();
    if (!userlogin.Username) {
      Swal.fire('warning', "Please Enter Username", 'warning');
      return false;
    }

    if (!userlogin.Password) {
      Swal.fire('warning', "Please Enter Password", 'warning');
      return false;
    }

    if (!userlogin.recaptchaReactive) {
      Swal.fire('warning', "Please Enter Captcha", 'warning');
      return false;
    }
    req.INPUT_01 = userlogin.Username,
      req.INPUT_02 = userlogin.Password,
      req.INPUT_03 = this.capres.idval,
      req.INPUT_04 = userlogin.recaptchaReactive

    const returnUrl = this.route.snapshot.queryParamMap.get('returnUrl') || '/';
    this.authService.login("Login", req)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data.StatusCode == "100") {
            console.log(data.Details[0]);
            var userdata = data.Details[0];
            if (userdata.iS_PASSWORD_CHANGE == "0")
              this.router.navigate(['/ChangePassword']);
            else
            this.router.navigate(['/UserProfile']);
            // else if (userdata.useR_ROLE == "103")
            //   this.router.navigate(['/UserProfile']);
            // else if (userdata.useR_ROLE == "104" || userdata.useR_ROLE == "106")
            //   this.router.navigate(['/WarehouseLayout']);
            // else if (userdata.useR_ROLE == "105")
            //   this.router.navigate(['/Deadstockrgnapproval']);           
            // else if (userdata.useR_ROLE == "101" || userdata.useR_ROLE == "102") {
            //   if (userdata.useR_NAME == "APSWC_BL")
            //     this.router.navigate(['/Warehousemaster']);
            //   else
            //     this.router.navigate(['/EmployeeDetails']);
            // }

          }
          else {
            Swal.fire('warning', data.StatusMessage, 'warning');
            this.loading = false;
            this.insertForm.reset();
            this.insertForm.setErrors({
              invalidLogin: true
            });
            this.LoadCaptcha();

          }
        },
        (error) => {
          this.loading = false;
          this.insertForm.reset();
          this.insertForm.setErrors({
            invalidLogin: true
          });
          this.LoadCaptcha();
        });
  }

}

