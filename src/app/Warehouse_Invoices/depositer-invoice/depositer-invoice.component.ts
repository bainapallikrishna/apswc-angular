import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';
import {InvoicebuttonRendererComponent  } from 'src/app/custome-directives/Invoicebutton-renderer.component';


@Component({
  selector: 'app-depositer-invoice',
  templateUrl: './depositer-invoice.component.html',
  styleUrls: ['./depositer-invoice.component.css']
})
export class DepositerInvoiceComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  designationid=sessionStorage.getItem("logUserSectionID");


  NgbDateStruct:any;
  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  public components;
  public defaultColDef;

  public icons;
  StockDetails: any[];
  loader: boolean = true;
  DepositorForm: FormGroup;
  isSubmit: boolean = false;
  istable: boolean = false;
  isdiv: boolean = false;

  farmerlist: any[];
  cmdtylist: any[];
  mothlist: any[];
  yearlist: any[];
  WHTypelist:any[];
  whlist:any[];

  @ViewChild('agGrid') agGrid: AgGridAngular;

  columnDefs = [
    { headerName: '#', width: 60, cellRenderer: 'rowIdRenderer',floatingFilter: false },    
    { headerName: 'Depositor Id', maxWidth: 100, field: 'registereD_ID' },
    { headerName: 'Depositor Name', maxWidth: 150, field: 'registratioN_NAME'},
    { headerName: 'Booking Id', maxWidth: 150, field: 'bookinG_ID' },
    { headerName: 'Commodity Name', width: 150, field: 'commodity' },
    //{ headerName: 'Commodity Id', width: 150, field: 'commoditY_ID' },
    { headerName: 'Contract Type', width: 120, field: 'contracT_TYPE_NAME'},
    { headerName: 'Invoice Mode', width: 100, field: 'invoicE_MODE_NAME' },
  //   { headerName: 'Is Tax Applicable', width: 120, field: 'iS_TAX_APPLICABLE',
  //   cellRenderer: params => {
  //     return params.value ?  (params.value == "1" ? "Taxable" : "Non Taxable") : "";
  //   } 
  // }, 
  { headerName: 'Is Tax Applicable', width: 120, field: 'invoicE_NAME'}, 
  { headerName: 'Invoice Code', width: 120, field: 'invoicE_CODE'}, 
    { headerName: 'Action', width: 160, field: 'invoicE_STATUS', cellRenderer: 'buttonRenderer',floatingFilter: false, 
      cellStyle: { 'text-align': "left" },
      cellRendererParams: {
        nextClick: this.Downloadpdf.bind(this),

      }, 
  
  },

  ];



  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder, 


  ) {


    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }


    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
    this.frameworkComponents = {
      buttonRenderer: InvoicebuttonRendererComponent,
      
    }

    this.icons = {
      filter: ' '
    }
    
    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText:true,
     autoHeight:true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };
  }

  ngOnInit(): void {

    
    
    let now: Date = new Date();    
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }   
        
    this.DepositorForm = this.formBuilder.group({    
      WHtype:[null],
      Whname:[null],
      month: [null, Validators.required],
      year: [null, Validators.required],
      

    });
    
    if(this.logUserrole=='101' || this.logUserrole=="105")
    {
      this.isdiv=true;
      this.istable=false;
      this.LoadWhDetails('WHTYPE');
      
    }
    
    this.LoadMonths();
    this.LoadYears();
    

  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //this.LoadDetails();
    params.api.setRowData(this.StockDetails);

  }
  LoadWhDetails(val) {

    this.loader = true;
    const req = new InputRequest();
    req.INPUT_01 = val;
    req.INPUT_08 = this.qualty.WHtype.value;
    this.service.postData(req, "GetWHDetails").subscribe(data => {
      this.loader = false;
      if (data.StatusCode == "100") {
        if(val=="WHTYPE")
        this.WHTypelist = data.Details;
        else
        this.whlist = data.Details;
      }

    },
      error => console.log(error));
    

  }
  LoadWarehouses(val)
  {
    this.qualty.Whname.setValue(null);
    this.qualty.month.setValue(null);
    this.qualty.year.setValue(null);
    this.istable=false;
     this.LoadWhDetails(val);
  }

  LoadDetails() {
    
    this.loader = true;
    const req = new InputRequest();
    req.INPUT_02 = (this.logUserrole=="101" || this.logUserrole=="105")?this.qualty.Whname.value: this.workLocationCode;
    req.INPUT_03 = this.qualty.month.value;
    req.INPUT_04 = this.qualty.year.value;   

    this.service.postData(req, "GetInvoiceDetails").subscribe(data => {
      this.loader = false;
      this.istable = true;
      if (data.StatusCode == "100") {
       
        this.StockDetails = data.Details;
        this.gridApi.setRowData(this.StockDetails);        


      }
      else {
        //Swal.fire('warning', data.StatusMessage, 'warning');
        this.gridApi.setRowData(data.Details);

      }

    },
      error => console.log(error));
    
  }


  LoadMonths() {
    this.loader = true;
    const req = new InputRequest();
    req.INPUT_01 = 'Months';
    req.INPUT_02 = this.workLocationCode;

    this.service.postData(req, "GetMonthsAndYears").subscribe(data => {
      this.loader = false;
      if (data.StatusCode == "100") {
        this.mothlist = data.Details;       
        let month = ('0' + (this.NgbDateStruct.month)).slice(-2)
        this.mothlist = this.mothlist.filter(m => m.column2 <= month);
        
      }

    },
      error => console.log(error));
    

  }

  LoadYears() {

    this.loader = true;
    const req = new InputRequest();
    req.INPUT_01 = 'Years';
    req.INPUT_02 = this.workLocationCode;

    this.service.postData(req, "GetMonthsAndYears").subscribe(data => {
      this.loader = false;
      if (data.StatusCode == "100") {
        this.yearlist = data.Details;
      }

    },
      error => console.log(error));
     

  }


  get qualty() { return this.DepositorForm.controls; }


  GetDetails() {

    this.isSubmit = true; 
    this.StockDetails=[];

    if(this.logUserrole=="101" || this.logUserrole=="105")
    {
      if(!this.qualty.WHtype.value)
      {
        Swal.fire("warning","Select Warehouse Type","warning");
        return false;
      }
      
      if(!this.qualty.Whname.value)
      {
        Swal.fire("warning","Select Warehouse Name","warning");
        return false;
      } 
     


    }


    if (this.DepositorForm.invalid) {
      return false;
    }
    this.LoadDetails();

  }

  Downloadpdf(ivlist)
  {
    


    
    let now: Date = new Date();
    const dataiv = ivlist.rowData;

    // if(dataiv.invoicE_STATUS=="0")
    // {
    //   Swal.fire("warning","You Are Not Authorized To Generate Invoice","warning");
    //   return false;
    // }
    this.loader = true;
    const req = new InputRequest();
    req.DIRECTION_ID="18";
    //req.TYPEID="SPACE_RESERVATION";
     req.TYPEID="WAREHOUSE_INVOICES";
    req.INPUT_02=this.qualty.year.value;
    req.INPUT_03=this.qualty.month.value;
    req.INPUT_04=dataiv.registereD_ID;
    req.INPUT_05=dataiv.bookinG_ID;
    req.INPUT_06=dataiv.invoicE_MODE;
    req.INPUT_07=dataiv.iS_TAX_APPLICABLE;
    req.INPUT_08=this.logUserName;
    req.INPUT_09=dataiv.contracT_TYPE;
    req.INPUT_10=(this.logUserrole=="101"|| this.logUserrole=="105")?this.qualty.Whname.value: this.workLocationCode;
    req.INPUT_11=dataiv.invoicE_CODE;
    req.INPUT_12=dataiv.commoditY_ID;
    this.service.postData(req, "APSWCMapsServiceConsume").subscribe(data => {
     
      if (data.Status == "Success") {
        if(data.CertIssuerData.Base64pdf && data.CertIssuerData.Base64pdf!="101")
        {
        var b = this.service.s_sd(data.CertIssuerData.Base64pdf, 'application/pdf');
        var l = document.createElement('a');
        l.href = window.URL.createObjectURL(b);
        l.download = dataiv.registratioN_NAME+"_"+dataiv.contracT_TYPE_NAME+"_"+"Invoice"+".pdf";
        document.body.appendChild(l);
        l.click();
        document.body.removeChild(l);
        }
        if(data.CertIssuerData.Base64pdf2 && data.CertIssuerData.Base64pdf2!="101")
        {
          var b = this.service.s_sd(data.CertIssuerData.Base64pdf2, 'application/pdf');
          var l = document.createElement('a');
          l.href = window.URL.createObjectURL(b);
          l.download = dataiv.registratioN_NAME+"_"+dataiv.contracT_TYPE_NAME+"_"+"Invoice"+".pdf";
          document.body.appendChild(l);
          l.click();
          document.body.removeChild(l);
        }

        if(data.CertIssuerData.Base64pdf3 && data.CertIssuerData.Base64pdf3!="101")
        {
          var b = this.service.s_sd(data.CertIssuerData.Base64pdf3, 'application/pdf');
          var l = document.createElement('a');
          l.href = window.URL.createObjectURL(b);
          l.download = dataiv.registratioN_NAME+"_"+dataiv.contracT_TYPE_NAME+"_"+"Invoice"+".pdf";
          document.body.appendChild(l);
          l.click();
          document.body.removeChild(l);
        }
        if(data.CertIssuerData.Base64pdf4 && data.CertIssuerData.Base64pdf4!="101")
        {
          var b = this.service.s_sd(data.CertIssuerData.Base64pdf4, 'application/pdf');
          var l = document.createElement('a');
          l.href = window.URL.createObjectURL(b);
          l.download = dataiv.registratioN_NAME+"_"+dataiv.contracT_TYPE_NAME+"_"+"Invoice"+".pdf";
          document.body.appendChild(l);
          l.click();
          document.body.removeChild(l);
        }


       
       
        //console.log(data.Details);Base64pdf
        //this.fillPDFData(data.Details[0])
        this. GetDetails();
        this.loader = false;
      }
      else
      {
        Swal.fire('warning', data.Reason, 'warning');
        this.loader = false;
      }

    },
      error => console.log(error));
      

  }


}
