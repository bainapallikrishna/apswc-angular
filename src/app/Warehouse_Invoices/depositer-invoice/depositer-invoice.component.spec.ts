import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { DepositerInvoiceComponent } from './depositer-invoice.component';

describe('DepositerInvoiceComponent', () => {
  let component: DepositerInvoiceComponent;
  let fixture: ComponentFixture<DepositerInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepositerInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepositerInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
