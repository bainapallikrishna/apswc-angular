import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditDebitInvoiceComponent } from './credit-debit-invoice.component';

describe('CreditDebitInvoiceComponent', () => {
  let component: CreditDebitInvoiceComponent;
  let fixture: ComponentFixture<CreditDebitInvoiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditDebitInvoiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditDebitInvoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
