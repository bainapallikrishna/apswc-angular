import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MustMatch } from 'src/app/custome-directives/must-match.validator';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  logUserrole:string = sessionStorage.getItem("logUserrole");
  logUsercode:string = sessionStorage.getItem("logUserCode");
  issubmitted:boolean = false;
  showloader:boolean = true;
  ChangePasswordForm: FormGroup;

  constructor(private service: CommonServices,private router: Router,private formBuilder: FormBuilder) {
    if(!this.logUserrole){
      this.router.navigate(['/Login']);
      return;
    }
   }

  ngOnInit(): void {
    this.ChangePasswordForm = this.formBuilder.group({
      EMPCODE: [''],
      EmpPassword: ['', Validators.required],
      EmpNewPassword: ['', Validators.required,Validators.minLength(4),Validators.maxLength(20)],
      EmpConPassword: ['', Validators.required,Validators.minLength(4),Validators.maxLength(20)],
    }, {
      validator: MustMatch('EmpNewPassword', 'EmpConPassword')
    }
    );

    this.showloader = false;
  }

  get pass() { return this.ChangePasswordForm.controls; }

  SaveChangePassword(){

    this.issubmitted = true;

    // stop here if form is invalid
    if (this.ChangePasswordForm.invalid) {
      return false;
    }

    if(this.pass.EmpNewPassword.value != this.pass.EmpConPassword.value){
      Swal.fire('warning', "New Password & Confirm Password Should be Equal", 'warning');
      return false;
    }

    if(this.pass.EmpPassword.value == this.pass.EmpNewPassword.value){
      Swal.fire('warning', "Password & New Password Should not be Equal", 'warning');
      return false;
    }
    
    const req = new InputRequest();
    req.INPUT_01 = this.logUsercode;
    req.INPUT_02 = this.pass.EmpPassword.value;
    req.INPUT_03 = this.pass.EmpNewPassword.value;

    // var data = { 'INPUT_01': this.f.EmpAadhaar.value };
    this.service.postData(req, "SaveChangePassword").subscribe(data => {
      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          sessionStorage.setItem("logUserisChangePassword","1");
          Swal.fire('success', "Password Updated Successfully !!!", 'success');
          if(this.logUserrole == "101" || this.logUserrole == "102")
          this.router.navigate(['/EmployeeDetails']);
          else if(this.logUserrole == "103")
          this.router.navigate(['/UserProfile']);
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

}
