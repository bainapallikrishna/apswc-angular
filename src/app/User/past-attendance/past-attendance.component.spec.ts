import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastAttendanceComponent } from './past-attendance.component';

describe('PastAttendanceComponent', () => {
  let component: PastAttendanceComponent;
  let fixture: ComponentFixture<PastAttendanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastAttendanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastAttendanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
