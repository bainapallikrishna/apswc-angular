import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { CommonServices } from 'src/app/Services/common.services';
import { gridbuttonrenderer } from 'src/app/custome-directives/gridbutton-renderer.component';
import { AgGridAngular } from 'ag-grid-angular';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { Router } from '@angular/router';



@Component({
  selector: 'app-past-attendance',
  templateUrl: './past-attendance.component.html',
  styleUrls: ['./past-attendance.component.css']
})
export class PastAttendanceComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('historyempModal') historyempModal: ModalDirective;
  @ViewChild('LeaveDetailsempModal') LeaveDetailsempModal: ModalDirective;



  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");


  columnDefs = [
    { headerName: '#', maxWidth: 50, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Application ID', maxWidth: 180, field: 'applicatioN_ID', sortable: true, filter: true },
    { headerName: 'Name', maxWidth: 120, field: 'emP_NAME', sortable: true, filter: true },
    { headerName: 'From Date', maxWidth: 120, field: 'froM_DATE', sortable: true, filter: true },
    { headerName: 'To Date', maxWidth: 120, field: 'tO_DATE', sortable: true, filter: true },
    { headerName: 'In', maxWidth: 100, field: 'checK_IN_TIME', sortable: true, filter: true },
    { headerName: 'Out', maxWidth: 100, field: 'checK_OUT_TIME', sortable: true, filter: true },
    { headerName: 'Reason', maxWidth: 140, field: 'forgeT_REASON', sortable: true, filter: true },
    { headerName: 'Status', maxWidth: 120, field: 'status', sortable: true, filter: true },
    {
      headerName: 'Action', maxWidth: 400, cellRenderer: 'buttonRenderer',
      cellRendererParams: {

        btnEdit: 'Cancel',
        btnView: 'View',
        btnHistory: 'History',
        editClick: this.EditEmpLeaveMaster.bind(this),
        viewClick: this.ViewEmpLeave.bind(this),
        historyClick: this.HistoryLeave.bind(this),

      },
    }
  ];


  Application_ID: string;
  LeaveType_View: string;
  FromDate_VIEW: string;
  ToDate_VIEW: string;
  NoOfDays_VIEW: string;
  ReportingMgr_VIEW: string;
  ContactNo_VIEW: string;
  status_VIEW: string;
  Reason_VIEW: string;
  EmpName_VIEW: string;
  In_VIEW: string;
  Out_VIEW: string;

  Designation_VIEW: string;
  LeaveDuration_VIEW: string;
  Approved_by: string;
  Designation: string;
  ApprovedDate_VIEW: string;
  currentdate = new Date();

  frameworkComponents: any;
  public components;
  showloader: boolean = true;


  consubmitted: boolean = true;

  leavetypes: any = [];
  NgbDateStruct: any;


  MasterHistlist: any[];

  EMP_CODE: string;
  EMP_NAME: string;
  REPORTING_MGR: string;
  EMR_NUM: string;

  GridFill: any[];
  GridFillRowData: any[];

  gridApi: any = [];
  defaultColDef: any = [];
  gridColumnApi: any = [];

  currentyear: number;
  EmpLeaveDetails: any = [];


  employeeAttendanceForm: FormGroup;
  showreasonerror: boolean = false;

  get EmpAttendanceCtrl() { return this.employeeAttendanceForm.controls; }

  constructor(private formBuilder: FormBuilder, private service: CommonServices, private datef: CustomDateParserFormatter,
    private datecon: NgbDatepickerConfig, private router: Router,) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.frameworkComponents = {
      buttonRenderer: gridbuttonrenderer,

    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };



  }


  PreviousMonthTo: any;
  PreviousMonthFrom: any;


  ngOnInit(): void {

    this.showloader = false;

    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }

    this.PreviousMonthTo = { year: now.getFullYear(), month: now.getMonth(), day: new Date(now.getFullYear(), now.getMonth(), 0).getDate() }
    this.PreviousMonthFrom = { year: now.getFullYear(), month: now.getMonth(), day: 1 }

    this.currentyear = now.getFullYear();


    this.employeeAttendanceForm = this.formBuilder.group({
      FromDate: ['', Validators.required],
      ToDate: ['', Validators.required],
      InHours: ['', Validators.required],
      InMin: ['', Validators.required],
      InAM: ['AM'],

      OutHours: ['', Validators.required],
      OutMinu: ['', Validators.required],
      OutPM: ['PM'],

      Reason: ['', Validators.required],
      ReasonOther: [''],
      RptManager: ['', Validators.required],
      RptManagerID: ['']

    });


    this.fillGriddata();
    this.GetEmployeeDetails();

  }


  GetEmployeeDetails() {


    this.EmpAttendanceCtrl.RptManagerID.setValue("APSWC0118");
    this.EmpAttendanceCtrl.RptManager.setValue("Ramadevi");


    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    this.service.postData(req, "GetEmployeeDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        const empdata = data.Details[0];
        if (empdata) {
          this.EmpAttendanceCtrl.RptManager.setValue(data.Details[0].reportinG_OFFICER_NAME);
          this.EmpAttendanceCtrl.RptManagerID.setValue(data.Details[0].reportinG_OFFICER);
        }
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }



  SubmitEmpLeave() {
    this.showreasonerror = false;

    this.consubmitted = true;

    if (this.employeeAttendanceForm.invalid) {
      return false;
    }


    if (this.EmpAttendanceCtrl.ReasonOther.value.toString().trim() == "" && this.EmpAttendanceCtrl.Reason.value == "Others") {
      this.showreasonerror = true;
      return false;
    }
    if (this.datef.HolidayGreaterDate(this.EmpAttendanceCtrl.FromDate.value, this.EmpAttendanceCtrl.ToDate.value)) {
      Swal.fire('warning', "Attendance To Date Shoud be greater than From Date", 'warning');
      return false;
    }



    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = this.datef.format(this.EmpAttendanceCtrl.FromDate.value);
    req.INPUT_03 = this.datef.format(this.EmpAttendanceCtrl.ToDate.value);

    req.INPUT_04 = this.EmpAttendanceCtrl.InHours.value + ":" + this.EmpAttendanceCtrl.InMin.value + " " + this.EmpAttendanceCtrl.InAM.value;
    req.INPUT_05 = this.EmpAttendanceCtrl.OutHours.value + ":" + this.EmpAttendanceCtrl.OutMinu.value + " " + this.EmpAttendanceCtrl.OutPM.value;


    req.INPUT_06 = "Pending";

    req.INPUT_07 = this.EmpAttendanceCtrl.Reason.value == "Others" ? this.EmpAttendanceCtrl.ReasonOther.value : this.EmpAttendanceCtrl.Reason.value;
    req.INPUT_08 = this.EmpAttendanceCtrl.RptManagerID.value;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;


    this.showloader = true;

    this.service.postData(req, "PastAttendance_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          this.fillGriddata();

          this.editModal.hide();
          Swal.fire('success', "Employee Past Attendance Saved Successfully !!!", 'success');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }


  resetallforms() {


    this.EmpAttendanceCtrl.FromDate.setValue("");
    this.EmpAttendanceCtrl.ToDate.setValue("");
    this.EmpAttendanceCtrl.InHours.setValue("");
    this.EmpAttendanceCtrl.InMin.setValue("");
    this.EmpAttendanceCtrl.OutHours.setValue("");
    this.EmpAttendanceCtrl.OutMinu.setValue("");
    this.EmpAttendanceCtrl.Reason.setValue("");
    this.EmpAttendanceCtrl.ReasonOther.setValue("");

    this.consubmitted = false;
    this.showreasonerror = false;


  }

  CloseEditModal() { this.editModal.hide(); }


  EditEmpLeaveMaster(row: any) {
    const rowdata = row.rowData;

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to cancel the Application " + rowdata.applicatioN_ID + "!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {
        const req = new InputRequest();
        req.INPUT_01 = rowdata.applicatioN_ID;
        //req.INPUT_01 = "2021";
        req.INPUT_02 = "Cancelled";
        req.CALL_SOURCE = "WEB";
        req.USER_NAME = this.logUserName;

        this.showloader = true;

        this.service.postData(req, "PastAttendance_Update").subscribe(data => {
          this.showloader = false;

          if (data.StatusCode == "100") {
            let result = data.Details[0];
            if (result.rtN_ID === 1) {

              this.fillGriddata();
              this.editModal.hide();
              Swal.fire('success', "Your application has been cancelled successfully !!!", 'success');
            }
            else {
              Swal.fire('warning', result.statuS_TEXT, 'warning');
            }

          }
          else
            Swal.fire('warning', data.StatusMessage, 'warning');

        },
          error => console.log(error));



      }
    })
  }
  ViewEmpLeave(row: any) {

    this.showloader = true;
    const datarow = row.rowData;
    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = datarow.applicatioN_ID;
    req.INPUT_03 = "0";
    this.showloader = true;
    //408     
    this.service.postData(req, "PastAttendance_Get").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {



        //this.ReportingMgr_VIEW = data.Details[0].reportinG_OFFICER;

        this.Application_ID = data.Details[0].applicatioN_ID;
        this.EmpName_VIEW = data.Details[0].emP_NAME;
        this.Designation = data.Details[0].designation;
        this.FromDate_VIEW = data.Details[0].froM_DATE.replace("T00:00:00", "");
        this.ToDate_VIEW = data.Details[0].tO_DATE.replace("T00:00:00", "");
        this.Out_VIEW = data.Details[0].checK_OUT_TIME;
        this.In_VIEW = data.Details[0].checK_IN_TIME;
        this.Reason_VIEW = data.Details[0].forgeT_REASON;
        this.Approved_by = data.Details[0].approveD_BY;
        this.ApprovedDate_VIEW = data.Details[0].approveD_DATE.replace("T00:00:00", "");


        this.viewModal.show();

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));


  }
  HistoryLeave(row: any) { this.Historyrow(row); }
  HistoryAll() { this.history_All(); }
  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.GridFillRowData);

  }

  AddEmpLeave() {

    //this.EditEmpLeaveMaster.res();

    this.resetallforms();
    this.editModal.show();
  }

  fillGriddata() {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = "0";
    req.INPUT_03 = "0";

    this.showloader = true;
    this.service.postData(req, "PastAttendance_Get").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.GridFill = data.Details;
        this.GridFillRowData = data.Details;
        this.gridApi.setRowData(this.GridFillRowData);


      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));


  }







  history_All() {
    const req = new InputRequest();

    req.INPUT_02 = "FORGET_ATTENDANCE";
    req.INPUT_01 = this.logUserName;

    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }
  Historyrow(row): void {
    this.showloader = true;
    const data = row.rowData;
    const req = new InputRequest();

    req.INPUT_02 = "FORGET_ATTENDANCE";
    req.INPUT_01 = this.logUserName;
    req.INPUT_03 = data.applicatioN_ID;


    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }










}
