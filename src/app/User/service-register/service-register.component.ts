import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';

import { MustMatch } from 'src/app/custome-directives/must-match.validator';
import Swal from 'sweetalert2';

import { AadharValidateService } from 'src/app/Services/aadhar-validate.service';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { AgGridAngular } from 'ag-grid-angular';
import { srempdetailsRendererComponent } from 'src/app/custome-directives/srempdetails-renderer.component';
import { HttpEventType } from '@angular/common/http';
import { NullTemplateVisitor } from '@angular/compiler';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'
import { EmployeeAnnualPropertie } from 'src/app/Interfaces/user';


@Component({
  selector: 'app-service-register',
  templateUrl: './service-register.component.html',
  styleUrls: ['./service-register.component.css']
})

export class ServiceRegisterComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('historySection') public historySection: ModalDirective;
  @ViewChild('historyAllSection') public historyAllSection: ModalDirective;
  @ViewChild('AddModify') public AddModify: ModalDirective;
  @ViewChild('Employeeprofile') public Employeeprofilemodal: ModalDirective;

  @ViewChild('agGrid') agGrid: AgGridAngular;

  showloader: boolean = true;
  ismodal: boolean = false;
  modeltitle: string = "Add";
  locationtypeid: string = "";
  empkeyword: string = "emP_NAME";
  EmployeeAnnualPropertie: EmployeeAnnualPropertie;

  columnDefs = [
    { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer', },
    { //headerName: 'Employee Name', maxWidth: 200, field: 'emP_NAME', sortable: true, filter: true 

      headerName: 'Employee Name', maxWidth: 200, field: 'emP_NAME',
      cellRenderer: "EmpdataGet",
      sortable: true, filter: true
    },
    { headerName: 'Login Code', maxWidth: 150, field: 'emP_CODE', sortable: true, filter: true },
    { headerName: 'Employee Type', maxWidth: 150, field: 'servicE_TYPE', sortable: true, filter: true },
    { headerName: 'Designation', maxWidth: 200, field: 'designation', sortable: true, filter: true },
    { headerName: 'Mobile No', maxWidth: 120, field: 'mobilE_NUMBER', sortable: true, filter: true },
    {
      headerName: 'Action', maxWidth: 400, cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        //btnEdit: 'SR',
        //btnView: 'View',
        //btnHistory: 'History',
        //viewClick: this.EditEmployee.bind(this),
        viewClick: this.ViewEmployee.bind(this),
        //historyClick: this.HistoryEmployee.bind(this),

      },
    }
  ];

  EmployeerowData = [];

  employee: any;
  employees: any[];
  uidstatus: boolean = true;
  uid_isvalid: boolean = true;
  Emp_mob_isvalid = true;
  Emp_Emergencymob_isvalid = true;
  PanPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  IFSCPattern = "^[A-Z]{4}0[A-Z0-9]{6}$";


  NgbDateStruct: any;
  public isEdit: boolean = false;
  public isAdmin: boolean = false;
  public consubmitted: boolean = false;
  public commsubmi: boolean = false;
  public worksubmi: boolean = false;
  public banksubmi: boolean = false;
  public familysubmi: boolean = false;
  public pfsubmi: boolean = false;

  public contactcomple: boolean = false;
  public commcomple: boolean = false;
  public workcomple: boolean = false;
  public bankcomple: boolean = false;
  public familycomple: boolean = false;
  public pfcomple: boolean = false;

  public hideperaadr: boolean = false;


  public screen1: boolean = true;

  public screen2: boolean = false;
  public backscreen: boolean = false;


  public EmpName: string = "";
  public empid: string = "";
  public service_name: string = "";


  public eventname: string = "";
  employeeContactForm: FormGroup;
  employeeCommuForm: FormGroup;
  employeeWorkForm: FormGroup;
  employeeBankForm: FormGroup;
  employeeFamilyForm: FormGroup;
  employeePFForm: FormGroup;

  employeeslist: any = [];
  emptypes: any = [];
  genders: any = [];
  sections: any = [];
  educations: any = [];
  bloodgroups: any = [];
  expyears: any = [];
  expmonths: any = [];
  nationalites: any = [];
  religions: any = [];
  communities: any = [];
  mstatus: any = [];
  desinations: any = [];

  districts: any = [];
  urlist: any = [];
  pmandals: any = [];
  pvillages: any = [];

  pemandals: any = [];
  pevillages: any = [];

  states: any = [];
  ndistricts: any = [];
  nmandals: any = [];
  nvillages: any = [];

  worklocations: any = [];
  locations: any = [];
  rmanagers: any = [];
  relations: any = [];
  rowData: any = [];
  defaultColDef: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];

  vaadhaar: string;
  vempcode: string;
  vempid: string;
  vemptype: string;
  vempname: string;
  vdesignation: string;
  vgender: string;
  vdob: string;
  vdoj: string;
  veducation: string;
  vbloodgrp: string;
  vemail: string;
  vmobile: string;
  vecno: string;
  vexpears: string;
  vexmonths: string;
  vnationality: string;
  vreligion: string;
  vcommunity: string;
  vmstatus: string;
  vspouname: string;
  vanaadate: string;
  vempphoto: string;
  vaadhaarphoto: string;

  vpdistrict: string;
  vpru: string;
  vpmandal: string;
  vpvillage: string;
  vphno: string;
  vpstreett: string;
  vplanmark: string;
  vppincode: string;

  vpedistrict: string;
  vperu: string;
  vpemandal: string;
  vpevillage: string;
  vpehno: string;
  vpestreett: string;
  vpelanmark: string;
  vpepincode: string;

  vnstate: string;
  vndistrict: string;
  vnru: string;
  vnmandal: string;
  vnvillage: string;
  vnhno: string;
  vnstreett: string;
  vnlanmark: string;
  vnpincode: string;

  vloctype: string;
  vlocname: string;
  vsectionname: string;
  vrmanager: string;
  vastatus: string;

  vifsccode: string;
  vaccname: string;
  vaccno: string;
  vbankname: string;
  vbranch: string;

  V_identification1: string;
  V_identification2: string;
  V_Mothertongue: string;
  V_Height: string;
  V_Nativeplace: string;

  familylist: any = [];

  vuanno: string;
  vpfno: string;
  vpfdate: string;
  vpfoffice: string;
  vpfaccno: string;
  vpanno: string;

  vid1: string;
  vid2: string;
  vheight: string;
  vmothertounge: string;
  vnativeplace: string;



  frameworkComponents: any;
  public components;

  allemphistory: any = [];
  emphistory: any = [];
  progress: number = 0;
  message: string = "";

  FamilyList = [];
  FamilyList_keys = [];

  EmpTransferList = [];
  EmpTransferList_keys = [];

  LeavesList = [];
  Leaves_List_keys = [];


  SuspentionList = [];
  SuspentionList_keys = [];

  IncrementList = [];
  IncrementList_keys = [];

  PunishmentList = [];
  PunishmentList_keys = [];

  ReinstatteList = [];
  ReinstatteList_keys = [];

  PromotionList = [];
  PromotionList_keys = [];

  AllocationList = [];
  AllocationList_keys = [];

  ReleivingList = [];
  ReleivingList_keys = [];


  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private uidservice: AadharValidateService,
    private datef: CustomDateParserFormatter,
    private datecon: NgbDatepickerConfig, private sanitizer: DomSanitizer
  ) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }

    if (this.logUserrole == "101" || this.logUserrole == "102")
      this.isAdmin = true;
    this.frameworkComponents = {
      buttonRenderer: srempdetailsRendererComponent

    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },

      EmpdataGet: function (params) {

        return '<u style="color:blue">' + params.value + '</u>';

      },
    };


  }

  propertielist = [];
  propertielist_decdoc = [];

  decryptFile_document(filepath, Orderid): string {
    if (filepath) {

      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          console.log(data.docBase64);

          let objIndex = this.propertielist_decdoc.findIndex((obj => obj.Orderid == Orderid));


          const blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
          const blobUrl = URL.createObjectURL(blob);

          this.propertielist_decdoc[objIndex].document = (this.sanitizer.bypassSecurityTrustResourceUrl(blobUrl));


          return "";

        },
          error => {
            console.log(error);
            //this.vempphoto = "";
          });
    }
    else { return ""; }

  }


  MStatusChange(event): void {
    this.contact.EmpSpouseName.setValue("");
    this.contact.EmpADate.setValue(this.datef.format(null));
  }


  //this.datecon.maxDate = new NgbDateStruct({year:2021,month:4:day:2}) 

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //params.api.setRowData(this.EmployeerowData);
    //this.gridApi.sizeColumnsToFit();
    this.LoadEmployeeList()
  }

  uidvalidate(EmpAadhaar) {
    if (EmpAadhaar.length == 12) {
      this.mob_uid_validate(EmpAadhaar);
      this.uidstatus = this.uidservice.validate(this.employeeContactForm.controls.EmpAadhaar.value);
      // if(this.uidstatus==false)      
      // alert("Invalid Aadhar Number");    
    }
  }

  IsEmployeeExist(EmpAadhaar) {
    if (EmpAadhaar.length == 12) {
      //alert(EmpAadhaar);
    }
  }

  mob_uid_validate(mobilephone) {
    if (mobilephone.length === 10)
      this.Emp_mob_isvalid = this.uidservice.validatemob(mobilephone);
    else if (mobilephone.length === 12)
      this.uid_isvalid = this.uidservice.validatemob(this.employeeContactForm.controls.EmpAadhaar.value);
  }
  mob_emr_validate(mobilephone) {
    if (mobilephone.length === 10)
      this.Emp_Emergencymob_isvalid = this.uidservice.validatemob(mobilephone);
  }

  EmpNameSelect(event): void {
    this.contact.EmpName.setValue("");
  }

  DetailsGet(value) {

    //   this.SR_EVENTS = false;
    //   this.SR_BASIC = false;
    //   this.eventname = "";

    //   if (value == "1") {
    //     this.SR_BASIC = true;
    //     this.eventname = "Employee Details Section";

    //   }

  }

  back() {
    this.screen2 = false;
    this.screen1 = true;

  }

  home() {
    //   this.SR_EVENTS = false;
    //   this.SR_BASIC = false;

  }

  ngOnInit(): void {


    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }


    //this.LoadEmployeeList();

    this.LoadDistricts(); // load districts
    this.LoadurFlags(); // load areas
    this.LoadEmployeeTypes(); // load employee types
    this.LoadGenders();
    this.LoadSections();
    this.LoadDesignations();
    this.LoadEducations();
    this.LoadBloodGroups();
    this.LoadEmpExpYears();
    this.LoadEmpExpMonths();
    this.LoadNationality();
    this.LoadReligions();
    this.LoadCommunities();
    this.LoadMaritalStatus();
    this.LoadStates();
    this.LoadLocationTypes();
    this.LoadRManagers();
    this.LoadRelations();


    this.employeeContactForm = this.formBuilder.group({
      EMPCODE: [''],
      EmpAadhaar: ['', Validators.required],
      EmpId: [''],
      EmpName: ['', Validators.required],
      EmpType: [null, Validators.required],
      EmpDesign: [null, Validators.required],
      EmpGender: [null, Validators.required],
      EmpDOB: ['', Validators.required],
      EmpDOJ: ['', Validators.required],
      EmpEdu: [null, Validators.required],
      BloodGroup: [null, Validators.required],
      EmpExpYears: [null],
      EmpExpMonths: [null],
      EmpNationality: ['', Validators.required],
      EmpReligion: [null],
      EmpCommunity: [null],
      EmpMStatus: [null, Validators.required],
      EmpSpouseName: [''],
      EmpADate: [null],
      Base64Image: [''],
      AadhaarBase64Image: [''],
      EmpEmail: [''],
      EmpMobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      EmpCMobile: [''],
      EmpAadhaarDecla: [''],
      ImagePath: [''],
      AadhaarImagePath: [''],
      EmpIdentification1: ['', Validators.required],
      EmpIdentification2: ['', Validators.required],
      Height: ['', Validators.required],
      MotherTongue: ['', Validators.required],
      EmpNativeplace: ['', Validators.required],

    });

    this.employeeCommuForm = this.formBuilder.group({
      PDistrict: [null, Validators.required],
      PArea: [null, Validators.required],
      PMandal: [null, Validators.required],
      PVillage: [null, Validators.required],
      PHNO: ['', Validators.required],
      PStrName: ['', Validators.required],
      PLandmark: [''],
      PPincode: ['', [Validators.required, Validators.minLength(6)]],

      PasPerAddr: [false],
      nativesameas: [''],
      PerAddrnotAvailable: [false],

      PEDistrict: [null, Validators.required],
      PEArea: [null, Validators.required],
      PEMandal: [null, Validators.required],
      PEVillage: [null, Validators.required],
      PEHNO: ['', Validators.required],
      PEStrName: ['', Validators.required],
      PELandmark: [''],
      PEPincode: ['', [Validators.required, Validators.minLength(6)]],


      NState: [null, Validators.required],
      NDistrict: [null, Validators.required],
      NArea: [null, Validators.required],
      NMandal: [null, Validators.required],
      NVillage: [null, Validators.required],
      NHNO: ['', Validators.required],
      NStrName: ['', Validators.required],
      NLandmark: [''],
      NPincode: ['', [Validators.required, Validators.minLength(6)]],

    });

    this.employeeWorkForm = this.formBuilder.group({
      WorkLocType: [null, Validators.required],
      LocationName: [null, Validators.required],
      SectionName: [null],
      ReportManager: [null, Validators.required],
      IsEmpActive: ['1'],
      EmpRemarks: ['']
    });

    this.employeeBankForm = this.formBuilder.group({
      AccLength: [],
      AccountHName: ['', Validators.required],
      AccountNumber: ['', Validators.required],
      ConfirmAccountNumber: ['', Validators.required],
      IFSCCode: ['', [Validators.required, Validators.minLength(11), Validators.pattern(this.IFSCPattern)]],
      BankName: ['', Validators.required],
      BranchName: ['', Validators.required],

    }
      , {
        validator: MustMatch('AccountNumber', 'ConfirmAccountNumber')
      }
    );

    this.employeeFamilyForm = this.formBuilder.group({
      //FamilyCount:['0'],
      familymembers: new FormArray([
        this.formBuilder.group({
          fname: ['', Validators.required],
          fdob: ['', Validators.required],
          frelation: [null, Validators.required],
          isnominee: [''],
        })
      ])
    });

    this.employeePFForm = this.formBuilder.group({
      UANNo: [''],
      PFNo: [''],
      PFData: [''],
      PFOffice: [''],
      PFAccNo: [''],
      PANNo: ['', [Validators.required, Validators.minLength(10),
      Validators.pattern(this.PanPattern)]]
    });
    //this.showloader = false;
  }

  get contact() { return this.employeeContactForm.controls; }
  get commu() { return this.employeeCommuForm.controls; }
  get work() { return this.employeeWorkForm.controls; }
  get bank() { return this.employeeBankForm.controls; }
  get fam() { return this.employeeFamilyForm.controls; }
  get pf() { return this.employeePFForm.controls; }

  get f() { return this.fam.familymembers as FormArray; }

  CloseEditModal() {
    this.editModal.hide();
    this.LoadEmployeeList();
  }

  CloseViewModal() {
    this.viewModal.hide();
    this.LoadEmployeeList();
  }

  AddEmployee() {
    this.resetallforms();

    this.uid_isvalid = true;
    this.uidstatus = true;

    this.modeltitle = "Add";
    this.showloader = true;
    this.isEdit = false;
    this.contact.EMPCODE.setValue("");
    this.work.IsEmpActive.setValue("1");

    this.f.push(this.formBuilder.group({
      fname: ['', Validators.required],
      fdob: ['', Validators.required],
      frelation: [null, Validators.required],
      isnominee: [''],
    }));

    this.showloader = false;
    this.editModal.show();
  }
  dataclicked(params) {
    if (params.colDef.cellRenderer == "EmpdataGet") {
      this.EditEmployee(params);
    }

  }
  EditEmployee(row) {
    const data = row.data;
    this.resetallforms();
    this.modeltitle = "Edit";
    this.showloader = true;
    this.isEdit = true;
    this.uid_isvalid = true;
    this.uidstatus = true;
    this.contact.EMPCODE.setValue(data.emP_CODE);
    this.contact.EmpAadhaar.setValue(data.emP_UID);
    this.contact.EmpName.setValue(data.emP_NAME);
    this.GetEmployeeDetails(data.emP_CODE, 'edit');
    this.GetEmpFamilyDetails(data.emP_CODE, 'edit');

    this.showloader = false;
    //this.editModal.show();
    //this.AddModify.show();

    this.screen1 = false;
    this.screen2 = true;


  }

  ViewEmployee(row) {
    const data = row.rowData;
    //this.GetEmployeeDetails(data.emP_CODE, 'view');
    //this.GetEmpFamilyDetails(data.emP_CODE, 'view');
    this.GetEmpDetails_View(data);
    this.Employeeprofilemodal.show();
  }

  GetEmpDetails_View(rowdata) {

    const req = new InputRequest();

    req.DIRECTION_ID = "14";
    req.TYPEID = "ALL_SERVICE_REGISTER";

    req.INPUT_01 = rowdata.emP_CODE;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUsercode;

    this.showloader = true;
    this.service.postData(req, "SERVICE_PROFILE").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.IncrementList = data.Details['table7'];
        if (this.IncrementList.length > 0) {
          this.IncrementList_keys = Object.keys(this.IncrementList[0]);
        }


        this.EmpTransferList = data.Details['table1'];
        if (this.EmpTransferList.length > 0) {
          this.EmpTransferList_keys = Object.keys(this.EmpTransferList[0]);
        }

        this.SuspentionList = data.Details['table2'];
        if (this.SuspentionList.length > 0) {
          this.SuspentionList_keys = Object.keys(this.SuspentionList[0]);
        }


        this.PunishmentList = data.Details['table3'];
        if (this.PunishmentList.length > 0) {
          this.PunishmentList_keys = Object.keys(this.PunishmentList[0]);
        }


        this.ReinstatteList = data.Details['table4'];
        if (this.ReinstatteList.length > 0) {
          this.ReinstatteList_keys = Object.keys(this.ReinstatteList[0]);
        }


        this.PromotionList = data.Details['table5'];
        if (this.PromotionList.length > 0) {
          this.PromotionList_keys = Object.keys(this.PromotionList[0]);
        }

        this.AllocationList = data.Details['table6'];
        if (this.AllocationList.length > 0) {
          this.AllocationList_keys = Object.keys(this.AllocationList[0]);
        }

        //Empdetails table7

        this.familylist = data.Details['table8'];
        if (this.familylist.length > 0) {
          this.FamilyList_keys = Object.keys(this.familylist[0]);
        }


        this.ReleivingList = data.Details['table9'];
        if (this.ReleivingList.length > 0) {
          this.ReleivingList_keys = Object.keys(this.ReleivingList[0]);
        }


        this.LeavesList = data.Details['table10'];
        if (this.LeavesList.length > 0) {
          this.Leaves_List_keys = Object.keys(this.LeavesList[0]);
        }

        this.propertielist = data.Details['table11'];


        this.propertielist_decdoc = [];
        this.propertielist.forEach((val, i) => {
          this.EmployeeAnnualPropertie = {
            Orderid: val.procedinG_NO,
            Orderdate: val.procedinG_DATE,
            year: val.financiaL_YEAR,
            docnumber: val.documenT_NO,
            document: this.decryptFile_document(val.document, val.procedinG_NO),
          };

          this.propertielist_decdoc.push(this.EmployeeAnnualPropertie);


        })


        var empdata = data.Details['table'][0];

        this.vaadhaar = empdata.uiD_MASK;
        this.vempcode = empdata.emP_CODE;
        this.vempid = empdata.emP_ID;

        this.vemptype = empdata.categorY_NAME;
        this.vempname = empdata.emP_NAME;
        this.vdesignation = empdata.designation;
        this.vgender = empdata.gendeR_NAME;
        this.vdob = (empdata.dob ? empdata.dob : "").replace("T00:00:00", "");
        this.vdoj = (empdata.joininG_DATE ? empdata.joininG_DATE : "").replace("T00:00:00", "");
        this.veducation = empdata.educatioN_QUALIFICATION;
        this.vbloodgrp = empdata.blooD_GROUP1;
        this.vemail = empdata.emaiL_ID;
        this.vmobile = empdata.mobilE_NUMBER;
        this.vecno = empdata.emergencY_CONTACT_NO;
        this.vexpears = empdata.experiancE_YEARS;
        this.vexmonths = empdata.experiancE_MONTH;
        this.vnationality = empdata.nationality;
        this.vreligion = empdata.religion;
        this.vcommunity = empdata.communitY_NAME;
        this.vmstatus = empdata.martiaL_STATUS;
        this.vspouname = empdata.spousE_NAME;
        this.vanaadate = (empdata.anniversarY_DATE ? empdata.anniversarY_DATE : "").replace("T00:00:00", "");
        this.decryptFile(empdata.emP_PHOTO);
        this.vaadhaarphoto = empdata.emP_PHOTO;

        this.vpdistrict = empdata.preS_DISTRICT_NAME;
        this.vpru = empdata.preS_RURAL_URBAN_NAME;
        this.vpmandal = empdata.preS_MMC_NAME;
        this.vpvillage = empdata.preS_WARD_VILLAGE_NAME;
        this.vphno = empdata.preS_HOUSE_NO;
        this.vpstreett = empdata.preS_STREET_NAME;
        this.vplanmark = empdata.preS_LAND_MARK;
        this.vppincode = empdata.preS_PINCODE;

        this.vpedistrict = empdata.perM_DISTRICT_NAME;
        this.vperu = empdata.perM_RURAL_URBAN_NAME;
        this.vpemandal = empdata.perM_MMC_NAME;
        this.vpevillage = empdata.perM_WARD_VILLAGE_NAME;
        this.vpehno = empdata.perM_HOUSE_NO;
        this.vpestreett = empdata.perM_STREET_NAME;
        this.vpelanmark = empdata.perM_LAND_MARK;
        this.vpepincode = empdata.perM_PINCODE;

        this.vnstate = empdata.nativE_STATE_NAME;
        this.vndistrict = empdata.nativE_DISTRICT_NAME;
        this.vnru = empdata.nativE_RURAL_URBAN_NAME;
        this.vnmandal = empdata.nativE_MMC_NAME;
        this.vnvillage = empdata.nativE_WARD_VILLAGE_NAME;
        this.vnhno = empdata.nativE_HOUSE_NO;
        this.vnstreett = empdata.nativE_STREET_NAME;
        this.vnlanmark = empdata.nativE_LAND_MARK;
        this.vnpincode = empdata.nativE_PINCODE;

        this.vloctype = empdata.worK_LOCATION_TYPE;
        this.vlocname = empdata.worK_LOCATION;
        this.vsectionname = empdata.sectioN_NAME;
        this.vrmanager = empdata.reportinG_OFFICER_NAME;
        this.vastatus = empdata.iS_ACTIVE;

        this.vifsccode = empdata.ifsC_CODE;
        this.vaccname = empdata.accounT_HOLDER_NAME;
        this.vaccno = empdata.accounT_NUMBER_MASK;
        this.vbankname = empdata.banK_NAME;
        this.vbranch = empdata.banK_BRANCH;

        this.vuanno = empdata.uaN_NO;
        this.vpfno = empdata.pF_NO;
        this.vpfdate = (empdata.pF_DATE ? empdata.pF_DATE : "").replace("T00:00:00", "");
        this.vpfoffice = empdata.pF_OFFICE;
        this.vpfaccno = empdata.pF_AC_NO;
        this.vpanno = empdata.paN_NO;


        this.V_Height = empdata.height;
        this.V_Mothertongue = empdata.motheR_TONGUE;
        this.V_Nativeplace = empdata.emP_NATIVE_PLACE;
        this.V_identification1 = empdata.marK_IDENTIFICATION1;
        this.V_identification2 = empdata.marK_IDENTIFICATION2;


      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }

  HistoryAllEmployees(): void {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_02 = "EMPLOYEES";


    this.service.postData(req, "GetEmployeeHistory").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        console.log(data);
        this.allemphistory = data.Details;
        for (let i = 0; i < this.allemphistory.length; i++) {
          let colname = this.allemphistory[i].columN_NAME;
          let oldvalue = this.allemphistory[i].olD_VALUE;
          let newvalue = this.allemphistory[i].neW_VALUE;
          if (colname == "EMP_PHOTO" && oldvalue) {
            this.decryptHistoryFile(oldvalue, i, 'oldvalue');
          }
          if (colname == "EMP_PHOTO" && newvalue) {
            // this.allemphistory[i].neW_VALUE = this.service.getBase64(newvalue);
            // console.log(this.allemphistory[i].neW_VALUE);
            this.decryptHistoryFile(newvalue, i, 'newvalue');
          }
        }
        this.historyAllSection.show();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  decryptHistoryFile(filepath, i, ftype) {
    const coltype: string = ftype;
    const indx = i;
    if (coltype == 'oldvalue')
      this.allemphistory[indx].olD_VALUE = "";
    else
      this.allemphistory[indx].neW_VALUE = "";

    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (coltype == 'oldvalue')
            this.allemphistory[indx].olD_VALUE = data.docBase64;
          else
            this.allemphistory[indx].neW_VALUE = data.docBase64;
        })
    }
  };


  onRowSelected(event) {

    this.EditEmployee(event);
  }

  HistoryEmployee(row): void {
    this.showloader = true;
    const data = row.rowData;
    const req = new InputRequest();
    req.INPUT_01 = data.emP_CODE;

    this.service.postData(req, "GetEmployeeHistory").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.emphistory = data.Details;
        for (let i = 0; i < this.emphistory.length; i++) {
          let colname = this.emphistory[i].columN_NAME;
          let oldvalue = this.emphistory[i].olD_VALUE;
          let newvalue = this.emphistory[i].neW_VALUE;
          if (colname == "EMP_PHOTO" && oldvalue) {
            this.decryptempHistoryFile(oldvalue, i, 'oldvalue');
          }
          if (colname == "EMP_PHOTO" && newvalue) {
            this.decryptempHistoryFile(newvalue, i, 'newvalue');
          }
        }
        this.historySection.show();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  decryptempHistoryFile(filepath, i, ftype) {
    const coltype: string = ftype;
    const indx = i;
    if (coltype == 'oldvalue')
      this.emphistory[indx].olD_VALUE = "";
    else
      this.emphistory[indx].neW_VALUE = "";

    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (coltype == 'oldvalue')
            this.emphistory[indx].olD_VALUE = data.docBase64;
          else
            this.emphistory[indx].neW_VALUE = data.docBase64;
        })
    }
  };

  GetEmployeeDetailsVIEW(emP_CODE: string) {

  }
  GetEmployeeDetails(emP_CODE: string, ptype: string) {




    const rtype: string = ptype;
    const req = new InputRequest();
    req.INPUT_01 = emP_CODE;

    // var data = { 'INPUT_01': this.f.EmpAadhaar.value };
    this.service.postData(req, "GetEmployeeDetails").subscribe(data => {

      console.log(data.Details[0]);

      if (data.StatusCode == "100") {
        const empdata = data.Details[0];
        if (empdata) {
          if (rtype == 'edit')
            this.FillEditEmployeeDetails(empdata);
          else if (rtype == 'view')
            this.FillViewEmployeeDetails(empdata);
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }
  closeprofile() {
    this.Employeeprofilemodal.hide();
  }
  GetEmpFamilyDetails(emP_CODE: string, ptype: string) {
    const rtype: string = ptype;
    const req = new InputRequest();
    req.INPUT_01 = emP_CODE;
    // var data = { 'INPUT_01': this.f.EmpAadhaar.value };
    this.service.postData(req, "GetEmpFamilyDetails").subscribe(data => {

      console.log(data);

      if (data.StatusCode == "100") {
        const empdata = data.Details;
        if (rtype == 'edit') {
          if (empdata.length > 0)
            this.FillEmpFamilyDetails(empdata);
          else {
            this.f.push(this.formBuilder.group({
              fname: ['', Validators.required],
              fdob: ['', Validators.required],
              frelation: [null, Validators.required],
              isnominee: [''],
            }));
          }
        }
        else if (rtype == 'view') {
          this.familylist = empdata;
          console.log(this.familylist);
        }

      }
      else {
        this.f.push(this.formBuilder.group({
          fname: ['', Validators.required],
          fdob: ['', Validators.required],
          frelation: [null, Validators.required],
          isnominee: [''],
        }));
        //Swal.fire('warning', data.StatusMessage, 'warning');
      }


    },
      error => console.log(error));


  }

  FillEditEmployeeDetails(empdata: any) {
    console.log(empdata);


    this.EmpName = empdata.emP_NAME;
    this.empid = empdata.emP_ID;
    this.service_name = empdata.designation;


    this.contact.EmpId.setValue(empdata.emP_ID);
    this.contact.EmpType.setValue(empdata.category);
    this.contact.EmpDesign.setValue(empdata.designatioN_ID)
    this.contact.EmpGender.setValue(empdata.gender)

    this.contact.EmpDOB.setValue(this.datef.parse((empdata.dob ? empdata.dob : "").replace("T00:00:00", "")))
    this.contact.EmpDOJ.setValue(this.datef.parse((empdata.joininG_DATE ? empdata.joininG_DATE : "").replace("T00:00:00", "")))
    this.contact.EmpEdu.setValue(empdata.educatioN_ID)
    this.contact.BloodGroup.setValue(empdata.blooD_GROUP)
    this.contact.EmpEmail.setValue(empdata.emaiL_ID)
    this.contact.EmpMobile.setValue(empdata.mobilE_NUMBER)
    this.contact.EmpCMobile.setValue(empdata.emergencY_CONTACT_NO)
    this.contact.EmpExpYears.setValue(empdata.experiancE_YEARS)
    this.contact.EmpExpMonths.setValue(empdata.experiancE_MONTH)
    this.contact.EmpNationality.setValue(empdata.nationality)
    this.contact.EmpReligion.setValue(empdata.religioN_ID)
    this.contact.EmpCommunity.setValue(empdata.community.toString())//communitY_NAME//
    this.contact.EmpMStatus.setValue(empdata.martiaL_STATUS_ID)
    this.contact.Base64Image.setValue(empdata.emP_PHOTO)
    this.contact.EmpSpouseName.setValue(empdata.spousE_NAME)
    this.contact.EmpADate.setValue(this.datef.parse((empdata.anniversarY_DATE ? empdata.anniversarY_DATE : "").replace("T00:00:00", "")))

    this.contact.EmpIdentification1.setValue(empdata.marK_IDENTIFICATION1);
    this.contact.EmpIdentification2.setValue(empdata.marK_IDENTIFICATION2);
    this.contact.Height.setValue(empdata.height)
    this.contact.MotherTongue.setValue(empdata.motheR_TONGUE)
    this.contact.EmpNativeplace.setValue(empdata.emP_NATIVE_PLACE)



    this.commu.PDistrict.setValue(empdata.preS_DISTRICT_CODE);
    this.commu.PArea.setValue(empdata.preS_RURAL_URBAN);
    this.commu.PEDistrict.setValue(empdata.perM_DISTRICT_CODE);
    this.commu.PEArea.setValue(empdata.perM_RURAL_URBAN);
    this.commu.NState.setValue(empdata.nativE_STATE_CODE.toString());// //nativE_STATE_CODE
    this.commu.NDistrict.setValue(empdata.nativE_DISTRICT_CODE);
    this.commu.NArea.setValue(empdata.nativE_RURAL_URBAN);

    this.commu.nativesameas.setValue("");

    this.DistrictChange('P');
    this.DistrictChange('PE');
    this.DistrictChange('N');


    this.commu.PMandal.setValue(empdata.preS_MMC_CODE);
    this.commu.PEMandal.setValue(empdata.perM_MMC_CODE);
    this.commu.NMandal.setValue(empdata.nativE_MMC_CODE);

    this.MandalChange('P');
    this.MandalChange('PE');
    this.MandalChange('N');

    this.commu.PVillage.setValue(empdata.preS_WARD_VILLAGE_CODE);
    this.commu.PEVillage.setValue(empdata.perM_WARD_VILLAGE_CODE);
    this.commu.NVillage.setValue(empdata.nativE_WARD_VILLAGE_CODE);

    this.commu.PHNO.setValue(empdata.preS_HOUSE_NO);
    this.commu.PStrName.setValue(empdata.preS_STREET_NAME);
    this.commu.PLandmark.setValue(empdata.preS_LAND_MARK);
    this.commu.PPincode.setValue(empdata.preS_PINCODE.toString());
    this.commu.PasPerAddr.setValue(empdata.iS_PRES_PERM_SAME == 1 ? true : false)

    this.commu.PEHNO.setValue(empdata.perM_HOUSE_NO);
    this.commu.PEStrName.setValue(empdata.perM_STREET_NAME);
    this.commu.PELandmark.setValue(empdata.perM_LAND_MARK);
    this.commu.PEPincode.setValue(empdata.perM_PINCODE);

    this.commu.NHNO.setValue(empdata.nativE_HOUSE_NO);
    this.commu.NStrName.setValue(empdata.nativE_STREET_NAME);
    this.commu.NLandmark.setValue(empdata.nativE_LAND_MARK);
    this.commu.NPincode.setValue(empdata.nativE_PINCODE);

    if (empdata.worK_LOCATION_TYPE_ID)
      this.work.WorkLocType.setValue(empdata.worK_LOCATION_TYPE_ID + ":" + empdata.worK_LOCATION_TYPE)
    this.LocationChange();
    if (empdata.worK_LOCATION_CODE)
      this.work.LocationName.setValue(empdata.worK_LOCATION_CODE + ":" + empdata.worK_LOCATION)
    this.work.SectionName.setValue(empdata.section)
    this.work.ReportManager.setValue(empdata.reportinG_OFFICER)
    this.work.IsEmpActive.setValue(empdata.iS_ACTIVE == 'Y' ? '1' : (empdata.iS_ACTIVE == 'N' ? '0' : null))
    this.work.EmpRemarks.setValue(empdata.remarks)

    this.bank.IFSCCode.setValue(empdata.ifsC_CODE)
    this.bank.AccountHName.setValue(empdata.accounT_HOLDER_NAME)
    this.bank.AccountNumber.setValue(empdata.accounT_NUMBER)
    this.bank.ConfirmAccountNumber.setValue(empdata.accounT_NUMBER)
    this.bank.BankName.setValue(empdata.banK_NAME)
    this.bank.BranchName.setValue(empdata.banK_BRANCH)

    this.pf.UANNo.setValue(empdata.uaN_NO)
    this.pf.PFNo.setValue(empdata.pF_NO)
    this.pf.PFData.setValue(this.datef.parse((empdata.pF_DATE ? empdata.pF_DATE : "").replace("T00:00:00", "")))
    this.pf.PFOffice.setValue(empdata.pF_OFFICE)
    this.pf.PFAccNo.setValue(empdata.pF_AC_NO)
    this.pf.PANNo.setValue(empdata.paN_NO)

  }

  FillEmpFamilyDetails(empdata: any) {

    // this.fam.EMPCODE.setValue(empdata.emP_CODE)
    //this.fam.fM_ID.setValue(empdata.fM_ID)
    for (let i = 0; i < empdata.length; i++) {
      // if (i != 0) {
      this.f.push(this.formBuilder.group({
        fname: ['', Validators.required],
        fdob: ['', Validators.required],
        frelation: ['', Validators.required],
        isnominee: [''],
      }));
      // }

      this.f.controls[i].patchValue({
        fname: empdata[i].fM_NAME,
        fdob: this.datef.parse(empdata[i].fM_DOB.replace("T00:00:00", "")),
        frelation: empdata[i].relation,
        isnominee: (empdata[i].fM_NOMINEE == 'Y' ? '1' : '0')
      });

    }
  }

  FillViewEmployeeDetails(empdata: any) {
    this.vemptype = empdata.categorY_NAME;
    this.vempname = empdata.emP_NAME;
    this.vdesignation = empdata.designation;
    this.vgender = empdata.gendeR_NAME;
    this.vdob = (empdata.dob ? empdata.dob : "").replace("T00:00:00", "");
    this.vdoj = (empdata.joininG_DATE ? empdata.joininG_DATE : "").replace("T00:00:00", "");
    this.veducation = empdata.educatioN_QUALIFICATION;
    this.vbloodgrp = empdata.blooD_GROUP1;
    this.vemail = empdata.emaiL_ID;
    this.vmobile = empdata.mobilE_NUMBER;
    this.vecno = empdata.emergencY_CONTACT_NO;
    this.vexpears = empdata.experiancE_YEARS;
    this.vexmonths = empdata.experiancE_MONTH;
    this.vnationality = empdata.nationality;
    this.vreligion = empdata.religion;
    this.vcommunity = empdata.communitY_NAME;
    this.vmstatus = empdata.martiaL_STATUS;
    this.vspouname = empdata.spousE_NAME;
    this.vanaadate = (empdata.anniversarY_DATE ? empdata.anniversarY_DATE : "").replace("T00:00:00", "");
    this.decryptFile(empdata.emP_PHOTO);
    this.vaadhaarphoto = empdata.emP_PHOTO;

    this.vpdistrict = empdata.preS_DISTRICT_NAME;
    this.vpru = empdata.preS_RURAL_URBAN_NAME;
    this.vpmandal = empdata.preS_MMC_NAME;
    this.vpvillage = empdata.preS_WARD_VILLAGE_NAME;
    this.vphno = empdata.preS_HOUSE_NO;
    this.vpstreett = empdata.preS_STREET_NAME;
    this.vplanmark = empdata.preS_LAND_MARK;
    this.vppincode = empdata.preS_PINCODE;

    this.vpedistrict = empdata.perM_DISTRICT_NAME;
    this.vperu = empdata.perM_RURAL_URBAN_NAME;
    this.vpemandal = empdata.perM_MMC_NAME;
    this.vpevillage = empdata.perM_WARD_VILLAGE_NAME;
    this.vpehno = empdata.perM_HOUSE_NO;
    this.vpestreett = empdata.perM_STREET_NAME;
    this.vpelanmark = empdata.perM_LAND_MARK;
    this.vpepincode = empdata.perM_PINCODE;

    this.vnstate = empdata.nativE_STATE_NAME;
    this.vndistrict = empdata.nativE_DISTRICT_NAME;
    this.vnru = empdata.nativE_RURAL_URBAN_NAME;
    this.vnmandal = empdata.nativE_MMC_NAME;
    this.vnvillage = empdata.nativE_WARD_VILLAGE_NAME;
    this.vnhno = empdata.nativE_HOUSE_NO;
    this.vnstreett = empdata.nativE_STREET_NAME;
    this.vnlanmark = empdata.nativE_LAND_MARK;
    this.vnpincode = empdata.nativE_PINCODE;

    this.vloctype = empdata.worK_LOCATION_TYPE;
    this.vlocname = empdata.worK_LOCATION;
    this.vsectionname = empdata.sectioN_NAME;
    this.vrmanager = empdata.reportinG_OFFICER_NAME;
    this.vastatus = empdata.iS_ACTIVE;

    this.vifsccode = empdata.ifsC_CODE;
    this.vaccname = empdata.accounT_HOLDER_NAME;
    this.vaccno = empdata.accounT_NUMBER_MASK;
    this.vbankname = empdata.banK_NAME;
    this.vbranch = empdata.banK_BRANCH;

    this.vuanno = empdata.uaN_NO;
    this.vpfno = empdata.pF_NO;
    this.vpfdate = (empdata.pF_DATE ? empdata.pF_DATE : "").replace("T00:00:00", "");
    this.vpfoffice = empdata.pF_OFFICE;
    this.vpfaccno = empdata.pF_AC_NO;
    this.vpanno = empdata.paN_NO;
  }

  decryptFile(filepath) {
    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          console.log(data.docBase64);
          this.vempphoto = data.docBase64;
        },
          error => {
            console.log(error);
            this.vempphoto = "";
          });
    }
    else
      this.vempphoto = "";
  }

  resetallforms() {
    this.consubmitted = false;
    this.commsubmi = false;
    this.worksubmi = false;
    this.banksubmi = false;
    this.familysubmi = false;
    this.pfsubmi = false;

    this.contactcomple = false;
    this.commcomple = false;
    this.workcomple = false;
    this.bankcomple = false;
    this.familycomple = false;
    this.pfcomple = false;

    this.employeeContactForm.reset();
    this.employeeCommuForm.reset();
    this.employeeWorkForm.reset();
    this.employeeBankForm.reset();
    this.employeeFamilyForm.reset();
    this.employeePFForm.reset();
    this.f.reset();
    this.f.clear();

    this.contact.EmpType.setValue(null);
    this.contact.EmpDesign.setValue(null);
    this.contact.EmpGender.setValue(null);
    this.contact.EmpEdu.setValue(null);
    this.contact.BloodGroup.setValue(null);
    this.contact.EmpExpYears.setValue(null);
    this.contact.EmpExpMonths.setValue(null);
    this.contact.EmpNationality.setValue(null);
    this.contact.EmpReligion.setValue(null);
    this.contact.EmpCommunity.setValue(null);
    this.contact.EmpMStatus.setValue(null);

    this.contact.MotherTongue.setValue("");


    this.commu.PDistrict.setValue(null);
    this.commu.PArea.setValue(null);
    this.commu.PMandal.setValue(null);
    this.commu.PVillage.setValue(null);

    this.commu.PEDistrict.setValue(null);
    this.commu.PEArea.setValue(null);
    this.commu.PEMandal.setValue(null);
    this.commu.PEVillage.setValue(null);

    this.commu.PerAddrnotAvailable.setValue(false);
    this.commu.nativesameas.setValue("");
    this.commu.NState.setValue(null);
    this.commu.NDistrict.setValue(null);
    this.commu.NArea.setValue(null);
    this.commu.NMandal.setValue(null);
    this.commu.NVillage.setValue(null);

    this.work.WorkLocType.setValue(null);
    this.work.LocationName.setValue(null);
    this.work.SectionName.setValue(null);
    this.work.ReportManager.setValue(null);

  }


  AddFalimy() {
    this.f.push(this.formBuilder.group({
      fname: ['', Validators.required],
      fdob: ['', Validators.required],
      frelation: [null, Validators.required],
      isnominee: [''],
    }));
  }

  RemoveFalimy(index) {
    this.f.removeAt(index);
  }

  onSelectFile(event, ptype: string) {
    let url: string;
    let Phototype = ptype;
    if (event.target.files && event.target.files[0]) {
      let imagetype = event.target.files[0].type;
      let imagesize = event.target.files[0].size;

      if (imagetype != 'image/jpeg' && imagetype != 'image/png') {
        Swal.fire('info', 'Please Upload jpeg,jpg,png files only', 'info');
        return false;
      }

      if (imagesize > 2097152) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', "ReceiptInRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.'
            this.uploadFinished(event.body, Phototype);
          }

        });


      // var reader = new FileReader();

      // reader.readAsDataURL(event.target.files[0]); // read file as data url

      // reader.onload = (event) => { // called once readAsDataURL is completed
      //   if (Phototype == 'photo')
      //     this.contact.Base64Image.setValue(event.target.result);
      //   else if (Phototype == 'aadhaar')
      //     this.contact.AadhaarBase64Image.setValue(event.target.result);
      // }
    }
  }

  public uploadFinished = (event, Phototype) => {
    if (Phototype == 'photo')
      this.contact.Base64Image.setValue(event.fullPath);
    else if (Phototype == 'aadhaar')
      this.contact.AadhaarBase64Image.setValue(event.fullPath);
    //this.dep.DocPath.setValue(event.fullPath);
  }

  onDateSelection(event) {

  }

  LoadEmployeeList() {
    this.showloader = true;
    this.service.getData("GetEmpList").subscribe(data => {
      console.log(data);
      if (data.StatusCode == "100") {
        this.employees = data.Details;
        this.EmployeerowData = data.Details;
        this.EmployeerowData = this.EmployeerowData.filter(function (el) { return el.servicE_TYPE == "Regular" })
        if (this.EmployeerowData.length > 0) {
          this.gridApi.setRowData(this.EmployeerowData);
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));
  }

  SubmitContactDetails() {
    this.consubmitted = true;



    // stop here if form is invalid
    if (this.employeeContactForm.invalid) {
      return false;
    }

    if (!this.uidstatus) {
      return false;
    }

    if (!this.contact.EmpName.value) {
      Swal.fire('warning', "Please Enter Employee Name", 'warning');
      return false;
    }

    if (this.datef.GreaterDate(this.contact.EmpDOB.value, this.contact.EmpDOJ.value)) {
      Swal.fire('warning', "Employee Date Of Join Shoud be greater than Date of Birth", 'warning');
      return false;
    }

    if (this.datef.getage(this.contact.EmpDOB.value) < 18) {
      Swal.fire('warning', "Employee Date Of Birth Shoud be 18 years ago of current date", 'warning');
      return false;
    }

    if (this.datef.DiffOfYears(this.contact.EmpDOJ.value, this.contact.EmpDOB.value) < 18) {
      Swal.fire('warning', "Employee Join Date Should be Greater than 18 years of  Employee Date Of Birth", 'warning');
      return false;
    }

    if (this.contact.EmpMStatus.value != '1') {
      // if (!this.contact.EmpSpouseName.value) {
      //   Swal.fire('warning', "Please Enter Spouse Name", 'warning');
      //   return false;
      // }
      if (!this.contact.EmpADate.value) {
        Swal.fire('warning', "Please Select Anniversary Date", 'warning');
        return false;
      }
    }

    if (this.contact.EmpMobile.value == this.contact.EmpCMobile.value) {
      Swal.fire('warning', " Mobile Number & Emergency Contact Number Should Not be Equal", 'warning');
      return false;
    }

    // if(!this.contact.AadhaarImagePath.value){
    //   Swal.fire('warning', " Please Upload Aadhaar Photo", 'warning');
    //   return false;
    // }

    if (!this.contact.EmpAadhaarDecla.value) {
      Swal.fire('warning', "Please Accept Terms & Conditions", 'warning');
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.contact.EmpAadhaar.value;
    req.INPUT_02 = this.contact.EmpId.value;
    req.INPUT_03 = this.contact.EmpType.value;
    req.INPUT_04 = this.contact.EmpName.value;
    req.INPUT_05 = this.contact.EmpDesign.value;
    req.INPUT_06 = this.contact.EmpGender.value;

    req.INPUT_07 = this.datef.format(this.contact.EmpDOB.value);
    req.INPUT_08 = this.datef.format(this.contact.EmpDOJ.value);
    req.INPUT_09 = this.contact.EmpEdu.value;
    req.INPUT_10 = this.contact.BloodGroup.value;
    req.INPUT_11 = this.contact.EmpExpYears.value;
    req.INPUT_12 = this.contact.EmpExpMonths.value;
    req.INPUT_13 = this.contact.EmpNationality.value;
    req.INPUT_14 = this.contact.EmpReligion.value;
    req.INPUT_15 = this.contact.EmpCommunity.value;
    req.INPUT_16 = this.contact.EmpMStatus.value;
    req.INPUT_17 = this.contact.EmpSpouseName.value;
    req.INPUT_18 = this.datef.format(this.contact.EmpADate.value);
    req.INPUT_35 = this.contact.Base64Image.value;
    req.INPUT_20 = this.contact.EmpEmail.value;
    req.INPUT_21 = this.contact.EmpMobile.value;
    req.INPUT_22 = this.contact.EmpCMobile.value;
    req.INPUT_23 = this.contact.AadhaarBase64Image.value;
    req.USER_NAME = this.logUsercode;

    this.service.postData(req, "SaveEmpPrimaryDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.contact.EMPCODE.setValue(result.emP_CODE);
          Swal.fire('success', "Employee General Details Saved Successfully !!!", 'success');
          this.contactcomple = true;
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  UpdateContactDetails() {
    this.consubmitted = true;
    // stop here if form is invalid
    if (this.employeeContactForm.invalid) {
      return false;
    }

    if (this.logUserrole == '101' || this.logUserrole == '102') {
      if (!this.uidstatus) {
        return false;
      }
    }

    if (!this.contact.EmpName.value) {
      Swal.fire('warning', "Please Enter Employee Name", 'warning');
      return false;
    }

    if (this.datef.GreaterDate(this.contact.EmpDOB.value, this.contact.EmpDOJ.value)) {
      Swal.fire('warning', "Employee Date Of Join Shoud be greater than Date of Birth", 'warning');
      return false;
    }

    if (this.datef.getage(this.contact.EmpDOB.value) < 18) {
      Swal.fire('warning', "Employee Date Of Birth Shoud be 18 years ago greater than Date of Birth", 'warning');
      return false;
    }

    if (this.datef.DiffOfYears(this.contact.EmpDOJ.value, this.contact.EmpDOB.value) < 18) {
      Swal.fire('warning', "Employee Join Date Should be Greater than 18 years of  Employee Date Of Birth", 'warning');
      return false;
    }

    if (this.contact.EmpMobile.value == this.contact.EmpCMobile.value) {
      Swal.fire('warning', " Mobile Number & Emergency Contact Number Should Not be Equal", 'warning');
      return false;
    }

    if (this.contact.EmpMStatus.value != '1') {
      // if (!this.contact.EmpSpouseName.value) {
      //   Swal.fire('warning', "Please Enter Spouse Name", 'warning');
      //   return false;
      // }
      if (!this.contact.EmpADate.value) {
        Swal.fire('warning', "Please Select Anniversary Date", 'warning');
        return false;
      }
    }

    this.showloader = true;

    const req = new InputRequest();

    if (this.logUserrole == '101' || this.logUserrole == '102') {
      req.INPUT_01 = this.contact.EMPCODE.value;
      req.INPUT_02 = this.contact.EmpAadhaar.value;
      req.INPUT_03 = this.contact.EmpId.value;
      req.INPUT_04 = this.contact.EmpType.value;
      req.INPUT_05 = this.contact.EmpName.value;
      req.INPUT_06 = this.contact.EmpDesign.value;
      req.INPUT_07 = this.contact.EmpGender.value;
      req.INPUT_08 = this.datef.format(this.contact.EmpDOB.value);
      req.INPUT_09 = this.datef.format(this.contact.EmpDOJ.value);
      req.INPUT_10 = this.contact.EmpEdu.value;
      req.INPUT_11 = this.contact.BloodGroup.value;
      req.INPUT_12 = this.contact.EmpExpYears.value;
      req.INPUT_13 = this.contact.EmpExpMonths.value;
      req.INPUT_14 = this.contact.EmpNationality.value;
      req.INPUT_15 = this.contact.EmpReligion.value;
      req.INPUT_16 = this.contact.EmpCommunity.value;
      req.INPUT_17 = this.contact.EmpMStatus.value;
      req.INPUT_18 = this.contact.EmpSpouseName.value;
      req.INPUT_19 = this.datef.format(this.contact.EmpADate.value);
      req.INPUT_35 = this.contact.Base64Image.value;
      req.INPUT_21 = this.contact.EmpEmail.value;
      req.INPUT_22 = this.contact.EmpMobile.value;
      req.INPUT_23 = this.contact.EmpCMobile.value;

      req.INPUT_27 = this.contact.EmpIdentification1.value;
      req.INPUT_28 = this.contact.EmpIdentification2.value;
      req.INPUT_26 = this.contact.Height.value;
      req.INPUT_25 = this.contact.MotherTongue.value;
      req.INPUT_29 = this.contact.EmpNativeplace.value;

    }
    else {
      req.INPUT_01 = this.contact.EMPCODE.value;
      req.INPUT_02 = this.contact.EmpType.value;
      req.INPUT_03 = this.contact.EmpDesign.value;
      req.INPUT_04 = this.contact.EmpGender.value;

      req.INPUT_05 = this.datef.format(this.contact.EmpDOB.value);
      req.INPUT_06 = this.datef.format(this.contact.EmpDOJ.value);
      req.INPUT_07 = this.contact.EmpEdu.value;
      req.INPUT_08 = this.contact.BloodGroup.value;
      req.INPUT_09 = this.contact.EmpExpYears.value;
      req.INPUT_10 = this.contact.EmpExpMonths.value;
      req.INPUT_11 = this.contact.EmpNationality.value;
      req.INPUT_12 = this.contact.EmpReligion.value;
      req.INPUT_13 = this.contact.EmpCommunity.value;
      req.INPUT_14 = this.contact.EmpMStatus.value;
      req.INPUT_15 = this.contact.EmpSpouseName.value;
      req.INPUT_16 = this.datef.format(this.contact.EmpADate.value);
      req.INPUT_35 = this.contact.Base64Image.value;
      req.INPUT_18 = this.contact.EmpEmail.value;
      req.INPUT_19 = this.contact.EmpMobile.value;
      req.INPUT_20 = this.contact.EmpCMobile.value;

      req.INPUT_27 = this.contact.EmpIdentification1.value;
      req.INPUT_28 = this.contact.EmpIdentification2.value;
      req.INPUT_26 = this.contact.Height.value;
      req.INPUT_25 = this.contact.MotherTongue.value;
      req.INPUT_29 = this.contact.EmpNativeplace.value;

    }

    let methodname: string = "";
    if (this.logUserrole == '101' || this.logUserrole == '102')
      methodname = "UpdateAdminEmpPrimaryDetails";
    else
      methodname = "UpdatedEmployeeDetails";

    req.USER_NAME = this.logUsercode;

    this.service.postData(req, methodname).subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.contactcomple = true;
          Swal.fire('success', "Employee General Details Updated Successfully !!!", 'success');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }


  SubmitCommuDetails() {

    this.commsubmi = true;
    // stop here if form is invalid

    if (!this.commu.PasPerAddr.value && !this.commu.PerAddrnotAvailable) {
      if (!this.commu.PEDistrict.value || !this.commu.PEArea.value || !this.commu.PEMandal.value || !this.commu.PEVillage.value || !this.commu.PEHNO.value || !this.commu.PEStrName.value || !this.commu.PEPincode.value) {
        return false;
      }
    }

    if (!this.commu.nativesameas.value) {
      if (!this.commu.NState.value || !this.commu.NDistrict.value || !this.commu.NArea.value || !this.commu.NMandal.value || !this.commu.NVillage.value || !this.commu.NHNO.value || !this.commu.NStrName.value || !this.commu.NPincode.value) {
        return false;
      }
    }

    if (this.employeeCommuForm.invalid) {
      return false;
    }

    if (!this.contact.EMPCODE.value) {
      Swal.fire('warning', "Please submit general section first", 'warning');
      return false;
    }





    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.contact.EMPCODE.value;
    req.INPUT_02 = this.commu.PDistrict.value;
    req.INPUT_03 = this.commu.PArea.value;
    req.INPUT_04 = this.commu.PMandal.value;
    req.INPUT_05 = this.commu.PVillage.value;
    req.INPUT_06 = this.commu.PHNO.value;
    req.INPUT_07 = this.commu.PStrName.value;
    req.INPUT_08 = this.commu.PLandmark.value;
    req.INPUT_09 = this.commu.PPincode.value;
    req.INPUT_10 = (this.commu.PasPerAddr.value == true ? "1" : "0");

    req.INPUT_11 = (this.commu.PasPerAddr.value == true ? this.commu.PDistrict.value : this.commu.PEDistrict.value);
    req.INPUT_12 = (this.commu.PasPerAddr.value == true ? this.commu.PArea.value : this.commu.PEArea.value);
    req.INPUT_13 = (this.commu.PasPerAddr.value == true ? this.commu.PMandal.value : this.commu.PEMandal.value);
    req.INPUT_14 = (this.commu.PasPerAddr.value == true ? this.commu.PVillage.value : this.commu.PEVillage.value);
    req.INPUT_15 = (this.commu.PasPerAddr.value == true ? this.commu.PHNO.value : this.commu.PEHNO.value);
    req.INPUT_16 = (this.commu.PasPerAddr.value == true ? this.commu.PStrName.value : this.commu.PEStrName.value);
    req.INPUT_17 = (this.commu.PasPerAddr.value == true ? this.commu.PLandmark.value : this.commu.PELandmark.value);
    req.INPUT_18 = (this.commu.PasPerAddr.value == true ? this.commu.PPincode.value : this.commu.PEPincode.value);

    req.INPUT_19 = (this.commu.nativesameas.value == 'present' ? "28" : (this.commu.nativesameas.value == 'permanent' ? "28" : this.commu.NState.value));
    req.INPUT_20 = (this.commu.nativesameas.value == 'present' ? this.commu.PDistrict.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PDistrict.value : this.commu.PEDistrict.value) : this.commu.NDistrict.value));
    req.INPUT_21 = (this.commu.nativesameas.value == 'present' ? this.commu.PArea.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PArea.value : this.commu.PEArea.value) : this.commu.NArea.value));
    req.INPUT_22 = (this.commu.nativesameas.value == 'present' ? this.commu.PMandal.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PMandal.value : this.commu.PEMandal.value) : this.commu.NMandal.value));
    req.INPUT_23 = (this.commu.nativesameas.value == 'present' ? this.commu.PVillage.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PVillage.value : this.commu.PEVillage.value) : this.commu.NVillage.value));
    req.INPUT_24 = (this.commu.nativesameas.value == 'present' ? this.commu.PHNO.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PHNO.value : this.commu.PEHNO.value) : this.commu.NHNO.value));
    req.INPUT_25 = (this.commu.nativesameas.value == 'present' ? this.commu.PStrName.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PStrName.value : this.commu.PEStrName.value) : this.commu.NStrName.value));
    req.INPUT_26 = (this.commu.nativesameas.value == 'present' ? this.commu.PLandmark.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PLandmark.value : this.commu.PELandmark.value) : this.commu.NLandmark.value));
    req.INPUT_27 = (this.commu.nativesameas.value == 'present' ? this.commu.PPincode.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PPincode.value : this.commu.PEPincode.value) : this.commu.NPincode.value));
    req.INPUT_28 = this.commu.nativesameas.value;
    req.USER_NAME = this.logUsercode;

    this.service.postData(req, "SaveEmpCommuDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.commcomple = true;
          Swal.fire('success', "Employee Communication Details Saved Successfully !!!", 'success');
        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        ;
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  SubmitWorkDetails() {
    this.worksubmi = true;
    // stop here if form is invalid
    if (this.employeeWorkForm.invalid) {
      return false;
    }

    if (!this.contact.EMPCODE.value) {
      Swal.fire('warning', "Please submit general section first", 'warning');
      return false;
    }

    if (this.work.WorkLocType.value == '1' && !this.work.SectionName.value) {
      Swal.fire('warning', "Please Section Name", 'warning');
      return false;
    }

    if (this.work.IsEmpActive.value == '0' && !this.work.EmpRemarks.value) {
      Swal.fire('warning', "Please Enter Remarks", 'warning');
      return false;
    }


    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.contact.EMPCODE.value;
    req.INPUT_02 = this.work.WorkLocType.value.split(":")[1];
    req.INPUT_03 = this.work.LocationName.value.split(":")[0];
    req.INPUT_04 = this.work.LocationName.value.split(":")[1];
    req.INPUT_05 = this.work.ReportManager.value;
    req.INPUT_06 = this.work.IsEmpActive.value == 1 ? "Y" : "N";
    req.INPUT_07 = this.work.SectionName.value;
    req.INPUT_08 = this.work.EmpRemarks.value;
    req.USER_NAME = this.logUsercode;

    this.service.postData(req, "SaveEmpWorkDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.workcomple = true;
          Swal.fire('success', "Employee Work Details Saved Successfully !!!", 'success');
        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  SubmitBankDetails() {
    this.banksubmi = true;
    // stop here if form is invalid
    if (this.employeeBankForm.invalid) {
      return false;
    }

    if (!this.contact.EMPCODE.value) {
      Swal.fire('warning', "Please submit general section first", 'warning');
      return false;
    }

    if (this.bank.AccountNumber.value.length != this.bank.AccLength.value) {
      Swal.fire('warning', this.bank.AccountHName.value + " Account Number Shoud be " + this.bank.AccLength.value + " digits", 'warning');
      return false;
    }

    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.contact.EMPCODE.value;
    req.INPUT_02 = this.bank.IFSCCode.value;
    req.INPUT_03 = this.bank.AccountHName.value;
    req.INPUT_04 = this.bank.AccountNumber.value;
    req.INPUT_05 = this.bank.BankName.value;
    req.INPUT_06 = this.bank.BranchName.value;
    req.USER_NAME = this.logUsercode;

    this.service.postData(req, "SaveEmpBankDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.bankcomple = true;
          Swal.fire('success', "Employee Bank Details Saved Successfully !!!", 'success');
        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  SubmitFamilyDetails() {
    this.familysubmi = true;

    let isnomineeselect: boolean = false
    let age: number = 0;

    // stop here if form is invalid
    if (this.employeeFamilyForm.invalid) {
      return false;
    }

    for (let i = 0; i < this.f.length; i++) {
      if (this.f.value[i].isnominee) {
        isnomineeselect = true;
        age = this.datef.getage(this.f.value[i].fdob)
      }
    }

    if (!isnomineeselect) {
      Swal.fire('warning', "Please Select Atleast one person as Nominee", 'warning');
      return false;
    }
    if (age < 18) {
      Swal.fire('warning', "Nominee age should be 18 years or above", 'warning');
      return false;
    }

    if (!this.contact.EMPCODE.value) {
      Swal.fire('warning', "Please submit general section first", 'warning');
      return false;
    }

    this.showloader = true;

    let idslist: string[] = [];
    let fnamelist: string[] = [];
    let fdoblist: string[] = [];
    let frellist: string[] = [];
    let fnomineelist: string[] = [];

    for (let i = 0; i < this.f.length; i++) {
      idslist.push((i + 1).toString());
      fnamelist.push(this.f.value[i].fname);
      fdoblist.push(this.datef.format(this.f.value[i].fdob));
      frellist.push(this.f.value[i].frelation);
      fnomineelist.push((this.f.value[i].isnominee == "1" ? "1" : "0"));
    }

    const req = new InputRequest();
    req.INPUT_01 = this.contact.EMPCODE.value;
    req.INPUT_02 = idslist.join(',');
    req.INPUT_03 = fnamelist.join(',');
    req.INPUT_04 = fdoblist.join(',');
    req.INPUT_05 = frellist.join(',');
    req.INPUT_06 = fnomineelist.join(',');
    req.USER_NAME = this.logUsercode;

    this.service.postData(req, "SaveEmpFamilyDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.familycomple = true;
          Swal.fire('success', "Employee Family Details Saved Successfully !!!", 'success');
        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  SubmitPFDetails() {
    this.pfsubmi = true;
    // stop here if form is invalid
    if (this.employeePFForm.invalid) {
      return false;
    }

    if (!this.contact.EMPCODE.value) {
      Swal.fire('warning', "Please submit general section first", 'warning');
      return false;
    }

    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.contact.EMPCODE.value;
    req.INPUT_02 = this.pf.UANNo.value;
    req.INPUT_03 = this.pf.PFNo.value;
    req.INPUT_04 = this.datef.format(this.pf.PFData.value);
    req.INPUT_05 = this.pf.PFOffice.value;
    req.INPUT_06 = this.pf.PFAccNo.value;
    req.INPUT_07 = this.pf.PANNo.value;
    req.USER_NAME = this.logUsercode;

    this.service.postData(req, "SaveEmpPFDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.pfcomple = true;
          Swal.fire('success', "Employee Providend Fund Details Saved Successfully !!!", 'success');
          // $("#editSection").modal("hide");
          this.editModal.hide();
          this.LoadEmployeeList();
        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }


  GetIFSCDetails() {
    if (this.bank.IFSCCode.value.length == 11) {
      this.showloader = true;
      var data = { 'INPUT_01': this.bank.IFSCCode.value };
      this.service.postData(data, "GetIFSCCodeDetails").subscribe(data => {
        this.showloader = false;

        if (data.StatusCode == "100") {
          let result = data.Details[0];

          this.bank.BankName.setValue(result.banK_NAME);
          this.bank.BranchName.setValue(result.branch);
          this.bank.AccLength.setValue(result.aC_LEANTH);

        }
        else
          Swal.fire('warning', data.StatusMessage, 'warning');

      },
        error => console.log(error));
    }

  }

  LoadEmployeeTypes() {
    this.service.getData("GetEmployeeTypes").subscribe(data => {
      if (data.StatusCode == "100") {
        this.emptypes = data.Details;
      }
    },
      error => console.log(error));
  }

  LoadGenders() {
    this.service.getData("GetGenders").subscribe(data => {

      if (data.StatusCode == "100") {
        this.genders = data.Details;
      }

    },
      error => console.log(error));
  }

  LoadSections() {
    this.service.getData("GetSections").subscribe(data => {

      if (data.StatusCode == "100") {
        this.sections = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadEducations() {
    this.service.getData("GetEducations").subscribe(data => {

      if (data.StatusCode == "100") {
        this.educations = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadBloodGroups() {
    this.service.getData("GetBloodGroups").subscribe(data => {

      if (data.StatusCode == "100") {
        this.bloodgroups = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadEmpExpYears() {
    this.service.getData("GetExperianceYears").subscribe(data => {

      if (data.StatusCode == "100") {
        this.expyears = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadEmpExpMonths() {
    this.service.getData("GetExperianceMonths").subscribe(data => {

      if (data.StatusCode == "100") {
        this.expmonths = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadNationality() {
    // this.service.getData("GetNationality").subscribe(data => {
    //   console.log(data);
    //   if (data.StatusCode == "100") {
    //     this.nationalites = data.Details;
    //     console.log(this.nationalites);
    //   }

    // },
    //   error => console.log(error));
  }

  LoadReligions() {
    this.service.getData("GetReligions").subscribe(data => {

      if (data.StatusCode == "100") {
        this.religions = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadCommunities() {
    this.service.getData("GetCommunities").subscribe(data => {

      if (data.StatusCode == "100") {
        this.communities = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadMaritalStatus() {
    this.service.getData("GetMaritalStatus").subscribe(data => {

      if (data.StatusCode == "100") {
        this.mstatus = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadDesignations() {
    this.service.getData("GetDesignations").subscribe(data => {

      if (data.StatusCode == "100") {
        this.desinations = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadDistricts() {
    this.service.getData("GetDistricts").subscribe(data => {

      if (data.StatusCode == "100") {
        this.districts = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadurFlags() {
    this.service.getData("GetAreaTypes").subscribe(data => {

      if (data.StatusCode == "100") {
        this.urlist = data.Details;

      }

    },
      error => console.log(error));
  }

  DistrictChange(type: string) {
    let typevlaue: string = type;
    if (typevlaue == 'P') {
      this.pmandals = [];
      this.pvillages = [];
      this.commu.PMandal.setValue("");
      this.commu.PVillage.setValue("");
      if (this.commu.PDistrict.value && this.commu.PArea.value) {
        this.LoadMandals(typevlaue, this.commu.PDistrict.value, this.commu.PArea.value)
      }
    }
    else if (typevlaue == 'PE') {
      this.pemandals = [];
      this.pevillages = [];
      this.commu.PEMandal.setValue("");
      this.commu.PEVillage.setValue("");
      if (this.commu.PEDistrict.value && this.commu.PEArea.value) {
        this.LoadMandals(typevlaue, this.commu.PEDistrict.value, this.commu.PEArea.value)
      }
    }
    else if (typevlaue == 'N') {
      this.nmandals = [];
      this.nvillages = [];
      this.commu.NMandal.setValue("");
      this.commu.NVillage.setValue("");
      if (this.commu.NDistrict.value && this.commu.NArea.value) {
        this.LoadMandals(typevlaue, this.commu.NDistrict.value, this.commu.NArea.value)
      }
    }

  }

  LoadMandals(type: string, distval: string, areaval: string) {
    this.showloader = true;
    let typevlaue: string = type;
    const req = new InputRequest();
    req.INPUT_01 = areaval;
    req.INPUT_02 = distval;

    this.service.postData(req, "GetMandlas").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (typevlaue == 'P')
          this.pmandals = data.Details;
        else if (typevlaue == 'PE')
          this.pemandals = data.Details;
        else if (typevlaue == 'N')
          this.nmandals = data.Details;
      }

    },
      error => console.log(error));
  }

  MandalChange(type: string) {
    let typevlaue: string = type;

    if (typevlaue == 'P') {
      this.pvillages = [];
      this.commu.PVillage.setValue("");
      if (this.commu.PDistrict.value && this.commu.PMandal.value) {
        this.LoadVillages(typevlaue, this.commu.PDistrict.value, this.commu.PMandal.value)
      }
    }
    else if (typevlaue == 'PE') {
      this.pevillages = [];
      this.commu.PEVillage.setValue("");
      if (this.commu.PEDistrict.value && this.commu.PEMandal.value) {
        this.LoadVillages(typevlaue, this.commu.PEDistrict.value, this.commu.PEMandal.value)
      }
    }
    else if (typevlaue == 'N') {
      this.nvillages = [];
      this.commu.NVillage.setValue("");
      if (this.commu.NDistrict.value && this.commu.NMandal.value) {
        this.LoadVillages(typevlaue, this.commu.NDistrict.value, this.commu.NMandal.value)
      }
    }

  }

  LoadVillages(type: string, distval: string, manval: string) {
    this.showloader = true;
    let typevlaue: string = type;
    let obj: any = { "INPUT_02": distval, "INPUT_03": manval }

    this.service.postData(obj, "GetVillages").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        if (typevlaue == 'P')
          this.pvillages = data.Details;
        else if (typevlaue == 'PE')
          this.pevillages = data.Details;
        else if (typevlaue == 'N')
          this.nvillages = data.Details;



      }

    },
      error => console.log(error));
  }

  LoadStates() {
    this.service.getData("GetStates").subscribe(data => {

      if (data.StatusCode == "100") {
        this.states = data.Details;

      }

    },
      error => console.log(error));
  }

  PresentAsPerminentChange() {
    this.hideperaadr = !this.commu.PasPerAddr.value;

    if (this.commu.PasPerAddr.value) {

    }
    else {

    }

  }

  ResetPerminentDetails() {

  }

  LoadLocationTypes() {
    this.service.getData("GetWorkLocations").subscribe(data => {

      if (data.StatusCode == "100") {
        this.worklocations = data.Details;

      }

    },
      error => console.log(error));
  }

  LocationChange() {
    this.locations = [];
    this.locationtypeid = '';
    this.work.LocationName.setValue(null);
    this.work.ReportManager.setValue(null);
    if (this.work.WorkLocType.value) {
      this.locationtypeid = this.work.WorkLocType.value.split(":")[0];
      this.LoadLocations(this.work.WorkLocType.value.split(":")[1]);
    }

  }

  LoadLocations(loctype: string) {
    this.showloader = true;
    var data = { "INPUT_01": loctype };
    this.service.postData(data, "GetLocations").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.locations = data.Details;

      }

    },
      error => console.log(error));
  }


  LoadRManagers() {

    this.service.getData("GetRManagers").subscribe(data => {

      if (data.StatusCode == "100") {
        this.rmanagers = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadRelations() {
    this.service.getData("GetRelations").subscribe(data => {

      if (data.StatusCode == "100") {
        this.relations = data.Details;

      }

    },
      error => console.log(error));
  }



}