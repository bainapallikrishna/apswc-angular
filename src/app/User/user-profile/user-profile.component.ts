import { Component, OnInit,ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { MustMatch } from 'src/app/custome-directives/must-match.validator';
import { InputRequest } from 'src/app/Interfaces/employee';
import { AadharValidateService } from 'src/app/Services/aadhar-validate.service';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  logUserrole:string = sessionStorage.getItem("logUserrole");
  logUsercode:string = sessionStorage.getItem("logUserCode");
  @ViewChild('editModal') public editModal:ModalDirective;

  uidstatus: boolean = true;
  uid_isvalid: boolean = true;
  Emp_mob_isvalid = true;
  Emp_Emergencymob_isvalid = true;
  PanPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  IFSCPattern = "^[A-Z]{4}0[A-Z0-9]{6}$"; 

  NgbDateStruct:any;
  public isEdit: boolean = true;

  public consubmitted: boolean = false;
  public commsubmi: boolean = false;
  public worksubmi: boolean = false;
  public banksubmi: boolean = false;
  public familysubmi: boolean = false;
  public pfsubmi: boolean = false;

  public contactcomple: boolean = false;
  public commcomple: boolean = false;
  public workcomple: boolean = false;
  public bankcomple: boolean = false;
  public familycomple: boolean = false;
  public pfcomple: boolean = false;

  public hideperaadr: boolean = false;

  employeeContactForm: FormGroup;
  employeeCommuForm: FormGroup;
  employeeBankForm: FormGroup;
  employeeFamilyForm: FormGroup;
  employeePFForm: FormGroup;

  employeeslist: any = [];
  emptypes: any = [];
  genders: any = [];
  sections: any = [];
  educations: any = [];
  bloodgroups: any = [];
  expyears: any = [];
  expmonths: any = [];
  nationalites: any = [];
  religions: any = [];
  communities: any = [];
  mstatus: any = [];
  desinations: any = [];

  districts: any = [];
  urlist: any = [];
  pmandals: any = [];
  pvillages: any = [];

  pemandals: any = [];
  pevillages: any = [];

  states: any = [];
  ndistricts: any = [];
  nmandals: any = [];
  nvillages: any = [];

  worklocations: any = [];
  locations: any = [];
  rmanagers: any = [];
  relations: any = [];

  vaadhaar: string;
  vempcode: string;
  vempid: string;
  vemptype: string;
  vempname: string;
  vdesignation: string;
  vgender: string;
  vdob: string;
  vdoj: string;
  veducation: string;
  vbloodgrp: string;
  vemail: string;
  vmobile: string;
  vecno: string;
  vexpears: string;
  vexmonths: string;
  vnationality: string;
  vreligion: string;
  vcommunity: string;
  vmstatus: string;
  vspouname: string;
  vanaadate: string;
  vempphoto: string;
  vaadhaarphoto: string;

  vpdistrict: string;
  vpru: string;
  vpmandal: string;
  vpvillage: string;
  vphno: string;
  vpstreett: string;
  vplanmark: string;
  vppincode: string;

  vpedistrict: string;
  vperu: string;
  vpemandal: string;
  vpevillage: string;
  vpehno: string;
  vpestreett: string;
  vpelanmark: string;
  vpepincode: string;

  vnstate: string;
  vndistrict: string;
  vnru: string;
  vnmandal: string;
  vnvillage: string;
  vnhno: string;
  vnstreett: string;
  vnlanmark: string;
  vnpincode: string;

  vloctype: string;
  vlocname: string;
  vsectionname: string;
  vrmanager: string;
  vastatus: string;

  vifsccode: string;
  vaccname: string;
  vaccno: string;
  vbankname: string;
  vbranch: string;

  familylist:any=[];

  vuanno: string;
  vpfno: string;
  vpfdate: string;
  vpfoffice: string;
  vpfaccno: string;
  vpanno: string;

  showloader = true;

  constructor(private service: CommonServices, private formBuilder: FormBuilder, private uidservice: AadharValidateService, private datef: CustomDateParserFormatter,private router: Router) {
    if(!this.logUserrole){
      this.router.navigate(['/Login']);
      return;
    }
   }

  ngOnInit(): void {
    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }
    this.showloader = true;
    this.LoadDistricts(); // load districts
    this.LoadurFlags(); // load areas
    this.LoadEmployeeTypes(); // load employee types
    this.LoadGenders();
    this.LoadSections();
    this.LoadDesignations();
    this.LoadEducations();
    this.LoadBloodGroups();
    this.LoadEmpExpYears();
    this.LoadEmpExpMonths();
    this.LoadNationality();
    this.LoadReligions();
    this.LoadCommunities();
    this.LoadMaritalStatus();
    this.LoadStates();
    this.LoadRelations();

    this.employeeContactForm = this.formBuilder.group({
      EMPCODE: [''],
      EmpAadhaar: [''],
      EmpId: [''],
      EmpName: [''],
      EmpType: [''],
      EmpDesign: [''],
      EmpGender: [''],
      EmpDOB: [''],
      EmpDOJ: [''],
      EmpEdu: ['', Validators.required],
      BloodGroup: ['', Validators.required],
      EmpExpYears: [''],
      EmpExpMonths: [''],
      EmpNationality: ['', Validators.required],
      EmpReligion: ['', Validators.required],
      EmpCommunity: ['', Validators.required],
      EmpMStatus: ['', Validators.required],
      EmpSpouseName: [''],
      EmpADate: [''],
      Base64Image: [''],
      EmpEmail: ['', [Validators.required, Validators.email]],
      EmpMobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      EmpCMobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      ImagePath:[''],
    });

    this.employeeCommuForm = this.formBuilder.group({
      PDistrict: ['', Validators.required],
      PArea: ['', Validators.required],
      PMandal: ['', Validators.required],
      PVillage: ['', Validators.required],
      PHNO: ['', Validators.required],
      PStrName: ['', Validators.required],
      PLandmark: [''],
      PPincode: ['', Validators.required, Validators.minLength(6)],

      PasPerAddr: [false],
      nativesameas:[''],

      PEDistrict: ['', Validators.required],
      PEArea: ['', Validators.required],
      PEMandal: ['', Validators.required],
      PEVillage: ['', Validators.required],
      PEHNO: ['', Validators.required],
      PEStrName: ['', Validators.required],
      PELandmark: [''],
      PEPincode: ['', Validators.required, Validators.minLength(6)],


      NState: ['', Validators.required],
      NDistrict: ['', Validators.required],
      NArea: ['', Validators.required],
      NMandal: ['', Validators.required],
      NVillage: ['', Validators.required],
      NHNO: ['', Validators.required],
      NStrName: ['', Validators.required],
      NLandmark: [''],
      NPincode: ['', Validators.required, Validators.minLength(6)],

    });

    this.employeeBankForm = this.formBuilder.group({
      AccLength: [],
      AccountHName: ['', Validators.required],
      AccountNumber: ['', Validators.required],
      ConfirmAccountNumber: ['', Validators.required],
      IFSCCode: ['', [Validators.required, Validators.minLength(11), Validators.pattern(this.IFSCPattern)]],
      BankName: ['', Validators.required],
      BranchName: ['', Validators.required],

    }
      , {
        validator: MustMatch('AccountNumber', 'ConfirmAccountNumber')
      }
    );

    this.employeeFamilyForm = this.formBuilder.group({
      //FamilyCount:['0'],
      familymembers: new FormArray([
        this.formBuilder.group({
          fname: ['', Validators.required],
          fdob: ['', Validators.required],
          frelation: ['', Validators.required],
          isnominee: [''],
        })
      ])
    });

    this.employeePFForm = this.formBuilder.group({
      UANNo: [''],
      PFNo: [''],
      PFData: [''],
      PFOffice: [''],
      PFAccNo: [''],
      PANNo: ['', [Validators.required, Validators.minLength(10),
      Validators.pattern(this.PanPattern)]]
    });

    this.contact.EMPCODE.setValue(this.logUsercode);

    this.GetEmployeeDetails(this.logUsercode);
    this.GetEmpFamilyDetails(this.logUsercode);
    this.showloader = false;
  }

  get contact() { return this.employeeContactForm.controls; }
  get commu() { return this.employeeCommuForm.controls; }
  get bank() { return this.employeeBankForm.controls; }
  get fam() { return this.employeeFamilyForm.controls; }
  get pf() { return this.employeePFForm.controls; }
  get f() { return this.fam.familymembers as FormArray; }

  CloseEditModal() {
    this.editModal.hide();
  }

  AddFalimy() {
    this.f.push(this.formBuilder.group({
      fname: ['', Validators.required],
      fdob: ['', Validators.required],
      frelation: ['', Validators.required],
      isnominee: [''],
    }));
  }

  RemoveFalimy(index) {
    this.f.removeAt(index);
  }

  uidvalidate(EmpAadhaar) {
    if (EmpAadhaar.length == 12) {
      this.mob_uid_validate(EmpAadhaar);
      this.uidstatus = this.uidservice.validate(this.employeeContactForm.controls.EmpAadhaar.value);
      
    }
  }

  IsEmployeeExist(EmpAadhaar){
    if (EmpAadhaar.length == 12) {
      //alert(EmpAadhaar);
    }
  }

  mob_uid_validate(mobilephone) {
    if (mobilephone.length === 10)
      this.Emp_mob_isvalid = this.uidservice.validatemob(mobilephone);
    else if (mobilephone.length === 12)
      this.uid_isvalid = this.uidservice.validatemob(this.employeeContactForm.controls.EmpAadhaar.value);
  }
  mob_emr_validate(mobilephone) {
    if (mobilephone.length === 10)
      this.Emp_Emergencymob_isvalid = this.uidservice.validatemob(mobilephone);
  }

  GetEmployeeDetails(emP_CODE: string) {
   
    const req = new InputRequest();
    req.INPUT_01 = emP_CODE;
    // var data = { 'INPUT_01': this.f.EmpAadhaar.value };
    this.service.postData(req, "GetEmployeeDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        const empdata = data.Details[0];
        if (empdata) {
            this.FillEditEmployeeDetails(empdata);
            this.FillViewEmployeeDetails(empdata);
        }
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      
    },
      error => console.log(error));


  }

  GetEmpFamilyDetails(emP_CODE: string) {
    const req = new InputRequest();
    req.INPUT_01 = emP_CODE;
    this.service.postData(req, "GetEmpFamilyDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        const empdata = data.Details;
          this.familylist = empdata;
          console.log(this.familylist);

          if (empdata.length > 0)
            this.FillEmpFamilyDetails(empdata);
      }
    },
      error => console.log(error));
  }

  FillViewEmployeeDetails(empdata: any) {
    this.vaadhaar=empdata.uiD_MASK;
    this.vempid = empdata.emP_ID;
    this.vemptype = empdata.categorY_NAME;
    this.vempname = empdata.emP_NAME;
    this.vdesignation = empdata.designation;
    this.vgender = empdata.gendeR_NAME;
    this.vdob = (empdata.dob ? empdata.dob : "").replace("T00:00:00","");
    this.vdoj = (empdata.joininG_DATE ? empdata.joininG_DATE : "").replace("T00:00:00","");
    this.veducation = empdata.educatioN_QUALIFICATION;
    this.vbloodgrp = empdata.blooD_GROUP1;
    this.vemail = empdata.emaiL_ID;
    this.vmobile = empdata.mobilE_NUMBER;
    this.vecno = empdata.emergencY_CONTACT_NO;
    this.vexpears = empdata.experiancE_YEARS;
    this.vexmonths = empdata.experiancE_MONTH;
    this.vnationality = empdata.nationality;
    this.vreligion = empdata.religion;
    this.vcommunity = empdata.communitY_NAME;
    this.vmstatus = empdata.maritaL_STATUS;
    this.vspouname = empdata.spousE_NAME;
    this.vanaadate = (empdata.anniversarY_DATE ? empdata.anniversarY_DATE : "").replace("T00:00:00","");
    this.vempphoto = empdata.emP_PHOTO;
    this.vaadhaarphoto = empdata.emP_PHOTO;

    this.vpdistrict = empdata.preS_DISTRICT_NAME;
    this.vpru = empdata.preS_RURAL_URBAN_NAME;
    this.vpmandal = empdata.preS_MMC_NAME;
    this.vpvillage = empdata.preS_WARD_VILLAGE_NAME;
    this.vphno = empdata.preS_HOUSE_NO;
    this.vpstreett = empdata.preS_STREET_NAME;
    this.vplanmark = empdata.preS_LAND_MARK;
    this.vppincode = empdata.preS_PINCODE;

    this.vpedistrict = empdata.perM_DISTRICT_NAME;
    this.vperu = empdata.perM_RURAL_URBAN_NAME;
    this.vpemandal = empdata.perM_MMC_NAME;
    this.vpevillage = empdata.perM_WARD_VILLAGE_NAME;
    this.vpehno = empdata.perM_HOUSE_NO;
    this.vpestreett = empdata.perM_STREET_NAME;
    this.vpelanmark = empdata.perM_LAND_MARK;
    this.vpepincode = empdata.perM_PINCODE;

    this.vnstate = empdata.nativE_STATE_NAME;
    this.vndistrict = empdata.nativE_DISTRICT_NAME;
    this.vnru = empdata.nativE_RURAL_URBAN_NAME;
    this.vnmandal = empdata.nativE_MMC_NAME;
    this.vnvillage = empdata.nativE_WARD_VILLAGE_NAME;
    this.vnhno = empdata.nativE_HOUSE_NO;
    this.vnstreett = empdata.nativE_STREET_NAME;
    this.vnlanmark = empdata.nativE_LAND_MARK;
    this.vnpincode = empdata.nativE_PINCODE;

    this.vloctype = empdata.worK_LOCATION_TYPE;
    this.vlocname = empdata.worK_LOCATION;
    this.vsectionname = empdata.sectioN_NAME;
    this.vrmanager = empdata.reportinG_OFFICER;
    this.vastatus = empdata.iS_ACTIVE;

    this.vifsccode = empdata.ifsC_CODE;
    this.vaccname = empdata.accounT_HOLDER_NAME;
    this.vaccno = empdata.accounT_NUMBER_MASK;
    this.vbankname = empdata.banK_NAME;
    this.vbranch = empdata.banK_BRANCH;

    this.vuanno = empdata.uaN_NO;
    this.vpfno = empdata.pF_NO;
    this.vpfdate = (empdata.pF_DATE ? empdata.pF_DATE : "").replace("T00:00:00","");
    this.vpfoffice = empdata.pF_OFFICE;
    this.vpfaccno = empdata.pF_AC_NO;
    this.vpanno = empdata.paN_NO;
    this.showloader = false;
  }

  FillEditEmployeeDetails(empdata: any) {
    this.contact.EmpAadhaar.setValue(empdata.uiD_MASK);
    this.contact.EmpId.setValue(empdata.emP_ID);
    this.contact.EmpName.setValue(empdata.emP_NAME);
    this.contact.EmpType.setValue(empdata.categorY_NAME);
    this.contact.EmpDesign.setValue(empdata.designation)
    this.contact.EmpGender.setValue(empdata.gendeR_NAME)
    
    this.contact.EmpDOB.setValue(this.datef.parse((empdata.dob ? empdata.dob : "").replace("T00:00:00","")))
    this.contact.EmpDOJ.setValue(this.datef.parse((empdata.joininG_DATE ? empdata.joininG_DATE:"").replace("T00:00:00","")))
    this.contact.EmpEdu.setValue(empdata.educatioN_ID)
    this.contact.BloodGroup.setValue(empdata.blooD_GROUP)
    this.contact.EmpEmail.setValue(empdata.emaiL_ID)
    this.contact.EmpMobile.setValue(empdata.mobilE_NUMBER)
    this.contact.EmpCMobile.setValue(empdata.emergencY_CONTACT_NO)
    this.contact.EmpExpYears.setValue(empdata.experiancE_YEARS)
    this.contact.EmpExpMonths.setValue(empdata.experiancE_MONTH)
    this.contact.EmpNationality.setValue(empdata.nationality)
    this.contact.EmpReligion.setValue(empdata.religioN_ID)
    this.contact.EmpCommunity.setValue(empdata.community)
    this.contact.EmpMStatus.setValue(empdata.martiaL_STATUS_ID)
    this.contact.Base64Image.setValue(empdata.emP_PHOTO)
    this.contact.EmpSpouseName.setValue(empdata.spousE_NAME)
    this.contact.EmpADate.setValue(this.datef.parse((empdata.anniversarY_DATE ? empdata.anniversarY_DATE : "").replace("T00:00:00","")))

    this.commu.PDistrict.setValue(empdata.preS_DISTRICT_CODE)
    this.commu.PArea.setValue(empdata.preS_RURAL_URBAN)
    this.commu.PEDistrict.setValue(empdata.perM_DISTRICT_CODE)
    this.commu.PEArea.setValue(empdata.perM_RURAL_URBAN)
    this.commu.NState.setValue(empdata.nativE_STATE_CODE)
    this.commu.NDistrict.setValue(empdata.nativE_DISTRICT_CODE)
    this.commu.NArea.setValue(empdata.nativE_RURAL_URBAN)

    this.commu.nativesameas.setValue("");

    this.DistrictChange('P');
    this.DistrictChange('PE');
    this.DistrictChange('N');

    
    this.commu.PMandal.setValue(empdata.preS_MMC_CODE);
    this.commu.PEMandal.setValue(empdata.perM_MMC_CODE);
    this.commu.NMandal.setValue(empdata.nativE_MMC_CODE);

    this.MandalChange('P');
    this.MandalChange('PE');
    this.MandalChange('N');

    this.commu.PVillage.setValue(empdata.preS_WARD_VILLAGE_CODE);
    this.commu.PEVillage.setValue(empdata.perM_WARD_VILLAGE_CODE);
    this.commu.NVillage.setValue(empdata.nativE_WARD_VILLAGE_CODE);
    
    this.commu.PHNO.setValue(empdata.preS_HOUSE_NO)
    this.commu.PStrName.setValue(empdata.preS_STREET_NAME)
    this.commu.PLandmark.setValue(empdata.preS_LAND_MARK)
    //this.commu.PPincode.setValue((empdata.preS_PINCODE ? empdata.preS_PINCODE.toFixed() : ""))
    this.commu.PasPerAddr.setValue(empdata.iS_PRES_PERM_SAME == 1 ? true : false)
    
    this.commu.PEHNO.setValue(empdata.perM_HOUSE_NO)
    this.commu.PEStrName.setValue(empdata.perM_STREET_NAME)
    this.commu.PELandmark.setValue(empdata.perM_LAND_MARK)
    //this.commu.PEPincode.setValue((empdata.perM_PINCODE ? empdata.perM_PINCODE.toFixed() : ""))

    this.commu.NHNO.setValue(empdata.nativE_HOUSE_NO)
    this.commu.NStrName.setValue(empdata.nativE_STREET_NAME)
    this.commu.NLandmark.setValue(empdata.nativE_LAND_MARK)
    //this.commu.NPincode.setValue((empdata.nativE_PINCODE ? empdata.nativE_PINCODE.toFixed() : ""))

    this.bank.IFSCCode.setValue(empdata.ifsC_CODE)
    this.bank.AccountHName.setValue(empdata.accounT_HOLDER_NAME)
    this.bank.AccountNumber.setValue(empdata.accounT_NUMBER)
    this.bank.ConfirmAccountNumber.setValue(empdata.accounT_NUMBER)
    this.bank.BankName.setValue(empdata.banK_NAME)
    this.bank.BranchName.setValue(empdata.banK_BRANCH)

    this.pf.UANNo.setValue(empdata.uaN_NO)
    this.pf.PFNo.setValue(empdata.pF_NO)
    this.pf.PFData.setValue(this.datef.parse((empdata.pF_DATE ? empdata.pF_DATE : "").replace("T00:00:00",""))) 
    this.pf.PFOffice.setValue(empdata.pF_OFFICE)
    this.pf.PFAccNo.setValue(empdata.pF_AC_NO)
    this.pf.PANNo.setValue(empdata.paN_NO)

  }

  FillEmpFamilyDetails(empdata: any) {

    // this.fam.EMPCODE.setValue(empdata.emP_CODE)
    //this.fam.fM_ID.setValue(empdata.fM_ID)
    for (let i = 0; i < empdata.length; i++) {
      if (i != 0) {
        this.f.push(this.formBuilder.group({
          fname: ['', Validators.required],
          fdob: ['', Validators.required],
          frelation: ['', Validators.required],
          isnominee: [''],
        }));
      }

      this.f.controls[i].patchValue({
        fname: empdata[i].fM_NAME,
        fdob: this.datef.parse(empdata[i].fM_DOB.replace("T00:00:00", "")),
        frelation: empdata[i].fM_RELATION,
        isnominee: (empdata[i].fM_IS_NOMINEE == 'Y' ? '1' : '0')
      });

    }
  }

  onSelectFile(event, ptype: string) {
    let url: string;
    let Phototype = ptype;
    if (event.target.files && event.target.files[0]) {
      let imagetype = event.target.files[0].type;
      let imagesize = event.target.files[0].size;

      if (imagetype != 'image/jpeg' && imagetype != 'image/png') {
        Swal.fire('info', 'Please Upload jpeg,jpg,png files only', 'info');
        return false;
      }

      if (imagesize > 2097152) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]); // read file as data url

      reader.onload = (event) => { // called once readAsDataURL is completed
        if (Phototype == 'photo')
          this.contact.Base64Image.setValue(event.target.result);
      }
    }
  }

  UpdateProfile(): void {
    this.isEdit = true;
    this.uid_isvalid = true;
    this.uidstatus = true;
    this.editModal.show();
  }

  UpdateContactDetails(){
    this.consubmitted = true;
    // stop here if form is invalid
    if (this.employeeContactForm.invalid) {
      return false;
    }

    if(this.contact.EmpMobile.value == this.contact.EmpCMobile.value){     
      Swal.fire('warning', " Mobile Number & Emergency Contact Number Should Not be Equal", 'warning');
      return false;
    }

    if (this.contact.EmpMStatus.value != '1') {
      if (!this.contact.EmpSpouseName.value) {
        Swal.fire('warning', "Please Enter Spouse Name", 'warning');
        return false;
      }
      if (!this.contact.EmpADate.value) {
        Swal.fire('warning', "Please Select Anniversary Date", 'warning');
        return false;
      }
    }

    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.contact.EMPCODE.value;
    req.INPUT_02 = this.contact.EmpEdu.value;
    req.INPUT_03 = this.contact.BloodGroup.value;
    req.INPUT_04 = this.contact.EmpNationality.value;
    req.INPUT_05 = this.contact.EmpReligion.value;
    req.INPUT_06 = this.contact.EmpCommunity.value;
    req.INPUT_07 = this.contact.EmpMStatus.value;
    req.INPUT_08 = this.contact.EmpSpouseName.value;
    req.INPUT_09 = this.datef.format(this.contact.EmpADate.value);
    req.INPUT_35 = this.contact.Base64Image.value;
    req.INPUT_11 = this.contact.EmpEmail.value;
    req.INPUT_12 = this.contact.EmpMobile.value;
    req.INPUT_13 = this.contact.EmpCMobile.value;
    req.USER_NAME =  this.logUsercode;

    this.service.postData(req, "UpdateEmpPrimaryDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
         this.contactcomple =true;
          Swal.fire('success', "Employee General Details Updated Successfully !!!", 'success');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
      Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  SubmitCommuDetails() {

    this.commsubmi = true;
    // stop here if form is invalid

    if(!this.commu.PasPerAddr.value){
      if (!this.commu.PEDistrict.value || !this.commu.PEArea.value || !this.commu.PEMandal.value || !this.commu.PEVillage.value || !this.commu.PEHNO.value || !this.commu.PEStrName.value || !this.commu.PEPincode.value ) {
        return false;
      }
    }

    if(!this.commu.nativesameas.value) {
      if (!this.commu.NState.value || !this.commu.NDistrict.value || !this.commu.NArea.value || !this.commu.NMandal.value || !this.commu.NVillage.value || !this.commu.NHNO.value || !this.commu.NStrName.value || !this.commu.NPincode.value) {
        return false;
      }
    }
    
    if (this.employeeCommuForm.invalid) {
      return false;
    }

    if (!this.contact.EMPCODE.value) {
      Swal.fire('warning', "Please submit general section first", 'warning');
      return false;
    }

    
    
    

    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.contact.EMPCODE.value;
    req.INPUT_02 = this.commu.PDistrict.value;
    req.INPUT_03 = this.commu.PArea.value;
    req.INPUT_04 = this.commu.PMandal.value;
    req.INPUT_05 = this.commu.PVillage.value;
    req.INPUT_06 = this.commu.PHNO.value;
    req.INPUT_07 = this.commu.PStrName.value;
    req.INPUT_08 = this.commu.PLandmark.value;
    req.INPUT_09 = this.commu.PPincode.value;
    req.INPUT_10 = (this.commu.PasPerAddr.value == true ? "1" : "0");

    req.INPUT_11 = (this.commu.PasPerAddr.value == true ? this.commu.PDistrict.value : this.commu.PEDistrict.value);
    req.INPUT_12 = (this.commu.PasPerAddr.value == true ? this.commu.PArea.value : this.commu.PEArea.value);
    req.INPUT_13 = (this.commu.PasPerAddr.value == true ? this.commu.PMandal.value : this.commu.PEMandal.value);
    req.INPUT_14 = (this.commu.PasPerAddr.value == true ? this.commu.PVillage.value : this.commu.PEVillage.value);
    req.INPUT_15 = (this.commu.PasPerAddr.value == true ? this.commu.PHNO.value : this.commu.PEHNO.value);
    req.INPUT_16 = (this.commu.PasPerAddr.value == true ? this.commu.PStrName.value : this.commu.PEStrName.value);
    req.INPUT_17 = (this.commu.PasPerAddr.value == true ? this.commu.PLandmark.value : this.commu.PELandmark.value);
    req.INPUT_18 = (this.commu.PasPerAddr.value == true ? this.commu.PPincode.value : this.commu.PEPincode.value);

    req.INPUT_19 = (this.commu.nativesameas.value == 'present' ? "28" : (this.commu.nativesameas.value == 'permanent' ? "28" : this.commu.NState.value));
    req.INPUT_20 = (this.commu.nativesameas.value == 'present' ? this.commu.PDistrict.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PDistrict.value : this.commu.PEDistrict.value) : this.commu.NDistrict.value));
    req.INPUT_21 = (this.commu.nativesameas.value == 'present' ? this.commu.PArea.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PArea.value : this.commu.PEArea.value) : this.commu.NArea.value));
    req.INPUT_22 = (this.commu.nativesameas.value == 'present' ? this.commu.PMandal.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PMandal.value : this.commu.PEMandal.value) : this.commu.NMandal.value));
    req.INPUT_23 = (this.commu.nativesameas.value == 'present' ? this.commu.PVillage.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PVillage.value : this.commu.PEVillage.value) : this.commu.NVillage.value));
    req.INPUT_24 = (this.commu.nativesameas.value == 'present' ? this.commu.PHNO.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PHNO.value : this.commu.PEHNO.value) : this.commu.NHNO.value));
    req.INPUT_25 = (this.commu.nativesameas.value == 'present' ? this.commu.PStrName.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PStrName.value : this.commu.PEStrName.value) : this.commu.NStrName.value));
    req.INPUT_26 = (this.commu.nativesameas.value == 'present' ? this.commu.PLandmark.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PLandmark.value : this.commu.PELandmark.value) : this.commu.NLandmark.value));
    req.INPUT_27 = (this.commu.nativesameas.value == 'present' ? this.commu.PPincode.value : (this.commu.nativesameas.value == 'permanent' ? (this.commu.PasPerAddr.value == true ? this.commu.PPincode.value : this.commu.PEPincode.value) : this.commu.NPincode.value));
    req.INPUT_28 = this.commu.nativesameas.value;
    req.USER_NAME =  this.logUsercode;

    this.service.postData(req, "SaveEmpCommuDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.commcomple = true;
          Swal.fire('success', "Employee Communication Details Saved Successfully !!!", 'success');
        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        ;
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  SubmitBankDetails() {
    this.banksubmi = true;
    // stop here if form is invalid
    if (this.employeeBankForm.invalid) {
      return false;
    }

    if (!this.contact.EMPCODE.value) {
      Swal.fire('warning', "Please submit general section first", 'warning');
      return false;
    }

    if(this.bank.AccountNumber.value.length != this.bank.AccLength.value){
      Swal.fire('warning', this.bank.AccountHName.value + " Account Number Shoud be " + this.bank.AccLength.value + " digits", 'warning');
      return false;
    }

    this.showloader = true;

    const req = new InputRequest();
      req.INPUT_01 = this.contact.EMPCODE.value;
      req.INPUT_02 = this.bank.IFSCCode.value;
      req.INPUT_03 = this.bank.AccountHName.value;
      req.INPUT_04 = this.bank.AccountNumber.value;
      req.INPUT_05 = this.bank.BankName.value;
      req.INPUT_06 = this.bank.BranchName.value;
      req.USER_NAME =  this.logUsercode;

    this.service.postData(req, "SaveEmpBankDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.bankcomple = true;
          Swal.fire('success', "Employee Bank Details Saved Successfully !!!", 'success');
        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        
      }
      else
      Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  SubmitFamilyDetails() {
    this.familysubmi = true;

    let isnomineeselect: boolean = false
    let age: number = 0;

    // stop here if form is invalid
    if (this.employeeFamilyForm.invalid) {
      return false;
    }

    for (let i = 0; i < this.f.length; i++) {
      if (this.f.value[i].isnominee) {
        isnomineeselect = true;
        age = this.datef.getage(this.f.value[i].fdob)
      }
    }

    if (!isnomineeselect) {
      Swal.fire('warning', "Please Select Atleast one person as Nominee", 'warning');
      return false;
    }
    if (age < 18) {
      Swal.fire('warning', "Nominee age should be 18 years or above", 'warning');
      return false;
    }

    if (!this.contact.EMPCODE.value) {
      Swal.fire('warning', "Please submit general section first", 'warning');
      return false;
    }

    this.showloader = true;

    let idslist: string[] = [];
    let fnamelist: string[] = [];
    let fdoblist: string[] = [];
    let frellist: string[] = [];
    let fnomineelist: string[] = [];

    for (let i = 0; i < this.f.length; i++) {
      idslist.push((i + 1).toString());
      fnamelist.push(this.f.value[i].fname);
      fdoblist.push( this.datef.format(this.f.value[i].fdob));
      frellist.push(this.f.value[i].frelation);
      fnomineelist.push((this.f.value[i].isnominee == "1" ? "1" : "0"));
    }

    const req = new InputRequest();
    req.INPUT_01 = this.contact.EMPCODE.value;
    req.INPUT_02 = idslist.join(',');
    req.INPUT_03 = fnamelist.join(',');
    req.INPUT_04 = fdoblist.join(',');
    req.INPUT_05 = frellist.join(',');
    req.INPUT_06 = fnomineelist.join(',');
    req.USER_NAME =  this.logUsercode;

    this.service.postData(req, "SaveEmpFamilyDetails").subscribe(data => {
      this.showloader = false;
  
      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.familycomple = true;
          Swal.fire('success', "Employee Family Details Saved Successfully !!!", 'success');
        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');
     
      }
      else
      Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  SubmitPFDetails() {
    this.pfsubmi = true;
    // stop here if form is invalid
    if (this.employeePFForm.invalid) {
      return false;
    }

    if (!this.contact.EMPCODE.value) {
      Swal.fire('warning', "Please submit general section first", 'warning');
      return false;
    }

    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = this.contact.EMPCODE.value;
    req.INPUT_02 = this.pf.UANNo.value;
    req.INPUT_03 = this.pf.PFNo.value;
    req.INPUT_04 = this.datef.format(this.pf.PFData.value);
    req.INPUT_05 = this.pf.PFOffice.value;
    req.INPUT_06 = this.pf.PFAccNo.value;
    req.INPUT_07 = this.pf.PANNo.value;
    req.USER_NAME =  this.logUsercode;

    this.service.postData(req, "SaveEmpPFDetails").subscribe(data => {
      this.showloader = false;
  
      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.pfcomple = true;
          Swal.fire('success', "Employee Providend Fund Details Saved Successfully !!!", 'success');
          this.editModal.hide();
        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');
 
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }


  GetIFSCDetails() {
    if (this.bank.IFSCCode.value.length == 11) {
      this.showloader = true;
      var data = { 'INPUT_01': this.bank.IFSCCode.value };
      this.service.postData(data, "GetIFSCCodeDetails").subscribe(data => {
        this.showloader = false;

        if (data.StatusCode == "100") {
          let result = data.Details[0];

          this.bank.BankName.setValue(result.banK_NAME);
          this.bank.BranchName.setValue(result.branch);
          this.bank.AccLength.setValue(result.aC_LEANTH);

        }
        else
        Swal.fire('warning', data.StatusMessage, 'warning');

      },
        error => console.log(error));
    }

  }

  LoadEmployeeTypes() {
    this.service.getData("GetEmployeeTypes").subscribe(data => {
      if (data.StatusCode == "100") {
        this.emptypes = data.Details;
      }
    },
      error => console.log(error));
  }

  LoadGenders() {
    this.service.getData("GetGenders").subscribe(data => {
     
      if (data.StatusCode == "100") {
        this.genders = data.Details;
        console.log(this.genders);
      }

    },
      error => console.log(error));
  }

  LoadSections() {
    this.service.getData("GetSections").subscribe(data => {

      if (data.StatusCode == "100") {
        this.sections = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadEducations() {
    this.service.getData("GetEducations").subscribe(data => {

      if (data.StatusCode == "100") {
        this.educations = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadBloodGroups() {
    this.service.getData("GetBloodGroups").subscribe(data => {

      if (data.StatusCode == "100") {
        this.bloodgroups = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadEmpExpYears() {
    this.service.getData("GetExperianceYears").subscribe(data => {
      
      if (data.StatusCode == "100") {
        this.expyears = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadEmpExpMonths() {
    this.service.getData("GetExperianceMonths").subscribe(data => {

      if (data.StatusCode == "100") {
        this.expmonths = data.Details;
       
      }

    },
      error => console.log(error));
  }

  LoadNationality() {
    // this.service.getData("GetNationality").subscribe(data => {
    //   console.log(data);
    //   if (data.StatusCode == "100") {
    //     this.nationalites = data.Details;
    //     console.log(this.nationalites);
    //   }

    // },
    //   error => console.log(error));
  }

  LoadReligions() {
    this.service.getData("GetReligions").subscribe(data => {

      if (data.StatusCode == "100") {
        this.religions = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadCommunities() {
    this.service.getData("GetCommunities").subscribe(data => {

      if (data.StatusCode == "100") {
        this.communities = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadMaritalStatus() {
    this.service.getData("GetMaritalStatus").subscribe(data => {

      if (data.StatusCode == "100") {
        this.mstatus = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadDesignations() {
    this.service.getData("GetDesignations").subscribe(data => {

      if (data.StatusCode == "100") {
        this.desinations = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadDistricts() {
    this.service.getData("GetDistricts").subscribe(data => {

      if (data.StatusCode == "100") {
        this.districts = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadurFlags() {
    this.service.getData("GetAreaTypes").subscribe(data => {

      if (data.StatusCode == "100") {
        this.urlist = data.Details;

      }

    },
      error => console.log(error));
  }

  DistrictChange(type: string) {
    let typevlaue: string = type;
    if (typevlaue == 'P') {
      this.pmandals = [];
      this.pvillages = [];
      this.commu.PMandal.setValue("");
      this.commu.PVillage.setValue("");
      if (this.commu.PDistrict.value && this.commu.PArea.value) {
        this.LoadMandals(typevlaue, this.commu.PDistrict.value, this.commu.PArea.value)
      }
    }
    else if (typevlaue == 'PE') {
      this.pemandals = [];
      this.pevillages = [];
      this.commu.PEMandal.setValue("");
      this.commu.PEVillage.setValue("");
      if (this.commu.PEDistrict.value && this.commu.PEArea.value) {
        this.LoadMandals(typevlaue, this.commu.PEDistrict.value, this.commu.PEArea.value)
      }
    }
    else if (typevlaue == 'N') {
      this.nmandals = [];
      this.nvillages = [];
      this.commu.NMandal.setValue("");
      this.commu.NVillage.setValue("");
      if (this.commu.NDistrict.value && this.commu.NArea.value) {
        this.LoadMandals(typevlaue, this.commu.NDistrict.value, this.commu.NArea.value)
      }
    }

  }

  LoadMandals(type: string, distval: string, areaval: string) {
    this.showloader = true;
    let typevlaue: string = type;
    const req = new InputRequest();
    req.INPUT_01 = areaval;
    req.INPUT_02 = distval;
    
    this.service.postData(req, "GetMandlas").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (typevlaue == 'P')
          this.pmandals = data.Details;
        else if (typevlaue == 'PE')
          this.pemandals = data.Details;
        else if (typevlaue == 'N')
          this.nmandals = data.Details;
      }

    },
      error => console.log(error));
  }

  MandalChange(type: string) {
    let typevlaue: string = type;

    if (typevlaue == 'P') {
      this.pvillages = [];
      this.commu.PVillage.setValue("");
      if (this.commu.PDistrict.value && this.commu.PMandal.value) {
        this.LoadVillages(typevlaue, this.commu.PDistrict.value, this.commu.PMandal.value)
      }
    }
    else if (typevlaue == 'PE') {
      this.pevillages = [];
      this.commu.PEVillage.setValue("");
      if (this.commu.PEDistrict.value && this.commu.PEMandal.value) {
        this.LoadVillages(typevlaue, this.commu.PEDistrict.value, this.commu.PEMandal.value)
      }
    }
    else if (typevlaue == 'N') {
      this.nvillages = [];
      this.commu.NVillage.setValue("");
      if (this.commu.NDistrict.value && this.commu.NMandal.value) {
        this.LoadVillages(typevlaue, this.commu.NDistrict.value, this.commu.NMandal.value)
      }
    }

  }

  LoadVillages(type: string, distval: string, manval: string) {
    this.showloader = true;
    let typevlaue: string = type;
    let obj: any = { "INPUT_02": distval, "INPUT_03": manval }

    this.service.postData(obj, "GetVillages").subscribe(data => {
      this.showloader = false;
      
      if (data.StatusCode == "100") {
        if (typevlaue == 'P')
          this.pvillages = data.Details;
        else if (typevlaue == 'PE')
          this.pevillages = data.Details;
        else if (typevlaue == 'N')
          this.nvillages = data.Details;



      }

    },
      error => console.log(error));
  }

  LoadStates() {
    this.service.getData("GetStates").subscribe(data => {

      if (data.StatusCode == "100") {
        this.states = data.Details;

      }

    },
      error => console.log(error));
  }

  PresentAsPerminentChange() {
    this.hideperaadr = !this.commu.PasPerAddr.value;

    if(this.commu.PasPerAddr.value){

    }
    else{

    }
    
  }

  ResetPerminentDetails() {
    
  }

  LoadRelations() {
    this.service.getData("GetRelations").subscribe(data => {
      if (data.StatusCode == "100") {
        this.relations = data.Details;
      }

    },
      error => console.log(error));
  }

}
