import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';

import Swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';

import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { AgGridAngular } from 'ag-grid-angular';
import { sreventsRendererComponent } from 'src/app/custome-directives/srevents-renderer.component';
//import { EMPVIEWSALBUTTONRendererComponent } from 'src/app/custome-directives/Empviewsalbutton-renderer.component';
import { srempdetailsRendererComponent } from 'src/app/custome-directives/srempdetails-renderer.component';
import { EmployeeAnnualPropertie } from 'src/app/Interfaces/user';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'

@Component({
  selector: 'app-service-details',
  templateUrl: './service-details.component.html',
  styleUrls: ['./service-details.component.css']
})
export class ServiceDetailsComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");

  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('historySection') public historySection: ModalDirective;
  @ViewChild('historyAllSection') public historyAllSection: ModalDirective;


  @ViewChild('emptransfermodal') public emptransfermodal: ModalDirective;
  @ViewChild('suspentionmodal') public suspentionmodal: ModalDirective;
  @ViewChild('stopincrementmodal') public stopincrementmodal: ModalDirective;
  @ViewChild('Reinstatingdutiesmodal') public Reinstatingdutiesmodal: ModalDirective;
  @ViewChild('promotionmodal') public promotionmodal: ModalDirective;
  @ViewChild('additionalchargemodal') public additionalchargemodal: ModalDirective;
  @ViewChild('empincrementmodal') public empincrementmodal: ModalDirective;
  @ViewChild('Releavingadditionalchargemodal') public Reladditionalchargemodal: ModalDirective;
  @ViewChild('Employeeprofile') public Employeeprofilemodal: ModalDirective;
  @ViewChild('propertiemodal') public propertiemodal: ModalDirective;


  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('agGrid2') agGrid2: AgGridAngular;


  showloader: boolean = true;
  ismodal: boolean = false;
  modeltitle: string = "Add";
  locationtypeid: string = "";
  empkeyword: string = "emP_NAME";
  GridFillRowData: any[];
  EmployeeAnnualPropertie: EmployeeAnnualPropertie;
  columnDefs = [
    { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Employee Name', maxWidth: 200, field: 'emP_NAME',
      cellRenderer: "EmpdataGet",
      sortable: true, filter: true
    },
    { headerName: 'Login Code', maxWidth: 150, field: 'emP_CODE', sortable: true, filter: true },
    { headerName: 'Employee Type', maxWidth: 150, field: 'servicE_TYPE', sortable: true, filter: true },
    { headerName: 'Designation', maxWidth: 200, field: 'designation', sortable: true, filter: true },
    { headerName: 'Mobile No', maxWidth: 120, field: 'mobilE_NUMBER', sortable: true, filter: true },
    {
      headerName: 'Action', maxWidth: 400, cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        btnEdit: 'SR',
        btnView: 'View',
        btnHistory: 'History',
        viewClick: this.ViewEmployee.bind(this),
        //viewClick: this.ViewEmployee.bind(this),
        //historyClick: this.HistoryEmployee.bind(this),

      },

    }
  ];

  columnDefs_events = [
    { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Proceeding No', maxWidth: 100, field: 'procedinG_NO', sortable: true, filter: true },
    { headerName: 'Event Type', maxWidth: 150, field: 'evenT_TYPE', sortable: true, filter: true },
    { headerName: 'Event Discription', width: 700, field: 'evenT_DESCRIPTION', sortable: true, filter: true, wrapText: true, autoHeight: true },
    {
      headerName: 'Action', maxWidth: 120, cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        // btnEdit: 'SR',
        // btnView: 'View',
        // btnHistory: 'History',
        viewClick: this.EditEmployee.bind(this),
        //viewClick: this.ViewEmployee.bind(this),
        //historyClick: this.HistoryEmployee.bind(this),

      },
    }
  ];





  EmployeerowData = [];
  EmployeerowData_Events = [];

  employee: any;
  employees: any[];
  uidstatus: boolean = true;
  uid_isvalid: boolean = true;
  Emp_mob_isvalid = true;
  Emp_Emergencymob_isvalid = true;
  PanPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  IFSCPattern = "^[A-Z]{4}0[A-Z0-9]{6}$";


  NgbDateStruct: any;
  public isEdit: boolean = false;
  public isAdmin: boolean = false;
  public consubmitted: boolean = false;


  public screen1: boolean = true; //emp details
  public screen2: boolean = false; // selected emp name
  public screen3: boolean = false; // saved data
  public screen4: boolean = false; // events


  public backscreen: boolean = false;


  public EmpName: string = "";
  public empid: string = "";
  public service_name: string = "";


  public eventname: string = "";


  rowData: any = [];
  defaultColDef: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];

  gridColumnApi_event: any = [];
  gridApi_event: any = [];


  leavetypes: any = [];


  frameworkComponents: any;
  public components;

  frameworkComponents_events: any;
  public components_events;

  allemphistory: any = [];
  emphistory: any = [];
  progress: number = 0;
  message: string = "";

  vaadhaar: string;
  vempcode: string;
  vempid: string;
  vemptype: string;
  vempname: string;
  vdesignation: string;
  vgender: string;
  vdob: string;
  vdoj: string;
  veducation: string;
  vbloodgrp: string;
  vemail: string;
  vmobile: string;
  vecno: string;
  vexpears: string;
  vexmonths: string;
  vnationality: string;
  vreligion: string;
  vcommunity: string;
  vmstatus: string;
  vspouname: string;
  vanaadate: string;
  vempphoto: string;
  vaadhaarphoto: string;

  vpdistrict: string;
  vpru: string;
  vpmandal: string;
  vpvillage: string;
  vphno: string;
  vpstreett: string;
  vplanmark: string;
  vppincode: string;

  vpedistrict: string;
  vperu: string;
  vpemandal: string;
  vpevillage: string;
  vpehno: string;
  vpestreett: string;
  vpelanmark: string;
  vpepincode: string;

  vnstate: string;
  vndistrict: string;
  vnru: string;
  vnmandal: string;
  vnvillage: string;
  vnhno: string;
  vnstreett: string;
  vnlanmark: string;
  vnpincode: string;

  vloctype: string;
  vlocname: string;
  vsectionname: string;
  vrmanager: string;
  vastatus: string;

  vifsccode: string;
  vaccname: string;
  vaccno: string;
  vbankname: string;
  vbranch: string;

  familylist: any = [];

  vuanno: string;
  vpfno: string;
  vpfdate: string;
  vpfoffice: string;
  vpfaccno: string;
  vpanno: string;

  V_identification1: string;
  V_identification2: string;
  V_Mothertongue: string;
  V_Height: string;
  V_Nativeplace: string;


  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private datef: CustomDateParserFormatter,
    private datecon: NgbDatepickerConfig, private sanitizer: DomSanitizer
  ) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }

    if (this.logUserrole == "101" || this.logUserrole == "102")
      this.isAdmin = true;
    this.frameworkComponents = {
      buttonRenderer: srempdetailsRendererComponent,

    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
      EmpdataGet: function (params) {

        return '<u style="color:blue">' + params.value + '</u>';

      },
    };


    this.frameworkComponents_events = {
      buttonRenderer: sreventsRendererComponent,

    }

    this.components_events = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }




  //this.datecon.maxDate = new NgbDateStruct({year:2021,month:4:day:2}) 

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //params.api.setRowData(this.EmployeerowData);
    //this.gridApi.sizeColumnsToFit();
    this.LoadEmployeeList();
  }
  dataclicked(params) {
    if (params.colDef.cellRenderer == "EmpdataGet") {
      this.EditEmployee(params);
    }

  }

  BindData_Events(params) {
    this.gridApi_event = params.api;
    this.gridColumnApi_event = params.columnApi;
    this.ServiceEvents_Get();
    //params.api.setRowData(this.EmployeerowData_Events);
    //this.gridApi.setRowData(this.EmployeerowData);
  }

  back() {

    this.screen1 = true;
    this.screen2 = false;
    this.screen3 = false;
    this.screen4 = false;

    this.eventname = "";





  }



  emptransferform: FormGroup;

  get subf() { return this.emptransferform.controls; }

  get emptransfer() { return this.subf.emptransferservicesreg as FormArray; }


  empsuspentionform: FormGroup;

  get subsuspention() { return this.empsuspentionform.controls; }

  get suspention() { return this.subsuspention.empsuspentionformservicesreg as FormArray; }


  empstopincform: FormGroup;

  get subempstopincform() { return this.empstopincform.controls; }

  get stopincrement() { return this.subempstopincform.empstopincformservicesreg as FormArray; }


  empdutiesform: FormGroup;

  get subempdutiesform() { return this.empdutiesform.controls; }

  get Reinstatingduties() { return this.subempdutiesform.empdutiesformservicesreg as FormArray; }




  emppromotionform: FormGroup;

  get subemppromotionform() { return this.emppromotionform.controls; }

  get promotion() { return this.subemppromotionform.emppromotionformservicesreg as FormArray; }


  empadditionalchargeform: FormGroup;

  get subempadditionalchargeform() { return this.empadditionalchargeform.controls; }

  get additionalcharge() { return this.subempadditionalchargeform.empadditionalchargeformservicesreg as FormArray; }


  Relempadditionalchargeform: FormGroup;

  get Relsubempadditionalchargeform() { return this.Relempadditionalchargeform.controls; }

  get Reladditionalcharge() { return this.Relsubempadditionalchargeform.Relempadditionalchargeformservicesreg as FormArray; }


  empincrementform: FormGroup;

  get subempincrementform() { return this.empincrementform.controls; }

  get empincrement() { return this.subempincrementform.empincrementformformservicesreg as FormArray; }



  emppropertieform: FormGroup;

  get subemppropertieform() { return this.emppropertieform.controls; }

  get annualpropertie() { return this.subemppropertieform.emppropertieformformservicesreg as FormArray; }


  ngOnInit(): void {


    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() };

    this.emptransferform = this.formBuilder.group({
      emptransferservicesreg: new FormArray([
        this.formBuilder.group({
          PROCEDING_NO: ['', Validators.required],
          PROCEDING_DATE: [null, Validators.required],
          TRANSFER_DESIGNATION: [null, Validators.required],
          TRANSFER_LOCATION: [null, Validators.required],
          RELEAVING_LOCATION: [null, Validators.required],
          RELEAVING_DATE: [null, Validators.required],
          REPORTING_LOCATION: [null, Validators.required],
          REPORTING_DATE: [null, Validators.required],



        })
      ])

    });



    this.empsuspentionform = this.formBuilder.group({
      empsuspentionformservicesreg: new FormArray([
        this.formBuilder.group({
          SUS_PROCEDING_NO: ['', Validators.required],
          SUS_PROCEDING_DATE: [null, Validators.required],
          Suspendtion_Date: [null, Validators.required],
          SUSPENTION_LOCATION: [null, Validators.required],
          Reason: [null, Validators.required]
        })
      ])




    });


    this.empstopincform = this.formBuilder.group({
      empstopincformservicesreg: new FormArray([
        this.formBuilder.group({
          INC_PROCEEDING_NO: ['', Validators.required],
          INC_PROCEEDING_DATE: [null, Validators.required],
          //Inc_Stop_Reason: [null, Validators.required],
          STOP_EMP_PUNISHMENT: [null, Validators.required],

        })
      ])




    });



    this.empdutiesform = this.formBuilder.group({
      empdutiesformservicesreg: new FormArray([
        this.formBuilder.group({
          duties_ProceedingNo: ['', Validators.required],
          duties_PROCEDING_DATE: [null, Validators.required],
          duties_DATE: [null, Validators.required],
          DUTIES_REPORTING_LOCATION: [null, Validators.required],
          duties_Reporting_DATE: [null, Validators.required],

          duties_Reason: [null, Validators.required],

        })
      ])




    });


    this.emppromotionform = this.formBuilder.group({
      emppromotionformservicesreg: new FormArray([
        this.formBuilder.group({
          PROMOTION_PROCEEDING_NO: ['', Validators.required],
          PROMOTION_PROCEEDING_DATE: [null, Validators.required],
          TRANSFER_EMP_DESIGNATION: [null, Validators.required],
          PROMOTION_FROM_AMOUNT: [null, Validators.required],
          PROMOTION_TO_AMOUNT: [null, Validators.required],
          PROMOTION_RULE: [null, Validators.required],
          PROMOTION_AMOUNT: [null, Validators.required],
          PROMOTION_DATE: [null, Validators.required],
          //STOP_REASON: [null, Validators.required],
          //STOP_PROCNO: [null, Validators.required],
          //STOP_PROCDATE: [null, Validators.required],
          //NEXT_PROCDATE: [null, Validators.required],


        })
      ])

    });


    this.empadditionalchargeform = this.formBuilder.group({
      empadditionalchargeformservicesreg: new FormArray([
        this.formBuilder.group({
          ADD_PROCEEDING_NO: ['', Validators.required],
          ADD_PROCEEDING_DATE: [null, Validators.required],
          ADD_EMP_DESIGNATION: [null, Validators.required],
          ADD_EMP_Location: [null, Validators.required],
          ADD_EMP_OfficeLocation: [null, Validators.required],
          Add_FromDate: [null, Validators.required]


        })
      ])

    });


    this.Relempadditionalchargeform = this.formBuilder.group({
      Relempadditionalchargeformservicesreg: new FormArray([
        this.formBuilder.group({
          Rel_PROCEEDING_NO: ['', Validators.required],
          Rel_PROCEEDING_DATE: [null, Validators.required],
          Rel_EMP_DESIGNATION: [null, Validators.required],
          Rel_EMP_Location: [null, Validators.required],
          //Rel_EMP_OfficeLocation: [null, Validators.required],
          Rel_FromDate: [null, Validators.required]


        })
      ])

    });

    this.empincrementform = this.formBuilder.group({
      empincrementformformservicesreg: new FormArray([
        this.formBuilder.group({
          INC_PAYSCLAE_FROM: ['', Validators.required],
          INC_PAYSCLAE_TO: [null, Validators.required],
          INC_PAYSCLAE_SANC_RS: [null, Validators.required],
          Inc_PROCEEDING_NO: [null, Validators.required],
          Inc_ProcDate: [null, Validators.required]


        })
      ])

    });


    this.emppropertieform = this.formBuilder.group({
      emppropertieformformservicesreg: new FormArray([
        this.formBuilder.group({
          propertie_PROCEEDING_NO: ['', Validators.required],
          propertie_PROCEEDING_DATE: [null, Validators.required],
          propertie_year: [null, Validators.required],
          propertie_doc_NO: [null, Validators.required],
          propertieDOC: [null, Validators.required],
          propertieDOCnumber: [null, Validators.required],
          progress: [0],
          message: [''],

        })
      ])

    });


    this.LoadDesignations();
    this.LoadLocationTypes();


  }

  desinations: any = [];
  worklocations: any = [];




  LoadLocationTypes() {

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "LOCATION";


    this.service.postData(req, "SR_GETALLLOCATIONS").subscribe(data => {
      if (data.StatusCode == "100") {
        this.worklocations = data.Details;
      }

    },
      error => console.log(error));
  }
  years: any[];
  LoadYear() {

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "GET_ANNUAL_YEAR";
    req.INPUT_01 = this.empid;

    this.service.postData(req, "SERVICE_EVENTS_GET").subscribe(data => {
      if (data.StatusCode == "100") {
        this.years = data.Details;

      }

    },
      error => console.log(error));
  }
  LoadDesignations() {

    this.service.getData("GetDesignations").subscribe(data => {

      if (data.StatusCode == "100") {
        this.desinations = data.Details;

      }

    },
      error => console.log(error));
  }

  EditEmpLeaveMaster() {

  }
  ViewEmpLeave() {

  }
  HistoryLeave() {

  }

  eventid: string;

  DetailsGet(value) {
    if (value == "1") {

      this.resetallforms();

      this.Addtransfer();

      this.emptransfermodal.show();
    }
    if (value == "2") {

      this.resetallforms();

      this.AddSuspention();

      this.suspentionmodal.show();
    }
    if (value == "3") {
      this.resetallforms();

      this.AddAnnulInc();

      this.stopincrementmodal.show();
    }
    if (value == "4") {
      this.resetallforms();

      this.AddDuties();
      this.Reinstatingdutiesmodal.show();
    }
    if (value == "5") {
      this.resetallforms();

      this.AddPromotion();
      this.promotionmodal.show();
    }
    if (value == "6") {
      this.resetallforms();

      this.AddCharge();
      this.additionalchargemodal.show();
    }
    if (value == "8") {
      this.resetallforms();

      this.AddReleaving();
      this.Reladditionalchargemodal.show();
    }
    if (value == "7") {
      this.resetallforms();

      this.AddIncrement();
      this.empincrementmodal.show();
    }
    if (value == "9") {
      this.resetallforms();

      this.AddPropertie();
      this.propertiemodal.show();
    }
  }
  CloseViewModal() {
    this.viewModal.hide();
  }
  uploadFile(event, i) {
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;

      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF') {
        Swal.fire('info', 'Please upload pdf files only', 'info');
        return false;
      }

      if (filesize > 10615705) {
        Swal.fire('info', 'File size must be upto 10 MB', 'info');
        return false;
      }

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', "Employee_AnnualPropertie");

      this.service.UploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            //this.progress = Math.round(100 * event.loaded / event.total);
            this.annualpropertie.controls[i].patchValue({ progress: Math.round(100 * event.loaded / event.total) });

          else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.';
            //this.uploadFinished(event.body, "EMP_TDOC");
            //this.annualpropertie.value[i].propertieDOCnumber=event.body.fullPath;
            this.annualpropertie.controls[i].patchValue({ message: 'Upload success.' });

            this.annualpropertie.controls[i].patchValue({ propertieDOCnumber: event.body.fullPath });


          }

        });


    }
  };

  AddPropertie() {

    this.annualpropertie.push(
      this.formBuilder.group({
        propertie_PROCEEDING_NO: ['', Validators.required],
        propertie_PROCEEDING_DATE: [null, Validators.required],
        propertie_year: [null, Validators.required],
        propertie_doc_NO: [null, Validators.required],
        propertieDOC: [null, Validators.required],
        propertieDOCnumber: [null, Validators.required],
        progress: [0],
        message: [''],

      }));


  };

  RemovePropertie(index) { this.annualpropertie.removeAt(index); }


  SubmitPropertie() {
    this.consubmitted = true;

    // stop here if form is invalid
    if (this.annualpropertie.invalid) {
      return false;
    }

    // for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
    //   if (this.datef.GreaterDate(this.EmpLeaveCtrl.value[i].startdate,this.EmpLeaveCtrl.value[i].enddate)) {
    //     Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
    //     return false;
    //   }
    // } 


    this.showloader = true;


    let propertie_PROCEEDING_NOlist: string[] = [];
    let propertie_PROCEEDING_DATElist: string[] = [];
    let propertie_yearlist: string[] = [];
    let propertie_doc_NOlist: string[] = [];
    let propertieDOCnumberlist: string[] = [];
    let empcodelist: string[] = [];

    for (let i = 0; i < this.annualpropertie.length; i++) {
      propertie_PROCEEDING_NOlist.push(this.annualpropertie.value[i].propertie_PROCEEDING_NO);
      propertie_PROCEEDING_DATElist.push(this.datef.format(this.annualpropertie.value[i].propertie_PROCEEDING_DATE));
      propertie_yearlist.push(this.annualpropertie.value[i].propertie_year);
      propertie_doc_NOlist.push(this.annualpropertie.value[i].propertie_doc_NO);
      propertieDOCnumberlist.push(this.annualpropertie.value[i].propertieDOCnumber);
      empcodelist.push(this.empid);

    }

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "210";

    req.INPUT_01 = empcodelist.join(',');
    req.INPUT_02 = propertie_PROCEEDING_NOlist.join(',');
    req.INPUT_03 = propertie_PROCEEDING_DATElist.join(',');
    req.INPUT_06 = propertie_yearlist.join(',');
    req.INPUT_04 = propertie_doc_NOlist.join(',');
    req.INPUT_05 = propertieDOCnumberlist.join(',');

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "Web";

    req.INPUT_35 = "Error Occured while SR Emplyoee Annual Propertie Insert Details";


    this.service.postData(req, "SR_Common_Insert_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Employee Annual Propertie Saved Successfully !!!", 'success');

          this.resetallforms();

          this.ServiceEvents_Get();


        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }
  Addtransfer() {

    this.emptransfer.push(
      this.formBuilder.group({
        PROCEDING_NO: ['', Validators.required],
        PROCEDING_DATE: [null, Validators.required],
        TRANSFER_DESIGNATION: [null, Validators.required],
        TRANSFER_LOCATION: [null, Validators.required],
        RELEAVING_LOCATION: [null, Validators.required],
        RELEAVING_DATE: [null, Validators.required],
        REPORTING_LOCATION: [null, Validators.required],
        REPORTING_DATE: [null, Validators.required],
      }));


  };
  Removetransfer(index) { this.emptransfer.removeAt(index); }
  SubmitEmpTransfer() {


    this.consubmitted = true;

    // stop here if form is invalid
    if (this.emptransfer.invalid) {
      return false;
    }

    // for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
    //   if (this.datef.GreaterDate(this.EmpLeaveCtrl.value[i].startdate,this.EmpLeaveCtrl.value[i].enddate)) {
    //     Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
    //     return false;
    //   }
    // } 


    this.showloader = true;


    let PROCEDING_NOlist: string[] = [];
    let PROCEDING_DATElist: string[] = [];
    let TRANSFER_DESIGNATIONlist: string[] = [];
    let TRANSFER_LOCATIONlist: string[] = [];
    let RELEAVING_LOCATIONlist: string[] = [];
    let RELEAVING_DATElist: string[] = [];
    let REPORTING_LOCATIONlist: string[] = [];
    let REPORTING_DATElist: string[] = [];
    let empcodelist: string[] = [];

    for (let i = 0; i < this.emptransfer.length; i++) {
      PROCEDING_NOlist.push(this.emptransfer.value[i].PROCEDING_NO);
      PROCEDING_DATElist.push(this.datef.format(this.emptransfer.value[i].PROCEDING_DATE));
      TRANSFER_DESIGNATIONlist.push(this.emptransfer.value[i].TRANSFER_DESIGNATION);
      TRANSFER_LOCATIONlist.push(this.emptransfer.value[i].TRANSFER_LOCATION);
      RELEAVING_LOCATIONlist.push(this.emptransfer.value[i].RELEAVING_LOCATION);
      RELEAVING_DATElist.push(this.datef.format(this.emptransfer.value[i].RELEAVING_DATE));
      REPORTING_LOCATIONlist.push(this.emptransfer.value[i].REPORTING_LOCATION);
      REPORTING_DATElist.push(this.datef.format(this.emptransfer.value[i].REPORTING_DATE));
      empcodelist.push(this.empid);

    }

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "203";

    req.INPUT_01 = empcodelist.join(',');
    req.INPUT_02 = PROCEDING_NOlist.join(',');
    req.INPUT_03 = PROCEDING_DATElist.join(',');
    req.INPUT_04 = TRANSFER_DESIGNATIONlist.join(',');
    req.INPUT_05 = TRANSFER_LOCATIONlist.join(',');
    req.INPUT_06 = RELEAVING_LOCATIONlist.join(',');
    req.INPUT_07 = RELEAVING_DATElist.join(',');
    req.INPUT_08 = REPORTING_LOCATIONlist.join(',');
    req.INPUT_09 = REPORTING_DATElist.join(',');
    req.INPUT_10 = "";
    req.INPUT_11 = "Pending";

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "Web";

    req.INPUT_35 = "Error Occured while SR Emplyoee Transfer Insert Details";


    this.service.postData(req, "SR_Common_Insert_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Employee Transfer Details Saved Successfully !!!", 'success');
          this.resetallforms();
          this.ServiceEvents_Get();


        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }


  AddSuspention() {

    this.suspention.push(
      this.formBuilder.group({
        SUS_PROCEDING_NO: ['', Validators.required],
        SUS_PROCEDING_DATE: [null, Validators.required],
        Suspendtion_Date: [null, Validators.required],
        SUSPENTION_LOCATION: [null, Validators.required],
        Reason: [null, Validators.required]
      }));


  };

  RemoveSuspention(index) { this.suspention.removeAt(index); }
  SubmitSuspention() {


    this.consubmitted = true;

    // stop here if form is invalid
    if (this.suspention.invalid) {
      return false;
    }

    // for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
    //   if (this.datef.GreaterDate(this.EmpLeaveCtrl.value[i].startdate,this.EmpLeaveCtrl.value[i].enddate)) {
    //     Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
    //     return false;
    //   }
    // } 


    this.showloader = true;


    let SUS_PROCEDING_NOlist: string[] = [];
    let SUS_PROCEDING_DATElist: string[] = [];
    let Suspendtion_Datelist: string[] = [];
    let SUSPENTION_LOCATIONlist: string[] = [];
    let Reasonlist: string[] = [];
    let empcodelist: string[] = [];

    for (let i = 0; i < this.suspention.length; i++) {
      SUS_PROCEDING_NOlist.push(this.suspention.value[i].SUS_PROCEDING_NO);
      SUS_PROCEDING_DATElist.push(this.datef.format(this.suspention.value[i].SUS_PROCEDING_DATE));
      Suspendtion_Datelist.push(this.datef.format(this.suspention.value[i].Suspendtion_Date));
      SUSPENTION_LOCATIONlist.push(this.suspention.value[i].SUSPENTION_LOCATION);
      Reasonlist.push(this.suspention.value[i].Reason);
      empcodelist.push(this.empid);

    }

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "204";

    req.INPUT_01 = empcodelist.join(',');
    req.INPUT_02 = SUS_PROCEDING_NOlist.join(',');
    req.INPUT_03 = SUS_PROCEDING_DATElist.join(',');
    req.INPUT_04 = Suspendtion_Datelist.join(',');
    req.INPUT_05 = SUSPENTION_LOCATIONlist.join(',');
    req.INPUT_06 = Reasonlist.join(',');
    //req.INPUT_07 = "Pending";

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "Web";

    req.INPUT_35 = "Error Occured while SR Emplyoee Suspention Insert Details";

    this.service.postData(req, "SR_Common_Insert_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Employee Suspention Details Saved Successfully !!!", 'success');
          this.resetallforms();
          this.ServiceEvents_Get();


        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }


  AddDuties() {

    this.Reinstatingduties.push(
      this.formBuilder.group({
        duties_ProceedingNo: ['', Validators.required],
        duties_PROCEDING_DATE: [null, Validators.required],
        duties_DATE: [null, Validators.required],
        DUTIES_REPORTING_LOCATION: [null, Validators.required],
        duties_Reporting_DATE: [null, Validators.required],
        duties_Reason: [null, Validators.required],

      }));


  };

  RemoveDuties(index) { this.Reinstatingduties.removeAt(index); }
  SubmitDuties() {


    this.consubmitted = true;

    // stop here if form is invalid
    if (this.Reinstatingduties.invalid) {
      return false;
    }

    // for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
    //   if (this.datef.GreaterDate(this.EmpLeaveCtrl.value[i].startdate,this.EmpLeaveCtrl.value[i].enddate)) {
    //     Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
    //     return false;
    //   }
    // } 


    this.showloader = true;


    let duties_ProceedingNolist: string[] = [];
    let duties_PROCEDING_DATElist: string[] = [];
    let duties_DATElist: string[] = [];
    let DUTIES_REPORTING_LOCATIONlist: string[] = [];
    let duties_Reporting_DATEist: string[] = [];
    let duties_Reasonlist: string[] = [];

    let empcodelist: string[] = [];

    for (let i = 0; i < this.Reinstatingduties.length; i++) {
      duties_ProceedingNolist.push(this.Reinstatingduties.value[i].duties_ProceedingNo);
      duties_PROCEDING_DATElist.push(this.datef.format(this.Reinstatingduties.value[i].duties_PROCEDING_DATE));
      duties_DATElist.push(this.datef.format(this.Reinstatingduties.value[i].duties_DATE));
      DUTIES_REPORTING_LOCATIONlist.push(this.Reinstatingduties.value[i].DUTIES_REPORTING_LOCATION);
      duties_Reporting_DATEist.push(this.datef.format(this.Reinstatingduties.value[i].duties_Reporting_DATE));
      duties_Reasonlist.push(this.Reinstatingduties.value[i].duties_Reason);

      empcodelist.push(this.empid);

    }

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "206";

    req.INPUT_01 = empcodelist.join(',');
    req.INPUT_02 = duties_ProceedingNolist.join(',');
    req.INPUT_03 = duties_PROCEDING_DATElist.join(',');
    req.INPUT_04 = duties_DATElist.join(',');
    req.INPUT_05 = DUTIES_REPORTING_LOCATIONlist.join(',');
    req.INPUT_06 = duties_Reporting_DATEist.join(',');

    req.INPUT_07 = duties_Reasonlist.join(',');
    //req.INPUT_07 = "Pending";

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "Web";

    req.INPUT_35 = "Error Occured while SR Emplyoee ReinstatingDuties Insert Details";

    this.service.postData(req, "SR_Common_Insert_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Employee ReinstatingDuties Details Saved Successfully !!!", 'success');
          this.resetallforms();
          this.ServiceEvents_Get();


        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }

  AddAnnulInc() {

    this.stopincrement.push(
      this.formBuilder.group({
        INC_PROCEEDING_NO: ['', Validators.required],
        INC_PROCEEDING_DATE: [null, Validators.required],
        //Inc_Stop_Reason: [null, Validators.required],
        STOP_EMP_PUNISHMENT: [null, Validators.required],

        //Elegibility_Reason:[null, Validators.required],

      }));


  };

  RemoveAnnulInc(index) { this.stopincrement.removeAt(index); }
  SubmitAnnulInc() {


    this.consubmitted = true;

    // stop here if form is invalid
    if (this.stopincrement.invalid) {
      return false;
    }

    // for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
    //   if (this.datef.GreaterDate(this.EmpLeaveCtrl.value[i].startdate,this.EmpLeaveCtrl.value[i].enddate)) {
    //     Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
    //     return false;
    //   }
    // } 


    this.showloader = true;


    let INC_PROCEEDING_NOlist: string[] = [];
    let INC_PROCEEDING_DATElist: string[] = [];
    let Inc_Stop_Reasonlist: string[] = [];
    let STOP_EMP_PUNISHMENTlist: string[] = [];
    let empcodelist: string[] = [];

    for (let i = 0; i < this.stopincrement.length; i++) {
      INC_PROCEEDING_NOlist.push(this.stopincrement.value[i].INC_PROCEEDING_NO);
      INC_PROCEEDING_DATElist.push(this.datef.format(this.stopincrement.value[i].INC_PROCEEDING_DATE));
      //Inc_Stop_Reasonlist.push(this.stopincrement.value[i].Inc_Stop_Reason);
      STOP_EMP_PUNISHMENTlist.push(this.stopincrement.value[i].STOP_EMP_PUNISHMENT);
      empcodelist.push(this.empid);

    }

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "205";

    req.INPUT_01 = empcodelist.join(',');
    req.INPUT_02 = INC_PROCEEDING_NOlist.join(',');
    req.INPUT_03 = INC_PROCEEDING_DATElist.join(',');
    //req.INPUT_04 = Inc_Stop_Reasonlist.join(',');
    req.INPUT_06 = STOP_EMP_PUNISHMENTlist.join(',');
    //req.INPUT_07 = "Pending";

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "Web";

    req.INPUT_35 = "Error Occured while SR Emplyoee Stoppage Increment Insert Details";

    this.service.postData(req, "SR_Common_Insert_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Employee Stoppage Increment Saved Successfully !!!", 'success');
          this.resetallforms();
          this.ServiceEvents_Get();

        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }


  AddPromotion() {

    this.promotion.push(
      this.formBuilder.group({
        PROMOTION_PROCEEDING_NO: ['', Validators.required],
        PROMOTION_PROCEEDING_DATE: [null, Validators.required],
        TRANSFER_EMP_DESIGNATION: [null, Validators.required],
        PROMOTION_FROM_AMOUNT: [null, Validators.required],
        PROMOTION_TO_AMOUNT: [null, Validators.required],
        PROMOTION_RULE: [null, Validators.required],
        PROMOTION_AMOUNT: [null, Validators.required],
        PROMOTION_DATE: [null, Validators.required],
        //STOP_REASON: [null, Validators.required],
        //STOP_PROCNO: [null, Validators.required],
        //STOP_PROCDATE: [null, Validators.required],
        //NEXT_PROCDATE: [null, Validators.required],

      }));


  };

  RemovePromotion(index) { this.promotion.removeAt(index); }
  SubmitPromotion() {


    this.consubmitted = true;

    // stop here if form is invalid
    if (this.promotion.invalid) {
      return false;
    }

    // for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
    //   if (this.datef.GreaterDate(this.EmpLeaveCtrl.value[i].startdate,this.EmpLeaveCtrl.value[i].enddate)) {
    //     Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
    //     return false;
    //   }
    // } 


    this.showloader = true;


    let PROMOTION_PROCEEDING_NOlist: string[] = [];
    let PROMOTION_PROCEEDING_DATElist: string[] = [];
    let TRANSFER_EMP_DESIGNATIONlist: string[] = [];
    let PROMOTION_FROM_AMOUNTlist: string[] = [];
    let PROMOTION_TO_AMOUNTlist: string[] = [];
    let PROMOTION_RULElist: string[] = [];
    let PROMOTION_AMOUNTlist: string[] = [];
    let PROMOTION_DATElist: string[] = [];

    //let STOP_REASONlist: string[] = [];
    //let STOP_PROCNOlist: string[] = [];
    //let STOP_PROCDATElist: string[] = [];
    //let NEXT_PROCDATElist: string[] = [];


    let empcodelist: string[] = [];

    for (let i = 0; i < this.promotion.length; i++) {
      PROMOTION_PROCEEDING_NOlist.push(this.promotion.value[i].PROMOTION_PROCEEDING_NO);
      PROMOTION_PROCEEDING_DATElist.push(this.datef.format(this.promotion.value[i].PROMOTION_PROCEEDING_DATE));
      TRANSFER_EMP_DESIGNATIONlist.push(this.promotion.value[i].TRANSFER_EMP_DESIGNATION);
      PROMOTION_FROM_AMOUNTlist.push(this.promotion.value[i].PROMOTION_FROM_AMOUNT);
      PROMOTION_TO_AMOUNTlist.push(this.promotion.value[i].PROMOTION_TO_AMOUNT);
      PROMOTION_RULElist.push(this.promotion.value[i].PROMOTION_RULE);
      PROMOTION_AMOUNTlist.push(this.promotion.value[i].PROMOTION_AMOUNT);
      PROMOTION_DATElist.push(this.datef.format(this.promotion.value[i].PROMOTION_DATE));

      //STOP_REASONlist.push(this.promotion.value[i].STOP_REASON);
      //STOP_PROCNOlist.push(this.promotion.value[i].STOP_PROCNO);
      //STOP_PROCDATElist.push(this.datef.format(this.promotion.value[i].STOP_PROCDATE));
      //NEXT_PROCDATElist.push(this.datef.format(this.promotion.value[i].NEXT_PROCDATE));

      empcodelist.push(this.empid);

    }

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "207";

    req.INPUT_01 = empcodelist.join(',');
    req.INPUT_02 = PROMOTION_PROCEEDING_NOlist.join(',');
    req.INPUT_03 = PROMOTION_PROCEEDING_DATElist.join(',');
    req.INPUT_04 = TRANSFER_EMP_DESIGNATIONlist.join(',');
    req.INPUT_05 = PROMOTION_FROM_AMOUNTlist.join(',');
    req.INPUT_06 = PROMOTION_TO_AMOUNTlist.join(',');
    req.INPUT_07 = PROMOTION_RULElist.join(',');
    req.INPUT_08 = PROMOTION_AMOUNTlist.join(',');
    req.INPUT_09 = PROMOTION_DATElist.join(',');
    //req.INPUT_10 = STOP_REASONlist.join(',');
    //req.INPUT_11 = STOP_PROCNOlist.join(',');
    //req.INPUT_12 = STOP_PROCDATElist.join(',');
    //req.INPUT_13 = NEXT_PROCDATElist.join(',');

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "Web";

    req.INPUT_35 = "Error Occured while SR Emplyoee Promotion Insert Details";

    this.service.postData(req, "SR_Common_Insert_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Employee Promotion Details Saved Successfully !!!", 'success');
          this.resetallforms();
          this.ServiceEvents_Get();


        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }
  AddCharge() {

    this.additionalcharge.push(
      this.formBuilder.group({
        ADD_PROCEEDING_NO: ['', Validators.required],
        ADD_PROCEEDING_DATE: [null, Validators.required],
        ADD_EMP_DESIGNATION: [null, Validators.required],
        ADD_EMP_Location: [null, Validators.required],
        ADD_EMP_OfficeLocation: [null, Validators.required],
        Add_FromDate: [null, Validators.required],

      }));


  };

  RemoveCharge(index) { this.additionalcharge.removeAt(index); }
  SubmitCharge() {

    this.consubmitted = true;

    // stop here if form is invalid
    if (this.additionalcharge.invalid) {
      return false;
    }

    // for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
    //   if (this.datef.GreaterDate(this.EmpLeaveCtrl.value[i].startdate,this.EmpLeaveCtrl.value[i].enddate)) {
    //     Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
    //     return false;
    //   }
    // } 


    this.showloader = true;

    let ADD_PROCEEDING_NOlist: string[] = [];
    let ADD_PROCEEDING_DATElist: string[] = [];
    let ADD_EMP_DESIGNATIONlist: string[] = [];
    let ADD_EMP_Locationlist: string[] = [];
    let ADD_EMP_OfficeLocationlist: string[] = [];
    let Add_FromDatelist: string[] = [];
    let Add_ToDatelist: string[] = [];
    let empcodelist: string[] = [];

    for (let i = 0; i < this.additionalcharge.length; i++) {
      ADD_PROCEEDING_NOlist.push(this.additionalcharge.value[i].ADD_PROCEEDING_NO);
      ADD_PROCEEDING_DATElist.push(this.datef.format(this.additionalcharge.value[i].ADD_PROCEEDING_DATE));
      ADD_EMP_DESIGNATIONlist.push(this.additionalcharge.value[i].ADD_EMP_DESIGNATION);
      ADD_EMP_Locationlist.push(this.additionalcharge.value[i].ADD_EMP_Location);
      ADD_EMP_OfficeLocationlist.push(this.additionalcharge.value[i].ADD_EMP_OfficeLocation);
      Add_FromDatelist.push(this.datef.format(this.additionalcharge.value[i].Add_FromDate));
      //Add_ToDatelist.push(this.datef.format(this.additionalcharge.value[i].Add_ToDate));
      empcodelist.push(this.empid);

    }

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "208";

    req.INPUT_01 = empcodelist.join(',');
    req.INPUT_02 = ADD_PROCEEDING_NOlist.join(',');
    req.INPUT_03 = ADD_PROCEEDING_DATElist.join(',');
    req.INPUT_04 = ADD_EMP_DESIGNATIONlist.join(',');
    req.INPUT_05 = ADD_EMP_Locationlist.join(',');
    req.INPUT_06 = ADD_EMP_OfficeLocationlist.join(',');
    req.INPUT_07 = Add_FromDatelist.join(',');
    //req.INPUT_08 = Add_ToDatelist.join(',');    

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "Web";

    req.INPUT_35 = "Error Occured while SR Emplyoee Additional Charge Insert Details";

    this.service.postData(req, "SR_Common_Insert_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Employee Additional Charge Details Saved Successfully !!!", 'success');
          this.resetallforms();
          this.ServiceEvents_Get();


        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }


  AddReleaving() {

    this.Reladditionalcharge.push(
      this.formBuilder.group({
        Rel_PROCEEDING_NO: ['', Validators.required],
        Rel_PROCEEDING_DATE: [null, Validators.required],
        Rel_EMP_DESIGNATION: [null, Validators.required],
        Rel_EMP_Location: [null, Validators.required],
        //Rel_EMP_OfficeLocation: [null, Validators.required],
        Rel_FromDate: [null, Validators.required],

      }));


  };

  RemoveReleaving(index) { this.Reladditionalcharge.removeAt(index); }
  SubmitReleaving() {

    this.consubmitted = true;

    // stop here if form is invalid
    if (this.Reladditionalcharge.invalid) {
      return false;
    }

    // for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
    //   if (this.datef.GreaterDate(this.EmpLeaveCtrl.value[i].startdate,this.EmpLeaveCtrl.value[i].enddate)) {
    //     Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
    //     return false;
    //   }
    // } 


    this.showloader = true;

    let Rel_PROCEEDING_NOlist: string[] = [];
    let Rel_PROCEEDING_DATElist: string[] = [];
    let Rel_EMP_DESIGNATIONlist: string[] = [];
    let Rel_EMP_Locationlist: string[] = [];
    let Rel_EMP_OfficeLocationlist: string[] = [];
    let Rel_FromDatelist: string[] = [];
    let Rel_ToDatelist: string[] = [];
    let empcodelist: string[] = [];

    for (let i = 0; i < this.Reladditionalcharge.length; i++) {
      Rel_PROCEEDING_NOlist.push(this.Reladditionalcharge.value[i].Rel_PROCEEDING_NO);
      Rel_PROCEEDING_DATElist.push(this.datef.format(this.Reladditionalcharge.value[i].Rel_PROCEEDING_DATE));
      Rel_EMP_DESIGNATIONlist.push(this.Reladditionalcharge.value[i].Rel_EMP_DESIGNATION);
      Rel_EMP_Locationlist.push(this.Reladditionalcharge.value[i].Rel_EMP_Location);
      //Rel_EMP_OfficeLocationlist.push(this.Reladditionalcharge.value[i].Rel_EMP_OfficeLocation);
      Rel_FromDatelist.push(this.datef.format(this.Reladditionalcharge.value[i].Rel_FromDate));
      //Add_ToDatelist.push(this.datef.format(this.additionalcharge.value[i].Add_ToDate));
      empcodelist.push(this.empid);

    }

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "209";

    req.INPUT_01 = empcodelist.join(',');
    req.INPUT_02 = Rel_PROCEEDING_NOlist.join(',');
    req.INPUT_03 = Rel_PROCEEDING_DATElist.join(',');
    req.INPUT_04 = Rel_EMP_DESIGNATIONlist.join(',');
    req.INPUT_05 = Rel_EMP_Locationlist.join(',');
    //req.INPUT_06 = Rel_EMP_OfficeLocationlist.join(',');
    req.INPUT_07 = Rel_FromDatelist.join(',');
    //req.INPUT_08 = Add_ToDatelist.join(',');    

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "Web";

    req.INPUT_35 = "Error Occured while SR Emplyoee Releaving Additional Charge Insert Details";

    this.service.postData(req, "SR_Common_Insert_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Employee Releaving Additional Charge Details Saved Successfully !!!", 'success');
          this.resetallforms();
          this.ServiceEvents_Get();


        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }

  AddIncrement() {

    this.empincrement.push(
      this.formBuilder.group({
        INC_PAYSCLAE_FROM: ['', Validators.required],
        INC_PAYSCLAE_TO: [null, Validators.required],
        INC_PAYSCLAE_SANC_RS: [null, Validators.required],
        Inc_PROCEEDING_NO: [null, Validators.required],
        Inc_ProcDate: [null, Validators.required]
      }));


  };

  RemoveIncrement(index) { this.empincrement.removeAt(index); }
  SubmitIncrement() {


    this.consubmitted = true;

    // stop here if form is invalid
    if (this.empincrement.invalid) {
      return false;
    }

    // for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
    //   if (this.datef.GreaterDate(this.EmpLeaveCtrl.value[i].startdate,this.EmpLeaveCtrl.value[i].enddate)) {
    //     Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
    //     return false;
    //   }
    // } 


    this.showloader = true;


    let INC_PAYSCLAE_FROMlist: string[] = [];
    let INC_PAYSCLAE_TOlist: string[] = [];
    let INC_PAYSCLAE_SANC_RSlist: string[] = [];
    let Inc_PROCEEDING_NOlist: string[] = [];
    let Inc_ProcDatelist: string[] = [];
    let empcodelist: string[] = [];

    for (let i = 0; i < this.empincrement.length; i++) {
      INC_PAYSCLAE_FROMlist.push(this.empincrement.value[i].INC_PAYSCLAE_FROM);
      INC_PAYSCLAE_TOlist.push(this.empincrement.value[i].INC_PAYSCLAE_TO);
      INC_PAYSCLAE_SANC_RSlist.push(this.empincrement.value[i].INC_PAYSCLAE_SANC_RS);
      Inc_PROCEEDING_NOlist.push(this.empincrement.value[i].Inc_PROCEEDING_NO);
      Inc_ProcDatelist.push(this.datef.format(this.empincrement.value[i].Inc_ProcDate));
      empcodelist.push(this.empid);

    }

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "202";
    req.INPUT_01 = Inc_PROCEEDING_NOlist.join(',');
    req.INPUT_02 = empcodelist.join(',');
    req.INPUT_03 = Inc_ProcDatelist.join(',');
    req.INPUT_04 = INC_PAYSCLAE_FROMlist.join(',');
    req.INPUT_05 = INC_PAYSCLAE_TOlist.join(',');
    //req.INPUT_06 = IncWEFDatelist.join(',');
    req.INPUT_09 = INC_PAYSCLAE_SANC_RSlist.join(',');


    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "Web";

    req.INPUT_35 = "Error Occured while SR Emplyoee Increment Insert Details";

    this.service.postData(req, "SR_Common_Insert_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Employee Increment Details Saved Successfully !!!", 'success');
          this.resetallforms();
          this.ServiceEvents_Get();


        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }

  onRowSelected(event) {
    this.EditEmployee(event);
  }
  EditEmployee(row) {

    this.resetallforms();
    this.showloader = true;
    this.isEdit = true;

    this.showloader = false;

    // this.screen1 = false;
    // this.screen2 = false;
    // this.screen3 = false;
    // this.screen4 = true;


    this.screen1 = false;
    this.screen2 = true;

    this.screen3 = true;
    this.screen4 = false;

    const data = row.data;
    this.EmpName = data.emP_NAME;
    this.empid = data.emP_CODE;
    this.service_name = data.designation;

    this.ServiceEvents_Get();

    this.LoadYear();
  }

  ViewEmployee(row) {
    const data = row.rowData;
    this.GetEmpDetails_View(data);

    //this.GetEmployeeDetails(data.emP_CODE, 'view');
    //this.GetEmpFamilyDetails(data.emP_CODE, 'view');
    this.Employeeprofilemodal.show();
  }

  FamilyList = [];
  FamilyList_keys = [];

  EmpTransferList = [];
  EmpTransferList_keys = [];

  LeavesList = [];
  Leaves_List_keys = [];

  propertielist = [];
  propertielist_decdoc = [];

  SuspentionList = [];
  SuspentionList_keys = [];

  IncrementList = [];
  IncrementList_keys = [];

  PunishmentList = [];
  PunishmentList_keys = [];

  ReinstatteList = [];
  ReinstatteList_keys = [];

  PromotionList = [];
  PromotionList_keys = [];

  AllocationList = [];
  AllocationList_keys = [];

  ReleivingList = [];
  ReleivingList_keys = [];



  closeprofile() {
    this.Employeeprofilemodal.hide();
  }


  GetEmpDetails_View(rowdata) {
    const req = new InputRequest();

    req.DIRECTION_ID = "14";
    req.TYPEID = "ALL_SERVICE_REGISTER";

    req.INPUT_01 = rowdata.emP_CODE;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUsercode;

    this.showloader = true;
    this.service.postData(req, "SERVICE_PROFILE").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.IncrementList = data.Details['table7'];
        if (this.IncrementList.length > 0) {
          this.IncrementList_keys = Object.keys(this.IncrementList[0]);
        }


        this.EmpTransferList = data.Details['table1'];
        if (this.EmpTransferList.length > 0) {
          this.EmpTransferList_keys = Object.keys(this.EmpTransferList[0]);
        }

        this.SuspentionList = data.Details['table2'];
        if (this.SuspentionList.length > 0) {
          this.SuspentionList_keys = Object.keys(this.SuspentionList[0]);
        }


        this.PunishmentList = data.Details['table3'];
        if (this.PunishmentList.length > 0) {
          this.PunishmentList_keys = Object.keys(this.PunishmentList[0]);
        }


        this.ReinstatteList = data.Details['table4'];
        if (this.ReinstatteList.length > 0) {
          this.ReinstatteList_keys = Object.keys(this.ReinstatteList[0]);
        }


        this.PromotionList = data.Details['table5'];
        if (this.PromotionList.length > 0) {
          this.PromotionList_keys = Object.keys(this.PromotionList[0]);
        }

        this.AllocationList = data.Details['table6'];
        if (this.AllocationList.length > 0) {
          this.AllocationList_keys = Object.keys(this.AllocationList[0]);
        }

        //Empdetails table7

        this.familylist = data.Details['table8'];
        if (this.familylist.length > 0) {
          this.FamilyList_keys = Object.keys(this.familylist[0]);
        }


        this.ReleivingList = data.Details['table9'];
        if (this.ReleivingList.length > 0) {
          this.ReleivingList_keys = Object.keys(this.ReleivingList[0]);
        }


        this.LeavesList = data.Details['table10'];
        if (this.LeavesList.length > 0) {
          this.Leaves_List_keys = Object.keys(this.LeavesList[0]);
        }


        this.propertielist = data.Details['table11'];

        this.propertielist_decdoc = [];
        this.propertielist.forEach((val, i) => {
          this.EmployeeAnnualPropertie = {
            Orderid: val.procedinG_NO,
            Orderdate: val.procedinG_DATE,
            year: val.financiaL_YEAR,
            docnumber: val.documenT_NO,
            document: this.decryptFile_document(val.document, val.procedinG_NO),
          };

          this.propertielist_decdoc.push(this.EmployeeAnnualPropertie);


        })

        //propertielist_decdoc
        //EmployeeAnnualPropertie


        var empdata = data.Details['table'][0];

        this.vaadhaar = empdata.uiD_MASK;
        this.vempcode = empdata.emP_CODE;
        this.vempid = empdata.emP_ID;

        this.vemptype = empdata.categorY_NAME;
        this.vempname = empdata.emP_NAME;
        this.vdesignation = empdata.designation;
        this.vgender = empdata.gendeR_NAME;
        this.vdob = (empdata.dob ? empdata.dob : "").replace("T00:00:00", "");
        this.vdoj = (empdata.joininG_DATE ? empdata.joininG_DATE : "").replace("T00:00:00", "");
        this.veducation = empdata.educatioN_QUALIFICATION;
        this.vbloodgrp = empdata.blooD_GROUP1;
        this.vemail = empdata.emaiL_ID;
        this.vmobile = empdata.mobilE_NUMBER;
        this.vecno = empdata.emergencY_CONTACT_NO;
        this.vexpears = empdata.experiancE_YEARS;
        this.vexmonths = empdata.experiancE_MONTH;
        this.vnationality = empdata.nationality;
        this.vreligion = empdata.religion;
        this.vcommunity = empdata.communitY_NAME;
        this.vmstatus = empdata.martiaL_STATUS;
        this.vspouname = empdata.spousE_NAME;
        this.vanaadate = (empdata.anniversarY_DATE ? empdata.anniversarY_DATE : "").replace("T00:00:00", "");
        this.decryptFile(empdata.emP_PHOTO);
        this.vaadhaarphoto = empdata.emP_PHOTO;

        this.vpdistrict = empdata.preS_DISTRICT_NAME;
        this.vpru = empdata.preS_RURAL_URBAN_NAME;
        this.vpmandal = empdata.preS_MMC_NAME;
        this.vpvillage = empdata.preS_WARD_VILLAGE_NAME;
        this.vphno = empdata.preS_HOUSE_NO;
        this.vpstreett = empdata.preS_STREET_NAME;
        this.vplanmark = empdata.preS_LAND_MARK;
        this.vppincode = empdata.preS_PINCODE;

        this.vpedistrict = empdata.perM_DISTRICT_NAME;
        this.vperu = empdata.perM_RURAL_URBAN_NAME;
        this.vpemandal = empdata.perM_MMC_NAME;
        this.vpevillage = empdata.perM_WARD_VILLAGE_NAME;
        this.vpehno = empdata.perM_HOUSE_NO;
        this.vpestreett = empdata.perM_STREET_NAME;
        this.vpelanmark = empdata.perM_LAND_MARK;
        this.vpepincode = empdata.perM_PINCODE;

        this.vnstate = empdata.nativE_STATE_NAME;
        this.vndistrict = empdata.nativE_DISTRICT_NAME;
        this.vnru = empdata.nativE_RURAL_URBAN_NAME;
        this.vnmandal = empdata.nativE_MMC_NAME;
        this.vnvillage = empdata.nativE_WARD_VILLAGE_NAME;
        this.vnhno = empdata.nativE_HOUSE_NO;
        this.vnstreett = empdata.nativE_STREET_NAME;
        this.vnlanmark = empdata.nativE_LAND_MARK;
        this.vnpincode = empdata.nativE_PINCODE;

        this.vloctype = empdata.worK_LOCATION_TYPE;
        this.vlocname = empdata.worK_LOCATION;
        this.vsectionname = empdata.sectioN_NAME;
        this.vrmanager = empdata.reportinG_OFFICER_NAME;
        this.vastatus = empdata.iS_ACTIVE;

        this.vifsccode = empdata.ifsC_CODE;
        this.vaccname = empdata.accounT_HOLDER_NAME;
        this.vaccno = empdata.accounT_NUMBER_MASK;
        this.vbankname = empdata.banK_NAME;
        this.vbranch = empdata.banK_BRANCH;

        this.vuanno = empdata.uaN_NO;
        this.vpfno = empdata.pF_NO;
        this.vpfdate = (empdata.pF_DATE ? empdata.pF_DATE : "").replace("T00:00:00", "");
        this.vpfoffice = empdata.pF_OFFICE;
        this.vpfaccno = empdata.pF_AC_NO;
        this.vpanno = empdata.paN_NO;


        this.V_Height = empdata.height;
        this.V_Mothertongue = empdata.motheR_TONGUE;
        this.V_Nativeplace = empdata.emP_NATIVE_PLACE;
        this.V_identification1 = empdata.marK_IDENTIFICATION1;
        this.V_identification2 = empdata.marK_IDENTIFICATION2;



      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }


  GetEmployeeDetails(emP_CODE: string, ptype: string) {




    const rtype: string = ptype;
    const req = new InputRequest();
    req.INPUT_01 = emP_CODE;
    // var data = { 'INPUT_01': this.f.EmpAadhaar.value };
    this.service.postData(req, "GetEmployeeDetails").subscribe(data => {

      if (data.StatusCode == "100") {
        const empdata = data.Details[0];
        if (empdata) {
          //if (rtype == 'edit')
          //this.FillEditEmployeeDetails(empdata);
          if (rtype == 'view')
            this.FillViewEmployeeDetails(empdata);
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }

  GetEmpFamilyDetails(emP_CODE: string, ptype: string) {
    const rtype: string = ptype;
    const req = new InputRequest();
    req.INPUT_01 = emP_CODE;
    // var data = { 'INPUT_01': this.f.EmpAadhaar.value };
    this.service.postData(req, "GetEmpFamilyDetails").subscribe(data => {

      if (data.StatusCode == "100") {
        const empdata = data.Details;
        if (rtype == 'view') {
          this.familylist = empdata;
          console.log(this.familylist);
        }

      }



    },
      error => console.log(error));


  }



  FillViewEmployeeDetails(empdata: any) {
    this.vemptype = empdata.categorY_NAME;
    this.vempname = empdata.emP_NAME;
    this.vdesignation = empdata.designation;
    this.vgender = empdata.gendeR_NAME;
    this.vdob = (empdata.dob ? empdata.dob : "").replace("T00:00:00", "");
    this.vdoj = (empdata.joininG_DATE ? empdata.joininG_DATE : "").replace("T00:00:00", "");
    this.veducation = empdata.educatioN_QUALIFICATION;
    this.vbloodgrp = empdata.blooD_GROUP1;
    this.vemail = empdata.emaiL_ID;
    this.vmobile = empdata.mobilE_NUMBER;
    this.vecno = empdata.emergencY_CONTACT_NO;
    this.vexpears = empdata.experiancE_YEARS;
    this.vexmonths = empdata.experiancE_MONTH;
    this.vnationality = empdata.nationality;
    this.vreligion = empdata.religion;
    this.vcommunity = empdata.communitY_NAME;
    this.vmstatus = empdata.martiaL_STATUS;
    this.vspouname = empdata.spousE_NAME;
    this.vanaadate = (empdata.anniversarY_DATE ? empdata.anniversarY_DATE : "").replace("T00:00:00", "");
    this.decryptFile(empdata.emP_PHOTO);
    this.vaadhaarphoto = empdata.emP_PHOTO;

    this.vpdistrict = empdata.preS_DISTRICT_NAME;
    this.vpru = empdata.preS_RURAL_URBAN_NAME;
    this.vpmandal = empdata.preS_MMC_NAME;
    this.vpvillage = empdata.preS_WARD_VILLAGE_NAME;
    this.vphno = empdata.preS_HOUSE_NO;
    this.vpstreett = empdata.preS_STREET_NAME;
    this.vplanmark = empdata.preS_LAND_MARK;
    this.vppincode = empdata.preS_PINCODE;

    this.vpedistrict = empdata.perM_DISTRICT_NAME;
    this.vperu = empdata.perM_RURAL_URBAN_NAME;
    this.vpemandal = empdata.perM_MMC_NAME;
    this.vpevillage = empdata.perM_WARD_VILLAGE_NAME;
    this.vpehno = empdata.perM_HOUSE_NO;
    this.vpestreett = empdata.perM_STREET_NAME;
    this.vpelanmark = empdata.perM_LAND_MARK;
    this.vpepincode = empdata.perM_PINCODE;

    this.vnstate = empdata.nativE_STATE_NAME;
    this.vndistrict = empdata.nativE_DISTRICT_NAME;
    this.vnru = empdata.nativE_RURAL_URBAN_NAME;
    this.vnmandal = empdata.nativE_MMC_NAME;
    this.vnvillage = empdata.nativE_WARD_VILLAGE_NAME;
    this.vnhno = empdata.nativE_HOUSE_NO;
    this.vnstreett = empdata.nativE_STREET_NAME;
    this.vnlanmark = empdata.nativE_LAND_MARK;
    this.vnpincode = empdata.nativE_PINCODE;

    this.vloctype = empdata.worK_LOCATION_TYPE;
    this.vlocname = empdata.worK_LOCATION;
    this.vsectionname = empdata.sectioN_NAME;
    this.vrmanager = empdata.reportinG_OFFICER_NAME;
    this.vastatus = empdata.iS_ACTIVE;

    this.vifsccode = empdata.ifsC_CODE;
    this.vaccname = empdata.accounT_HOLDER_NAME;
    this.vaccno = empdata.accounT_NUMBER_MASK;
    this.vbankname = empdata.banK_NAME;
    this.vbranch = empdata.banK_BRANCH;

    this.vuanno = empdata.uaN_NO;
    this.vpfno = empdata.pF_NO;
    this.vpfdate = (empdata.pF_DATE ? empdata.pF_DATE : "").replace("T00:00:00", "");
    this.vpfoffice = empdata.pF_OFFICE;
    this.vpfaccno = empdata.pF_AC_NO;
    this.vpanno = empdata.paN_NO;
  }
  decryptFile(filepath) {
    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          console.log(data.docBase64);
          this.vempphoto = data.docBase64;
        },
          error => {
            console.log(error);
            this.vempphoto = "";
          });
    }
    else
      this.vempphoto = "";
  }





  decryptFile_document(filepath, Orderid): string {
    if (filepath) {

      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {
          let objIndex = this.propertielist_decdoc.findIndex((obj => obj.Orderid == Orderid));


          const blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
          const blobUrl = URL.createObjectURL(blob);

          this.propertielist_decdoc[objIndex].document = (this.sanitizer.bypassSecurityTrustResourceUrl(blobUrl));


          return "";

        },
          error => {
            console.log(error);
            //this.vempphoto = "";
          });
    }
    else { return ""; }

  }

  HistoryAllEmployees(): void {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_02 = "EMPLOYEES";


    this.service.postData(req, "GetEmployeeHistory").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.allemphistory = data.Details;
        for (let i = 0; i < this.allemphistory.length; i++) {
          let colname = this.allemphistory[i].columN_NAME;
          let oldvalue = this.allemphistory[i].olD_VALUE;
          let newvalue = this.allemphistory[i].neW_VALUE;
          if (colname == "EMP_PHOTO" && oldvalue) {
            this.decryptHistoryFile(oldvalue, i, 'oldvalue');
          }
          if (colname == "EMP_PHOTO" && newvalue) {
            // this.allemphistory[i].neW_VALUE = this.service.getBase64(newvalue);
            // console.log(this.allemphistory[i].neW_VALUE);
            this.decryptHistoryFile(newvalue, i, 'newvalue');
          }
        }
        this.historyAllSection.show();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }




  decryptHistoryFile(filepath, i, ftype) {
    const coltype: string = ftype;
    const indx = i;
    if (coltype == 'oldvalue')
      this.allemphistory[indx].olD_VALUE = "";
    else
      this.allemphistory[indx].neW_VALUE = "";

    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (coltype == 'oldvalue')
            this.allemphistory[indx].olD_VALUE = data.docBase64;
          else
            this.allemphistory[indx].neW_VALUE = data.docBase64;
        })
    }
  };

  decryptempHistoryFile(filepath, i, ftype) {
    const coltype: string = ftype;
    const indx = i;
    if (coltype == 'oldvalue')
      this.emphistory[indx].olD_VALUE = "";
    else
      this.emphistory[indx].neW_VALUE = "";

    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (coltype == 'oldvalue')
            this.emphistory[indx].olD_VALUE = data.docBase64;
          else
            this.emphistory[indx].neW_VALUE = data.docBase64;
        })
    }
  };

  HistoryEmployee(row): void {
    this.showloader = true;
    const data = row.rowData;
    const req = new InputRequest();
    req.INPUT_01 = data.emP_CODE;

    this.service.postData(req, "GetEmployeeHistory").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.emphistory = data.Details;
        for (let i = 0; i < this.emphistory.length; i++) {
          let colname = this.emphistory[i].columN_NAME;
          let oldvalue = this.emphistory[i].olD_VALUE;
          let newvalue = this.emphistory[i].neW_VALUE;
          if (colname == "EMP_PHOTO" && oldvalue) {
            this.decryptempHistoryFile(oldvalue, i, 'oldvalue');
          }
          if (colname == "EMP_PHOTO" && newvalue) {
            this.decryptempHistoryFile(newvalue, i, 'newvalue');
          }
        }
        this.historySection.show();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }
  list_items: any[];
  keys: any[];

  ServiceEvents_Get(): void {
    this.showloader = true;
    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "GET_SR_EVENTS";
    req.INPUT_01 = this.empid;

    this.service.postData(req, "SERVICE_EVENTS_GET").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        //this.list_items = data.Details;

        //this.keys = Object.keys(this.list_items[0]);

        this.EmployeerowData_Events = data.Details;

        this.gridApi_event.setRowData(this.EmployeerowData_Events);

        // this.screen1 = false;
        // this.screen2 = true;

        // this.screen3 = true;
        // this.screen4 = false;

        // if(typeid=="GET_EMP_TRANSFER")
        // {
        //   this.eventid="1";
        //   this.eventname="Employee Transfer"
        // }
        // if(typeid=="GET_EMP_SUSPENSION")
        // {
        //   this.eventid="2";
        //   this.eventname="Employee Suspension/Detention"
        // }
        // if(typeid=="GET_ANNUAL_INCREMENT")
        // {
        //   this.eventid="3";
        //   this.eventname="Stoppage of Annual Grade Increment"
        // }
        // if(typeid=="GET_SR_DUTIES")
        // {
        //   this.eventid="4";
        //   this.eventname="Reinstating Duties"
        // }
        // if(typeid=="GET_SR_PROMOTION")
        // {
        //   this.eventid="5";
        //   this.eventname="Employee Promotion"
        // }
        // if(typeid=="GET_SR_CHARGE")
        // {
        //   this.eventid="6";
        //   this.eventname="Allocation of Additional Charge"
        // }


      }
      else {
        //Swal.fire('warning', data.StatusMessage, 'warning');
      }

    },
      error => console.log(error));
  }



  resetallforms() {

    this.consubmitted = false;

    this.emptransferform.reset();

    this.emptransfer.reset();
    this.emptransfer.clear();

    this.empsuspentionform.reset();

    this.suspention.reset();
    this.suspention.clear();

    this.empsuspentionform.reset();

    this.stopincrement.reset();
    this.stopincrement.clear();

    this.empdutiesform.reset();

    this.Reinstatingduties.reset();
    this.Reinstatingduties.clear();

    this.emppromotionform.reset();

    this.promotion.reset();
    this.promotion.clear();

    this.empadditionalchargeform.reset();

    this.additionalcharge.reset();
    this.additionalcharge.clear();

    this.Relempadditionalchargeform.reset();

    this.Reladditionalcharge.reset();
    this.Reladditionalcharge.clear();



    this.empincrementform.reset();

    this.empincrement.reset();
    this.empincrement.clear();


    this.emppropertieform.reset();

    this.annualpropertie.reset();
    this.annualpropertie.clear();

    this.emptransfermodal.hide();
    this.suspentionmodal.hide();
    this.stopincrementmodal.hide();
    this.Reinstatingdutiesmodal.hide();
    this.promotionmodal.hide();
    this.additionalchargemodal.hide();
    this.empincrementmodal.hide();

    this.Reladditionalchargemodal.hide();


  }





  LoadEmployeeList() {
    this.showloader = true;
    this.service.getData("GetEmpList").subscribe(data => {
      if (data.StatusCode == "100") {
        this.employees = data.Details;
        this.EmployeerowData = data.Details;
        this.EmployeerowData = this.EmployeerowData.filter(function (el) { return el.servicE_TYPE == "Regular" })

        if (this.EmployeerowData.length > 0) {
          this.gridApi.setRowData(this.EmployeerowData);
        }
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));
  }

  RemoveEvent(row: any) {
    const rowdata = row.rowData;

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to Remove the Event " + rowdata.procedinG_NO + " !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {

        const req = new InputRequest();

        req.DIRECTION_ID = "14";
        req.TYPEID = "REMOVE_SR_EVENTS";

        req.INPUT_01 = rowdata.procedinG_NO;
        req.INPUT_02 = rowdata.evenT_TYPE;
        req.INPUT_03 = this.empid;

        req.CALL_SOURCE = "WEB";
        req.USER_NAME = this.logUsercode;

        this.showloader = true;

        this.service.postData(req, "SERVICE_EVENTS_GET").subscribe(data => {
          this.showloader = false;

          if (data.StatusCode == "100") {
            let result = data.Details[0];
            if (result.rtN_ID === 1) {

              this.ServiceEvents_Get();
              //this.editModal.hide();
              Swal.fire('success', "Event Removed Successfully !!!", 'success');
            }
            else {
              Swal.fire('warning', result.statuS_TEXT, 'warning');
            }

          }
          else
            Swal.fire('warning', data.StatusMessage, 'warning');

        },
          error => console.log(error));


      }
    })

  }

}
