import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrleaveledgerComponent } from './srleaveledger.component';

describe('SrleaveledgerComponent', () => {
  let component: SrleaveledgerComponent;
  let fixture: ComponentFixture<SrleaveledgerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrleaveledgerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrleaveledgerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
