import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';

import Swal from 'sweetalert2';

import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { AgGridAngular } from 'ag-grid-angular';
import { NullTemplateVisitor } from '@angular/compiler';
import { sreventsRendererComponent } from 'src/app/custome-directives/srevents-renderer.component';
import { srempdetailsRendererComponent } from 'src/app/custome-directives/srempdetails-renderer.component';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser'
import { EmployeeAnnualPropertie } from 'src/app/Interfaces/user';


@Component({
  selector: 'app-srleaveledger',
  templateUrl: './srleaveledger.component.html',
  styleUrls: ['./srleaveledger.component.css']
})
export class SrleaveledgerComponent implements OnInit {

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('historySection') public historySection: ModalDirective;
  @ViewChild('historyAllSection') public historyAllSection: ModalDirective;
  @ViewChild('Employeeprofile') public Employeeprofilemodal: ModalDirective;

  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('agGridLeave') agGridLeave: AgGridAngular;

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserCode: string = sessionStorage.getItem("logUserCode");


  showloader: boolean = true;
  ismodal: boolean = false;
  modeltitle: string = "Add";
  locationtypeid: string = "";
  empkeyword: string = "emP_NAME";
  EmployeeAnnualPropertie: EmployeeAnnualPropertie;

  columnDefs = [
    { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Employee Name', maxWidth: 200, field: 'emP_NAME',
      cellRenderer: "EmpdataGet",
      sortable: true, filter: true
    },
    { headerName: 'Login Code', maxWidth: 150, field: 'emP_CODE', sortable: true, filter: true },
    { headerName: 'Employee Type', maxWidth: 150, field: 'servicE_TYPE', sortable: true, filter: true },
    { headerName: 'Designation', maxWidth: 200, field: 'designation', sortable: true, filter: true },
    { headerName: 'Mobile No', maxWidth: 120, field: 'mobilE_NUMBER', sortable: true, filter: true },
    {
      headerName: 'Action', maxWidth: 400, cellRenderer: 'buttonRenderer',
      cellRendererParams: {

        viewClick: this.ViewEmployee.bind(this),
        //viewClick: this.ViewEmployee.bind(this),
        //historyClick: this.HistoryEmployee.bind(this),

      },
    }
  ];


  columnDefs_Leaves = [
    { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Proceeding No', maxWidth: 200, field: 'procedinG_NO', sortable: true, filter: true },
    { headerName: 'Event Type', maxWidth: 150, field: 'evenT_TYPE', sortable: true, filter: true },
    { headerName: 'Event Discription', width: 600, field: 'evenT_DESCRIPTION', sortable: true, filter: true, wrapText: true, autoHeight: true },
    {
      headerName: 'Action', maxWidth: 120, cellRenderer: 'buttonRenderer',
      cellRendererParams: {

        RemoveClick: this.EditEmpLeaveMaster.bind(this),
        //viewClick: this.ViewEmpLeave.bind(this),
        //historyClick:this.HistoryLeave.bind(this),
        //disableRefreshButtons: this..field.status.value=="Cancel" ? true : false;

      },
    }
  ];
  EmployeerowData = [];

  employee: any;
  employees: any[];
  uidstatus: boolean = true;
  uid_isvalid: boolean = true;
  Emp_mob_isvalid = true;
  Emp_Emergencymob_isvalid = true;
  PanPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  IFSCPattern = "^[A-Z]{4}0[A-Z0-9]{6}$";


  NgbDateStruct: any;
  public isEdit: boolean = false;
  public isAdmin: boolean = false;
  public consubmitted: boolean = false;


  public screen1: boolean = true;

  public screen2: boolean = false;
  public backscreen: boolean = false;

  GridFill: any[];
  GridFillRowData: any[];


  public EmpName: string = "";
  public empid: string = "";
  public service_name: string = "";


  public eventname: string = "";

  propertielist = [];
  propertielist_decdoc = [];

  rowData: any = [];
  defaultColDef: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];

  rowData_Leave: any = [];
  defaultColDef_Leave: any = [];
  gridColumnApi_Leave: any = [];
  gridApi_Leave: any = [];


  vaadhaar: string;
  vempcode: string;
  vempid: string;
  vemptype: string;
  vempname: string;
  vdesignation: string;
  vgender: string;
  vdob: string;
  vdoj: string;
  veducation: string;
  vbloodgrp: string;
  vemail: string;
  vmobile: string;
  vecno: string;
  vexpears: string;
  vexmonths: string;
  vnationality: string;
  vreligion: string;
  vcommunity: string;
  vmstatus: string;
  vspouname: string;
  vanaadate: string;
  vempphoto: string;
  vaadhaarphoto: string;

  vpdistrict: string;
  vpru: string;
  vpmandal: string;
  vpvillage: string;
  vphno: string;
  vpstreett: string;
  vplanmark: string;
  vppincode: string;

  vpedistrict: string;
  vperu: string;
  vpemandal: string;
  vpevillage: string;
  vpehno: string;
  vpestreett: string;
  vpelanmark: string;
  vpepincode: string;

  vnstate: string;
  vndistrict: string;
  vnru: string;
  vnmandal: string;
  vnvillage: string;
  vnhno: string;
  vnstreett: string;
  vnlanmark: string;
  vnpincode: string;

  vloctype: string;
  vlocname: string;
  vsectionname: string;
  vrmanager: string;
  vastatus: string;

  vifsccode: string;
  vaccname: string;
  vaccno: string;
  vbankname: string;
  vbranch: string;

  familylist: any = [];

  vuanno: string;
  vpfno: string;
  vpfdate: string;
  vpfoffice: string;
  vpfaccno: string;
  vpanno: string;

  V_identification1: string;
  V_identification2: string;
  V_Mothertongue: string;
  V_Height: string;
  V_Nativeplace: string;


  frameworkComponents: any;
  public components;
  frameworkComponents_Leaves: any;
  public components_Leaves;

  allemphistory: any = [];
  emphistory: any = [];
  progress: number = 0;
  message: string = "";

  constructor(
    private service: CommonServices,
    private router: Router,
    private formBuilder: FormBuilder,
    private datef: CustomDateParserFormatter,
    private datecon: NgbDatepickerConfig, private sanitizer: DomSanitizer
  ) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }

    if (this.logUserrole == "101" || this.logUserrole == "102")
      this.isAdmin = true;
    this.frameworkComponents = {
      buttonRenderer: srempdetailsRendererComponent,

    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
      EmpdataGet: function (params) {

        return '<u style="color:blue">' + params.value + '</u>';

      },
    };

    this.frameworkComponents_Leaves = {
      buttonRenderer: sreventsRendererComponent,

    }

    this.components_Leaves = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }

  encashtitle: string = "";
  changetype(i) {

    var name = this.leavetypes.filter(v => v.id == this.EmpLeaveCtrl.value[i].LeaveType)[0].leave;

    if (name.toString().toUpperCase() == "EARNED LEAVE") {
      this.EmpLeaveCtrl.value[i].encashtitle.setValue("Encashment of");
    }
    else { this.EmpLeaveCtrl.value[i].encashtitle.setValue(""); }
  }

  //this.datecon.maxDate = new NgbDateStruct({year:2021,month:4:day:2}) 

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //params.api.setRowData(this.EmployeerowData);
    //this.gridApi.sizeColumnsToFit();
    this.LoadEmployeeList()
  }

  BindData_Leaves(params) {
    this.gridApi_Leave = params.api;
    this.gridColumnApi_Leave = params.columnApi;
    params.api.setRowData(this.GridFillRowData);
    //this.gridApi.sizeColumnsToFit();

  }
  decryptFile_document(filepath, Orderid): string {
    if (filepath) {

      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          let objIndex = this.propertielist_decdoc.findIndex((obj => obj.Orderid == Orderid));


          const blob = this.service.s_sd((data.docBase64).split(',')[1], "application/pdf");
          const blobUrl = URL.createObjectURL(blob);

          this.propertielist_decdoc[objIndex].document = (this.sanitizer.bypassSecurityTrustResourceUrl(blobUrl));


          return "";

        },
          error => {
            console.log(error);
            //this.vempphoto = "";
          });
    }
    else { return ""; }

  }

  back() {
    this.screen2 = false;
    this.screen1 = true;

  }

  dataclicked(params) {
    if (params.colDef.cellRenderer == "EmpdataGet") {
      this.EditEmployee(params);
    }

  }

  EditEmpLeaveMaster(row: any) {

    const rowdata = row.rowData;

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to Remove the Event " + rowdata.procedinG_NO + " !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {

        const req = new InputRequest();

        req.DIRECTION_ID = "14";
        req.TYPEID = "REMOVE_SR_EVENTS";

        req.INPUT_01 = rowdata.procedinG_NO;
        req.INPUT_02 = rowdata.evenT_TYPE;
        req.INPUT_03 = this.empid;

        req.CALL_SOURCE = "WEB";
        req.USER_NAME = this.logUserCode;

        this.showloader = true;

        this.service.postData(req, "SERVICE_EVENTS_GET").subscribe(data => {
          this.showloader = false;

          if (data.StatusCode == "100") {
            let result = data.Details[0];
            if (result.rtN_ID === 1) {

              this.fillGriddata(this.empid);
              //this.editModal.hide();
              Swal.fire('success', "Leave Ledger Event Removed Successfully !!!", 'success');
            }
            else {
              Swal.fire('warning', result.statuS_TEXT, 'warning');
            }

          }
          else
            Swal.fire('warning', data.StatusMessage, 'warning');

        },
          error => console.log(error));


      }
    })

  }
  ViewEmpLeave() {

  }
  HistoryLeave() {

  }

  AddSR() {
    this.resetallforms();
    this.editModal.show();
  }
  onRowSelected(event) {
    this.EditEmployee(event);
  }
  SubmitEmpLeave() {

    this.consubmitted = true;

    // stop here if form is invalid
    if (this.employeeLeaveForm.invalid) {
      return false;
    }

    // for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
    //   if (this.datef.GreaterDate(this.EmpLeaveCtrl.value[i].startdate,this.EmpLeaveCtrl.value[i].enddate)) {
    //     Swal.fire('warning', "End Date Shoud be greater than Start Date", 'warning');
    //     return false;
    //   }
    // } 


    this.showloader = true;


    let LeaveTypelist: string[] = [];
    let LeaveDaylist: string[] = [];
    let FromDatelist: string[] = [];
    let ToDatelist: string[] = [];
    let NoOfdayslist: string[] = [];
    let RptManagerlist: string[] = [];
    let AppliedDatelist: string[] = [];
    let ApproveDatelist: string[] = [];
    let SRNOLIST: string[] = [];
    let empcodelist: string[] = [];
    let periodlist: string[] = [];

    for (let i = 0; i < this.EmpLeaveCtrl.length; i++) {
      SRNOLIST.push(this.EmpLeaveCtrl.value[i].srno);
      LeaveTypelist.push(this.EmpLeaveCtrl.value[i].LeaveType);
      //LeaveDaylist.push(this.EmpLeaveCtrl.value[i].LeaveDay);
      FromDatelist.push(this.datef.format(this.EmpLeaveCtrl.value[i].FromDate));
      ToDatelist.push(this.datef.format(this.EmpLeaveCtrl.value[i].ToDate));
      NoOfdayslist.push(this.EmpLeaveCtrl.value[i].NoOfdays);
      //RptManagerlist.push(this.EmpLeaveCtrl.value[i].RptManager);
      AppliedDatelist.push(this.datef.format(this.EmpLeaveCtrl.value[i].AppliedDate));
      //ApproveDatelist.push(this.datef.format(this.EmpLeaveCtrl.value[i].ApproveDate ));
      empcodelist.push(this.empid);
      periodlist.push(this.EmpLeaveCtrl.value[i].period);

    }

    const req = new InputRequest();
    req.DIRECTION_ID = "14";
    req.TYPEID = "201";

    req.INPUT_01 = SRNOLIST.join(',');
    req.INPUT_02 = LeaveTypelist.join(',');
    req.INPUT_03 = FromDatelist.join(',');
    req.INPUT_04 = ToDatelist.join(',');
    req.INPUT_05 = AppliedDatelist.join(',');
    //req.INPUT_06 = ApproveDatelist.join(',');
    req.INPUT_07 = NoOfdayslist.join(',');
    req.INPUT_08 = "Pending";
    //req.INPUT_09 = RptManagerlist.join(',');    
    req.INPUT_10 = "";
    req.INPUT_11 = empcodelist.join(',');
    //req.INPUT_12 = LeaveDaylist.join(',');
    req.INPUT_13 = periodlist.join(',');

    req.USER_NAME = this.logUserCode;
    req.CALL_SOURCE = "Web";

    this.service.postData(req, "SR_LeaveLedger_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          Swal.fire('success', "Leave Ledger Details Saved Successfully !!!", 'success');
          this.resetallforms();
          this.fillGriddata(this.empid);


        }
        else
          Swal.fire('warning', result.statuS_TEXT, 'warning');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));



  }
  employeeLeaveForm: FormGroup;
  leavetypes: any[];
  //get EmpLeaveCtrl() { return this.employeeLeaveForm.controls; }
  get subf() { return this.employeeLeaveForm.controls; }

  get EmpLeaveCtrl() { return this.subf.employeeSRLeaves as FormArray; }

  ngOnInit(): void {


    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }




    this.employeeLeaveForm = this.formBuilder.group({
      employeeSRLeaves: new FormArray([
        this.formBuilder.group({
          srno: ['', Validators.required],
          LeaveType: [null, Validators.required],
          //LeaveDay:[null, Validators.required],
          FromDate: [null, Validators.required],
          ToDate: [null, Validators.required],
          NoOfdays: ['', Validators.required],
          //RptManager:['', Validators.required],
          AppliedDate: [null, Validators.required],
          //ApproveDate:[null, Validators.required],
          period: [null, Validators.required],
          // encashtitle:['']


        })
      ])

    });

    this.LoadLeaveType();

  }

  AddLeave() {


    this.EmpLeaveCtrl.push(this.formBuilder.group({
      srno: ['', Validators.required],
      LeaveType: [null, Validators.required],
      //LeaveDay:[null, Validators.required],
      FromDate: [null, Validators.required],
      ToDate: [NullTemplateVisitor, Validators.required],
      NoOfdays: ['', Validators.required],
      //RptManager:['', Validators.required],
      AppliedDate: [null, Validators.required],
      //ApproveDate:[null, Validators.required],
      period: [null, Validators.required],

    }));
  }

  RemoveLeave(index) { this.EmpLeaveCtrl.removeAt(index); }


  CloseViewModal() {
    //this.viewModal.hide();
    //this.LoadEmployeeList();
  }


  EditEmployee(row) {
    const data = row.data;
    //this.resetallforms();



    this.modeltitle = "Edit";
    this.isEdit = true;
    this.uid_isvalid = true;
    this.uidstatus = true;


    this.EmpName = data.emP_NAME;
    this.empid = data.emP_CODE;
    this.service_name = data.designation;

    this.screen1 = false;
    this.screen2 = true;



    this.fillGriddata(data.emP_CODE);

  }

  ViewEmployee(row) {
    const data = row.rowData;

    //this.GetEmployeeDetails(data.emP_CODE, 'view');
    //this.GetEmpFamilyDetails(data.emP_CODE, 'view');
    this.GetEmpDetails_View(data);

    //this.GetEmployeeDetails(data.emP_CODE, 'view');
    //this.GetEmpFamilyDetails(data.emP_CODE, 'view');
    this.Employeeprofilemodal.show();
    console.log(data);
  }

  FamilyList = [];
  FamilyList_keys = [];

  EmpTransferList = [];
  EmpTransferList_keys = [];

  LeavesList = [];
  Leaves_List_keys = [];


  SuspentionList = [];
  SuspentionList_keys = [];

  IncrementList = [];
  IncrementList_keys = [];

  PunishmentList = [];
  PunishmentList_keys = [];

  ReinstatteList = [];
  ReinstatteList_keys = [];

  PromotionList = [];
  PromotionList_keys = [];

  AllocationList = [];
  AllocationList_keys = [];

  ReleivingList = [];
  ReleivingList_keys = [];



  closeprofile() {
    this.Employeeprofilemodal.hide();
  }


  GetEmpDetails_View(rowdata) {
    const req = new InputRequest();

    req.DIRECTION_ID = "14";
    req.TYPEID = "ALL_SERVICE_REGISTER";

    req.INPUT_01 = rowdata.emP_CODE;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserCode;

    this.showloader = true;
    this.service.postData(req, "SERVICE_PROFILE").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {

        this.IncrementList = data.Details['table7'];
        if (this.IncrementList.length > 0) {
          this.IncrementList_keys = Object.keys(this.IncrementList[0]);
        }


        this.EmpTransferList = data.Details['table1'];
        if (this.EmpTransferList.length > 0) {
          this.EmpTransferList_keys = Object.keys(this.EmpTransferList[0]);
        }

        this.SuspentionList = data.Details['table2'];
        if (this.SuspentionList.length > 0) {
          this.SuspentionList_keys = Object.keys(this.SuspentionList[0]);
        }


        this.PunishmentList = data.Details['table3'];
        if (this.PunishmentList.length > 0) {
          this.PunishmentList_keys = Object.keys(this.PunishmentList[0]);
        }


        this.ReinstatteList = data.Details['table4'];
        if (this.ReinstatteList.length > 0) {
          this.ReinstatteList_keys = Object.keys(this.ReinstatteList[0]);
        }


        this.PromotionList = data.Details['table5'];
        if (this.PromotionList.length > 0) {
          this.PromotionList_keys = Object.keys(this.PromotionList[0]);
        }

        this.AllocationList = data.Details['table6'];
        if (this.AllocationList.length > 0) {
          this.AllocationList_keys = Object.keys(this.AllocationList[0]);
        }

        //Empdetails table7

        this.familylist = data.Details['table8'];
        if (this.familylist.length > 0) {
          this.FamilyList_keys = Object.keys(this.familylist[0]);
        }


        this.ReleivingList = data.Details['table9'];
        if (this.ReleivingList.length > 0) {
          this.ReleivingList_keys = Object.keys(this.ReleivingList[0]);
        }


        this.LeavesList = data.Details['table10'];
        if (this.LeavesList.length > 0) {
          this.Leaves_List_keys = Object.keys(this.LeavesList[0]);
        }


        this.propertielist = data.Details['table11'];
        console.log(this.propertielist);

        this.propertielist_decdoc = [];
        this.propertielist.forEach((val, i) => {
          this.EmployeeAnnualPropertie = {
            Orderid: val.procedinG_NO,
            Orderdate: val.procedinG_DATE,
            year: val.financiaL_YEAR,
            docnumber: val.documenT_NO,
            document: this.decryptFile_document(val.document, val.procedinG_NO),
          };

          this.propertielist_decdoc.push(this.EmployeeAnnualPropertie);


        })




        var empdata = data.Details['table'][0];

        this.vaadhaar = empdata.uiD_MASK;
        this.vempcode = empdata.emP_CODE;
        this.vempid = empdata.emP_ID;

        this.vemptype = empdata.categorY_NAME;
        this.vempname = empdata.emP_NAME;
        this.vdesignation = empdata.designation;
        this.vgender = empdata.gendeR_NAME;
        this.vdob = (empdata.dob ? empdata.dob : "").replace("T00:00:00", "");
        this.vdoj = (empdata.joininG_DATE ? empdata.joininG_DATE : "").replace("T00:00:00", "");
        this.veducation = empdata.educatioN_QUALIFICATION;
        this.vbloodgrp = empdata.blooD_GROUP1;
        this.vemail = empdata.emaiL_ID;
        this.vmobile = empdata.mobilE_NUMBER;
        this.vecno = empdata.emergencY_CONTACT_NO;
        this.vexpears = empdata.experiancE_YEARS;
        this.vexmonths = empdata.experiancE_MONTH;
        this.vnationality = empdata.nationality;
        this.vreligion = empdata.religion;
        this.vcommunity = empdata.communitY_NAME;
        this.vmstatus = empdata.martiaL_STATUS;
        this.vspouname = empdata.spousE_NAME;
        this.vanaadate = (empdata.anniversarY_DATE ? empdata.anniversarY_DATE : "").replace("T00:00:00", "");
        this.decryptFile(empdata.emP_PHOTO);
        this.vaadhaarphoto = empdata.emP_PHOTO;

        this.vpdistrict = empdata.preS_DISTRICT_NAME;
        this.vpru = empdata.preS_RURAL_URBAN_NAME;
        this.vpmandal = empdata.preS_MMC_NAME;
        this.vpvillage = empdata.preS_WARD_VILLAGE_NAME;
        this.vphno = empdata.preS_HOUSE_NO;
        this.vpstreett = empdata.preS_STREET_NAME;
        this.vplanmark = empdata.preS_LAND_MARK;
        this.vppincode = empdata.preS_PINCODE;

        this.vpedistrict = empdata.perM_DISTRICT_NAME;
        this.vperu = empdata.perM_RURAL_URBAN_NAME;
        this.vpemandal = empdata.perM_MMC_NAME;
        this.vpevillage = empdata.perM_WARD_VILLAGE_NAME;
        this.vpehno = empdata.perM_HOUSE_NO;
        this.vpestreett = empdata.perM_STREET_NAME;
        this.vpelanmark = empdata.perM_LAND_MARK;
        this.vpepincode = empdata.perM_PINCODE;

        this.vnstate = empdata.nativE_STATE_NAME;
        this.vndistrict = empdata.nativE_DISTRICT_NAME;
        this.vnru = empdata.nativE_RURAL_URBAN_NAME;
        this.vnmandal = empdata.nativE_MMC_NAME;
        this.vnvillage = empdata.nativE_WARD_VILLAGE_NAME;
        this.vnhno = empdata.nativE_HOUSE_NO;
        this.vnstreett = empdata.nativE_STREET_NAME;
        this.vnlanmark = empdata.nativE_LAND_MARK;
        this.vnpincode = empdata.nativE_PINCODE;

        this.vloctype = empdata.worK_LOCATION_TYPE;
        this.vlocname = empdata.worK_LOCATION;
        this.vsectionname = empdata.sectioN_NAME;
        this.vrmanager = empdata.reportinG_OFFICER_NAME;
        this.vastatus = empdata.iS_ACTIVE;

        this.vifsccode = empdata.ifsC_CODE;
        this.vaccname = empdata.accounT_HOLDER_NAME;
        this.vaccno = empdata.accounT_NUMBER_MASK;
        this.vbankname = empdata.banK_NAME;
        this.vbranch = empdata.banK_BRANCH;

        this.vuanno = empdata.uaN_NO;
        this.vpfno = empdata.pF_NO;
        this.vpfdate = (empdata.pF_DATE ? empdata.pF_DATE : "").replace("T00:00:00", "");
        this.vpfoffice = empdata.pF_OFFICE;
        this.vpfaccno = empdata.pF_AC_NO;
        this.vpanno = empdata.paN_NO;


        this.V_Height = empdata.height;
        this.V_Mothertongue = empdata.motheR_TONGUE;
        this.V_Nativeplace = empdata.emP_NATIVE_PLACE;
        this.V_identification1 = empdata.marK_IDENTIFICATION1;
        this.V_identification2 = empdata.marK_IDENTIFICATION2;


      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }



  GetEmployeeDetails(emP_CODE: string, ptype: string) {




    const rtype: string = ptype;
    const req = new InputRequest();
    req.INPUT_01 = emP_CODE;
    // var data = { 'INPUT_01': this.f.EmpAadhaar.value };
    this.service.postData(req, "GetEmployeeDetails").subscribe(data => {

      console.log(data.Details[0]);

      if (data.StatusCode == "100") {
        const empdata = data.Details[0];
        if (empdata) {
          //if (rtype == 'edit')
          //this.FillEditEmployeeDetails(empdata);
          if (rtype == 'view')
            this.FillViewEmployeeDetails(empdata);
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }

  GetEmpFamilyDetails(emP_CODE: string, ptype: string) {
    const rtype: string = ptype;
    const req = new InputRequest();
    req.INPUT_01 = emP_CODE;
    // var data = { 'INPUT_01': this.f.EmpAadhaar.value };
    this.service.postData(req, "GetEmpFamilyDetails").subscribe(data => {

      console.log(data);

      if (data.StatusCode == "100") {
        const empdata = data.Details;
        if (rtype == 'view') {
          this.familylist = empdata;
          console.log(this.familylist);
        }

      }



    },
      error => console.log(error));


  }



  FillViewEmployeeDetails(empdata: any) {
    this.vemptype = empdata.categorY_NAME;
    this.vempname = empdata.emP_NAME;
    this.vdesignation = empdata.designation;
    this.vgender = empdata.gendeR_NAME;
    this.vdob = (empdata.dob ? empdata.dob : "").replace("T00:00:00", "");
    this.vdoj = (empdata.joininG_DATE ? empdata.joininG_DATE : "").replace("T00:00:00", "");
    this.veducation = empdata.educatioN_QUALIFICATION;
    this.vbloodgrp = empdata.blooD_GROUP1;
    this.vemail = empdata.emaiL_ID;
    this.vmobile = empdata.mobilE_NUMBER;
    this.vecno = empdata.emergencY_CONTACT_NO;
    this.vexpears = empdata.experiancE_YEARS;
    this.vexmonths = empdata.experiancE_MONTH;
    this.vnationality = empdata.nationality;
    this.vreligion = empdata.religion;
    this.vcommunity = empdata.communitY_NAME;
    this.vmstatus = empdata.martiaL_STATUS;
    this.vspouname = empdata.spousE_NAME;
    this.vanaadate = (empdata.anniversarY_DATE ? empdata.anniversarY_DATE : "").replace("T00:00:00", "");
    this.decryptFile(empdata.emP_PHOTO);
    this.vaadhaarphoto = empdata.emP_PHOTO;

    this.vpdistrict = empdata.preS_DISTRICT_NAME;
    this.vpru = empdata.preS_RURAL_URBAN_NAME;
    this.vpmandal = empdata.preS_MMC_NAME;
    this.vpvillage = empdata.preS_WARD_VILLAGE_NAME;
    this.vphno = empdata.preS_HOUSE_NO;
    this.vpstreett = empdata.preS_STREET_NAME;
    this.vplanmark = empdata.preS_LAND_MARK;
    this.vppincode = empdata.preS_PINCODE;

    this.vpedistrict = empdata.perM_DISTRICT_NAME;
    this.vperu = empdata.perM_RURAL_URBAN_NAME;
    this.vpemandal = empdata.perM_MMC_NAME;
    this.vpevillage = empdata.perM_WARD_VILLAGE_NAME;
    this.vpehno = empdata.perM_HOUSE_NO;
    this.vpestreett = empdata.perM_STREET_NAME;
    this.vpelanmark = empdata.perM_LAND_MARK;
    this.vpepincode = empdata.perM_PINCODE;

    this.vnstate = empdata.nativE_STATE_NAME;
    this.vndistrict = empdata.nativE_DISTRICT_NAME;
    this.vnru = empdata.nativE_RURAL_URBAN_NAME;
    this.vnmandal = empdata.nativE_MMC_NAME;
    this.vnvillage = empdata.nativE_WARD_VILLAGE_NAME;
    this.vnhno = empdata.nativE_HOUSE_NO;
    this.vnstreett = empdata.nativE_STREET_NAME;
    this.vnlanmark = empdata.nativE_LAND_MARK;
    this.vnpincode = empdata.nativE_PINCODE;

    this.vloctype = empdata.worK_LOCATION_TYPE;
    this.vlocname = empdata.worK_LOCATION;
    this.vsectionname = empdata.sectioN_NAME;
    this.vrmanager = empdata.reportinG_OFFICER_NAME;
    this.vastatus = empdata.iS_ACTIVE;

    this.vifsccode = empdata.ifsC_CODE;
    this.vaccname = empdata.accounT_HOLDER_NAME;
    this.vaccno = empdata.accounT_NUMBER_MASK;
    this.vbankname = empdata.banK_NAME;
    this.vbranch = empdata.banK_BRANCH;

    this.vuanno = empdata.uaN_NO;
    this.vpfno = empdata.pF_NO;
    this.vpfdate = (empdata.pF_DATE ? empdata.pF_DATE : "").replace("T00:00:00", "");
    this.vpfoffice = empdata.pF_OFFICE;
    this.vpfaccno = empdata.pF_AC_NO;
    this.vpanno = empdata.paN_NO;
  }


  decryptFile(filepath) {
    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          console.log(data.docBase64);
          this.vempphoto = data.docBase64;
        },
          error => {
            console.log(error);
            this.vempphoto = "";
          });
    }
    else
      this.vempphoto = "";
  }


  HistoryAllEmployees(): void {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_02 = "EMPLOYEES";


    this.service.postData(req, "GetEmployeeHistory").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        console.log(data);
        this.allemphistory = data.Details;
        for (let i = 0; i < this.allemphistory.length; i++) {
          let colname = this.allemphistory[i].columN_NAME;
          let oldvalue = this.allemphistory[i].olD_VALUE;
          let newvalue = this.allemphistory[i].neW_VALUE;
          if (colname == "EMP_PHOTO" && oldvalue) {
            this.decryptHistoryFile(oldvalue, i, 'oldvalue');
          }
          if (colname == "EMP_PHOTO" && newvalue) {
            // this.allemphistory[i].neW_VALUE = this.service.getBase64(newvalue);
            // console.log(this.allemphistory[i].neW_VALUE);
            this.decryptHistoryFile(newvalue, i, 'newvalue');
          }
        }
        this.historyAllSection.show();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }



  decryptHistoryFile(filepath, i, ftype) {
    const coltype: string = ftype;
    const indx = i;
    if (coltype == 'oldvalue')
      this.allemphistory[indx].olD_VALUE = "";
    else
      this.allemphistory[indx].neW_VALUE = "";

    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (coltype == 'oldvalue')
            this.allemphistory[indx].olD_VALUE = data.docBase64;
          else
            this.allemphistory[indx].neW_VALUE = data.docBase64;
        })
    }
  };

  decryptempHistoryFile(filepath, i, ftype) {
    const coltype: string = ftype;
    const indx = i;
    if (coltype == 'oldvalue')
      this.emphistory[indx].olD_VALUE = "";
    else
      this.emphistory[indx].neW_VALUE = "";

    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (coltype == 'oldvalue')
            this.emphistory[indx].olD_VALUE = data.docBase64;
          else
            this.emphistory[indx].neW_VALUE = data.docBase64;
        })
    }
  };

  HistoryEmployee(row): void {
    this.showloader = true;
    const data = row.rowData;
    const req = new InputRequest();
    req.INPUT_01 = data.emP_CODE;

    this.service.postData(req, "GetEmployeeHistory").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.emphistory = data.Details;
        for (let i = 0; i < this.emphistory.length; i++) {
          let colname = this.emphistory[i].columN_NAME;
          let oldvalue = this.emphistory[i].olD_VALUE;
          let newvalue = this.emphistory[i].neW_VALUE;
          if (colname == "EMP_PHOTO" && oldvalue) {
            this.decryptempHistoryFile(oldvalue, i, 'oldvalue');
          }
          if (colname == "EMP_PHOTO" && newvalue) {
            this.decryptempHistoryFile(newvalue, i, 'newvalue');
          }
        }
        this.historySection.show();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }




  resetallforms() {

    this.employeeLeaveForm.reset();

    this.EmpLeaveCtrl.reset();
    this.EmpLeaveCtrl.clear();


    this.editModal.hide();

    this.consubmitted = false;

    this.AddLeave();

  }




  onDateSelection(event) {

  }

  LoadLeaveType() {
    this.showloader = true;

    const req = new InputRequest();
    //req.INPUT_01 = this.logUserCode;
    req.DIRECTION_ID = "14";
    req.TYPEID = "LEAVES_ENCASHMENT";

    //req.INPUT_01 = empcode;

    this.service.postData(req, "SR_LeaveLedger_Get").subscribe(data => {
      this.showloader = false;
      console.log(data);
      if (data.StatusCode == "100") {
        this.leavetypes = data.Details;
      }

    },
      error => console.log(error));
  }

  LoadEmployeeList() {
    this.showloader = true;
    this.service.getData("GetEmpList").subscribe(data => {
      console.log(data);
      if (data.StatusCode == "100") {
        this.employees = data.Details;
        this.EmployeerowData = data.Details;
        this.EmployeerowData = this.EmployeerowData.filter(function (el) { return el.servicE_TYPE == "Regular" });

        if (this.EmployeerowData.length > 0) {
          this.gridApi.setRowData(this.EmployeerowData);
        }
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));
  }

  fillGriddata(empcode: string) {
    this.showloader = true;


    const req = new InputRequest();


    req.DIRECTION_ID = "14";
    req.TYPEID = "GET_SR_LEAVES";

    req.INPUT_01 = empcode;

    this.showloader = true;
    this.service.postData(req, "SR_LeaveLedger_Get").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.GridFill = data.Details;
        this.GridFillRowData = data.Details;
        this.gridApi_Leave.setRowData(this.GridFillRowData);


      }
      else {
        Swal.fire('warning', data.StatusMessage, 'warning');
        this.GridFillRowData = [];
        this.gridApi_Leave.setRowData(this.GridFillRowData);

        this.showloader = false;
      }

    },
      error => console.log(error));


  }


}
