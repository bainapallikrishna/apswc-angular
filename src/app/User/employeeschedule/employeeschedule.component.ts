
//import {Component, OnInit} from '@angular/core';
import { CalendarOptions, FullCalendarModule } from '@fullcalendar/angular'; // useful for typechecking
import { Component, AfterViewChecked, OnInit, ViewChild, ElementRef } from '@angular/core';

import { FullCalendarComponent } from '@fullcalendar/angular';
import { EventInput, Calendar, startOfDay } from '@fullcalendar/core';
import dayGridPlugin from '@fullcalendar/daygrid';
//import timeGrigPlugin from '@fullcalendar/timegrid'; 
import interactionPlugin from '@fullcalendar/interaction';
//import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { CommonServices } from 'src/app/Services/common.services';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common'; // useful for typechecking
import { EmpEvents } from 'src/app/Interfaces/user';
//import { NgTippyModule, NgTippyService,Options } from 'angular-tippy';

@Component({
  selector: 'app-employeeschedule',
  templateUrl: './employeeschedule.component.html',
  styleUrls: ['./employeeschedule.component.css']
})
export class EmployeescheduleComponent implements OnInit {


  @ViewChild('empinoutModal') empinoutModal: ModalDirective;
  @ViewChild('empNotesModal') empNotesModal: ModalDirective;
  @ViewChild('empNotesdisplayModal') empNotesdisplayModal: ModalDirective;


  @ViewChild('fc') fc: FullCalendarModule;

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");


  constructor(private formBuilder: FormBuilder, private service: CommonServices, private datef: CustomDateParserFormatter,
    private datecon: NgbDatepickerConfig, private router: Router, public datepipe: DatePipe) {

    if (!this.logUserrole) {
      this.router.navigate(['/Login']);
      return;
    }
  }

  CalDataLoad: boolean = false;

  consubmitted: boolean = false;
  EmpEvents: EmpEvents;
  employeeeventsList: any[] = [];

  calendarOptions: CalendarOptions;
  EmpNotesForm: FormGroup;
  lattitude: string;
  longitute: string;
  isNotesAdd: boolean = false;
  get EmpNotesFormCtrl() { return this.EmpNotesForm.controls; }

  ngOnInit(): void {


    this.showloader = true;
    this.CalDataLoad = true;
    //this.isNotesAdd=true;

    setTimeout(() => {
      this.LoadEmployeeEvents();
    }, 2000);
    setTimeout(() => {
      this.calendarOptions = {
        initialView: 'dayGridMonth',
        //dateClick: this.handleDateClick.bind(this), // bind is important!
        events: this.employeeeventsList,
        //eventRender :(event) => this._eventRender(event),
        //eventr:this.eventRender.bind(this),
        eventClick: this.eventClick.bind(this),
        eventContent: this.renderEventContent.bind(this),
        eventDidMount: this.renderEventstyle.bind(this),
        eventOrder: 'displayorder',
        firstDay: 1,

        headerToolbar: {
          left: 'prev,next today,prevYear,nextYear',
          center: 'title',
          right: 'dayGridMonth,dayGridWeek,dayGridDay'
        },
        //dayMaxEvents: true,
      };

    }, 5000);

    setTimeout(() => {
      this.CalDataLoad = false;
    }, 5000);

    this.EmpNotesForm = this.formBuilder.group({
      Notes: ['', Validators.required],
    });

    this.Locataion();

    this.LoadEmp_Remainders();

  }
  Mainyear: string;
  Mainyearchange(event) { this.refreshcalendar(); }

  // eventsfromdb:any[] =  [
  //   {title: 'IN',start:new Date(),end:new Date(), id:'1',"className": "inout",backgroundColor:"black",textColor:"wight",allDay : true},
  //   {title: 'Sriamnavami',start:"2021-04-25",end:"2021-04-29", id:'2',backgroundColor:"black",textColor:"wight",allDay : true },
  //   {title: 'UGADI', start:"2021-04-25",end:"2021-04-25",id:'2',backgroundColor:"black",textColor:"wight",allDay : true },

  // ];



  // this.employeeeventsList.push({title: 'ADDNOTES',start:"2021-04-27",end:new Date(), 
  // id:'1',"className": "INOUT",backgroundColor:"black",textColor:"wight",allDay : true,URL:"",displayorder:'2'});


  // this.employeeeventsList.push({title: 'IN',start:"2021-04-27",end:new Date(), 
  // id:'1',"className": "INOUT",backgroundColor:"black",textColor:"wight",allDay : true,URL:"",displayorder:'3'});

  today: any[];
  thisweek: any[];
  nextweek: any[];
  nextmonth: any[];


  refreshcalendar() {
    this.CalDataLoad = true;
    setTimeout(() => {
      this.LoadEmployeeEvents();
    }, 2000);
    setTimeout(() => {

      this.calendarOptions = {
        initialView: 'dayGridMonth',
        //dateClick: this.handleDateClick.bind(this), // bind is important!
        events: this.employeeeventsList,
        //eventRender :(event) => this._eventRender(event),
        //eventr:this.eventRender.bind(this),
        eventClick: this.eventClick.bind(this),
        eventContent: this.renderEventContent.bind(this),
        eventDidMount: this.renderEventstyle.bind(this),
        eventOrder: 'displayorder',
        firstDay: 1,
        headerToolbar: {
          left: 'prev,next today,prevYear,nextYear',
          center: 'title',
          right: 'dayGridMonth,dayGridWeek,dayGridDay'
        },
        //dayMaxEvents: true,
      };

    }, 5000);

    setTimeout(() => {
      this.CalDataLoad = false;
    }, 5000);
  }


  SubmitEmpNotes() {

    this.consubmitted = true;

    // if(this.longitute=="" || this.longitute=="null" || this.longitute == undefined || this.lattitude=="" || this.lattitude=="null" || this.lattitude == undefined)
    // {
    //      this.Locataion(); 

    //      Swal.fire('warning', "Please Click Allow To Know Your Location..!", 'warning');

    //      return false;
    // }

    if (this.EmpNotesForm.invalid) {
      return false;
    }

    if (this.EmpNotesFormCtrl.Notes.value.trim() == "") {
      Swal.fire('warning', "Notes is Required..!", 'warning');
      return false;
    }


    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = this.longitute;
    req.INPUT_03 = this.lattitude;
    req.INPUT_04 = this.EmpNotesFormCtrl.Notes.value;
    req.INPUT_05 = this.NotesDate;


    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;

    this.showloader = true;

    this.service.postData(req, "Emp_Notes_Save").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          //this.empNotesModal.hide();
          //this.refreshcalendar();
          this.isNotesAdd = false;;

          Swal.fire('success', "Notes  Added  Successfully !!!", 'success');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }

  renderEventstyle(elemment) {

    if (elemment.el.offsetParent.className == "fc-daygrid-event-harness" && (elemment.event._def.title == "ADDNOTES" || elemment.event._def.title == "INOUT" || elemment.event._def.title == "NOTES")) {
      elemment.el.offsetParent.className = 'fc-daygrid-event-harness self-left';
    }
    if (elemment.el.offsetParent.className == "fc-daygrid-event-harness" && (elemment.event._def.title == "IN" || elemment.event._def.title == "OUT")) {
      elemment.el.offsetParent.className = 'fc-daygrid-event-harness self-right';
    }


    //this.option = {content:elemment.event._def.title};
    //this.tippyService.init(elemment.el,this.option);
    //elemment.el[0].querySelectorAll(".fc-content")[0].setAttribute("data-tooltip", event.event.title);

  }

  handleDateClick(arg) {

  }

  Checkinoutlist: any[];
  CheckNoteslist: any[]

  // LoadEmpCheckIn_Out(eventdata: any) {
  //   const req = new InputRequest();
  //   req.INPUT_01 = this.logUserName;
  //   req.INPUT_02 = this.datepipe.transform(eventdata.event.start, 'yyyy-MM-dd');
  //   req.INPUT_11 = "IN_OUT";
  //   req.CALL_SOURCE = "WEB";
  //   req.USER_NAME = this.logUserName;

  //   this.showloader = true;

  //   this.service.postData(req, "Get_Emp_CheckIn_Out_Details").subscribe(data => {
  //     this.showloader = false;

  //     if (data.StatusCode == "100") {
  //       console.log(data.Details);
  //       this.Checkinoutlist = data.Details;
  //       this.empinoutModal.show();
  //     }
  //     else {

  //       Swal.fire("info", data.StatusMessage, "info");
  //       this.empinoutModal.hide();
  //       this.showloader = false;
  //     }
  //   },
  //     error => console.log(error));
  // }
  notesview: string = "";
  LoadEmpNotes(eventdata: any) {
    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = this.datepipe.transform(eventdata.event.start, 'yyyy-MM-dd');
    req.INPUT_11 = "NOTES";
    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;

    this.showloader = true;
    this.EmpNotesFormCtrl.Notes.setValue("");
    this.service.postData(req, "Get_Emp_CheckIn_Out_Details").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        if (data.Details != null && data.Details.length > 0) {
          this.CheckNoteslist = data.Details;
          this.EmpNotesFormCtrl.Notes.setValue(data.Details[0].notes);
          this.isNotesAdd = true;
        }
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.empNotesdisplayModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));
  }

  remainder: any[];
  todaynodata: boolean = false;
  thisweeknodata: boolean = false;
  nextweeknodata: boolean = false;
  nextmonthnodata: boolean = false;


  LoadEmp_Remainders() {
    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    //req.INPUT_02 = this.datepipe.transform(eventdata.event.start, 'yyyy-MM-dd');
    req.INPUT_11 = "REMINDER";
    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;

    this.showloader = true;

    this.service.postData(req, "Get_Emp_CheckIn_Out_Details").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {


        this.today = data.Details.filter(
          (elem) => elem.evenT_DURATION === "TODAY");

        if (this.today.length > 0) { this.todaynodata = false; }
        else { this.todaynodata = true; }

        this.thisweek = data.Details.filter(
          (elem) => elem.evenT_DURATION === "THIS WEAK");

        if (this.thisweek.length > 0) { this.thisweeknodata = false; }
        else { this.thisweeknodata = true; }


        this.nextweek = data.Details.filter(
          (elem) => elem.evenT_DURATION === "NEXT WEAK");

        if (this.nextweek.length > 0) { this.nextweeknodata = false; }
        else { this.nextweeknodata = true; }



        this.nextmonth = data.Details.filter(
          (elem) => elem.evenT_DURATION === "NEXT MONTH");

        if (this.nextmonth.length > 0) { this.nextmonthnodata = false; }
        else { this.nextmonthnodata = true; }



      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        //this.empinoutModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));
  }

  LoadEmployeeEvents() {



    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    req.INPUT_02 = this.longitute;
    req.INPUT_03 = this.lattitude;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;

    this.showloader = true;

    this.service.postData(req, "Emp_All_Events").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.employeeeventsList = [];
        data.Details.forEach((val, i) => {
          this.EmpEvents = {
            title: val.evenT_NAME,
            start: val.evenT_FROM_DATE,
            end: val.evenT_END_DATE,
            description: val.evenT_DISCRIPTION,
            id: i,
            className: val.clasS_NAME,
            backgroundColor: 'wight',
            textColor: val.colour,
            allDay: true,
            displayorder: val.displaY_ORDER,
            inouttime: val.time
          };
          this.employeeeventsList.push(this.EmpEvents);


        })

      }
    },

      error => console.log(error));
    this.showloader = false;

  }

  NotesDate: string;
  isNotesSavebtn: boolean = false;
  eventClick(info) {

    var clickedevent = info.event._def;

    if (clickedevent.title == "INOUT") {
      //this.Emp_checkin_out("IN");
      //this.LoadEmpCheckIn_Out(info);

    }
    else if (clickedevent.title == "NOTES") {

      this.LoadEmpNotes(info);
      this.isNotesSavebtn = false;

    }
    else if (clickedevent.title == "ADDNOTES") {
      this.LoadEmpNotes(info);
      this.consubmitted = false;

      //this.EmpNotesFormCtrl.Notes.setValue("");

      this.NotesDate = this.datepipe.transform(info.event.start, 'yyyy-MM-dd');
      //this.empNotesModal.show();
      this.isNotesSavebtn = true;

      this.isNotesAdd = true;

    }
    else if (clickedevent.title == "IN") { this.Emp_checkin_out(info.event._def); }
    else if (clickedevent.title == "OUT") { this.Emp_checkin_out(info.event._def); }


  }

  showloader: boolean = false;

  calendarEvents: EventInput[] = [];


  loadEvents() {
    const event = {
      title: 'test',
      start: Date.now(),
      allDay: true
    };
  }

  //Event Render Function
  renderEventContent(eventInfo, createElement) {

    var innerHtml;

    // if (eventInfo.event._def.title == "ATTENDENCE_IN") {

    //   innerHtml = eventInfo.event._def.title;

    //   innerHtml = '<i class="fa fa-calendar"> '+ eventInfo.event._def.extendedProps.inouttime +' </i>'

    //   return createElement = { html: '<div>' + innerHtml + '</div>' }
    // }
    if (eventInfo.event._def.title == "NOTES") {
      innerHtml = eventInfo.event._def.title;

      innerHtml = '<i class="fa fa-sticky-note"></i>'

      return createElement = { html: '<div>' + innerHtml + '</div>' }
    }
    if (eventInfo.event._def.title == "ADDNOTES") {
      innerHtml = eventInfo.event._def.title;

      innerHtml = '<i class="fa fa-edit"></i>'

      return createElement = { html: '<div>' + innerHtml + '</div>' }
    }
    //Check if event has image
    if (eventInfo.event._def.extendedProps.imageUrl) {

      // Store custom html code in variable
      innerHtml = eventInfo.event._def.title + "<img style='width:100px;' src='" + eventInfo.event._def.extendedProps.imageUrl + "'>";
      //Event with rendering html
      return createElement = { html: '<div>' + innerHtml + '</div>' }
    }


    // var tooltip = new Tooltip(info.el, {
    //   title: info.event.extendedProps.description,
    //   placement: 'top',
    //   trigger: 'hover',
    //   container: 'body'
    // });

  }

  Locataion() {

    this.service.getPosition().then(pos => {
      this.longitute = pos.lat;
      this.lattitude = pos.lng;
    });
  }


  NgbDateStruct: any;

  CheckLocation() {
    if (navigator.geolocation) {
      this.service.CheckLocation().then(pos => {
        if (pos.state == 'prompt') {
          this.Locataion();
          Swal.fire({
            title: 'APSWC need location acces permission for Capturing Attendance...Please click Allow to access!',
            showDenyButton: false,
            showCancelButton: false,
            confirmButtonText: `OK`,

          }).then((result) => {
            if (result.isConfirmed) {
              //Swal.fire('Saved!', '', 'success')
            } else if (result.isDenied) {
              //Swal.fire('Changes are not saved', '', 'info')
            }
          })

          return false;
        }
        if (pos.state == 'denied') {

          this.Locataion();

          Swal.fire({
            title: 'This site has been blocked from accessing your location...!',
            showDenyButton: false,
            showCancelButton: false,
            confirmButtonText: `OK`,

          }).then((result) => {
            if (result.isConfirmed) {
              //Swal.fire('Saved!', '', 'success')
            } else if (result.isDenied) {
              //Swal.fire('Changes are not saved', '', 'info')
            }
          })

          return false;
        }
        if (pos.state == 'granted') {
          //Swal.fire('warning', "granted", 'warning');
          return true;
        }
      });
    }
    else {
      Swal.fire('Geolocation is not supported for this Browser/OS.');
    }

  }

  closenotes() {
    this.isNotesAdd = false;

  }

  Emp_checkin_out(info: any) {

    this.CheckLocation();

    let value = info.title;

    let now: Date = new Date();
    //this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }

    const req = new InputRequest();
    req.INPUT_01 = this.logUserName;
    if (value == "IN") {
      req.INPUT_02 = this.lattitude;
      req.INPUT_03 = this.longitute;
    }
    else {
      req.INPUT_05 = this.lattitude;
      req.INPUT_06 = this.longitute;
    }
    req.INPUT_04 = value;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;

    this.showloader = true;

    this.service.postData(req, "Emp_CheckIn_Out").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {

          //this.router.rel
          // if (value == "NOTES") {
          //this.LoadEmpCheckIn_Out(info);
          //this.refreshcalendar();
          //}
          // if (value == "IN") {

          //   // this.employeeeventsList.push({
          //   //   title: 'OUT', start: new Date(), end: new Date(),
          //   //   id: '1', "className": "INOUT", backgroundColor: "", textColor: "", allDay: true, displayorder: '3',time:""
          //   // });

          //   var removeIndex = this.employeeeventsList.map(function (item) { return item.title; }).indexOf("IN");

          //   this.employeeeventsList.splice(removeIndex, 1);

          //   this.calendarOptions.events = this.employeeeventsList

          // }
          // else {

          //   // this.employeeeventsList.push({
          //   title: 'IN', start: new Date(), end: new Date(),
          //   id: '1', "className": "INOUT", backgroundColor: "", textColor: "", allDay: true, displayorder: '3'
          // });

          //   var removeIndex = this.employeeeventsList.map(function (item) { return item.title; }).indexOf("OUT");

          //   this.employeeeventsList.splice(removeIndex, 1);

          //   this.calendarOptions.events = this.employeeeventsList;
          // }
          this.refreshcalendar();
          //Swal.fire('success', "Check " + value + "  Successfully !!!", 'success');
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }

}



