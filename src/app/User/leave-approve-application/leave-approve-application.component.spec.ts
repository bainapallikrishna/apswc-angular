import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeaveApproveApplicationComponent } from './leave-approve-application.component';

describe('LeaveApproveApplicationComponent', () => {
  let component: LeaveApproveApplicationComponent;
  let fixture: ComponentFixture<LeaveApproveApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeaveApproveApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeaveApproveApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
