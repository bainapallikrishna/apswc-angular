import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { CommonServices } from 'src/app/Services/common.services';
import { leaveApprovegridbuttoncomponent } from 'src/app/custome-directives/LeaveApprovegridbutton-renderer.component';
import { AgGridAngular } from 'ag-grid-angular';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { Router } from '@angular/router';

@Component({
  selector: 'app-leave-approve-application',
  templateUrl: './leave-approve-application.component.html',
  styleUrls: ['./leave-approve-application.component.css']
})
export class LeaveApproveApplicationComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('historyempModal') historyempModal: ModalDirective;


  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");


  columnDefs = [
    { headerName: '#', maxWidth: 50, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Application ID', maxWidth: 120, field: 'applicatioN_ID', sortable: true, filter: true },
    { headerName: 'Name', maxWidth: 150, field: 'emP_NAME', sortable: true, filter: true },
    { headerName: 'From Date', maxWidth: 120, field: 'froM_DATE', sortable: true, filter: true },
    { headerName: 'To Date', maxWidth: 120, field: 'tO_DATE', sortable: true, filter: true },
    { headerName: 'Leave Type', maxWidth: 150, field: 'leavE_TYPE', sortable: true, filter: true },
    { headerName: 'No.Of Days', maxWidth: 150, field: 'nO_OF_LEAVES', sortable: true, filter: true },
    { headerName: 'Status', maxWidth: 120, field: 'status', sortable: true, filter: true },
    { headerName: '', maxWidth: 0, field: 'emP_CODE', hide: true },

    {
      headerName: 'Action', width: 350, cellRenderer: 'buttonRenderer',
      cellRendererParams: {


        btnEdit: 'Approve',
        btnReject: 'Reject',
        btnView: 'View',
        btnHistory: 'History',
        ApproveClick: this.ApproveClick.bind(this),
        RejectClick: this.RejectClick.bind(this),
        ViewClick: this.ViewEmpLeave.bind(this),
        historyClick: this.HistoryLeave.bind(this)



      },
    }
  ];
  Application_ID: string;
  LeaveType_View: string;
  FromDate_VIEW: string;
  ToDate_VIEW: string;
  NoOfDays_VIEW: string;
  ReportingMgr_VIEW: string;
  ContactNo_VIEW: string;
  status_VIEW: string;
  Reason_VIEW: string;
  EmpName_VIEW: string;
  Designation_VIEW: string;
  LeaveDuration_VIEW: string;
  ApprovedDate_VIEW: string;
  Approved_by: string;
  Encashment_VIEW: string;
  Designation: string;
  currentdate = new Date();
  frameworkComponents: any;
  public components;
  showloader: boolean = true;

  employeeLeaveForm: FormGroup;

  consubmitted: boolean = true;

  leavetypes: any = [];
  NgbDateStruct: any;


  MasterHistlist: any[];

  EMP_CODE: string;
  EMP_NAME: string;
  REPORTING_MGR: string;
  EMR_NUM: string;

  GridFill: any[];
  GridFillRowData: any[];

  gridApi: any = [];
  defaultColDef: any = [];
  gridColumnApi: any = [];

  currentyear: number;
  get EmpLeaveCtrl() { return this.employeeLeaveForm.controls; }

  constructor(private formBuilder: FormBuilder, private service: CommonServices, private datef: CustomDateParserFormatter,
    private datecon: NgbDatepickerConfig, private router: Router) {

    if (!this.logUserrole) {
      this.router.navigate(['/Login']);
      return;
    }

    this.frameworkComponents = {
      buttonRenderer: leaveApprovegridbuttoncomponent,

    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };



  }
  ngOnInit(): void {

    this.showloader = false;

    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }


    this.currentyear = now.getFullYear();





    this.fillGriddata();


    //this.LoadLeaveTypes();


  }



  resetallforms() {

    //this.employeeLeaveForm.reset();

    //this.EmpLeaveCtrl.LeaveDay.setValue("");
    //this.EmpLeaveCtrl.LeaveType.setValue("");

    //this.EmpLeaveCtrl.FromDate.setValue("");
    //this.EmpLeaveCtrl.ToDate.setValue("");
    //this.EmpLeaveCtrl.NoofDays.setValue("");
    //this.EmpLeaveCtrl.Reason.setValue("");

    //this.consubmitted = false;
  }



  RejectClick(row: any) {
    const rowdata = row.rowData;

    Swal.fire({
      title: 'Are you sure?',
      html: "You want to Reject the Leave Application " + rowdata.applicatioN_ID + "!" + "<br/><br/>" + "Reason",
      //type:input,
      input: 'text',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes',
      allowOutsideClick: false,
      inputValidator: value => value.trim() == "" && 'Reason is Required !'
    }).then((result) => {

      if (result.isConfirmed) {

        const req = new InputRequest();
        req.INPUT_01 = rowdata.applicatioN_ID;
        req.INPUT_02 = "Rejected";
        req.INPUT_03 = result.value.trim();
        req.CALL_SOURCE = "WEB";
        req.USER_NAME = this.logUserName;

        this.service.postData(req, "EmpLeave_Cancel").subscribe(data => {
          this.showloader = false;

          if (data.StatusCode == "100") {
            let result = data.Details[0];
            if (result.rtN_ID === 1) {

              this.fillGriddata();
              //this.editModal.hide();
              Swal.fire('success', "Leave Application has been Rejected Successfully !!!", 'success');
            }
            else {
              Swal.fire('warning', result.statuS_TEXT, 'warning');
            }

          }
          else
            Swal.fire('warning', data.StatusMessage, 'warning');

        },
          error => console.log(error));


      }
    })

  }
  ApproveClick(row: any) {
    const rowdata = row.rowData;

    Swal.fire({
      title: 'Are you sure?',
      text: "You want to Approve the Leave Application " + rowdata.applicatioN_ID + " !",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      cancelButtonText: 'No',
      confirmButtonText: 'Yes'
    }).then((result) => {
      if (result.isConfirmed) {

        const req = new InputRequest();
        req.INPUT_01 = rowdata.applicatioN_ID;
        //req.INPUT_01 = "2021";
        req.INPUT_02 = "Approved";
        req.CALL_SOURCE = "WEB";
        req.USER_NAME = this.logUserName;

        this.showloader = true;

        this.service.postData(req, "EmpLeave_Cancel").subscribe(data => {
          this.showloader = false;

          if (data.StatusCode == "100") {
            let result = data.Details[0];
            if (result.rtN_ID === 1) {

              this.fillGriddata();
              //this.editModal.hide();
              Swal.fire('success', "Leave Application has been Approved Successfully !!!", 'success');
            }
            else {
              Swal.fire('warning', result.statuS_TEXT, 'warning');
            }

          }
          else
            Swal.fire('warning', data.StatusMessage, 'warning');

        },
          error => console.log(error));


      }
    })
  }

  ViewEmpLeave(row: any) {
    this.showloader = true;
    const datarow = row.rowData;
    const req = new InputRequest();
    req.INPUT_01 = datarow.emP_CODE;
    req.INPUT_02 = "0";
    req.INPUT_03 = datarow.applicatioN_ID;

    this.service.postData(req, "GetEmpLeaves").subscribe(data => {
      if (data.StatusCode == "100") {
        this.Application_ID = data.Details[0].applicatioN_ID;
        this.FromDate_VIEW = data.Details[0].froM_DATE.replace("T00:00:00", "");
        this.ToDate_VIEW = data.Details[0].tO_DATE.replace("T00:00:00", "");
        this.NoOfDays_VIEW = data.Details[0].nO_OF_LEAVES;
        this.ReportingMgr_VIEW = data.Details[0].reportinG_OFFICER;
        this.ContactNo_VIEW = data.Details[0].emergencY_CONTACT_NO;
        this.LeaveType_View = data.Details[0].leavE_TYPE;

        //this.LeaveDuration_VIEW = "FromDate : " + data.Details[0].froM_DATE + "<br>" + "To Date" + data.Details[0].tO_DATE;
        this.EmpName_VIEW = data.Details[0].emP_NAME;
        this.Approved_by = data.Details[0].approveD_BY;
        this.Designation = data.Details[0].designation;

        this.status_VIEW = data.Details[0].status;
        this.Reason_VIEW = data.Details[0].reason;

        this.ApprovedDate_VIEW = data.Details[0].approveD_DATE.replace("T00:00:00", "");
        this.Encashment_VIEW = data.Details[0].encashment;

        this.viewModal.show();

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));


  }
  HistoryLeave(row: any) { this.Historyrow(row); }
  HistoryAll() { this.history_All(); }
  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.GridFillRowData);

  }



  fillGriddata() {

    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = "0";
    req.INPUT_02 = "0";
    req.INPUT_03 = "0";
    req.INPUT_04 = this.logUserName;;
    this.service.postData(req, "GetEmpLeaves").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.GridFill = data.Details;
        this.GridFillRowData = data.Details;
        this.gridApi.setRowData(this.GridFillRowData);


      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));


  }
  history_All() {
    const req = new InputRequest();

    req.INPUT_02 = "EMP_LEAVES";

    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }

  Historyrow(row): void {
    this.showloader = true;
    const data = row.rowData;
    const req = new InputRequest();
    req.INPUT_01 = "0";
    req.INPUT_03 = data.applicatioN_ID;
    req.INPUT_02 = "EMP_LEAVES";
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }

}
