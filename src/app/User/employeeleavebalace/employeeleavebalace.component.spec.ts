import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeleavebalaceComponent } from './employeeleavebalace.component';

describe('EmployeeleavebalaceComponent', () => {
  let component: EmployeeleavebalaceComponent;
  let fixture: ComponentFixture<EmployeeleavebalaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeleavebalaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeleavebalaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
