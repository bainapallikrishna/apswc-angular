import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { CommonServices } from 'src/app/Services/common.services';
import { AgGridAngular } from 'ag-grid-angular';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { Router } from '@angular/router';


@Component({
  selector: 'app-employeeleavebalace',
  templateUrl: './employeeleavebalace.component.html',
  styleUrls: ['./employeeleavebalace.component.css']
})
export class EmployeeleavebalaceComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('LeaveDetailsempModal') LeaveDetailsempModal: ModalDirective;
  @ViewChild('historyempModal') historyempModal: ModalDirective;



  columnDefs = [];

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");

  dynamicheader = [];
  //columnDefs = [];
  DynamicData: any[];


  //HistoryLeave(){}

  Application_ID: string;
  LeaveType_View: string;
  FromDate_VIEW: string;
  ToDate_VIEW: string;
  NoOfDays_VIEW: string;
  ReportingMgr_VIEW: string;
  ContactNo_VIEW: string;
  status_VIEW: string;
  Reason_VIEW: string;
  EmpName_VIEW: string;
  Designation_VIEW: string;
  LeaveDuration_VIEW: string;
  Approved_by: string;
  Designation: string;
  currentdate = new Date();
  frameworkComponents: any;
  public components;
  showloader: boolean = true;

  employeeLeaveForm: FormGroup;

  consubmitted: boolean = true;

  leavetypes: any = [];
  NgbDateStruct: any;


  MasterHistlist: any[];

  EMP_CODE: string;
  EMP_NAME: string;
  REPORTING_MGR: string;
  EMR_NUM: string;

  GridFill: any[];
  GridFillRowData: any[] = [];

  gridApi: any = [];
  defaultColDef: any = [];
  gridColumnApi: any = [];

  currentyear: number;
  submitdata: any[] = [];

  get EmpLeaveCtrl() { return this.employeeLeaveForm.controls; }


  ;

  constructor(private formBuilder: FormBuilder, private service: CommonServices, private datef: CustomDateParserFormatter, private router: Router) {

    if (!this.logUserrole) {
      this.router.navigate(['/Login']);
      return;
    }

    // this.frameworkComponents = {
    //   buttonRenderer: leaveApprovegridbuttoncomponent,

    // }
    //@ViewChild('historyempModal') historyemp1: ModalDirective
    //historyempModal.show();


    this.components = {





      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);

      },


      numericCellEditor: function (params) {

        
        //if (params.data['IS_UPDATE'] != '1') {
 //if (val == "Employee Code" || val == "Employee Name" || val == "Employee Type" || val == "Designation" || val == "Mobile Number") {


          if ((params.column.colId != 'Employee Code' && params.column.colId != 'Employee Name' &&
          params.column.colId != 'Employee Type'  && params.column.colId != 'Mobile Number'
          && params.column.colId != 'Designation')) {
      
            if((params.value == "YES" || params.value >=0) && params.value !=null)
            {
              var newLink = document.createElement('input');
              newLink.className = "form-control";
              //newLink.setAttribute("maxlength",2);
              newLink.type = "number";
              //newLink.maxLength = 2;
              newLink.min="0";
              //newLink.oninput="validity.valid||(value='')";
              newLink.addEventListener('input', function (this) {
                
                return this.validity.valid||(this.value='');
  
              });
              
              params.data[params.colDef.headerName] = (params.value  =="YES" ? "0" : params.value);
              if(params.value=="YES"){newLink.value="0"}
              
              else{newLink.value = params.value;}
  
              newLink.addEventListener('change', function () {
                
                if( newLink.value==""){newLink.value = params.data[params.colDef.headerName];}
                else{params.data[params.colDef.headerName] = newLink.value;
                }
  
              });
  
              return newLink;
            }
            else{return params.value;}

          }
          else{return params.value;}


          // console.log(params);
          // if ((params.value == "YES" || params.value > 0) && (params.column.colId != 'EMP_CODE' && params.column.colId != 'EMP_NAME' &&
          //   params.column.colId != 'EMP_TYPE' && params.column.colId != 'IS_UPDATE' && params.column.colId != 'MOBILE_NUMBER'
          //   && params.column.colId != 'DESIGNATION')) {
          //   var newLink = document.createElement('input');
          //   newLink.className = "form-control";
          //   //newLink.setAttribute("maxlength",2);
          //   newLink.maxLength = 2;
          //   newLink.type = "number";
          //   params.data[params.colDef.headerName] = "0";
          //   if(params.value=="YES"){newLink.value=="0"}
            
          //   else{newLink.value = params.value;}

          //   newLink.addEventListener('change', function () {

          //     params.data[params.colDef.headerName] = newLink.value;

          //   });

          //   return newLink;
          // }


          // else if (params.value == "NO" && (params.column.colId != 'EMP_CODE' &&
          //   params.column.colId != 'EMP_NAME' && params.column.colId != 'EMP_TYPE'
          //   && params.column.colId != 'DESIGNATION' && params.column.colId != 'IS_UPDATE'  && params.column.colId != 'MOBILE_NUMBER')) { return params.value = '' }
          // else
          //   return params.value;
        //}
        //else { return params.value; }
      },
      Leavebuttonrender: function (params) {

        var eSpan3 = document.createElement('button');
        eSpan3.innerHTML = 'Details';
        eSpan3.className = "btn btn-primary btn-sm";
        return eSpan3;

      },
      Historybuttonrender: function (params) {
       
        var eSpan1 = document.createElement('button');
        eSpan1.innerHTML = 'History';
        eSpan1.className="btn btn-primary btn-sm";
        
        return eSpan1;
      },
      buttonrender: function (params) {
        //if (params.data['IS_UPDATE'] != '1') {
          var eSpan = document.createElement('button');
          eSpan.innerHTML = '<i class="fa fa-pencil"></i>';
          eSpan.className = "btn";

          return eSpan;
       // }
        //else { return ""; }
      }

    };
  }

  EmpLeaveDetails: any[];


  BindData(params) {
   
    //this.gridApi.refreshCells(params);
    
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.showloader = true;
    
     this.GridFillRowData=[];
      this.DynamicData =[];
         const req = new InputRequest();
    req.INPUT_01 = "0";
    req.INPUT_04 = this.logUserName;;
    this.service.postData(req, "Emp_Leave_Balance").subscribe(data => {
      this.showloader = false;

      console.log(data);
      if (data.StatusCode == "100") {

     
        this.GridFillRowData = data.Details;
        var res = data.Details;
        

        console.log(data.Details);
        this.DynamicData = this.GridFillRowData[0];
        console.log(this.DynamicData[0]);

        for (var i = 0; i < res.length; i++) {
          for (var key in res[i]) {
            if (key.charAt(0).toUpperCase()+key.slice(1) != key) {
              res[i][key.charAt(0).toUpperCase()+key.slice(1)] = res[i][key];
              delete res[i][key]
            }
          }
        }

        let keys = Object.keys(this.DynamicData); //

        this.dynamicheader=[];

        keys.forEach((val) => {
          
          if (val != null && val != undefined) {

            if (val == "Employee Code" || val == "Employee Name" || val == "Employee Type" || val == "Designation" || val == "Mobile Number") {

              this.dynamicheader.push({ headerName: val, field: val, cellRenderer: 'numericCellEditor', width: '200px', sortable: true, filter: true })
            }
            // else if (val == "IS_UPDATE") {
            //   this.dynamicheader.push({ headerName: val, field: val, hide: true })
            // }
            else {
              this.dynamicheader.push({ headerName: val, field: val, cellRenderer: 'numericCellEditor', width: '200px', sortable: true, filter: true })

            }

          }
        })


        this.dynamicheader.push({ headerName: "", cellRenderer: 'buttonrender', width: '100px' })


        this.dynamicheader.push({ headerName: "Action", cellRenderer: 'Leavebuttonrender', width: '100px',left:'1400px !impotant' })

        this.dynamicheader.push({ headerName: "", cellRenderer: 'Historybuttonrender', width: '120px',left:'1465px !impotant' })
 

        this.gridApi.setColumnDefs(this.dynamicheader);

        params.api.setRowData(this.GridFillRowData);

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));

  }



  LeaveDetails(empid: any) {
    const req = new InputRequest();

    req.INPUT_01 = empid;

    this.showloader = true;

    this.service.postData(req, "GetEmpLeaveDetails").subscribe(data => {
      console.log(data);
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.EmpLeaveDetails = data.Details;
        this.LeaveDetailsempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.LeaveDetailsempModal.hide();
        this.EmpLeaveDetails = [];
        this.showloader = false;
      }
    },
      error => console.log(error));

  }

  
  buttonrender(params) {
    console.log(params);
    
    if (params.colDef.cellRenderer == "Leavebuttonrender") {
      console.log(params.data['Employee Code']);
      
      this.LeaveDetails(params.data['Employee Code']);


    }
    if(params.colDef.cellRenderer=="Historybuttonrender")
  {
    console.log(params.data['Employee Code']);
    this.HistoryLeave(params.data['Employee Code'])
  }
    if (params.colDef.cellRenderer == "buttonrender") {

      let keys = Object.keys(params.data); //

      let submitdata = [];
      let empcode: string;
      let ltype: string;
      let balnce: string;

      keys.forEach((val) => {
        if (val != null && val != undefined) {
          if (val == "Employee Code") {
            empcode = params.data[val]; //,LeaveType:params.data[val],Balance:params.data['Emp_Code']})
          }
          //
          
          if (val != "Employee Code" && val != "Employee Name" && val != "Employee Type" && val != "Designation" && val != "Mobile Number") {
            //submitdata .push({Emp_Code:val,LeaveType:val,Balance:val})
            ltype = val;
            balnce = params.data[ltype]
          }
          if(balnce!=null)
          {
            submitdata.push({ Emp_Code: empcode, LeaveType: ltype, Balance: balnce == null || balnce == "YES" ? 0 : balnce });
          }
          //editable: function (params) { return params.data.val === "YES" ? true : false }});
        }
      })

      console.log(submitdata);

      //submitdata:[];

      let saveempcode: String = "";
      let LeaveType: String = "";
      let Balance: String = "";


      submitdata.forEach((val, i) => {

        if (val.LeaveType != undefined) {
          if (val.Emp_Code != "" && saveempcode == "") {
            saveempcode = val.Emp_Code
          }

          LeaveType = LeaveType + ',' + val.LeaveType;

          Balance = Balance + ',' + val.Balance;

        }
      });

      const req = new InputRequest();
      req.INPUT_01 = saveempcode.toString()
      req.INPUT_02 = LeaveType.toString();
      req.INPUT_03 = Balance.toString();
      req.INPUT_04 = "";

      req.CALL_SOURCE = "WEB";
      req.USER_NAME = this.logUserName;

      this.showloader = true;

      this.service.postData(req, "Emp_Leave_Balance_Save").subscribe(data => {
        this.showloader = false;

        if (data.StatusCode == "100") {
          let result = data.Details[0];
          if (result.rtN_ID === 1) {
            //fillGriddata();
            //this.editModal.hide();
            this.BindData(params);
            Swal.fire('success', "Leave Balance Saved Successfully !!!", 'success');
          }
          else {
            Swal.fire('warning', result.statuS_TEXT, 'warning');
          }

        }
        else
          Swal.fire('warning', data.StatusMessage, 'warning');

      },
        error => console.log(error));
    }

  }

  ngOnInit(): void {

    this.columnDefs = this.dynamicheader;

    this.showloader = false;


  }


  fillGriddata(params: any) {

    this.showloader = true;
    this.GridFillRowData=[];
    this.DynamicData =[];
    this.dynamicheader=[];
    const req = new InputRequest();
    req.INPUT_01 = "0";
    req.INPUT_04 = this.logUserName;;
    this.service.postData(req, "Emp_Leave_Balance").subscribe(data => {
      this.showloader = false;

      console.log(data);
      if (data.StatusCode == "100") {

     
        this.GridFillRowData = data.Details;
        var res = data.Details;
        
        // for (var i = 0; i < res.length; i++) {
        //   for (var key in res[i]) {
        //     if (key.replace("_","") != key) {
        //       res[i][key.replace("_","")] = res[i][key];
        //       delete res[i][key]
        //     }
        //   }
        // }

        for (var i = 0; i < res.length; i++) {
          for (var key in res[i]) {
            if (key.charAt(0).toUpperCase()+key.slice(1) != key) {
              res[i][key.charAt(0).toUpperCase()+key.slice(1)] = res[i][key];
              delete res[i][key]
            }
          }
        }
        console.log(data.Details);
        this.DynamicData = this.GridFillRowData[0];
        console.log(this.DynamicData[0]);

        let keys = Object.keys(this.DynamicData); //


        keys.forEach((val) => {
          if (val != null && val != undefined) {

            if (val == "Employee Code" || val == "Employee Name" || val == "Employee Type" || val == "Designation" || val == "Mobile Number") {

              this.dynamicheader.push({ headerName: val, field: val, cellRenderer: 'numericCellEditor', width: '150px', sortable: true, filter: true })
            }
            // else if (val == "IS_UPDATE") {
            //   this.dynamicheader.push({ headerName: val, field: val, hide: true })
            // }
            else {
              this.dynamicheader.push({ headerName: val, field: val, cellRenderer: 'numericCellEditor', width: '100px', sortable: true, filter: true })

            }

          }
        })


        this.dynamicheader.push({ headerName: "", cellRenderer: 'buttonrender', width: '50px' })


        this.dynamicheader.push({ headerName: "Action", cellRenderer: 'Leavebuttonrender', width: '90px' })



        this.gridApi.setColumnDefs(this.columnDefs);

        params.api.setRowData(this.GridFillRowData);

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');
      this.showloader = false;

    },
      error => console.log(error));

  }

  history_All() {
    const req = new InputRequest();

    req.INPUT_02 = "LEAVE_BALANCE";

    this.showloader = true;
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }
  HistoryLeave(row: any) { this.Historyrow(row); }
  HistoryAll() { this.history_All(); }

  Historyrow(empid): void {
    this.showloader = true;
    //const data = row.rowData;
    const req = new InputRequest();
    req.INPUT_01 = empid;
    req.INPUT_02 = "LEAVE_BALANCE";
    this.service.postData(req, "GetMastersHistory").subscribe(data => {
      if (data.StatusCode == "100") {
        this.MasterHistlist = data.Details;
        this.historyempModal.show();
        this.showloader = false;
      }
      else {

        Swal.fire("info", data.StatusMessage, "info");
        this.historyempModal.hide();
        this.showloader = false;
      }
    },
      error => console.log(error));

  }



}