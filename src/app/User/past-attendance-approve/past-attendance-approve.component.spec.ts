import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PastAttendanceApproveComponent } from './past-attendance-approve.component';

describe('PastAttendanceApproveComponent', () => {
  let component: PastAttendanceApproveComponent;
  let fixture: ComponentFixture<PastAttendanceApproveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastAttendanceApproveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PastAttendanceApproveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
