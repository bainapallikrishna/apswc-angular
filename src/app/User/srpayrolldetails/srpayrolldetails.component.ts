import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';

import Swal from 'sweetalert2';

import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { AgGridAngular } from 'ag-grid-angular';
import { EMPSALBUTTONRendererComponent } from 'src/app/custome-directives/Empsalbutton-renderer.component';
@Component({
  selector: 'app-srpayrolldetails',
  templateUrl: './srpayrolldetails.component.html',
  styleUrls: ['./srpayrolldetails.component.css']
})
export class SrpayrolldetailsComponent implements OnInit {

    logUserrole: string = sessionStorage.getItem("logUserrole");
    isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
    logUsercode: string = sessionStorage.getItem("logUserCode");
   
    @ViewChild('viewModal') public viewModal: ModalDirective;
    @ViewChild('historySection') public historySection: ModalDirective;
    @ViewChild('historyAllSection') public historyAllSection: ModalDirective;

    @ViewChild('appointment') public appointment: ModalDirective;

   
    @ViewChild('agGrid') agGrid: AgGridAngular;
  
    showloader: boolean = true;
    ismodal: boolean = false;
    modeltitle: string = "Add";
    locationtypeid: string = "";
    empkeyword: string = "emP_NAME";
    GridFillRowData:any[];
  
    columnDefs = [
      { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer', },
      { headerName: 'Employee Name', maxWidth: 200, field: 'emP_NAME', sortable: true, filter: true },
      { headerName: 'Login Code', maxWidth: 120, field: 'emP_CODE', sortable: true, filter: true },
      { headerName: 'Employee Type', maxWidth: 150, field: 'servicE_TYPE', sortable: true, filter: true },
      { headerName: 'Designation', maxWidth: 200, field: 'designation', sortable: true, filter: true },
      { headerName: 'Mobile No', maxWidth: 120, field: 'mobilE_NUMBER', sortable: true, filter: true },
      {
        headerName: 'Action', maxWidth: 400, cellRenderer: 'buttonRenderer',
        cellRendererParams: {
          btnEdit: 'SR',
          btnView: 'View',
          btnHistory: 'History',
          editClick: this.EditEmployee.bind(this),
          viewClick: this.ViewEmployee.bind(this),
          historyClick: this.HistoryEmployee.bind(this),
  
        },
      }
    ];

  
  
    EmployeerowData = [];
  
    employee: any;
    employees: any[];
    uidstatus: boolean = true;
    uid_isvalid: boolean = true;
    Emp_mob_isvalid = true;
    Emp_Emergencymob_isvalid = true;
    PanPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
    IFSCPattern = "^[A-Z]{4}0[A-Z0-9]{6}$";
  
  
    NgbDateStruct: any;
    public isEdit: boolean = false;
    public isAdmin: boolean = false;
    public consubmitted: boolean = false;
    
    
    public screen1: boolean = true;
  
    public screen2: boolean = false;
    public backscreen: boolean = false;
  
  
    public EmpName: string = "";
    public empid: string = "";
    public service_name: string = "";
    
  
    public eventname: string = "";
    
    
    rowData: any = [];
    defaultColDef: any = [];
    gridColumnApi: any = [];
    gridApi: any = [];
  
    
    
    
    frameworkComponents: any;
    public components;
  
    allemphistory: any = [];
    emphistory: any = [];
    progress: number = 0;
    message: string = "";
  
    constructor(
      private service: CommonServices,
      private router: Router,
      private formBuilder: FormBuilder,
      private datef: CustomDateParserFormatter,
      private datecon: NgbDatepickerConfig
    ) {
      if (!this.logUserrole || this.isPasswordChanged == "0") {
        this.router.navigate(['/Login']);
      }
  
      if (this.logUserrole == "101" || this.logUserrole == "102")
        this.isAdmin = true;
      this.frameworkComponents = {
        buttonRenderer: EMPSALBUTTONRendererComponent,
  
      }
  
      this.components = {
        rowIdRenderer: function (params) {
          return '' + (params.rowIndex + 1);
        },
      };
    }
  
    
  
  
    //this.datecon.maxDate = new NgbDateStruct({year:2021,month:4:day:2}) 
  
    BindData(params) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
      //params.api.setRowData(this.EmployeerowData);
      //this.gridApi.sizeColumnsToFit();
      this.LoadEmployeeList()
    }
  
    
    BindData_Leaves(params) {
      this.gridApi = params.api;
      this.gridColumnApi = params.columnApi;
      params.api.setRowData(this.GridFillRowData);
      
    }
    
     back() {
      this.screen2 = false;
      this.screen1 = true;
  
     }
  
    
  
    ngOnInit(): void {
  
  
      let now: Date = new Date();
      this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }
  
    }
    
    EditEmpLeaveMaster()
    {

    }
    ViewEmpLeave()
    {

    }
    HistoryLeave()
    {

    }
    DetailsGet(value ){if(value=="1"){ this.appointment.show(); }}
    CloseViewModal() {
      this.viewModal.hide();
      this.LoadEmployeeList();
    }
    AddAppointment(){}
    RemoveAppointment(i){}
    SubmitAppointment(){}
    
    AddEmployee() {
      this.resetallforms();
  
      this.uid_isvalid = true;
      this.uidstatus = true;
  
      this.modeltitle = "Add";
      this.showloader = true;
      this.isEdit = false;
    
    
    }
  
    EditEmployee(row) {
      const data = row.rowData;
      this.resetallforms();
      this.modeltitle = "Edit";
      this.showloader = true;
      this.isEdit = true;
      
      this.showloader = false;
      //this.editModal.show();
      //this.AddModify.show();
  
       this.screen1=false;
       this.screen2=true;

       
  
  
    }
  
    ViewEmployee(row) {
      const data = row.rowData;
      this.viewModal.show();
      console.log(data);
    }
  
    HistoryAllEmployees(): void {
      this.showloader = true;
  
      const req = new InputRequest();
      req.INPUT_02 = "EMPLOYEES";
  
  
      this.service.postData(req, "GetEmployeeHistory").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          console.log(data);
          this.allemphistory = data.Details;
          for (let i = 0; i < this.allemphistory.length; i++) {
            let colname = this.allemphistory[i].columN_NAME;
            let oldvalue = this.allemphistory[i].olD_VALUE;
            let newvalue = this.allemphistory[i].neW_VALUE;
            if (colname == "EMP_PHOTO" && oldvalue) {
              this.decryptHistoryFile(oldvalue, i, 'oldvalue');
            }
            if (colname == "EMP_PHOTO" && newvalue) {
              // this.allemphistory[i].neW_VALUE = this.service.getBase64(newvalue);
              // console.log(this.allemphistory[i].neW_VALUE);
              this.decryptHistoryFile(newvalue, i, 'newvalue');
            }
          }
          this.historyAllSection.show();
        }
        else
          Swal.fire('warning', data.StatusMessage, 'warning');
  
      },
        error => console.log(error));
  
    }
  
    
  
  
    decryptHistoryFile(filepath, i, ftype) {
      const coltype: string = ftype;
      const indx = i;
      if (coltype == 'oldvalue')
        this.allemphistory[indx].olD_VALUE = "";
      else
        this.allemphistory[indx].neW_VALUE = "";
  
      if (filepath) {
        this.service.decryptFile(filepath, "DecryptFile")
          .subscribe(data => {
  
            if (coltype == 'oldvalue')
              this.allemphistory[indx].olD_VALUE = data.docBase64;
            else
              this.allemphistory[indx].neW_VALUE = data.docBase64;
          })
      }
    };
  
    decryptempHistoryFile(filepath, i, ftype) {
      const coltype: string = ftype;
      const indx = i;
      if (coltype == 'oldvalue')
        this.emphistory[indx].olD_VALUE = "";
      else
        this.emphistory[indx].neW_VALUE = "";
  
      if (filepath) {
        this.service.decryptFile(filepath, "DecryptFile")
          .subscribe(data => {
  
            if (coltype == 'oldvalue')
              this.emphistory[indx].olD_VALUE = data.docBase64;
            else
              this.emphistory[indx].neW_VALUE = data.docBase64;
          })
      }
    };
  
    HistoryEmployee(row): void {
      this.showloader = true;
      const data = row.rowData;
      const req = new InputRequest();
      req.INPUT_01 = data.emP_CODE;
  
      this.service.postData(req, "GetEmployeeHistory").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.emphistory = data.Details;
          for (let i = 0; i < this.emphistory.length; i++) {
            let colname = this.emphistory[i].columN_NAME;
            let oldvalue = this.emphistory[i].olD_VALUE;
            let newvalue = this.emphistory[i].neW_VALUE;
            if (colname == "EMP_PHOTO" && oldvalue) {
              this.decryptempHistoryFile(oldvalue, i, 'oldvalue');
            }
            if (colname == "EMP_PHOTO" && newvalue) {
              this.decryptempHistoryFile(newvalue, i, 'newvalue');
            }
          }
          this.historySection.show();
        }
        else
          Swal.fire('warning', data.StatusMessage, 'warning');
  
      },
        error => console.log(error));
    }
  
    
    
  
    resetallforms() {
    
    }
  
  
    
    
    onDateSelection(event) {
  
    }
  
    LoadEmployeeList() {
      this.showloader = true;
      this.service.getData("GetEmpList").subscribe(data => {
        console.log(data);
        if (data.StatusCode == "100") {
          this.employees = data.Details;
          this.EmployeerowData = data.Details;
          if (this.EmployeerowData.length > 0) {
            this.gridApi.setRowData(this.EmployeerowData);
          }
        }
        else
          Swal.fire('warning', data.StatusMessage, 'warning');
        this.showloader = false;
  
      },
        error => console.log(error));
    }
  
    
      
  }
