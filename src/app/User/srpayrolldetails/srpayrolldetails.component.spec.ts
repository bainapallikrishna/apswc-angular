import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SrpayrolldetailsComponent } from './srpayrolldetails.component';

describe('SrpayrolldetailsComponent', () => {
  let component: SrpayrolldetailsComponent;
  let fixture: ComponentFixture<SrpayrolldetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SrpayrolldetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SrpayrolldetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
