import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';

@Component({
  selector: 'app-gallery-upload',
  templateUrl: './gallery-upload.component.html',
  styleUrls: ['./gallery-upload.component.css']
})
export class GalleryUploadComponent implements OnInit {

  public progress: number;
  public message: string;
  
  @Output() public onUploadFinished = new EventEmitter();
  @Output() public onUploadcmFinished = new EventEmitter();
  @Output() public onUploadministerFinished = new EventEmitter();
  constructor(private http: HttpClient) { }

  ngOnInit(): void {

  }
  public uploadFile = (files) => {
    if (files.length === 0) {
      return;
    }
    
    let fileToUpload = <File>files[0];
    const formData = new FormData();
    //for (let file of files)  
    //formData.append(file.name, file);  
    formData.append('file', fileToUpload, fileToUpload.name);
    //formData.append('username', "chandu");
    
    //this.http.post('http://localhost/APSWCAPP/api/FilesUpload/GalleryUploadFileDetails', formData, {reportProgress: true, observe: 'events'})
    this.http.post('https://apswc.ap.gov.in/APSWCAPP/api/FilesUpload/GalleryUploadFileDetails', formData, {reportProgress: true, observe: 'events'})
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.message = 'Upload success.';
          this.onUploadFinished.emit(event.body);
        }
      });
  }


}
