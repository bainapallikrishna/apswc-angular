import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InsepctionUploadComponent } from './insepction-upload.component';

describe('InsepctionUploadComponent', () => {
  let component: InsepctionUploadComponent;
  let fixture: ComponentFixture<InsepctionUploadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InsepctionUploadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InsepctionUploadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
