import { Component, OnInit, EventEmitter, Output, ViewChild } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DomSanitizer } from '@angular/platform-browser';
import { CommonServices } from 'src/app/Services/common.services';

@Component({
  selector: 'app-insepction-upload',
  templateUrl: './insepction-upload.component.html',
  styleUrls: ['./insepction-upload.component.css']
})
export class InsepctionUploadComponent implements OnInit {

  public progress: number;
  public message: string;
  preview: string = "";
  seldocpath: any;
  seldoctype: string = "";
  seldoccat: string = "";
  @ViewChild('previewModal') public previewModal: ModalDirective;
  @Output() public onUploadFinished = new EventEmitter();

  constructor(private http: HttpClient, private sanitizer: DomSanitizer, private service: CommonServices) { }

  ngOnInit(): void {

  }

  fileType = {
    PDF: 'PDF',
    IMAGE: 'IMAGE',
  };

  fileSize = {
    twentyKB: 20480,
    thirtyKB: 30720,
    hundredKB: 102400,
    twoHundredKB: 204800,
    oneMB: 1024000,
  };

  // public uploadFile = (files, event, fileType, size) => {
  public uploadFile = (files) => {
    this.seldocpath = "";
    this.seldoctype = "";
    // console.log("files111",files)
    // console.log("event1321",event)
    // console.log("size123",size)
    // console.log("fileType",fileType)

    if (files.length === 0) {
      return;
    }
    //this.fileType = files[0].type
    // //console.log(fileType, "abc")
    //     return new Promise((resolve, reject) => {
    //       console.log("files.length",files.length)
    //       if (files.length > 0) {
    //         if (
    //           files[0].type === 'image/jpeg' &&
    //         this.fileType === this.fileType.IMAGE
    //         ) {
    //           if (files[0].size < size) {
    //             const file: File = files[0];
    //             const reader = new FileReader();
    //             reader.readAsDataURL(file);
    //             reader.onload = () => resolve(reader.result);
    //             reader.onerror = (error) => reject(error);
    //           } else {
    //             alert('Uploaded image must be less than 100KB');
    //           }
    //         } else if (

    //           files[0].type === 'application/pdf' &&
    //           fileType === this.fileType.PDF
    //         ) {
    //           if (files[0].size < size) {
    //             const file: File = files[0];
    //             const reader = new FileReader();
    //             reader.readAsDataURL(file);
    //             reader.onload = () => resolve(reader.result);
    //             reader.onerror = (error) => reject(error);
    //           } else {
    //             alert('Uploaded file must be less than 1MB');
    //           }
    //         } else {
    //           console.log(" files[0].type ", files[0] )

    //           alert('File Uploading !!!');
    //         }
    //       } else {
    //         alert('file is Empty !!!, Please try again.');
    //       }
    let imagetype = files[0].type;
    this.seldoccat = imagetype;
    if (imagetype == 'image/jpeg' || imagetype == 'image/png')
      this.seldoctype = 'IMAGE';
    else
      this.seldoctype = 'PDF';

    const reader = new FileReader();
    reader.onload = () => {
      if (this.seldoctype == "PDF") {
        const result = reader.result as string;
        const blob = this.service.s_sd((result).split(',')[1], this.seldoccat);
        const blobUrl = URL.createObjectURL(blob);
        this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
      }
      else {
        this.seldocpath = reader.result as string;
        this.preview = this.seldocpath;
      }

    }
    reader.readAsDataURL(<File>files[0])

    let fileToUpload = <File>files[0];
    const formData = new FormData();
    //for (let file of files)  
    //formData.append(file.name, file);  
    formData.append('file', fileToUpload, fileToUpload.name);
    //console.log("formData111111",formData)

    //this.http.post('http://uat.apswc.ap.gov.in/apswcapp/api/FilesUpload/GalleryUploadFileDetails', formData, {reportProgress: true, observe: 'events'})
    this.http.post('https://apswc.ap.gov.in/apswcapp/api/FilesUpload/GalleryUploadFileDetails', formData, { reportProgress: true, observe: 'events' })
      .subscribe(event => {
        if (event.type === HttpEventType.UploadProgress)
          this.progress = Math.round(100 * event.loaded / event.total);
        else if (event.type === HttpEventType.Response) {
          this.message = 'Upload success.';
          this.onUploadFinished.emit(event.body);
        }
      });
  };

  Docpriview(val) {

    // const blob = this.service.s_sd((val).split(',')[1], this.seldoccat);
    // const blobUrl = URL.createObjectURL(blob);
    // this.preview = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
    //this.preview = this.sanitizer.bypassSecurityTrustUrl(val);
    //this.preview = this.preview.changingThisBreaksApplicationSecurity;
    this.previewModal.show();

  }
}
