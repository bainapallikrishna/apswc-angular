import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestlearnComponent } from './testlearn.component';

describe('TestlearnComponent', () => {
  let component: TestlearnComponent;
  let fixture: ComponentFixture<TestlearnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestlearnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestlearnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
