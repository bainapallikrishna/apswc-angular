import { Injectable } from '@angular/core';
import { HttpClient, HttpParams,HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { resourceUsage } from 'process';

@Injectable({
  providedIn: 'root'
})
export class ServicesCommonService {

  private baseurl:"https://localhost:4431/api/APSWC/";

  constructor(private http: HttpClient) { }


     PostSevice(methodname:string,data:any)
  {
    const httpOptions = {  
      headers: new HttpHeaders({  
        'Content-Type': 'application/json; charset=utf-8'  
      })  
    };  
    console.log(data);
   return this.http.post(methodname, data,httpOptions)  
   .pipe(
    map((result) => {
      return result;
    }));
    
  }

  GetService(methodName:string)
  {
   return this.http.get(this.baseurl+methodName)  
    .subscribe((res: Response) => {  
      return res;  
    });  
  }

}
