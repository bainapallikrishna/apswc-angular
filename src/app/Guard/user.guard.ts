import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree,Router } from '@angular/router';
import { Observable } from 'rxjs';
import {UserRole} from 'src/app/Interfaces/user';
import{User} from 'src/app/Interfaces/login';
import {AuthService} from 'src/app/Services/auth.service';
@Injectable({
  providedIn: 'root'
})
export class UserGuard implements CanActivate {
  userDataSubscription: any;    
  userData = new User(); 
  constructor(private router: Router, private authService: AuthService) {    
    this.userDataSubscription = this.authService.userData.asObservable().subscribe(data => {    
      this.userData = data;    
    });    
  }   
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (this.userData.role === UserRole.User) {    
        return true;    
      }    
      
      this.router.navigate(['/Login'], { queryParams: { returnUrl: state.url } });    
      return false; 
  }
  
}
