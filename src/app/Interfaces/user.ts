export interface User {
    Warehouseid: string,
    name: string,
    InsecptionDesc: string,
    imgPath: string

}

export interface EmployeeTypes{
    EmployeeID:string,
    EmployeeTYpe:string,
    Isselected:boolean
}

export interface EmployeeAnnualPropertie{
    Orderid:string,
    Orderdate:string,
    year:string,
    docnumber:string,
    document:string,
    
}


export interface EmpEvents{
    title:string,
    start:string,
    end:string,
    description :string,
    id:string,
    className:string,
    backgroundColor:string,
    textColor:string,
    allDay:boolean
    displayorder:string
    inouttime:string

}

export interface WareHouseMaster
{
    SNo: string ,
    Region: string, 
    RegionCode:string,
    District: string,
    DistrictCode: string,
    warehouseName: string,
    WarehouseCode:string, 
    WarehouseType: string,
    TypeCode: string,
    WarehouseSubType: string
}
export interface Inspection
{
    WarehouseId: string,
    WarehouseName: string,
    Description: string,
    uploadeddate: Date,     
    FilePath: string
}

export interface ServiceCharter
{
    Sectionname:string,
    name:string,
    SectionDescription:string,
    SectionDays:string
}

export interface storagecharges
{
    ItemName:string,
    CommodityName:string,
    PackageName:string,
    Weight:string,
    STDRate:string,
    HighRate1:string,
    HighRate2:string,
    Insurance:string
}
export interface contactus{
    nameofWareHouse:string,
    nameofWareHouseManager:string,
    telePhoneNo:string,
    capacity:string,
    mobileNum:string,
    deptName:string,
    distName:string
}
export interface section
{
    section_CODE:string,
    section:string,
    imagepath:string
}
export interface Gallery
{
    Name:string,
    Description: string,
    uploadeddate: Date,     
    FilePath: string
}
export enum UserRole {    
    Admin = 'Admin',    
    User = 'User'    
}   

export interface Dashboard {
    rtN_ID:number,
     reC_ID:number,
    totaL_SPACE: number,
     occupieD_SPACE: number, 
     availablE_SPACE: number,
     asoN_DATE:Date
    
}