export interface Login {
    UserName: string;
    FirstName: string;
    Password:string;
    UserType:string;
    GToken:string;
    Idval:string;
    
}
export class Captcha
{
    Capchid:string;
    id:string;
    idval:string;
    imgurl:string;
    code:string;
    
}
export class User {    
    userName: string;    
    firstName: string;    
    isLoggedIn: boolean;    
    role: string;    
} 
