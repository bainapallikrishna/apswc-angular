export interface Employee {
    employeeName:string,
    employeeId:string,
    employeeDob:string,
    employeeGender:string, 
    employeeMobile:string,
    employeeMailId:string,
    employeesection:string,
    employeedesignation:string,  
    employeelocationType:string,  
    employeelocation:string,
    employeeAddress:string,
    employeedateofjoining:string
}
export class employee {
    employeename:string;
    employeeid:string;
    dob:string;
    gender:string;
    employeemobile:string;
    employeemailid:string;
    sectionname:string;
    designation:string;
    locationtype:string;
    location:string;
    employeeaddress:string;
    doj:string;

  } 
  export interface  Storagecharges{
    name:string;
    imagePath:string;
    tagName:string;

  }
  export class InputRequest {
    DIRECTION_ID:string;
    TYPEID:string;
    INPUT_01:string;
    INPUT_02:string;
    INPUT_03:string;
    INPUT_04:string;
    INPUT_05:string;
    INPUT_06:string;
    INPUT_07:string;
    INPUT_08:string;
    INPUT_09:string;
    INPUT_10:string;
    INPUT_11:string;
    INPUT_12:string;
    INPUT_13:string;
    INPUT_14:string;
    INPUT_15:string;
    INPUT_16:string;
    INPUT_17:string;
    INPUT_18:string;
    INPUT_19:string;
    INPUT_20:string;
    INPUT_21:string;
    INPUT_22:string;
    INPUT_23:string;
    INPUT_24:string;
    INPUT_25:string;
    INPUT_26:string;
    INPUT_27:string;
    INPUT_28:string;
    INPUT_29:string;
    INPUT_30:string;
    INPUT_31:string;
    INPUT_32:string;
    INPUT_33:string;
    INPUT_34:string;
    INPUT_35:string;
    USER_NAME:string;
    CALL_SOURCE:string;
    CALL_PAGE_ACTIVITY:string;
    CALL_BRO_APP_VER:string;
    CALL_MOBILE_MODEL:string;
    CALL_LATITUDE:string;
    CALL_LONGITUDE:string;
    CALL_IP_IMEI:string;

    code:string;
    RedirectURL:string;
    token:string;
    url:string;
    pagename:string;
    mime:string;
  }

  export class EmployeeSalaryInputRequest {
    DIRECTION_ID:string;
    TYPEID:string;
    INPUT_01:string;
    INPUT_02:string;
    INPUT_03:string;
    INPUT_04:string;
    INPUT_05:string;
    INPUT_06:string;
    INPUT_07:string;
    INPUT_08:string;
    INPUT_09:string;
    INPUT_10:string;
    INPUT_11:string;
    INPUT_12:string;
    INPUT_13:string;
    INPUT_14:string;
    INPUT_15:string;
    INPUT_16:string;
    INPUT_17:string;
    INPUT_18:string;
    INPUT_19:string;
    INPUT_20:string;
    INPUT_21:string;
    INPUT_22:string;
    INPUT_23:string;
    INPUT_24:string;
    INPUT_25:string;
    INPUT_26:string;
    INPUT_27:string;
    INPUT_28:string;
    INPUT_29:string;
    INPUT_30:string;
    INPUT_31:string;
    INPUT_32:string;
    INPUT_33:string;
    INPUT_34:string;
    INPUT_35:string;

    INPUT_36:string;
    INPUT_37:string;
    INPUT_38:string;
    INPUT_39:string;
    INPUT_40:string;
    INPUT_41:string;

    INPUT_42:string;
    INPUT_43:string;

    INPUT_44:string;
    INPUT_45:string;

    INPUT_46:string;
    INPUT_47:string;

    INPUT_48:string;
    INPUT_49:string;
    INPUT_50:string;


    USER_NAME:string;
    CALL_SOURCE:string;
    CALL_PAGE_ACTIVITY:string;
    CALL_BRO_APP_VER:string;
    CALL_MOBILE_MODEL:string;
    CALL_LATITUDE:string;
    CALL_LONGITUDE:string;
    CALL_IP_IMEI:string;

    
  }


  export enum Alphabets {A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z}

