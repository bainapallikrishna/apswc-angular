import { BrowserModule } from '@angular/platform-browser';
import { DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';
import { AppComponent } from './app.component'
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './website/home/home.component'
import { WelcomePageComponent } from './PageManu/welcome-page/welcome-page.component';
import { BoardofdirectorsComponent } from './website/boardofdirectors/boardofdirectors.component';
import { TendersComponent } from './website/tenders/tenders.component';
import { RegulationsComponent } from './website/regulations/regulations.component';
import { GodownlocatorComponent } from './website/godownlocator/godownlocator.component';
import { InspectionpageComponent } from './adminmodule/inspectionpage/inspectionpage.component';
import { FooterComponent } from './website/footer/footer.component';
import { HeaderComponent } from './website/header/header.component';
import { InsepctionUploadComponent } from './upload/insepction-upload/insepction-upload.component';
import { GalleryUploadComponent } from './upload/gallery-upload/gallery-upload.component';
import { GalleryPageComponent } from './adminmodule/gallery-page/gallery-page.component';
import { HomePageComponent } from './adminmodule/home-page/home-page.component';
import { TendersPageComponent } from './adminmodule/tenders-page/tenders-page.component'
import { HttpInterceptorService } from 'src/app/Services/http-interceptor.service';
import { ErrorInterceptorService } from 'src/app/Services/error-interceptor.service';
import { LayoutHeaderComponent } from './layout/layout-header/layout-header.component';
import { LayoutFooterComponent } from './layout/layout-footer/layout-footer.component';
import { LayoutSidemenuComponent } from './layout/layout-sidemenu/layout-sidemenu.component';
import { AdminLayoutComponent } from './layout/admin-layout/admin-layout.component';
import { WebsiteLayoutComponent } from './website/website-layout/website-layout.component';
import { ServiceCharterComponent } from './adminmodule/service-charter/service-charter.component';
import { AStorageChargesComponent } from './adminmodule/Astorage-charges/Astorage-charges.component';
import { PhotogalleryComponent } from './website/photogallery/photogallery.component';
import { InspectiongalleryComponent } from './website/inspectiongallery/inspectiongallery.component';
import { OrganisationalstructureComponent } from './website/organisationalstructure/organisationalstructure.component';
import { ServicecharterComponent } from './website/servicecharter/servicecharter.component';
import { PrivacypolicyComponent } from './website/privacypolicy/privacypolicy.component';
import { HyperlinkpolicyComponent } from './website/hyperlinkpolicy/hyperlinkpolicy.component';
import { CopyrightspolicyComponent } from './website/copyrightspolicy/copyrightspolicy.component';
import { AccessibilityComponent } from './website/accessibility/accessibility.component';
import { DisclaimerComponent } from './website/disclaimer/disclaimer.component';
import { ContactusComponent } from './website/contactus/contactus.component';
import { BlankpageComponent } from './adminmodule/blankpage/blankpage.component';
import { StoragechargesComponent } from './website/storagecharges/storagecharges.component';
import { WarehouseactComponent } from './website/warehouseact/warehouseact.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbDateAdapter, NgbDateNativeAdapter, NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ContactPageComponent } from './adminmodule/contact-page/contact-page.component';
import { EmployeeRegistrationComponent } from './sectionmodule/employee-registration/employee-registration.component';
import { WarehouseRegistrationComponent } from './sectionmodule/warehouse-registration/warehouse-registration.component';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { NgCircleProgressModule } from 'ng-circle-progress';
import { SitemapComponent } from './website/sitemap/sitemap.component';
import { ChartsModule } from 'ng2-charts';
import { ScreenreaderComponent } from './website/screenreader/screenreader.component';
import { GodownMapsComponent } from './website/godown-maps/godown-maps.component';
import { StgothchrgRegistrationComponent } from './adminmodule/stgothchrg-registration/stgothchrg-registration.component'
import { EmployeedetailsComponent } from './adminmodule/employeedetails/employeedetails.component'
import { NumberDirective } from './custome-directives/numbers-only.directive';
import { DigitsDirective } from './custome-directives/digits-only.directive';
import { AlphaNumbersDirective } from './custome-directives/alpha-numbers.directive';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import { LetterDirective } from './custome-directives/letters-only.directive';
import { NgxMaskModule, IConfig } from 'ngx-mask';
import { WarehousemasterComponent } from './adminmodule/warehousemaster/warehousemaster.component';
import { TermsandConditionsComponent } from './website/termsand-conditions/termsand-conditions.component';
import { EmployeemasterComponent } from './adminmodule/employeemaster/employeemaster.component';
import { RegistrationmasterComponent } from './adminmodule/registrationmaster/registrationmaster.component';
import { WarehouseregistrationmasterComponent } from './adminmodule/warehouseregistrationmaster/warehouseregistrationmaster.component';
import { ButtonRendererComponent } from './custome-directives/button-renderer.component';
import { ConditionalRenderer } from './custome-directives/conditional-renderer.component';
import { GBbuttonRendererComponent } from './custome-directives/GBbutton-renderer.component';
import {InvoicebuttonRendererComponent  } from './custome-directives/Invoicebutton-renderer.component';
import { PQCbuttonRendererComponent } from './custome-directives/PQCbutton-renderer.component';
import { AgGridModule } from 'ag-grid-angular';
import { UserProfileComponent } from './User/user-profile/user-profile.component';
import { LeavemasterComponent } from './adminmodule/leavemaster/leavemaster.component';
import { EmployeeLeaveComponent } from './adminmodule/employee-leave/employee-leave.component';
import { HolidaymasterComponent } from './adminmodule/holidaymaster/holidaymaster.component';
import { ChangePasswordComponent } from './User/change-password/change-password.component';
import { TestComponent } from './adminmodule/test/test.component';
import { ScrollingMessageComponent } from './adminmodule/scrolling-message/scrolling-message.component';
import { BoardofdirectorsRegComponent } from './adminmodule/boardofdirectors-reg/boardofdirectors-reg.component';
import { HomeUploadComponent } from './upload/home-upload/home-upload.component';
import { NgxEditorModule } from 'ngx-editor'
import { ApswcWhyEditorComponent } from './adminmodule/apswc-why-editor/apswc-why-editor.component';
import { ApswcOurvisionComponent } from './adminmodule/apswc-ourvision/apswc-ourvision.component';
import { ApswcOurobjectiveComponent } from './adminmodule/apswc-ourobjective/apswc-ourobjective.component';
import { NewsUpdateComponent } from './adminmodule/news-update/news-update.component';
import { HomeHeaderlayoutComponent } from './layout/home-headerlayout/home-headerlayout.component';
import { ApswcHomepageImagesComponent } from './adminmodule/apswc-homepage-images/apswc-homepage-images.component';
import { UserpermissionComponent } from './adminmodule/userpermission/userpermission.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { NgPipesModule } from 'ngx-pipes';
import { QRCodeModule } from 'angularx-qrcode';
import { GeneralbookingComponent } from './adminmodule/warehouse/generalbooking/generalbooking.component';
import { LeaveApproveApplicationComponent } from './User/leave-approve-application/leave-approve-application.component';
import { EmployeescheduleComponent } from './User/employeeschedule/employeeschedule.component';
import { WarehouselayoutComponent } from './adminmodule/warehouselayout/warehouselayout.component';
import { RegistrationComponent } from './website/registration/registration.component';
import { RegistrationuserComponent } from './website/registrationuser/registrationuser.component';
import { OutsourcingAgenciesComponent } from './adminmodule/outsourcing-agencies/outsourcing-agencies.component';
import { WarehouselayoutconfigComponent } from './adminmodule/warehouselayoutconfig/warehouselayoutconfig.component';
import { DeadStockComponent } from './adminmodule/dead-stock/dead-stock.component';
import { EnggmaintenanceComponent } from './adminmodule/dead-stock/enggmaintenance/enggmaintenance.component';
import { DSButtonRendererComponent } from './custome-directives/DSbutton-renderer.component';
import { DeadstockrgnapprovalComponent } from './adminmodule/dead-stock/deadstockrgnapproval/deadstockrgnapproval.component';
import {DSAPPButtonRendererComponent} from './custome-directives/DSAPPbutton-renderer.component';
import { DeadstocksecapprovalComponent } from './adminmodule/dead-stock/deadstocksecapproval/deadstocksecapproval.component'
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import interactionPlugin from '@fullcalendar/interaction';
import { EmployeeleavebalaceComponent } from './User/employeeleavebalace/employeeleavebalace.component'; // a plugin
import { GateInComponent } from './Warehouse/Depositor-Receipt-In/gate-in/gate-in.component';
import { FarmerRegistrationComponent } from './website/farmer-registration/farmer-registration.component';
import { GovtRegistrationComponent } from './website/govt-registration/govt-registration.component';
import { IndRegistrationComponent } from './website/ind-registration/ind-registration.component';
import { WeighmentInComponent } from './Warehouse/Depositor-Receipt-In/weighment-in/weighment-in.component';
import { QualityCheckingComponent } from './Warehouse/Depositor-Receipt-In/quality-checking/quality-checking.component'; // a plugin
import { LayoutStructureComponent } from './adminmodule/layout-structure/layout-structure.component'; // a plugin
// import { TooltipModule } from 'ng2-tooltip-directive';
// import { NgTippyModule } from 'angular-tippy';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { StackingComponent } from './Warehouse/Depositor-Receipt-In/stacking/stacking.component';
import { InsuranceCompanyComponent } from './Warehouse/Insurance/insurance-company/insurance-company.component';
import { GateOutComponent } from './Warehouse/Depositor-Receipt-In/gate-out/gate-out.component';
import { WeighmentOutComponent } from './Warehouse/Depositor-Receipt-In/weighment-out/weighment-out.component';
import { ReceiptOutRequestComponent } from './Warehouse/Depositor-Receipt-Out/receipt-out-request/receipt-out-request.component';
import { StackOutComponent } from './Warehouse/Depositor-Receipt-Out/stack-out/stack-out.component';
import { OutGateInComponent } from './Warehouse/Depositor-Receipt-Out/out-gate-in/out-gate-in.component';
import { ReceiptInRequestComponent } from './Warehouse/Depositor-Receipt-In/receipt-in-request/receipt-in-request.component';
import { OutWeighmentInComponent } from './Warehouse/Depositor-Receipt-Out/out-weighment-in/out-weighment-in.component';
import { OutWeighmentOutComponent } from './Warehouse/Depositor-Receipt-Out/out-weighment-out/out-weighment-out.component';
import { PropertyWaterTaxPaymentComponent } from './Warehouse/Tax-Payment/property-water-tax-payment/property-water-tax-payment.component';
import { QualityCheckingtOutComponent } from './Warehouse/Depositor-Receipt-Out/quality-checkingt-out/quality-checkingt-out.component';
import { OutGateOutComponent } from './Warehouse/Depositor-Receipt-Out/out-gate-out/out-gate-out.component';
import { PeriodicQualityCheckingComponent } from './Warehouse/periodic-quality-checking/periodic-quality-checking.component';
import { DumpingComponent } from './Warehouse/Depositor-Receipt-In/dumping/dumping.component';
import { AdminhelpComponent } from './adminmodule/help/adminhelp/adminhelp.component';
import { AssetmanagementComponent } from './adminmodule/assetmanagement/assetmanagement.component';
import { EmployeePayRoleComponent } from './adminmodule/employee-pay-role/employee-pay-role.component';
import { PastAttendanceComponent } from './User/past-attendance/past-attendance.component';
import { PastAttendanceApproveComponent } from './User/past-attendance-approve/past-attendance-approve.component';
import { WhReceiptComponent } from './Warehouse/wh-receipt/wh-receipt.component';
import { DynamicbuttonRendererComponent } from './custome-directives/dynamic-button-render.components';
import { OgateInComponent } from './Warehouse/Receipt-In-Other/ogate-in/ogate-in.component';
import { OqualityCheckComponent } from './Warehouse/Receipt-In-Other/oquality-check/oquality-check.component';
import { OweighmentInComponent } from './Warehouse/Receipt-In-Other/oweighment-in/oweighment-in.component';
import { OstackInComponent } from './Warehouse/Receipt-In-Other/ostack-in/ostack-in.component';
import { OweighmentOutComponent } from './Warehouse/Receipt-In-Other/oweighment-out/oweighment-out.component';
import { OgateOutComponent } from './Warehouse/Receipt-In-Other/ogate-out/ogate-out.component';
import { MobileUserPermissionComponent } from './adminmodule/mobile-user-access/mobile-user-access.component';
import { EmployeePayroleHistoryComponent } from './adminmodule/employee-payrole-history/employee-payrole-history.component';
import { ServiceRegisterComponent } from './User/service-register/service-register.component';
import { DummycalendarComponent } from './dummycalendar/dummycalendar.component';
import { TestlearnComponent } from './testlearn/testlearn.component';
import { EmployeeloanRequestComponent } from './adminmodule/employeeloan-request/employeeloan-request.component';
import { EmployeeloanApprovalComponent } from './adminmodule/employeeloan-approval/employeeloan-approval.component';
import { MDlevelApprovalComponent } from './adminmodule/mdlevel-approval/mdlevel-approval.component';
import { SrleaveledgerComponent } from './User/srleaveledger/srleaveledger.component';
import { SrpayrolldetailsComponent } from './User/srpayrolldetails/srpayrolldetails.component';
import { ServiceDetailsComponent } from './User/service-details/service-details.component';
import { MailSettingsComponent } from './adminmodule/Mail/mail-settings/mail-settings.component';
import { StockRegisterComponent } from './Warehouse_Reports/stock-register/stock-register.component';
import { LorryWeighbridgeReportComponent } from './Warehouse_Reports/lorry-weighbridge-report/lorry-weighbridge-report.component';
import { DailyValuationStockComponent } from './Warehouse_Reports/daily-valuation-stock/daily-valuation-stock.component';
import { OpeningBalanceComponent } from './Warehouse_Reports/opening-balance/opening-balance.component';
import { ImprestRegisterComponent } from './Warehouse_Reports/imprest-register/imprest-register.component';
import { BankloanregisterComponent } from './Warehouse_Reports/bankloanregister/bankloanregister.component';
import { DailyTransactionRegComponent } from './Warehouse_Reports/daily-transaction-reg/daily-transaction-reg.component';
import { DepositorLedgerComponent } from './Warehouse_Reports/depositor-ledger/depositor-ledger.component';
import { MatTableModule } from '@angular/material/table';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { WarehouseSpillageComponent } from './Warehouse_Reports/warehouse-spillage/warehouse-spillage.component';
import { WHChemicalConsumptionComponent } from './Warehouse_Reports/wh-chemical-consumption/wh-chemical-consumption.component';
import { WarehouseInvoiceMatserComponent } from './adminmodule/warehouse-invoice-matser/warehouse-invoice-matser.component';
import { WarehousequalityMatserComponent } from './adminmodule/warehousequality-matser/warehousequality-matser.component';
import { WarehouseReservationMatserComponent } from './adminmodule/warehouse-reservation-matser/warehouse-reservation-matser.component';
import { MedicalreimbursementMatserComponent } from './adminmodule/medicalreimbursement-matser/medicalreimbursement-matser.component';
import { MedicalReimbursementRequestComponent } from './adminmodule/medical-reimbursement-request/medical-reimbursement-request.component';
import { MedicalReimbursementApprovalComponent } from './adminmodule/medical-reimbursement-approval/medical-reimbursement-approval.component';
import { StackRegisterComponent } from './Warehouse_Reports/stack-register/stack-register.component';
import { DepositerInvoiceComponent } from './Warehouse_Invoices/depositer-invoice/depositer-invoice.component';
import { WarehousemanagerComponent } from './adminmodule/warehouse/warehousemanager/warehousemanager.component';
import { NgApexchartsModule } from "ng-apexcharts";
import { SpaceReservationComponent } from './adminmodule/warehouse/space-reservation/space-reservation.component';
import {MedicalAPPREJBUTTONRendererComponent} from './custome-directives/MedicalApproveRejectbutton-renderer.component';
import { HandtdetailsComponent } from './adminmodule/handtdetails/handtdetails.component';
import { WhhtMappingComponent } from './Warehouse/whht-mapping/whht-mapping.component';
import { EMPButtonRendererComponent } from './custome-directives/Empbutton-renderer.component';
import { WHButtonRendererComponent } from './custome-directives/whbutton-renderer.component';
import { gridbuttonrenderer } from './custome-directives/gridbutton-renderer.component';
import { leaveApprovegridbuttoncomponent } from './custome-directives/LeaveApprovegridbutton-renderer.component';
import { EMPSALBUTTONRendererComponent } from './custome-directives/Empsalbutton-renderer.component';
import { EMPVIEWSALBUTTONRendererComponent } from './custome-directives/Empviewsalbutton-renderer.component';
import { srempdetailsRendererComponent } from './custome-directives/srempdetails-renderer.component';
import { OFFICERAPPREJBUTTONRendererComponent } from './custome-directives/OfficerApproveRejectbutton-renderer.component';
import { sreventsRendererComponent } from './custome-directives/srevents-renderer.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { CreditDebitInvoiceComponent } from './Warehouse_Invoices/credit-debit-invoice/credit-debit-invoice.component';
import { HistorybuttonRendererComponent } from './custome-directives/Historybutton-renderer.component';
import { QuantityExaminationComponent } from 'src/app/Warehouse/Depositor-Receipt-In/quantity-examination/quantity-examination.component';
import { WhackComponent } from './Warehouse/Depositor-Receipt-In/whack/whack.component';
import { HrdashboardComponent } from './adminmodule/hrdashboard/hrdashboard.component';
import { FinanceDashboardComponent } from './adminmodule/finance-dashboard/finance-dashboard.component'
import { CashBookComponent } from './Warehouse_Reports/cash-book/cash-book.component';
import { ExistingStockEntryComponent } from './WarehouseManagement/existing-stock-entry/existing-stock-entry.component';
import { StorageLossEntryComponent } from './WarehouseManagement/storage-loss-entry/storage-loss-entry.component';

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);


const maskConfigFunction: () => Partial<IConfig> = () => {
  return {
    validation: false,
  };
};



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    HomeComponent,
    WelcomePageComponent,
    BoardofdirectorsComponent,
    BoardofdirectorsRegComponent,
    TendersComponent,
    RegulationsComponent,
    GodownlocatorComponent,
    InspectionpageComponent,
    InsepctionUploadComponent,
    GalleryUploadComponent,
    GalleryPageComponent,
    HomePageComponent,
    TendersPageComponent,
    LayoutHeaderComponent,
    LayoutFooterComponent,
    LayoutSidemenuComponent,
    AdminLayoutComponent,
    WebsiteLayoutComponent,
    ServiceCharterComponent,
    AStorageChargesComponent,
    StoragechargesComponent,
    ContactusComponent,
    PhotogalleryComponent,
    InspectiongalleryComponent,
    OrganisationalstructureComponent,
    ServicecharterComponent,
    PrivacypolicyComponent,
    HyperlinkpolicyComponent,
    CopyrightspolicyComponent,
    AccessibilityComponent,
    DisclaimerComponent,
    BlankpageComponent,
    WarehouseactComponent,
    ContactPageComponent,
    EmployeeRegistrationComponent,
    WarehouseRegistrationComponent,
    SitemapComponent,
    ScreenreaderComponent,
    GodownMapsComponent,
    StgothchrgRegistrationComponent,
    EmployeedetailsComponent,
    NumberDirective,
    DigitsDirective,
    AlphaNumbersDirective,
    TestComponent,
    LetterDirective,
    WarehousemasterComponent,
    TermsandConditionsComponent,
    //ModalModule.forRoot()
    EmployeemasterComponent,
    RegistrationmasterComponent,
    WarehouseregistrationmasterComponent,
    // StgothchrgRegistrationComponent
    TermsandConditionsComponent,
    ButtonRendererComponent,
    ConditionalRenderer,
    GBbuttonRendererComponent,
    MedicalAPPREJBUTTONRendererComponent,
    InvoicebuttonRendererComponent,
    PQCbuttonRendererComponent,
    UserProfileComponent,
    LeavemasterComponent,
    EmployeeLeaveComponent,
    HolidaymasterComponent,
    ChangePasswordComponent,
    ScrollingMessageComponent,
    HomeUploadComponent,
    ApswcWhyEditorComponent,
    ApswcOurvisionComponent,
    ApswcOurobjectiveComponent,
    NewsUpdateComponent,
    HomeHeaderlayoutComponent,
    UserpermissionComponent,
    ApswcHomepageImagesComponent,
    EmployeescheduleComponent,
    LeaveApproveApplicationComponent,  
    WarehouselayoutComponent,
    RegistrationComponent,
    RegistrationuserComponent,
    OutsourcingAgenciesComponent,
    WarehouselayoutconfigComponent,
    DeadStockComponent, 
    GeneralbookingComponent,
    EnggmaintenanceComponent,
    DSButtonRendererComponent,
    DeadstockrgnapprovalComponent,
    DSAPPButtonRendererComponent,
    DeadstocksecapprovalComponent,   
    EmployeeleavebalaceComponent,
    GateInComponent,
    ConditionalRenderer,
    FarmerRegistrationComponent,
    GovtRegistrationComponent,
    IndRegistrationComponent,
    WeighmentInComponent,
    StackingComponent,
    InsuranceCompanyComponent,
    QualityCheckingComponent,
    LayoutStructureComponent,    
    StackingComponent,
    WeighmentOutComponent,
    GateOutComponent,
    ReceiptOutRequestComponent,
    StackOutComponent,
    OutGateInComponent,
    ReceiptInRequestComponent,
    OutWeighmentInComponent,
    OutWeighmentOutComponent,
    PropertyWaterTaxPaymentComponent,    
    QualityCheckingtOutComponent,
    OutGateOutComponent,
    PeriodicQualityCheckingComponent,
    DumpingComponent,
    AdminhelpComponent,
    AssetmanagementComponent,
    EmployeePayRoleComponent,
    DynamicbuttonRendererComponent,  
    PastAttendanceComponent,
    PastAttendanceApproveComponent,
    WhReceiptComponent,
    OgateInComponent,
    OqualityCheckComponent,
    OweighmentInComponent,
    OstackInComponent,
    OweighmentOutComponent,
    OgateOutComponent,
    MobileUserPermissionComponent,
    EmployeePayroleHistoryComponent,
    ServiceRegisterComponent,
    DummycalendarComponent,
    TestlearnComponent,
    EmployeeloanRequestComponent,
    EmployeeloanApprovalComponent,
    MDlevelApprovalComponent,
    SrleaveledgerComponent,
    SrpayrolldetailsComponent,
    ServiceDetailsComponent,
    MailSettingsComponent,
    StockRegisterComponent,
    LorryWeighbridgeReportComponent,
    DailyValuationStockComponent,
    OpeningBalanceComponent,
    ImprestRegisterComponent,
    BankloanregisterComponent,
    DailyTransactionRegComponent,
    DepositorLedgerComponent,
    WarehouseSpillageComponent,
    WHChemicalConsumptionComponent,
    WarehouseInvoiceMatserComponent,
    WarehousequalityMatserComponent,
    WarehouseReservationMatserComponent,
    MedicalreimbursementMatserComponent,
    MedicalReimbursementRequestComponent,
    MedicalReimbursementApprovalComponent,
    StackRegisterComponent,
    DepositerInvoiceComponent,
    WarehousemanagerComponent,
    SpaceReservationComponent,
    HandtdetailsComponent,
    WhhtMappingComponent,
    EMPButtonRendererComponent,
    WHButtonRendererComponent,
    gridbuttonrenderer,
    leaveApprovegridbuttoncomponent,
    EMPSALBUTTONRendererComponent,
    EMPVIEWSALBUTTONRendererComponent,
    srempdetailsRendererComponent,
    OFFICERAPPREJBUTTONRendererComponent,
    sreventsRendererComponent,
    PagenotfoundComponent,
    CreditDebitInvoiceComponent,
    HistorybuttonRendererComponent,
    QuantityExaminationComponent,
    WhackComponent,
    HrdashboardComponent,
    FinanceDashboardComponent,
    CashBookComponent,
    ExistingStockEntryComponent,
    StorageLossEntryComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RecaptchaModule,  //this is the recaptcha main module
    RecaptchaFormsModule,
    BrowserAnimationsModule,
    CarouselModule, // this is the owl Carosel
    NgbModule,
    ChartsModule,
    BrowserAnimationsModule,
    CarouselModule,
    NgApexchartsModule,
    DragDropModule,
    //NgxMaterialTimepickerModule ,
    NgxEditorModule.forRoot({
      locals: {
        bold: 'Bold',
        italic: 'Italic',
        code: 'Code',
        underline: 'Underline',
        // ...
      }
    }),
    ModalModule.forRoot(),
    // this is the owl Carosel
    NgxMaskModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
    NgCircleProgressModule.forRoot({
      // set defaults here
      radius: 60,
      outerStrokeWidth: 5,
      innerStrokeWidth: 1,
      outerStrokeColor: "#2ab8e2",
      innerStrokeColor: "#2ab8e2",
      animationDuration: 300,
      backgroundColor: "#2ab8e2",
      backgroundPadding: -10,
      unitsColor: "#ffffff",
      titleColor: "#ffffff",
      showSubtitle: false

    }),

    ModalModule.forRoot(),
    AgGridModule.withComponents([ButtonRendererComponent,
       ConditionalRenderer,
        GBbuttonRendererComponent,
        MedicalAPPREJBUTTONRendererComponent,
        InvoicebuttonRendererComponent,
        DSButtonRendererComponent,
        DSAPPButtonRendererComponent,PQCbuttonRendererComponent,DynamicbuttonRendererComponent,HistorybuttonRendererComponent,
        EMPButtonRendererComponent,WHButtonRendererComponent,gridbuttonrenderer,leaveApprovegridbuttoncomponent,
        EMPSALBUTTONRendererComponent,
        EMPVIEWSALBUTTONRendererComponent,
        srempdetailsRendererComponent,
        OFFICERAPPREJBUTTONRendererComponent,
        sreventsRendererComponent]),
    NgSelectModule,
    AutocompleteLibModule,
    QRCodeModule,

    FormsModule,
    FullCalendarModule,
    NgPipesModule,
    PopoverModule.forRoot(),
    MatTableModule,
    NgApexchartsModule
  ],
  // providers: [],
  providers: [
    { provide: DatePipe },
    { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptorService, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptorService, multi: true },
    EncrDecrServiceService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }