import { Component, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking

@Component({
  selector: 'app-dummycalendar',
  templateUrl: './dummycalendar.component.html',
  styleUrls: ['./dummycalendar.component.css']
})
export class DummycalendarComponent implements OnInit {

  constructor() { }
  calendarOptions: CalendarOptions;
  
  ngOnInit(): void {


    this.calendarOptions = {
      initialView: 'dayGridMonth',
      dateClick: this.handleDateClick.bind(this), // bind is important!
      events: [
        { title: 'event 1', date: '2021-05-25' },
        { title: 'event 2', date: '2021-05-24' }
      ],
      headerToolbar: {
        left: 'prev,next today,prevYear,nextYear',
        center: 'title',
        right: 'dayGridMonth,dayGridWeek,dayGridDay'
      }
    };

  }

  handleDateClick(arg) {
    console.log('date click! ' + arg.dateStr)
  }

}
