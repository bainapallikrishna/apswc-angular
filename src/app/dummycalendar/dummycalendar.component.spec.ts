import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DummycalendarComponent } from './dummycalendar.component';

describe('DummycalendarComponent', () => {
  let component: DummycalendarComponent;
  let fixture: ComponentFixture<DummycalendarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DummycalendarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DummycalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
