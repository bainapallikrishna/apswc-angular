import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { InspectionpageComponent } from './adminmodule/inspectionpage/inspectionpage.component';
import { GalleryPageComponent } from './adminmodule/gallery-page/gallery-page.component';
import { HomePageComponent } from './adminmodule/home-page/home-page.component';
import { TendersPageComponent } from './adminmodule/tenders-page/tenders-page.component';
import { AdminGuard } from 'src/app/Guard/admin.guard';
import { AdminLayoutComponent } from 'src/app/layout/admin-layout/admin-layout.component';
import { WebsiteLayoutComponent } from './website/website-layout/website-layout.component';
import { ServiceCharterComponent } from './adminmodule/service-charter/service-charter.component';
import { HomeComponent } from './website/home/home.component';
import { HeaderComponent } from './website/header/header.component';
import { BoardofdirectorsComponent } from './website/boardofdirectors/boardofdirectors.component';
import { AccessibilityComponent } from './website/accessibility/accessibility.component';
import { ContactusComponent } from './website/contactus/contactus.component';
import { OrganisationalstructureComponent } from './website/organisationalstructure/organisationalstructure.component';
import { PrivacypolicyComponent } from './website/privacypolicy/privacypolicy.component';
import { StoragechargesComponent } from './website/storagecharges/storagecharges.component';
import { RegulationsComponent } from './website/regulations/regulations.component';
import { ServicecharterComponent } from './website/servicecharter/servicecharter.component';
import { PhotogalleryComponent } from './website/photogallery/photogallery.component';
import { InspectiongalleryComponent } from './website/inspectiongallery/inspectiongallery.component';
import { HyperlinkpolicyComponent } from './website/hyperlinkpolicy/hyperlinkpolicy.component';
import { DisclaimerComponent } from './website/disclaimer/disclaimer.component';
import { CopyrightspolicyComponent } from './website/copyrightspolicy/copyrightspolicy.component';
import { BlankpageComponent } from './adminmodule/blankpage/blankpage.component';
import { AStorageChargesComponent } from './adminmodule/Astorage-charges/Astorage-charges.component';
import { TendersComponent } from './website/tenders/tenders.component';
import { WarehouseactComponent } from './website/warehouseact/warehouseact.component';
import { GodownlocatorComponent } from './website/godownlocator/godownlocator.component';
import { ContactPageComponent } from './adminmodule/contact-page/contact-page.component';
import { EmployeeRegistrationComponent } from './sectionmodule/employee-registration/employee-registration.component';
import { WarehouseRegistrationComponent } from './sectionmodule/warehouse-registration/warehouse-registration.component';
import { SitemapComponent } from './website/sitemap/sitemap.component';
import { ScreenreaderComponent } from './website/screenreader/screenreader.component';
import { GodownMapsComponent } from './website/godown-maps/godown-maps.component';
import { TestlearnComponent } from 'src/app/testlearn/testlearn.component';

import { StgothchrgRegistrationComponent } from './adminmodule/stgothchrg-registration/stgothchrg-registration.component';

import { EmployeedetailsComponent } from './adminmodule/employeedetails/employeedetails.component';

import { TestComponent } from './adminmodule/test/test.component';
import { BoardofdirectorsRegComponent } from './adminmodule/boardofdirectors-reg/boardofdirectors-reg.component';
import { WarehousemasterComponent } from './adminmodule/warehousemaster/warehousemaster.component';
import { TermsandConditionsComponent } from './website/termsand-conditions/termsand-conditions.component';
import { EmployeemasterComponent } from './adminmodule/employeemaster/employeemaster.component';
import { RegistrationmasterComponent } from './adminmodule/registrationmaster/registrationmaster.component';
import { WarehouseregistrationmasterComponent } from './adminmodule/warehouseregistrationmaster/warehouseregistrationmaster.component';
import { UserProfileComponent } from './User/user-profile/user-profile.component';
import { LeavemasterComponent } from './adminmodule/leavemaster/leavemaster.component';
import { EmployeeLeaveComponent } from './adminmodule/employee-leave/employee-leave.component';
import { HolidaymasterComponent } from './adminmodule/holidaymaster/holidaymaster.component';
import { ChangePasswordComponent } from './User/change-password/change-password.component';
import { ScrollingMessageComponent } from './adminmodule/scrolling-message/scrolling-message.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

import { ApswcWhyEditorComponent } from './adminmodule/apswc-why-editor/apswc-why-editor.component';
import { ApswcOurobjectiveComponent } from './adminmodule/apswc-ourobjective/apswc-ourobjective.component';
import { ApswcOurvisionComponent } from './adminmodule/apswc-ourvision/apswc-ourvision.component';
import { ApswcHomepageImagesComponent } from './adminmodule/apswc-homepage-images/apswc-homepage-images.component';
import { UserpermissionComponent } from './adminmodule/userpermission/userpermission.component';
import { NewsUpdateComponent } from './adminmodule/news-update/news-update.component';
import { GeneralbookingComponent } from './adminmodule/warehouse/generalbooking/generalbooking.component';
import { DeadStockComponent } from './adminmodule/dead-stock/dead-stock.component';

import { EnggmaintenanceComponent } from './adminmodule/dead-stock/enggmaintenance/enggmaintenance.component';
import { DeadstockrgnapprovalComponent } from './adminmodule/dead-stock/deadstockrgnapproval/deadstockrgnapproval.component';
import { DeadstocksecapprovalComponent } from './adminmodule/dead-stock/deadstocksecapproval/deadstocksecapproval.component';

import { LeaveApproveApplicationComponent } from './User/leave-approve-application/leave-approve-application.component';
import { EmployeescheduleComponent } from './User/employeeschedule/employeeschedule.component';
import { WarehouselayoutComponent } from './adminmodule/warehouselayout/warehouselayout.component';
import { RegistrationComponent } from './website/registration/registration.component';
import { RegistrationuserComponent } from './website/registrationuser/registrationuser.component';
import { WarehouselayoutconfigComponent } from './adminmodule/warehouselayoutconfig/warehouselayoutconfig.component';
import { OutsourcingAgenciesComponent } from './adminmodule/outsourcing-agencies/outsourcing-agencies.component';
import { EmployeeleavebalaceComponent } from './User/employeeleavebalace/employeeleavebalace.component';
import { GateInComponent } from './Warehouse/Depositor-Receipt-In/gate-in/gate-in.component';
import { GovtRegistrationComponent } from './website/govt-registration/govt-registration.component';
import { FarmerRegistrationComponent } from './website/farmer-registration/farmer-registration.component';
import { IndRegistrationComponent } from './website/ind-registration/ind-registration.component';
import { LayoutStructureComponent } from './adminmodule/layout-structure/layout-structure.component';
import { WeighmentInComponent } from './Warehouse/Depositor-Receipt-In/weighment-in/weighment-in.component';
import { QualityCheckingComponent } from './Warehouse/Depositor-Receipt-In/quality-checking/quality-checking.component';
import { StackingComponent } from './Warehouse/Depositor-Receipt-In/stacking/stacking.component';
import { InsuranceCompanyComponent } from './Warehouse/Insurance/insurance-company/insurance-company.component';
import { GateOutComponent } from './Warehouse/Depositor-Receipt-In/gate-out/gate-out.component';

import { WeighmentOutComponent } from './Warehouse/Depositor-Receipt-In/weighment-out/weighment-out.component';
import { ReceiptOutRequestComponent } from './Warehouse/Depositor-Receipt-Out/receipt-out-request/receipt-out-request.component';
import { StackOutComponent } from './Warehouse/Depositor-Receipt-Out/stack-out/stack-out.component';
import { OutGateInComponent } from './Warehouse/Depositor-Receipt-Out/out-gate-in/out-gate-in.component';
import { OutWeighmentInComponent } from './Warehouse/Depositor-Receipt-Out/out-weighment-in/out-weighment-in.component';
import { OutWeighmentOutComponent } from './Warehouse/Depositor-Receipt-Out/out-weighment-out/out-weighment-out.component';
import { PropertyWaterTaxPaymentComponent } from './Warehouse/Tax-Payment/property-water-tax-payment/property-water-tax-payment.component';
import { from } from 'rxjs';
import { ReceiptInRequestComponent } from './Warehouse/Depositor-Receipt-In/receipt-in-request/receipt-in-request.component';
import { QualityCheckingtOutComponent } from './Warehouse/Depositor-Receipt-Out/quality-checkingt-out/quality-checkingt-out.component'
import { OutGateOutComponent } from './Warehouse/Depositor-Receipt-Out/out-gate-out/out-gate-out.component';
import { PeriodicQualityCheckingComponent } from './Warehouse/periodic-quality-checking/periodic-quality-checking.component';
import { DumpingComponent } from './Warehouse/Depositor-Receipt-In/dumping/dumping.component';
import { AdminhelpComponent } from './adminmodule/help/adminhelp/adminhelp.component';
import { ServiceRegisterComponent } from './User/service-register/service-register.component';


import { AssetmanagementComponent } from './adminmodule/assetmanagement/assetmanagement.component';
import { EmployeePayRoleComponent } from './adminmodule/employee-pay-role/employee-pay-role.component';
import { PastAttendanceComponent } from './User/past-attendance/past-attendance.component';
import { PastAttendanceApproveComponent } from './User/past-attendance-approve/past-attendance-approve.component';
import { WhReceiptComponent } from './Warehouse/wh-receipt/wh-receipt.component';
import { MobileUserPermissionComponent } from './adminmodule/mobile-user-access/mobile-user-access.component';

import { OgateInComponent } from './Warehouse/Receipt-In-Other/ogate-in/ogate-in.component';
import { OqualityCheckComponent } from './Warehouse/Receipt-In-Other/oquality-check/oquality-check.component';
import { OstackInComponent } from './Warehouse/Receipt-In-Other/ostack-in/ostack-in.component';
import { OweighmentInComponent } from './Warehouse/Receipt-In-Other/oweighment-in/oweighment-in.component';
import { OweighmentOutComponent } from './Warehouse/Receipt-In-Other/oweighment-out/oweighment-out.component';
import { OgateOutComponent } from './Warehouse/Receipt-In-Other/ogate-out/ogate-out.component';
import { EmployeePayroleHistoryComponent } from './adminmodule/employee-payrole-history/employee-payrole-history.component';
import { DummycalendarComponent } from './dummycalendar/dummycalendar.component';
import { SrleaveledgerComponent } from './User/srleaveledger/srleaveledger.component';
import { SrpayrolldetailsComponent } from './User/srpayrolldetails/srpayrolldetails.component';
import { ServiceDetailsComponent } from './User/service-details/service-details.component';
import { MailSettingsComponent } from './adminmodule/Mail/mail-settings/mail-settings.component';
import { StockRegisterComponent } from './Warehouse_Reports/stock-register/stock-register.component';
import { LorryWeighbridgeReportComponent } from './Warehouse_Reports/lorry-weighbridge-report/lorry-weighbridge-report.component';
import { EmployeeloanRequestComponent } from './adminmodule/employeeloan-request/employeeloan-request.component';
import { EmployeeloanApprovalComponent } from './adminmodule/employeeloan-approval/employeeloan-approval.component';
import { MDlevelApprovalComponent } from './adminmodule/mdlevel-approval/mdlevel-approval.component';
import { DailyValuationStockComponent } from './Warehouse_Reports/daily-valuation-stock/daily-valuation-stock.component';
import { OpeningBalanceComponent } from './Warehouse_Reports/opening-balance/opening-balance.component';
import { ImprestRegisterComponent } from './Warehouse_Reports/imprest-register/imprest-register.component';
import { DailyTransactionRegComponent } from './Warehouse_Reports/daily-transaction-reg/daily-transaction-reg.component';
import { BankloanregisterComponent } from './Warehouse_Reports/bankloanregister/bankloanregister.component';
import { DepositorLedgerComponent } from './Warehouse_Reports/depositor-ledger/depositor-ledger.component';
import { WarehouseSpillageComponent } from './Warehouse_Reports/warehouse-spillage/warehouse-spillage.component';
import { WHChemicalConsumptionComponent } from './Warehouse_Reports/wh-chemical-consumption/wh-chemical-consumption.component';
import { WarehouseInvoiceMatserComponent } from './adminmodule/warehouse-invoice-matser/warehouse-invoice-matser.component';
import { WarehousequalityMatserComponent } from './adminmodule/warehousequality-matser/warehousequality-matser.component';
import { WarehouseReservationMatserComponent } from './adminmodule/warehouse-reservation-matser/warehouse-reservation-matser.component';
import { MedicalreimbursementMatserComponent } from './adminmodule/medicalreimbursement-matser/medicalreimbursement-matser.component';
import { MedicalReimbursementRequestComponent } from './adminmodule/medical-reimbursement-request/medical-reimbursement-request.component';
import { MedicalReimbursementApprovalComponent } from './adminmodule/medical-reimbursement-approval/medical-reimbursement-approval.component';
import { StackRegisterComponent } from './Warehouse_Reports/stack-register/stack-register.component';
import { WarehousemanagerComponent } from './adminmodule/warehouse/warehousemanager/warehousemanager.component';
import { SpaceReservationComponent } from './adminmodule/warehouse/space-reservation/space-reservation.component';
import { DepositerInvoiceComponent } from './Warehouse_Invoices/depositer-invoice/depositer-invoice.component';
import {CreditDebitInvoiceComponent  } from './Warehouse_Invoices/credit-debit-invoice/credit-debit-invoice.component';
import { HandtdetailsComponent } from './adminmodule/handtdetails/handtdetails.component';
import { WhhtMappingComponent } from './Warehouse/whht-mapping/whht-mapping.component';
import { QuantityExaminationComponent } from './Warehouse/Depositor-Receipt-In/quantity-examination/quantity-examination.component';
import { WhackComponent } from './Warehouse/Depositor-Receipt-In/whack/whack.component';
import { HrdashboardComponent } from './adminmodule/hrdashboard/hrdashboard.component';
import { FinanceDashboardComponent } from './adminmodule/finance-dashboard/finance-dashboard.component';
import { CashBookComponent } from './Warehouse_Reports/cash-book/cash-book.component';
import { ExistingStockEntryComponent } from './WarehouseManagement/existing-stock-entry/existing-stock-entry.component';
import { StorageLossEntryComponent } from './WarehouseManagement/storage-loss-entry/storage-loss-entry.component';

const routes: Routes = [
  {
    path: '',
    component: WebsiteLayoutComponent,

    children: [
      { path: '', component: HomeComponent },
      { path: 'Home', component: HomeComponent },
      { path: 'Header', component: HeaderComponent },
      { path: 'Boardofdirector', component: BoardofdirectorsComponent },
      {
        path: 'OrganisationalStructure',
        component: OrganisationalstructureComponent,
      },
      
      { path: 'Tender', component: TendersComponent },
      { path: 'Contactus', component: ContactusComponent },
      { path: 'StorageCharges', component: StoragechargesComponent },
      { path: 'ServicesCharter', component: ServicecharterComponent },
      { path: 'Regulation', component: RegulationsComponent },
      { path: 'PhotoGallery', component: PhotogalleryComponent },
      { path: 'InpectionGallery', component: InspectiongalleryComponent },
      { path: 'Hyperlinkpolicy', component: HyperlinkpolicyComponent },
      { path: 'Disclaimer', component: DisclaimerComponent },
      { path: 'CopyrightsPolicy', component: CopyrightspolicyComponent },
      { path: 'Accessibility', component: AccessibilityComponent },
      { path: 'PrivacyPolicy', component: PrivacypolicyComponent },
      { path: 'WarehouseAct', component: WarehouseactComponent },
      { path: 'GodownLocator', component: GodownlocatorComponent },
      { path: 'Sitemap', component: SitemapComponent },
      { path: 'ScreenReaderAccess', component: ScreenreaderComponent },
      { path: 'maps', component: GodownMapsComponent },
      
      { path: 'Test', component: TestComponent },
      { path: 'TermsConditions', component: TermsandConditionsComponent },
      { path: 'Registration', component: RegistrationComponent },
      { path: 'RegistrationUser', component: RegistrationuserComponent },
      { path: 'RegistrationGovt', component: GovtRegistrationComponent },
      { path: 'RegistrationFarmer', component: FarmerRegistrationComponent },
      { path: 'RegistrationInd', component: IndRegistrationComponent },
    ],
  },

  {
    path: '',
    component: AdminLayoutComponent,
    children: [
      // { path: 'AdminWelcome', component: SamplepageComponent,canActivate:[AdminGuard] },
      {
        path: 'Boardofdirector',
        component: BoardofdirectorsComponent,
        canActivate: [AdminGuard],
      },
      { path: 'sample', component: TestlearnComponent },
      { path: 'InspectionPage', component: InspectionpageComponent },
      { path: 'Tenderpage', component: TendersPageComponent },
      { path: 'ServiceCharterPage', component: ServiceCharterComponent },
      { path: 'AStorageacharge', component: AStorageChargesComponent },
      { path: 'AdminWelcome', component: InspectionpageComponent },
      { path: 'Gallerypage', component: GalleryPageComponent },
      { path: 'BlankPage', component: BlankpageComponent },
      { path: 'ContactPage', component: ContactPageComponent },
      {
        path: 'EmployeeRegistration',
        component: EmployeeRegistrationComponent,
      },
      {
        path: 'WarehouseRegistration',
        component: WarehouseRegistrationComponent,
      },
      { path: 'EmployeeDetails', component: EmployeedetailsComponent },
      {
        path: 'DirectorsRegistration',
        component: BoardofdirectorsRegComponent,
      },
      { path: 'Warehousemaster', component: WarehousemasterComponent },
      { path: 'InspectionPage', component: InspectionpageComponent },
      { path: 'Homepage', component: HomePageComponent },
      { path: 'Tenderpage', component: TendersPageComponent },
      { path: 'ServiceCharterPage', component: ServiceCharterComponent },
      { path: 'AStorageacharge', component: AStorageChargesComponent },
      { path: 'AdminWelcome', component: InspectionpageComponent },
      { path: 'Gallerypage', component: GalleryPageComponent },
      { path: 'BlankPage', component: BlankpageComponent },
      { path: 'ContactPage', component: ContactPageComponent },
      {
        path: 'EmployeeRegistration',
        component: EmployeeRegistrationComponent,
      },
      {
        path: 'WarehouseRegistration', component: WarehouseRegistrationComponent,
      },
      { path: 'EmployeeDetails', component: EmployeedetailsComponent },
      { path: 'DirectorsRegistration', component: BoardofdirectorsRegComponent },
      { path: 'Warehousemaster', component: WarehousemasterComponent },
      { path: 'UserProfile', component: UserProfileComponent },
      { path: 'LeaveMaster', component: LeavemasterComponent },
      { path: 'EmployeeLeave', component: EmployeeLeaveComponent },
      { path: 'HolidayMaster', component: HolidaymasterComponent },
      { path: 'LeaveApprove', component: LeaveApproveApplicationComponent },
      { path: 'EmployeeSchedule', component: EmployeescheduleComponent },
      { path: 'LeaveBalance', component: EmployeeleavebalaceComponent },
      { path: 'PastAttendance', component: PastAttendanceComponent },
      { path: 'PastAttendanceApprove', component: PastAttendanceApproveComponent },
      { path: 'dummycalendar', component: DummycalendarComponent },

      { path: 'sremplyoeedetails', component: ServiceRegisterComponent },
      { path: 'srleaveledger', component: SrleaveledgerComponent },
      { path: 'ServiceDetails', component: ServiceDetailsComponent },
      { path: 'srpayrolldetails', component: SrpayrolldetailsComponent },

      { path: 'ChangePassword', component: ChangePasswordComponent },
      { path: 'ScrollingMessage', component: ScrollingMessageComponent },
      { path: 'Newsupdate', component: NewsUpdateComponent },
      { path: 'Employeemaster', component: EmployeemasterComponent },
      { path: 'Registrationmaster', component: RegistrationmasterComponent },
      { path: 'Userpermission', component: UserpermissionComponent },
      { path: 'MobileUserpermission', component: MobileUserPermissionComponent },
      { path: 'WarehouseRegistrationMaster', component: WarehouseregistrationmasterComponent },
      { path: 'whyaspcwc', component: ApswcWhyEditorComponent },
      { path: 'ourvision', component: ApswcOurvisionComponent },
      { path: 'ourobjective', component: ApswcOurobjectiveComponent },
      { path: 'WarehouseLayout', component: WarehouselayoutComponent },
      { path: 'cmmimisterphotos', component: ApswcHomepageImagesComponent },
      { path: 'GeneralBooking', component: GeneralbookingComponent },
      { path: 'WarehouseConfiguration', component: WarehouselayoutconfigComponent },
      { path: 'OutsourcingAgencies', component: OutsourcingAgenciesComponent },
      { path: 'DeadStock', component: DeadStockComponent },
      { path: 'Enggmaintenance', component: EnggmaintenanceComponent },
      { path: 'Deadstockrgnapproval', component: DeadstockrgnapprovalComponent },
      { path: 'Deadstocksecapproval', component: DeadstocksecapprovalComponent },
      { path: 'GateIn', component: GateInComponent },
      { path: 'WeighmentIn', component: WeighmentInComponent },
      { path: 'QualityExamination', component: QualityCheckingComponent },
      { path: 'layoutStructure', component: LayoutStructureComponent },
      { path: 'Stacking', component: StackingComponent },
      { path: 'Insurance', component: InsuranceCompanyComponent },
      { path: 'GateOut', component: GateOutComponent },
      { path: 'Weighmentout', component: WeighmentOutComponent },
      { path: 'Dumping', component: DumpingComponent },
      { path: 'ReceiptOutRequest', component: ReceiptOutRequestComponent },
      { path: 'OutGateIn', component: OutGateInComponent },
      { path: 'StackOut', component: StackOutComponent },
      { path: 'OutWeighmentIn', component: OutWeighmentInComponent },
      { path: 'OutWeignmentOut', component: OutWeighmentOutComponent },
      { path: 'PropertyWaterTaxPayment', component: PropertyWaterTaxPaymentComponent },
      { path: 'ReceiptINRequest', component: ReceiptInRequestComponent },
      // {path:'QC_Out',component:QualityCheckingtOutComponent},
      { path: 'Out_GateOut', component: OutGateOutComponent },
      { path: 'Periodic_QC', component: PeriodicQualityCheckingComponent },
      { path: 'Adminhelp', component: AdminhelpComponent },
      { path: 'Assetmanagement', component: AssetmanagementComponent },
      { path: 'EmployeePayRole', component: EmployeePayRoleComponent },
      { path: 'WHReceipt', component: WhReceiptComponent },
      { path: 'OGateIn', component: OgateInComponent },
      { path: 'OQuality', component: OqualityCheckComponent },
      { path: 'OWeighmentIn', component: OweighmentInComponent },
      { path: 'OStackIn', component: OstackInComponent },
      { path: 'OWeighmentOut', component: OweighmentOutComponent },
      { path: 'OGateOut', component: OgateOutComponent },
      { path: 'EmployeePayroleHistory', component: EmployeePayroleHistoryComponent },
      { path: 'MailSettings', component: MailSettingsComponent },
      { path: 'StockRegister', component: StockRegisterComponent },
      { path: 'LorryWBRegister', component: LorryWeighbridgeReportComponent },
      { path: 'EmployeeloanRequest', component: EmployeeloanRequestComponent },
      { path: 'EmployeeloanApproval', component: EmployeeloanApprovalComponent },
      { path: 'MDlevelApproval', component: MDlevelApprovalComponent },
      { path: 'DailyValuation', component: DailyValuationStockComponent },
      { path: 'OpeningBalance', component: OpeningBalanceComponent },
      { path: 'ImprestRegister', component: ImprestRegisterComponent },
      { path: 'DailyTransReg', component: DailyTransactionRegComponent },
      { path: 'BLRegister', component: BankloanregisterComponent },
      { path: 'DepositorLedger', component: DepositorLedgerComponent },
      { path: 'WHSpillage', component: WarehouseSpillageComponent },
      { path: 'WHChemical', component: WHChemicalConsumptionComponent },
      { path: 'WarehouseInvoiceMaster', component: WarehouseInvoiceMatserComponent },
      { path: 'WarehousequalityMaster', component: WarehousequalityMatserComponent },
      { path: 'WarehouseReservationMaster', component: WarehouseReservationMatserComponent },
      { path: 'MedicalReimbursementMaster', component: MedicalreimbursementMatserComponent },
      { path: 'MedicalReimbursementRequest', component: MedicalReimbursementRequestComponent },
      { path: 'MedicalReimbursementApproval', component: MedicalReimbursementApprovalComponent },
      { path: 'StackRegister', component: StackRegisterComponent },
      { path: 'WarehouseManagerDashboard', component: WarehousemanagerComponent },
      { path: 'SpaceReservation', component: SpaceReservationComponent },
      { path: 'DepositerInvoice', component: DepositerInvoiceComponent },
      { path: 'CreditDebitNoteInvoice', component: CreditDebitInvoiceComponent },
      { path: 'HTContractDetails', component: HandtdetailsComponent },
      { path: 'WHHTMapping', component: WhhtMappingComponent },
      { path: 'QuantityExamination', component: QuantityExaminationComponent },
      { path: 'WeightCheckMemo', component: WhackComponent },
      { path: 'HRdashboard', component: HrdashboardComponent },
      { path: 'FinanceDashboard', component: FinanceDashboardComponent },
      { path: 'CashBookReport', component: CashBookComponent },
      { path: 'ExistingStockEntry', component: ExistingStockEntryComponent },
      { path: 'StorageLossEntry', component: StorageLossEntryComponent },
    ]
  },

  { path: 'Login', component: LoginComponent },
 
  { path: '*', redirectTo: '' },
  { path: '**', component: PagenotfoundComponent, pathMatch: 'full' },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }
