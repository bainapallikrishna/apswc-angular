import { TestBed } from '@angular/core/testing';

import { AadharValidateService } from './aadhar-validate.service';

describe('AadharValidateService', () => {
  let service: AadharValidateService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AadharValidateService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
