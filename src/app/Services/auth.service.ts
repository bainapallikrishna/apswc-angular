import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { User, Captcha } from 'src/app/Interfaces/login'
import{ EncrDecrServiceService}from 'src/app/Services/encr-decr-service';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  //private baseUrl ="https://apswc.ap.gov.in/apswcapp/api/APSWC"; //"http://localhost:53946/api/APSWC";
  private baseUrl =  "http://uat.apswc.ap.gov.in/apswcapp/api/APSWC"; //"http://localhost:53946/api/APSWC";

  //private captchUrl: string = 'http://localhost:53946/api/APSWC/Captcha';



  userData = new BehaviorSubject<User>(new User());

  constructor(private http: HttpClient, private router: Router, private EncrDecr:EncrDecrServiceService) { }

  login(methodname: string, userDetails) {
   // var encrypted=JSON.stringify(this.EncrDecr.set(userDetails)); 
    return this.http.post<any>(`${this.baseUrl}/${methodname}`, userDetails)
      .pipe(map(response => {
        if (response.StatusCode == 100) {
          localStorage.setItem('authToken', response.token);
          this.setUserDetails();
          sessionStorage.setItem("logUserCode", response.Details[0].useR_NAME);
          sessionStorage.setItem("logUserisChangePassword", response.Details[0].iS_PASSWORD_CHANGE);
          
          sessionStorage.setItem("logUserSectionID", response.Details[0].officeR_DESIGNATION_ID);
          sessionStorage.setItem("logUserworkLocationCode", response.Details[0].worK_LOCATION_CODE);
          sessionStorage.setItem("logUserworkLocationName", response.Details[0].worK_LOCATION_TYPE);
        }
        return response;
      }));
  }

  Getcaptchload(methodname:string) {
    return this.http.get<any>(`${this.baseUrl}/${methodname}`)
      .pipe(map(response => {
        return response;
      }));
  }
  setUserDetails() {
    if (localStorage.getItem('authToken')) {
      const userDetails = new User();
      const decodeUserDetails = JSON.parse(window.atob(localStorage.getItem('authToken').split('.')[1]));
      console.log(decodeUserDetails);
      userDetails.userName = decodeUserDetails.sub;
      userDetails.firstName = decodeUserDetails.firstName;
      userDetails.isLoggedIn = true;
      userDetails.role = decodeUserDetails.role;

      this.userData.next(userDetails);

      sessionStorage.setItem("logUserName",decodeUserDetails.sub);
      sessionStorage.setItem("logUserfirstName",decodeUserDetails.firstName);
      sessionStorage.setItem("isLoggedIn","true");
      sessionStorage.setItem("logUserrole",decodeUserDetails.role);
      sessionStorage.setItem("logintime", "" + (new Date().getTime() + (30 * 60000)));

    }
  }

  logout() {
    localStorage.removeItem('authToken');

    sessionStorage.removeItem("logUserName");
    sessionStorage.removeItem("logUserfirstName");
    sessionStorage.removeItem("isLoggedIn");
    sessionStorage.removeItem("logUserrole");
    sessionStorage.removeItem("logUserCode");
    sessionStorage.removeItem("logUserisChangePassword");
    sessionStorage.removeItem("logintime");
    sessionStorage.removeItem("logUserSectionID");
    sessionStorage.removeItem("logUserworkLocationCode");
    sessionStorage.removeItem("logUserworkLocationName");
    sessionStorage.clear();

    this.router.navigate(['/Login']);
    this.userData.next(new User());
  }
}
