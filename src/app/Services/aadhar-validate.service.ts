import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AadharValidateService {
  d:any[];
  p:any[];
  constructor() { 
  // multiplication table
  this.d = [
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    [1, 2, 3, 4, 0, 6, 7, 8, 9, 5], 
    [2, 3, 4, 0, 1, 7, 8, 9, 5, 6], 
    [3, 4, 0, 1, 2, 8, 9, 5, 6, 7], 
    [4, 0, 1, 2, 3, 9, 5, 6, 7, 8], 
    [5, 9, 8, 7, 6, 0, 4, 3, 2, 1], 
    [6, 5, 9, 8, 7, 1, 0, 4, 3, 2], 
    [7, 6, 5, 9, 8, 2, 1, 0, 4, 3], 
    [8, 7, 6, 5, 9, 3, 2, 1, 0, 4], 
    [9, 8, 7, 6, 5, 4, 3, 2, 1, 0]
  ]
  
  // permutation table
  this.p = [
    [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 
    [1, 5, 7, 6, 2, 8, 3, 0, 9, 4], 
    [5, 8, 0, 3, 7, 9, 6, 1, 4, 2], 
    [8, 9, 1, 6, 0, 4, 3, 5, 2, 7], 
    [9, 4, 5, 3, 1, 2, 6, 8, 7, 0], 
    [4, 2, 8, 6, 5, 7, 3, 9, 0, 1], 
    [2, 7, 9, 3, 8, 0, 6, 4, 1, 5], 
    [7, 0, 4, 6, 9, 1, 3, 2, 5, 8]
  ]

  }
 



// validates Aadhar number received as string
validate(aadharNumber) {
 
  let c = 0
  let invertedArray = aadharNumber.split('').map(Number).reverse()

  invertedArray.forEach((val, i) => {
	  c = this.d[c][this.p[(i % 8)][val]]
  })

  return (c === 0)
}
 validatemob(mob_uid_num){
  let c = 0
   if (mob_uid_num.length===10) {
    if (mob_uid_num==="6666666666" || mob_uid_num==="7777777777" || 
    mob_uid_num==="8888888888" || mob_uid_num==="9999999999") 
    return (c === 1)
    else
    return (c === 0)
   }
   else if(mob_uid_num.length===12){
   if(mob_uid_num==="111111111111" || mob_uid_num==="222222222222" || 
   mob_uid_num==="333333333333" || mob_uid_num==="444444444444" || 
   mob_uid_num==="555555555555" || mob_uid_num==="666666666666" || mob_uid_num==="777777777777" || mob_uid_num==="888888888888"|| mob_uid_num==="999999999999")
    return (c === 1)
    else
    return (c === 0)
    
   }
   
  
 

 }
}


