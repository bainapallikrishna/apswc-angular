import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { getAllJSDocTagsOfKind } from 'typescript';
import { map } from 'rxjs/operators';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import { SafeResourceUrl } from '@angular/platform-browser';

@Injectable({
    providedIn: 'root'
})

export class CommonServices {

    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8',
            'Accept-Language': 'application/json'
        })
    }

    public mapsUrl = "http://uat.apswc.ap.gov.in/gowdonmap/home/index";
    //public mapsUrl  = "https://apswc.ap.gov.in/gowdonmap/home/index";

    public pdfUrl = "http://uat.apswc.ap.gov.in/gowdonmap/api/LatLong";
    //public pdfUrl = "https://apswc.ap.gov.in/gowdonmap/api/LatLong";
 
    //public baseUrl ="http://localhost:53946/api/APSWC";
    public baseUrl ="http://uat.apswc.ap.gov.in/apswcapp/api/APSWC"; //"http://localhost:53946/api/APSWC"; 
    public filebaseUrl= "http://uat.apswc.ap.gov.in/apswcapp/api/FilesUpload"; //"http://localhost:53946/api/FilesUpload";
    //public filebaseUrl="https://apswc.ap.gov.in/apswcapp/api/FilesUpload"; //"http://localhost:53946/api/FilesUpload";  
    //public baseUrl ="https://apswc.ap.gov.in/apswcapp/api/APSWC"; //"http://localhost:53946/api/APSWC";  


  //  public filebaseUrl= "http://localhost:53946/api/FilesUpload";

    constructor(private http: HttpClient, private router: Router, private EncrDecr: EncrDecrServiceService) { }

    getPosition(): Promise<any> {
        return new Promise((resolve, reject) => {

            navigator.geolocation.getCurrentPosition(resp => {

                resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
            },
                err => {
                    reject(err);
                });
        });

    }


    CheckLocation(): Promise<any> {
        return new Promise((resolve, reject) => {

            navigator.permissions.query({
                name: 'geolocation'
              }).then(resp => {

                resolve(resp);
            },
                err => {
                    reject(err);
                });
        });

    }

    postData(body: Object, methodname: string): Observable<any> {
        //var encrypted=JSON.stringify(this.EncrDecr.set(body)); 
        return this.http.post(`${this.baseUrl}/${methodname}`, body, this.httpOptions).pipe(map(response => {
            return response;
        }));
    }   

    getData(methodname: string): Observable<any> {
        return this.http.get(`${this.baseUrl}/${methodname}`, this.httpOptions)
            .pipe(map(response => {
                return response;
            }));
    }

    UploadFile(body: FormData, methodename: string): Observable<any> {
        return this.http.post(`${this.filebaseUrl}/${methodename}`, body, { reportProgress: true, observe: 'events' })
            .pipe(map(response => {
                return response;
            }))
    }
    digilockFile(body: object, methodename: string): Observable<any> {
        return this.http.post(`${this.filebaseUrl}/${methodename}`, body, this.httpOptions).pipe(map(response => {
            return response;
        })); 
       }

    encryptUploadFile(body: FormData, methodename: string): Observable<any> {
        return this.http.post(`${this.filebaseUrl}/${methodename}`, body,{ reportProgress: true, observe: 'events' })
            .pipe(map(response => {
                return response;
            }))
    }

    decryptFile(filePath: string, methodename: string): Observable<any> {
        let path = {
            DBPath: filePath
        }
        return this.http.post(`${this.filebaseUrl}/${methodename}`, path, this.httpOptions)
            .pipe(map(response => {
                return response;
            }))
    }

    
    postPDFData(body: Object, methodname: string): Observable<any> {
        //var encrypted=JSON.stringify(this.EncrDecr.set(body)); 
        return this.http.post(`${this.pdfUrl}/${methodname}`, body, this.httpOptions).pipe(map(response => {
            return response;
        }));
    }

    s_sd(content, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        var byteCharacters = window.atob(content);
        var byteArrays = [
        ];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, {
            type: contentType
        });
        return blob;
    }
}
