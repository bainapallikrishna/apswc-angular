import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { getAllJSDocTagsOfKind } from 'typescript';
import { map } from 'rxjs/operators';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import { SafeResourceUrl } from '@angular/platform-browser';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { nullSafeIsEquivalent } from '@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root'
})

export class DigiServices {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      'Accept-Language': 'application/json'
    })
  }

  code: any;
  Accesstoken: any;
  FilesDD: any;
  Docpath: any;
  Authstatus: any;
  UploadFilesDD: any;
  IssuedDocView: boolean = true;
  viewbtnname: any;
  uri: any;
  showloader: any;
  Formaddarray: any;
  FilesList: any;
  pagename: any;

  constructor(private http: HttpClient, private router: Router, private service: CommonServices, private EncrDecr: EncrDecrServiceService) { }

  Digilock(pagename, _callback) {
    this.showloader = true;
    const req = new InputRequest();

    req.TYPEID = "103";
    this.pagename = pagename;

    this.service.postData(req, "DigiLocker").subscribe(data => {
      this.Formaddarray = data.result;
      if (data.StatusCode == 100) {
        var url = 'https://api.digitallocker.gov.in/public/oauth2/1/authorize?response_type=code&client_id=D6DB15C2&redirect_uri =apswc.ap.gov.in&state=' + data.Details[0]["transactioN_ID"] + '';
       var digwin= window.open(url, '_blank', "width=600,height=600");
        this.GetDigiCode(data.Details[0]["transactioN_ID"], _callback,digwin);
      }
      else {
        this.showloader = false;
        return null;
      }
    });
  }

  GetDigiCode(val, _callback,digwin) {
    const req = new InputRequest();

    req.INPUT_01 = val;
    req.TYPEID = "105";

    this.service.postData(req, "DigiLocker").subscribe(data => {
      this.Formaddarray = data.result;
      if (data.StatusCode == 100) {
        this.code = data.Details[0]["digilockeR_CODE"];
        if (!this.code) {
          this.GetDigiCode(val, _callback,digwin);
        }
        else {
          this.GetAccessToken(this.code, _callback,digwin);
        }
      }
      else {
        this.GetDigiCode(val, _callback,digwin);
      }
    });
  }

  GetAccessToken(val, _callback,digwin) {
    const req = new InputRequest();

    req.code = val;
    req.RedirectURL = "WH-EMPLOYE";

    this.service.postData(req, "DLAccessToken").subscribe(data => {
      this.Formaddarray = data.result;
      if (data.Status == "Success") {
        this.Accesstoken = data.DigiLockerToken.access_token;

        this.GetDigiFiles(this.Accesstoken, _callback,digwin);
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    });
  }

  GetDigiFiles(val, _callback,digwin) {
    const req = new InputRequest();

    req.token = val;

    this.service.postData(req, "DLIssuedfiles").subscribe(data => {
      this.Formaddarray = data.result;
      if (data.Status == "Success") {
        this.FilesDD = data.UserFiles.items;
        if (data.UserUploadFiles)
          this.UploadFilesDD = data.UserUploadFiles.items;

          digwin.close();
        _callback(data, val);

      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    });
  }

  ShowFiles(val, mimer, num, name, digitok, _callback) {
    this.showloader = true;
    const req = new InputRequest();
    req.token = digitok;
    req.url = val;
    req.pagename = this.pagename;
    if (num == "1") {
      if (mimer.includes("application/pdf")) {
        req.mime = "application/pdf";
      }
      else {
        req.mime = mimer[0];
      }
    }
    else if (num == "2") {
      req.mime = mimer;
    }


    this.service.digilockFile(req, "DLDownloadPdf").subscribe(data => {
      this.Formaddarray = data.result;
      if (data.StatusCode == "100") {
        this.showloader = false;
        this.Docpath = data.StatusMessage;
        _callback(this.Docpath, req.url);
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    });
  }

  GetDetails(uri, digitok, _callback) {
    const req = new InputRequest();

    req.token = digitok;
    req.url = uri;

    this.service.postData(req, "DLCertfIssuerData").subscribe(data => {
      this.Formaddarray = data.result;
      if (data.Status == "Success") {
        _callback(data);
      }
      else {
        this.showloader = false;
        Swal.fire("info", data.StatusMessage, "info");
      }
    });
  }

}