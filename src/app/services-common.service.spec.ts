import { TestBed } from '@angular/core/testing';

import { ServicesCommonService } from './services-common.service';

describe('ServicesCommonService', () => {
  let service: ServicesCommonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicesCommonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
