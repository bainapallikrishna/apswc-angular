import { Component, OnInit } from '@angular/core';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';


@Component({
  selector: 'app-tenders',
  templateUrl: './tenders.component.html',
  styleUrls: ['./tenders.component.css']
})
export class TendersComponent implements OnInit {

  tenderlist: any = [];
  constructor(private service: CommonServices) { }


  ngOnInit(): void {
    this.Gettendermessage();
  }

  Gettendermessage() {
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "GET_TENDERS";

    this.service.postData(req, "GetScrollNewMessage").subscribe(data => {
      if (data.StatusCode == "100") {
        this.tenderlist = data.Details;

      }

    });

  }
  viewdata(url: string) {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open(url, '_blank');
      return true;
    }
    else {
      return false;
    }
  }
  Redirect() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('https://tender.apeprocurement.gov.in/', '_blank');
      return true;
    }
    else {
      return false;
    }
  }
}
