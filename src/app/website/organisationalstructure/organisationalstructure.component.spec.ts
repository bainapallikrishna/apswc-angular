import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationalstructureComponent } from './organisationalstructure.component';

describe('OrganisationalstructureComponent', () => {
  let component: OrganisationalstructureComponent;
  let fixture: ComponentFixture<OrganisationalstructureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationalstructureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationalstructureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
