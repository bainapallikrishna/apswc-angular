import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";

@Component({
  selector: 'app-organisationalstructure',
  templateUrl: './organisationalstructure.component.html',
  styleUrls: ['./organisationalstructure.component.css']
})
export class OrganisationalstructureComponent implements OnInit {

  constructor() { }


  ngOnInit(): void {   
    this.loadScript('./assets/web-styles/js/orgchart.js');
    this.loadScript('./assets/web-styles/js/vieworg.js'); 

  }
  public loadScript(url: string) {
    const body = <HTMLDivElement> document.body;
    const script = document.createElement('script');
    script.innerHTML = '';
    script.src = url;
    script.async = false;
    script.defer = true;
    script.type = 'text/javascript';
    body.appendChild(script);
  }

  



  
}
