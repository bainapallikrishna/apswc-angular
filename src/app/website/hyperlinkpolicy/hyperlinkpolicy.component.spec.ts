import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HyperlinkpolicyComponent } from './hyperlinkpolicy.component';

describe('HyperlinkpolicyComponent', () => {
  let component: HyperlinkpolicyComponent;
  let fixture: ComponentFixture<HyperlinkpolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HyperlinkpolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HyperlinkpolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
