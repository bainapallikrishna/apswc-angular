import { Component, OnInit } from '@angular/core';
import { CommonServices } from 'src/app/Services/common.services';

@Component({
  selector: 'app-boardofdirectors',
  templateUrl: './boardofdirectors.component.html',
  styleUrls: ['./boardofdirectors.component.css']
})
export class BoardofdirectorsComponent implements OnInit {
  boardofDirectorsList: any[];

  constructor(private service: CommonServices) { }

  ngOnInit(): void {



    this.service.getData("GetBoardofDirectors").subscribe(data => {



      this.boardofDirectorsList = data.Details;
      console.log(this.boardofDirectorsList);


    },
      error => console.log(error));
  }

}
