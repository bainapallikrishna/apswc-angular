import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarehouseactComponent } from './warehouseact.component';

describe('WarehouseactComponent', () => {
  let component: WarehouseactComponent;
  let fixture: ComponentFixture<WarehouseactComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarehouseactComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarehouseactComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
