import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { CircleProgressComponent, CircleProgressOptions } from 'ng-circle-progress';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { Color } from 'ng-chartjs';
import { CommonServices } from 'src/app/Services/common.services';
import { Dashboard } from 'src/app/Interfaces/user';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import * as $ from "jquery";
import { InputRequest } from 'src/app/Interfaces/employee';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent {

  dashboardval: Dashboard;
  ulapswcabout: string;
  ulapswcwhy: string;
  ulourvision: string;
  ulourobjective: string;
  cmname: string;
  cmphoto: string;
  ministerphoto: string;
  ministername: string;
  ministerdesignation: string;
  // Pie Chart 
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['Occupied', 'Available'];
  public pieChartData: number[] = [1270675, 300936];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['#488DF5', '#1b2942'],
    },
  ];

  @ViewChild('circleProgress') circleProgress: CircleProgressComponent;

  // Line Chart Homepage Start  
  public lineChartData: ChartDataSets[] = [
    { data: [1201962, 1276707, 1318704, 1425430, 1682246], label: 'Total' },
    { data: [1139171, 1148973, 1138287, 1256139, 1544926], label: 'Occupied' },
  ];
  public lineChartLabels: Label[] = ['2017', '2018', '2019', '2020', '2021'];
  public lineChartOptions: any = {
    responsive: true,
    lineTension: 0
  };
  public lineChartColors: Color[] = [
    {
      borderColor: "#c9d07a",
      backgroundColor: ["rgba(240,244,195,0.3)"],
    },
    {
      borderColor: "#009688",
      backgroundColor: ["rgba(0,150,136,0.3)"],
    }
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  tenderlist: any = [];
  // Line Chart Homepage End

  availbleper: number;
  occupiedper: number;
  scrollmesage: string;
  newslist: any = [];
  constructor(private service: CommonServices, private sanitizer: DomSanitizer) {
  }

  MapsURL() {
    return this.sanitizer.bypassSecurityTrustUrl(this.service.mapsUrl);
  }


  ngOnInit(): void {

    this.Gettendermessage();
    this.Getscrollmessage();
    this.Getnewsmessage();

    this.service.getData("GetSpaceDetails").subscribe(data => {

      if (data.StatusCode == "100") {
        console.log(data.Details[0]);

        this.dashboardval = data.Details[0];
        //console.log(data.Details);
        this.occupiedper = (this.dashboardval.occupieD_SPACE / this.dashboardval.totaL_SPACE) * (100);
        this.availbleper = (this.dashboardval.availablE_SPACE / this.dashboardval.totaL_SPACE) * (100);
      }

    },
      error => console.log(error));

    //home page content
    this.service.getData("GetHomePageConent").subscribe(data => {

      if (data.StatusCode == "100") {
        console.log(data.Details[0]);
        this.ulapswcabout = data.Details[0]["abouT_APSWC"];
        this.ulapswcwhy = data.Details[0]["whY_APSWC"];
        this.ulourvision = data.Details[0]["ouR_VISION"];
        this.ulourobjective = data.Details[0]["ouR_OBJECTIVES"];
        this.cmphoto = data.Details[0]["cM_PHOTO"];
        this.ministername = data.Details[0]["ministeR_NAME"];
        this.ministerdesignation = data.Details[0]["ministeR_DESIGNATION"];
        this.ministerphoto = data.Details[0]["ministeR_PHOTO"];

      }

    },
      error => console.log(error));



    // daylight

    // var now = (new Date()).getTime();
    // var fakeMidnight = now + (5 * 1000);
    // var untilMidnight = (fakeMidnight - now);


    // console.log("it will become night over the next : " + (untilMidnight/ 1000) + " seconds.")
    // $("slider-animation").animate({'background-color':'#000'}, (fakeMidnight - now), 'linear')

    // Homepage Banner vechile Animation 
    let toogle = 1;
    let MAX_POSITION = 600;
    const truck1 = <HTMLElement>document.querySelector("#pic");
    const truck2 = <HTMLElement>document.querySelector("#pic1");
    const truck3 = <HTMLElement>document.querySelector("#pic2");
    const load = <HTMLElement>document.querySelector("#load");
    function sleep(ms) {
      return new Promise((resolve) => setTimeout(resolve, ms));
    }
    let position = 0;
    truck2.style.display = "none";
    truck3.style.display = "none";

    async function animate() {
      position += 1;
      if (position > MAX_POSITION) {

        await sleep(500);
        load.style.display = "none";
        await sleep(3000);
        // position = 0;
        position += 1;
        MAX_POSITION += 800;
        // load.style.display = 'block';
      }


      if (position === 1024) {
        // load.style.display = "none";


        if (toogle === 1) {
          truck1.style.display = "none";

          await sleep(3000);
          truck2.style.display = "block";

          // await sleep(6000);
          // truck3.style.display = "block";
          toogle = 2;
        }

        else if (toogle === 2) {
          truck2.style.display = "none";
          await sleep(3000);
          truck3.style.display = "block";
          toogle = 3;
        }

        else if (toogle === 3) {
          truck3.style.display = "none";
          await sleep(3000);
          load.style.display = 'block';
          truck1.style.display = "block";
          toogle = 1;
        }

        position = 0;
        MAX_POSITION = 600;
      }
      truck1.style.transform = `translateX(${position}px)`;
      truck2.style.transform = `translateX(${position}px)`;
      truck3.style.transform = `translateX(${position}px)`;
      load.style.transform = `translateX(${position}px)`;
      await sleep(10);
      animate();
    }
    animate();


  }


  Gettendermessage() {
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "GET_TENDERS";

    this.service.postData(req, "GetScrollNewMessage").subscribe(data => {
      if (data.StatusCode == "100") {
        this.tenderlist = data.Details;
      }
    });

  }

  Getscrollmessage() {
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "GET_SCROLL";

    this.service.postData(req, "GetScrollNewMessage").subscribe(data => {

      this.scrollmesage = "";
      if (data.StatusCode == "100") {

        data.Details.forEach(item => {
          if (item.iS_ACTIVE == '1') {
            this.scrollmesage += item.contenT_BODY + " "
          }

        });
        console.log(this.scrollmesage);

      }

    });

  }

  Getnewsmessage() {
    const req = new InputRequest();
    req.DIRECTION_ID = "3";
    req.TYPEID = "GET_NEWS";

    this.service.postData(req, "GetScrollNewMessage").subscribe(data => {

      this.scrollmesage = "";
      if (data.StatusCode == "100") {

        this.newslist = data.Details;


      }

    });

  }

  // Owl Carsoule

  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    margin: 15,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 3
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: false
  }

  onClickMe() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('https://wdra.gov.in/', '_blank');
      return true;
    }
    else {
      return false;
    }
  }
  onClickMe2() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('http://cewacor.nic.in/', '_blank');
      return true;
    }
    else {
      return false;
    }
  }

  _timer = null;

  controlGroups = [
    {
      groupName: 'Basic', controls: [
        { name: 'percent', type: 'range', min: 1, max: 1000, step: 0.01 },
        { name: 'maxPercent', type: 'range', min: 50, max: 1000, step: 10 },
        { name: 'toFixed', type: 'range', min: 0, max: 5, step: 1 },
        { name: 'showTitle', type: 'checkbox' },
        { name: 'showUnits', type: 'checkbox' },
        { name: 'showSubtitle', type: 'checkbox' },
        { name: 'showImage', type: 'checkbox' },
        { name: 'showBackground', type: 'checkbox' },
        { name: 'showInnerStroke', type: 'checkbox' },
        { name: 'clockwise', type: 'checkbox' },
        { name: 'responsive', type: 'checkbox' },
        { name: 'startFromZero', type: 'checkbox' },
        { name: 'showZeroOuterStroke', type: 'checkbox' },
      ]
    },
    {
      groupName: 'Size', controls: [
        { name: 'radius', type: 'range', min: 20, max: 250, step: 1 },
        { name: 'backgroundPadding', type: 'range', min: -50, max: 50, step: 1 },
        { name: 'imageHeight', type: 'range', min: 20, max: 250, step: 1 },
        { name: 'imageWidth', type: 'range', min: 20, max: 250, step: 1 },
      ],
    },
    {
      groupName: 'Color', controls: [
        { name: 'backgroundGradient', type: 'checkbox' },
        { name: 'backgroundOpacity', type: 'range', min: 0, max: 1, step: 0.1 },
        { name: 'backgroundColor', type: 'color' },
        { name: 'backgroundGradientStopColor', type: 'color' },
        { name: 'backgroundStroke', type: 'color' },
        { name: 'outerStrokeGradient', type: 'checkbox' },
        { name: 'outerStrokeColor', type: 'color' },
        { name: 'outerStrokeGradientStopColor', type: 'color' },
        { name: 'innerStrokeColor', type: 'color' },
        { name: 'titleColor', type: 'color' },
        { name: 'unitsColor', type: 'color' },
        { name: 'subtitleColor', type: 'color' },
      ],
    },
    {
      groupName: 'Stroke', controls: [
        { name: 'outerStrokeWidth', type: 'range', min: 1, max: 50, step: 1 },
        { name: 'space', type: 'range', min: -20, max: 50, step: 1 },
        { name: 'innerStrokeWidth', type: 'range', min: 0, max: 50, step: 1 },
        { name: 'backgroundStrokeWidth', type: 'range', min: 0, max: 50, step: 1 },
        { name: 'outerStrokeLinecap', type: 'select', options: ['butt', 'round', 'square', 'inherit'] },
      ],
    },
    {
      groupName: 'Font', controls: [
        { name: 'titleFontSize', type: 'range', min: 10, max: 100, step: 1 },
        { name: 'unitsFontSize', type: 'range', min: 10, max: 100, step: 1 },
        { name: 'subtitleFontSize', type: 'range', min: 10, max: 100, step: 1 },
        { name: 'titleFontWeight', type: 'range', min: 100, max: 900, step: 100 },
        { name: 'unitsFontWeight', type: 'range', min: 100, max: 900, step: 100 },
        { name: 'subtitleFontWeight', type: 'range', min: 100, max: 900, step: 100 },
      ]
    },
    {
      groupName: 'Animation', controls: [
        { name: 'animation', type: 'checkbox' },
        { name: 'animateTitle', type: 'checkbox' },
        { name: 'lazy', type: 'checkbox' },
        { name: 'animationDuration', type: 'range', min: 0, max: 10000, step: 100 },
      ]
    },
  ]

  options = new CircleProgressOptions();


  ngCircleOptions = {
    percent: 85,
    radius: 60,
    showBackground: false,
    outerStrokeWidth: 10,
    innerStrokeWidth: 5,
    startFromZero: false,
    outerStrokeColor: null,
    showSubtitle: false,
    subtitleFormat: (percent: number): string => {
      if (percent < 25) {
        this.ngCircleOptions.outerStrokeColor = "red";
      } else if (percent < 50) {
        this.ngCircleOptions.outerStrokeColor = "yellow";
      } else if (percent < 75) {
        this.ngCircleOptions.outerStrokeColor = "blue";
      } else {
        this.ngCircleOptions.outerStrokeColor = "green";
      }
      return '';
    }
  }

  optionsA = {
    percent: 85,
    radius: 60,
    showBackground: false,
    outerStrokeWidth: 10,
    innerStrokeWidth: 5,
    subtitleFormat: false,  // clear subtitleFormat coming from other options, because Angular does not assign if variable is undefined. 
    startFromZero: false,
  }

  optionsB = {
    percent: 50,
    maxPercent: 200,
    radius: 60,
    showSubtitle: false,
    showInnerStroke: false,
    outerStrokeWidth: 5,
    outerStrokeColor: '#FFFFFF',
    innerStrokeColor: '#FFFFFF',
    backgroundColor: '#FDB900',
    backgroundStrokeWidth: 0,
    backgroundPadding: 5,
    titleColor: '#483500',
    units: ' Point',
    unitsColor: '#483500',
    subtitleColor: '#483500',
    subtitleFormat: false,  // clear subtitleFormat coming from other options, because Angular does not assign if variable is undefined. 
    startFromZero: false,
  }

  optionsC = {
    percent: 99.99,
    radius: 60,
    outerStrokeWidth: 10,
    innerStrokeWidth: 1,
    backgroundColor: '#F1F1F1',
    backgroundPadding: -18,
    backgroundStrokeWidth: 0,
    innerStrokeColor: '#32CD32',
    outerStrokeColor: '#FF6347',
    toFixed: 2,
    subtitleFormat: false,  // clear subtitleFormat coming from other options, because Angular does not assign if variable is undefined. 
    startFromZero: false,
  }

  optionsD = {
    percent: 101,
    maxPercent: 100,
    radius: 60,
    showInnerStroke: false,
    outerStrokeWidth: 10,
    innerStrokeWidth: 0,
    backgroundPadding: -10,
    backgroundStrokeWidth: 0,
    outerStrokeColor: '#61A9DC',
    backgroundColor: '#ffffff',
    backgroundGradientStopColor: '#c0c0c0',
    backgroundGradient: true,
    subtitleColor: '#444444',
    startFromZero: false,
    subtitleFormat: (percent: number): string => {
      if (percent >= 100) {
        return "Congratulations!"
      } else {
        return "Progress"
      }
    }
  }

  optionsE = {
    percent: 75,
    radius: 60,
    outerStrokeWidth: 10,
    innerStrokeWidth: 10,
    space: -10,
    outerStrokeColor: "#4882c2",
    innerStrokeColor: "#e7e8ea",
    showBackground: false,
    title: 'UI',
    animateTitle: false,
    showUnits: false,
    clockwise: false,
    animationDuration: 1000,
    startFromZero: false,
    outerStrokeGradient: true,
    outerStrokeGradientStopColor: '#53a9ff',
    lazy: true,
    subtitleFormat: (percent: number): string => {
      return `${percent}%`;
    }
  }

  optionsF = {
    percent: 60,
    radius: 60,
    backgroundPadding: 7,
    outerStrokeWidth: 2,
    innerStrokeWidth: 2,
    space: -2,
    outerStrokeColor: "#808080",
    innerStrokeColor: "#e7e8ea",
    showBackground: true,
    title: ['working', 'in', 'progress'],
    titleFontSize: 12,
    subtitleFontSize: 20,
    animateTitle: false,
    showUnits: false,
    clockwise: false,
    animationDuration: 1000,
    subtitleFormat: (percent: number): string => {
      return `${percent}%`;
    }
  }

  optionsG = {
    percent: 75,
    radius: 60,
    outerStrokeWidth: 5,
    innerStrokeWidth: 5,
    space: -5,
    outerStrokeColor: "#76C2AF",
    innerStrokeColor: "#ffffff",
    showBackground: false,
    showImage: true,
    imageSrc: "assets/images/music.svg",
    imageHeight: 105,
    imageWidth: 105,
  }



  onValueChanged = (event) => {
    try {
      if (event.srcElement.name === 'toFixed') {
        let toFixed = +event.srcElement.value;
        this.controlGroups[0].controls[0]['step'] = 1 / Math.pow(10, toFixed);
      }
    } catch (e) {
      console.error(e)
    }
  }

  copyOptions = (event, options) => {
    this.options = Object.assign({}, this.circleProgress.defaultOptions, options);
  }

  resetOptions = () => {
    this.stop();
    this.options = new CircleProgressOptions();
  }

  start = () => {
    if (this._timer !== null) {
      clearInterval(this._timer);
    }
    this._timer = window.setInterval(() => {
      this.options.percent = (Math.round(Math.random() * 100));
    }, 1000);
  }

  stop = () => {
    if (this._timer !== null) {
      clearInterval(this._timer);
      this._timer = null;
    }
  }

  destroyed: Boolean = false;

  toggleDestroyed = () => {
    this.destroyed = !this.destroyed;
  }

  getConfiguration = () => {
    // Didn't find a better way to fix "ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked."
    return {};
  }

  ngAfterViewInit() {
    // Didn't find a better way to fix "ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked."
    this.getConfiguration = () => {
      let configurations = Object.assign({}, this.options);
      delete configurations['percent'];
      delete configurations['subtitleFormat'];
      for (let key of Object.keys(configurations)) {
        if (configurations[key] === this.circleProgress.defaultOptions[key]) {
          delete configurations[key];
        }
      }
      return configurations;
    };

    function updateBackground() {

      let hr = (new Date()).getHours();
      let slider = <HTMLElement>document.querySelector(".slider-animation");
      if (slider) {
        let bstyle = slider.style

        if (hr > 5 && hr < 18) {
          bstyle.backgroundColor = "white";
          slider.className += " dayanimation";

        } else {
          bstyle.backgroundColor = "gray";
          slider.className += " darkanimationslider";
        }
      }

    }
    setInterval(updateBackground, 1000 * 60);
    updateBackground();
  }





}
