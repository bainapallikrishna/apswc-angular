import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-screenreader',
  templateUrl: './screenreader.component.html',
  styleUrls: ['./screenreader.component.css']
})
export class ScreenreaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  onClickMe() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('http://www.nvda-project.org/', '_blank');
      return true;
    }
    else {
      return false;
    }
  }

  
  onClickMe2() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('http://www.satogo.com/', '_blank');
      return true;
    }
    else {
      return false;
    }
  }

  onClickMe3() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('http://www.yourdolphin.co.uk/productdetail.asp?id=5', '_blank');
      return true;
    }
    else {
      return false;
    }
  }
  onClickMe4() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('http://www.freedomscientific.com/jaws-hq.asp', '_blank');
      return true;
    }
    else {
      return false;
    }
  }
  onClickMe5() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('http://www.yourdolphin.co.uk/productdetail.asp?id=1', '_blank');
      return true;
    }
    else {
      return false;
    }
  }
  onClickMe6() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('http://www.gwmicro.com/Window-Eyes/', '_blank');
      return true;
    }
    else {
      return false;
    }
  }
}
