import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndRegistrationComponent } from './ind-registration.component';

describe('IndRegistrationComponent', () => {
  let component: IndRegistrationComponent;
  let fixture: ComponentFixture<IndRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
