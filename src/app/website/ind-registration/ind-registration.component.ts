import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import * as $ from "jquery";
import { InputRequest } from 'src/app/Interfaces/employee';
import { AadharValidateService } from 'src/app/Services/aadhar-validate.service';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ind-registration',
  templateUrl: './ind-registration.component.html',
  styleUrls: ['./ind-registration.component.css']
})
export class IndRegistrationComponent implements OnInit {

  RUserType: string = sessionStorage.getItem("UserRType");
  public showloader: boolean = true;
  public regsub: boolean = false;
  public docsub: boolean = false;
  public commusubmi:boolean=false;
  public Emp_mob_isvalid: boolean = true;
  RegForm: FormGroup;
  DocForm: FormGroup;
  ComForm: FormGroup;
  PanPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  states: any = [];
  districts: any = [];
  urlist: any = [];
  nmandals: any = [];
  nvillages: any = [];
  doculist: any = [];
  Userslist: any=[];
  currtab: string = "Reg";
  response: { dbPath: '' };
  userkeyword: string = "name";
  uidstatus: boolean = true;
  uid_isvalid: boolean = true;

  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder, private uidservice: AadharValidateService) {
    if (!this.RUserType) {
      this.router.navigate(['/Registration']);
    }
  }

  ngOnInit(): void {

    this.RegForm = this.formBuilder.group({
      REGCODE: [''],
      RegName: ['', Validators.required],
      FarAadhaar: ['', Validators.required],
      RegEmail: [''],
      RegMobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      PANNo: [''],
      NState: [null, Validators.required],
      NDistrict: [null, Validators.required],
      NArea: [null, Validators.required],
      NMandal: [null, Validators.required],
      NVillage: [null, Validators.required],
      NHNO: ['', Validators.required],
      NStrName: ['', Validators.required],
      NLandmark: [''],
      NPincode: ['', [Validators.required, Validators.minLength(6)]],
    });

    this.DocForm = this.formBuilder.group({
      Documents: new FormArray([
        this.formBuilder.group({
          DocID: ['', Validators.required],
          DocName: ['', Validators.required],
          DocType:['', Validators.required],
          DocPath:['', Validators.required],
          RegDoc:[''],
          progress: [0],
          message: [''],
        })
      ])
    });

    this.ComForm = this.formBuilder.group({
      Communication: new FormArray([
        this.formBuilder.group({
          CName: ['', Validators.required],
          CDesignation: ['', Validators.required],
          CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
          CEmail: [''],

        })
      ])
    });
    this.LoadDocuList();
    this.LoadStates();
    this.LoadDistricts(); // load districts
    this.LoadurFlags(); // load areas
    this.loaduserlist();
    this.showloader = false;
  }

  get reg() { return this.RegForm.controls; }
  get doc() { return this.DocForm.controls; }
  get com() { return this.ComForm.controls; }

  get d() { return this.doc.Documents as FormArray; }
  get c() { return this.com.Communication as FormArray; }

  mob_uid_validate(mobilephone) {
    if (mobilephone.length === 10)
      this.Emp_mob_isvalid = this.uidservice.validatemob(mobilephone);
      else if (mobilephone.length === 12)
      this.uid_isvalid = this.uidservice.validatemob(this.reg.FarAadhaar.value);
  }

  uidvalidate(FarAadhaar) {
    if (FarAadhaar.length == 12) {
      this.mob_uid_validate(FarAadhaar);
      this.uidstatus = this.uidservice.validate(this.reg.FarAadhaar.value);  
    }
  }

  loaduserlist():void  {
    this.showloader = true;
    const req = new InputRequest();
    this.service.postData(req, "GetExistingUser_Details").subscribe(data => {   
      this.showloader = false;   
      if (data.StatusCode == "100") {
        this.Userslist = data.Details;
      }
    },   
      error => console.log(error));
  }

  UserSelect(event):void{
    this.reg.RegName.setValue("");
  }

  AddCommu() {
    this.c.push(this.formBuilder.group({
      CName: ['', Validators.required],
      CDesignation: ['', Validators.required],
      CMobile: ['', [Validators.required, Validators.pattern('[6-9]\\d{9}')]],
      CEmail: [''],
     
    }));
  }

  RemoveCommu(index) {
    this.c.removeAt(index);
  }

  UserRegistration() {
    this.regsub = true;
    
    // stop here if form is invalid
    if (this.RegForm.invalid) {
      return false;
    }

    if(!this.Emp_mob_isvalid){
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.RUserType;
    req.INPUT_02 = this.reg.RegName.value;
    req.INPUT_03 = this.reg.RegEmail.value;
    req.INPUT_04 = this.reg.RegMobile.value;
    req.INPUT_05 = this.reg.PANNo.value;
    req.INPUT_06 = this.reg.NState.value;
    req.INPUT_07 = this.reg.NDistrict.value;
    req.INPUT_08 = this.reg.NMandal.value;
    req.INPUT_09 = this.reg.NVillage.value;
    req.INPUT_10 = this.reg.NArea.value;
    req.INPUT_11 = this.reg.NHNO.value;
    req.INPUT_12 = this.reg.NStrName.value;
    req.INPUT_13 = this.reg.NLandmark.value;
    req.INPUT_14 = this.reg.NPincode.value;
    //req.INPUT_15 = this.reg.FarType.value;
    req.INPUT_16 = this.reg.FarAadhaar.value;

    req.USER_NAME =  this.reg.RegMobile.value;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveRegistrationDetails").subscribe(data => {
      this.showloader = false;
      
      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          this.reg.REGCODE.setValue(result.registratioN_ID);
          Swal.fire('success', "Registration Details Saved Successfully !!!", 'success');
          this.currtab = "Doc";
          $("#doctab").addClass("active");
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }
 
      }
      else
      Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  SaveDocDetails() {
    this.docsub = true;
    for (let i = 0; i < this.d.length; i++) {
      if (!this.d.value[i].DocPath) {
        Swal.fire('warning', "Please Upload all Required Documents", 'warning');
        return false;
      }
    }
    if (this.DocForm.invalid) {
      return false;
    }

    if (!this.reg.REGCODE.value) {
      Swal.fire('warning', "Please submit registration first", 'warning');
      return false;
    }

    let idslist: string[] = [];
    let pathlist: string[] = [];
    let typelist: string[] = [];

    for (let i = 0; i < this.d.length; i++) {
      if (!this.d.value[i].DocPath) {
        Swal.fire('warning', "Please Upload all Required Documents", 'warning');
        return false;
      }
      idslist.push(this.d.value[i].DocID);
      pathlist.push(this.d.value[i].DocPath);
      typelist.push(this.d.value[i].DocType);
    }

    const req = new InputRequest();
    req.INPUT_01 = this.reg.REGCODE.value;
    req.INPUT_02 = idslist.join(',');
    req.INPUT_03 = pathlist.join(',');
    req.INPUT_04 = typelist.join(',');

    req.USER_NAME =  this.reg.REGCODE.value;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveRegDocDetails").subscribe(data => {
      this.showloader = false;
      
      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Document Upload Details Saved Successfully !!!", 'success');
          this.currtab = "Com";
          $("#commtab").addClass("active");
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }
 
      }
      else
      Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
    
  }

  SaveCommDetails(){
    this.commusubmi = true;
    if (this.ComForm.invalid) {
      return false;
    }

    if (!this.reg.REGCODE.value) {
      Swal.fire('warning', "Please submit registration first", 'warning');
      return false;
    }

    if(!this.Emp_mob_isvalid){
      return false;
    }

    let namelist: string[] = [];
    let desilist: string[] = [];
    let moblist: string[] = [];
    let mailist: string[] = [];

    for (let i = 0; i < this.c.length; i++) {
      namelist.push(this.c.value[i].CName);
      desilist.push(this.c.value[i].CDesignation);
      moblist.push(this.c.value[i].CMobile);
      mailist.push(this.c.value[i].CEmail);
    }

    const req = new InputRequest();
    req.INPUT_01 = this.reg.REGCODE.value;
    req.INPUT_02 = namelist.join(',');
    req.INPUT_03 = desilist.join(',');
    req.INPUT_04 = moblist.join(',');
    req.INPUT_05 = mailist.join(',');

    req.USER_NAME =  this.reg.REGCODE.value;
    req.CALL_SOURCE = "WEB";
    
    this.service.postData(req, "SaveRegCommuDetails").subscribe((data) => {
      this.showloader = false;
      
      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Communication Details Saved Successfully !!!", 'success');
          this.router.navigate(['/Registration'])
          
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }
 
      }
      else
      Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  LoadDocuList() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.RUserType }

    this.service.postData(obj, "GetReqDocByReg").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.doculist = data.Details;
        this.FillDocDetails(this.doculist);
      }

    },
      error => console.log(error));
  }

  FillDocDetails(docdata: any) {
    this.d.reset();
    this.d.clear();
    for (let i = 0; i < docdata.length; i++) {
      //if (i != 0) {
        this.d.push(this.formBuilder.group({
          DocID: ['', Validators.required],
          DocName: ['', Validators.required],
          DocType: ['', Validators.required],
          DocPath: ['', Validators.required],
          RegDoc: [''],
          progress: [0],
          message: [''],
        }));
      //}

      this.d.controls[i].patchValue({
        DocID: docdata[i].iteM_ID,
        DocName: docdata[i].iteM_NAME
      });

    }
  }

  uploadFile(event,index:number){
    if (event.target.files && event.target.files[0]) {
      let indx : number = index;
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      if (filetype != 'image/jpeg' || filetype != 'image/png')
        doctype = 'IMAGE';
      else
        doctype = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name );
      formData.append('regid', this.reg.REGCODE.value);
      formData.append('category', this.d.value[indx].DocName);
      formData.append('pagename', "Ind Registration");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
          this.d.controls[indx].patchValue({ progress: Math.round(100 * event.loaded / event.total) });
          else if (event.type === HttpEventType.Response) {
            this.d.controls[indx].patchValue({ message: 'Upload success.' });
            this.d.controls[indx].patchValue({ DocType: doctype });
            this.uploadFinished(event.body,indx);
          }

        });
    }
  }

  public createImgPath = (serverPath: string) => {
    return `${this.service.filebaseUrl.replace("api/FilesUpload","")}${serverPath}`;
  }

  public uploadFinished = (event,indx) => {
    //alert(event.dbPath);
    this.d.controls[indx].patchValue({ DocPath: event.fullPath });
    //this.response = event.dbPath;
    //alert(this.response.dbPath)
  }

  LoadStates() {
    this.service.getData("GetStates").subscribe(data => {

      if (data.StatusCode == "100") {
        this.states = data.Details;
      }

    },
      error => console.log(error));
  }

  LoadDistricts() {
    this.service.getData("GetDistricts").subscribe(data => {

      if (data.StatusCode == "100") {
        this.districts = data.Details;

      }

    },
      error => console.log(error));
  }

  LoadurFlags() {
    this.service.getData("GetAreaTypes").subscribe(data => {

      if (data.StatusCode == "100") {
        this.urlist = data.Details;

      }

    },
      error => console.log(error));
  }

  DistrictChange(type: string) {
    this.nmandals = [];
    this.nvillages = [];
    this.reg.NMandal.setValue(null);
    this.reg.NVillage.setValue(null);
    if (this.reg.NDistrict.value && this.reg.NArea.value) {
      this.LoadMandals(this.reg.NDistrict.value, this.reg.NArea.value)
    }
  }

  LoadMandals(distval: string, areaval: string) {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = areaval;
    req.INPUT_02 = distval;

    this.service.postData(req, "GetMandlas").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.nmandals = data.Details;
      }

    },
      error => console.log(error));
  }

  MandalChange(type: string) {
    this.nvillages = [];
    this.reg.NVillage.setValue(null);
    if (this.reg.NDistrict.value && this.reg.NMandal.value) {
      this.LoadVillages(this.reg.NDistrict.value, this.reg.NMandal.value)
    }
  }

  LoadVillages(distval: string, manval: string) {
    this.showloader = true;
    let obj: any = { "INPUT_02": distval, "INPUT_03": manval }

    this.service.postData(obj, "GetVillages").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.nvillages = data.Details;
      }

    },
      error => console.log(error));
  }

}
