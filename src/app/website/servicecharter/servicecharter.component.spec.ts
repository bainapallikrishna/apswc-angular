import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ServicecharterComponent } from './servicecharter.component';

describe('ServicecharterComponent', () => {
  let component: ServicecharterComponent;
  let fixture: ComponentFixture<ServicecharterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ServicecharterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ServicecharterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
