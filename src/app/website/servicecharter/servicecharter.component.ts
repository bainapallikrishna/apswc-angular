import { Component, OnInit } from '@angular/core';
import { InputRequest } from 'src/app/Interfaces/employee';
import { section } from 'src/app/Interfaces/user';
import { CommonServices } from 'src/app/Services/common.services';

@Component({
  selector: 'app-servicecharter',
  templateUrl: './servicecharter.component.html',
  styleUrls: ['./servicecharter.component.css']
})
export class ServicecharterComponent implements OnInit {
  sectioncharterlist:section[]=[];
 secs:section;
 servicecharterdetails:any[];
 modaltitle:string;
 modalimage:string;
 modalsection:string;
  constructor(private service:CommonServices) { }

  ngOnInit(): void {

    this.service.getData("GetSections").subscribe(data => {      
       if (data.StatusCode == "100") {              
        // console.log(data.Details);
         data.Details.forEach(item => {        
           
            this.secs=
            { section_CODE:item.sectioN_CODE,
              section:item.section,
              imagepath:item.sectioN_CODE=='CON'?"CON1.svg":item.sectioN_CODE+".svg"
            }
            //tagName:item.storagE_TYPE_CODE}
            
          this.sectioncharterlist.push(this.secs);
           //console.log(this.Storagelist);
          });
       }
 
     },
       error => console.log(error));
   
  }

  GetSectionDetails(sc:section)
  {
    const req=new InputRequest();
    this.modaltitle=sc.section
    this.modalimage=sc.imagepath;
    this.modalsection=sc.section_CODE;
    req.DIRECTION_ID="1";
    req.TYPEID="";
    req.INPUT_01=sc.section;
    
    
    this.service.postData(req,"GetServiceCharterDetails").subscribe(data => {
   
       if (data.StatusCode == "100") {     
         
        this.servicecharterdetails=data.Details;
 
       }
 
     },
       error => console.log(error)); 
  }

}
