import { Component, OnInit } from '@angular/core';
import { CommonServices } from 'src/app/Services/common.services';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

@Component({
  selector: 'app-godownlocator',
  templateUrl: './godownlocator.component.html',
  styleUrls: ['./godownlocator.component.css']
})
export class GodownlocatorComponent implements OnInit {

  constructor(private service:CommonServices,private sanitizer: DomSanitizer) {

   }

  ngOnInit(): void {
    
  }

  MapsURL() {
    return this.sanitizer.bypassSecurityTrustUrl(this.service.mapsUrl);
  }

}
