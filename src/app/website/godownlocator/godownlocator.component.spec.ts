import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownlocatorComponent } from './godownlocator.component';

describe('GodownlocatorComponent', () => {
  let component: GodownlocatorComponent;
  let fixture: ComponentFixture<GodownlocatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownlocatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownlocatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
