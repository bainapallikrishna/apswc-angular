import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InspectiongalleryComponent } from './inspectiongallery.component';

describe('InspectiongalleryComponent', () => {
  let component: InspectiongalleryComponent;
  let fixture: ComponentFixture<InspectiongalleryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InspectiongalleryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InspectiongalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
