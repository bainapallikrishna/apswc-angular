import { Component, EventEmitter, OnInit } from '@angular/core';

@Component({
  selector: 'app-photogallery',
  templateUrl: './photogallery.component.html',
  styleUrls: ['./photogallery.component.css']
})
export class PhotogalleryComponent implements OnInit {

  public expanded: boolean;
  public expandedChange: EventEmitter<boolean> = new EventEmitter();

  
// Timeline 1

  show = false;
  buttonName = 'Show';
  hide: any;

  toggle() {
  this.show = !this.show
  this.expanded = !this.expanded;
  this.expandedChange.next(this.expanded);

  if(this.show) {
  this.buttonName = 'Hide'
  console.log(this.show)
  }
  else {
  this.buttonName = 'Show'
  }
  }

// Timeline 2
  show2 = false;
  buttonName2 = 'Show';
  hide2: any;

  toggle2() {
  this.show2 = !this.show2

  if(this.show2) {
  this.buttonName2 = 'Hide2'
  console.log(this.show2)
  }
  else {
  this.buttonName2 = 'Show2'
  }
  }
// Timeline 3
  show3 = false;
  buttonName3 = 'Show';
  hide3: any;

  toggle3() {
  this.show3 = !this.show3

  if(this.show3) {
  this.buttonName3 = 'Hide3'
  console.log(this.show3)
  }
  else {
  this.buttonName3 = 'Show3'
  }
  }

 
  ngOnInit(): void {
  }

}
