import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CopyrightspolicyComponent } from './copyrightspolicy.component';

describe('CopyrightspolicyComponent', () => {
  let component: CopyrightspolicyComponent;
  let fixture: ComponentFixture<CopyrightspolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CopyrightspolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CopyrightspolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
