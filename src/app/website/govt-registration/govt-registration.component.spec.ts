import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GovtRegistrationComponent } from './govt-registration.component';

describe('GovtRegistrationComponent', () => {
  let component: GovtRegistrationComponent;
  let fixture: ComponentFixture<GovtRegistrationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GovtRegistrationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GovtRegistrationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
