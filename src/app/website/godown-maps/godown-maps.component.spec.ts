import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GodownMapsComponent } from './godown-maps.component';

describe('GodownMapsComponent', () => {
  let component: GodownMapsComponent;
  let fixture: ComponentFixture<GodownMapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GodownMapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GodownMapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
