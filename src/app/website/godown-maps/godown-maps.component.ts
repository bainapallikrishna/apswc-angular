import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-godown-maps',
  templateUrl: './godown-maps.component.html',
  styleUrls: ['./godown-maps.component.css']
})
export class GodownMapsComponent implements OnInit {

  zoom: number = 8;
  
  // initial center position for the map
  lat: number = 15.573731;
  lng: number = 79.9345364;
  
  constructor() { }

  ngOnInit(): void {
  }
 
}
// just an interface for type safety.
interface marker {
	lat: number;
	lng: number;
	label?: string;
	draggable: boolean;
}