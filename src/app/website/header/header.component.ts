import { Component} from '@angular/core';
import * as $ from "jquery";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent{
  
  constructor() { }

  ngOnInit(): void {


    
let $affectedElements = $("p, h1,h2, h3, h4,h5, a, li, img");
$affectedElements.each(function() {
    var $this = $(this);
    $this.data("orig-size", $this.css("font-size"));
});
$("#btn-increase").click(function() {
    changeFontSize(1);
})
$("#btn-decrease").click(function() {
    changeFontSize(-1);
})
$("#btn-orig").click(function() {
    $affectedElements.each(function() {
        var $this = $(this);
        $this.css("font-size", $this.data("orig-size"));
    });
})

function changeFontSize(direction) {
    $affectedElements.each(function() {
        var $this = $(this);
        $this.css("font-size", parseInt($this.css("font-size")) + direction);
    });
}
  
   
  }
  // fontSize = 14;
 
  // changeFont(operator) {
  // }
//   changeFont(operator) {
//     if(operator=='1')
//     {
//       this.fontSize = 14;
//     (this.para.nativeElement as HTMLDivElement).style.fontSize = `${this.fontSize}px`;
//     }
// else
// {
//     operator === '+' ? this.fontSize++ : this.fontSize--;
//     (this.para.nativeElement as HTMLParagraphElement).style.fontSize = `${this.fontSize}px`;
// }
    
//   }



}
