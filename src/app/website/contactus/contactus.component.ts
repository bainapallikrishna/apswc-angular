import { Component, OnInit } from '@angular/core';
import { InputRequest } from 'src/app/Interfaces/employee';
import { contactus } from 'src/app/Interfaces/user';
import { CommonServices } from 'src/app/Services/common.services';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
  RegionalOfffices: any[];
  InvestorGodowsList: contactus[] = [];
  cnt: contactus;

  constructor(private service: CommonServices) { }

  ngOnInit(): void {

    // const req=new InputRequest();
    // req.DIRECTION_ID="1";
    // req.TYPEID="";
    //  req.INPUT_01="Regional";
    // this.service.postData(req,"GetLocations").subscribe(data => {
    //    if (data.StatusCode == "100") {     

    //     this.RegionalOfffices=data.Details;

    //      console.log(data.Details);
    //    }

    //  },
    //    error => console.log(error));


    this.RegionalOfffices = [

      {
        regionalname: 'VIZIANAGARAM',
        Address: ' 8-30-9A, Plot No.24, Sai Nagar, Near Gayathri College, Thotapalem, Vizianagaram Dist 535001',
        imagepath: './assets/web-styles/images/contact/Ellipse 33.png'
      },
      {
        regionalname: 'KAKINADA',
        Address: '67-3-3/1,MSN Junior College Lane Kakinada , Ashok Nagar, E.G. Dist.533003',
        imagepath: './assets/web-styles/images/contact/Ellipse 37.png'
      },
      {
        regionalname: 'TADEPALLIGUDEM',
        Address: 'Plot No.7,D.No.1-56-8/4, FCI Colony, Road no.4, opp.3rd water Tank, T.P.Gudem, WG.Distt 534101',
        imagepath: './assets/web-styles/images/contact/Ellipse 36.png'

      },
      {
        regionalname: 'VIJAYAWADA',
        Address: '11-97/1, 5th floor, Ashok Nagar, Beside Time Hospital, Kanur, Vijayawada -520007',
        imagepath: './assets/web-styles/images/contact/Ellipse 34.png',
      },
      {
        regionalname: 'KADAPA',
        Address: '1/292 (Upstairs ) Haritha Hotel, Near TV 9 Office ,YSR KADAPA , DIST 516021.',
        imagepath: './assets/web-styles/images/contact/Ellipse 35.png'
      }
    ];

  }

  getcontactDetails(val: string) {
    this.InvestorGodowsList = [];
    const req = new InputRequest();
    req.DIRECTION_ID = "1";
    req.TYPEID = "";
    req.INPUT_01 = val;
    this.service.postData(req, "GetContactList").subscribe(data => {
      if (data.StatusCode == "100") {

        data.Details.forEach(item => {

          this.cnt = {
            nameofWareHouse: item.officE_LOCATION,
            nameofWareHouseManager: item.officeR_NAME,
            telePhoneNo: item.telephonE_NO,
            capacity: item.capacity,
            mobileNum: item.mobilE_NO,
            deptName: item.wH_TYPE == null ? "HeadOffice" : item.wH_TYPE,
            distName: item.region
          }
          this.InvestorGodowsList.push(this.cnt);

        });
        console.log(this.InvestorGodowsList);
      }

    },
      error => console.log(error));

  }


}

