import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  constructor(private router: Router) { 
    sessionStorage.removeItem("UserRType");
  }

  ngOnInit(): void {
  }

  RegisterUser(type:string){
    sessionStorage.setItem("UserRType",type);
    if(type == 'FER' || type == 'PVT' || type == 'PRO' )
    this.router.navigate(['/RegistrationUser']);
    else if(type == 'STA' || type == 'CEN' || type == 'PSU' )
    this.router.navigate(['/RegistrationGovt']);
    else if(type == 'FAR')
    this.router.navigate(['/RegistrationFarmer']);
    else if(type == 'IND')
    this.router.navigate(['/RegistrationInd']);
  }
}
