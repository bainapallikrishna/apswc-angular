import { Component, OnInit } from '@angular/core';
import { CommonServices } from 'src/app/Services/common.services';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  onClickMe3() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('https://www.ap.gov.in/', '_blank');
      return true;
    }
    else {
      return false;
    }
  }
  onClickMe4() {
    var retVal = confirm("You are being redirected to an external website. Please note that APSWC Website cannot be held responsible for external websites content & privacy policies.");
    if (retVal == true) {
      window.open('https://market.ap.nic.in/index.html', '_blank');
      return true;
    }
    else {
      return false;
    }
  }

  constructor(private service:CommonServices) { }
visitorscount:number;
lastchangedate:string ="";
  ngOnInit(): void {
    let now: Date = new Date();
    this.lastchangedate = now.getDate() + "." + ((now.getMonth() + 1) < 10 ? '0' + (now.getMonth() + 1) : (now.getMonth() + 1)) + "." + now.getFullYear();
     //home page content
     this.service.getData("GetVisitorsCount").subscribe(data => {
         if (data.StatusCode == "100") {
           console.log(data.Details[0]); 

           this.visitorscount=data.Details[0]["count"];

         }
        },
        error => console.log(error));
  }

}
