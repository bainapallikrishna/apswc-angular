import { Component, OnInit } from '@angular/core';
import { InputRequest, Storagecharges } from 'src/app/Interfaces/employee';

import { CommonServices } from 'src/app/Services/common.services';
@Component({
  selector: 'app-storagecharges',
  templateUrl: './storagecharges.component.html',
  styleUrls: ['./storagecharges.component.css']
})
export class StoragechargesComponent implements OnInit {

  GodownsList: any[];
  RiceList: any[];
  FoodGrainsList: any[];
  FertilisersList: any[];
  FoodProductsList: any[];
  PulsesList: any[];
  SpicesList: any[];
  FoodStuffsList: any[];
  OilSeedsList: any[];
  OilsList: any[];
  FibersList: any[];
  SeedsList: any[];
  FeedList: any[];
  CommoditiesList: any[];
  TeaCoffeList: any[];
  MiscellaneousList: any[];
  Storagelist: Storagecharges[] = [];
  st: Storagecharges;
  modalname: string;
  modaltitle: string;
  modalimage: string;
  //req:InputRequest;
  constructor(private service: CommonServices) { }

  ngOnInit(): void {

    this.service.getData("GetStorageTypes").subscribe(data => {
      if (data.StatusCode == "100") {


        data.Details.forEach(item => {


          this.st = {
            name: item.storagE_TYPE.toString(),
            imagePath: item.storagE_TYPE_CODE.replace(' ', '_') + ".svg",
            tagName: item.storagE_TYPE_CODE
          }

          this.Storagelist.push(this.st);
        });
      }

    },
      error => console.log(error));


    this.GodownsList = [
      {
        "Sno": "1",
        "wtype": "Own",
        "Nos": "65",
        "Capacity": "7.92",
        "Occupancy": "4.76"
      },
      {
        "Sno": "2",
        "wtype": "Hired",
        "Nos": "98",
        "Capacity": "7.79",
        "Occupancy": "8.16"
      },

      {
        "Sno": "3",
        "wtype": "Other Government Facilities",
        "Nos": "1",
        "Capacity": "0.22",
        "Occupancy": "0.22"
      }
      // {
      //   "Sno": "",
      //   "wtype": "Total",
      //   "Nos": "164",
      //   "Capacity": "15.93",
      //   "Occupancy": "13.15"
      // }
    ];

    this.RiceList = [
      {
        "name": "APSCSC / Rice",
        "type": "Bags",
        "netweight": "Up to 50 Kgs.",
        "sRate": "5.00",
        "hRate1": "5.00",
        "hRate2": "5.00",
        "remarks": "Inclusive Insurance"
      },
      {
        "name": "FCI / Rice",
        "type": "Bags",
        "netweight": "Up to 50 Kgs.",
        "sRate": "5.39",
        "hRate1": "5.39",
        "hRate2": "5.39",
        "remarks": "Inclusive Insurance"
      }
    ]

    this.FoodGrainsList = [
      {
        "name": "Food grains other than Paddy and Rice",
        "type": "Bags",
        "netweight": "50 Kgs",
        "sRate": "4.50",
        "hRate1": "5.55",
        "hRate2": "4.95",
        "remarks": "Including Insurance"
      },
      {
        "name": "Food grains other than Paddy and Rice",
        "type": "Bags",
        "netweight": "51-75 Kgs",
        "sRate": "6.05",
        "hRate1": "7.45",
        "hRate2": "6.65",
        "remarks": "Including Insurance"
      },
      {
        "name": "Food grains other than Paddy and Rice",
        "type": "Bags",
        "netweight": "76 - 100 Kgs",
        "sRate": "7.35",
        "hRate1": "8.05",
        "hRate2": "9.00",
        "remarks": "Including Insurance"
      },
      {
        "name": "Rice",
        "type": "Bags",
        "netweight": "50 Kgs",
        "sRate": "5.80",
        "hRate1": "7.15",
        "hRate2": "6.35",
        "remarks": "Including Insurance"
      },
      {
        "name": "Rice",
        "type": "Bags",
        "netweight": "50Kgs.",
        "sRate": "5.80",
        "hRate1": "7.15",
        "hRate2": "6.35",
        "remarks": "Including Insurance"
      },
      {
        "name": "Rice",
        "type": "Bags",
        "netweight": "76 - 100 Kgs",
        "sRate": "8.05",
        "hRate1": "9.95",
        "hRate2": "8.85",
        "remarks": "Including Insurance"
      },
      {
        "name": "Paddy",
        "type": "Bags",
        "netweight": "Up to 35 Kgs",
        "sRate": "4.65",
        "hRate1": "5.70",
        "hRate2": "5.10",
        "remarks": "Including Insurance"
      },
      {
        "name": "Paddy",
        "type": "Bags",
        "netweight": "36-50 Kgs",
        "sRate": "6.05",
        "hRate1": "7.45",
        "hRate2": "6.65",
        "remarks": "Including Insurance"
      },
      {
        "name": "Paddy",
        "type": "Bags",
        "netweight": "51-75 Kgs",
        "sRate": "8.05",
        "hRate1": "9.95",
        "hRate2": "8.85",
        "remarks": "Including Insurance"
      },
      {
        "name": "Paddy",
        "type": "Bags",
        "netweight": "76 - 85 Kgs",
        "sRate": "8.75",
        "hRate1": "10.75",
        "hRate2": "9.60",
        "remarks": "Including Insurance"
      },
      {
        "name": "Maize / Jowar / Bajra / Ragi",
        "type": "Bags",
        "netweight": "50 Kgs",
        "sRate": "5",
        "hRate1": "5.35",
        "hRate2": "5.55",
        "remarks": "Including Insurance"
      },
      {
        "name": "Maize / Jowar / Bajra / Ragi",
        "type": "Bags",
        "netweight": "51-75 Kgs",
        "sRate": "6.9",
        "hRate1": "7.35",
        "hRate2": "7.65",
        "remarks": "Including Insurance"
      },
      {
        "name": "Maize / Jowar / Bajra / Ragi",
        "type": "Bags",
        "netweight": "76 - 100 Kgs",
        "sRate": "8.2",
        "hRate1": "9.1",
        "hRate2": "8.75",
        "remarks": "Including Insurance"
      }
    ]

    this.FertilisersList = [
      {
        "name": "( All fertilisers rate per MT per month )",
        "type": "Bags",
        "netweight": "Up to 100 kgs",
        "sRate": "5.00",
        "hRate1": "5.00",
        "hRate2": "5.00",
        "remarks": "Inclusive Insurance"
      },
      {
        "name": "Bone meal",
        "type": "Bags",
        "netweight": "Up to 50 Kgs.",
        "sRate": "3.00",
        "hRate1": "3.60",
        "hRate2": "3.30",
        "remarks": "Excluding Insurance"
      }
      ,
      {
        "name": "Bone meal",
        "type": "Bags",
        "netweight": "51-100 Kgs",
        "sRate": "5.00",
        "hRate1": "6.00",
        "hRate2": "5.50",
        "remarks": "Excluding Insurance"
      }
    ]

    this.FoodProductsList = [
      {
        "name": "Wheat flour",
        "type": "Bags",
        "netweight": "90 Kgs",
        "sRate": "6.80",
        "hRate1": "8.35",
        "hRate2": "7.45",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Maida / Sooji / Pea flour/ Besan",
        "type": "Bags",
        "netweight": "100 Kgs",
        "sRate": "6.80",
        "hRate1": "8.35",
        "hRate2": "7.45",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Sago",
        "type": "Bags",
        "netweight": "Up to 50 Kgs",
        "sRate": "3.40",
        "hRate1": "4.20",
        "hRate2": "3.75",
        "remarks": "Excluding Insurance"
      }
      ,
      {
        "name": "Sago",
        "type": "Bags",
        "netweight": "51 - 100 Kgs",
        "sRate": "6.00",
        "hRate1": "7.40",
        "hRate2": "6.60",
        "remarks": "Excluding Insurance"
      }
    ]

    this.PulsesList = [
      {
        "name": "( a )All Pulses (broken/ whole)",
        "type": "Bags",
        "netweight": "25 Kgs",
        "sRate": "3.10",
        "hRate1": "3.40",
        "hRate2": "3.3",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "( a )All Pulses (broken/ whole)",
        "type": "Bags",
        "netweight": "50 Kgs",
        "sRate": "4.45",
        "hRate1": "4.95",
        "hRate2": "4.75",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "( a )All Pulses (broken/ whole)",
        "type": "Bags",
        "netweight": "100 Kgs",
        "sRate": "7.30",
        "hRate1": "8.10",
        "hRate2": "7.80",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "( b ) All Pulses",
        "type": "Bags",
        "netweight": "Bengal gram 60 Kgs",
        "sRate": "5.45",
        "hRate1": "6.00",
        "hRate2": "5.8",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Black Gram( Broken )",
        "type": "Bags",
        "netweight": "40 Kgs",
        "sRate": "4.15",
        "hRate1": "4.60",
        "hRate2": "4.45",
        "remarks": "Excluding Insurance"
      }
    ]

    this.SpicesList = [
      {
        "name": "Aniseed, Arecanut, Collary, Dry Ginger, Lavage, Turmeric, Sopanu,Rajigira (Ramdhana )",
        "type": "Bags",
        "netweight": "75 Kgs",
        "sRate": "6.00",
        "hRate1": "7.40",
        "hRate2": "6.60",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "a) Coriander (Dhani)3",
        "type": "Bags",
        "netweight": "40 Kgs",
        "sRate": "5.55",
        "hRate1": "6.85",
        "hRate2": "6.10",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Black Pepper",
        "type": "Bags",
        "netweight": "75 kgs",
        "sRate": "6.45",
        "hRate1": "7.90",
        "hRate2": "7.05",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Chillies",
        "type": "Bags",
        "netweight": "31 Kgs",
        "sRate": "10.70",
        "hRate1": "13.15",
        "hRate2": "11.70",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Cardamom(Big)",
        "type": "Bags",
        "netweight": "60 Kgs",
        "sRate": "14.00",
        "hRate1": "17.20",
        "hRate2": "15.30",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Tamrind( seedless )",
        "type": "	Mats / Bags",
        "netweight": "10 Kgs",
        "sRate": "1.50",
        "hRate1": "1.85",
        "hRate2": "1.65",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Tamrind (with seed )",
        "type": "Bags 2",
        "netweight": "50 Kgs",
        "sRate": "4.10",
        "hRate1": "5.05",
        "hRate2": "4.50",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Tepico Crips",
        "type": "Bags",
        "netweight": "	65 Kgs",
        "sRate": "14.00",
        "hRate1": "17.20",
        "hRate2": "15.30",
        "remarks": "Excluding Insurance"
      }
    ]

    this.FoodStuffsList = [
      {
        "name": "Bulgar Wheat",
        "type": "Bags",
        "netweight": "25 Kgs",
        "sRate": "2.80",
        "hRate1": "3.45",
        "hRate2": "3.05",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Butter Oil",
        "type": "Carton",
        "netweight": "16 Kgs",
        "sRate": "2.30",
        "hRate1": "2.80",
        "hRate2": "2.50",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "CBM( Balended )",
        "type": "Bags",
        "netweight": "25 kgs",
        "sRate": "2.00",
        "hRate1": "2.50",
        "hRate2": "2.20",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Milk Powder",
        "type": "Bags",
        "netweight": "25 Kgs",
        "sRate": "2.80",
        "hRate1": "3.45",
        "hRate2": "3.05",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Salad Oil",
        "type": "Drum Tins 2",
        "netweight": "220 Kgs",
        "sRate": "29.65",
        "hRate1": "36.50",
        "hRate2": "32.50",
        "remarks": "Excluding Insurance"
      }
    ]

    this.OilSeedsList = [
      {
        "name": "Ambadi, Castor, Karanji, Linseed, Mustard &Taraira, Popy, Sunflower, Sesanum(Gingelly), Sunflower, Sunhemps, and Toria, Soyabean, Ground Nut, Kernals",
        "type": "Bags",
        "netweight": "100 Kgs",
        "sRate": "6.00",
        "hRate1": "7.40",
        "hRate2": "6.60",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Cotton Seed",
        "type": "Bags 3",
        "netweight": "25 Kgs",
        "sRate": "3.60",
        "hRate1": "4.40",
        "hRate2": "3.90",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Ground Nuts ( Pods )",
        "type": "Bags 2",
        "netweight": "40 kgs",
        "sRate": "4.95",
        "hRate1": "6.10",
        "hRate2": "5.45",
        "remarks": "Excluding Insurance"
      }
    ]

    this.OilsList = [
      {
        "name": "Coconut, Groundnut, Mustard, Hydrogenated Tins, Vegetable Oil",
        "type": "Tins",
        "netweight": "16.5 Kgs",
        "sRate": "1.85",
        "hRate1": "2.25",
        "hRate2": "2.00",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Sugar / Khandasari	",
        "type": "Bags 3",
        "netweight": "50 Kgs",
        "sRate": "3.60",
        "hRate1": "4.40",
        "hRate2": "3.90",
        "remarks": "Excluding Insurance"
      },
      {
        "name": "Jaggery",
        "type": "Lumps 5",
        "netweight": "15 kgs",
        "sRate": "1.25",
        "hRate1": "1.50",
        "hRate2": "1.35",
        "remarks": "Excluding Insurance"
      }
    ]

  }

  GetGowdowns(stval: Storagecharges) {
    this.modalname = stval.tagName;
    const req = new InputRequest();
    this.modaltitle = stval.name;
    this.modalimage = stval.imagePath;
    req.DIRECTION_ID = "1";
    req.TYPEID = "";
    req.INPUT_01 = stval.name;
    console.log(stval.tagName);

    this.service.postData(req, "GetChargeDetails").subscribe(data => {
      if (data.StatusCode == "100") {

        //this.GodownsList=data.Details;

        console.log(data.Details);
      }

    },
      error => console.log(error));

  }

}
