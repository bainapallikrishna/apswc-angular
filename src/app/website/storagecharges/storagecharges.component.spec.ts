import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StoragechargesComponent } from './storagecharges.component';

describe('StoragechargesComponent', () => {
  let component: StoragechargesComponent;
  let fixture: ComponentFixture<StoragechargesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StoragechargesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StoragechargesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
