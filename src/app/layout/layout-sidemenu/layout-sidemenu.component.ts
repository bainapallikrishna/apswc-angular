import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/Interfaces/login';
import { AuthService } from 'src/app/Services/auth.service';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-layout-sidemenu',
  templateUrl: './layout-sidemenu.component.html',
  styleUrls: ['./layout-sidemenu.component.css']
})
export class LayoutSidemenuComponent implements OnInit {
  userDataSubscription: any;
  userData = new User();

  isActive1: boolean = false;
  isActive2: boolean = false;
  isActive3: boolean = false;
  isActive4: boolean = false;
  isActive5: boolean = false;
  isActive6: boolean = false;
  isActive7: boolean = false;
  menulist: any;
  mainmenulist:any=[];
  indlist:any=[];
  isMenuActive:string="";


  logUserrole: string = sessionStorage.getItem("logUserrole");
  logUserCode: string = sessionStorage.getItem("logUserCode");
  constructor(private authService: AuthService, private service: CommonServices) {

    this.userDataSubscription = this.authService.userData.asObservable().subscribe(data => {
      this.userData = data;
    });
  }

  ngOnInit(): void {
    this.loadmenudetails();
  }

  activeclick(value: string) {
    if (this.isMenuActive === value)
      this.isMenuActive = "";
    else
      this.isMenuActive = value;
  }

  ApplyCss(mainIndex) {
    return {
      'active': ((mainIndex === this.isMenuActive) ? true : false) 
    };
  }

  active1click(value: string) {
    if (value == "1") {
      this.isActive2 = false;
      this.isActive5 = false;
      this.isActive6 = false;
      this.isActive7 = false;
      this.isActive1 = !this.isActive1;
    }
    if (value == "2") {
      this.isActive1 = false;
      this.isActive3 = false;
      this.isActive4 = false;
      this.isActive5 = false;
      this.isActive6 = false;
      this.isActive7 = false;
      this.isActive2 = !this.isActive2;
    }
    if (value == "3") {
      this.isActive1 = false;
      this.isActive2 = false;
      this.isActive4 = false;
      this.isActive5 = false;
      this.isActive6 = false;
      this.isActive7 = false;
      this.isActive3 = !this.isActive3;
    }
    if (value == "4") {
      this.isActive1 = false;
      this.isActive2 = false;
      this.isActive3 = false;
      this.isActive5 = false;
      this.isActive6 = false;
      this.isActive7 = false;
      this.isActive4 = !this.isActive4;
    }
    if (value == "5") {
      this.isActive1 = false;
      this.isActive2 = false;
      this.isActive3 = false;
      this.isActive4 = false;
      this.isActive6 = false;
      this.isActive7 = false;
      this.isActive5 = !this.isActive5;
    }
    if (value == "6") {
      this.isActive1 = false;
      this.isActive2 = false;
      this.isActive3 = false;
      this.isActive4 = false;
      this.isActive5 = false;
      this.isActive7 = false;
      this.isActive6 = !this.isActive6;
    }
    if (value == "7") {
      this.isActive1 = false;
      this.isActive2 = false;
      this.isActive3 = false;
      this.isActive4 = false;
      this.isActive5 = false;
      this.isActive6 = false;
      this.isActive7 = !this.isActive6;
    }
  }

  UserLogout(): void {
    this.authService.logout();
  }
  loadmenudetails() {
    const req = new InputRequest();
    this.logUserrole == '101' ? req.INPUT_01 = this.logUserrole  : req.INPUT_01 = null;
    this.logUserrole != '101' ? req.INPUT_02 = this.logUserCode  : req.INPUT_02 = null;

    this.service.postData(req, "GetuserAccess_menu").subscribe(data => {
      if (data.StatusCode == "100") {
        this.menulist = data.Details;

        this.mainmenulist = data.Details.filter(x=> !x.routE_LINK).sort();
        this.indlist = data.Details.filter(x=> !x.menU_ITEM_ID && x.routE_LINK).sort()

      } 
    },
      error => console.log(error));



  }


}
