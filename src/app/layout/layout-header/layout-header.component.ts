import { Component, OnInit, ViewChild } from '@angular/core';
import { User } from 'src/app/Interfaces/login';
import { AuthService } from 'src/app/Services/auth.service';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Editor, Toolbar,toHTML } from "ngx-editor";
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout-header',
  templateUrl: './layout-header.component.html',
  styleUrls: ['./layout-header.component.css']
})


export class LayoutHeaderComponent implements OnInit {

  editor: Editor;
  toolbar: Toolbar = [
    ["bold", "italic"],
    ["underline", "strike"],
    ["code", "blockquote"],
    ["ordered_list", "bullet_list"],
    [{ heading: ["h1", "h2", "h3", "h4", "h5", "h6"] }],
    ["link", "image"],
    ["text_color", "background_color"],
    ["align_left", "align_center", "align_right", "align_justify"]
  ];


  @ViewChild('helpModal') public helpModal: ModalDirective;
  userDataSubscription: any;
  userData = new User();
  Description: any ;
  PageList : any [];
  currentURL : string ;
  currentURLARR : any [];
  PageID : string;
  ISEdit :boolean=false;
  submitted:boolean=false;
  showloader:boolean=false;
  divhelpabout:string;

  inout: string = "Mark-In";
  lattitude: string;
  longitute: string;
  showloaderheader: boolean = false;

  loginusercode: string = sessionStorage.getItem("logUserCode");
  Username: string = sessionStorage.getItem("logUserName");
  constructor(
    private authService: AuthService,
    private router:Router,
    private service:CommonServices,
    private fb:FormBuilder) { 
    this.userDataSubscription = this.authService.userData.asObservable().subscribe(data => {
      this.userData = data;
      //this.Username = this.userData.userName;
    });


    var currtime = new Date().getTime();
    var logintime = parseInt(sessionStorage.getItem("logintime"));

    if (currtime > logintime) {
      this.authService.logout();
    }
    
  }


  ngOnInit(): void {

    this.LoadIn_Out();
    this.Notifications_Get();
    this.editor = new Editor();
    this.currentURL=window.location.href;
    this.currentURLARR=this.currentURL.split('/').splice(-1);
    this.currentURL=this.currentURLARR[0];
    this.GetPageID();
    if(this.Username=="ADMIN"){
      this.ISEdit=true;
    }
    else{
      this.ISEdit=false;
    }

    var selectedTheme;
    $(document).ready(function(){
			$('#theme-select a').on('click', function(){
			 var path = $(this).data('path');
			 $('#color-switcher').attr('href', path);
       localStorage.setItem('href', path);
			});
      selectedTheme = localStorage.getItem('href');
      $('#color-switcher').attr('href', selectedTheme);
		});

  }

  get subf() { return this.HelpAddForm.controls; }
  
  HelpAddForm=this.fb.group({
    Description: ['', Validators.required]
  });

  GetPageID(){
    const req = new InputRequest();

    req.INPUT_01 = "PAGE_ID";
     req.INPUT_02="/"+this.currentURL;
    this.PageList=[];
    this.service.postData(req, "GetHelpDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.PageList = data.Details;
        this.PageID=this.PageList[0]["pagE_ID"];
        console.log(this.PageList);
        this.LoadPageHelp();
      }
      // else{
      //   Swal.fire('warning', data.StatusMessage, 'warning');
      // }
    },
      error => console.log(error));

  }

  LoadPageHelp() {
  
    const req = new InputRequest();

    req.INPUT_01 = "PAGE";
     req.INPUT_02=this.PageID;
     
    this.PageList=[];
    this.service.postData(req, "GetHelpDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.PageList = data.Details;
        this.subf.Description.setValue(this.PageList[0]["description"]);
        this.divhelpabout=this.PageList[0]["description"];
        console.log(this.PageList);
      }
      // else{
      //   Swal.fire('warning', data.StatusMessage, 'warning');
      // }
    },
      error => console.log(error));

  }

  SubmitAddDetails() {
    this.submitted=true;
    // if(this.HelpAddForm.invalid){
    //   return false;
    // }

    if(!this.subf.Description || this.subf.Description.value=="<p></p>"){
      Swal.fire("error", "Please enter description", "error");
      return;
    }
    const req = new InputRequest();
  
  req.INPUT_01 = "HELP_SUBMISSION";
  req.INPUT_02=this.PageID;
  req.INPUT_03=this.subf.Description.value;
  req.CALL_SOURCE="Web";
  req.USER_NAME=this.Username;

  this.service.postData(req, "HelpInsertion").subscribe(data => {
      if (data.StatusCode == 100) {
      this.showloader= false;  
        Swal.fire("success", "Data Submitted Successfully", "success");
        this.helpModal.hide();
    }
    else{
      this.showloader= false;  
      Swal.fire("info",data.StatusMessage,"info");
    }
  });
  }

  UserLogout(): void {
    this.authService.logout();
  }

  CheckLocation() {
    if (navigator.geolocation) {
      this.service.CheckLocation().then(pos => {
        console.log(pos);
        if (pos.state == 'prompt') {
          this.Locataion();
          Swal.fire({
            title: 'APSWC need location acces permission for Capturing Attendance...Please click Allow to access!',
            showDenyButton: false,
            showCancelButton: false,
            confirmButtonText: `OK`,

          }).then((result) => {
            if (result.isConfirmed) {
              //Swal.fire('Saved!', '', 'success')
            } else if (result.isDenied) {
              //Swal.fire('Changes are not saved', '', 'info')
            }
          })

          return false;
        }
        if (pos.state == 'denied') {

          this.Locataion();

          Swal.fire({
            title: 'This site has been blocked from accessing your location...!',
            showDenyButton: false,
            showCancelButton: false,
            confirmButtonText: `OK`,

          }).then((result) => {
            if (result.isConfirmed) {
              //Swal.fire('Saved!', '', 'success')
            } else if (result.isDenied) {
              //Swal.fire('Changes are not saved', '', 'info')
            }
          })

          return false;
        }
        if (pos.state == 'granted') {
          //Swal.fire('warning', "granted", 'warning');
          this.Locataion();
          return true;
        }
      });
    }
    else {
      Swal.fire('Geolocation is not supported for this Browser/OS.');
    }

  }

  Notifications: any[]

  Notifications_Get() {

    const req = new InputRequest();
    req.INPUT_01 = this.loginusercode;
    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.loginusercode;

    this.service.postData(req, "Notifications_Get").subscribe(data => {
      if (data.StatusCode == "100") {
        if (data.Details.length > 0) {
          console.log(data.Details);
          this.Notifications = data.Details;
          if (data.Details[0]["noT_READ_COUNT"] > 0) {
            this.notificatioN_Count = data.Details[0]["noT_READ_COUNT"]; //reaD_STATUS
            this.notificatioN_Counttext = data.Details[0]["noT_READ_COUNT"] + " New";
          }
          else {
            this.notificatioN_Counttext = "";
            this.notificatioN_Count = "";
          }


        }
        else {
          this.notificatioN_Counttext = "";
          this.notificatioN_Count = "";
        }

      }
      else {
      }
    },
      error => console.log(error));
  }
  notificatioN_Count: string;
  notificatioN_Counttext: string;
  Notifications_Update() {

    if (this.notificatioN_Count >= "1") {
      const req = new InputRequest();
      req.INPUT_01 = this.loginusercode;
      req.CALL_SOURCE = "WEB";
      req.USER_NAME = this.loginusercode;

      this.service.postData(req, "Notifications_Update").subscribe(data => {
        if (data.StatusCode == "100") {
          console.log("notification success");

          this.notificatioN_Counttext = "";
          this.notificatioN_Count = "";

          //this.Notifications_Get();
        }
        else {
          console.log("notification error" + data.StatusCode);

        }

      },
        error => console.log(error));
    }
  }



  LoadIn_Out() {

    const req = new InputRequest();
    req.INPUT_01 = this.loginusercode;
    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.loginusercode;

    this.service.postData(req, "Emp_In_Out_Get").subscribe(data => {
      if (data.StatusCode == "100") {
        console.log(data.Details);
        this.inout = data.Details[0].evenT_NAME == "IN" ? "Mark-In" : "Mark-Out";
      }
      else {
      }
    },
      error => console.log(error));
  }

  Emp_checkin_out() {

    this.CheckLocation();

    const req = new InputRequest();
    req.INPUT_01 = this.loginusercode;
    req.INPUT_04 = this.inout == "Mark-In" ? "IN" : "OUT";
    if (this.inout == "Mark-In") {
      req.INPUT_02 = this.lattitude;
      req.INPUT_03 = this.longitute;
    }
    else {
      req.INPUT_05 = this.lattitude;
      req.INPUT_06 = this.longitute;
    }
    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.loginusercode;

    this.showloaderheader = true;

    this.service.postData(req, "Emp_CheckIn_Out").subscribe(data => {
      this.showloaderheader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          if (this.inout == "Mark-In") {
            this.inout = "Mark-Out";
          }
          else { this.inout = "Mark-In"; }
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }


  Locataion() {

    this.service.getPosition().then(pos => {
      this.longitute = pos.lat;
      this.lattitude = pos.lng;
    });
  }

  viewHelp() {
    this.helpModal.show();
    this.LoadPageHelp();
  }

  closeHelp() {
    this.helpModal.hide();
  }
}
