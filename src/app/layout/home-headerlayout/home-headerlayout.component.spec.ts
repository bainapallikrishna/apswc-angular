import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeHeaderlayoutComponent } from './home-headerlayout.component';

describe('HomeHeaderlayoutComponent', () => {
  let component: HomeHeaderlayoutComponent;
  let fixture: ComponentFixture<HomeHeaderlayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeHeaderlayoutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeHeaderlayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
