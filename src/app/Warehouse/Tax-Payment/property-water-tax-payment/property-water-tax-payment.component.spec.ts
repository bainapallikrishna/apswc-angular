import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PropertyWaterTaxPaymentComponent } from './property-water-tax-payment.component';

describe('PropertyWaterTaxPaymentComponent', () => {
  let component: PropertyWaterTaxPaymentComponent;
  let fixture: ComponentFixture<PropertyWaterTaxPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PropertyWaterTaxPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PropertyWaterTaxPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
