import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControlName } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { HttpEventType } from '@angular/common/http';
import { InputRequest } from 'src/app/Interfaces/employee';
import { AgGridAngular } from 'ag-grid-angular';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-property-water-tax-payment',
  templateUrl: './property-water-tax-payment.component.html',
  styleUrls: ['./property-water-tax-payment.component.css']
})
export class PropertyWaterTaxPaymentComponent implements OnInit {
  frameworkComponents: any;
  NgbDateStruct: any;
  Taxtype: any;
  progressbar: boolean = false;
  message: string = "";
  UploadDocType: string = "";
  UploadDocPath: string = "";
  progress: number = 0;
  submitted: boolean = false;
  showloader: boolean = false;
  gridColumnApi: any = [];
  tokens: any = [];
  gridApi: any = [];
  public rowseledted: object;
  imprestlist: any = [];
  TaxPaymentForm: FormGroup;
  preview: string = "";
  seldocpath: any;
  public components;
  seldoctype: string = "";
  seldoccat: string = "";

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  @ViewChild('TaxPaymentModal') TaxPaymentModal: ModalDirective;
  @ViewChild('previewModal') public previewModal: ModalDirective;
  columnDefs = [
    { headerName: '#', maxWidth: 80, cellRenderer: 'rowIdRenderer' },
    { headerName: 'Payment Type', maxWidth: 250, field: 'taX_NAME', sortable: true, filter: true },
    { headerName: 'Payment Date', maxWidth: 250, field: 'paymenT_DATE', sortable: true, filter: true },
    { headerName: 'Amount', maxWidth: 250, field: 'taX_AMOUNT', sortable: true, filter: true },
    { headerName: 'Created by', maxWidth: 250, field: 'inserteD_BY', sortable: true, filter: true },
    { headerName: 'Remarks', width: 350, field: 'remarks', sortable: true, filter: true }

  ];
  constructor(private formBuilder: FormBuilder,
    private service: CommonServices,
    private router: Router,
    private datef: CustomDateParserFormatter,
    private sanitizer: DomSanitizer) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {

      this.router.navigate(['/Login']);
      return;
    }


    this.components = {

      rowIdRenderer: function (params) {

        return '' + (params.rowIndex + 1);
      }
    }

  }

  ngOnInit(): void {

    let now: Date = new Date();
    this.NgbDateStruct = { year: now.getFullYear(), month: now.getMonth() + 1, day: now.getDate() }

    this.TaxPaymentForm = this.formBuilder.group({

      Taxtype: ['', Validators.required],
      EndDate: ['', Validators.required],
      TaxAmount: ['', Validators.required],
      DocPath: [''],
      taxRemarks: [''],
      RegDoc: ['']
    });

    this.LoadImprestTypes();
  }

  LoadImprestTypes() {
    this.showloader = true;
    this.service.getData("GetImprestTypes").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.imprestlist = data.Details;
      }
    },
      error => console.log(error));
  }

  uploadFile(event) {
    this.seldocpath = "";
    this.seldoctype = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        this.progressbar = false;
        return false;
      }

      this.seldoccat = filetype;
      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype = 'IMAGE';
      else
        this.seldoctype = 'PDF';



      const reader = new FileReader();
      reader.onload = () => {
        if (this.seldoctype == "PDF") {
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else {
          this.seldocpath = reader.result as string;
          this.preview = this.seldocpath;
        }

      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', "PropertyTax");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.'
            this.uploadFinished(event.body);
            this.progressbar = true;
          }

        });
    }
  }

  public uploadFinished = (event) => {
    this.TaxPaymentForm.controls.DocPath.setValue(event.fullPath);
  }
  get reg() { return this.TaxPaymentForm.controls }


  Taxchange() {

    this.TaxPaymentForm.controls.DocPath.setValue('');
    this.TaxPaymentForm.controls.EndDate.setValue('');
    this.TaxPaymentForm.controls.TaxAmount.setValue('');
    this.TaxPaymentForm.controls.RegDoc.setValue('');
    this.TaxPaymentForm.controls.taxRemarks.setValue('');
    this.progressbar = false;

  }

  Sumbitfun() {

    this.showloader = true;

    const req = new InputRequest();

    this.submitted = true;

    if (this.TaxPaymentForm.invalid) {
      this.showloader = false;
      return;

    }

    if (!this.reg.DocPath.value) {
      this.showloader = false;
      Swal.fire('info', "Please Upload Document", 'info');
      return false;
    }
    req.INPUT_01 = this.workLocationCode;//rowsleted.tokeN_ID this.dep.TokenNo.value;
    req.INPUT_02 = this.TaxPaymentForm.controls.Taxtype.value;
    req.INPUT_03 = this.TaxPaymentForm.controls.TaxAmount.value;
    req.INPUT_04 = this.datef.format(this.TaxPaymentForm.controls.EndDate.value); //Receipt Out
    req.INPUT_05 = this.TaxPaymentForm.controls.taxRemarks.value;
    req.INPUT_06 = this.TaxPaymentForm.controls.DocPath.value;
    req.INPUT_07 = this.logUserrole;
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveTaxDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.LoadTaxData();
        this.reloadCurrentRoute();
        // this.TaxPaymentForm.reset();
        // this.TaxPaymentModal.hide();
        Swal.fire('success', "Tax Details Submitted Successfully", 'success');

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }


  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.LoadTaxData();
  }
  LoadTaxData() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetTaxData").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));
  }

  Taxclick() {
    this.submitted = false;
    this.TaxPaymentForm.reset();
    this.TaxPaymentForm.controls.Taxtype.setValue('');
    this.TaxPaymentModal.show();
  }
  hideTaxModal() {
    this.submitted = false;
    this.TaxPaymentForm.reset();
    this.TaxPaymentModal.hide();
  }

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  Docpriview(val) {

    // const blob = this.service.s_sd((val).split(',')[1], this.seldoccat);
    // const blobUrl = URL.createObjectURL(blob);
    // this.preview = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
    //this.preview = this.sanitizer.bypassSecurityTrustUrl(val);
    //this.preview = this.preview.changingThisBreaksApplicationSecurity;
    this.previewModal.show();

  }
}
