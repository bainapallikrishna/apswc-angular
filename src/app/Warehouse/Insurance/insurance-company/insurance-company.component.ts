import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CommonServices } from 'src/app/Services/common.services';
import { InputRequest } from 'src/app/Interfaces/employee';
import Swal from 'sweetalert2';
import { HistorybuttonRendererComponent } from 'src/app/custome-directives/Historybutton-renderer.component';
import { AgGridAngular } from 'ag-grid-angular';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';

@Component({
  selector: 'app-insurance-company',
  templateUrl: './insurance-company.component.html',
  styleUrls: ['./insurance-company.component.css']
})
export class InsuranceCompanyComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");

  showloader:boolean=true;
  issubmitted:boolean=false;
  isEdit: boolean = false;
  insurances:any=[];
  gridApi: any;
  gridColumnApi: any;
  frameworkComponents: any;
  public components;
  InsForm:FormGroup;
  modeltitle:string="Add";
  ins_history:any=[];
  subf:any=[];
  public defaultColDef:any=[];

  vcname:string="";
  vdisc:string="";
  vsdate:string="";
  vedate:string="";
  vpamount:string="";
  vactive:string="";
  vremarks:string="";

  columnDefs = [
    { headerName: '#', width: 60,floatingFilter: false, cellRenderer: 'rowIdRenderer' },
    { headerName: 'Insurance Name', width: 200, field: 'insurancE_COMPANY_NAME', cellRenderer: 'actionrender'},
    { headerName: 'Description', width: 200, field: 'insurancE_DESCRIPTION' },
    { headerName: 'Start Date', width: 120, field: 'insurancE_START_DATE' },
    { headerName: 'End Date', width: 120, field: 'insurancE_END_DATE' },
    { headerName: 'Pemium Amount', width: 120, field: 'insurancE_PREMIUM_AMOUNT' },
    { headerName: 'Status', width: 100, field: 'iS_ACTIVE', cellRenderer: params => {
      return (params.value ?  (params.value == "1" ? "<i class='fa fa-circle text-success'></i> Active" : "<i class='fa fa-circle text-danger'></i> Inactive>") : "");
    } },
    { headerName: 'Action',width: 70,floatingFilter: false, cellRenderer: 'buttonRenderer'}
  ];

  constructor(private service: CommonServices, private router: Router, private formBuilder: FormBuilder, private datef: CustomDateParserFormatter) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText: true,
      autoHeight: true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.frameworkComponents = {
      buttonRenderer: HistorybuttonRendererComponent,
    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
      actionrender: function (params) {

        var eSpan3 = document.createElement('div');

        eSpan3.innerHTML = '<u style="color:blue" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + params.value + '</u>' +
          '<div class="dropdown-menu more-option-menu" aria-labelledby="dropdownMenuButton">' +
          '<span class="dropdown-item text-center EditClick"><i class="fa fa-pencil-square-o">Edit</i></span>' +
          '<span class="dropdown-item text-center ViewClick"><i class="fa fa-eye">View</i></span>' +
          '</div>';

        return eSpan3;

      },
    };

    this.InsForm = this.formBuilder.group({
      INS_CODE: [''],
      CompanyName: ['', Validators.required],
      Description: ['', Validators.required],
      StartDate: ['', Validators.required],
      EndDate: ['', Validators.required],
      PremiumAmount: ['', Validators.required],
      IsInsActive: [''],
      InsRemarks: [''],
    })
   }

   dataclicked(params) {
    if (params.colDef.cellRenderer == "actionrender") {
      if (params.event.path[0].classList.value == "dropdown-item text-center EditClick" || params.event.path[0].classList.value == "fa fa-pencil-square-o") {
        this.EditInsurance(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center ViewClick" || params.event.path[0].classList.value == "fa fa-eye") {
        this.ViewInsurance(params);
      }
    }
    if (params.colDef.cellRenderer == "buttonRenderer") {
      this.HistoryInsurance(params);
    }
  }

  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('viewModal') public viewModal: ModalDirective;
  @ViewChild('historyModal') public historyModal: ModalDirective;
  @ViewChild('agGrid') agGrid: AgGridAngular;

  ngOnInit(): void {
    this.LoadInsurances();
    this.showloader = false;
  }

  get ins() { return this.InsForm.controls; }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.insurances);
    //this.gridApi.sizeColumnsToFit();
  }

  LoadInsurances(){
    this.service.getData("GetInsurancesList").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.insurances = data.Details;
        console.log(this.insurances);
        this.gridApi.setRowData(this.insurances);
      }
    },
      error => console.log(error));
  }

  SubmitInsDetails(){
    this.issubmitted = true;

    // stop here if form is invalid
    if (this.InsForm.invalid) {
      return false;
    }

    if(this.datef.GreaterDate(this.ins.StartDate.value,this.ins.EndDate.value)){     
      Swal.fire('warning', "End Date Shoud be greater than Start Date ", 'warning');
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.ins.CompanyName.value;
    req.INPUT_02 = this.datef.format(this.ins.StartDate.value);
    req.INPUT_03 = this.datef.format(this.ins.EndDate.value);
    req.INPUT_04 = this.ins.PremiumAmount.value;
    req.INPUT_05 = this.ins.Description.value;
   
    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveInsuranceDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Insurance Company Details Saved Successfully !!!", 'success');
        console.log(data);
        this.LoadInsurances();
        this.editModal.hide();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  UpdateOutDetails(){
    this.issubmitted = true;

    // stop here if form is invalid
    if (this.InsForm.invalid) {
      return false;
    }

    if(this.ins.IsInsActive.value == "0" && !this.ins.InsRemarks.value){
      Swal.fire('warning', "Remarks is Required ", 'warning');
      return false;
    }

    if(this.datef.GreaterDate(this.ins.StartDate.value,this.ins.EndDate.value)){     
      Swal.fire('warning', "End Date Shoud be greater than Start Date ", 'warning');
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.ins.INS_CODE.value;
    req.INPUT_02 = this.datef.format(this.ins.StartDate.value);
    req.INPUT_03 = this.ins.IsInsActive.value;
    req.INPUT_04 = this.ins.InsRemarks.value;
    req.INPUT_05 = this.ins.Description.value;
    req.INPUT_06 = this.ins.PremiumAmount.value;
   
    req.USER_NAME = this.logUserName;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "UpdateInsuranceDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Insurance Company Details Saved Successfully !!!", 'success');
        console.log(data);
        this.LoadInsurances();
        this.editModal.hide();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  AddInsurance(){
    this.modeltitle = "Add";
    this.InsForm.reset();
    this.issubmitted = false;
    this.isEdit = false;
    this.editModal.show();
  }

  EditInsurance(row){
    const data = row.data;
    this.InsForm.reset();
    this.isEdit = true;
    this.issubmitted = false;
    this.modeltitle = "Edit";

    this.ins.INS_CODE.setValue(data.insurancE_ID);
    this.ins.CompanyName.setValue(data.insurancE_COMPANY_NAME);
    this.ins.Description.setValue(data.insurancE_DESCRIPTION);
    this.ins.StartDate.setValue(this.datef.parse((data.insurancE_START_DATE ? data.insurancE_START_DATE : "").replace("T00:00:00", "")));
    this.ins.EndDate.setValue(this.datef.parse((data.insurancE_END_DATE ? data.insurancE_END_DATE : "").replace("T00:00:00", "")));
    this.ins.PremiumAmount.setValue(data.insurancE_PREMIUM_AMOUNT);
    this.ins.IsInsActive.setValue(data.iS_ACTIVE ? data.iS_ACTIVE.toString() : '1');
    this.ins.InsRemarks.setValue(data.remarks);

    this.editModal.show();
  }

  HistoryInsurance(row){
    this.showloader = true;

    const req = new InputRequest();
    if (row == 'all') {
      this.isEdit = true;
      req.INPUT_01 = null;
    }
    else {
      this.isEdit = false;
      req.INPUT_01 = row.data.insurancE_ID;
    }

    req.INPUT_02 = "INSURANCE";

    this.service.postData(req, "GetEmployeeHistory").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.ins_history = data.Details;
        this.historyModal.show();
      }

    },
      error => console.log(error));
  }

  ViewInsurance(row){
    const data = row.data;

    this.vcname = data.insurancE_COMPANY_NAME;
    this.vdisc = data.insurancE_DESCRIPTION;
    this.vsdate = data.insurancE_START_DATE;
    this.vedate = data.insurancE_END_DATE;
    this.vpamount = data.insurancE_PREMIUM_AMOUNT;
    this.vactive = data.iS_ACTIVE;
    this.vremarks = data.remarks;

    this.viewModal.show();
  }

  CloseEditModal(){
    this.editModal.hide();
    this.LoadInsurances();
  }

}
