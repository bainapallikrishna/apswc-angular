import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhReceiptComponent } from './wh-receipt.component';

describe('WhReceiptComponent', () => {
  let component: WhReceiptComponent;
  let fixture: ComponentFixture<WhReceiptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhReceiptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhReceiptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
