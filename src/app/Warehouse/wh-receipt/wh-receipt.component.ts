import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DynamicbuttonRendererComponent } from 'src/app/custome-directives/dynamic-button-render.components';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';

// import { jsPDF } from 'jspdf';
// import html2canvas from 'html2canvas';

@Component({
  selector: 'app-wh-receipt',
  templateUrl: './wh-receipt.component.html',
  styleUrls: ['./wh-receipt.component.css']
})
export class WhReceiptComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  reciptForm: FormGroup;
  GenerateForm: FormGroup;
  issubmit: boolean = false;
  isGenerate: boolean = false;
  showloader: boolean = true;
  defaultColDef: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];
  receipts: any = [];
  frameworkComponents: any;
  public components;
  insurances: any = [];

  preceiptno: string = "";
  pwarehousename: string = "";
  pdepname: string = "";
  pcommodity: string = "";
  pbags: string = "";
  pweight: string = "";
  pgrade: string = "";
  pwlicence: string = "";
  pwlicenceno: string = "";
  pvalidupto: string = "";
  pgcondition: string = "";
  PPmarks: string = "";
  Psrate: string = "";
  pinstype: string = "";
  pinsname: string = "";
  pamount: string = "";
  pinsfromdate: string = "";
  pinstodate: string = "";
  pmarkvalue: string = "";
  pvaluation: string = "";
  presdate: string = "";
  ppackagetype: string = "";
  public myAngularxQrCode: string = null;

  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Depositor ID', width: 100, field: 'registratioN_ID' },
    { headerName: 'Depositor Name', width: 200, field: 'registraioN_NAME' },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID' },
    { headerName: 'Commodity Name', width: 200, field: 'commodity' },
    { headerName: 'Contract Type', width: 150, field: 'contracT_TYPE_NAME' },
    { headerName: 'Bags', width: 100, field: 'bags' },
    { headerName: "Weight(MT's)", width: 100, field: 'weight' },
    {
      headerName: 'Action', width: 150, floatingFilter: false, field: 'receipT_NUMBER', cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        ButtonClick: this.ButtonClick.bind(this),
        DownloadReceipt: this.DownloadReceipt.bind(this),
        label: 'Generate Receipt',
        label2: 'Download Receipt',
      },
    }
  ];

  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('receiptModal') public receiptModal: ModalDirective;

  constructor(private service: CommonServices,
    private router: Router,
    private EncrDecr: EncrDecrServiceService,
    private formBuilder: FormBuilder) {
    this.frameworkComponents = {
      buttonRenderer: DynamicbuttonRendererComponent,
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }

  ngOnInit(): void {
    this.reciptForm = this.formBuilder.group({
      ReceiptType: [null, Validators.required]
    });

    this.GenerateForm = this.formBuilder.group({
      REGISTRATION_ID: [''],
      BOOKING_ID: [''],
      COMMODITY_CODE: [''],
      NO_OF_BAGS: [''],
      WEIGHT: [''],
      GoodsCondition: [null, Validators.required],
      GunnyBagsCondition: [null, Validators.required],
      ValidUpto: [null, Validators.required],
      PrivateMarks: [''],
      InsCompany: [null, Validators.required],
      PolicyName: ['', Validators.required],
      PolicyAmount: [''],
      MarketRate: [''],
      Valuation: [''],
      Remarks: [''],
    });
    this.LoadInsurances();
    this.showloader = false;
  }

  get rec() { return this.reciptForm.controls; }
  get gen() { return this.GenerateForm.controls; }
  get i() { return this.gen.insurences as FormArray; }

  ButtonClick(row) {
    this.GenerateForm.reset();

    const data = row.rowData;

    this.gen.REGISTRATION_ID.setValue(data.registratioN_ID);
    this.gen.BOOKING_ID.setValue(data.bookinG_ID);
    this.gen.COMMODITY_CODE.setValue(data.commoditY_ID);
    this.gen.NO_OF_BAGS.setValue(data.bags);
    this.gen.WEIGHT.setValue(data.weight);

    this.editModal.show();
  }

  DownloadReceipt(row) {
    if (row.rowData.receipT_NUMBER)
      this.DownloadPdf(row.rowData.receipT_NUMBER)
  }

  DownloadPdf(receptno) {
    let reciptnumber = receptno;
    this.showloader = true;
    var data = { "INPUT_01": receptno, "TYPEID": "WAREHOUSE_RECEIPT" };
    this.service.postData(data, "APSWCMapsServiceConsume").subscribe(data => {
      this.showloader = false;
      if (data.Status == "Success") {
        var b = this.service.s_sd(data.CertIssuerData.Base64pdf, 'application/pdf');
        var l = document.createElement('a');
        l.href = window.URL.createObjectURL(b);
        l.download = "Warehouse_Receipt_" + reciptnumber + ".pdf";
        document.body.appendChild(l);
        l.click();
        document.body.removeChild(l);

        //console.log(data.Details);Base64pdf
        //this.fillPDFData(data.Details[0])
      }
      else
        Swal.fire('warning', data.Reason, 'warning');

    },
      error => console.log(error));

    // this.showloader = true;
    // var data = { "INPUT_01": receptno };
    // this.service.postData(data, "DownloadReceipts").subscribe(data => {
    //   this.showloader = false;
    //   if (data.StatusCode == "100") {
    //     console.log(data.Details);
    //     this.fillPDFData(data.Details[0])
    //   }
    //   else
    //     Swal.fire('warning', data.StatusMessage, 'warning');

    // },
    //   error => console.log(error));
  }

  CloseEditModal() {
    this.GetWHReceipts();
    this.editModal.hide();
  }

  // AddInsurance(){
  //   this.i.push(this.formBuilder.group({
  //     InsCompany: [null, Validators.required],
  //         PolicyName: ['', Validators.required],
  //         PolicyNo: ['', Validators.required],
  //         InsAmt: ['', Validators.required],
  //         FromDate: ['', Validators.required],
  //         ToDate: ['', Validators.required],
  //   }));
  // }

  // RemoveInsurance(index){
  //   this.i.removeAt(index);
  // }


  GetWHReceipts() {
    this.issubmit = true;
    // stop here if form is invalid
    if (this.reciptForm.invalid) {
      return false;
    }
    let methodname: string = "";
    if (this.rec.ReceiptType.value == 'Pending')
      methodname = "GetPendingWHReceipts";
    else
      methodname = "GetGeneratedWHReceipts";
    this.showloader = true;
    this.receipts = [];
    var data = { "INPUT_01": this.workLocationCode };
    this.service.postData(data, methodname).subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.receipts = data.Details;
        console.log(this.receipts);
        
      }
      this.gridApi.setRowData(this.receipts);
    },
      error => console.log(error));
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.receipts);
    //this.gridApi.sizeColumnsToFit();
    //this.LoadEmployeeList()
  }

  SaveReceiptDetails() {
    this.isGenerate = true;
    // stop here if form is invalid
    if (this.GenerateForm.invalid) {
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.workLocationCode;
    req.INPUT_02 = this.gen.REGISTRATION_ID.value;
    req.INPUT_03 = this.gen.BOOKING_ID.value;
    req.INPUT_04 = this.gen.COMMODITY_CODE.value;
    req.INPUT_05 = this.gen.GoodsCondition.value;
    req.INPUT_06 = this.gen.PrivateMarks.value;
    req.INPUT_07 = this.gen.PolicyName.value;
    req.INPUT_08 = this.gen.InsCompany.value;
    req.INPUT_09 = this.gen.PolicyAmount.value;
    req.INPUT_10 = this.gen.MarketRate.value;
    req.INPUT_11 = this.gen.Valuation.value;
    req.INPUT_12 = this.gen.Remarks.value;
    req.INPUT_13 = this.gen.NO_OF_BAGS.value;
    req.INPUT_14 = this.gen.WEIGHT.value;
    req.INPUT_15 = this.gen.GunnyBagsCondition.value;
    req.INPUT_16 = this.gen.ValidUpto.value;

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = 'WEB';
    this.service.postData(req, "SaveReceiptDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        console.log(data);
        this.GetWHReceipts();
        this.editModal.hide();
        this.DownloadPdf(data.Details[0].receipT_NUMBER);
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  LoadInsurances() {
    this.service.getData("GetInsuranceList").subscribe(data => {
      if (data.StatusCode == "100") {
        this.insurances = data.Details;
      }
    },
      error => console.log(error));
  }

  fillPDFData(data) {
    this.preceiptno = data.receipT_NUMBER;
    this.myAngularxQrCode = this.EncrDecr.set(this.preceiptno);
    this.pwarehousename = data.namE_LOCATION_WARESHOUSE;
    this.pdepname = data.depositeR_ADDRESS;
    this.pcommodity = data.commodity;
    this.pbags = data.bags;
    this.ppackagetype = data.packagE_TYPE;
    this.pweight = data.weight;
    this.pgrade = "";
    this.pwlicence = data.wB_LICENCE;
    this.pwlicenceno = "";
    this.pvalidupto = data.wH_VALIDTY_DATE;
    this.pgcondition = data.conditioN_OF_GOODS;
    this.PPmarks = data.privatE_MARKS_DEPOSITER;
    this.Psrate = data.rate;
    this.pinstype = data.insurancE_TYPE;
    this.pinsname = data.insurancE_COMPANY_NAME;
    this.pamount = data.insurancE_AMOUNT;
    this.pinsfromdate = data.froM_DATE;
    this.pinstodate = data.enD_DATE;
    this.pmarkvalue = data.markeT_RATE_PER_TON;
    this.pvaluation = data.markeT_RATE_AMOUNT;
    this.presdate = data.receipT_GENERATION_DATE;

    this.receiptModal.show();
  }

  public openPDF(): void {


    //window.scrollTo(0, 0);
    // var data = document.getElementById('htmlData');
    // html2canvas(data, {
    //   scale: 3, scrollY: 0
    // }).then(canvas => {
    //   // Few necessary setting options  
    //   var imgWidth = 210;
    //   var pageHeight = 295;
    //   var imgHeight = canvas.height * imgWidth / canvas.width;
    //   var heightLeft = imgHeight;

    //   const contentDataURL = canvas.toDataURL('image/jpeg')
    //   // let pdf = new jsPDF('p', 'mm', 'a4'); // A4 size page of PDF  
    //   let pdf = new jsPDF('p', 'mm', 'a4');
    //   var position = 0;
    //   pdf.addImage(contentDataURL, 'jpeg', 0, position, imgWidth, imgHeight)
    //   heightLeft -= pageHeight;

    //   while (heightLeft >= 0) {
    //     position = heightLeft - imgHeight;
    //     pdf.addPage();
    //     pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight);
    //     heightLeft -= pageHeight;
    //   }
    //   pdf.save('warehouse-receipt.pdf'); // Generated PDF 
    // });
  }

  viewReceipt() {
    this.receiptModal.show();
  }

  closeReceipt() {
    this.receiptModal.hide();
  }
}
