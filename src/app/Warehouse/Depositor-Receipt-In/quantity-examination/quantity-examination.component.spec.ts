import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuantityExaminationComponent } from './quantity-examination.component';

describe('QuantityExaminationComponent', () => {
  let component: QuantityExaminationComponent;
  let fixture: ComponentFixture<QuantityExaminationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuantityExaminationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuantityExaminationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
