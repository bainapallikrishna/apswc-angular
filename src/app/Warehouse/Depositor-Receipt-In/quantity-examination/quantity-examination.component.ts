import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-quantity-examination',
  templateUrl: './quantity-examination.component.html',
  styleUrls: ['./quantity-examination.component.css']
})
export class QuantityExaminationComponent implements OnInit {

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  showloader: boolean = false;
  isSubmit: boolean = false;
  isReject: boolean = false;
  isQtySubmit: boolean = false;
  DepForm: FormGroup;
  QtyForm: FormGroup;
  tokens: any = [];
  isTable: boolean = false;
  gridColumnApi: any = [];
  gridApi: any = [];
  public rowseledted: object;
  frameworkComponents: any;
  public components;
  public defaultColDef;

  netWeight: any = 0;
  BagsWeight: any = 0;
  commoWeight: any = 0;

  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID', width: 100, field: 'tokeN_ID', onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Token Id",
    },
    { headerName: 'Depositor Name', width: 200, field: 'regisratioN_NAME', },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID', },
    { headerName: 'Commodity Group', width: 150, field: 'commoditY_GROUP', },
    { headerName: 'Commodity Name', width: 200, field: 'commodity', },
    { headerName: 'Contract Type', width: 150, field: 'contracT_TYPE_NAME', },
    // { headerName: 'Quantity Type',  width: 250, field: 'quantitY_TYPE_NAME',   },
    // { headerName: 'No Of Bags/Weight',  width: 250, field: 'nO_OF_BAGS',   },   
    { headerName: 'Mode Of Transport', width: 150, field: 'modE_OF_TRANSPORT_NAME', },
    { headerName: 'Vehicle No', width: 100, field: 'vehiclE_NUMBER', }

  ];

  @ViewChild('previewModal') public previewModal: ModalDirective;

  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText: true,
      autoHeight: true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

    this.DepForm = this.formBuilder.group({
      NoOfBags: ['', Validators.required],
      ABagWeight: ['', Validators.required],
      AvgBagWeight: ['', Validators.required],
      DiffBagWeight: [null, Validators.required],
      SourceOfReceipt: ['', Validators.required],
      CropYear: ['', Validators.required],
      NameConsignor: ['', Validators.required],
      ConOfBags: [null, Validators.required],
    });

    this.QtyForm = this.formBuilder.group({
      bags: this.formBuilder.array([
        this.formBuilder.group({
          BagWeight: ['', Validators.required],
        })
      ])
    });
  }

  ngOnInit(): void {
  }

  get dep() { return this.DepForm.controls; }
  get qty() { return this.QtyForm.controls; }
  get b() { return this.qty.bags as FormArray; }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.LoadTokens();
  }

  LoadTokens() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetQuantityTokens").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));
  }

  makeCellClicked(event) {

    if (event.node.selected == true) {
      this.rowseledted = {};
      this.isTable = true;

      this.rowseledted = event.node.data;

      this.netWeight = Number(event.node.data.neT_WEIGHT);
      //this.BagsWeight = Number(event.node.data.neT_WEIGHT);
      this.commoWeight = Number(event.node.data.commoditY_WEIGHT);

      this.ClearBookingDatails();
    }
  }

  CalDiffWeight(event) {
    if (Number(this.dep.ABagWeight.value) > 0 && Number(this.dep.AvgBagWeight.value) > 0) {
      this.dep.DiffBagWeight.setValue(((Number(this.dep.AvgBagWeight.value) - Number(this.dep.ABagWeight.value)) * 1000).toFixed(0))
      if (Number(this.dep.DiffBagWeight.value) < -250)
        this.isReject = true;
      else
        this.isReject = false;
    }
    else
      this.dep.DiffBagWeight.setValue('');
  }

  QuantityPreview() {
    if (!this.dep.NoOfBags.value) {
      Swal.fire('warning', "Please Enter No Of Bags", 'warning');
      return false;
    }
    this.previewModal.show();
  }

  CalBagWeight() {
    if (this.dep.NoOfBags.value) {
      let bagva = ((this.netWeight / Number(this.dep.NoOfBags.value)) * 1000).toFixed(3);
      this.dep.ABagWeight.setValue(bagva);
    }
    else
      this.dep.ABagWeight.setValue('');
  }

  AddBag() {
    this.b.push(this.formBuilder.group({
      BagWeight: ['', Validators.required],
    }));
  }

  RemoveBag(index) {
    this.b.removeAt(index);
  }

  ClearBookingDatails() {
    this.DepForm.reset();
    this.QtyForm.reset();
    this.b.clear();
    this.b.reset();
    this.AddBag();

    // this.htserlist = [];
    // this.htpriclist = [];
    // this.comartments = [];
    // this.stacks = [];
    // this.s.reset();
    // this.s.clear();

    // this.s.push(this.formBuilder.group({
    //   GodownName: [null, Validators.required],
    //   CompartmentName: [null, Validators.required],
    //   StackType: [null, Validators.required],
    //   StackName: [null, Validators.required],
    //   BagsCnt: ['', Validators.required],
    //   VWeight: [''],
    // }));

  }

  SaveQuatity() {
    this.isQtySubmit = true;

    // stop here if form is invalid
    if (this.QtyForm.invalid) {
      return false;
    }

    this.showloader = true;
    let bagwet = 0;
    for (let i = 0; i < this.b.length; i++) {
      bagwet += Number(this.b.value[i].BagWeight);
    }
    let avgbagwe = (bagwet / this.b.length).toFixed(3);
    this.dep.AvgBagWeight.setValue(avgbagwe);
    this.dep.DiffBagWeight.setValue(((Number(this.dep.AvgBagWeight.value) - Number(this.dep.ABagWeight.value)) * 1000).toFixed(3));
    this.previewModal.hide();
    this.showloader = false;
  }

  SaveQuantitytyChecking(stype) {
    this.isSubmit = true;
    const savetype: string = stype;
    // stop here if form is invalid
    if (this.DepForm.invalid) {
      return false;
    }



    this.showloader = true;
    let bagslist: string[] = [];
    for (let i = 0; i < this.b.length; i++) {
      bagslist.push(this.b.value[i].BagWeight);
    }

    const req = new InputRequest();
    const rowsleted = this.rowseledted as any;
    const bcount: string = rowsleted.bookinG_COUNT;


    req.INPUT_01 = rowsleted.tokeN_ID;
    req.INPUT_02 = rowsleted.warehousE_ID;
    req.INPUT_03 = rowsleted.bookinG_ID
    req.INPUT_04 = rowsleted.reservatioN_ID;
    req.INPUT_05 = rowsleted.commoditY_CODE;
    req.INPUT_06 = rowsleted.commoditY_GROUP;
    req.INPUT_07 = rowsleted.commodity_id;
    req.INPUT_08 = rowsleted.commodity;
    req.INPUT_09 = "1";//Receipt In
    req.INPUT_10 = savetype;//0 Reject 1 Accept
    req.INPUT_11 = rowsleted.registratioN_TYPE;
    req.INPUT_12 = this.dep.NoOfBags.value;
    req.INPUT_13 = this.dep.ABagWeight.value;
    req.INPUT_14 = this.dep.AvgBagWeight.value;
    req.INPUT_15 = this.dep.DiffBagWeight.value;
    req.INPUT_16 = bagslist.join(",");

    req.INPUT_18 = this.dep.SourceOfReceipt.value;
    req.INPUT_19 = this.dep.CropYear.value;
    req.INPUT_20 = this.dep.NameConsignor.value;
    req.INPUT_21 = this.dep.ConOfBags.value;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUsercode;
    this.service.postData(req, "SaveQuantityChecking").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];

        if (savetype == '1')
          Swal.fire('success', "Token : " + rowsleted.tokeN_ID + " , Quantity Examination Accept Details Saved Successfully !!!", 'success');
        else
          Swal.fire('success', "Token : " + rowsleted.tokeN_ID + " , Quantity Examination Reject Details Saved Successfully !!!", 'success');


        if (savetype == '1' && bcount == '1')
          this.router.navigate(['/QualityExamination']);
        else
          this.reloadCurrentRoute();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
}
