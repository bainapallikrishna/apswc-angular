import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeighmentInComponent } from './weighment-in.component';

describe('WeighmentInComponent', () => {
  let component: WeighmentInComponent;
  let fixture: ComponentFixture<WeighmentInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeighmentInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeighmentInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
