import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import { HttpEventType } from '@angular/common/http';
import Swal from 'sweetalert2';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-receipt-in-request',
  templateUrl: './receipt-in-request.component.html',
  styleUrls: ['./receipt-in-request.component.css']
})
export class ReceiptInRequestComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  @ViewChild('previewModal') public previewModal: ModalDirective;

  showloader: boolean = false;
  isSubmit: boolean = false;
  DepForm: FormGroup;
  message: string = "";
  progress: number = 0;
  preview: any;

  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Depositor ID', width: 100, field: 'registereD_ID', checkboxSelection: true, },
    { headerName: 'Depositor Name', width: 200, field: 'name' },
    { headerName: 'Depositor Type', width: 150, field: 'registratioN_NAME' },
    { headerName: 'Reservation ID', width: 160, field: 'bookinG_ID' },
    { headerName: 'Reservation Date', width: 150, field: 'bookinG_DATE' },
    { headerName: 'Commodity Name', width: 200, field: 'commodity' },
    { headerName: 'Start Date', width: 100, field: 'starT_DATE' },
    { headerName: 'End Date', width: 100, field: 'enD_DATE' },
    // { headerName: 'Contract Type', width: 150, field: 'contracT_TYPE_NAME', sortable: true, filter: true },
    // { headerName: 'Weight/Bags', width: 200, field: 'nO_OF_BAGS', sortable: true, filter: true }

  ];

  DepositorrowData = [];
  public defaultColDef;
  dipositors: any = [];
  transports: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];
  frameworkComponents: any;
  public components;
  selbookids: string = "";
  seldepids: string = "";
  selregtypes: string = "";
  selcomids: any = [];
  seldocpath: any ;
  seldoctype: string = "";
  seldoccat: string = "";

  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder, private sanitizer: DomSanitizer) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }


    this.DepForm = this.formBuilder.group({
      DocPath: ['', Validators.required],
      RegDoc: [''],
    });

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }

  ngOnInit(): void {
    let now: Date = new Date();
    let currdate = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();

  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //params.api.setRowData(this.DepositorrowData);
    //this.gridApi.sizeColumnsToFit();
    this.LoadDepositorList()
  }

  get dep() { return this.DepForm.controls; }

  getSelectedRows(): void {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    this.selbookids = selectedData.map(node => `${node.bookinG_ID}`).join(',');
    this.seldepids = selectedData.map(node => `${node.registereD_ID}`).join(',');
    this.selregtypes = selectedData.map(node => `${node.registratioN_TYPE}`).join(',');
    this.selcomids = selectedData.map(node => `${node.commoditY_ID}`).join(',');
  }

  LoadDepositorList() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetSpaceReservationDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.dipositors = data.Details;
        this.gridApi.setRowData(this.dipositors);
      }
      else
        this.gridApi.setRowData(this.dipositors);

    },
      error => console.log(error));
  }


  uploadFile(event) {
    this.seldocpath = "" ;
    this.seldoctype = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      this.seldoccat = filetype;

      if (filetype == 'image/jpeg' || filetype == 'image/png')
        this.seldoctype = 'IMAGE';
      else
        this.seldoctype = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      const reader = new FileReader();
      reader.onload = () => {
        if(this.seldoctype == "PDF"){
          const result = reader.result as string;
          const blob = this.service.s_sd((result).split(',')[1], this.seldoccat);
          const blobUrl = URL.createObjectURL(blob);
          this.seldocpath = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
        }
        else{
          this.seldocpath = reader.result as string;
          this.preview = this.seldocpath;
        }
        
      }
      reader.readAsDataURL(<File>event.target.files[0])

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', "ReceiptInRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)
            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.'
            this.uploadFinished(event.body);
          }

        });
    }
  }
  public uploadFinished = (event) => {
    this.dep.DocPath.setValue(event.fullPath);
  }




  SaveGateInDetails() {
    this.getSelectedRows();
    this.isSubmit = true;


    if (!this.selbookids) {
      Swal.fire('warning', "Please Select at least One Depositor", 'warning');
      return false;
    }

    if (!this.dep.DocPath.value) {
      Swal.fire('warning', "Please Upload Document", 'warning');
      return false;
    }


    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.workLocationCode;
    req.INPUT_02 = this.seldepids;
    req.INPUT_03 = this.selbookids;
    req.INPUT_04 = "1";
    req.INPUT_05 = this.dep.DocPath.value;
    req.INPUT_06 = this.selregtypes;
    req.INPUT_07 = this.selcomids;

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "Savereceiptin").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Goods IN Deatils Saved & Token " + data.Details[0].tokeN_ID + " Generated Successfully !!!", 'success');
        //this.reloadCurrentRoute();
        if (this.selregtypes.includes('STA') || this.selregtypes.includes('PSU') || this.selregtypes.includes('CEN'))
          this.router.navigate(['/GateIn']);
        else
          this.router.navigate(['/OGateIn']);

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

  Docpriview(val) {

    // const blob = this.service.s_sd((val).split(',')[1], this.seldoccat);
    // const blobUrl = URL.createObjectURL(blob);
    // this.preview = this.sanitizer.bypassSecurityTrustUrl(blobUrl);
    //this.preview = this.sanitizer.bypassSecurityTrustUrl(val);
    //this.preview = this.preview.changingThisBreaksApplicationSecurity;
    this.previewModal.show();

  }

}

