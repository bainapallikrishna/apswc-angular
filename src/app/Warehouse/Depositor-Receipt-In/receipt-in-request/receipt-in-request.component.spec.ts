import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptInRequestComponent } from './receipt-in-request.component';

describe('ReceiptInRequestComponent', () => {
  let component: ReceiptInRequestComponent;
  let fixture: ComponentFixture<ReceiptInRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptInRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptInRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
