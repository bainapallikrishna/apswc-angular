import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';

@Component({
  selector: 'app-gate-out',
  templateUrl: './gate-out.component.html',
  styleUrls: ['./gate-out.component.css']
})
export class GateOutComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  showloader: boolean = false;
  isSubmit: boolean = false;
  DepForm: FormGroup;
  tokens: any = [];

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  isTable: boolean = false;
  gridColumnApi: any = [];
  gridApi: any = [];
  public rowseledted: object;
  frameworkComponents: any;
  public components;
  public defaultColDef;

  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID', width: 100, field: 'tokeN_ID' ,onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Token Id"
    },
    { headerName: 'Depositor Name', width: 200, field: 'regisratioN_NAME' },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID' },
    { headerName: 'Commodity Group', width: 150, field: 'commoditY_GROUP' },
    { headerName: 'Commodity Name', width: 200, field: 'commodity' },
    { headerName: 'Contract Type', width: 150, field: 'contracT_TYPE_NAME' },
    // { headerName: 'Quantity Type', width: 250, field: 'quantitY_TYPE_NAME'  },
    // { headerName: 'No Of Bags/Weight', width: 250, field: 'nO_OF_BAGS'  },
    { headerName: 'Mode Of Transport', width: 150, field: 'modE_OF_TRANSPORT_NAME' },
    { headerName: 'Vehicle No', width: 100, field: 'vehiclE_NUMBER' },
  ];

  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }
    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText:true,
      autoHeight:true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };
    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

    this.DepForm = this.formBuilder.group({
      //TokenNo: [null, Validators.required],
      MOT: [''],
      VehicleNO: [''],
      TruckType: [''],
    });

  }

  ngOnInit(): void {
    // this.LoadTokens();
  }
  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.LoadTokens();
  }

  makeCellClicked(event) {

    if (event.node.selected == true) {
      this.rowseledted = {};
      this.isTable = true;

      this.rowseledted = event.node.data;

    }



  }

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
    //window.alert('selection changed, ' + rowCount + ' rows selected');
  }


  get dep() { return this.DepForm.controls; }

  LoadTokens() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetGateoutTokens").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));
  }

  GetTokenInfo(event) {
    this.ClearDetails();
    if (event) {
      this.showloader = true;
      let obj: any = { "INPUT_01": event }

      this.service.postData(obj, "GetTokenInfo").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.FillDetails(data.Details[0]);
        }
      },
        error => console.log(error));
    }
  }

  ClearDetails() {
    this.dep.MOT.setValue('');
    this.dep.VehicleNO.setValue('');
    this.dep.TruckType.setValue(null);
  }

  FillDetails(data) {
    this.dep.MOT.setValue(data.modE_OF_TRANSPORT_NAME);
    this.dep.VehicleNO.setValue(data.vehiclE_NUMBER);
  }

  SaveGateOut(stype) {
    this.isSubmit = true;
    const savetype = stype;
    // stop here if form is invalid
    if (this.DepForm.invalid) {
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    const rowsleted = this.rowseledted as any;
    req.INPUT_01 = rowsleted.tokeN_ID;//this.dep.TokenNo.value;
    req.INPUT_02 = 'EMPTY';//this.dep.TruckType.value;
    req.INPUT_03 = "1";//Receipt In

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveGateOut").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Token : " + rowsleted.tokeN_ID + " , Gate Out Deatils Saved Successfully !!!", 'success');
        if (savetype == 'save')
          this.reloadCurrentRoute();
        else
          this.router.navigate(['/QuantityExamination']);
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

}
