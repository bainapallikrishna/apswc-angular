import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';

@Component({
  selector: 'app-weighment-out',
  templateUrl: './weighment-out.component.html',
  styleUrls: ['./weighment-out.component.css']
})
export class WeighmentOutComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  showloader: boolean = false;
  isSubmit: boolean = false;

  isTable: boolean = false;
  WehoutForm: FormGroup;
  Tokenlist: any = [];
  tokeninfo: any = [];
  public defaultColDef;

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  gridColumnApi: any = [];
  gridApi: any = [];
  public rowseledted: object;
  frameworkComponents: any;
  public components;


  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID', width: 100, field: 'tokeN_ID', onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Token Id"
    },
    { headerName: 'Depositor Name', width: 200, field: 'regisratioN_NAME' },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID' },
    { headerName: 'Commodity Group', width: 150, field: 'commoditY_GROUP' },
    { headerName: 'Commodity Name', width: 200, field: 'commodity' },
    { headerName: 'Contract Type', width: 100, field: 'contracT_TYPE_NAME' },
    // { headerName: 'Quantity Type', width: 250, field: 'quantitY_TYPE_NAME' },
    // { headerName: 'No Of Bags/Weight', width: 250, field: 'nO_OF_BAGS' },   
    { headerName: 'Mode Of Transport', width: 150, field: 'modE_OF_TRANSPORT_NAME' },
    { headerName: 'Vehicle No', width: 100, field: 'vehiclE_NUMBER' }

  ];


  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.WehoutForm = this.formBuilder.group({
      //Tknid: [null, Validators.required],
      TarWeight: ['', Validators.required],
      EmptyBagsWeight: ['', Validators.required],
      grswght: [''],
      Netwght: [''],
      Commowght: [''],
    });

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText:true,
      autoHeight:true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

  }


  ngOnInit(): void {

  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.LoadTokens();
  }


  get dep() { return this.WehoutForm.controls; }

  LoadTokens() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode, "INPUT_02": "WEIGHT_OUTTKN" }

    this.service.postData(obj, "Weighment_OUTTokenList").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Tokenlist = data.Details;
        this.gridApi.setRowData(this.Tokenlist);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.Tokenlist);

      }
    },
      error => console.log(error));
  }

  makeCellClicked(event) {

    if (event.node.selected == true) {
      this.rowseledted = {};
      this.isTable = true;
      this.dep.grswght.setValue(event.node.data.grosS_WEIGHT);
      this.rowseledted = event.node.data;

    }



  }

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
    //window.alert('selection changed, ' + rowCount + ' rows selected');
  }

  GetTokenInfo(event) {
    this.tokeninfo = [];
    if (event) {
      this.showloader = true;
      let obj: any = { "INPUT_01": event, "INPUT_02": "COMDLISTOFTKN" }

      this.service.postData(obj, "Weighment_OUTTokenList").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.tokeninfo = data.Details;
          this.filldetails(this.tokeninfo)
          this.isTable = true;
        }
      },
        error => console.log(error));
    }

  }

  filldetails(data: any) {

    this.dep.grswght.setValue(data[0].grosS_WEIGHT);
  }

  Loadnetweght(val) {
    const grossweight = Number(this.dep.grswght.value);
    const tareweight = Number(this.dep.TarWeight.value);
    if (tareweight >= grossweight) {
      Swal.fire('info', "Tare Weight is Not Greater than Gross Weight !!!", 'info');
      this.dep.TarWeight.setValue("");
      this.dep.Netwght.setValue("");
      return false;

    }
    else {
      this.dep.Netwght.setValue((Number(this.dep.grswght.value) - Number(this.dep.TarWeight.value)).toFixed(3))
      if(this.dep.EmptyBagsWeight.value){
        this.dep.Commowght.setValue((Number(this.dep.Netwght.value) - Number(this.dep.EmptyBagsWeight.value)).toFixed(3))
      }
    }
  }

  LoadCommweght(val) {
    const netweight = Number(this.dep.Netwght.value);
    const empweight = Number(this.dep.EmptyBagsWeight.value);
    if (empweight >= netweight) {
      Swal.fire('info', "Empty Bags Weight is Not Greater than Net Weight !!!", 'info');
      this.dep.empweight.setValue("");
      this.dep.netweight.setValue("");
      return false;

    }
    else {
      this.dep.Commowght.setValue((Number(this.dep.Netwght.value) - Number(this.dep.EmptyBagsWeight.value)).toFixed(3))
    }
  }

  SaveWeightDetails(stype) {
    const savingtype: string = stype;
    this.isSubmit = true;`  `

    // stop here if form is invalid
    if (this.WehoutForm.invalid) {
      return false;
    }

    if (Number(this.dep.TarWeight.value) > Number(this.dep.grswght.value)) {
      Swal.fire('warning', "Empty Vehicle Weight not greater than Gross Weight", 'warning');
      return false;
    }

    this.showloader = true;
    const rowsleted = this.rowseledted as any;
    const req = new InputRequest();
    req.INPUT_01 = this.dep.TarWeight.value;
    req.INPUT_02 = rowsleted.tokeN_ID;//this.dep.Tknid.value;
    req.INPUT_03 = "0"; //this.dep.EmptyBagsWeight.value;    
    req.INPUT_04 = this.dep.Netwght.value;
    req.INPUT_05 = this.workLocationCode;
    req.INPUT_06 = "1";
    req.INPUT_07 = this.dep.Commowght.value;
    req.INPUT_08 = this.dep.EmptyBagsWeight.value;
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveWeighmentOut").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Token : " + rowsleted.tokeN_ID + " , Weighment Out Deatils Saved Successfully !!!", 'success');
        if (savingtype == 'save')
          this.reloadCurrentRoute();
        else
          this.router.navigate(['/GateOut']);
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
}
