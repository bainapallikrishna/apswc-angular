import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeighmentOutComponent } from './weighment-out.component';

describe('WeighmentOutComponent', () => {
  let component: WeighmentOutComponent;
  let fixture: ComponentFixture<WeighmentOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeighmentOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeighmentOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
