import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { DynamicbuttonRendererComponent } from 'src/app/custome-directives/dynamic-button-render.components';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import { EncrDecrServiceService } from 'src/app/Services/encr-decr-service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-whack',
  templateUrl: './whack.component.html',
  styleUrls: ['./whack.component.css']
})
export class WhackComponent implements OnInit {
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  defaultColDef: any = [];
  gridColumnApi: any = [];
  frameworkComponents: any;
  public components;
  receipts: any = [];
  gridApi: any = [];
  issubmit: boolean = false;
  showloader: boolean = true;
  reciptForm: FormGroup;

  columnDefs = [
    { headerName: '#', width: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Acknowledge No', width: 150, field: 'acknowledgE_NO' },
    { headerName: 'Stack In Date', width: 150, field: 'acknowlegmenT_DATE' },
    { headerName: 'Depositor ID', width: 150, field: 'registratioN_ID' },
    { headerName: 'Depositor Name', width: 150, field: 'registratioN_NAME' },
    { headerName: 'Token ID', width: 100, field: 'tokeN_ID' },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID' },
    { headerName: 'Commodity Name', width: 200, field: 'commoditY_NAME' },
    { headerName: 'No Of Days', width: 150, field: 'nO_OF_BAGS' },
    { headerName: "Bag Weight(KG's)", width: 150, field: 'neT_WEIGHT' },
    {
      headerName: 'Action', width: 150, floatingFilter: false, field: 'acknowledgE_NO', cellRenderer: 'buttonRenderer',
      cellRendererParams: {
        ButtonClick: this.ButtonClick.bind(this),
        DownloadReceipt: this.DownloadReceipt.bind(this),
        label: 'Generate Memo',
        label2: 'Download Memo',
      },
    }
  ];

  @ViewChild('agGrid') agGrid: AgGridAngular;

  constructor(private service: CommonServices,
    private router: Router,
    private EncrDecr: EncrDecrServiceService,
    private formBuilder: FormBuilder) {
    this.frameworkComponents = {
      buttonRenderer: DynamicbuttonRendererComponent,
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }

  ngOnInit(): void {
    this.reciptForm = this.formBuilder.group({
      ReceiptType: [null, Validators.required]
    });
    this.showloader = false;
  }

  get rec() { return this.reciptForm.controls; }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    params.api.setRowData(this.receipts);
    //this.gridApi.sizeColumnsToFit();
    //this.LoadEmployeeList()
  }

  GetWHReceipts() {
    this.issubmit = true;
    // stop here if form is invalid
    if (this.reciptForm.invalid) {
      return false;
    }
    let methodname: string = "";
    if (this.rec.ReceiptType.value == 'Pending')
      methodname = "GetPendingWHMemos";
    else
      methodname = "GetCompletedWHMemos";
    this.showloader = true;
    this.receipts = [];
    var data = { "INPUT_01": this.workLocationCode };
    this.service.postData(data, methodname).subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.receipts = data.Details;
        console.log(this.receipts);
        
      }
      this.gridApi.setRowData(this.receipts);
    },
      error => console.log(error));
  }

  ButtonClick(row) {
    let tokennumber = row.rowData.tokeN_ID;
    let whnumber = row.rowData.warehousE_ID;
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = row.rowData.warehousE_ID;
    req.INPUT_02 = row.rowData.tokeN_ID;

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = 'WEB';
    this.service.postData(req, "SavePendingWHMemosDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        console.log(data);
        this.GetWHReceipts();
        this.DownloadPdf(tokennumber,whnumber);
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => {
        this.showloader = false;
        console.log(error)}
        );
  }

  DownloadReceipt(row) {
    if (row.rowData.tokeN_ID)
    this.DownloadPdf(row.rowData.tokeN_ID,row.rowData.warehousE_CODE);
  }

  DownloadPdf(receptno, whname) {
    let reciptnumber = receptno;
    this.showloader = true;
    var data = { "DIRECTION_ID": "12", "TYPEID": "ACK_PDF_GENERATION", "INPUT_01": whname, "INPUT_02": receptno };
    this.service.postData(data, "APSWCMapsServiceConsume").subscribe(data => {
      this.showloader = false;
      if (data.Status == "Success") {
        var b = this.service.s_sd(data.CertIssuerData.Base64pdf, 'application/pdf');
        var l = document.createElement('a');
        l.href = window.URL.createObjectURL(b);
        l.download = "Weght_Check_Memo" + reciptnumber + ".pdf";
        document.body.appendChild(l);
        l.click();
        document.body.removeChild(l);

        //console.log(data.Details);Base64pdf
        //this.fillPDFData(data.Details[0])
      }
      else
        Swal.fire('warning', data.Reason, 'warning');

    },
      error => console.log(error));
  }

}
