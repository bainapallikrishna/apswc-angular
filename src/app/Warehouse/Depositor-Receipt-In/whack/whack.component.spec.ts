import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhackComponent } from './whack.component';

describe('WhackComponent', () => {
  let component: WhackComponent;
  let fixture: ComponentFixture<WhackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
