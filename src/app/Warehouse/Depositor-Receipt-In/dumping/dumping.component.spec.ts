import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DumpingComponent } from './dumping.component';

describe('DumpingComponent', () => {
  let component: DumpingComponent;
  let fixture: ComponentFixture<DumpingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DumpingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DumpingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
