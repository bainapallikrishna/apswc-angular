import { Component, OnInit,ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';

@Component({
  selector: 'app-dumping',
  templateUrl: './dumping.component.html',
  styleUrls: ['./dumping.component.css']
})
export class DumpingComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridAngular;
  showloader: boolean = false;
  isSubmit: boolean = false;
 
  DepForm: FormGroup;
  tokens: any = [];
  tokeninfo: any = [];

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  isTable:boolean = false;
  gridColumnApi: any = [];
  gridApi: any = [];
  public rowseledted: object;
  frameworkComponents: any;
  public components;
  godowns:any=[];
  comartments:any=[];
  public defaultColDef;

  columnDefs = [
    { headerName: '#', width: 60,floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID', width: 100, field: 'tokeN_ID' ,onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Token Id"
    },
    { headerName: 'Depositor Name', width: 200, field: 'regisratioN_NAME'},
    { headerName: 'Reservation ID', width: 120, field: 'bookinG_ID' },
    { headerName: 'Commodity Group', width: 150, field: 'commoditY_GROUP' },
    { headerName: 'Commodity Name', width: 200, field: 'commodity' },
    { headerName: 'Contract Type', width: 100, field: 'contracT_TYPE_NAME' },
    // { headerName: 'Quantity Type', width: 250, field: 'quantitY_TYPE_NAME' },
    // { headerName: 'No Of Bags/Weight', width: 250, field: 'nO_OF_BAGS' },   
    { headerName: 'Mode Of Transport', width: 150, field: 'modE_OF_TRANSPORT_NAME' },
    { headerName: 'Vehicle No', width: 100, field: 'vehiclE_NUMBER' }

  ];


  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.DepForm = this.formBuilder.group({
      GodownName: [null, Validators.required],
      CompartmentName: [null, Validators.required],
    });

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText:true,
      autoHeight:true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }

  ngOnInit(): void {
    
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.LoadTokens();
    this.LoadGodowns();
  }


  get dep() { return this.DepForm.controls; }

  LoadTokens() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetDumpingTokens").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {
        
        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);
        
      }
    },
      error => console.log(error));
  }
  
  makeCellClicked(event) {
        
    if(event.node.selected==true){
      this.rowseledted={};    
      this.isTable = true;
      
      this.rowseledted = event.node.data;
      
    }
    

  
  }

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
    //window.alert('selection changed, ' + rowCount + ' rows selected');
  }

  LoadGodowns() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetGodowns").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.godowns = data.Details;
      }
    },
      error => console.log(error));
  }

  GetCompartmentInfo(event) {
    this.comartments = [];
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode, "INPUT_02": event }

    this.service.postData(obj, "GetCompartments").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.comartments = data.Details;
      }
    },
      error => console.log(error));

  }



  SaveDumpingDetails(stype){
    const savetype:string = stype;
    this.isSubmit = true;

    // stop here if form is invalid
    if (this.DepForm.invalid) {
      return false;
    }

    this.showloader = true;
    const rowsleted=this.rowseledted as any;
    const req = new InputRequest();
    req.INPUT_01 = rowsleted.tokeN_ID;//this.dep.TokenNo.value;
    req.INPUT_02 = this.workLocationCode;
    req.INPUT_03 = this.dep.GodownName.value;
    req.INPUT_04 = this.dep.CompartmentName.value;
    req.INPUT_05 = rowsleted.bookinG_ID;
    req.INPUT_06 = rowsleted.reservatioN_ID;
    req.INPUT_07 = rowsleted.commoditY_ID;
    
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveDumping").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Token : " + rowsleted.tokeN_ID + " , Dumping Deatils Saved Successfully !!!", 'success');
        if (savetype == 'save')
        this.reloadCurrentRoute();
      else
        this.router.navigate(['/Weighmentout']);

      }
      else
      Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

}
