import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OgateInComponent } from './ogate-in.component';

describe('OgateInComponent', () => {
  let component: OgateInComponent;
  let fixture: ComponentFixture<OgateInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OgateInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OgateInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
