import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ogate-in',
  templateUrl: './ogate-in.component.html',
  styleUrls: ['./ogate-in.component.css']
})
export class OgateInComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridAngular;
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  showloader: boolean = false;
  isSubmit: boolean = false;
  DepForm: FormGroup;


  columnDefs = [
    { headerName: '#', maxWidth: 60, floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID', maxWidth: 100, field: 'tokeN_ID',onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Token Id",
    },
    { headerName: 'Depositor Name', maxWidth: 200, field: 'name', },
    { headerName: 'Reservation ID', maxWidth: 200, field: 'bookinG_ID', },
    { headerName: 'Commodity Group', maxWidth: 200, field: 'commoditY_GROUP', },
    { headerName: 'Commodity Name', maxWidth: 200, field: 'commodity', },
    { headerName: 'Contract Type', maxWidth: 200, field: 'contracT_TYPE_NAME', },
    // { headerName: 'Quantity Type', maxWidth: 250, field: 'quantitY_TYPE_NAME',   },
    // { headerName: 'No Of Bags/Weight', maxWidth: 250, field: 'nO_OF_BAGS',   }


  ];

  DepositorrowData = [];

  dipositors: any = [];
  transports: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];
  frameworkComponents: any;
  public components;
  selbookids: string = "";
  seldepids: string = "";
  isTable: boolean = false;
  tokens: any = [];
  public rowseledted: object;
  public defaultColDef;

  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText:true,
      autoHeight:true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.DepForm = this.formBuilder.group({
      //DepositorName: [null, Validators.required],
      //GInDate: ['', Validators.required],
      //GInTime: ['', Validators.required],
      //Gatno: ['', Validators.required],
      MOTransport: [null, Validators.required],
      VehicleNo: ['', Validators.required],
      TruckType: [''],
      Waybill:['',Validators.required]
      // RegMobile: ['', [Validators.required, Validators.minLength(10), Validators.pattern('[6-9]\\d{9}')]],
      // PANNo: ['', [Validators.required, Validators.minLength(10), Validators.pattern(this.PanPattern)]],
      // NState: [null, Validators.required],
      // NDistrict: [null, Validators.required],
      // NArea: [null, Validators.required],
      // NMandal: [null, Validators.required],
      // NVillage: [null, Validators.required],
      // NHNO: ['', Validators.required],
      // NStrName: ['', Validators.required],
      // NLandmark: [''],
      // NPincode: ['', [Validators.required, Validators.minLength(6)]],

    });

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }

  ngOnInit(): void {
    let now: Date = new Date();
    let currdate = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();
    //this.dep.GInDate.setValue(currdate);
    this.LoadTrasnport();
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //params.api.setRowData(this.DepositorrowData);
    //this.gridApi.sizeColumnsToFit();
    this.LoadTokens()
  }

  LoadTokens() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetOtherReceiptInTokens").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));
  }

  makeCellClicked(event) {

    if (event.node.selected == true) {
      this.rowseledted = {};
      this.isTable = true;

      this.rowseledted = event.node.data;

    }



  }

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
    //window.alert('selection changed, ' + rowCount + ' rows selected');
  }

  get dep() { return this.DepForm.controls; }

  getSelectedRows(): void {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    this.selbookids = selectedData.map(node => `${node.bookinG_ID}`).join(',');
    this.seldepids = selectedData.map(node => `${node.registereD_ID}`).join(',');
  }

  LoadDepositorList() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetSpaceReservationDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.dipositors = data.Details;
        this.gridApi.setRowData(this.dipositors);
      }
      else
        this.gridApi.setRowData(this.dipositors);

    },
      error => console.log(error));
  }

  LoadTrasnport() {
    this.showloader = true;
    this.service.getData("GetTransportDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.transports = data.Details;
      }
    },
      error => console.log(error));
  }

  SaveGateInDetails(stype) {
    const savetype: string = stype;
    this.getSelectedRows();
    this.isSubmit = true;

    // stop here if form is invalid
    if (this.DepForm.invalid) {
      return false;
    }

    if (!this.selbookids) {
      Swal.fire('warning', "Please Select at least One Depositor", 'warning');
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    const rowsleted = this.rowseledted as any;
    req.INPUT_01 = rowsleted.tokeN_ID;//this.dep.TokenNo.value;
    req.INPUT_02 = this.workLocationCode;//wh id
    req.INPUT_03 = this.dep.VehicleNo.value;
    req.INPUT_04 = this.dep.MOTransport.value;
    req.INPUT_05 = 'LOADED' //this.dep.TruckType.value;
    req.INPUT_06 = "1";//Receipt In
    req.INPUT_07 = rowsleted.bookinG_ID;//Booking id      
    req.INPUT_08 = rowsleted.registereD_ID;//Farmer id
    req.INPUT_09 = rowsleted.registratioN_TYPE;//registration type
    req.INPUT_10 = rowsleted.commodity_id;//commodity_id
    req.INPUT_12= this.dep.Waybill.value;
    
    
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveGateIn").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Token : " + rowsleted.tokeN_ID + ", Gate IN Deatils Saved Successfully !!!", 'success');
        if (savetype == 'save')
          this.reloadCurrentRoute();
        else
          this.router.navigate(['/OQuality']);

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
}
