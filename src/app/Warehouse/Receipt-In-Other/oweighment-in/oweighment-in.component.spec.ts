import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OweighmentInComponent } from './oweighment-in.component';

describe('OweighmentInComponent', () => {
  let component: OweighmentInComponent;
  let fixture: ComponentFixture<OweighmentInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OweighmentInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OweighmentInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
