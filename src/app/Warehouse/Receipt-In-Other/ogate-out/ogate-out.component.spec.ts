import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OgateOutComponent } from './ogate-out.component';

describe('OgateOutComponent', () => {
  let component: OgateOutComponent;
  let fixture: ComponentFixture<OgateOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OgateOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OgateOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
