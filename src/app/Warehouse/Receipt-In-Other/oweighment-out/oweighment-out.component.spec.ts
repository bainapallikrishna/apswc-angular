import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OweighmentOutComponent } from './oweighment-out.component';

describe('OweighmentOutComponent', () => {
  let component: OweighmentOutComponent;
  let fixture: ComponentFixture<OweighmentOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OweighmentOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OweighmentOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
