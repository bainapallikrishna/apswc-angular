import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OqualityCheckComponent } from './oquality-check.component';

describe('OqualityCheckComponent', () => {
  let component: OqualityCheckComponent;
  let fixture: ComponentFixture<OqualityCheckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OqualityCheckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OqualityCheckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
