import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OstackInComponent } from './ostack-in.component';

describe('OstackInComponent', () => {
  let component: OstackInComponent;
  let fixture: ComponentFixture<OstackInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OstackInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OstackInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
