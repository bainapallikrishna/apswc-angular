import { Component, OnInit,ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';

@Component({
  selector: 'app-ostack-in',
  templateUrl: './ostack-in.component.html',
  styleUrls: ['./ostack-in.component.css']
})
export class OstackInComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  showloader: boolean = false;
  isSubmit: boolean = false;
  DepForm: FormGroup;
  tokens: any = [];
  commodities: any = [];
  godowns:any=[];
  comartments:any=[];
  stacktypes:any=[];
  stacks:any=[];

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  isTable:boolean = false;
  gridColumnApi: any = [];
  gridApi: any = [];
  public rowseledted: object;
  frameworkComponents: any;
  public components;
  public defaultColDef;
  htcomlist: string[] = [];
  htserlist: string[] = [];
  htpriclist: string[] = [];

  columnDefs = [
    { headerName: '#',  width: 60, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID',  width: 100, field: 'tokeN_ID',onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Token Id", 
    },
    { headerName: 'Depositor Name',  width: 200, field: 'regisratioN_NAME',   },
    { headerName: 'Reservation ID',  width: 100, field: 'bookinG_ID',   },
    { headerName: 'Commodity Group',  width: 150, field: 'commoditY_GROUP',   },
    { headerName: 'Commodity Name',  width: 200, field: 'commodity',   },
    { headerName: 'Contract Type',  width: 150, field: 'contracT_TYPE_NAME',   },
    // { headerName: 'Quantity Type',  width: 250, field: 'quantitY_TYPE_NAME',   },
    // { headerName: 'No Of Bags/Weight',  width: 250, field: 'nO_OF_BAGS',   },   
    { headerName: 'Mode Of Transport',  width: 150, field: 'modE_OF_TRANSPORT_NAME',   },
    { headerName: 'Vehicle No',  width: 100, field: 'vehiclE_NUMBER',   }

  ];

  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText: true,
      autoHeight: true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };
    
    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
    
    this.DepForm = this.formBuilder.group({
      CommodityCode: [''],
      TokenNo: [''],
      CommodityName: [''],
      DepositorName: [''],
      ContractType: [''],
      MOT: [''],
      VehicleNO: [''],
      HTCompanyName: [null, Validators.required],
      ServiceName: [null],
      BagWeight: ['', Validators.required],
      ServicePrice: [null],
      MarketValue: [null, Validators.required],
      GunnyBags: [''],
      stackings: new FormArray([
        this.formBuilder.group({
          GodownName: [null, Validators.required],
          CompartmentName: [null, Validators.required],
          StackType: [null, Validators.required],
          StackName: [null, Validators.required],
          BagsCnt: ['', Validators.required],
          VWeight: [''],
        })
      ])      
    });
  }

  ngOnInit(): void {
    this.LoadGodowns();
    this.LoadStackTypes();
    this.LoadHTCompanyServices();
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.LoadTokens();
  }

  get dep() { return this.DepForm.controls; }
  get s() { return this.dep.stackings as FormArray; }

  AddStack(){
    this.s.push(this.formBuilder.group({
      GodownName: [null, Validators.required],
          CompartmentName: [null, Validators.required],
          StackType: [null, Validators.required],
          StackName: [null, Validators.required],
          BagsCnt: ['', Validators.required],
          VWeight: [''],
    }));
  }

  RemoveStack(index) {
    this.s.removeAt(index);
  }

  LoadTokens() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetOtherStackTokens").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {
        
        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);
        
      }
    },
      error => console.log(error));
  }
  
  LoadHTCompanyServices() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetWHHTContractors").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.htcomlist = data.Details;
        if (this.htcomlist.length == 1) {
          this.dep.HTCompanyName.setValue(data.Details[0].hT_REGISTRATION_ID);
          this.LoadHTServices(data.Details[0].hT_REGISTRATION_ID);
        }
      }
    },
      error => console.log(error));
  }

  LoadHTServices(event) {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode, "INPUT_02": "1", "INPUT_03": "Inward", "INPUT_04": event }

    this.service.postData(obj, "GetHTServicesList").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.htserlist = data.Details;
      }
    },
      error => console.log(error));
  }

  LoadGodowns() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetGodowns").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.godowns = data.Details;
      }
    },
      error => console.log(error));
  }

  LoadStackTypes() {
    this.showloader = true;

    this.service.getData("GetStackTypes").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.stacktypes = data.Details;
      }
    },
      error => console.log(error));
  }

  makeCellClicked(event) {
        
    if(event.node.selected==true){
      this.rowseledted={};    
      this.isTable = true;
      
      this.rowseledted = event.node.data;
      this.ClearBookingDatails();
    }
  }

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
  }

  GetPrice(event) {
    if (this.dep.ServiceName.value && this.dep.BagWeight.value) {
      this.htpriclist = [];
      this.showloader = true;
      let obj: any = { "INPUT_01": this.workLocationCode, "INPUT_02": "1", "INPUT_03": "Inward", "INPUT_04": this.dep.ServiceName.value, "INPUT_05": this.dep.BagWeight.value , "INPUT_06": this.dep.HTCompanyName.value}
  
      this.service.postData(obj, "GetHTServicesPrice").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.htpriclist = data.Details;
        }
      },
        error => console.log(error));
    }
    else {
      this.htpriclist = [];
    }
  }

  CalStackWeight(event,index){
    if(this.dep.BagWeight.value && this.s.value[index].BagsCnt){
      this.s.controls[index].patchValue({VWeight:((Number(this.dep.BagWeight.value) * Number(this.s.value[index].BagsCnt))/1000).toFixed(3)})
    }
    else
    this.s.controls[index].patchValue({VWeight:''})
  }


  GetTokenInfo(event) {
    this.ClearBookingDatails();
    this.dep.CommodityName.setValue(null);
    this.commodities = [];
    if (event) {
      this.showloader = true;
      let obj: any = { "INPUT_01": event }

      this.service.postData(obj, "GetStackTokenInfo").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.commodities = data.Details;
        }
      },
        error => console.log(error));
    }
  }

  GetCommodityInfo(event){
    this.ClearBookingDatails();
    const data = this.commodities.filter(x=> x.bookinG_ID == event);
    if(data[0]){
      this.FillBookingDatails(data[0]);
    }
  }

  FillBookingDatails(data){
    this.dep.CommodityCode.setValue(data.commoditY_ID);
    this.dep.DepositorName.setValue(data.registratioN_NAME);
    this.dep.ContractType.setValue(data.contracT_TYPE_NAME);
    this.dep.MOT.setValue(data.modE_OF_TRANSPORT_NAME);
    this.dep.VehicleNO.setValue(data.vehiclE_NUMBER);
  }

  ClearBookingDatails(){
    this.DepForm.reset();
    this.htserlist = [];
    this.htpriclist = [];
    this.comartments = [];
    this.stacks = [];
    this.s.reset();
    this.s.clear();

    this.s.push(this.formBuilder.group({
      GodownName: [null, Validators.required],
      CompartmentName: [null, Validators.required],
      StackType: [null, Validators.required],
      StackName: [null, Validators.required],
      BagsCnt: ['', Validators.required],
      VWeight: [''],
    }));

  }

  GetCompartmentInfo(event,ind){
    this.comartments = [];
    this.stacks = [];

    this.s.controls[ind].patchValue({
      CompartmentName: null,
      StackType: null,
      StackName: null,
      BagsCnt: '',
      VWeight: '',
    });

    if (event) {
      this.showloader = true;
      let obj: any = { "INPUT_01": this.workLocationCode ,  "INPUT_02": event }

      this.service.postData(obj, "GetCompartments").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.comartments = data.Details;
        }
      },
        error => console.log(error));
    }
  }

  GetStacksInfo(event,ind){
    this.stacks = [];
    this.s.controls[ind].patchValue({
      StackType: null,
      StackName: null,
      BagsCnt: '',
      VWeight: '',
    });

    if (event) {
      this.showloader = true;
      let obj: any = { "INPUT_01": this.workLocationCode , "INPUT_02": this.s.value[ind].GodownName, "INPUT_03": event }

      this.service.postData(obj, "GetStacks").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.stacks = data.Details;
        }
      },
        error => console.log(error));
    }
  }

  SaveStacking(stype){
    this.isSubmit = true;
    const savetype:string = stype;
    // stop here if form is invalid
    if (this.DepForm.invalid) {
      return false;
    }

    if(this.dep.HTCompanyName.value != 'None' && !this.dep.ServiceName.value){
      Swal.fire('warning', "Please Select Service Name", 'warning');
      return false;
    }

    if(this.dep.HTCompanyName.value != 'None' && !this.dep.ServicePrice.value){
      Swal.fire('warning', "Please Select Service Price", 'warning');
      return false;
    }

    let godlist: string[] = [];
    let comlist: string[] = [];
    let stypelist: string[] = [];
    let staclist: string[] = [];
    let bagslist: string[] = [];
    let weilist: string[] = [];

    for (let i = 0; i < this.s.length; i++) {

      godlist.push(this.s.value[i].GodownName);
      comlist.push(this.s.value[i].CompartmentName);
      stypelist.push(this.s.value[i].StackType);
      staclist.push(this.s.value[i].StackName);
      bagslist.push(this.s.value[i].BagsCnt);
      weilist.push(this.s.value[i].VWeight);
    }

    this.showloader = true;
    const req = new InputRequest();
    const rowsleted=this.rowseledted as any;
    const bcount:string = rowsleted.booking_count;
    req.INPUT_01 = this.workLocationCode;
    req.INPUT_02 = rowsleted.tokeN_ID;  
    req.INPUT_03 = rowsleted.bookinG_ID
    req.INPUT_04 = rowsleted.commodity_id;
    req.INPUT_05 = godlist.join(',');
    req.INPUT_06 = comlist.join(',');
    req.INPUT_07 = staclist.join(',');
    req.INPUT_08 = stypelist.join(',');
    req.INPUT_09 = bagslist.join(',');
    req.INPUT_10 = weilist.join(',');
    req.INPUT_11 = "1";//Receipt In
    req.INPUT_12 = rowsleted.reservatioN_ID;
    req.INPUT_13 = rowsleted.registratioN_TYPE;
    req.INPUT_16 = this.dep.HTCompanyName.value;
    req.INPUT_17 = this.dep.ServiceName.value;
    req.INPUT_18 = "1";
    req.INPUT_19 = this.dep.BagWeight.value;
    req.INPUT_20 = this.dep.ServicePrice.value;
    req.INPUT_21 = this.dep.MarketValue.value;
    req.INPUT_22 = this.dep.GunnyBags.value;
    req.INPUT_23 = "Inward";
    
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveStacking").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Token : " + rowsleted.tokeN_ID + " , Stacking Deatils Saved Successfully !!!", 'success');
        if(savetype == 'next' && bcount == '1')
        this.router.navigate(['/OWeighmentOut']);
        else
        this.reloadCurrentRoute();
      }
      else
      Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

}
