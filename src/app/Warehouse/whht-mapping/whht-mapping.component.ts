import { HttpEventType } from '@angular/common/http';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbDatepickerConfig } from '@ng-bootstrap/ng-bootstrap';
import { AgGridAngular } from 'ag-grid-angular';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CustomDateParserFormatter } from 'src/app/custome-directives/dateformat';
import { WHButtonRendererComponent } from 'src/app/custome-directives/whbutton-renderer.component';
import { InputRequest } from 'src/app/Interfaces/employee';
import { AadharValidateService } from 'src/app/Services/aadhar-validate.service';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-whht-mapping',
  templateUrl: './whht-mapping.component.html',
  styleUrls: ['./whht-mapping.component.css']
})
export class WhhtMappingComponent implements OnInit {

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");

  showloader: boolean = true;
  modeltitle: string = "Add";
  isAdmin: boolean = false;
  isEdit: boolean = false;
  frameworkComponents: any;
  public components;
  defaultColDef: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];
  HTMappingForm: FormGroup;
  isSubmit: boolean = false;
  states: any = [];
  districts: any = [];
  urlist: any = [];
  nmandals: any = [];
  nvillages: any = [];
  public Emp_mob_isvalid: boolean = true;
  oursources: any = [];
  wh_history: any = [];
  htlist: any = [];
  hmlist: any = [];
  tslist: any = [];

  progress: number = 0;
  message: string = "";
  UploadDocType: string = "";
  UploadDocPath: string = "";

  PanPattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
  GstPattern = "^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$";
  IFSCPattern = "^[A-Z]{4}0[A-Z0-9]{6}$";

  vcomname: string = "";
  voffname: string = "";
  vmobile: string = "";
  vemail: string = "";
  vstartdate: string = "";
  venddate: string = "";
  vpanno: string = "";
  vgstno: string = "";
  vstate: string = "";
  vdistrict: string = "";
  varea: string = "";
  vmandal: string = "";
  vvillage: string = "";
  vhno: string = "";
  vstreet: string = "";
  vlanmark: string = "";
  vpincode: string = "";
  vdocumentpath: string = "";
  vastatus: string = "";
  vremarks: string = "";
  vofficals: any = [];
  vhamalies: any = [];
  vtransports: any = [];

  columnDefs = [
    { headerName: '#', width: 60, cellRenderer: 'rowIdRenderer' },
    { headerName: 'Warehouse Name', width: 200, field: 'warehousE_NAME', cellRenderer: 'actionrender' },
    { headerName: 'Warehouse Address', width: 200, field: 'address' },
    { headerName: 'Regional Office', width: 120, field: 'region' },
    { headerName: 'Warehouse Type', width: 120, field: 'warehousE_TYPE' },
    { headerName: 'Total Storage Capacity', width: 170, field: 'totaL_STORAGE' },
    { headerName: 'Status', width: 100, field: 'iS_ACTIVE' },
    { headerName: 'Action',width: 70, cellRenderer: 'buttonRenderer'}
  ];

  constructor(private service: CommonServices, private router: Router, private formBuilder: FormBuilder, private uidservice: AadharValidateService, private datef: CustomDateParserFormatter, private datecon: NgbDatepickerConfig) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
    }

    if (this.logUserrole == "101" || this.logUserrole == "102")
      this.isAdmin = true;

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText: true,
      autoHeight: true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.frameworkComponents = {
      buttonRenderer: WHButtonRendererComponent,
    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
      actionrender: function (params) {

        var eSpan3 = document.createElement('div');

        eSpan3.innerHTML = '<u style="color:blue" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + params.value + '</u>' +
          '<div class="dropdown-menu more-option-menu" aria-labelledby="dropdownMenuButton">' +
          '<span class="dropdown-item text-center EditClick"><i class="fa fa-pencil-square-o">Add</i></span>' +
          '<span class="dropdown-item text-center ViewClick"><i class="fa fa-eye">View</i></span>' +
          '</div>';

        return eSpan3;

      },
    };
  }

  dataclicked(params) {
    if (params.colDef.cellRenderer == "actionrender") {
      if (params.event.path[0].classList.value == "dropdown-item text-center EditClick" || params.event.path[0].classList.value == "fa fa-pencil-square-o") {
        this.EditHT(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center ViewClick" || params.event.path[0].classList.value == "fa fa-eye") {
        this.ViewHT(params);
      }
    }
    if (params.colDef.cellRenderer == "buttonRenderer") {
      this.HistoryHT(params);
    }
  }

  @ViewChild('agGrid') public agGrid: AgGridAngular;
  @ViewChild('editModal') public editModal: ModalDirective;
  @ViewChild('historyModal') public historyModal: ModalDirective;
  @ViewChild('viewModal') public viewModal: ModalDirective;

  ngOnInit(): void {
    let now: Date = new Date();

    this.HTMappingForm = this.formBuilder.group({
      HTComName: [null, Validators.required],
      StartDate: ['', Validators.required],
      EndDate: ['', Validators.required],
      WHCode: [''],
      hmservices: new FormArray([
        this.formBuilder.group({
          ServiceName: [null, Validators.required],
          HMPrice: ['', Validators.required],
          HMUptoKg: ['', Validators.required],
        })
      ]),
      tsservices: new FormArray([
        this.formBuilder.group({
          TrasportName: [null, Validators.required],
          TransportPrice: ['', Validators.required]
        })
      ]),
    });
    this.LoadContractors();
    this.LoadHMServices();
    this.LoadTSServices();
    this.LoadData();
  }

  get reg() { return this.HTMappingForm.controls; }
  get h() { return this.reg.hmservices as FormArray; }
  get t() { return this.reg.tsservices as FormArray; }

  AddHmService() {
    this.h.push(this.formBuilder.group({
      ServiceName: [null, Validators.required],
      HMPrice: ['', Validators.required],
      HMUptoKg: ['', Validators.required],
    }));
  }

  RemoveHmService(index) {
    this.h.removeAt(index);
  }

  AddTSService() {
    this.t.push(this.formBuilder.group({
      TrasportName: [null, Validators.required],
      TransportPrice: ['', Validators.required]
    }));
  }

  RemoveTSService(index) {
    this.t.removeAt(index);
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    params.api.setRowData(this.oursources);
    //this.gridApi.sizeColumnsToFit();

  }

  LoadData() {
    const req = new InputRequest();
    req.INPUT_01 = null;
    this.service.postData(req, "GetWH_List").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.oursources = data.Details;
        this.gridApi.setRowData(this.oursources);
      }
    },
      error => console.log(error));

  }

  mob_uid_validate(mobilephone) {
    if (mobilephone.length === 10)
      this.Emp_mob_isvalid = this.uidservice.validatemob(mobilephone);
  }

  AddHT() {
    this.modeltitle = "Add";
    this.resetallData();
    this.isSubmit = false;
    this.isEdit = false;
    this.progress = 0;
    this.message = "";
    this.AddHmService();
    this.editModal.show();
  }

  HistoryHT(row) {

    this.showloader = true;

    const req = new InputRequest();
    if (row == 'all') {
      this.isEdit = true;
      req.INPUT_01 = null;
    }
    else {
      this.isEdit = false;
      req.INPUT_01 = row.data.warehousE_CODE;
    }

    //req.INPUT_02 = "OUTSOURCING";

    this.service.postData(req, "GetHTMapHistoryDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.wh_history = data.Details;

        for (let i = 0; i < this.wh_history.length; i++) {
          let colname = this.wh_history[i].columN_NAME;
          let oldvalue = this.wh_history[i].olD_VALUE;
          let newvalue = this.wh_history[i].neW_VALUE;
          if ((colname == "AGREMENT_DOCUMENT") && oldvalue) {
            this.decryptHistoryFile(oldvalue, i, 'oldvalue');
          }
          if ((colname == "AGREMENT_DOCUMENT") && newvalue) {
            this.decryptHistoryFile(newvalue, i, 'newvalue');
          }
        }

        this.historyModal.show();
      }

    },
      error => console.log(error));


  }

  EditHT(row) {

    this.isEdit = false;
    this.isSubmit = false;
    this.modeltitle = "Add";
    this.resetallData();
    this.AddHmService();
    this.AddTSService();
    this.reg.WHCode.setValue(row.data.warehousE_CODE);
    //this.GetContractDetails(row, 'edit');
    this.editModal.show();
  }

  ViewHT(row) {
    this.GetContractDetails(row, 'view');
  }

  GetContractDetails(row, ptype: string) {
    this.showloader = true;
    const rtype: string = ptype;
    const req = new InputRequest();
    req.INPUT_01 = row.data.warehousE_CODE;

    this.service.postData(req, "GetHTMappingDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (rtype == 'edit')
          this.FillEditContractDetails(data.Details);
        else if (rtype == 'view')
          this.FillViewContractDetails(data.Details);
      }
      else {
        Swal.fire('warning', data.StatusMessage, 'warning');
      }

    },
      error => console.log(error));
  }


  resetallData() {
    this.HTMappingForm.reset();
    this.h.clear();
    this.h.reset();
    this.t.clear();
    this.t.reset();
    this.progress = 0;
    this.message = "";
    this.UploadDocType = "";
    this.UploadDocPath = "";
  }

  FillEditContractDetails(data) {
    this.reg.HTCode.setValue(data.hT_REGISTRATION_ID)
    this.reg.HTComName.setValue(data.contacT_NAME);
    this.reg.OffiName.setValue(data.officiaL_NAME);
    this.reg.CMobile.setValue(data.mobileno);
    this.reg.CEmail.setValue(data.emaiL_ADDRESS);
    this.reg.StartDate.setValue(this.datef.parse((data.starT_DATE ? data.starT_DATE : "").replace("T00:00:00", "")))
    this.reg.EndDate.setValue(this.datef.parse((data.enD_DATE ? data.enD_DATE : "").replace("T00:00:00", "")))
    this.reg.PANNo.setValue(data.paN_NO);
    this.reg.GSTNo.setValue(data.gstiN_NO);
    this.reg.NState.setValue(data.statE_CODE);
    this.reg.NDistrict.setValue(data.districT_CODE)
    this.reg.NArea.setValue(data.ruraL_URBAN)

    this.DistrictChange();
    this.reg.NMandal.setValue(data.mmC_CODE);

    this.MandalChange();
    this.reg.NVillage.setValue(data.warD_VILLAGE_CODE);

    this.reg.NHNO.setValue(data.dooR_NO);
    this.reg.NStrName.setValue(data.streaT_NAME);
    this.reg.NLandmark.setValue(data.lanD_MARK);
    this.reg.NPincode.setValue(data.pincode);
    this.reg.oldOutDoc.setValue(data.agremenT_DOCUMENT);
    this.reg.IsOutActive.setValue(data.iS_ACTIVE)
    this.reg.OutRemarks.setValue(data.remarks)

    this.editModal.show();
  }

  FillViewContractDetails(data) {

    this.vhamalies = data.filter(x => x.hamalieS_TRANSPORT_ID == 1);
    this.vtransports = data.filter(x => x.hamalieS_TRANSPORT_ID == 2);

    this.vcomname = data[0].contacT_NAME;
    this.vstartdate = data[0].starT_DATE;
    this.venddate = data[0].enD_DATE;

    this.viewModal.show();
  }

  CloseEditModal() {
    this.LoadData();
    this.editModal.hide();
  }

  SubmitHTDetails() {
    this.isSubmit = true;

    // stop here if form is invalid
    if (this.HTMappingForm.invalid) {
      return false;
    }


    if (this.datef.GreaterDate(this.reg.StartDate.value, this.reg.EndDate.value)) {
      Swal.fire('warning', "End Date Should be greater than Start Date", 'warning');
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();

    let seridlist: string[] = [];
    let sertypelist: string[] = [];
    let serpricelist: string[] = [];
    let serweitlist: string[] = [];
    let sercatlist: string[] = [];

    for (let i = 0; i < this.h.length; i++) {
      seridlist.push(this.h.value[i].ServiceName.split(":")[0]);
      sertypelist.push(this.h.value[i].ServiceName.split(":")[1]);
      sercatlist.push(this.h.value[i].ServiceName.split(":")[2]);
      serpricelist.push(this.h.value[i].HMPrice);
      serweitlist.push(this.h.value[i].HMUptoKg);
    }
    for (let i = 0; i < this.t.length; i++) {
      seridlist.push(this.t.value[i].TrasportName.split(":")[0]);
      sertypelist.push(this.t.value[i].TrasportName.split(":")[1]);
      sercatlist.push(this.t.value[i].TrasportName.split(":")[2]);
      serpricelist.push(this.t.value[i].TransportPrice);
      serweitlist.push("");
    }

    req.INPUT_01 = this.reg.WHCode.value;
    req.INPUT_02 = this.reg.HTComName.value.split(":")[0];
    req.INPUT_03 = sercatlist.join(',');  //this.reg.HTComName.value.split(":")[1];
    req.INPUT_04 = this.datef.format(this.reg.StartDate.value);
    req.INPUT_05 = this.datef.format(this.reg.EndDate.value);
    req.INPUT_06 = seridlist.join(',');
    req.INPUT_07 = serpricelist.join(',');
    req.INPUT_08 = serweitlist.join(',');
    req.INPUT_09 = sertypelist.join(',');

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveWHHTMapping").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Warehouse And H&T Contractor Mapping Details Saved Successfully !!!", 'success');
        this.editModal.hide();
        this.LoadData();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));


  }

  UpdateHTDetails() {
    this.isSubmit = true;
    // stop here if form is invalid
    if (this.HTMappingForm.invalid) {
      return false;
    }

    if (!this.Emp_mob_isvalid) {
      return false;
    }

    if (this.reg.IsOutActive.value == "0" && !this.reg.OutRemarks.value) {
      Swal.fire('warning', "Remarks is Required ", 'warning');
      return false;
    }

    if (this.datef.GreaterDate(this.reg.StartDate.value, this.reg.EndDate.value)) {
      Swal.fire('warning', "End Date Should be greater than Start Date", 'warning');
      return false;
    }

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.reg.NHNO.value;
    req.INPUT_02 = this.reg.NState.value;
    req.INPUT_03 = this.reg.NDistrict.value;
    req.INPUT_04 = this.reg.NMandal.value;
    req.INPUT_05 = this.reg.NVillage.value;
    req.INPUT_06 = this.reg.NArea.value;
    req.INPUT_07 = this.reg.NStrName.value;
    req.INPUT_08 = this.reg.NLandmark.value;
    req.INPUT_09 = this.reg.NPincode.value;
    req.INPUT_10 = this.UploadDocPath ? this.UploadDocPath : this.reg.oldOutDoc.value;
    req.INPUT_11 = this.reg.CMobile.value;
    req.INPUT_12 = this.reg.CEmail.value;
    req.INPUT_13 = this.reg.HTCode.value;
    req.INPUT_14 = this.reg.OutRemarks.value;
    req.INPUT_15 = this.reg.IsOutActive.value;


    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "UpdateHTDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Outsourcing Agency Details Updated Successfully !!!", 'success');
        this.LoadData();
        this.editModal.hide();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  uploadFile(event) {

    this.message = "";
    this.UploadDocType = "";
    this.UploadDocPath = "";
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF') {
        Swal.fire('info', 'Please uploadpdf files only', 'info');
        return false;
      }

      if (filetype != 'image/jpeg' || filetype != 'image/png')
        doctype = 'IMAGE';
      else
        doctype = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();


      formData.append('file', fileToUpload, fileToUpload.name);

      formData.append('regid', "HTContracts");
      formData.append('category', this.reg.PANNo.value);
      formData.append('pagename', "HTContracts");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          if (event.type === HttpEventType.UploadProgress)

            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.';
            this.UploadDocType = doctype;

            this.uploadFinished(event.body);
          }

        });
    }
  }

  public createImgPath = (serverPath: string) => {

    return `${this.service.filebaseUrl.replace("api/FilesUpload", "")}${serverPath}`;

  }

  public uploadFinished = (event) => {
    this.UploadDocPath = event.fullPath;
  }

  LoadContractors() {
    const req = new InputRequest();
    this.service.postData(req, "GetHTContractorsDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.htlist = data.Details;
      }
    },
      error => console.log(error));
  }

  LoadHMServices() {
    const req = new InputRequest();
    req.INPUT_01 = "1";
    this.service.postData(req, "GetHTServicesDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.hmlist = data.Details;
      }
    },
      error => console.log(error));
  }

  LoadTSServices() {
    const req = new InputRequest();
    req.INPUT_01 = "2";
    this.service.postData(req, "GetHTServicesDetails").subscribe(data => {
      if (data.StatusCode == "100") {
        this.tslist = data.Details;
      }
    },
      error => console.log(error));
  }

  DistrictChange() {
    this.nmandals = [];
    this.nvillages = [];
    this.reg.NMandal.setValue(null);
    this.reg.NVillage.setValue(null);
    if (this.reg.NDistrict.value && this.reg.NArea.value) {
      this.LoadMandals(this.reg.NDistrict.value, this.reg.NArea.value)
    }
  }

  LoadMandals(distval: string, areaval: string) {
    this.showloader = true;

    const req = new InputRequest();
    req.INPUT_01 = areaval;
    req.INPUT_02 = distval;

    this.service.postData(req, "GetMandlas").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.nmandals = data.Details;
      }

    },
      error => console.log(error));
  }

  MandalChange() {
    this.nvillages = [];
    this.reg.NVillage.setValue(null);
    if (this.reg.NDistrict.value && this.reg.NMandal.value) {
      this.LoadVillages(this.reg.NDistrict.value, this.reg.NMandal.value)
    }
  }

  LoadVillages(distval: string, manval: string) {
    this.showloader = true;
    let obj: any = { "INPUT_02": distval, "INPUT_03": manval }

    this.service.postData(obj, "GetVillages").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.nvillages = data.Details;
      }

    },
      error => console.log(error));
  }

  viewPDF(input): void {
    let pdfWindow = window.open('', '_blank')
    pdfWindow.document.write(`<iframe width='100%' height='100%' src='${encodeURI(input)}'></iframe>`)
  }

  decryptFile(filepath, vtype) {
    const docvtype = vtype;
    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (docvtype == 'view')
            this.vdocumentpath = data.docBase64;
        },
          error => {
            console.log(error);

          });
    }

  }

  decryptHistoryFile(filepath, i, ftype) {
    const coltype: string = ftype;
    const indx = i;
    if (coltype == 'oldvalue')
      this.wh_history[indx].olD_VALUE = "";
    else
      this.wh_history[indx].neW_VALUE = "";

    if (filepath) {
      this.service.decryptFile(filepath, "DecryptFile")
        .subscribe(data => {

          if (coltype == 'oldvalue')
            this.wh_history[indx].olD_VALUE = data.docBase64;
          else
            this.wh_history[indx].neW_VALUE = data.docBase64;
        })
    }
  };

}
