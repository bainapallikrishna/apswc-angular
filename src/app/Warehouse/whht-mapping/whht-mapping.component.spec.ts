import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhhtMappingComponent } from './whht-mapping.component';

describe('WhhtMappingComponent', () => {
  let component: WhhtMappingComponent;
  let fixture: ComponentFixture<WhhtMappingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhhtMappingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhhtMappingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
