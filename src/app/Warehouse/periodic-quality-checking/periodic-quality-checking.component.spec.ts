import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PeriodicQualityCheckingComponent } from './periodic-quality-checking.component';

describe('PeriodicQualityCheckingComponent', () => {
  let component: PeriodicQualityCheckingComponent;
  let fixture: ComponentFixture<PeriodicQualityCheckingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PeriodicQualityCheckingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PeriodicQualityCheckingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
