import { Component, OnInit, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AgGridAngular } from 'ag-grid-angular';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { HistorybuttonRendererComponent } from 'src/app/custome-directives/Historybutton-renderer.component';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-periodic-quality-checking',
  templateUrl: './periodic-quality-checking.component.html',
  styleUrls: ['./periodic-quality-checking.component.css']
})
export class PeriodicQualityCheckingComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  @ViewChild('PQCModal') public PQCModal: ModalDirective;
  @ViewChild('DisinfestationModal') public DisinfestationModal: ModalDirective;
  @ViewChild('DisiHistoryModal') public DisiHistoryModal: ModalDirective;
  @ViewChild('SpillageModal') public SpillageModal: ModalDirective;

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  @Output() public onUploadFinished = new EventEmitter();

  showloader: boolean = false;
  isSubmit: boolean = false;
  istable: boolean = false;
  QualtyForm: FormGroup;
  DisintForm: FormGroup;
  SpillageForm: FormGroup;
  Qparms: FormArray;

  comd_vartylist: any[];
  gradelist: any[];
  qualtypramlist: any[];
  comdtylist: any[];
  public rowseledted: object;
  DepositorrowData = [];

  dipositors: any = [];
  transports: any = [];
  chemicallist: any = [];
  dishistory: any = [];
  gridColumnApi: any = [];
  gridApi: any = [];
  frameworkComponents: any;
  public components;
  public defaultColDef;
  public icons;
  public progress: number;
  public message: string;

  response: { dbPath: "" };

  doc_change: boolean = false;

  tableDataSrc: any;
  tableCols: string[] = [];
  tableData: {}[] = []

  columnDefs = [
    { headerName: '#', width: 50, cellRenderer: 'rowIdRenderer', },
    { headerName: 'Depositor Name', width: 180, field: 'registratioN_NAME', cellRenderer: 'actionrender' },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID' },
    { headerName: 'Commodity Group', width: 150, field: 'commoditY_GROUP' },
    { headerName: 'Commodity Name', width: 150, field: 'commodity' },
    // { headerName: 'Contract Type', width: 180, field: 'contracT_TYPE_NAME'},
    { headerName: 'Godown Name', width: 100, field: 'godowN_NAME' },
    { headerName: 'Compartment Name', width: 150, field: 'compartmenT_NAME' },
    { headerName: 'Stack Name', width: 80, field: 'stacK_NAME' },
    { headerName: 'No Of Bags', width: 80, field: 'bags' },
    { headerName: 'Weight(MT)', width: 80, field: 'weight' },
  ];


  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }
    this.frameworkComponents = {
      buttonRenderer: HistorybuttonRendererComponent,
    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
      actionrender: function (params) {

        var eSpan3 = document.createElement('div');

        eSpan3.innerHTML = '<u style="color:blue" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' + params.value + '</u>' +
          '<div class="dropdown-menu more-option-menu" aria-labelledby="dropdownMenuButton">' +
          '<span class="dropdown-item text-center QCClick"><i class="fa fa-list-alt">Quality Checking</i></span>' +
          '<span class="dropdown-item text-center DesinClick"><i class="fa fa-spray-can">Disinfestation</i></span>' +
          '<span class="dropdown-item text-center DesinHisClick"><i class="fa fa-clock-o">Disinfestation History</i></span>' +
          '<span class="dropdown-item text-center SpillClick"><i class="fa fa-broom">Spillage</i></span>' +
          '</div>';

        return eSpan3;

      },
    };

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText: true,
      autoHeight: true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

  }

  dataclicked(params) {
    if (params.colDef.cellRenderer == "actionrender") {
      if (params.event.path[0].classList.value == "dropdown-item text-center QCClick" || params.event.path[0].classList.value == "fa fa-list-alt") {
        this.addPQC(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center DesinClick" || params.event.path[0].classList.value == "fa fa-spray-can") {
        this.adddisinfestation(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center DesinHisClick" || params.event.path[0].classList.value == "fa fa-clock-o") {
        this.disinfestationHistory(params);
      }
      if (params.event.path[0].classList.value == "dropdown-item text-center SpillClick" || params.event.path[0].classList.value == "fa fa-broom") {
        this.AddSpillage(params);
      }
    }
  }

  ngOnInit(): void {
    let now: Date = new Date();
    let currdate = now.getFullYear() + "-" + (now.getMonth() + 1) + "-" + now.getDate();

    this.QualtyForm = this.formBuilder.group({
      //Tknid: [null, Validators.required],
      //DepositorName: [null, Validators.required],
      commoditygroup: ['', Validators.required],
      //commodity: [null, Validators.required],
      grade: [null, Validators.required],
      Variety: [null, Validators.required],
      DocPath: [''],
      RegDoc: [''],
      Qparms: new FormArray([
        this.formBuilder.group({
          PramID: ['', Validators.required],
          PramName: ['', Validators.required],
          pramvalue: ['', Validators.required],
          pramentervalue: [''],


        })
      ])

    });
    this.DisintForm = this.formBuilder.group({
      Disitype: [null, Validators.required],
      ChemicalName: [null, Validators.required],
      ChemicalQuantity: ['', Validators.required],
      DocPath: [''],
      RegDoc: [''],

    });

    this.SpillageForm = this.formBuilder.group({
      SourceOfReceipt: ['', Validators.required],
      QuantityReceived: ['', Validators.required],
      ModeOfCleaning: ['', Validators.required],
      QuaRecAfterClean: ['', Validators.required],
      QtyDist: [''],
      GunnyBags: [''],
      Remarks: [''],

    });



    this.LoadChemicals();
  }


  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //params.api.setRowData(this.DepositorrowData);
    //this.gridApi.sizeColumnsToFit();
    this.LoadDepositorList()
  }

  LoadChemicals() {
    this.showloader = true;
    this.service.getData("GetChemicalTypes").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.chemicallist = data.Details;
      }
    },
      error => console.log(error));
  }

  CalDustQty(val) {
    const recqty = Number(this.spi.QuantityReceived.value);
    const cleanqty = Number(this.spi.QuaRecAfterClean.value);
    if (cleanqty >= recqty) {
      Swal.fire('info', "Quantity Received After Cleaning is Not Greater than Quantity Received !!!", 'info');
      this.spi.QuantityReceived.setValue("");
      this.spi.QuaRecAfterClean.setValue("");
      this.spi.QtyDist.setValue("");
      return false;

    }
    else {
      this.spi.QtyDist.setValue((recqty - cleanqty).toFixed(3))
    }
  }


  addPQC(rowsletedlist) {
    this.resetallforms();
    this.rowseledted = rowsletedlist.data;
    this.loadvarieties(this.rowseledted, "VARIETY");
    this.PQCModal.show();
  }
  adddisinfestation(rowsletedlist) {
    this.rowseledted = rowsletedlist.data;
    this.resetallforms();
    this.DisinfestationModal.show();

  }

  disinfestationHistory(rowsletedlist) {
    this.rowseledted = rowsletedlist.data;
    const data = rowsletedlist.data;
    const req = new InputRequest();

    req.INPUT_01 = data.warehousE_ID;
    req.INPUT_02 = data.godowN_ID;
    req.INPUT_03 = data.compartmenT_ID;
    req.INPUT_04 = data.stacK_ID;
    req.INPUT_05 = data.bookinG_ID;
    req.INPUT_06 = data.commoditY_ID;
    req.INPUT_07 = data.registratioN_ID;

    this.service.postData(req, "GetDisiHistory").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.dishistory = data.Details;
        console.log(this.dishistory);
        this.DisiHistoryModal.show();
      }

    },
      error => console.log(error));


  }

  AddSpillage(rowsletedlist) {
    this.rowseledted = rowsletedlist.data;
    this.resetallforms();
    this.SpillageModal.show();
  }

  resetallforms() {
    this.qualty.commoditygroup.setValue("");
    this.qualty.grade.setValue(null);
    this.qualty.Variety.setValue(null);
    this.comd_vartylist = [];
    this.gradelist = [];
    this.qualtypramlist = [];
    this.istable = false;
    this.isSubmit = false;

    this.QP.reset();
    this.QP.clear();
    this.QualtyForm.reset();
    this.DisintForm.reset();
    this.SpillageForm.reset();

    this.progress = 0;
    this.message = "";
    this.doc_change = false;
  }

  LoadDepositorList() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetPeriodicQCDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.dipositors = data.Details;
        this.gridApi.setRowData(this.dipositors);
      }
      else
        this.gridApi.setRowData(this.dipositors);

    },
      error => console.log(error));
  }

  loadvarieties(slctddata: any, val) {


    const req = new InputRequest();
    if (val == "VARIETY") {

      req.INPUT_01 = slctddata.commoditY_CODE.trim();
      req.INPUT_02 = slctddata.commoditY_ID.trim();
      req.INPUT_04 = val;
    }
    else {
      this.gradelist = [];
      req.INPUT_01 = slctddata.commoditY_CODE.trim();
      req.INPUT_02 = slctddata.commoditY_ID.trim();
      req.INPUT_03 = this.qualty.Variety.value.split(":")[0];
      req.INPUT_04 = val;
    }
    this.service.postData(req, "GetVariety_GradeList").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (val == "VARIETY")
          this.comd_vartylist = data.Details;
        else {
          this.gradelist = data.Details;

        }

      }
      else {
        //Swal.fire('warning', data.StatusMessage, 'warning');
        console.log(data.StatusMessage);
        this.showloader = false;
      }

    },

      error => console.log(error));
    this.showloader = false;

  }

  loadgrades(val) {

    this.gradelist = [];
    this.istable = false;
    this.qualtypramlist = [];
    this.qualty.grade.setValue(null);
    if (this.qualty.Variety.value) {
      this.showloader = true;
      this.loadvarieties(this.rowseledted, val)
    }
  }
  LoadQualtyprams() {

    this.qualty.commoditygroup.setValue("");
    const rowsleted = this.rowseledted as any;
    this.qualtypramlist = [];
    this.QP.reset();
    this.QP.clear();
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = rowsleted.commoditY_CODE.trim();
    req.INPUT_02 = rowsleted.commoditY_ID.trim();
    req.INPUT_03 = this.qualty.Variety.value.split(":")[0];
    req.INPUT_04 = this.qualty.grade.value.split(":")[0];
    this.qualty.commoditygroup.setValue(rowsleted.commoditY_GROUP);
    this.service.postData(req, "GetQualityPrameters").subscribe(data => {
      if (data.StatusCode == "100") {

        this.qualtypramlist = data.Details;
        this.AddQp();
        this.FillQPDetails(this.qualtypramlist);
        this.showloader = false;

      }
      else {
        Swal.fire('warning', data.StatusMessage, 'warning');
        this.showloader = false;
      }

    },

      error => console.log(error));

    this.showloader = true;
    const req1 = new InputRequest();
    req1.INPUT_01 = rowsleted.warehousE_ID.trim();
    req1.INPUT_02 = rowsleted.bookinG_ID.trim();
    req1.INPUT_03 = rowsleted.registratioN_ID.trim();
    req1.INPUT_04 = rowsleted.commoditY_ID.trim();
    req1.INPUT_05 = this.qualty.Variety.value.split(":")[0];
    req1.INPUT_06 = this.qualty.grade.value.split(":")[0];

    this.service.postData(req1, "GetQualityiHistory").subscribe(data => {
      this.tableCols = [];
      if (data.StatusCode == "100") {
        this.tableData = data.Details;
        for (var type in data.Details[0]) {
          this.tableCols.push(type);
        }
        this.tableDataSrc = new MatTableDataSource(this.tableData);

        this.showloader = false;

      }
      else {
        Swal.fire('warning', data.StatusMessage, 'warning');
        this.showloader = false;
      }

    },

      error => console.log(error));

    //this.showloader = false;
  }

  AddQp() {
    this.QP.push(this.formBuilder.group({
      PramID: ['', Validators.required],
      PramName: ['', Validators.required],
      pramvalue: ['', Validators.required],
      pramentervalue: [''],


    }));

  }


  FillQPDetails(QPdata: any) {
    for (let i = 0; i < QPdata.length; i++) {
      if (i != 0) {
        this.QP.push(this.formBuilder.group({
          PramID: ['', Validators.required],
          PramName: ['', Validators.required],
          pramvalue: ['', Validators.required],
          pramentervalue: [''],


        }));
      }

      this.QP.controls[i].patchValue({
        PramID: QPdata[i].qtY_ID,
        PramName: QPdata[i].qtY_NAME,
        pramvalue: QPdata[i].percentage,
      });

    }
    this.istable = true;
  }



  uploadFile(event) {
    if (event.target.files && event.target.files[0]) {
      let filetype = event.target.files[0].type;
      let filesize = event.target.files[0].size;
      let doctype = "";
      if (event.target.files.length === 0) {
        return false;
      }
      if (filetype != 'application/pdf' && filetype != 'application/PDF' && filetype != 'image/jpeg' && filetype != 'image/png') {
        Swal.fire('info', 'Please upload jpeg,jpg,png,pdf files only', 'info');
        return false;
      }

      if (filetype != 'image/jpeg' || filetype != 'image/png')
        doctype = 'IMAGE';
      else
        doctype = 'PDF';

      if (filesize > 2615705) {
        Swal.fire('info', 'File size must be upto 2 MB', 'info');
        return false;
      }

      let fileToUpload = <File>event.target.files[0];
      const formData = new FormData();

      formData.append('file', fileToUpload, fileToUpload.name);
      formData.append('pagename', "ReceiptInRequest");

      this.service.encryptUploadFile(formData, "EncryptFileUpload")
        .subscribe(event => {
          this.doc_change = true;
          if (event.type === HttpEventType.UploadProgress)
            this.progress = Math.round(100 * event.loaded / event.total);
          else if (event.type === HttpEventType.Response) {
            this.message = 'Upload success.'
            this.doc_change = false;
            this.uploadFinished(event.body);
          }

        });
    }
  }
  public uploadFinished = (event) => {
    this.qualty.DocPath.setValue(event.fullPath);
    this.dis.DocPath.setValue(event.fullPath);
  }


  get qualty() { return this.QualtyForm.controls; }
  get dis() { return this.DisintForm.controls; }
  get spi() { return this.SpillageForm.controls; }
  get QP() { return this.qualty.Qparms as FormArray; }


  SaveQualityChecking() {
    this.isSubmit = true;

    // stop here if form is invalid
    if (this.QualtyForm.invalid) {
      return false;
    }
    if (this.doc_change == true) {
      if (!this.qualty.DocPath.value) {

        Swal.fire('warning', "Please Upload Quality Checking File ", 'warning');
        return false;

      }
    }


    for (let i = 0; i < this.QP.length; i++) {
      if (!this.QP.value[i].pramentervalue) {
        Swal.fire('warning', "Please Enter All Quality Parameters", 'warning');
        return false;
      }
    }

    let QP_idlist: string[] = [];
    let QP_namelist: string[] = [];
    let QP_ACTUALlist: string[] = [];
    let QP_ENTERlist: string[] = [];

    for (let i = 0; i < this.QP.length; i++) {
      if (!this.QP.value[i].pramentervalue) {
        Swal.fire('warning', "Please Enter All Quality Parameters", 'warning');
        return false;
      }
      QP_idlist.push(this.QP.value[i].PramID);
      QP_namelist.push(this.QP.value[i].PramName);
      QP_ACTUALlist.push(this.QP.value[i].pramvalue);
      QP_ENTERlist.push(this.QP.value[i].pramentervalue);
    }

    const rowsleted = this.rowseledted as any;
    const req = new InputRequest();

    req.INPUT_01 = rowsleted.registratioN_ID;//regist id
    req.INPUT_02 = rowsleted.bookinG_ID;//booking id
    req.INPUT_03 = rowsleted.commoditY_ID;
    req.INPUT_04 = this.workLocationCode;//WHID
    req.INPUT_05 = rowsleted.commoditY_CODE;
    req.INPUT_06 = this.qualty.DocPath.value;
    req.INPUT_07 = QP_ENTERlist.join(',');
    req.INPUT_08 = QP_ACTUALlist.join(',');//percentage
    req.INPUT_09 = this.logUserrole;
    req.INPUT_10 = QP_idlist.join(',');
    req.INPUT_11 = "1";//PQC_Status
    req.INPUT_12 = this.qualty.Variety.value.split(":")[0];
    req.INPUT_13 = this.qualty.grade.value.split(":")[0];
    req.INPUT_14 = rowsleted.godowN_ID;//godown id
    req.INPUT_15 = rowsleted.compartmenT_ID;//compartment id
    req.INPUT_16 = rowsleted.stacK_ID;//statck id

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;
    this.service.postData(req, "SavePeriodicQualityChecking").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Quality Checking Details Saved Successfully !!!", 'success');
          this.PQCModal.hide();
          this.resetallforms();
          this.LoadDepositorList();

        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  Savedisinfestation() {
    this.isSubmit = true;

    // stop here if form is invalid
    if (this.DisintForm.invalid) {
      return false;
    }

    // if (this.doc_change == true) {
    //   if (!this.dis.DocPath.value) {
    //     Swal.fire('warning', "Please Upload Disinfestation File ", 'warning');
    //     return false;

    //   }
    // }

    const rowsleted = this.rowseledted as any;
    const req = new InputRequest();

    req.INPUT_01 = rowsleted.warehousE_ID;
    req.INPUT_02 = rowsleted.godowN_ID;
    req.INPUT_03 = rowsleted.compartmenT_ID;
    req.INPUT_04 = rowsleted.stacK_ID;
    req.INPUT_05 = rowsleted.bookinG_ID;
    req.INPUT_06 = rowsleted.commoditY_ID;
    req.INPUT_07 = rowsleted.registratioN_ID;
    req.INPUT_08 = rowsleted.bags;
    req.INPUT_09 = rowsleted.weight;
    req.INPUT_10 = this.dis.ChemicalName.value;
    req.INPUT_11 = this.dis.ChemicalQuantity.value;
    req.INPUT_12 = this.dis.Disitype.value;
    req.INPUT_13 = this.dis.DocPath.value;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;
    this.service.postData(req, "SaveDisinfestationDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Disinfestation Details Saved Successfully !!!", 'success');
          this.DisinfestationModal.hide();
          this.resetallforms();
          this.LoadDepositorList();
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  SaveSpillage() {
    this.isSubmit = true;

    // stop here if form is invalid
    if (this.SpillageForm.invalid) {
      return false;
    }

    const rowsleted = this.rowseledted as any;
    const req = new InputRequest();

    req.INPUT_01 = rowsleted.warehousE_ID;
    req.INPUT_02 = rowsleted.godowN_ID;
    req.INPUT_03 = rowsleted.compartmenT_ID;
    req.INPUT_04 = rowsleted.stacK_ID;
    req.INPUT_05 = rowsleted.bookinG_ID;
    req.INPUT_06 = rowsleted.commoditY_ID;
    req.INPUT_07 = rowsleted.registratioN_ID;
    req.INPUT_08 = rowsleted.bags;
    req.INPUT_09 = rowsleted.weight;
    req.INPUT_10 = this.spi.QuantityReceived.value;
    req.INPUT_11 = this.spi.QuaRecAfterClean.value;
    req.INPUT_12 = this.spi.QtyDist.value;
    req.INPUT_13 = this.spi.SourceOfReceipt.value;
    req.INPUT_14 = this.spi.ModeOfCleaning.value;
    req.INPUT_15 = this.spi.GunnyBags.value;
    req.INPUT_16 = this.spi.Remarks.value;

    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;
    this.service.postData(req, "SaveSpillingDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Spillage Details Saved Successfully !!!", 'success');
          this.SpillageModal.hide();
          this.resetallforms();
          this.LoadDepositorList();
        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

}


