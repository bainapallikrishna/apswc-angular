import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutGateOutComponent } from './out-gate-out.component';

describe('OutGateOutComponent', () => {
  let component: OutGateOutComponent;
  let fixture: ComponentFixture<OutGateOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutGateOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutGateOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
