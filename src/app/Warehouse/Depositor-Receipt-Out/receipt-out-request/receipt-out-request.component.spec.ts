import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReceiptOutRequestComponent } from './receipt-out-request.component';

describe('ReceiptOutRequestComponent', () => {
  let component: ReceiptOutRequestComponent;
  let fixture: ComponentFixture<ReceiptOutRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReceiptOutRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReceiptOutRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
