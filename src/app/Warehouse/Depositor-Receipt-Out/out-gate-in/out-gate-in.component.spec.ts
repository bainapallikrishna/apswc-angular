import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutGateInComponent } from './out-gate-in.component';

describe('OutGateInComponent', () => {
  let component: OutGateInComponent;
  let fixture: ComponentFixture<OutGateInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutGateInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutGateInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
