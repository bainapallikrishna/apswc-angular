import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-out-gate-in',
  templateUrl: './out-gate-in.component.html',
  styleUrls: ['./out-gate-in.component.css']
})
export class OutGateInComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  logUserrole:string = sessionStorage.getItem("logUserrole");
  isPasswordChanged:string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode:string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  showloader:boolean = false;
  isSubmit:boolean = false;
  DepForm:FormGroup;
  public rowseledted: object;
  public defaultColDef;
  columnDefs = [
    { headerName: '#', width: 60,floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID', width: 100, field: 'tokeN_ID',onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Token Id",sortable: true, filter: true
    },
    { headerName: 'Depositor ID', width: 100, field: 'registratioN_ID' , },
    { headerName: 'Depositor Name', width: 200, field: 'registratioN_NAME'  },
    { headerName: 'Commidity Name', width: 200, field: 'commodity'  },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID'  },
    { headerName: 'Stack In Date', width: 200, field: 'stacK_IN_DATE'  },
    { headerName: 'Contract Type', width: 150, field: 'contracT_TYPE_NAME'  },
    { headerName: 'No Of Bags', width: 100, field: 'bags'  },
    { headerName: "Weight(MT's)", width: 100, field: 'weight'  }

  ];

  DepositorrowData = [];

  dipositors:any=[];
  transports:any=[];
  gridColumnApi:any =[];
  gridApi:any=[];
  frameworkComponents: any;
  public components;
  selbookids:string="";
  seldepids:string="";
  seltokenids:string="";
  isTable:boolean=false;
  
  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder) { 
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText:true,
      autoHeight:true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.DepForm = this.formBuilder.group({
      //DepositorName: [null, Validators.required],
      //GInDate: ['', Validators.required],
      //GInTime: ['', Validators.required],
      //Gatno: ['', Validators.required],
      MOTransport: [null, Validators.required],
      VehicleNo: ['', Validators.required],
      trucksheetNo:['',Validators.required],
      TruckType: [''],
    });

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };
  }

  ngOnInit(): void {
    let now: Date = new Date();
    let currdate = now.getFullYear() +"-"+(now.getMonth() + 1)+"-"+now.getDate();
    //this.dep.GInDate.setValue(currdate);
    this.LoadTrasnport();
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    //params.api.setRowData(this.DepositorrowData);
    //this.gridApi.sizeColumnsToFit();
    this.LoadDepositorList()
  }

  makeCellClicked(event) {
    if (event.node.selected) {
      this.rowseledted = {};
      this.isTable = true;
      this.rowseledted = event.node.data;
    }
  }

  get dep() { return this.DepForm.controls; }

  getSelectedRows(): void {
    const selectedNodes = this.agGrid.api.getSelectedNodes();
    const selectedData = selectedNodes.map(node => node.data);
    this.selbookids = selectedData.map(node => `${node.bookinG_ID}`).join(',');
    this.seltokenids = selectedData.map(node => `${node.tokeN_ID}`).join(',');
    this.seldepids = selectedData.map(node => `${node.registratioN_ID}`).join(',');
  }

  LoadDepositorList(){
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetReceiptOutDetails").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        this.dipositors = data.Details;
        this.gridApi.setRowData(this.dipositors);
      }
      else 
      this.gridApi.setRowData(this.dipositors);

    },
      error => console.log(error));
  }

  LoadTrasnport() {
    this.showloader = true;
    this.service.getData("GetTransportDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.transports = data.Details;
      }
    },
      error => console.log(error));
  }

  SaveGateInDetails(stype) {
    this.getSelectedRows();
    this.isSubmit = true;
    const savetype : string = stype;
    if(!this.seltokenids){
      Swal.fire('warning', "Please Select the Token", 'warning');
      return false;
    }
    
    // stop here if form is invalid
    if (this.DepForm.invalid) {
      return false;
    }

    

    this.showloader = true;
    const req = new InputRequest();
    const rowsleted = this.rowseledted as any;
    req.INPUT_01 = this.seltokenids;
    req.INPUT_02 = this.workLocationCode;
    req.INPUT_03 = this.dep.VehicleNo.value;
    req.INPUT_04 = this.dep.MOTransport.value;
    req.INPUT_05 = 'EMPTY';//this.dep.TruckType.value;
    req.INPUT_06 = "2";
    req.INPUT_07 = this.selbookids;
    req.INPUT_08 = this.seldepids;
    req.INPUT_09=  rowsleted.registratioN_TYPE;//registration type
    req.INPUT_10 = rowsleted.commoditY_ID;//commodity_id
    req.INPUT_11 =this.dep.trucksheetNo.value;//commodity_id
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveGateIn").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Token : " + rowsleted.tokeN_ID + " , Gate IN Deatils Saved Successfully !!!", 'success');
        if(savetype == 'save')
          this.reloadCurrentRoute();
        else
        this.router.navigate(['/OutWeighmentIn']);

      }
      else
      Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));

  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
}

}
