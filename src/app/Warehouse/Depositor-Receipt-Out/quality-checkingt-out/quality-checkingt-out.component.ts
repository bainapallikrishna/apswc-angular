import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-quality-checkingt-out',
  templateUrl: './quality-checkingt-out.component.html',
  styleUrls: ['./quality-checkingt-out.component.css']
})
export class QualityCheckingtOutComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUserName: string = sessionStorage.getItem("logUserCode");
  isLoggedIn: string = sessionStorage.getItem("isLoggedIn");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  showloader: boolean = false;
  isSubmit: boolean = false;
  istable: boolean = false;
  QualtyForm: FormGroup;
  Qparms: FormArray;
  Tokenlist: any[];
  dep_comf_list: any[];
  comd_vartylist: any[];
  gradelist: any[];
  qualtypramlist: any[];
  comdtylist: any[];

  gridColumnApi: any = [];
  gridApi: any = [];
  public rowseledted: object;
  frameworkComponents: any;
  public components;


  columnDefs = [
    { headerName: '#', maxWidth: 60, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID', maxWidth: 150, field: 'tokeN_ID',onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        // below line is just to create empty without any action hyperlink
        // to trick the user, but actual action happen onViewCellCliced() // function
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Click On Token Id And Proceed",

      sortable: true, filter: true
    },
    { headerName: 'Depositor Name', maxWidth: 250, field: 'regisratioN_NAME', sortable: true, filter: true },
    { headerName: 'Reservation ID', maxWidth: 180, field: 'bookinG_ID', sortable: true, filter: true },
    { headerName: 'Commodity Group', maxWidth: 250, field: 'commoditY_GROUP', sortable: true, filter: true },
    { headerName: 'Commodity Name', maxWidth: 250, field: 'commodity', sortable: true, filter: true },
    // { headerName: 'Contract Type', maxWidth: 150, field: 'contracT_TYPE_NAME', sortable: true, filter: true },
    // { headerName: 'Quantity Type', maxWidth: 150, field: 'quantitY_TYPE_NAME', sortable: true, filter: true },
    // { headerName: 'Weight/Bags', maxWidth: 200, field: 'nO_OF_BAGS', sortable: true, filter: true },
    { headerName: 'Mode Of Transport', maxWidth: 250, field: 'modE_OF_TRANSPORT_NAME', sortable: true, filter: true },
    { headerName: 'Vehicle No', maxWidth: 150, field: 'vehiclE_NUMBER', sortable: true, filter: true }

  ];

  constructor(
    private router: Router,
    private service: CommonServices,
    private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

  }
  ngOnInit(): void {

    this.QualtyForm = this.formBuilder.group({
      //Tknid: [null, Validators.required],
      //DepositorName: [null, Validators.required],
      commoditygroup: ['', Validators.required],
      //commodity: [null, Validators.required],
      grade: [null, Validators.required],
      Variety: [null, Validators.required],
      Qparms: new FormArray([
        this.formBuilder.group({
          PramID: ['', Validators.required],
          PramName: ['', Validators.required],
          pramvalue: ['', Validators.required],
          pramentervalue: [''],


        })
      ])

    });



  }


  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.loadtokenlist()
  }

  
  loadtokenlist() {
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.workLocationCode;    
    this.service.postData(req, "QC_OUT_Tokens").subscribe(data => {
      if (data.StatusCode == "100") {

        this.Tokenlist = data.Details;
        this.gridApi.setRowData(this.Tokenlist);
        this.showloader = false;

      }
      else {
        //Swal.fire('warning', data.StatusMessage, 'warning');
        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.Tokenlist);
        this.showloader = false;
      }

    },

      error => console.log(error));
    this.showloader = false;


  }


  makeCellClicked(event) {
    
    this.comd_vartylist = [];
    this.gradelist = [];
    this.istable = false;
    this.qualtypramlist = [];
    this.qualty.Variety.setValue(null);
    this.qualty.commoditygroup.setValue("");
    this.qualty.grade.setValue(null);
    this.showloader = true;
    if(event.node.selected==true){
      this.rowseledted={};    
      this.loadvarieties(event.node.data, "VARIETY")
      this.rowseledted = event.node.data;
      
    }
    

  
  }

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
    //window.alert('selection changed, ' + rowCount + ' rows selected');
  }

  loadvarieties(slctddata: any, val) {


    const req = new InputRequest();
    if (val == "VARIETY") {
      
      req.INPUT_01 = slctddata.commoditY_CODE.trim();
      req.INPUT_02 = slctddata.commodity_id.trim();
      req.INPUT_04 = val;
    }
    else {
      this.gradelist = [];
      req.INPUT_01 = slctddata.commoditY_CODE.trim();
      req.INPUT_02 = slctddata.commodity_id.trim();
      req.INPUT_03 = this.qualty.Variety.value.split(":")[0];
      req.INPUT_04 = val;
    }
    this.service.postData(req, "GetVariety_GradeList").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        if (val == "VARIETY")
          this.comd_vartylist = data.Details;
        else {
          this.gradelist = data.Details;

        }

      }
      else {
        //Swal.fire('warning', data.StatusMessage, 'warning');
        console.log(data.StatusMessage);
        this.showloader = false;
      }

    },

      error => console.log(error));
    this.showloader = false;

  }

  loadgrades(val) {
    
    
    this.gradelist = [];
    this.istable = false;
    this.qualtypramlist = [];       
    this.qualty.grade.setValue(null);
    if(this.qualty.Variety.value){
    this.showloader=true;
    this.loadvarieties(this.rowseledted, val)
    }
  }
  LoadQualtyprams() {

    this.qualty.commoditygroup.setValue("");
    const rowsleted=this.rowseledted as any;
    this.qualtypramlist = [];
    this.QP.reset();
    this.QP.clear();
    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = rowsleted.commoditY_CODE.trim();
    req.INPUT_02 = rowsleted.commodity_id.trim();
    req.INPUT_03 = this.qualty.Variety.value.split(":")[0];
    req.INPUT_04 = this.qualty.grade.value.split(":")[0];
    this.qualty.commoditygroup.setValue(rowsleted.commoditY_GROUP);
    this.service.postData(req, "GetQualityPrameters").subscribe(data => {
      if (data.StatusCode == "100") {

        this.qualtypramlist = data.Details;
        this.AddQp();        
        this.FillQPDetails(this.qualtypramlist);
        this.showloader = false;

      }
      else {
        Swal.fire('warning', data.StatusMessage, 'warning');
        this.showloader = false;
      }

    },

      error => console.log(error));
    this.showloader = false;



  }

  AddQp() {
    this.QP.push(this.formBuilder.group({
      PramID: ['', Validators.required],
      PramName: ['', Validators.required],
      pramvalue: ['', Validators.required],
      pramentervalue: [''],


    }));

  }


  FillQPDetails(QPdata: any) {
    for (let i = 0; i < QPdata.length; i++) {
      if (i != 0) {
        this.QP.push(this.formBuilder.group({
          PramID: ['', Validators.required],
          PramName: ['', Validators.required],
          pramvalue: ['', Validators.required],
          pramentervalue: [''],


        }));
      }

      this.QP.controls[i].patchValue({
        PramID: QPdata[i].qtY_ID,
        PramName: QPdata[i].qtY_NAME,
        pramvalue: QPdata[i].percentage,
      });

    }
    this.istable = true;
  }

  get qualty() { return this.QualtyForm.controls; }
  get QP() { return this.qualty.Qparms as FormArray; }



  SaveQualityChecking() {
    this.isSubmit = true;

    // stop here if form is invalid
    if (this.QualtyForm.invalid) {
      return false;
    }

    for (let i = 0; i < this.QP.length; i++) {
      if (!this.QP.value[i].pramentervalue) {
        Swal.fire('warning', "Please Enter All Quality Parameters", 'warning');
        return false;
      }
    }

    let QP_idlist: string[] = [];
    let QP_namelist: string[] = [];
    let QP_ACTUALlist: string[] = [];
    let QP_ENTERlist: string[] = [];

    for (let i = 0; i < this.QP.length; i++) {
      if (!this.QP.value[i].pramentervalue) {
        Swal.fire('warning', "Please Enter All Quality Parameters", 'warning');
        return false;
      }
      QP_idlist.push(this.QP.value[i].PramID);
      QP_namelist.push(this.QP.value[i].PramName);
      QP_ACTUALlist.push(this.QP.value[i].pramvalue);
      QP_ENTERlist.push(this.QP.value[i].pramentervalue);
    }

    const rowsleted=this.rowseledted as any;
    const req = new InputRequest();
    // req.INPUT_01 = this.qualty.Tknid.value;
    // req.INPUT_02 = this.qualty.DepositorName.value.split(":")[1];//booking id
    // req.INPUT_03 = this.qualty.DepositorName.value.split(":")[2];//regist id
    // req.INPUT_04 = this.workLocationCode;//WHID
    // req.INPUT_05 = this.qualty.commoditygroup.value.split(":")[0];
    // req.INPUT_06 = this.qualty.commoditygroup.value.split(":")[1];
    // req.INPUT_07 = this.qualty.commodity.value.split(":")[0];
    // req.INPUT_08 = this.qualty.commodity.value.split(":")[1];
    
    req.INPUT_01 = rowsleted.tokeN_ID;
    req.INPUT_02 = rowsleted.bookinG_ID;//booking id
    req.INPUT_03 = rowsleted.reservatioN_ID//regist id
    req.INPUT_04 = this.workLocationCode;//WHID
    req.INPUT_05 = rowsleted.commoditY_CODE;
    req.INPUT_06 = rowsleted.commoditY_GROUP;
    req.INPUT_07 =  rowsleted.commodity_id;
    req.INPUT_08 =  rowsleted.commodity;
    req.INPUT_09 = this.qualty.Variety.value.split(":")[0];
    req.INPUT_10 = this.qualty.Variety.value.split(":")[1];
    req.INPUT_11 = this.qualty.grade.value.split(":")[0];
    req.INPUT_12 = this.qualty.grade.value.split(":")[1];
    req.INPUT_13 = QP_namelist.join(',');
    req.INPUT_14 = QP_idlist.join(',');
    req.INPUT_15 = QP_ACTUALlist.join(',');//percentage
    req.INPUT_16 = QP_ENTERlist.join(',');
    req.INPUT_17 = "2";//Receipt out
    req.CALL_SOURCE = "WEB";
    req.USER_NAME = this.logUserName;
    this.service.postData(req, "SaveQualityChecking").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        let result = data.Details[0];
        if (result.rtN_ID === 1) {
          Swal.fire('success', "Quality Checking Details Saved Successfully !!!", 'success');
          this.reloadCurrentRoute();

        }
        else {
          Swal.fire('warning', result.statuS_TEXT, 'warning');
        }

      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));




  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }



  //old--req
  LoadDep_ComodityDetails() {
    this.dep_comf_list = [];
    this.qualtypramlist = [];
    this.comd_vartylist = [];
    this.gradelist = [];
    this.istable = false;
    this.qualty.DepositorName.setValue(null);
    this.qualty.commoditygroup.setValue(null);
    this.qualty.commodity.setValue(null);
    this.qualty.Variety.setValue(null);
    this.qualty.grade.setValue(null);

    this.showloader = true;
    const req = new InputRequest();
    req.INPUT_01 = this.qualty.Tknid.value;
    req.INPUT_02 = "COMDLISTOFTKN";
    this.service.postData(req, "WeighmentTokenList").subscribe(data => {
      if (data.StatusCode == "100") {

        this.dep_comf_list = data.Details;
        this.showloader = false;

      }
      else {
        //Swal.fire('warning', data.StatusMessage, 'warning');
        console.log(data.StatusMessage);
        this.showloader = false;
      }

    },

      error => console.log(error));
    this.showloader = false;

  }

  changedepstrname() {

    //alert(this.qualty.DepositorName.value.split(":")[1]);
    this.istable = false;
    this.qualtypramlist = [];
    this.comd_vartylist = [];
    this.gradelist = [];
    this.comdtylist = [];
    this.qualty.commoditygroup.setValue(null);
    this.qualty.commodity.setValue(null);
    this.qualty.Variety.setValue(null);
    this.qualty.grade.setValue(null);
    if (this.qualty.DepositorName.value) {
      this.comdtylist = this.dep_comf_list.filter(m => m.bookinG_ID == this.qualty.DepositorName.value.split(":")[1]);
      this.fillcommoditys(this.comdtylist);
    }


  }
  fillcommoditys(slctddata: any) {
    this.qualty.commoditygroup.setValue(slctddata.commoditY_CODE);
    this.qualty.commodity.setValue(slctddata.commodity_id);

  }
  changecmodtygrup() {

    this.comd_vartylist = [];
    this.gradelist = [];
    this.istable = false;

    this.qualty.commodity.setValue(null);
    this.qualty.Variety.setValue(null);
    this.qualty.grade.setValue(null);


  }

 
}

