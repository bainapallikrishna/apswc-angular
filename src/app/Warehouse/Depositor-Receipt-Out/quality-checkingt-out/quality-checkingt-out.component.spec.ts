import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QualityCheckingtOutComponent } from './quality-checkingt-out.component';

describe('QualityCheckingtOutComponent', () => {
  let component: QualityCheckingtOutComponent;
  let fixture: ComponentFixture<QualityCheckingtOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QualityCheckingtOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QualityCheckingtOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
