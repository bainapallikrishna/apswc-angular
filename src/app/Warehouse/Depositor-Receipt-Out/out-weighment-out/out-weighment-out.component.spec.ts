import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutWeighmentOutComponent } from './out-weighment-out.component';

describe('OutWeighmentOutComponent', () => {
  let component: OutWeighmentOutComponent;
  let fixture: ComponentFixture<OutWeighmentOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutWeighmentOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutWeighmentOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
