import { Component, OnInit,ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';


@Component({
  selector: 'app-out-weighment-out',
  templateUrl: './out-weighment-out.component.html',
  styleUrls: ['./out-weighment-out.component.css']
})
export class OutWeighmentOutComponent implements OnInit {
  @ViewChild('agGrid') agGrid: AgGridAngular;
  showloader: boolean = false;
  isSubmit: boolean = false;

  isTable:boolean = false;
  WehoutForm: FormGroup;
  Tokenlist: any = [];
  tokeninfo: any = [];

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  gridColumnApi: any = [];
  gridApi: any = [];
  public rowseledted: object;
  frameworkComponents: any;
  public components;
  Tareweight:any;
  tarevalue:any;
  public defaultColDef;

  columnDefs = [
    { headerName: '#', width: 60,floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID', width: 100, field: 'tokeN_ID',onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Token Id",

    },
    { headerName: 'Depositor ID', width: 100, field: 'reservatioN_ID',   },
    { headerName: 'Depositor Name', width: 200, field: 'regisratioN_NAME',   },
   
    { headerName: 'Commodity Name', width: 200, field: 'commodity',   },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID',   },
    // { headerName: 'Stack In Date', width: 200, field: 'stacK_IN_DATE',   },

    { headerName: 'Contract Type', width: 150, field: 'contracT_TYPE_NAME',   },

   // { headerName: 'Quantity Type', width: 250, field: 'quantitY_TYPE_NAME',   },
    //{ headerName: 'No Of Bags', width: 100, field: 'nO_OF_BAGS',   },
    //{ headerName: "Weight(MT's)", width: 100, field: 'weight',   },
      
    { headerName: 'Mode Of Transport', width: 150, field: 'modE_OF_TRANSPORT_NAME',   },
    { headerName: 'Vehicle No', width: 100, field: 'vehiclE_NUMBER',   }

  ];


  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText: true,
      autoHeight: true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };
    
    this.WehoutForm = this.formBuilder.group({
      //Tknid: [null, Validators.required],
      GrossWeight: ['', Validators.required],
      Netweight:[''],
      tarevalue:[''],
      
    });

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };


   }

  ngOnInit(): void {
  }
  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.LoadTokens();
  }


  get dep() { return this.WehoutForm.controls; }

  LoadTokens() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode}

    this.service.postData(obj, "GetWeighOutTokens").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.Tokenlist = data.Details;
        this.gridApi.setRowData(this.Tokenlist);
      }
      else {
        
        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.Tokenlist);
        
      }
    },
      error => console.log(error));
  }
  
  makeCellClicked(event) {
        
    if(event.node.selected==true){
      this.rowseledted={};    
      this.isTable = true;
      this.WehoutForm.controls.tarevalue.setValue(event.node.data.tarE_WEIGHT);
      this.rowseledted = event.node.data;
      
    }
    

  
  }

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
    //window.alert('selection changed, ' + rowCount + ' rows selected');
  }

  GetTokenInfo(event) {
    this.tokeninfo = [];
    if (event) {
      this.showloader = true;
      let obj: any = { "INPUT_01": event,"INPUT_02": "COMDLISTOFTKN" }

      this.service.postData(obj, "Weighment_OUTTokenList").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.tokeninfo = data.Details;
          this.filldetails(this.tokeninfo)
          this.isTable=true;
        }
      },
        error => console.log(error));
    }

  }

  filldetails(data: any) {

    this.dep.grswght.setValue(data[0].grosS_WEIGHT);
  }
  Loadnetweght(val) 
  {
   
  this.dep.Netweight.setValue((Number(this.dep.GrossWeight.value)-Number(this.dep.tarevalue.value)).toFixed(3));
  }
  SaveWeightDetails(stype){
    this.isSubmit = true;
    const savetype:string=stype;
    // stop here if form is invalid
    if (this.WehoutForm.invalid) {
      return false;
    }

    if(Number(this.dep.tarevalue.value) > this.dep.GrossWeight.value){
      Swal.fire('warning', "Empty Vehicle Weight not greater than Gross Weight", 'warning');
      return false;
    }

    this.showloader = true;
    const rowsleted=this.rowseledted as any;
    const req = new InputRequest();
    req.INPUT_01 =this.dep.GrossWeight.value;
    req.INPUT_02 = rowsleted.tokeN_ID;//this.dep.Tknid.value;
    req.INPUT_04 = (this.dep.GrossWeight.value-this.dep.tarevalue.value).toString(); 
    req.INPUT_05 = this.workLocationCode;
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";
    this.service.postData(req, "SaveOutWeighmentOut").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Token : " + rowsleted.tokeN_ID + " , Weighment Out Deatils Saved Successfully !!!", 'success');
        if (savetype == 'next')
          this.router.navigate(['/Out_GateOut']);
        else
          this.reloadCurrentRoute();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }
}
