import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';
import { AgGridAngular } from 'ag-grid-angular';
@Component({
  selector: 'app-out-weighment-in',
  templateUrl: './out-weighment-in.component.html',
  styleUrls: ['./out-weighment-in.component.css']
})
export class OutWeighmentInComponent implements OnInit {
  OutWeighmentinForm: FormGroup;
  showloader: boolean = false;
  frameworkComponents: any;
  public components;
  gridColumnApi: any = [];
  tokens: any = [];
  gridApi: any = [];
  isTable: boolean = false;
  public rowseledted: object;
  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");
  deprecWeight: any;
  idepSubmit: boolean = false;
  public defaultColDef;

  columnDefs = [
    { headerName: '#', width: 60,floatingFilter: false, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID', width: 100, field: 'tokeN_ID',onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Token Id"
    },

    { headerName: 'Depositor ID', width: 100, field: 'registratioN_ID' },
    { headerName: 'Depositor Name', width: 200, field: 'registraioN_NAME' },

    { headerName: 'Commodity Name', width: 200, field: 'commodity' },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID' },
    { headerName: 'Stack In Date', width: 200, field: 'stacK_IN_DATE' },

    { headerName: 'Contract Type', width: 150, field: 'contracT_TYPE_NAME' },

    // { headerName: 'Quantity Type', width: 250, field: 'quantitY_TYPE_NAME'  },
    { headerName: 'No Of Bags', width: 100, field: 'bag' },
    { headerName: "Weight(MT's)", width: 100, field: 'weight' },

    // { headerName: 'Mode Of Transport', width: 250, field: 'modE_OF_TRANSPORT_NAME'  },
    // { headerName: 'Vehicle No', width: 150, field: 'vehiclE_NUMBER'  }

  ];
  constructor(private formbuilder: FormBuilder, private service: CommonServices, private router: Router) {

    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText:true,
      autoHeight:true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.OutWeighmentinForm = this.formbuilder.group({

      deprecWeight: ['', Validators.required]
    });


    this.components = {

      rowIdRenderer: function (params) {

        return '' + (params.rowIndex + 1);
      }
    }

  }

  ngOnInit(): void {
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.LoadTokens();
  }
  LoadTokens() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetWeighinTokens").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));
  }
  makeCellClicked(event) {

    if (event.node.selected == true) {
      this.rowseledted = {};
      this.isTable = true;

      this.rowseledted = event.node.data;

    }
  }
  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
    //window.alert('selection changed, ' + rowCount + ' rows selected');
  }
  get outweighform() { return this.OutWeighmentinForm.controls; }

  SaveoutWeightDetails(stype) {
    const savetype: string = stype;
    this.idepSubmit = true;

    // stop here if form is invalid
    if (this.OutWeighmentinForm.invalid) {
      return false;
    }

    this.showloader = true;
    const rowsleted = this.rowseledted as any;
    const req = new InputRequest();
    req.INPUT_01 = rowsleted.tokeN_ID;//this.dep.TokenNo.value;
    req.INPUT_02 = this.workLocationCode;
    req.INPUT_03 = this.outweighform.deprecWeight.value;
    req.INPUT_04 = "2";//Receipt Out
    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveOutWeighmentIn").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Token : " + rowsleted.tokeN_ID + " , Weighment IN Deatils Saved Successfully !!!", 'success');
        if (savetype == 'save')
          this.reloadCurrentRoute();
        else
          this.router.navigate(['/StackOut']);
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }
  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }
}
