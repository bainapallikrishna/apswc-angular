import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutWeighmentInComponent } from './out-weighment-in.component';

describe('OutWeighmentInComponent', () => {
  let component: OutWeighmentInComponent;
  let fixture: ComponentFixture<OutWeighmentInComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutWeighmentInComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutWeighmentInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
