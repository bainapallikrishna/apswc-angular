import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StackOutComponent } from './stack-out.component';

describe('StackOutComponent', () => {
  let component: StackOutComponent;
  let fixture: ComponentFixture<StackOutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StackOutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StackOutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
