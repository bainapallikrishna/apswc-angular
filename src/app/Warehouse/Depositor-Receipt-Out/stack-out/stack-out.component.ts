import { Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AgGridAngular } from 'ag-grid-angular';
import { InputRequest } from 'src/app/Interfaces/employee';
import { CommonServices } from 'src/app/Services/common.services';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-stack-out',
  templateUrl: './stack-out.component.html',
  styleUrls: ['./stack-out.component.css']
})
export class StackOutComponent implements OnInit {

  @ViewChild('agGrid') agGrid: AgGridAngular;
  showloader: boolean = false;
  isSubmit: boolean = false;
  isBagWeight: boolean = false;
  DepForm: FormGroup;
  tokens: any = [];
  commodities: any = [];
  godowns: any = [];
  comartments: any = [];
  stacktypes: any = [];
  stacks: any = [];

  logUserrole: string = sessionStorage.getItem("logUserrole");
  isPasswordChanged: string = sessionStorage.getItem("logUserisChangePassword");
  logUsercode: string = sessionStorage.getItem("logUserCode");
  workLocationCode = sessionStorage.getItem("logUserworkLocationCode");

  isTable: boolean = false;
  gridColumnApi: any = [];
  gridApi: any = [];
  public rowseledted: object;
  frameworkComponents: any;
  public components;
  public defaultColDef;
  htcomlist: string[] = [];
  htserlist: string[] = [];
  htpriclist: string[] = [];

  columnDefs = [
    { headerName: '#', width: 60, cellRenderer: 'rowIdRenderer', },
    {
      headerName: 'Token ID', width: 100, field: 'tokeN_ID', onCellClicked: this.makeCellClicked.bind(this),
      cellRenderer: function (params) {
        return '<u style="color:blue">' + params.value + '</u>';
      },
      tooltipField: "TokenId", headerTooltip: "Token Id",
    },
    { headerName: 'Depositor ID', width: 100, field: 'registratioN_ID', },
    { headerName: 'Depositor Name', width: 200, field: 'registratioN_NAME', },
    { headerName: 'Commidity Name', width: 200, field: 'commodity', },
    { headerName: 'Reservation ID', width: 100, field: 'bookinG_ID', },
    { headerName: 'Stack In Date', width: 200, field: 'stacK_IN_DATE', },
    { headerName: 'Contract Type', width: 150, field: 'contracT_TYPE_NAME', },
    { headerName: 'No Of Bags', width: 100, field: 'bags', },
    { headerName: "Weight(MT's)", width: 100, field: 'weight', }

  ];

  constructor(private router: Router, private service: CommonServices, private formBuilder: FormBuilder) {
    if (!this.logUserrole || this.isPasswordChanged == "0") {
      this.router.navigate(['/Login']);
      return;
    }

    this.defaultColDef = {
      editable: false,
      sortable: true,
      //flex: 1,
      minWidth: 60,
      wrapText: true,
      autoHeight: true,
      filter: true,
      floatingFilter: true,
      resizable: true,
      suppressMenu: true,
      floatingFilterComponentParams: {
        suppressFilterButton: true,
        color: 'green',
      },
    };

    this.components = {
      rowIdRenderer: function (params) {
        return '' + (params.rowIndex + 1);
      },
    };

    this.DepForm = this.formBuilder.group({

      HTCompanyName: [null, Validators.required],
      ServiceName: [null, Validators.required],
      BagWeight: [''],
      ServicePrice: [null, Validators.required],
      MarketValue: [''],
      GunnyBags: [''],
      stackings: new FormArray([
        this.formBuilder.group({
          godowN_ID: [''],
          godowN_NAME: [''],
          compartmenT_ID: [''],
          compartmenT_NAME: [''],
          stacK_ID: [''],
          stacK_NAME: [''],
          stacK_TYPE: [''],
          stacK_TYPE_NAME: [''],
          nO_OF_BAGS: [''],
          weight: [''],
          Out_Bags: ['', Validators.required],
          Out_weight: [''],
          TransId: [''],
          StackinDate: [''],
        })
      ])
    });
  }

  ngOnInit(): void {
    this.LoadHTCompanyServices();
  }

  BindData(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

    this.LoadTokens();
  }

  get dep() { return this.DepForm.controls; }
  get s() { return this.dep.stackings as FormArray; }

  AddStack() {
    this.s.push(this.formBuilder.group({
      GodownName: [null, Validators.required],
      CompartmentName: [null, Validators.required],
      StackType: [null, Validators.required],
      StackName: [null, Validators.required],
      BagsCnt: ['', Validators.required],
      VWeight: ['', Validators.required],
    }));
  }

  RemoveStack(index) {
    this.s.removeAt(index);
  }

  LoadTokens() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetReceiptDetails").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.tokens = data.Details;
        this.gridApi.setRowData(this.tokens);
      }
      else {

        console.log(data.StatusMessage);
        this.gridApi.setRowData(this.tokens);

      }
    },
      error => console.log(error));
  }

  GetTokenInfo(event) {
    this.showloader = true;
    let obj: any = { "INPUT_01": event.bookinG_ID, "INPUT_02": event.registratioN_ID, "INPUT_03": event.warehousE_ID, "INPUT_04": event.commoditY_ID }
    this.service.postData(obj, "GetStackInCommodities").subscribe(data => {
      this.showloader = false;
      this.s.reset();
      this.s.clear();
      if (data.StatusCode == "100") {
        this.commodities = data.Details;
        this.FillStackinDetails(data.Details);
      }
    },
      error => console.log(error));
  }

  LoadHTCompanyServices() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetWHHTContractors").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.htcomlist = data.Details;
        if (this.htcomlist.length == 1) {
          this.dep.HTCompanyName.setValue(data.Details[0].hT_REGISTRATION_ID);
          this.LoadHTServices(data.Details[0].hT_REGISTRATION_ID);
        }
      }
    },
      error => console.log(error));
  }

  LoadHTServices(event) {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode, "INPUT_02": "1", "INPUT_03": "Outward", "INPUT_04": event }

    this.service.postData(obj, "GetHTServicesList").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.htserlist = data.Details;
      }
    },
      error => console.log(error));
  }

  FillStackinDetails(data) {
    for (let i = 0; i < data.length; i++) {
      this.s.push(this.formBuilder.group({
        godowN_ID: [''],
        godowN_NAME: [''],
        compartmenT_ID: [''],
        compartmenT_NAME: [''],
        stacK_ID: [''],
        stacK_NAME: [''],
        stacK_TYPE: [''],
        stacK_TYPE_NAME: [''],
        nO_OF_BAGS: [''],
        weight: [''],
        Out_Bags: ['', Validators.required],
        Out_weight: [''],
        TransId: [''],
        StackinDate: [''],
      }));

      this.s.controls[i].patchValue({
        godowN_ID: data[i].godowN_ID,
        godowN_NAME: data[i].godowN_NAME,
        compartmenT_ID: data[i].compartmenT_ID,
        compartmenT_NAME: data[i].compartmenT_NAME,
        stacK_ID: data[i].stacK_ID,
        stacK_NAME: data[i].stacK_NAME,
        stacK_TYPE: data[i].stacK_TYPE,
        stacK_TYPE_NAME: data[i].stacK_TYPE_NAME,
        nO_OF_BAGS: data[i].nO_OF_BAGS,
        weight: data[i].weight,
        Out_Bags: '',
        Out_weight: '',
        TransId: data[i].trx_id,
        StackinDate: data[i].stacK_IN_DATE,
      });

    }
  }

  LoadGodowns() {
    this.showloader = true;
    let obj: any = { "INPUT_01": this.workLocationCode }

    this.service.postData(obj, "GetGodowns").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.godowns = data.Details;
      }
    },
      error => console.log(error));
  }

  LoadStackTypes() {
    this.showloader = true;

    this.service.getData("GetStackTypes").subscribe(data => {
      this.showloader = false;
      if (data.StatusCode == "100") {
        this.stacktypes = data.Details;
      }
    },
      error => console.log(error));
  }

  makeCellClicked(event) {
    if (event.node.selected == true) {
      this.rowseledted = {};
      this.isTable = true;

      this.rowseledted = event.node.data;
      this.ClearBookingDatails();
      if (event.node.data.baG_WEIGHT) {
        this.dep.BagWeight.setValue(event.node.data.baG_WEIGHT);
        this.isBagWeight = true;
      }
      else
        this.isBagWeight = false;
      this.GetTokenInfo(this.rowseledted);
    }
  }

  GetPrice(event) {
    if (this.dep.ServiceName.value && this.dep.BagWeight.value) {
      this.htpriclist = [];
      this.showloader = true;
      let obj: any = { "INPUT_01": this.workLocationCode, "INPUT_02": "1", "INPUT_03": "Outward", "INPUT_04": this.dep.ServiceName.value, "INPUT_05": this.dep.BagWeight.value, "INPUT_06": this.dep.HTCompanyName.value }

      this.service.postData(obj, "GetHTServicesPrice").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.htpriclist = data.Details;
        }
      },
        error => console.log(error));
    }
    else {
      this.htpriclist = [];
    }
  }

  CalStackWeight(event,index){
    // if(this.dep.BagWeight.value && this.s.value[index].Out_Bags){
    //   this.s.controls[index].patchValue({Out_weight:((Number(this.dep.BagWeight.value) * Number(this.s.value[index].Out_Bags))/1000).toFixed(3)})
    // }
    // else
    // this.s.controls[index].patchValue({Out_weight:''})
  }

  onSelectionChanged(event) {
    var rowCount = event.api.getSelectedNodes().length;
  }

  GetCommodityInfo(event) {
    this.ClearBookingDatails();
    const data = this.commodities.filter(x => x.bookinG_ID == event);
    if (data[0]) {
      this.FillBookingDatails(data[0]);
    }
  }

  FillBookingDatails(data) {
    this.dep.CommodityCode.setValue(data.commoditY_ID);
    this.dep.DepositorName.setValue(data.registratioN_NAME);
    this.dep.ContractType.setValue(data.contracT_TYPE_NAME);
    this.dep.MOT.setValue(data.modE_OF_TRANSPORT_NAME);
    this.dep.VehicleNO.setValue(data.vehiclE_NUMBER);
  }

  ClearBookingDatails() {
    this.DepForm.reset();
    this.htserlist = [];
    this.htpriclist = [];
    this.comartments = [];
    this.stacks = [];
    this.s.reset();
    this.s.clear();
  }

  GetCompartmentInfo(event, ind) {
    this.comartments = [];
    this.stacks = [];

    this.s.controls[ind].patchValue({
      CompartmentName: null,
      StackType: null,
      StackName: null,
      BagsCnt: '',
      VWeight: '',
    });

    if (event) {
      this.showloader = true;
      let obj: any = { "INPUT_01": this.workLocationCode, "INPUT_02": event }

      this.service.postData(obj, "GetCompartments").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.comartments = data.Details;
        }
      },
        error => console.log(error));
    }
  }

  GetStacksInfo(event, ind) {
    this.stacks = [];
    this.s.controls[ind].patchValue({
      StackType: null,
      StackName: null,
      BagsCnt: '',
      VWeight: '',
    });

    if (event) {
      this.showloader = true;
      let obj: any = { "INPUT_01": this.workLocationCode, "INPUT_02": this.s.value[ind].GodownName, "INPUT_03": event }

      this.service.postData(obj, "GetStacks").subscribe(data => {
        this.showloader = false;
        if (data.StatusCode == "100") {
          this.stacks = data.Details;
        }
      },
        error => console.log(error));
    }
  }

  SaveStacking(stype) {
    this.isSubmit = true;
    const savetype: string = stype;

    // stop here if form is invalid
    if (this.DepForm.invalid) {
      return false;
    }

    let godlist: string[] = [];
    let comlist: string[] = [];
    let stypelist: string[] = [];
    let staclist: string[] = [];
    let bagslist: string[] = [];
    let weilist: string[] = [];
    let trxidlist: string[] = [];
    let stkdtlist: string[] = [];

    for (let i = 0; i < this.s.length; i++) {
      if (Number(this.s.value[i].Out_Bags) > Number(this.s.value[i].nO_OF_BAGS)) {
        Swal.fire('warning', "Stack-Out Bags is not greater than Stack-in Bags", 'warning');
        return false;
      }
      // if (Number(this.s.value[i].Out_weight) > Number(this.s.value[i].weight)) {
      //   Swal.fire('warning', "Stack-Out Weight is not greater than Stack-in Weight", 'warning');
      //   return false;
      // }
      if (Number(this.s.value[i].Out_Bags) > 0) {
        godlist.push(this.s.value[i].godowN_ID);
        comlist.push(this.s.value[i].compartmenT_ID);
        stypelist.push(this.s.value[i].stacK_TYPE);
        staclist.push(this.s.value[i].stacK_ID);
        bagslist.push(this.s.value[i].Out_Bags);
        weilist.push(this.s.value[i].Out_weight);
        trxidlist.push(this.s.value[i].TransId);
        stkdtlist.push(this.s.value[i].StackinDate);
      }
    }

    this.showloader = true;
    const req = new InputRequest();
    const rowsleted = this.rowseledted as any;
    const bcount: string = rowsleted.bookinG_COUNT;
    req.INPUT_01 = this.workLocationCode;
    req.INPUT_02 = rowsleted.tokeN_ID;
    req.INPUT_03 = rowsleted.bookinG_ID
    req.INPUT_04 = rowsleted.commoditY_ID;


    req.INPUT_05 = godlist.join(',');
    req.INPUT_06 = comlist.join(',');
    req.INPUT_07 = staclist.join(',');
    req.INPUT_08 = stypelist.join(',');
    req.INPUT_09 = bagslist.join(',');
    req.INPUT_10 = weilist.join(',');
    req.INPUT_11 = "2";//Receipt In
    req.INPUT_12 = rowsleted.registratioN_ID;
    req.INPUT_13 = rowsleted.registratioN_TYPE;
    req.INPUT_14 = trxidlist.join(',');
    req.INPUT_15 = stkdtlist.join(',');
    req.INPUT_16 = this.dep.HTCompanyName.value;
    req.INPUT_17 = this.dep.ServiceName.value;
    req.INPUT_18 = "1";
    req.INPUT_19 = this.dep.BagWeight.value;
    req.INPUT_20 = this.dep.ServicePrice.value;
    req.INPUT_21 = null;//this.dep.MarketValue.value;
    req.INPUT_22 = this.dep.GunnyBags.value;
    req.INPUT_23 = "Outward";

    req.USER_NAME = this.logUsercode;
    req.CALL_SOURCE = "WEB";

    this.service.postData(req, "SaveStacking").subscribe(data => {
      this.showloader = false;

      if (data.StatusCode == "100") {
        Swal.fire('success', "Token : " + rowsleted.tokeN_ID + " , Stack-Out Deatils Saved Successfully !!!", 'success');
        if (savetype == 'next' && bcount == '1')
          this.router.navigate(['/OutWeignmentOut']);
        else
          this.reloadCurrentRoute();
      }
      else
        Swal.fire('warning', data.StatusMessage, 'warning');

    },
      error => console.log(error));
  }

  reloadCurrentRoute() {
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', { skipLocationChange: true }).then(() => {
      this.router.navigate([currentUrl]);
    });
  }

}
